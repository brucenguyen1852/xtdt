﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.CacheMemory
{
    public class CacheMemoryDistributedService
    {
        private readonly IDistributedCache _distributedCache;
        public CacheMemoryDistributedService(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }
        /// <summary>
        /// get object with key on memory cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<string> GetObjectCacheByKey(string key)
        {
            var objCache = await _distributedCache.GetAsync(key);
            string serializedListPropertiesDefault;
            if (objCache != null)
            {
                serializedListPropertiesDefault = Encoding.UTF8.GetString(objCache);
                return serializedListPropertiesDefault;
            }
            return string.Empty;
        }
        /// <summary>
        /// set object with key on memory cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="listobject"></param>
        /// <returns></returns>
        public async Task<bool> SetObjectCacheByKey(string key,string listobject)
        {
            try
            {
                var redisCustomerList = Encoding.UTF8.GetBytes(listobject);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTime.Now.AddDays(1));
                await _distributedCache.SetAsync(key, redisCustomerList, options);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// remover
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<bool> RemoveObjectCacheByKey(string key)
        {
            try
            {
                await _distributedCache.RemoveAsync(key);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> RemoveObjectCacheByKeyPage(string key, int page)
        {
            try
            {
                for (int i = 1; i <= page; i++)
                {
                    string keypage = string.Format("{0}_{1}", key, i);
                    await _distributedCache.RemoveAsync(keypage);
                    
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
