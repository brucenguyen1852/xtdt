﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoT.Common
{
    public static class ConvertGeojsonToWkt
    {
        public static string GeoJsonToWKT(string geoJson ="", Geometry geom = null)
        {
            //var allWKTResult = new List<WKTResult>();
            var geometry = !string.IsNullOrEmpty(geoJson)?JsonConvert.DeserializeObject<Geometry>(geoJson): geom;
            //foreach (var feature in geoJsonObject)
            //{
            //WKTResult oneGeometry = new WKTResult();
            string number = string.Empty;
            //if (feature.properties.TryGetValue("NUMBER", out number))
            //{
            //    oneGeometry.Number = number;
            //}
            //else
            //{
            //    oneGeometry.Number = "1";
            //}
            var geometryItem = geometry.coordinates;
            var oneGeometryResult = string.Empty;
            var coordinatesAll = new List<string>();
            if (geometry.type.ToUpper() == "POLYGON" || geometry.type.ToUpper() == "MULTILINESTRING")
            {
                var geometryList = JsonConvert.DeserializeObject<List<List<List<double>>>>(JsonConvert.SerializeObject(geometryItem));
                foreach (var cootdinates in geometryList)
                {
                    var onePloygon = new List<string>();
                    foreach (var coordinate in cootdinates)
                    {
                        var oneCoordinates = string.Join(" ", coordinate).Replace(",",".");
                        onePloygon.Add(oneCoordinates);
                    }
                    var onePloygonString = string.Format("({0})", string.Join(", ", onePloygon));
                    coordinatesAll.Add(onePloygonString);

                }
            }
            if (geometry.type.ToUpper() == "POINT")
            {
                var onePoint = geometryItem.OfType<double>().ToList();
                var oneCoordinates = string.Join(" ", onePoint).Replace(",",".");
                coordinatesAll.Add(oneCoordinates);
            }
            if (geometry.type.ToUpper() == "LINESTRING" || geometry.type.ToUpper() == "MULTIPOINT")
            {
                var geometryList = JsonConvert.DeserializeObject<List<List<double>>>(JsonConvert.SerializeObject(geometryItem));
                foreach (var coordinate in geometryList)
                {
                    var oneCoordinates = string.Join(" ", coordinate).Replace(",", ".");
                    coordinatesAll.Add(oneCoordinates);
                }

            }
            if (geometry.type.ToUpper() == "MULTIPOLYGON")
            {
                var geometryList = JsonConvert.DeserializeObject<List<List<List<List<double>>>>>(JsonConvert.SerializeObject(geometryItem));

                foreach (var cootdinateList in geometryList)
                {
                    var multiplePloygon = new List<string>();
                    foreach (var coordinates in cootdinateList)
                    {
                        var oneCoordinate = new List<string>();
                        foreach (var coordinate in coordinates)
                        {
                            var oneCoordinates = string.Join(" ", coordinate).Replace(",", ".");
                            oneCoordinate.Add(oneCoordinates);
                        }
                        var ploygonString = string.Format("({0})", string.Join(", ", oneCoordinate));
                        multiplePloygon.Add(ploygonString);
                    }
                    var multiplePloygonString = string.Format("({0})", string.Join(", ", multiplePloygon));
                    coordinatesAll.Add(multiplePloygonString);
                }
            }

            oneGeometryResult = string.Format("{0}({1})", geometry.type.ToUpper(), string.Join(", ", coordinatesAll));
            //oneGeometry.WKTGeometry = oneGeometryResult;
            //allWKTResult.Add(oneGeometry);
            //}
            return oneGeometryResult;
        }
    }
    public class WKTResult
    {
        public string Number { get; set; }
        public string WKTGeometry { get; set; }
    }
    public class Geometry
    {
        [JsonProperty("type")]
        [BsonElement("type")]
        public string type { get; set; }

        /// <summary>
        /// Coordinates
        /// </summary>
        [JsonProperty("coordinates")]
        [BsonElement("coordinates")]
        public List<object> coordinates { get; set; }
    }
}
