﻿using System;
using System.Collections.Generic;

namespace IoT.Common
{
    public class DataTableViewModel
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<Column> columns { get; set; }
        public Search search { get; set; }
        public List<Order> order { get; set; }

        public string Code { get; set; }
        public string TypeProperties { get; set; }
        //public int parentId { get; set; }
        //public int intervalTime { get; set; } = 30;
        //public string interval { get; set; } = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy") + "-" +
        //                                        DateTime.Now.ToString("dd/MM/yyyy");

        public class Column
        {
            public string data { get; set; }
            public string name { get; set; }
            public bool searchable { get; set; }
            public bool orderable { get; set; }
            public Search search { get; set; }
        }

        public class Search
        {
            public string value { get; set; }
            public string regex { get; set; }
        }

        public class Order
        {
            public int column { get; set; }
            public string dir { get; set; }
        }
    }
}
