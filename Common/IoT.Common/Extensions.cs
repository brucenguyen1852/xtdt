﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace IoT.Common
{
    public static class Extensions
    {
        /// <summary>
        /// Convert Uni Text
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ConvertToUnSign(string input)
        {
            input = input.Trim();
            for (int i = 0x20; i < 0x30; i++)
            {
                input = input.Replace(((char)i).ToString(), " ");
            }

            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string str = input.Normalize(NormalizationForm.FormD);
            string str2 = regex.Replace(str, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
            while (str2.IndexOf("?") >= 0)
            {
                str2 = str2.Remove(str2.IndexOf("?"), 1);
            }

            return str2;
        }

        /// <summary>
        /// Convert string from vietnamese character to latin character
        /// </summary>
        /// <param name="searchstring">search string</param>
        /// <returns>result string</returns>
        public static string StringConvert(string searchstring)
        {
            searchstring = searchstring.ToLower();
            string convert = "ĂÂÀẰẦÁẮẤẢẲẨÃẴẪẠẶẬỄẼỂẺÉÊÈỀẾẸỆÔÒỒƠỜÓỐỚỎỔỞÕỖỠỌỘỢƯÚÙỨỪỦỬŨỮỤỰÌÍỈĨỊỲÝỶỸỴĐăâàằầáắấảẳẩãẵẫạặậễẽểẻéêèềếẹệôòồơờóốớỏổởõỗỡọộợưúùứừủửũữụựìíỉĩịỳýỷỹỵđ";
            string to = "AAAAAAAAAAAAAAAAAEEEEEEEEEEEOOOOOOOOOOOOOOOOOUUUUUUUUUUUIIIIIYYYYYDaaaaaaaaaaaaaaaaaeeeeeeeeeeeooooooooooooooooouuuuuuuuuuuiiiiiyyyyyd";
            for (int i = 0; i < to.Length; i++)
            {
                searchstring = searchstring.Replace(convert[i], to[i]);
            }

            return searchstring;
        }

        /// <summary>
        /// Clone source
        /// </summary>
        /// <typeparam name="D"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static D Clone<D>(this object source)
        {
            string json = null;
            if (source is string temp)
            {
                json = temp;
            }
            else
            {
                json = JsonConvert.SerializeObject(source);
            }
            if (typeof(D) == typeof(string))
            {
                return ChangeType<D>(json);
            }
            var rs = JsonConvert.DeserializeObject<D>(json);
            return rs;
        }

        /// <summary>
        /// Change type t
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ChangeType<T>(object value)
        {
            return (T)ChangeType(typeof(T), value);
        }

        /// <summary>
        /// Change type of object
        /// </summary>
        /// <param name="t"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object ChangeType(Type t, object value)
        {
            TypeConverter tc = TypeDescriptor.GetConverter(t);
            return tc.ConvertFrom(value);
        }

        /// <summary>
        /// Convert List object to List of List Object (if object is a List) and countinue convert deeper if object is still a list
        /// </summary>
        /// <returns></returns>
        public static IList<object> ConvertToListObject(IList<object> data)
        {

            var listReturnObject = new List<object>();
            foreach (var item in data)
            {

                if (item is IEnumerable<object> && item is JArray temp)
                {
                    var thisValue = DeserializeData(temp);
                    if (thisValue != null)
                    {
                        listReturnObject.Add(thisValue);
                    }
                }
                else
                {
                    if (item.GetType().Name == "JsonElement")
                    {
                        var list = JsonConvert.DeserializeObject<List<object>>(item.ToString());
                        var datalist = DeserializeDataList(list);
                        if (datalist != null)
                        {
                            listReturnObject.Add(list);
                        }
                    }
                    else
                        listReturnObject.Add(item);
                }
            }
            return listReturnObject;
        }

        /// <summary>
        /// Deserialize Data from JArray to List object
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static IList<object> DeserializeData(JArray data)
        {
            var list = data.ToObject<List<object>>();

            for (int i = 0; i < list.Count; i++)
            {
                var value = list[i];

                if (value is JArray)
                {
                    list[i] = DeserializeData(value as JArray);
                }
            }
            return list;
        }

        /// <summary>
        /// Deserialize Data from JArray to List object
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static IList<object> DeserializeDataList(List<object> list)
        {
            //var list = data.ToObject<List<object>>();

            for (int i = 0; i < list.Count; i++)
            {
                var value = list[i];

                if (value is JArray)
                {
                    list[i] = DeserializeData(value as JArray);
                }
            }
            return list;
        }


        public static string RemoveWhitespace(this string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }

    }
}
