﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.File
{
    public class FileService //: IFileService
    {
        private StorageService storageService;
        private IConfiguration configuration;
        private IHttpClientFactory httpClientFactory;
        public FileService(IConfiguration _configuration, IHttpClientFactory _httpClientFactory)
        {
            configuration = _configuration;
            httpClientFactory = _httpClientFactory;
            storageService = new StorageService(configuration, httpClientFactory);
        }
        public async Task<bool> DeleteFileAsync(string url)
        {
            try
            {
                var result = await DeleteFile(url); //delete file
                //DeleteFileColletion(url); //delete database
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<FileView> UploadFileAsync(IFormFile file, TypeFile typeFile)
        {
            FileView result;
            string objFile = file.FileName;
            result = await UploadFile(objFile, typeFile, file.OpenReadStream());
            //var fileEntity = CreateFileEntity(result.Name, result.Url, typeFile, objFile);
            //var data = await InsertFile(fileEntity);
            result.Id = Guid.NewGuid().ToString();
            return result;
        }

        #region---private---
        /// <summary>
        /// DeleteFile
        /// </summary>
        /// <param name="objFile"></param>
        /// <param name="typeFile"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        private async Task<bool> DeleteFile(string url)
        {
            var urlUpload = await storageService.DeleteObjectAsync(url);
            return urlUpload;
        }
        /// <summary>
        /// Delete file entity
        /// </summary>
        /// <param name="url"></param>
        //private void DeleteFileColletion(string url)
        //{
        //    string urlnew = WebUtility.UrlDecode(url);
        //    var type = Builders<FileModel>.Filter.Where(e => urlnew.Equals(e.Url));
        //    var file = fileCollection.Find(type);
        //    if (file != null && file.Count > 0)
        //    {
        //        fileCollection.Remove(file[0].Id);
        //    }
        //}
        //private async Task<FileModel> InsertFile(FileModel fileEntity)
        //{
        //    return await InsertValueAsync(fileEntity);
        //}
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="fileEntity"></param>
        /// <returns></returns>
        //private async Task<FileModel> InsertValueAsync(FileModel fileEntity)
        //{
        //    try
        //    {
        //        return await fileCollection.InsertAsync(fileEntity); //.InsertAsync(fileEntity);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameFile">Name file</param>
        /// <param name="typeFile">Type file</param>
        /// <param name="stream">Stream file</param>
        /// <returns></returns>
        private async Task<FileView> UploadFile(string nameFile, TypeFile typeFile, Stream stream)
        {
            var extension = System.IO.Path.GetExtension(nameFile);
            var name = Guid.NewGuid().ToString(); //Utils.GenId;
            if (string.IsNullOrWhiteSpace(extension))
            {
                name += ".txt";
            }
            else
            {
                name += extension;
            }
            string urlUpload = string.Empty;
            FileView result = null;
            switch (typeFile)
            {
                case TypeFile.Obj:
                    urlUpload = await storageService.UploadObjectAsync(configuration.GetSection("StorageInfo:Object").Value, name, stream, TypeFile.Obj);
                    break;
                //case TypeFile.Icon:
                //    urlUpload = await storageService.UploadObjectAsync(appSetting.Path.Icon, name, stream, TypeFile.Icon);
                //    break;
                //case TypeFile.Thumbnail:
                //    urlUpload = await storageService.UploadObjectAsync(appSetting.Path.Thumbnail, name, stream, TypeFile.Thumbnail);
                //    break;
                case TypeFile.Texture:
                    urlUpload = await storageService.UploadObjectAsync(configuration.GetSection("StorageInfo:Texture").Value, name, stream, TypeFile.Texture);
                    break;
                case TypeFile.Image:
                    urlUpload = await storageService.UploadObjectAsync(configuration.GetSection("StorageInfo:Image").Value, name, stream, TypeFile.Image);
                    break;
                //case TypeFile.HashTag:
                //    urlUpload = await storageService.UploadObjectAsync(appSetting.Path.HashTag, name, stream, TypeFile.Image);
                //    break;
                //case TypeFile.PlaceComment:
                //    urlUpload = await storageService.UploadObjectAsync(appSetting.Path.PlaceComment, name, stream, TypeFile.PlaceComment);
                //    break;
                //case TypeFile.ReportError:
                //    urlUpload = await storageService.UploadObjectAsync(appSetting.Path.ReportError, name, stream, TypeFile.ReportError);
                //    break;
                case TypeFile.Video:
                    urlUpload = await storageService.UploadObjectAsync(configuration.GetSection("StorageInfo:Video").Value, name, stream, TypeFile.Video);
                    break;
                case TypeFile.Document:
                    urlUpload = await storageService.UploadObjectAsync(configuration.GetSection("StorageInfo:Document").Value, name, stream, TypeFile.Document);
                    break;
                default:
                    urlUpload = await storageService.UploadObjectAsync(configuration.GetSection("StorageInfo:Temp").Value, name, stream, TypeFile.Temp);
                    break;
            }
            if (!string.IsNullOrEmpty(urlUpload))
            {
                result = new FileView()
                {
                    Name = name,
                    Url = urlUpload,
                };
            }
            return result;
        }

        /// <summary>
        /// Create file entity
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="typeFile"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        //private FileModel CreateFileEntity(string fileName, string url, TypeFile typeFile, string description)
        //{
        //    var file = new FileModel
        //    {
        //        Description = description,
        //        Name = fileName,
        //        Type = typeFile,
        //        IsDeleted = false,
        //        Url = url

        //    };
        //    //file.SetCreateEntityDefault(httpContextAccessor?.HttpContext);
        //    return file;
        //}
        #endregion
    }
}
