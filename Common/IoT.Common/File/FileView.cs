﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Common.File
{
    public class FileView
    {
        public string Id { get; set; }
        /// <summary>
        /// Url to access
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Name file
        /// </summary>
        public string Name { get; set; }
    }
    public class FileDelete
    {
        public string Url { get; set; }
    }

    public class FileViewIcon
    {
        public string Id { get; set; }
        /// <summary>
        /// Url to access
        /// </summary>
        public string UrlIcon { get; set; }
        public string UrlMap { get; set; }

        /// <summary>
        /// Name file
        /// </summary>
        public string Name { get; set; }
    }
    public class FileDeleteIcon
    {
        public string UrlIcon { get; set; }
        public string UrlMap { get; set; }
    }
}
