﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.File
{
    public interface IFileService
    {
        /// <summary>
        /// Upload object file from form
        /// </summary>
        /// <param name="file"></param>
        /// <param name="typeFile"></param>
        /// <returns></returns>
        Task<FileView> UploadFileAsync(IFormFile file, TypeFile typeFile);
        Task<bool> DeleteFileAsync(string url);
    }
}
