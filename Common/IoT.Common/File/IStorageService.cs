﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.File
{
    public interface IStorageService
    {
        Task<string> UploadObjectAsync(string container, string file, Stream stream, TypeFile typeFile);
        Task<bool> DeleteObjectAsync(string url);
    }
}
