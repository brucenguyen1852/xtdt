﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Common.File
{
    /// <summary>
    /// Role
    /// </summary>
    public class RoleStorage
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    /// <summary>
    /// Domain storate
    /// </summary>
    public class DomainStorage
    {
        /// <summary>
        /// Id domain
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    /// <summary>
    /// Project
    /// </summary>
    public class ProjectStorage
    {
        /// <summary>
        /// Domain
        /// </summary>
        [JsonProperty("domain")]
        public DomainStorage Domain { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    /// <summary>
    /// End point storage
    /// </summary>
    public class EndpointStorage
    {
        /// <summary>
        /// Region id
        /// </summary>
        [JsonProperty("region_id")]
        public string RegionId { get; set; }

        /// <summary>
        /// Url
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }

        /// <summary>
        /// Region
        /// </summary>
        [JsonProperty("region")]
        public string Region { get; set; }

        /// <summary>
        /// Inteface
        /// </summary>
        [JsonProperty("interface")]
        public string Interface { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    /// <summary>
    /// Catalog
    /// </summary>
    public class CatalogStorage
    {
        /// <summary>
        /// End point
        /// </summary>
        [JsonProperty("endpoints")]
        public IList<EndpointStorage> Endpoints { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    /// <summary>
    /// User storage
    /// </summary>
    public class UserStorage
    {
        /// <summary>
        /// Password expirate at
        /// </summary>
        [JsonProperty("password_expires_at")]
        public object PasswordExpiresAt { get; set; }

        /// <summary>
        /// Domain
        /// </summary>
        [JsonProperty("domain")]
        public DomainStorage Domain { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    /// <summary>
    /// Token storage
    /// </summary>
    public class TokenStorage
    {
        /// <summary>
        /// Is domain
        /// </summary>
        [JsonProperty("is_domain")]
        public bool IsDomain { get; set; }

        /// <summary>
        /// Method
        /// </summary>
        [JsonProperty("methods")]
        public IList<string> Methods { get; set; }

        /// <summary>
        /// Roles
        /// </summary>
        [JsonProperty("roles")]
        public IList<RoleStorage> Roles { get; set; }

        /// <summary>
        /// Expirate at
        /// </summary>
        [JsonProperty("expires_at")]
        public DateTime ExpiresAt { get; set; }

        /// <summary>
        /// Project
        /// </summary>
        [JsonProperty("project")]
        public ProjectStorage Project { get; set; }

        /// <summary>
        /// Catalog
        /// </summary>
        [JsonProperty("catalog")]
        public IList<CatalogStorage> Catalog { get; set; }

        /// <summary>
        /// User
        /// </summary>
        [JsonProperty("user")]
        public UserStorage User { get; set; }

        /// <summary>
        /// Audit ids
        /// </summary>
        [JsonProperty("audit_ids")]
        public IList<string> AuditIds { get; set; }

        /// <summary>
        /// Issued at
        /// </summary>
        [JsonProperty("issued_at")]
        public DateTime IssuedAt { get; set; }
    }

    /// <summary>
    /// Storage token
    /// </summary>
    public class StorageToken
    {
        /// <summary>
        /// Token
        /// </summary>
        [JsonProperty("token_authentication")]
        public string Token { get; set; }

        /// <summary>
        /// Url public
        /// </summary>
        [JsonProperty("url_public")]
        public string UrlPublic { get; set; }

        /// <summary>
        /// Token
        /// </summary>
        [JsonProperty("token")]
        public TokenStorage Storage { get; set; }
    }
}
