﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.File
{
    public class StorageService //: IStorageService
    {
        /// <summary>
        /// Url authen
        /// </summary>
        private readonly string urlAuthen;

        /// <summary>
        /// Username
        /// </summary>
        private readonly string username;

        /// <summary>
        /// Password
        /// </summary>
        private readonly string password;

        /// <summary>
        /// Project id
        /// </summary>
        private readonly string projectId;

        /// <summary>
        /// Storage token
        /// </summary>
        private StorageToken storageToken;

        /// <summary>
        /// appSettings
        /// </summary>
        //private AppSetting appSettings;
        private IConfiguration _configuration;

        /// <summary>
        /// IHttpClientFactory
        /// </summary>
        private IHttpClientFactory httpClientFactory;

        /// <summary>
        /// urlPublic
        /// </summary>
        private string urlPublic { get; set; }

        public const string StorageHttpClientKey = "StorageHttpClient";
        public const string AcceptStream = "application/octet-stream";
        public const string ApplicationJsonAccept = "application/json";
        public const string XAuthenTokenKey = "x-auth-token";
        public static string CacheControl = "Cache-Control";

        
        /// <summary>
        /// Storate service
        /// </summary>
        /// <param name="appSettings"></param>
        public StorageService(IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            this._configuration = configuration;
            this.httpClientFactory = httpClientFactory;
            urlAuthen = configuration.GetSection("StorageInfo:UrlAuthen").Value;//configuration.GetValue<string>("StorageInfo:UrlAuthen");
            username = configuration.GetSection("StorageInfo:UserName").Value;
            password = configuration.GetSection("StorageInfo:Password").Value;
            projectId = configuration.GetSection("StorageInfo:ProjectId").Value;
            urlPublic = configuration.GetSection("StorageInfo:UrlPublic").Value;
        }

        /// <summary>
        /// Upload object
        /// </summary>
        /// <param name="container">Path to storage</param>
        /// <param name="file"></param>
        /// <param name="stream"></param>
        /// <returns>image url</returns>
        public async Task<string> UploadObjectAsync(string container, string file, Stream stream, TypeFile typeFile)
        {
            var issueToken = await Authenticate();
            var streamContent = new StreamContent(stream);

            var httpClient = httpClientFactory.CreateClient(StorageHttpClientKey);
            SetToken(httpClient, issueToken.Token);
            SetAcception(httpClient, AcceptStream);
            SetCacheHeader(httpClient, typeFile);
            var url = $"{issueToken.UrlPublic}/{container}/{file}";
            var req = httpClient.PutAsync(url, streamContent);
            var response = await req;
            return response.IsSuccessStatusCode ? url : string.Empty;
            //return string.Empty;
        }
        /// <summary>
        /// Delete object
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<bool> DeleteObjectAsync(string url)
        {
            try
            {
                var issueToken = await Authenticate();
                var httpClient = httpClientFactory.CreateClient(StorageHttpClientKey);
                SetToken(httpClient, issueToken.Token);
                SetAcception(httpClient, AcceptStream);
                string urlnew = WebUtility.UrlDecode(url);
                Uri uri = new Uri(urlnew);
                //Uri uri = new Uri(url);

                var req = httpClient.DeleteAsync(uri);
                var response = await req;
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region ---- authen and send 

        private async Task<StorageToken> Authenticate()
        {
            var date = Utils.NowLocal.AddMinutes(5);
            if (storageToken == null || storageToken.Storage == null || date > storageToken.Storage.ExpiresAt)
            {
                string data = CreateDataBody();
                var body = new StringContent(data, Encoding.UTF8, ApplicationJsonAccept);

                var httpClient = httpClientFactory.CreateClient(StorageHttpClientKey);
                SetAcception(httpClient, ApplicationJsonAccept);

                var req = httpClient.PostAsync(urlAuthen, body);
                var msg = await req;
                var response = await msg.Content.ReadAsStringAsync();
                storageToken = JsonConvert.DeserializeObject<StorageToken>(response);
                storageToken.Token = msg.Headers.GetValues("x-subject-token").First();
                EndpointStorage endPoint = null;
                foreach (var item in storageToken.Storage.Catalog)
                {
                    if ("object-store".Equals(item.Type))
                    {
                        endPoint = item.Endpoints.FirstOrDefault(p => "public".Equals(p.Interface));
                        if (endPoint != null)
                        {
                            break;
                        }
                    }
                }

                // Set utc time to local time
                storageToken.Storage.ExpiresAt = storageToken.Storage.ExpiresAt.ToLocalTime();
                storageToken.UrlPublic = endPoint?.Url;
            }
            return storageToken;
        }

        /// <summary>
        /// Create data body for authen
        /// </summary>
        /// <returns></returns>
        private string CreateDataBody()
        {

            return "{\"auth\": {\n    \"identity\": {\n       \"methods\": [\"password\"],\n          \"password\": {\n             \"user\": {\n                " +
                   "\"domain\": {\"name\": \"default\"},\n                   \"name\": \"" + username + "\",\n                   \"password\": \"" + password + "\"\n             }" +
                   "\n          }\n       },\n       \"scope\": {\n          \"project\": {\n             \"domain\": {\"name\": \"default\"},\n                " +
                   "\"id\": \"" + projectId + "\"\n          }\n       }\n   }\n}";
        }
        /// <summary>
        /// Set Accept for request
        /// </summary>
        /// <param name="mediaTypeWith"></param>
        private void SetAcception(HttpClient httpClient, string mediaTypeWith)
        {
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaTypeWith));
        }
        /// <summary>
        /// Set token for request
        /// </summary>
        /// <param name="tokenStorage"></param>
        private void SetToken(HttpClient httpClient, string tokenStorage)
        {
            if (httpClient.DefaultRequestHeaders != null && httpClient.DefaultRequestHeaders.Any(x => x.Key == XAuthenTokenKey))
            {
                httpClient.DefaultRequestHeaders.Remove(XAuthenTokenKey);
            }

            httpClient.DefaultRequestHeaders.Add(XAuthenTokenKey, tokenStorage);

        }

        /// <summary>
        /// Set cache header for file
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="typeFilem"></param>
        private void SetCacheHeader(HttpClient httpClient, TypeFile typeFile)
        {
            if (httpClient.DefaultRequestHeaders.Contains(CacheControl))
            {
                httpClient.DefaultRequestHeaders.Remove(CacheControl);
            }
            switch (typeFile)
            {
                //case TypeFile.Obj:
                //    httpClient.DefaultRequestHeaders.Add(CommonConstants.CacheControl, "max-age=" + appSettings.ImageCacheTime.Obj);
                //    break;
                //case TypeFile.Icon:
                //    httpClient.DefaultRequestHeaders.Add(CommonConstants.CacheControl, "max-age=" + appSettings.ImageCacheTime.Icon);
                //    break;
                //case TypeFile.Thumbnail:
                //    httpClient.DefaultRequestHeaders.Add(CommonConstants.CacheControl, "max-age=" + appSettings.ImageCacheTime.Thumbnail);
                //    break;
                //case TypeFile.Texture:
                //    httpClient.DefaultRequestHeaders.Add(CommonConstants.CacheControl, "max-age=" + appSettings.ImageCacheTime.Texture);
                //    break;
                case TypeFile.Image:
                    break;
                case TypeFile.Temp:
                    break;
                case TypeFile.ViMapTemp:
                    break;
                case TypeFile.ViMapImage:
                    break;
                case TypeFile.ViMapTexture:
                    break;
                case TypeFile.ViMapObject:
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
