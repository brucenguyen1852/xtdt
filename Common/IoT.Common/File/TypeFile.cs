﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Common.File
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TypeFile
    {
        /// <summary>
        /// Type object file
        /// </summary>
        Obj = 0,

        /// <summary>
        /// Image icon
        /// </summary>
        Icon = 1,

        /// <summary>
        /// Thumbnail
        /// </summary>
        Thumbnail = 2,

        /// <summary>
        /// Texture
        /// </summary>
        Texture = 3,

        /// <summary>
        /// Image
        /// </summary>
        Image = 4,

        /// <summary>
        /// Temp
        /// </summary>
        Temp = 5,

        /// <summary>
        /// ViMapTemp
        /// </summary>
        ViMapTemp = 6,

        /// <summary>
        /// ViMapImage
        /// </summary>
        ViMapImage = 7,

        /// <summary>
        /// ViMapTexture
        /// </summary>
        ViMapTexture = 8,

        /// <summary>
        /// ViMapObject
        /// </summary>
        ViMapObject = 9,

        /// <summary>
        /// HashTag
        /// </summary>
        HashTag = 10,
        /// <summary>
        /// POI
        /// </summary>
        POI = 11,

        /// <summary>
        /// Place comment image
        /// </summary>
        PlaceComment = 12,

        /// <summary>
        /// 
        /// </summary>
        ReportError = 13,

        Video = 14,
        /// <summary>
        /// Documnet
        /// </summary>
        Document = 15,
    }
}
