﻿using IoT.Common.File;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.FileApp
{
    public class FileAppService
    {
        private IConfiguration configuration;
        //private IHttpClientFactory httpClientFactory;
        public FileAppService(IConfiguration _configuration)
        {
            configuration = _configuration;
        }
        public async Task<bool> DeleteFileAsync(string url, TypeFile typeFile)
        {
            try
            {
                string urlpath = await CheckExitFolder(typeFile);
                string[] fileName = url.Split('/');
                url = string.Format("{0}/{1}", urlpath, fileName[fileName.Length - 1]);
                if (System.IO.File.Exists(url))
                {
                    System.IO.File.Delete(url);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<FileView> UploadFileAsync(IFormFile file, TypeFile typeFile, string hostmain)
        {
            try
            {
                FileView result = new FileView();
                if (file.Length > 0)
                {
                    string objFile = file.FileName;
                    string path = await CheckExitFolder(typeFile);
                    string extension = Path.GetExtension(file.FileName);
                    result.Id = Guid.NewGuid().ToString();
                    string fileName = string.Format("{0}{1}", result.Id, extension);
                    string filePath = string.Format("{0}/{1}", path, fileName);
                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                    result.Url = await GetUrlFile(hostmain, filePath);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region---private---
        private async Task<string> GetUrlFile(string hostmain, string filePath)
        {
            try
            {
                string urlUpload = configuration.GetSection("AppInfo:Folder").Value;
                string split = urlUpload.Split('/')[0];
                string pathfile = filePath.Replace(split, hostmain);
                return pathfile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// check folder exit
        /// </summary>
        /// <param name="typeFile"></param>
        /// <returns></returns>
        private async Task<string> CheckExitFolder(TypeFile typeFile)
        {
            string urlUpload = configuration.GetSection("AppInfo:Folder").Value;
            switch (typeFile)
            {
                case TypeFile.Obj:
                    urlUpload = string.Format("{0}/{1}", urlUpload, configuration.GetSection("AppInfo:Object").Value);
                    break;
                case TypeFile.Texture:
                    urlUpload = string.Format("{0}/{1}", urlUpload, configuration.GetSection("AppInfo:Texture").Value);
                    break;
                case TypeFile.Image:
                    urlUpload = string.Format("{0}/{1}", urlUpload, configuration.GetSection("AppInfo:Image").Value);
                    break;
                case TypeFile.Video:
                    urlUpload = string.Format("{0}/{1}", urlUpload, configuration.GetSection("AppInfo:Video").Value);
                    break;
                case TypeFile.Document:
                    urlUpload = string.Format("{0}/{1}", urlUpload, configuration.GetSection("AppInfo:Document").Value);
                    break;
                default:
                    urlUpload = string.Format("{0}/{1}", urlUpload, configuration.GetSection("AppInfo:Temp").Value);
                    break;
            }
            if (!Directory.Exists(urlUpload))
            {
                Directory.CreateDirectory(urlUpload);
            }
            return urlUpload;
        }
        #endregion
    }
}
