﻿using IoT.Common.File;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.FileApp
{
    public interface IFileAppService
    {
        Task<FileView> UploadFileAsync(IFormFile file, TypeFile typeFile);
    }
}
