﻿using Amazon.S3;
using Amazon.S3.Model;
using IoT.Common.File;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.FileNetApp
{
    public class FileNetAppService : IFileNetAppService
    {
        private IConfiguration configuration;
        private AmazonS3Config config;
        private string accessKey,secretKey;
        private AmazonS3Client s3Client;
        public FileNetAppService(IConfiguration _configuration)
        {
            configuration = _configuration;
            config = new AmazonS3Config();
            config.ServiceURL = configuration.GetSection("StorageNetApp:urlStorage").Value;
            config.ForcePathStyle = true;
            accessKey = configuration.GetSection("StorageNetApp:accessKey").Value;
            secretKey = configuration.GetSection("StorageNetApp:secretKey").Value;
            s3Client = new AmazonS3Client(accessKey, secretKey, config);
        }

        public async Task<bool> DeleteFileAsync(string url, TypeFile typeFile)
        {
            try
            {
                var result = await DeleteFile(url, typeFile);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<FileView> UploadFileAsync(IFormFile file, TypeFile typeFile)
        {
            FileView result;
            string objFile = file.FileName;
            result = await UploadFile(objFile, typeFile, file.OpenReadStream());
            result.Id = Guid.NewGuid().ToString();
            return result;
        }

        #region------Upload delete storge NetAPP-----

        private async Task<FileView> UploadFile(string nameFile, TypeFile typeFile, Stream stream)
        {
            var extension = System.IO.Path.GetExtension(nameFile);
            var name = Guid.NewGuid().ToString(); //Utils.GenId;
            if (string.IsNullOrWhiteSpace(extension))
            {
                name += ".txt";
            }
            else
            {
                name += extension;
            }
            string urlUpload = string.Empty;
            FileView result = null;
            switch (typeFile)
            {
                case TypeFile.Obj:
                    urlUpload = await UploadObjectAsync(configuration.GetSection("StorageNetApp:Object").Value, name, stream, TypeFile.Obj);
                    break;
                case TypeFile.Texture:
                    urlUpload = await UploadObjectAsync(configuration.GetSection("StorageNetApp:Texture").Value, name, stream, TypeFile.Texture);
                    break;
                case TypeFile.Image:
                    urlUpload = await UploadObjectAsync(configuration.GetSection("StorageNetApp:Image").Value, name, stream, TypeFile.Image);
                    break;
                case TypeFile.Video:
                    urlUpload = await UploadObjectAsync(configuration.GetSection("StorageNetApp:Video").Value, name, stream, TypeFile.Video);
                    break;
                case TypeFile.Document:
                    urlUpload = await UploadObjectAsync(configuration.GetSection("StorageNetApp:Document").Value, name, stream, TypeFile.Document);
                    break;
                default:
                    urlUpload = await UploadObjectAsync(configuration.GetSection("StorageNetApp:Temp").Value, name, stream, TypeFile.Temp);
                    break;
            }
            if (!string.IsNullOrEmpty(urlUpload))
            {
                result = new FileView()
                {
                    Name = name,
                    Url = urlUpload,
                };
            }
            return result;
        }

        private async Task<string> UploadObjectAsync(string container, string file, Stream stream, TypeFile typeFile)
        {
            try
            {
                PutObjectRequest putObject = new PutObjectRequest
                {
                    BucketName = container,
                    Key= file,
                    InputStream = stream
                };
                PutObjectResponse putObjectResponse = await s3Client.PutObjectAsync(putObject);
                var url = $"{configuration.GetSection("StorageNetApp:urlStorage").Value}/{container}/{file}";
                return url;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        private async Task<bool> DeleteFile(string url, TypeFile typeFile)
        {
            try
            {
                string[] fileName = url.Split('/');
                string file = fileName[fileName.Length - 1];
                DeleteObjectRequest deleteObject = new DeleteObjectRequest {
                    BucketName = GetBucketName(typeFile),
                    Key = file
                };
                DeleteObjectResponse response = await s3Client.DeleteObjectAsync(deleteObject);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private string GetBucketName(TypeFile typeFile)
        {
            string bucketName = "";
            switch (typeFile)
            {
                case TypeFile.Obj:
                    bucketName = configuration.GetSection("StorageNetApp:Object").Value;
                    break;
                case TypeFile.Texture:
                    bucketName = configuration.GetSection("StorageNetApp:Texture").Value;
                    break;
                case TypeFile.Image:
                    bucketName = configuration.GetSection("StorageNetApp:Image").Value;
                    break;
                case TypeFile.Video:
                    bucketName = configuration.GetSection("StorageNetApp:Video").Value;
                    break;
                case TypeFile.Document:
                    bucketName = configuration.GetSection("StorageNetApp:Document").Value;
                    break;
                default:
                    bucketName = configuration.GetSection("StorageNetApp:Temp").Value;
                    break;
            }
            return bucketName;
        }

        #endregion------Upload delete storge NetAPP--
    }
}
