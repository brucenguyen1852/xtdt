﻿using IoT.Common.File;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Common.FileNetApp
{
    public interface IFileNetAppService
    {
        Task<FileView> UploadFileAsync(IFormFile file, TypeFile typeFile);
        Task<bool> DeleteFileAsync(string url, TypeFile typeFile);
    }
}
