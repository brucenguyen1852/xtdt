﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Common
{
    public class FilterProperties
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("search")]
        public string Search { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
