﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Common
{
    public static class LocationCenter
    {
        public static double Lat { get; set; }
        public static double Lng { get; set; }
    }
}
