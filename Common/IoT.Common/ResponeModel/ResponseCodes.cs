﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Common
{
    public enum ResponseCodes
    {
        Ok = 200,
        ErrorException = 417,
        NotImplemented = 501,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotAllowed = 405,
        ErrorData = 204,
    }
}
