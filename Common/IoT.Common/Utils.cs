﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace IoT.Common
{
    public static class Utils
    {
        public static DateTime NowLocal => DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
        public readonly static Dictionary<char, char> UTF8ToAscii = new Dictionary<char, char>() {
            {'à','a' },
            {'á','a' },
            {'ạ','a' },
            {'ả','a' },
            {'ã','a' },
            {'â','a' },
            {'ầ','a' },
            {'ấ','a' },
            {'ậ','a' },
            {'ẩ','a' },
            {'ẫ','a' },
            {'ă','a' },
            {'ằ','a' },
            {'ắ','a' },
            {'ặ','a' },
            {'ẳ','a' },
            {'ẵ','a' },
            {'ì','i' },
            {'í','i' },
            {'ị','i' },
            {'ỉ','i' },
            {'ĩ','i' },
            {'ù','u' },
            {'ú','u' },
            {'ụ','u' },
            {'ủ','u' },
            {'ũ','u' },
            {'ư','u' },
            {'ừ','u' },
            {'ứ','u' },
            {'ự','u' },
            {'ử','u' },
            {'ữ','u' },
            {'đ','d' },
            {'è','e' },
            {'é','e' },
            {'ẹ','e' },
            {'ẻ','e' },
            {'ẽ','e' },
            {'ê','e' },
            {'ề','e' },
            {'ế','e' },
            {'ệ','e' },
            {'ể','e' },
            {'ễ','e' },
            {'ò','o' },
            {'ó','o' },
            {'ọ','o' },
            {'ỏ','o' },
            {'õ','o' },
            {'ô','o' },
            {'ồ','o' },
            {'ố','o' },
            {'ộ','o' },
            {'ổ','o' },
            {'ỗ','o' },
            {'ơ','o' },
            {'ờ','o' },
            {'ớ','o' },
            {'ợ','o' },
            {'ở','o' },
            {'ỡ','o' },
            {'ỳ','y' },
            {'ý','y' },
            {'ỵ','y' },
            {'ỷ','y' },
            {'ỹ','y' }
        };

        /// <summary>
        /// Convert UTF-8 of vietnamese language to ascii and lower
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static string ConvertTiengVietToAscii(string query)
        {
            if (query == null || query.Length == 0)
            {
                return "";
            }
            var str = query.ToLower().Trim();
            string result = "";
            foreach (var item in str.ToCharArray())
            {
                string temp = item + "";
                switch (item)
                {
                    case (char)768:
                    case (char)769:
                    case (char)771:
                    case (char)777:
                    case (char)803:
                        temp = "";
                        break;
                    case '/':
                    case '\\':
                    case ',':
                    case '"':
                    case '\'':
                    case '!':
                    case '@':
                    case '#':
                    case '$':
                    case '^':
                    case '&':
                    case '*':
                    case '(':
                    case ')':
                    case '=':
                    case '+':
                    case '~':
                    case '`':
                    case '<':
                    case '>':
                    case '?':
                    case ';':
                    case ':':
                    //case '_':
                    case '-':
                        temp = "";
                        break;
                    default:
                        if (UTF8ToAscii.ContainsKey(item))
                        {
                            temp = UTF8ToAscii[item] + "";
                        }
                        break;
                }
                result = result + temp;
                result = Regex.Replace(result, @"\s+", " ");
            }
            //if (log.IsInfoEnabled)
            //{
            //    log.Info("Conver query from:" + query + " To:" + result);
            //}
            return result;
        }
        public static string ConvertTiengVietToAsciiNewNotEmpty(string query)
        {
            if (query == null || query.Length == 0)
            {
                return "";
            }
            var str = query.ToLower().Trim();
            string result = "";
            foreach (var item in str.ToCharArray())
            {
                string temp = item.ToString().Trim() + "";
                switch (item)
                {
                    case (char)768:
                    case (char)769:
                    case (char)771:
                    case (char)777:
                    case (char)803:
                        temp = "";
                        break;
                    case '/':
                    case '\\':
                    case ',':
                    case '"':
                    case '\'':
                    case '!':
                    case '@':
                    case '#':
                    case '$':
                    case '^':
                    case '&':
                    case '*':
                    case '(':
                    case ')':
                    case '=':
                    case '+':
                    case '~':
                    case '`':
                    case '<':
                    case '>':
                    case '?':
                    case ';':
                    case ':':
                    //case '_':
                    case '-':
                        temp = "";
                        break;
                    default:
                        if (UTF8ToAscii.ContainsKey(item))
                        {
                            temp = UTF8ToAscii[item] + "";
                        }
                        break;
                }
                result = result + temp;
                result = Regex.Replace(result, @"\s+", " ");
            }
            //if (log.IsInfoEnabled)
            //{
            //    log.Info("Conver query from:" + query + " To:" + result);
            //}
            return result;
        }
    }
}
