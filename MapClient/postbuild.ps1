Remove-Item "../src/IoT.Web/wwwroot/mapclient/static" -Recurse -ErrorAction Ignore -Force 
Remove-Item "../src/IoT.Web/wwwroot/mapclient/assets" -Recurse -ErrorAction Ignore -Force 
Copy-Item "./build/*" -Destination "../src/IoT.Web/wwwroot/mapclient" -Recurse -force
Remove-Item "./build" -Recurse -Force 
Get-ChildItem "../src/IoT.Web/wwwroot/mapclient/static/css/*.css" | Rename-Item -NewName { 'main.css'}
Get-ChildItem "../src/IoT.Web/wwwroot/mapclient/static/css/*.map" | Rename-Item -NewName { 'main.css.map'}
Get-ChildItem "../src/IoT.Web/wwwroot/mapclient/static/js/*.js" | Rename-Item -NewName { 'main.js'}
Get-ChildItem "../src/IoT.Web/wwwroot/mapclient/static/js/*.map" | Rename-Item -NewName { 'main.js.map'}
$Contents = Get-Content "../src/IoT.Web/wwwroot/mapclient/static/js/main.js"
$Contents[0..($Contents.count-2)] + "//# sourceMappingURL=main.js.map"| Out-File "../src/IoT.Web/wwwroot/mapclient/static/js/main.js"
$Contents = Get-Content "../src/IoT.Web/wwwroot/mapclient/static/css/main.css"
$Contents[0..($Contents.count-2)] + "/*# sourceMappingURL=main.css.map*/"| Out-File "../src/IoT.Web/wwwroot/mapclient/static/css/main.css"