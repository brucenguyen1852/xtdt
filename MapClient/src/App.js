//import logo from './logo.svg';
//import './App.css';
import ThemeConfig from './theme';
import Mapclient from './components/map/mapclient';
import SidebarMenu from './components/siderbarMenu/sidebarmenu';
import Siderlayer from './components/siderlayer/siderlayer';
import DetailObject from './components/detailObject';
import DetailAntenna from './components/detailObject/detailAntenna';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useState } from 'react';
function App() {
  const [staticmenu, setStaticmenu] = useState({
    layer: false, // menu layer data (lớp dữ liệu)
    note: false,  // menu note (ghi chú)
    search: false, // menu search (tìm kiếm)
    distance: false, // menu range distance (đo khoảng cách)
    broadcast: false, // menu broad cast (phạm vi phát sóng)
    directions: false, // menu direction (chỉ đường)
    group: true  // check box group (nhóm trụ)
  });

  // event update static menu
  const handleChange = (obj) => {
    setStaticmenu({ ...staticmenu });
  };
  return (
    <ThemeConfig>

      <Router>
        <Routes>
          <Route exact path="/" element={
            <>
              <SidebarMenu static={staticmenu} handleChange={handleChange} />
              <Mapclient />
              {(staticmenu.layer || staticmenu.note || staticmenu.search || staticmenu.group) && <Siderlayer showmenu={staticmenu} />}
              {/* <DetailObject/> */}

            </>} />
            
          <Route exact path="/BTS/MapAnten/new" element={
            <>

              {/* <SidebarMenu static={staticmenu} handleChange={handleChange} />
              <Mapclient />
              {(staticmenu.layer || staticmenu.note || staticmenu.search || staticmenu.group) && <Siderlayer showmenu={staticmenu} />} */}

            </>
          } />
          {/* <Route path="/my" element={<><Mapclient /> <MyRedux /></>} /> */}
          
        </Routes>
      </Router>

    </ThemeConfig>

  );
}

export default App;
