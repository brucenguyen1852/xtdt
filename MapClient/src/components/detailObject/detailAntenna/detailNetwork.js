import React, { useEffect, useRef, useState } from 'react';
import Grid from '@mui/material/Grid';
import { Accordion, AccordionSummary, Typography, AccordionDetails } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import AxiosService from '../../../services/axiosService';
import UrlConfig from '../../../config/urlConfig';
export default function DetailNetwork(props) {
  const service = new AxiosService();
  const [listNetwork, setListNetwork] = useState(null);
  useEffect(() => {
    props && getListNetworkByAntenaId(props.idAntena);
  });
  const getListNetworkByAntenaId = (id) => {
    let list = listNetwork && listNetwork[Object.keys(listNetwork)[0]]
    let check = list?.find(x => x.idInforTru == id);
    if (!check) {
      let params = {
        id: id
      }
      service.get(UrlConfig.antenna.getListNetworkById, params, (res) => {
        var re = res;
        if (re != null) {
          //let group = _.groupBy(re.data,"idDoanhNghiep");
          console.log(re.data);
          const group = re.data.reduce((catsSoFar, currentItem) => {
            if (!catsSoFar[currentItem.idDoanhNghiep]) catsSoFar[currentItem.idDoanhNghiep] = [];
            catsSoFar[currentItem.idDoanhNghiep].push(currentItem);
            return catsSoFar;
          }, {});
          console.log(group);
          setListNetwork(group);
        }
      });
    }

  }
  const romanize = (num) => {
    if (isNaN(num))
      return NaN;
    var digits = String(+num).split(""),
      key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
        "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
        "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
      roman = "",
      i = 3;
    while (i--)
      roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    return Array(+digits.join("") + 1).join("M") + roman;
  }
  return (
    <div>
      {/* {listNetwork && listNetwork.map((objparent, i) => ( */}
      {
        listNetwork && Object.keys(listNetwork).map((id,j) => (
          <div  key={j}>
            {listNetwork[id].length > 0 && <div className='title-nework'>{romanize(1)}/{(listNetwork[id][0].tenDoanhNghiep != null ? listNetwork[id][0].tenDoanhNghiep : "Chưa xác định")}</div>}
            {listNetwork[id].map((obj, i) => (
              <Accordion key={i}>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls={"panel" + (i) + "a-content"} id={"panel" + (i) + "a-header"}>
                  <Typography>{obj.tenLoaiTram}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Grid container spacing={2}>
                    <Grid item md={6}> <div>Tên trạm:</div></Grid>
                    <Grid item md={6}><div>{(obj.tenTram != "" ? obj.tenTram : "")}</div></Grid>
                    <Grid item md={6}> <div>Mã trạm:</div></Grid>
                    <Grid item md={6}><div>{(obj.maTram != "" ? obj.maTram : "")}</div></Grid>
                    <Grid item md={6}> <div>Tên thiết bị:</div></Grid>
                    <Grid item md={6}><div>{(obj.tenThietBi != "" ? obj.tenThietBi : "")}</div></Grid>
                    <Grid item md={6}> <div>Nước sản xuất:</div></Grid>
                    <Grid item md={6}><div>{(obj.nuocSanXuat != "" ? obj.nuocSanXuat : "")}</div></Grid>
                    <Grid item md={6}> <div>Bắt buộc kiểm định:</div></Grid>
                    <Grid item md={6}><div>{(obj.batBuotKiemDinh == true ? "có" : "không")}</div></Grid>
                    <Grid item md={6}> <div>Ngày phát sóng:</div></Grid>
                    <Grid item md={6}><div>{(obj.ngayPhatSong != "" ? new Date(obj.ngayPhatSong).toLocaleDateString() : "")}</div></Grid>
                    <Grid item md={6}> <div>Công suất trạm:</div></Grid>
                    <Grid item md={6}><div>{(obj.congSuatTram != "" ? obj.congSuatTram : "")}</div></Grid>
                    <Grid item md={6}> <div>Hệ thống truyền dẫn:</div></Grid>
                    <Grid item md={6}><div>{(obj.heThongTruyenDan == true ? "Viba" : "Quang")}</div></Grid>
                  </Grid>
                </AccordionDetails>
              </Accordion>
            ))}
          </div>
        ))
      }

      {/* ))} */}
    </div>
  )
}