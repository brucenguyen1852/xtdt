import React, { useEffect, useRef } from 'react';
import Grid from '@mui/material/Grid';
import DetailNetwork from './detailNetwork';
export default function DetailAntenna(props) {

    return (
        <div>
            <div className="card text-secondary">  
                <div className="card-header with-border">
                    <h5 className="card-title">A/ Thông tin công trình</h5>
                </div>
                <div className="card-body">
                <Grid container spacing={2}>
                    <Grid item md={6}> <div>Tên chủ công trình:</div></Grid>
                    <Grid item md={6}><div>{(props.inforobject.chuSoHuu != "" ? props.inforobject.chuSoHuuDetail.ten : "")}</div></Grid>
                    <Grid item md={6}> <div>Tên trụ:</div></Grid>
                    <Grid item md={6}><div>{props.inforobject.tenTru}</div></Grid>
                    <Grid item md={6}> <div>Địa chỉ của trụ: </div></Grid>
                    <Grid item md={6}><div>{props.inforobject.diaChi}</div></Grid>
                    <Grid item md={6}> <div>Loại cột:</div></Grid>
                    <Grid item md={6}><div>{props.inforobject.tenTru}</div></Grid>
                    <Grid item md={6}> <div>Chiều cao:</div></Grid>
                    <Grid item md={6}><div>{props.inforobject.chieuCao}</div></Grid>
                    <Grid item md={6}> <div>Năm xây dựng:</div></Grid>
                    <Grid item md={6}><div>{props.inforobject.namXayDung}</div></Grid>
                    <Grid item md={6}> <div>Nhà mạng:</div></Grid>
                    <Grid item md={6}><div>{props.inforobject.namXayDung}</div></Grid>
                    <Grid item md={6}> <div>Trạng thái:</div></Grid>
                    <Grid item md={6}><div>{(props.inforobject.trangThai == "KeHoach" ? "Kế hoạch" : "Hiện hữu")}</div></Grid>
                    <Grid item md={6}> <div>Kiểu cột:</div></Grid>
                    <Grid item md={6}><div>{(!props.inforobject.kieuCot ? "Tự đứng" : "Dây co")}</div></Grid>
                    <Grid item md={6}> <div>Công tác kiểm định:</div></Grid>
                    <Grid item md={6}><div>{(props.inforobject.congTacKiemDinh == true ? "Có" : "Không")}</div></Grid>
                </Grid>
                </div>
            </div>
            <div className="card text-secondary">
                <div className="card-header">
                    <h5 className="card-title">B/ Thông tin về nhà mạng</h5>
                </div>
                <div className="card-body">
                    <div className="form-group row showLoaiTram">
                    <DetailNetwork idAntena={props.inforobject.id}/>
                    </div>
                </div>
            </div>
        </div>

    )
}