import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import DetailAntenna from './detailAntenna';
import DetailNhaTram from './detailNhatram';
import {Scrollbars } from 'react-custom-scrollbars-2';
import './detailobject.css';
export default function DetailObject(props) {
    const mapview = useRef(null);
    mapview.current = useSelector((state) => state.map.mapview);
    const [objectLayer, setObjectLayer] = useState({ id: "", type: "",obj:null });
    const [toggleDetail, setToggleDetail] = useState(false);
    //const [markerView,setMarkerView] = useState();
    const markerView = useRef(null);;
    //event map click marker
    useEffect(() => {
        if (mapview.current != null) {
            mapview.current.addListener("click", (args) => {
                let userdata = args.marker.getUserData();
                if (userdata) {
                    setToggleDetail(true);
                    clearMarkerClick();
                    showMarkerClick(args.marker);
                    setObjectLayer({ id: userdata.obj.id, type: userdata.type,obj:userdata.obj });
                }
            }, { marker: true });
        }
    }, [mapview.current]);
    //show marker iconview
    const showMarkerClick = (marker) => {
        var html = `<div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
                        <div class="gm-style-iw-a" style="position: absolute; top: 60px;">
                            <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
                                <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px; height:40px">
                                    <img src= "${marker.getIcon().url}" style="width: 25px;" />
                                </div>
                            </div>
                        </div>
                    </div>`;
        let markerNew = new map4d.Marker({
            position: { lat: marker.getPosition().lat, lng: marker.getPosition().lng },
            iconView: html,
            anchor: [0.5, 1.0]
        })
        markerNew.setMap(mapview.current);
        //setMarkerView(markerNew);
        markerView.current = markerNew;
    }
    //clear marker iconview
    const clearMarkerClick = () => {
        if(markerView?.current){
            markerView.current.setMap(null);
            markerView.current = null;
        }
    }
    //hide detail infor object
    const inforDetailObject = () =>{
        setToggleDetail(!toggleDetail);
        clearMarkerClick();
    }
    return (
        <div className={"map-side-right " + (toggleDetail ? "showdetail" : "")}>
            <div className="form-inner content-side">
                <div className="form-header bg-header bg-primary text-center">
                    <h4 className="title">
                        Thông tin chi tiết
                    </h4>
                    <button className="close-sidebar-right" data-original-title="" title="" onClick={()=>{inforDetailObject()}}>
                        <svg id="svg_dong" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="g_icon_dong" data-name="g_icon_dong">
                                <path id="Path_2689" data-name="Path 2689" d="M0,0H24V24H0Z" fill="none" />
                                <path id="Path_2690" data-name="Path 2690" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-2a8,8,0,1,0-8-8A8,8,0,0,0,12,20Zm0-9.414,2.828-2.829,1.415,1.415L13.414,12l2.829,2.828-1.415,1.415L12,13.414,9.172,16.243,7.757,14.828,10.586,12,7.757,9.172,9.172,7.757Z" fill="#ffffff" />
                            </g>
                        </svg>
                    </button>
                </div>
                <div className="form-body">
                    <div className="scroll-content">
                        <Scrollbars>
                            <div className="content-form">
                                {objectLayer.type == "antenna" && <DetailAntenna inforobject={objectLayer.obj}/>}
                                {objectLayer.type == "nhatram" && <DetailNhaTram />}
                            </div>
                        </Scrollbars>
                        
                    </div>
                </div>
            </div>
        </div>
    )
}