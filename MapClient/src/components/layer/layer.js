import React, { useEffect, useRef, useState, useLayoutEffect } from 'react';
import UrlConfig from "../../config/urlConfig";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from 'redux';
import { Box, FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import AxiosService from "../../services/axiosService";
import { chuSoHuuActionCreator } from "../../redux/index";
import LayerAntenna from './layerAntenna';
import LayerNhaTram from './layerNhaTram';
import './layer.css';
export default function Layer(props) {
    //const mapview = useSelector((state) => state.map.mapview);
    //const [clusterAntenna,setClusterAntenna]=useState({cluster:null,listmarker:null});
    const [checkLayer, setCheckLayer] = useState({ antenna: true, nhatram: false, captreo: false, capngam: false });
    const dispatch = useDispatch();
    const { getListChuSoHuu } = bindActionCreators(chuSoHuuActionCreator, dispatch);
    const service = new AxiosService();

    //set data antenna
    // const setDataLayerAntenna = (data) =>{
    //     setClusterAntenna({...clusterAntenna});
    // }
    //get list chu so huu
    const getListDataChuSoHuu = () => {
        service.getAsync(UrlConfig.chuSoHuu.getListChuSoHuu, {}, (res) => {
            var re = res;
            if (re?.data != null && re?.data?.items) {
                getListChuSoHuu(re.data.items);
            }
        });
    }
    const changeLayer = (type) => {
        //setCheckLayer(null);
        //let check = checkLayer;
        let nt = false
        switch (type) {
            case "antenna":
                nt = !checkLayer.antenna;
                setCheckLayer({...checkLayer,antenna:nt});
                break;
            case "nhatram":
                nt = !checkLayer.nhatram;
                setCheckLayer({...checkLayer,nhatram:nt});
                break;
            case "captreo":
                //check.captreo = !check.captreo;
                break;
            case "capngam":
                //check.capngam = !check.capngam;
                break;
            default:
                break;
        }
    }
     useEffect(() => {
        getListDataChuSoHuu();
    },[]);
    return (
        <Box className={!props.checkShow ? "hide":""}>
            <div className="tab-header layer-header">
                <span>Lớp dữ liệu</span>
            </div>
            <div className="custom-header-btmborder"></div>
            <FormGroup>
                <FormControlLabel control={<Checkbox defaultChecked onChange={()=>changeLayer("antenna")}/>} label="Trụ ăng ten" />
                <FormControlLabel control={<Checkbox onChange={()=>changeLayer("nhatram")} />} label="Nhà trạm" />
                <FormControlLabel control={<Checkbox />} label="Tuyến cáp ngầm" />
                <FormControlLabel control={<Checkbox />} label="Tuyến cáp treo" />
            </FormGroup>
            <LayerAntenna checkLayer={checkLayer.antenna} group ={props.group}/>
            {/* <LayerNhaTram checkLayer={checkLayer.nhatram}/> */}
        </Box>
    );
}
