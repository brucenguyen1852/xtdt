import React, { useEffect, useRef, useState,useLayoutEffect } from 'react';
import UrlConfig from "../../../config/urlConfig";
import { useDispatch,useSelector } from "react-redux";
import { bindActionCreators } from 'redux';
import AxiosService from "../../../services/axiosService";
//import DrawPoint from '../../common/drawObject';
export default function LayerAntenna(props){
    const [clusterAntenna,setClusterAntenna]=useState({cluster:null,listmarker:null});
    const mapview=useRef(null);
    mapview.current =useSelector((state) => state.map.mapview);
    //const mapview = useSelector((state) => state.map.mapview);
    const listChuSoHuu=useRef(null);
    listChuSoHuu.current = useSelector((state) => state.chusohuu.chuSoHuu);
    const service = new AxiosService();
    
    useEffect(() => {
        {props.checkLayer && !clusterAntenna.cluster && getListTruAngTen();};
    }, []);

    //get list tru antena
    const getListTruAngTen = () => {
        var params = {
            code: "001053",
            chuSoHuu: "",
            loaiTram: "",
            loaiCot: "",
            lat: 0,
            lng: 0,
            radius: 0
        }
        service.get(UrlConfig.antenna.searchAntenna, params, (res) => {
            var re = res;
            if (re?.data != null) {
                if(mapview.current!=null){
                    showMarkerAntenna(re.data);
                }
                
            }
        });
    }
    //draw marker antena
    const showMarkerAntenna =(data)=>{
        let listmarker =[];
        let Cluster =null;
        if(listChuSoHuu.current != null)
                    console.log(listChuSoHuu.current);
        if(data){ 
            data.map((item,i)=>{
                // let iconDefault = `${process.env.REACT_APP_IOT_DOMAIN}/images/tramanten.png`;
                let iconDefault = GetIconChuSoHuu(item);
                let icon = new map4d.Icon(17, 21, iconDefault);
                var marker = new map4d.Marker({
                    position: { lat: item.lat, lng: item.lng },
                    icon: icon,
                    anchor: [0.5, 1.0]
                });
                let userData = { type: "antenna", obj: item }; 
                marker.setUserData(userData);
                listmarker.push(marker);
                //marker.setMap(mapview.current);
            });
            var option = map4d.MarkerClusterOptions = {
                minZoom: 0,
                maxZoom: 16,
                radius: 300,
                zoomOnClick: true
            };
            Cluster = new map4d.MarkerClusterer(listmarker, option);
            Cluster.setMap(mapview.current);
            clusterAntenna.cluster=Cluster;
            clusterAntenna.listmarker=listmarker;
        }
    }
    //get icon chu so huu
    const GetIconChuSoHuu=(item)=>{
        let iconDefault = `${process.env.REACT_APP_IOT_DOMAIN}/images/tramanten.png`;
        if(item.chuSoHuuDetail){
            if(item.trangThai && item.trangThai.toLowerCase() == "kehoach" && item.chuSoHuuDetail.iConKeHoach){
                iconDefault = item.chuSoHuuDetail.iConKeHoach &&`${process.env.REACT_APP_IOT_DOMAIN}/images/ChuSoHuu/KeHoach/${item.chuSoHuuDetail.iConKeHoach}`;
            }else {
                iconDefault = item.chuSoHuuDetail.icon && `${process.env.REACT_APP_IOT_DOMAIN}/images/ChuSoHuu/${item.chuSoHuuDetail.icon}`;
            }
        }
        return iconDefault;
    }
    //clear cluster
    const ClearClusterMarker =()=>{
        if(mapview.current!=null && clusterAntenna.cluster != null){
            clusterAntenna.cluster.setMap(null);
        }
    }
    //show cluster
    const ShowClusterMarker =()=>{
        if(mapview.current!=null && clusterAntenna.cluster != null){
            clusterAntenna.cluster.setMap(mapview.current);
        }
    }
    //show marker
    const ShowMarker=()=>{
        if(mapview.current!=null){
            if(clusterAntenna.cluster !== null) ClearClusterMarker();
            if(clusterAntenna.listmarker !==  null && clusterAntenna.listmarker.length > 0 && clusterAntenna.listmarker[0].map == null){
                clusterAntenna.listmarker.map((item,i)=>{
                    item.setMap(mapview.current);
                });
            }
        }
    }
    //clear marker
    const ClearMarker=()=>{
        if(mapview.current!=null){
            if(clusterAntenna.listmarker&& clusterAntenna.listmarker){
                clusterAntenna.listmarker.map((item,i)=>{
                    item.setMap(null);
                });
            }
        }
    }
    //check show clear marker
    const checkReturn = ()=>{
        // uncheck layer antenna
        if(!props.checkLayer) { 
            ClearClusterMarker();
            ClearMarker();
        }
        //group cluster
        if(props.checkLayer && clusterAntenna.cluster != null && props.group){
            ShowClusterMarker();
            ClearMarker();
        }
        //clear group cluster
        if(!props.group){
            ClearClusterMarker();
            ShowMarker()
        }
    }
    return(<>
        {checkReturn()}
    </>);
}