import React, { useEffect, useRef, useState } from 'react';
import UrlConfig from "../../../config/urlConfig";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from 'redux';
import AxiosService from "../../../services/axiosService";
//import DrawPoint from '../../common/drawObject';
export default function LayerNhaTram(props) {
    const [listNhaTram, setListNhaTram] = useState(null);
    const mapview = useRef(null);
    mapview.current = useSelector((state) => state.map.mapview);
    //const mapview = useSelector((state) => state.map.mapview);
    const listChuSoHuu = useRef(null);
    listChuSoHuu.current = useSelector((state) => state.chusohuu.chuSoHuu);
    const service = new AxiosService();
    //get list tru antena
    const getListNhaTram = () => {
        var params = {
            countryCode: "001053",
            chuSoHuu: "",
            lat: 0,
            lng: 0,
            radius: 0,
            page: 1
        }
        service.get(UrlConfig.nhatram.searchNhaTram, params, (res) => {
            var re = res;
            if (re?.data != null) {
                if (mapview.current != null) {
                    console.log(mapview.current);
                    console.log(re.data);
                    setListNhaTram(re.data.result.items);
                }
                if (listChuSoHuu.current != null)
                    console.log(listChuSoHuu.current);
            }
        });
    }
    useEffect(() => {
        if (mapview !== null && props.check) {
            getListNhaTram();
        }
    }, []);
    return (
        <>
            {/* {props.check && listNhaTram && <DrawPoint typeobject="nhatram" listnhatram={listNhaTram} />}
            {!props.check && <DrawPoint typeobject="nhatram" listnhatram={null} />} */}
        </>
    );
}