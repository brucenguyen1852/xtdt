import { defaultConfig } from "../../config/defaultConfig"
import React, { useEffect,useRef } from 'react';
import UrlConfig from "../../config/urlConfig";
import { useDispatch,useSelector } from "react-redux";
import { bindActionCreators } from 'redux';
import {mapActionCreator,countryActionCreator} from "../../redux/index";
import AxiosService from "../../services/axiosService";
import './mapclient.css';
export default function Mapclient(props) {
    const mapview = useSelector((state) => state.map.mapview);
    const dispatch = useDispatch();
    const {setMapView} = bindActionCreators(mapActionCreator,dispatch);
    const {getListCount} = bindActionCreators(countryActionCreator,dispatch);
    const service = new AxiosService();

    const initmap = () => {
        let map  = new map4d.Map(document.getElementById("maproot"), defaultConfig.mapOption)
        setMapView(map);
    };
    //get countr load page
    const getCountry = async ()=>{
        var params={
            code: "",
            level: 1
        }
        await service.getAsync(UrlConfig.country.getListCountry,params,(res)=>{
            var re = res;
            if(re?.data != null){
                getListCount(re.data)
            }
        });
    }
    useEffect(() => {
        if(mapview == null){
            initmap();
            getCountry();
        }
    },[]);
    return (
        <div id="maproot"></div>
    );
}
