import React, { useEffect, useRef } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import './sidebarmenu.css';
export default function SidebarMenu(props) {
    const LightTooltip = styled(({ className, ...props }) => (
        <Tooltip {...props} arrow classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.arrow}`]: {
            color: theme.palette.common.white,
        },
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: theme.palette.common.white,
            color: '#626262',
            padding: 5,
            //boxShadow: theme.shadows[1],
            boxShadow: '0.19rem 0 0.375rem 0 #00000029',
            fontSize: 14,
        },
    }));
    //event group marker
    const handleChangeGroup=(event)=>{
        let obj = props.static;
        obj.group=event.target.checked;
        props.handleChange(obj);
    }
    //event show menu
    const handleChangeMenu=(key)=>{
        let obj = props.static;
        switch (key) {
            case 'layer':
                obj.layer=true;
                obj.note=false;
                obj.search=false;
                break;
            case 'note':
                obj.note=true;
                obj.layer=false;
                obj.search=false;
                break;
            case 'search':
                obj.note=false;
                obj.layer=false;
                obj.search=true;
                break;
        }
        props.handleChange(obj);
    }
    return (
        <AppBar className='sidebar-menu'>
            <Toolbar>
                <LightTooltip title="Lớp dữ liệu">
                    <Button color="inherit" onClick={()=>handleChangeMenu("layer")}>
                        <svg id="Group_14735" data-name="Group 14735" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path id="Path_8050" data-name="Path 8050" d="M0,0H24V24H0Z" fill="none" />
                            <path id="Path_8051" className="tab-icon" data-name="Path 8051" d="M20.083,15.2l1.2.721a.5.5,0,0,1,0,.858l-8.77,5.262a1,1,0,0,1-1.03,0l-8.77-5.262a.5.5,0,0,1,0-.858l1.2-.721L12,20.05l8.083-4.85Zm0-4.7,1.2.721a.5.5,0,0,1,0,.858L12,17.65,2.715,12.079a.5.5,0,0,1,0-.858l1.2-.721L12,15.35ZM12.514,1.309l8.771,5.262a.5.5,0,0,1,0,.858L12,13,2.715,7.429a.5.5,0,0,1,0-.858l8.77-5.262a1,1,0,0,1,1.03,0ZM12,3.332,5.887,7,12,10.668,18.113,7Z" fill="#707070" />
                        </svg>
                    </Button>
                </LightTooltip>
                <div className="right-line-horizel"></div>
                <LightTooltip title="Chú thích">
                    <Button color="inherit" onClick={()=>handleChangeMenu("note")}>
                        <svg id="Group_14734" data-name="Group 14734" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path id="Path_8048" data-name="Path 8048" d="M0,0H24V24H0Z" fill="none"></path>
                            <path id="Path_8049" className="tab-icon" data-name="Path 8049" d="M12.414,5H21a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414ZM4,5V19H20V7H11.586l-2-2Zm7,7h2v5H11Zm0-3h2v2H11Z" fill="#707070"></path>
                        </svg>
                    </Button>
                </LightTooltip>
                <div className="right-line-horizel"></div>
                <LightTooltip title="Tìm kiếm">
                    <Button color="inherit" onClick={()=>handleChangeMenu("search")}>
                        <svg id="Group_14732" data-name="Group 14732" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path id="Path_8046" data-name="Path 8046" d="M0,0H24V24H0Z" fill="none"></path>
                            <path id="Path_8047" className="tab-icon" data-name="Path 8047" d="M18.031,16.617,22.314,20.9,20.9,22.314l-4.282-4.283a9,9,0,1,1,1.414-1.414Zm-2.006-.742a7,7,0,1,0-.15.15l.15-.15Z" fill="#707070"></path>
                        </svg>
                    </Button>
                </LightTooltip>
                <div className="right-line-horizel"></div>
                <LightTooltip title="Đo khoảng cách">
                    <Button color="inherit">
                        <svg id="do_khoang_cach" data-name="dokhoangcach" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <defs>
                                <clipPath id="clip-path">
                                    <rect id="Rectangle_3948" data-name="Rectangle 3948" width="24" height="24" transform="translate(651 16)" fill="#707070" stroke="#707070" strokeWidth="1" />
                                </clipPath>
                            </defs>
                            <g id="g_do_khoang_cach" data-name="g_do_khoang_cach" transform="translate(-651 -16)" clipPath="url(#clip-path)">
                                <g id="arrow-double-up-and-down-sign-svgrepo-com" transform="translate(653 64.14) rotate(-90)">
                                    <path id="Path_8039" data-name="Path 8039" d="M36.165,19.845l2.886-3.617a.412.412,0,0,0-.322-.669h-.954V4.441h.954a.412.412,0,0,0,.322-.669L36.165.155a.411.411,0,0,0-.644,0L32.636,3.772a.412.412,0,0,0,.322.669h.954V15.559h-.954a.412.412,0,0,0-.322.669l2.885,3.617a.412.412,0,0,0,.644,0Z" transform="translate(0 0)" fill="#707070" />
                                </g>
                            </g>
                        </svg>
                    </Button>
                </LightTooltip>
                <div className="right-line-horizel"></div>
                <LightTooltip title="Phạm vi phát sóng">
                    <Button color="inherit">
                        <svg id="svg_pham_vi_phat_song" data-name="svg_pham_vi_phat_song" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="icon">
                                <path id="Path_2812" data-name="Path 2812" d="M0,0H24V24H0Z" fill="none" />
                                <g id="podcast-svgrepo-com">
                                    <circle id="Ellipse_3" data-name="Ellipse 3" cx="2" cy="2" r="2" transform="translate(10 10)" fill="#707070" />
                                    <path id="Path_7884" data-name="Path 7884" d="M11,22h2l.5-7h-3Z" fill="#707070" />
                                    <path id="Path_7885" data-name="Path 7885" d="M12,2A9.993,9.993,0,0,0,9.549,21.683L9.4,19.556a8,8,0,1,1,5.206,0l-.152,2.127A9.994,9.994,0,0,0,12,2Z" fill="#707070" />
                                    <path id="Path_7886" data-name="Path 7886" d="M15.317,9.6a3.954,3.954,0,0,1,.6,1.43,4.049,4.049,0,0,1-.23,2.364,4.2,4.2,0,0,1-.369.68,4,4,0,0,1-.354.428l-.188,2.626a6.111,6.111,0,0,0,.58-.315,6,6,0,0,0-6.715-9.95,6,6,0,0,0,0,9.951,6.062,6.062,0,0,0,.578.313l-.188-2.625a4.1,4.1,0,0,1-.354-.426,3.954,3.954,0,0,1-.6-1.43,4.056,4.056,0,0,1,.231-2.364,3.979,3.979,0,0,1,.862-1.273,3.981,3.981,0,0,1,2.019-1.089,4.1,4.1,0,0,1,1.615,0,4.15,4.15,0,0,1,.748.231,4.042,4.042,0,0,1,1.761,1.449Z" fill="#707070" />
                                </g>
                            </g>
                        </svg>
                    </Button>
                </LightTooltip>
                <div className="right-line-horizel"></div>
                <LightTooltip title="Chỉ đường">
                    <Button color="inherit">
                        <svg id="svg_chi_duong" data-name="svg_chi_duong" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="icon">
                                <path id="Path_2825" data-name="Path 2825" d="M0,0H24V24H0Z" fill="none" />
                                <path id="Path_2826" data-name="Path 2826" d="M9,10a1,1,0,0,0-1,1v4h2V12h3v2.5L16.5,11,13,7.5V10Zm3.707-8.607,9.9,9.9a1,1,0,0,1,0,1.414l-9.9,9.9a1,1,0,0,1-1.414,0l-9.9-9.9a1,1,0,0,1,0-1.414l9.9-9.9a1,1,0,0,1,1.414,0Z" fill="#707070" />
                            </g>
                        </svg>

                    </Button>
                </LightTooltip>
                <div className="right-line-horizel"></div>
                <FormGroup className='menu_gomtru'>
                    {/* <FormControlLabel control={
                    <Switch  checked={true} />
                    } label="Gom trụ" /> */}
                    <FormControlLabel control={<Switch defaultChecked onChange={handleChangeGroup} />} label="Gom trụ" />
                </FormGroup>
                <div className="right-line-horizel"></div>
                <LightTooltip title="Đóng">
                    <Button color="inherit">
                        <svg id="svg_dong" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="g_icon_dong" data-name="g_icon_dong">
                                <path id="Path_2689" data-name="Path 2689" d="M0,0H24V24H0Z" fill="none" />
                                <path id="Path_2690" data-name="Path 2690" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-2a8,8,0,1,0-8-8A8,8,0,0,0,12,20Zm0-9.414,2.828-2.829,1.415,1.415L13.414,12l2.829,2.828-1.415,1.415L12,13.414,9.172,16.243,7.757,14.828,10.586,12,7.757,9.172,9.172,7.757Z" fill="#707070" />
                            </g>
                        </svg>
                    </Button>
                </LightTooltip>

            </Toolbar>
        </AppBar>
    );
}