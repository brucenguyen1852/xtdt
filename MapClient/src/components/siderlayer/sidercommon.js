import React, { useEffect, useRef, useState } from 'react';
import Button from '@mui/material/Button';
import './silderlayer.css'
export default function SiderCommon(props) {

    //event toggle sider
    const handleToggleCommon = () => {
        props.handleToggle();
    }

    return (
        <div className={"sideContent " + (props.toggle ? "showsider" : "")}>
            <div className='content-parent'>
                <div className="contentchild">
                    {props.children}
                </div>
                <Button className="map-side-toggle" onClick={handleToggleCommon}>
                    <svg className={"icon-show " + (props.toggle ? "" : "rotate-180")} alt="show" xmlns="http://www.w3.org/2000/svg" width="12.87" height="24" viewBox="0 0 12.87 24">
                        <path id="Path_2784" data-name="Path 2784" d="M14,7,9,12l5,5Z" transform="translate(-5.272)" fill="rgba(0,0,0,0.38)"></path>
                        <path id="Path_2785" data-name="Path 2785" d="M12.87,0V24H0V0Z" fill="none"></path>
                    </svg>
                </Button>
            </div>
        </div>
    )
}
