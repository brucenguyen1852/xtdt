import React, { useEffect, useRef,useState } from 'react';
import SiderCommon from './sidercommon';
import Layer from '../layer/layer';
import { Box, FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import DetailObject from '../detailObject'
import './silderlayer.css';
export default function Siderlayer(props) {
    const [toggle,setToggle] = useState(true);

    return (
        <SiderCommon toggle={toggle} handleToggle={()=>{setToggle(!toggle)}}>
            {/* {(props.showmenu.layer || props.showmenu.note) && <Layer group={props.showmenu.group}/>} */}
            <Layer group={props.showmenu.group} checkShow={props.showmenu.layer} checkDeteteData={props.showmenu.search}/>
            {props.showmenu.search && <Box/>}
            {props.showmenu.note && <Box/>}
            <DetailObject/>
        </SiderCommon>
    )

}
