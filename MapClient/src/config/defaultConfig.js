export const defaultConfig={
    mapOption:{
        center:{
            lat:10.075797466631855,
            lng:106.3331540590378
        },
        zoom:10,
        controls: true,
        geolocate: true,
        accessKey: 'b77745a2eb604d0989e2b5648d0019b2',
        restrictionBounds: [[105.81207275390625, 10.641713131230988], [106.89847156664331, 9.479901280242927]],
        shouldChangeMapMode: function () { return false; }
    },
    
}