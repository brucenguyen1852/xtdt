const UrlConfig={
    //url api country
    country:{
        getListCountry:`${process.env.REACT_APP_IOT_DOMAIN}/api/BTS/Country/getlist-country`,
        getGeojsonDictric:`${process.env.REACT_APP_IOT_DOMAIN}/api/BTS/CountryGeoJson/get-subDictric`,
    },
    //url api tru ang ten
    antenna:{
        searchAntenna:`${process.env.REACT_APP_IOT_DOMAIN}/api/BTS/InforTruAnten/search-tru-anten`,
        getListNetworkById:`${process.env.REACT_APP_IOT_DOMAIN}/api/BTS/InforTram/get-list-tram-by-truid`
    },
    //url api chu so huu
    chuSoHuu:{
        getListChuSoHuu:`${process.env.REACT_APP_IOT_DOMAIN}/api/BTS/ChuSoHuu/All-ChuSoHuu`
    },
    //url api loai tram
    loaiTram:{
        getListLoaiTram:`${process.env.REACT_APP_IOT_DOMAIN}/api/app/loai-tram/all`
    },
    //url api nha tram
    nhatram:{
        searchNhaTram:`${process.env.REACT_APP_IOT_DOMAIN}/api/management/quan-ly-trams/search-nha-tram`
    },
}
export default UrlConfig;