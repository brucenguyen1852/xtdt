import {ACTIONS_TYPE_MAP} from "../actionType/typeMap";

export const setMapView= (map)=>{
    return(dispatch)=>{
        dispatch({type:ACTIONS_TYPE_MAP.SET_MAP,
        payload:map
    });
    }
}