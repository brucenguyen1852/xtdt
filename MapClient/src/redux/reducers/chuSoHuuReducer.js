import {ACTIONS_TYPE_MAP} from "../actionType/typeMap";

const initialState = {
    chuSoHuu:null
}

const reducer = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case ACTIONS_TYPE_MAP.LIST_CHU_SO_HUU:
            return {
                ...state,
                chuSoHuu:action.payload
            }
        default:
            return state
    }
}

export default reducer;