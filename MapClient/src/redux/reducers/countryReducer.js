import {ACTIONS_TYPE_MAP} from "../actionType/typeMap";

const initialState = {
    country:null
}

const reducer = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case ACTIONS_TYPE_MAP.LIST_COUNTRY:
            return {
                ...state,
                country:action.payload
            }
        default:
            return state
    }
}

export default reducer;