import { combineReducers } from "redux";
import mapReducer from "./mapReducer";
import countryReducer from "./countryReducer";
import chuSoHuuReducer from "./chuSoHuuReducer"

const reducers = combineReducers({
    map:mapReducer,
    country:countryReducer,
    chusohuu:chuSoHuuReducer
})

export default reducers