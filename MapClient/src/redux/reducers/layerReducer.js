import {ACTIONS_TYPE_MAP} from "../actionType/typeMap";

const initialState = {
    antenna:null
}

const reducer = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case ACTIONS_TYPE_MAP.LIST_ANTENAN:
            return {
                ...state,
                antenna:action.payload
            }
        default:
            return state
    }
}

export default reducer;