import {ACTIONS_TYPE_MAP} from "../actionType/typeMap";

const initialState = {
    mapview:null
}

const reducer = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case ACTIONS_TYPE_MAP.SET_MAP:
            return {
                ...state,
                mapview:action.payload
            }
        default:
            return state
    }
}

export default reducer;