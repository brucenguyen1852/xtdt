import axios, { Axios } from "axios";
import { defaultConfig } from "../config/defaultConfig";

export default class AxiosService {
    constructor(namespace) {
        this.namespace = namespace;
        this.axios = axios.create({
            responseType: "json",
            //'Authorization': `Bearer ${defaultConfig.tokenDev}`
        });
        //if(process.env.NODE_ENV == )
        //console.log(process.env.REACT_APP_IOT_DOMAIN);
        //console.log({ REACT_APP_API_ENDPOINT: process.env.REACT_APP_API_ENDPOINT });
        this.axios.defaults.headers.common["Authorization"]=`Bearer ${process.env.REACT_APP_IOT_TOKEN}`;
    }
    //get api url, params or null, function callback
    get(url, params, callback) {
        this.axios.get(url,{params})
            .then(response => {
                callback(response);
            })
            .catch(error => {
                callback(error);
            })
    } 
    //post api url, params, function callback
    post(url,params,callback){
        this.axios.post(url,{params})
        then(response => {
            callback(response);
        })
        .catch(error =>{
            callback(error);
        })
    }
    //put api url,param, function callback
    put(url,params,callback){
        this.axios.put(url,{params})
        .then(response =>{
            callback(response);
        })
        .catch(error =>{
            callback(response);
        })
    }
    //delete api url,  function callback
    delete(url,callback){
        this.axios.delete(url)
        .then(response =>{
            callback(response);
        })
        .catch(error =>{
            callback(response);
        })
    }
    //get async api url, params or null, function callback
    async getAsync(url, params, callback) {
        await this.axios.get(url,{params})
            .then(response => {
                callback(response);
            })
            .catch(error => {
                callback(error);
            })
    } 
    //post async api url, params, function callback
    async postAsync(url,params,callback){
        await this.axios.post(url,{params})
        then(response => {
            callback(response);
        })
        .catch(error =>{
            callback(error);
        })
    }
    //put api url,param, function callback
    async putAsync(url,params,callback){
        await this.axios.put(url,{params})
        .then(response =>{
            callback(response);
        })
        .catch(error =>{
            callback(response);
        })
    }
    //delete api url,  function callback
    async deleteAsync(url,callback){
        await this.axios.delete(url)
        .then(response =>{
            callback(response);
        })
        .catch(error =>{
            callback(response);
        })
    }
}