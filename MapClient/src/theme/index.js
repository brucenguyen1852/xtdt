import { CssBaseline } from '@mui/material';
import { ThemeProvider, createTheme, StyledEngineProvider } from '@mui/material/styles';
import PropTypes from 'prop-types';
import { useMemo } from 'react';
import palette from './palette';

ThemeConfig.propTypes = {
    children: PropTypes.node
  };
  
  export default function ThemeConfig({ children }) {
    const themeOptions = useMemo(
      () => ({
        //palette,
        // shape,
        // typography,
        // shadows,
        // customShadows
      }),
      []
    );
  
    const theme = createTheme(themeOptions);
    //theme.components = componentsOverride(theme);
  
    return (
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {children}
        </ThemeProvider>
      </StyledEngineProvider>
    );
  }