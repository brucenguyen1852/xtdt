﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace IoT.Geoserver
{
    [DependsOn(
        typeof(GeoserverDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class GeoserverApplicationContractsModule : AbpModule
    {

    }
}
