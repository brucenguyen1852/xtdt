﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Geoserver.LayerGeoserver
{
    public class CreateLayerToLayerDto
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string LayerGeoserver { get; set; }//Table postgis and layer geoserver
        public int Status { get; set; }
        public string ColumnTable { get; set; } = "geom geometry,type varchar(50),loaiDat varchar(50)"; //các cột của table
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
