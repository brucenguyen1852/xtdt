﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Geoserver.LayerGeoserver
{
    public interface IGeoserverPostgresService
    {
        Task<bool> CreateTableWithLayer(string layerName);
    }
}
