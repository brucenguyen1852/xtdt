﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.Geoserver.LayerGeoserver
{
    public interface ILayerToLayerService : ICrudAppService< //Defines CRUD methods
            LayerToLayerGeoserverDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateLayerToLayerDto> //Used to create/update a book
    {
        //Task<LayerToLayerGeoserverDto> GetLayerToLayerAll();
        Task<object> CreateToLayerGeoserverAsync(CreateLayerToLayerDto param);
        Task<bool> AddObjectTableAsync(string id, string idLayer, string geometry, string type,string loaiDat);
        Task<bool> CreateStyleLayerGeoserverAsync(StyleLayerToLayerDto param);
        Task<bool> UpdateStyleLayerGeoserverAsync(StyleLayerToLayerDto param);
        Task<bool> UpdateObjectTableAsync(string id, string idLayer, string geometry, string type, string loaiDat);
        Task<bool> DeletebjectTableAsync(string id, string idLayer);
        Task<object> ChangeToLayerGeoserverAsync(CreateLayerToLayerDto param, List<MainObjectGeoserver> mainObjectGeoserver);
        Task<string> UpdateBboxLayerGeoserverAsync(string idLayer);
        Task<string> FindGetLink(string idLayer);
    }
}
