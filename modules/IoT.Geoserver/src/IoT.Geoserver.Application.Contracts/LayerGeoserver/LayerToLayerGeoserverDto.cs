﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.Geoserver.LayerGeoserver
{
    public class LayerToLayerGeoserverDto : AuditedEntityDto<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string LayerGeoserver { get; set; }//Table postgis and layer geoserver
        public int Status { get; set; }
        public string ColumnTable { get; set; } = "id SERIAL,geom geometry,type varchar(50)"; //các cột của table
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }

}
