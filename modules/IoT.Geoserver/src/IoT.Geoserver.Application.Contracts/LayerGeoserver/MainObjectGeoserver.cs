﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Geoserver.LayerGeoserver
{
    public class MainObjectGeoserver
    {
        public Guid Id { get; set; } //Id của thư mục
        public string IdDirectory { get; set; } //Id của thư mục
        public string NameObject { get; set; } // Tên đối tượng
        public Geometry Geometry { get; set; } // geojson 2D
    }
}
