﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Geoserver.LayerGeoserver
{
    public class ParamSearchWithProperties
    {
        public string Search { get; set; }
        public List<ParamGetInfor> Params { get; set; }
    }
}
