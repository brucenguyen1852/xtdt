﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Geoserver.LayerGeoserver
{
    public class StyleLayerToLayerDto
    {
        public string idLayer { get; set; }
        public string stroke { get; set; }
        public double strokeW { get; set; }
        public string fill { get; set; }
        public double fillO { get; set; }
    }
}
