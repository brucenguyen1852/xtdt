﻿using IoT.Geoserver.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace IoT.Geoserver.Permissions
{
    public class GeoserverPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(GeoserverPermissions.GroupName, L("Permission:Geoserver"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<GeoserverResource>(name);
        }
    }
}