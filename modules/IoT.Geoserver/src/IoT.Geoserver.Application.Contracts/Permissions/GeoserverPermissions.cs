﻿using Volo.Abp.Reflection;

namespace IoT.Geoserver.Permissions
{
    public class GeoserverPermissions
    {
        public const string GroupName = "Geoserver";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(GeoserverPermissions));
        }
    }
}