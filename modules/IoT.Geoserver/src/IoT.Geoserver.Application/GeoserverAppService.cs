﻿using IoT.Geoserver.Localization;
using Volo.Abp.Application.Services;

namespace IoT.Geoserver
{
    public abstract class GeoserverAppService : ApplicationService
    {
        protected GeoserverAppService()
        {
            LocalizationResource = typeof(GeoserverResource);
            ObjectMapperContext = typeof(GeoserverApplicationModule);
        }
    }
}
