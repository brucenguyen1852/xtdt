﻿using AutoMapper;
using IoT.Geoserver.LayerGeoserver;
using Volo.Abp.AutoMapper;

namespace IoT.Geoserver
{
    public class GeoserverApplicationAutoMapperProfile : Profile
    {
        public GeoserverApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
            CreateMap<LayerToLayerGeoserver, LayerToLayerGeoserverDto>();
            CreateMap<CreateLayerToLayerDto, LayerToLayerGeoserver>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.ExtraProperties, opt => opt.Ignore())
                .ForMember(x => x.LastModificationTime, opt => opt.Ignore())
                .ForMember(x => x.LastModifierId, opt => opt.Ignore())
                .ForMember(x => x.CreationTime, opt => opt.Ignore())
                .ForMember(x => x.CreatorId, opt => opt.Ignore())
                .ForMember(x => x.ConcurrencyStamp, opt => opt.Ignore());
        }
    }
}