﻿using IoT.Geoserver.PostgreSQL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace IoT.Geoserver.LayerGeoserver
{
    public class GeoserverLayerService
    {
        private PostgreSqlContext _context;
        public GeoserverLayerService(PostgreSqlContext context)
        {
            _context = context;
        }
        public async Task<bool> CreateTableWithLayer(string layerName, string columntable)
        {
            try
            {
                //string sql = @"DELETE FROM Customers WHERE CustomerId = @CustomerId";
                //SqlParameter pCustomerId = new SqlParameter("@CustomerId", customerId);
                string CreatetableSQL = string.Format("CREATE TABLE {0}(id varchar(50) not null PRIMARY key,{1})", layerName, columntable);
                var check = _context.ExecuteSqlCommand(CreatetableSQL);

                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> AddObjectTableWithLayer(string table, string wkt, string type,string id)
        {
            try
            {
                string query = @"INSERT INTO @table(id,geom, type) VALUES(@id,ST_GeomFromText(@geom),@type)";
                var check = _context.ExecuteSqlCommand(query,
                    new SqlParameter("table", table),
                    new SqlParameter("id", id),
                    new SqlParameter("geom", wkt),
                    new SqlParameter("type", type));
                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> UpdateObjectTableWithLayer(string table, string wkt, string type,string id)
        {
            try
            {
                string query = @"UPDATE @table SET geom=@geom,type=@type Where id=@id";
                var check = _context.ExecuteSqlCommand(query,
                    new SqlParameter("table", table),
                    new SqlParameter("geom", wkt),
                    new SqlParameter("type", type),
                    new SqlParameter("id", id));
                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> DeleteObjectTableWithLayer(string table, string id)
        {
            try
            {
                string query = @"DELETE FROM @table Where id=@id";
                var check = _context.ExecuteSqlCommand(query,
                    new SqlParameter("table", table),
                    new SqlParameter("id", id));
                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
