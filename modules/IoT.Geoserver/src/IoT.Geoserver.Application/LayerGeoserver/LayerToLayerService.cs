﻿using IoT.Common;
using IoT.Geoserver.PostgreSQL;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.Geoserver.LayerGeoserver
{
    public class LayerToLayerService : CrudAppService<
            LayerToLayerGeoserver, //The Book entity
            LayerToLayerGeoserverDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateLayerToLayerDto>, //Used to create/update a book
        ILayerToLayerService //implement the IBookAppService
    {
        private PostgreSqlContext _context;
        private IHttpClientFactory _httpClientFactory;
        private IConfiguration _configuration;
        private string Auth;
        private string GeoserverHttpClientKey = "Geoservertile";
        public LayerToLayerService(IRepository<LayerToLayerGeoserver, Guid> repository, PostgreSqlContext context,
            IHttpClientFactory httpClientFactory, IConfiguration configuration) : base(repository)
        {
            _context = context;
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;
            Auth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes(_configuration.GetSection("Geoserver:Authorization").Value)));

        }

        public async Task<object> CreateToLayerGeoserverAsync(CreateLayerToLayerDto param)
        {
            try
            {
                CreateLayerToLayerDto createLayerToLayerDto = new CreateLayerToLayerDto();
                /////change name lớp
                param.LayerGeoserver = Utils.ConvertTiengVietToAsciiNewNotEmpty(param.LayerGeoserver).ToLower();
                param.ColumnTable = !string.IsNullOrEmpty(param.ColumnTable) ? param.ColumnTable : createLayerToLayerDto.ColumnTable;
                ///lưu database của hệ thống
                LayerToLayerGeoserver layerToLayerGeoserver = new LayerToLayerGeoserver()
                {
                    IdDirectory = param.IdDirectory,
                    LayerGeoserver = param.LayerGeoserver,
                    Status = param.Status,
                    Tags = param.Tags,
                    ColumnTable = param.ColumnTable
                };
                var result = await Repository.InsertAsync(layerToLayerGeoserver); //await _layerToLayerService.CreateAsync(param);
                ///tạo table và publish layer
                if (result != null)
                {
                    var check = await CreateTableWithLayer(result.LayerGeoserver, result.ColumnTable);
                    if (check)
                    {
                        var checkPublish = await PublishLayerGeoserver(result.LayerGeoserver);
                        if (checkPublish)
                        {
                            //string layer = string.Format("{0}:{1}", _configuration.GetSection("Geoserver:Workspace").Value, result.LayerGeoserver);
                            //string subdomain = (_configuration.GetSection("Geoserver:UrlWMSGeoserver").Value).ToString().Replace("LAYERSValue", layer);
                            //string url = string.Format("{0}{1}", _configuration.GetSection("Geoserver:DomainUrl").Value, subdomain);
                            StyleLayerToLayerDto styleLayerToLayerDto = new StyleLayerToLayerDto() { fill = "#8b5b5b", fillO = 0.8, stroke = "#8b5b5b", strokeW = 1.0, idLayer = result.LayerGeoserver };
                            await CreateStyleLayer(styleLayerToLayerDto);
                            var obj = new
                            {
                                status = checkPublish,
                                result = GetUrlMaptile(result.LayerGeoserver)
                            };
                            return obj;
                        }
                    }
                    return false;
                }
                return new { status = false, result = "" };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> AddObjectTableAsync(string id, string idLayer, string geometry, string type, string loaiDat)
        {
            try
            {
                //Guid guid = Guid.Parse(idLayer);
                var layerGeoserver = await Repository.FirstAsync(x => x.IdDirectory == idLayer);
                var wkt = ConvertGeojsonToWkt.GeoJsonToWKT(geometry, null);
                var check = await AddObjectTableWithLayer(layerGeoserver.LayerGeoserver, wkt, type, loaiDat, id);
                return check;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> UpdateObjectTableAsync(string id, string idLayer, string geometry, string type, string loaiDat)
        {
            try
            {
                //Guid guid = Guid.Parse(idLayer);
                var layerGeoserver = await Repository.FirstAsync(x => x.IdDirectory == idLayer);
                var wkt = ConvertGeojsonToWkt.GeoJsonToWKT(geometry, null);
                var check = await UpdateObjectTableWithLayer(layerGeoserver.LayerGeoserver, wkt, type, id, loaiDat);
                return check;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> DeletebjectTableAsync(string id, string idLayer)
        {
            try
            {
                //Guid guid = Guid.Parse(idLayer);
                var layerGeoserver = await Repository.FirstAsync(x => x.IdDirectory == idLayer);
                var check = await DeleteObjectTableWithLayer(layerGeoserver.LayerGeoserver, id);
                return check;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> CreateStyleLayerGeoserverAsync(StyleLayerToLayerDto param)
        {
            try
            {
                //var layerGeoserver = await Repository.FirstAsync(x => x.IdDirectory == param.idLayer);
                var layerGeoserver = await Repository.FindAsync(x => x.IdDirectory == param.idLayer);
                if (layerGeoserver != null)
                {
                    param.idLayer = layerGeoserver.LayerGeoserver;
                }
                var check = await CreateStyleLayer(param);
                return check;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> UpdateStyleLayerGeoserverAsync(StyleLayerToLayerDto param)
        {
            try
            {
                var check = true;
                //var layerGeoserver = await Repository.FirstAsync(x => x.IdDirectory == param.idLayer);
                var layerGeoserver = await Repository.FindAsync(x => x.IdDirectory == param.idLayer);
                if (layerGeoserver != null)
                {
                    param.idLayer = layerGeoserver.LayerGeoserver;
                    check = await UpdateStyle(param);
                }
                else
                {
                    check = await CreateStyleLayer(param);
                }
                return check;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<object> ChangeToLayerGeoserverAsync(CreateLayerToLayerDto param, List<MainObjectGeoserver> mainObjectGeoserver)
        {
            try
            {
                //check layer geoserver exits
                var layerGeoserver = await Repository.FindAsync(x => x.IdDirectory == param.IdDirectory);
                if (layerGeoserver != null) //layer geoserver exits
                {
                    //sync data system to data geoserver
                    bool check = await SyncDataToGeoserver(layerGeoserver.LayerGeoserver, mainObjectGeoserver);
                    var obj = new
                    {
                        status = check,
                        result = GetUrlMaptile(layerGeoserver.LayerGeoserver)
                    };
                    return obj;
                }
                else
                {
                    //call function create
                    var objLayer = await CreateToLayerGeoserverAsync(param);
                    var propertyInfo = (bool)(objLayer.GetType().GetProperty("status")).GetValue(objLayer, null);
                    if (propertyInfo)
                    {
                        //sync data system to data geoserver
                        layerGeoserver = await Repository.FindAsync(x => x.IdDirectory == param.IdDirectory);
                        bool check = await SyncDataToGeoserver(layerGeoserver.LayerGeoserver, mainObjectGeoserver);
                        return objLayer;
                    }
                }
                return new { status = false, result = "" };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> UpdateBboxLayerGeoserverAsync(string idLayer)
        {
            try
            {
                LayerToLayerGeoserver layerGeoserver = await Repository.FirstAsync(x => x.IdDirectory == idLayer);
                bool check = await UpdateBBoxLayerGeoserver(layerGeoserver.LayerGeoserver);
                if (check)
                {
                    return await GetBboxFromLayerGeoserver(layerGeoserver.LayerGeoserver);
                }
                return "[]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> FindGetLink(string idLayer)
        {
            var layerGeoserver = await Repository.FindAsync(x => x.IdDirectory == idLayer);
            if (layerGeoserver != null)
            {
                return GetUrlMaptile(layerGeoserver.LayerGeoserver);
            }
            return string.Empty;
        }

        #region----------- Postgres -----------------------
        private async Task<bool> CreateTableWithLayer(string layerName, string columntable)
        {
            try
            {
                //string sql = @"DELETE FROM Customers WHERE CustomerId = @CustomerId";
                //SqlParameter pCustomerId = new SqlParameter("@CustomerId", customerId);
                string CreatetableSQL = string.Format("CREATE TABLE {0}(id varchar(50) not null PRIMARY key)", layerName);
                var check = _context.ExecuteSqlCommand(CreatetableSQL);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<bool> AddObjectTableWithLayer(string table, string wkt, string type, string loaiDat, string id)
        {
            try
            {
                string query = string.Format("INSERT INTO {0}(id,geom) VALUES(@id,ST_GeomFromText(@geom))", table);
                var check = _context.ExecuteSqlCommand(query,
                    new NpgsqlParameter("id", id),
                    new NpgsqlParameter("geom", wkt));
                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<bool> UpdateObjectTableWithLayer(string table, string wkt, string type, string id, string loaiDat)
        {
            try
            {
                string query = string.Format("UPDATE {0} SET geom=@geom Where id=@id", table);
                var check = _context.ExecuteSqlCommand(query,
                    new NpgsqlParameter("geom", wkt),
                    new NpgsqlParameter("type", type.ToLower()));
                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<bool> DeleteObjectTableWithLayer(string table, string id)
        {
            try
            {
                string query = string.Format("DELETE FROM {0} Where id=@id", table);
                var check = _context.ExecuteSqlCommand(query,
                    new NpgsqlParameter("id", id));
                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<bool> TruncateObjectTableWithLayer(string table)
        {
            try
            {
                string query = string.Format("TRUNCATE TABLE {0}", table);
                var check = _context.ExecuteSqlCommand(query);
                return check > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region-------------geoserver----------------
        private async Task<bool> PublishLayerGeoserver(string layerGeoserver)
        {
            try
            {
                string url = string.Format("{0}/geoserver/rest/workspaces/{1}/datastores/{2}/featuretypes", _configuration.GetSection("Geoserver:DomainUrl").Value, _configuration.GetSection("Geoserver:Workspace").Value, _configuration.GetSection("Geoserver:StoreName").Value);
                string body = string.Format("<featureType><name>{0}</name></featureType>", layerGeoserver);
                
                using (var client = new HttpClient())
                {
                    var content = new StringContent(body, Encoding.UTF8, "application/xml");
                    client.DefaultRequestHeaders.Add("Authorization", Auth);
                    var response = await client.PostAsync(url, content);
                    if (response.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<bool> CreateStyleLayer(StyleLayerToLayerDto param)
        {
            try
            {
                string url = string.Format("{0}/geoserver/rests/styles", _configuration.GetSection("Geoserver:DomainUrl").Value);
                string style = ChangeStyle(param);
                //string auth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes(_configuration.GetSection("Geoserver:Authorization").Value)));
                using (var client = new HttpClient())
                {
                    var content = new StringContent(style, Encoding.UTF8, "application/vnd.og.sld+xml");
                    client.DefaultRequestHeaders.Add("Authorization", Auth);
                    var response = await client.PostAsync(url, content);
                    if (response.IsSuccessStatusCode)
                    {
                        var check = await UpdateStyleforLayer(param.idLayer);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<bool> UpdateStyleforLayer(string layer)
        {
            string layerurl = string.Format("{0}:{1}", _configuration.GetSection("Geoserver:Workspace").Value, layer);
            string url = string.Format("{0}/geoserver/rest/layers/{1}", _configuration.GetSection("Geoserver:DomainUrl").Value, layerurl);
            string body = string.Format("<layer><defaultStyle><name>style_{0}</name></defaultStyle></layer>", layer);
            using (var client = new HttpClient())
            {
                var content = new StringContent(body, Encoding.UTF8, "text/xml");
                client.DefaultRequestHeaders.Add("Authorization", Auth);
                var response = await client.PutAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }
        private async Task<bool> UpdateStyle(StyleLayerToLayerDto param)
        {
            try
            {
                string value = ChangeUpdateStyle(param);
                string url = string.Format("{0}/geoserver/rest/styles/style_{1}", _configuration.GetSection("Geoserver:DomainUrl").Value, param.idLayer);
                using (var client = new HttpClient())
                {
                    var content = new StringContent(value, Encoding.UTF8, "application/vnd.og.sld+xml");
                    client.DefaultRequestHeaders.Add("Authorization", Auth);
                    var response = await client.PutAsync(url, content);
                    if (response.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private string ChangeStyle(StyleLayerToLayerDto param)
        {
            var xml = string.Format(@"<?xml version='1.0' encoding='UTF-8'?>
                    <StyledLayerDescriptor version='1.0.0' 
                     xsi:schemaLocation='http://www.opengis.net/sld StyledLayerDescriptor.xsd' 
                     xmlns='http://www.opengis.net/sld' 
                     xmlns:ogc='http://www.opengis.net/ogc' 
                     xmlns:xlink='http://www.w3.org/1999/xlink' 
                     xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                      <NamedLayer>
                        <Name>style_{4}</Name>
                        <UserStyle>
                          <Name>style_{4}</Name>
                          <Title>Default style</Title>
                          <Abstract>A sample style that draws a polygon</Abstract>
                          <FeatureTypeStyle>
                            <Rule>
                              <ogc:Filter>
                                <ogc:Or>
                                    <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>linestring</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                      <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>multilinestring</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                  </ogc:Or>
                              </ogc:Filter>
                              <LineSymbolizer>
                                <Stroke>
                                  <CssParameter name='stroke'>{0}</CssParameter>
                                  <CssParameter name='stroke-width'>{1}</CssParameter>
                                </Stroke>
                              </LineSymbolizer>
                            </Rule>
                            <Rule>
                            <ogc:Filter>
                                <ogc:Or>
                                    <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>multipolygon</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                    <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>polygon</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                  </ogc:Or>
                              </ogc:Filter>
                              <PolygonSymbolizer>
                                <Fill>
                                  <CssParameter name='fill'>{2}</CssParameter>
                                    <CssParameter name='fill-opacity'>{3}</CssParameter>
                                </Fill>
                                <Stroke>
                                  <CssParameter name='stroke'>{0}</CssParameter>
                                  <CssParameter name='stroke-width'>{1}</CssParameter>
                                </Stroke>
                              </PolygonSymbolizer>
                            </Rule>
                            <Rule>
                               <ogc:Filter>
                                <ogc:Or>
                                  <ogc:PropertyIsEqualTo>
                                  <ogc:PropertyName>type</ogc:PropertyName>
                                  <ogc:Literal>multipoint</ogc:Literal>
                                </ogc:PropertyIsEqualTo>
                                <ogc:PropertyIsEqualTo>
                                  <ogc:PropertyName>type</ogc:PropertyName>
                                  <ogc:Literal>point</ogc:Literal>
                                </ogc:PropertyIsEqualTo>
                               </ogc:Or>
                              </ogc:Filter>
                              <PointSymbolizer>
                                <Graphic>
                                  <Mark>
                                    <WellKnownName>circle</WellKnownName>
                                    <Fill>
                                      <CssParameter name='fill'>#FF0000</CssParameter>
                                      <CssParameter name='fill-opacity'>1.0</CssParameter>
                                    </Fill>
                                  </Mark>
                                  <Size>11</Size>
                                </Graphic>
                              </PointSymbolizer>
                              <PointSymbolizer>
                                <Graphic>
                                  <Mark>
                                    <WellKnownName>circle</WellKnownName>
                                    <Fill>
                                      <CssParameter name='fill'>#EDE513</CssParameter>
                                      <CssParameter name='fill-opacity'>1.0</CssParameter>
                                    </Fill>
                                  </Mark>
                                  <Size>7</Size>
                                </Graphic>
                              </PointSymbolizer>
                            </Rule>
                          </FeatureTypeStyle>
                        </UserStyle>
                      </NamedLayer>
                    </StyledLayerDescriptor>", param.stroke, param.strokeW.ToString().Replace(",", "."), param.fill, param.fillO.ToString().Replace(",", "."), param.idLayer);
            return xml;
        }
        private string ChangeUpdateStyle(StyleLayerToLayerDto param)
        {
            var xml = string.Format(@"<?xml version='1.0' encoding='UTF-8'?>
                    <StyledLayerDescriptor version='1.0.0' 
                     xsi:schemaLocation='http://www.opengis.net/sld StyledLayerDescriptor.xsd' 
                     xmlns='http://www.opengis.net/sld' 
                     xmlns:ogc='http://www.opengis.net/ogc' 
                     xmlns:xlink='http://www.w3.org/1999/xlink' 
                     xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                      <NamedLayer>
                        <UserStyle>
                          <FeatureTypeStyle>
                            <Rule>
                              <ogc:Filter>
                                <ogc:Or>
                                    <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>linestring</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                      <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>multilinestring</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                  </ogc:Or>
                              </ogc:Filter>
                              <LineSymbolizer>
                                <Stroke>
                                  <CssParameter name='stroke'>{0}</CssParameter>
                                  <CssParameter name='stroke-width'>{1}</CssParameter>
                                </Stroke>
                              </LineSymbolizer>
                            </Rule>
                            <Rule>
                           <ogc:Filter>
                                <ogc:Or>
                                    <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>multipolygon</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                    <ogc:PropertyIsEqualTo>
                                      <ogc:PropertyName>type</ogc:PropertyName>
                                      <ogc:Literal>polygon</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                  </ogc:Or>
                              </ogc:Filter>
                              <PolygonSymbolizer>
                                <Fill>
                                  <CssParameter name='fill'>{2}</CssParameter>
                                    <CssParameter name='fill-opacity'>{3}</CssParameter>
                                </Fill>
                                <Stroke>
                                  <CssParameter name='stroke'>{0}</CssParameter>
                                  <CssParameter name='stroke-width'>{1}</CssParameter>
                                </Stroke>
                              </PolygonSymbolizer>
                            </Rule>
                            <Rule>
                               <ogc:Filter>
                                <ogc:Or>
                                  <ogc:PropertyIsEqualTo>
                                  <ogc:PropertyName>type</ogc:PropertyName>
                                  <ogc:Literal>multipoint</ogc:Literal>
                                </ogc:PropertyIsEqualTo>
                                <ogc:PropertyIsEqualTo>
                                  <ogc:PropertyName>type</ogc:PropertyName>
                                  <ogc:Literal>point</ogc:Literal>
                                </ogc:PropertyIsEqualTo>
                               </ogc:Or>
                              </ogc:Filter>
                              <PointSymbolizer>
                                <Graphic>
                                  <Mark>
                                    <WellKnownName>circle</WellKnownName>
                                    <Fill>
                                      <CssParameter name='fill'>#FF0000</CssParameter>
                                      <CssParameter name='fill-opacity'>1.0</CssParameter>
                                    </Fill>
                                  </Mark>
                                  <Size>11</Size>
                                </Graphic>
                              </PointSymbolizer>
                              <PointSymbolizer>
                                <Graphic>
                                  <Mark>
                                    <WellKnownName>circle</WellKnownName>
                                    <Fill>
                                      <CssParameter name='fill'>#EDE513</CssParameter>
                                      <CssParameter name='fill-opacity'>1.0</CssParameter>
                                    </Fill>
                                  </Mark>
                                  <Size>7</Size>
                                </Graphic>
                              </PointSymbolizer>
                            </Rule>
                          </FeatureTypeStyle>
                        </UserStyle>
                      </NamedLayer>
                    </StyledLayerDescriptor>", param.stroke, param.strokeW.ToString().Replace(",", "."), param.fill, param.fillO.ToString().Replace(",", "."), param.idLayer);
            return xml;
        }
        private string GetUrlMaptile(string layerGeoserver)
        {
            string temp = string.Format("{0}:{1}", _configuration.GetSection("Geoserver:Workspace").Value, layerGeoserver);
            temp = (_configuration.GetSection("Geoserver:UrlWMSGeoserver").Value).ToString().Replace("LAYERSValue", temp);
            return string.Format("{0}{1}", _configuration.GetSection("Geoserver:DomainUrl").Value, temp);
        }
        private async Task<bool> SyncDataToGeoserver(string layerGeoserver, List<MainObjectGeoserver> listMainObjectGeoserver)
        {
            try
            {
                //truncate table
                await TruncateObjectTableWithLayer(layerGeoserver);
                bool countcheck = listMainObjectGeoserver.Count == 0? true:false;
                //add list object table
                foreach (var item in listMainObjectGeoserver)
                {
                    var geo = JsonConvert.SerializeObject(item.Geometry);
                    var wkt = ConvertGeojsonToWkt.GeoJsonToWKT(geo, null);
                    countcheck = await AddObjectTableWithLayer(layerGeoserver, wkt, item.Geometry.type, "", item.Id.ToString("D"));
                }
                return countcheck;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<bool> UpdateBBoxLayerGeoserver(string layerGeoserver)
        {
            try
            {
                string url = string.Format("{0}/geoserver/rest/workspaces/{1}/datastores/{2}/featuretypes/{3}?recalculate=nativebbox,latlonbbox",
                    _configuration.GetSection("Geoserver:DomainUrl").Value,
                    _configuration.GetSection("Geoserver:Workspace").Value,
                    _configuration.GetSection("Geoserver:StoreName").Value,
                    layerGeoserver);
                string body = "<featureType><enabled>true</enabled></featureType>";
                var httpClient = _httpClientFactory.CreateClient(GeoserverHttpClientKey);
                var content = new StringContent(body, Encoding.UTF8, "text/xml");
                httpClient.DefaultRequestHeaders.Add("Authorization", Auth);
                var response = await httpClient.PutAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<string> GetBboxFromLayerGeoserver(string layerGeoserver)
        {
            try
            {
                string url = string.Format("{0}/geoserver/rest/workspaces/{1}/datastores/{2}/featuretypes/{3}.json",
                    _configuration.GetSection("Geoserver:DomainUrl").Value,
                    _configuration.GetSection("Geoserver:Workspace").Value,
                    _configuration.GetSection("Geoserver:StoreName").Value,
                    layerGeoserver);
                var httpClient = _httpClientFactory.CreateClient(GeoserverHttpClientKey);
                //var content = new StringContent(body, Encoding.UTF8, "text/xml");
                httpClient.DefaultRequestHeaders.Add("Authorization", Auth);
                var response = await httpClient.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var a = await response.Content.ReadAsStringAsync();
                    dynamic obj = JsonConvert.DeserializeObject<dynamic>(a);
                    string minx = obj.featureType.latLonBoundingBox.minx;
                    string miny = obj.featureType.latLonBoundingBox.miny;
                    string maxx = obj.featureType.latLonBoundingBox.maxx;
                    string maxy = obj.featureType.latLonBoundingBox.maxy;
                    //string latlng= string.Format(@"[{lng: {0}, lat: {1}}, {lng: {2}, lat: {3}}]", minx, miny, maxx, maxy);
                    return "[{lng: " + minx + ", lat: " + minx + "}, {lng: " + maxx + ", lat: " + maxx + "}]";
                }
                return "[]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
