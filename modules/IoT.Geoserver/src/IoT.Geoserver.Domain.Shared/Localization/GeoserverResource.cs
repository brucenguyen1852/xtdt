﻿using Volo.Abp.Localization;

namespace IoT.Geoserver.Localization
{
    [LocalizationResourceName("Geoserver")]
    public class GeoserverResource
    {
        
    }
}
