﻿namespace IoT.Geoserver
{
    public static class GeoserverDbProperties
    {
        public static string DbTablePrefix { get; set; } = "Geoserver";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "Geoserver";
    }
}
