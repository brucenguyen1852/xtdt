﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace IoT.Geoserver
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(GeoserverDomainSharedModule)
    )]
    public class GeoserverDomainModule : AbpModule
    {

    }
}
