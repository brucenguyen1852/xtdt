﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.Geoserver.LayerGeoserver
{
    public class LayerToLayerGeoserver: AuditedAggregateRoot<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string LayerGeoserver { get; set; }//Table postgis and layer geoserver
        public int Status { get; set; }
        public string ColumnTable { get; set; } //các cột của table
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
