﻿using Volo.Abp.Settings;

namespace IoT.Geoserver.Settings
{
    public class GeoserverSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from GeoserverSettings class.
             */
        }
    }
}