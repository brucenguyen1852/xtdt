﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace IoT.Geoserver
{
    [DependsOn(
        typeof(GeoserverApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class GeoserverHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "Geoserver";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(GeoserverApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
