﻿using IoT.Geoserver.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace IoT.Geoserver
{
    public abstract class GeoserverController : AbpController
    {
        protected GeoserverController()
        {
            LocalizationResource = typeof(GeoserverResource);
        }
    }
}
