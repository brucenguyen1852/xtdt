﻿using Localization.Resources.AbpUi;
using IoT.Geoserver.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace IoT.Geoserver
{
    [DependsOn(
        typeof(GeoserverApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class GeoserverHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(GeoserverHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<GeoserverResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
