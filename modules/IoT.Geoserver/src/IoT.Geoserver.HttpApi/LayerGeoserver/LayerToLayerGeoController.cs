﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IoT.Common;
using IoT.Geoserver.PostgreSQL;
using IoT.HTKT.ManagementData;
using IoT.HTKT.PropertiesDirectory;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Newtonsoft.Json;
using Volo.Abp;

namespace IoT.Geoserver.LayerGeoserver
{
    [RemoteService]
    [Route("api/LayerToLayer/LayerGeo")]
    public class LayerToLayerGeoController : GeoserverController
    {
        private readonly ILayerToLayerService _layerToLayerService;
        private readonly PostgreSqlContext _postgreSqlContext;
        private GeoserverLayerService _geoserverLayerService;
        private readonly IMainObjectService _mainObjectService;
        private readonly IPropertiesDirectoryAppService _propertiesDirectoryAppService;
        public LayerToLayerGeoController(ILayerToLayerService layerToLayerService, PostgreSqlContext postgreSqlContext,
            IMainObjectService mainObjectService, IPropertiesDirectoryAppService propertiesDirectoryAppService)
        {
            _layerToLayerService = layerToLayerService;
            _postgreSqlContext = postgreSqlContext;
            _geoserverLayerService = new GeoserverLayerService(_postgreSqlContext);
            _mainObjectService = mainObjectService;
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
        }
        [HttpPost("CreateLayer")]
        public async Task<IActionResult> CreateLayerToLayer(CreateLayerToLayerDto param)
        {
            try
            {
                //CreateLayerToLayerDto createLayerToLayerDto = new CreateLayerToLayerDto();
                /////change name lớp
                //param.LayerGeoserver = Utils.ConvertTiengVietToAscii(param.LayerGeoserver).ToLower();
                /////lưu database của hệ thống
                //var result = await _layerToLayerService.CreateAsync(param);
                /////tạo table và publish layer
                //createLayerToLayerDto.ColumnTable = !string.IsNullOrEmpty(param.ColumnTable) ? param.ColumnTable : createLayerToLayerDto.ColumnTable;
                //var b = await _geoserverLayerService.CreateTableWithLayer(param.LayerGeoserver, createLayerToLayerDto.ColumnTable);
                var result = await _layerToLayerService.CreateToLayerGeoserverAsync(param);
                return Ok(new { code = "ok", message = "", result = result });
            }
            catch (Exception ex)
            {
                return Ok(new { code = "error", message = ex.Message, result = "" });
            }
        }

        #region ---------------Request API geoserver------------------
        [HttpPost("getInfor")]
        public async Task<ActionResult> GetInfor(List<ParamGetInfor> param)
        {
            try
            {
                foreach (var item in param)
                {
                    var url = item.Url;
                    HttpClient client = new HttpClient();
                    url = url.Replace("https://geoserver.com.vn:8443", "http://geoserver.com.vn:8080");
                    var response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        if (result != null && result.Length > 0)
                        {
                            if (item.Option == 2)
                            {
                                dynamic obj = JsonConvert.DeserializeObject<dynamic>(result);
                                var features = obj.features;
                                if (features.Count > 0)
                                {
                                    string id = obj.features[0].id;
                                    id = id.Split(".")[1];
                                    var re = await _mainObjectService.GetObject(id);
                                    return Ok(new { code = "ok", message = "2", result = re });
                                }
                            }
                            if (item.Option == 3)
                            {
                                return Ok(new { code = "ok", message = "3", result = result });
                            }
                        }
                    }
                }
                return Ok(new { code = "error", message = "Không get ", result = "" });
            }
            catch (Exception ex)
            {
                return Ok(new { code = "error", message = ex.Message, result = "" });
            }
        }
        [HttpPost("UpdateBbox")]
        public async Task<ActionResult> UpdateBbox([FromBody] string id)
        {
            await _layerToLayerService.UpdateBboxLayerGeoserverAsync(id);
            return Ok(new { code = "error", message = "Không get ", result = "" });
        }
        [HttpPost("searchBboxInfor")]
        public async Task<ActionResult> SearchBboxInfor(List<ParamGetInfor> param)
        {
            try
            {
                //-180,-90,180,90
                foreach (var item in param)
                {
                    var url = item.Url;
                    HttpClient client = new HttpClient();
                    url = url.Replace("https://geoserver.com.vn:8443", "http://geoserver.com.vn:8080");
                    var response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        if (result != null && result.Length > 0)
                        {
                            if (item.Option == 2)
                            {
                                dynamic obj = JsonConvert.DeserializeObject<dynamic>(result);
                                var features = obj.features;
                                List<object> list = new List<object>();
                                if (features.Count > 0)
                                {
                                    string idlayer = "";
                                    foreach (var feature in features)
                                    {
                                        string id = feature.id;
                                        id = id.Split(".")[1];
                                        try
                                        {
                                            var re = await _mainObjectService.GetObject(id);
                                            list.Add(re);
                                            if (idlayer.Length == 0)
                                                idlayer = Convert.ToString(re.GetType().GetProperty("IdDirectory").GetValue(re, null));
                                        }
                                        catch (Exception ex) { }
                                    }

                                    if (list.Count > 0)
                                    {
                                        var properties = await _propertiesDirectoryAppService.GetAsyncById(idlayer);
                                        return Ok(new { code = "ok", message = "2", result = list, properties = properties });
                                    }

                                    return Ok(new { code = "ok", message = "2", result = list, properties = "null" });
                                }
                            }
                            if (item.Option == 3)
                            {
                                return Ok(new { code = "ok", message = "3", result = result });
                            }
                        }
                    }
                }
                return Ok(new { code = "error", message = "Không get ", result = "" });
            }
            catch (Exception ex)
            {
                return Ok(new { code = "error", message = ex.Message, result = "" });
            }
        }

        [HttpPost("searchPropetiesInfor")]
        public async Task<ActionResult> SearchPropetiesInfor(ParamSearchWithProperties param)
        {
            try
            {
                var message = "";
                foreach (var item in param.Params)
                {
                    var url = item.Url;
                    HttpClient client = new HttpClient();
                    url = url.Replace("https://geoserver.com.vn:8443", "http://geoserver.com.vn:8080");
                    var response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)
                    {
                        message += "#URLSUCCESS";
                        var result = await response.Content.ReadAsStringAsync();
                        if (result != null && result.Length > 0)
                        {
                            if (item.Option == 2)
                            {
                                dynamic obj = JsonConvert.DeserializeObject<dynamic>(result);
                                var features = obj.features;
                                List<object> list = new List<object>();
                                if (features.Count > 0)
                                {
                                    message += "#Feature";
                                    string idlayer = "";
                                    foreach (var feature in features)
                                    {
                                        string id = feature.id;
                                        id = id.Split(".")[1];
                                        try
                                        {
                                            var re = await _mainObjectService.GetObjectByPropertiesValue(id, param.Search);
                                            if (re != null)
                                            {
                                                list.Add(re);
                                                if (idlayer.Length == 0)
                                                    idlayer = Convert.ToString(re.GetType().GetProperty("IdDirectory").GetValue(re, null));
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            message += "======" + ex.Message + "========";
                                        }
                                    }

                                    message += "---#REXXX---" + list.Count;
                                    if (list.Count > 0)
                                    {
                                        var properties = await _propertiesDirectoryAppService.GetAsyncById(idlayer);
                                        return Ok(new { code = "ok", message = "2", result = list, properties = properties });
                                    }
                                }
                            }
                            if (item.Option == 3)
                            {
                                return Ok(new { code = "ok", message = "3", result = result });
                            }
                        }
                    }
                }
                return Ok(new { code = "error", message = message, result = "" });
            }
            catch (Exception ex)
            {
                return Ok(new { code = "error", message = ex.Message, result = "" });
            }
        }
        #endregion
    }
}
