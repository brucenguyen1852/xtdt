﻿using IoT.Geoserver.LayerGeoserver;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.Geoserver.MongoDB
{
    [ConnectionStringName(GeoserverDbProperties.ConnectionStringName)]
    public class GeoserverMongoDbContext : AbpMongoDbContext, IGeoserverMongoDbContext
    {
        /* Add mongo collections here. Example:
         * public IMongoCollection<Question> Questions => Collection<Question>();
         */
        public IMongoCollection<LayerToLayerGeoserver> LayerToLayerGeoserver => Collection<LayerToLayerGeoserver>();

        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigureGeoserver();
        }
    }
}