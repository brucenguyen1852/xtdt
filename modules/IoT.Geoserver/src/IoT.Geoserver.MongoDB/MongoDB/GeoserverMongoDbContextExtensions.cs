﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace IoT.Geoserver.MongoDB
{
    public static class GeoserverMongoDbContextExtensions
    {
        public static void ConfigureGeoserver(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new GeoserverMongoModelBuilderConfigurationOptions(
                GeoserverDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}