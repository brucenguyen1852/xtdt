﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace IoT.Geoserver.MongoDB
{
    [DependsOn(
        typeof(GeoserverDomainModule),
        typeof(AbpMongoDbModule)
        )]
    public class GeoserverMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<GeoserverMongoDbContext>(options =>
            {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, MongoQuestionRepository>();
                 */
                options.AddDefaultRepositories();
            });
        }
    }
}
