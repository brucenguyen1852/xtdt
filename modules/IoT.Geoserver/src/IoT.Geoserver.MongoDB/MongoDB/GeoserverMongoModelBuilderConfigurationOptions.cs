﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace IoT.Geoserver.MongoDB
{
    public class GeoserverMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public GeoserverMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}