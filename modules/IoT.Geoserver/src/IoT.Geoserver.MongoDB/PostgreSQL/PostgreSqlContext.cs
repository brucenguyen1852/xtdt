﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace IoT.Geoserver.PostgreSQL
{
    public class PostgreSqlContext: DbContext
    {
        public PostgreSqlContext(DbContextOptions<PostgreSqlContext> options) : base(options)
        {
        }
        public int ExecuteSqlCommand(string sql,  params object[] parameters)
        {
            try
            {
                //string sql1 = @"DELETE FROM Customers WHERE CustomerId = @CustomerId";
                //SqlParameter pCustomerId = new SqlParameter("@CustomerId", customerId);
                int affected = this.Database.ExecuteSqlCommand(sql, parameters);
                return affected;
                //return this.Database.ExecuteSqlCommand(sql, param);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
