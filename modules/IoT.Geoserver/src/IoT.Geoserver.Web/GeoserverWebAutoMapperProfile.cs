﻿using AutoMapper;
using IoT.Geoserver.LayerGeoserver;

namespace IoT.Geoserver.Web
{
    public class GeoserverWebAutoMapperProfile : Profile
    {
        public GeoserverWebAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
            //CreateMap<LayerToLayerGeoserver, LayerToLayerGeoserverDto>();
        }
    }
}