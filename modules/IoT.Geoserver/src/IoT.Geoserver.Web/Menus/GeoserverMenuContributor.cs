﻿using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace IoT.Geoserver.Web.Menus
{
    public class GeoserverMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            //Add main menu items.
            //context.Menu.AddItem(new ApplicationMenuItem(GeoserverMenus.Prefix, displayName: "Geoserver", "~/Geoserver", icon: "fa fa-globe"));
            
            return Task.CompletedTask;
        }
    }
}