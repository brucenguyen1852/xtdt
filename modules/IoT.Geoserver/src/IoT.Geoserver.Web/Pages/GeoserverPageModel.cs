﻿using IoT.Geoserver.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace IoT.Geoserver.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class GeoserverPageModel : AbpPageModel
    {
        protected GeoserverPageModel()
        {
            LocalizationResourceType = typeof(GeoserverResource);
            ObjectMapperContext = typeof(GeoserverWebModule);
        }
    }
}