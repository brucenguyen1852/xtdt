﻿namespace IoT.Geoserver
{
    /* Inherit from this class for your application layer tests.
     * See SampleAppService_Tests for example.
     */
    public abstract class GeoserverApplicationTestBase : GeoserverTestBase<GeoserverApplicationTestModule>
    {

    }
}