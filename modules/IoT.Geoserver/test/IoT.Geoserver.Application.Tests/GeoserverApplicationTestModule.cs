﻿using Volo.Abp.Modularity;

namespace IoT.Geoserver
{
    [DependsOn(
        typeof(GeoserverApplicationModule),
        typeof(GeoserverDomainTestModule)
        )]
    public class GeoserverApplicationTestModule : AbpModule
    {

    }
}
