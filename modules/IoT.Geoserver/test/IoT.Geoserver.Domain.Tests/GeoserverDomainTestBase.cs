﻿namespace IoT.Geoserver
{
    /* Inherit from this class for your domain layer tests.
     * See SampleManager_Tests for example.
     */
    public abstract class GeoserverDomainTestBase : GeoserverTestBase<GeoserverDomainTestModule>
    {

    }
}