using IoT.Geoserver.MongoDB;
using Volo.Abp.Modularity;

namespace IoT.Geoserver
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(GeoserverMongoDbTestModule)
        )]
    public class GeoserverDomainTestModule : AbpModule
    {
        
    }
}
