﻿using IoT.Geoserver.Samples;

namespace IoT.Geoserver.EntityFrameworkCore.Samples
{
    public class SampleRepository_Tests : SampleRepository_Tests<GeoserverEntityFrameworkCoreTestModule>
    {
        /* Don't write custom repository tests here, instead write to
         * the base class.
         * One exception can be some specific tests related to EF core.
         */
    }
}
