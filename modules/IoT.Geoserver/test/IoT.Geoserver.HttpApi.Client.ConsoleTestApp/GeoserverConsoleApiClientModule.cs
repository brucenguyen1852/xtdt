﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace IoT.Geoserver
{
    [DependsOn(
        typeof(GeoserverHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class GeoserverConsoleApiClientModule : AbpModule
    {
        
    }
}
