﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.Country
{
    public class CountryGeoJsonDto : AuditedEntityDto<Guid>
    {
        public string IdCountry { get; set; }
        public string CodeCountry { get; set; }
        public Geometry Geometry { get; set; } // geojson 2D
    }
}
