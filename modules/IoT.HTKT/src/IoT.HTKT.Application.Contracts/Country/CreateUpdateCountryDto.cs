﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.Country
{
    public class CreateUpdateCountryDto
    {
        public bool isDeleted { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int level { get; set; }
        public string type { get; set; }
        public string code { get; set; }
        public string search { get; set; }
        public string fullCode { get; set; }
    }
}
