﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Country
{
    public class CreateUpdateGeojsonCountryDto
    {
        public string IdCountry { get; set; }
        public string CodeCountry { get; set; }
        public Geometry Geometry { get; set; } // geojson 2D
    }
}
