﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Country
{
    public interface ICountryService:IApplicationService
    {
        /// <summary>
        /// get country by code, level
        /// </summary>
        /// <param name="code"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        Task<List<CountryDto>> GetCountry(string code, int level);
        /// <summary>
        /// create country
        /// </summary>
        /// <param name="param">list country</param>
        /// <returns></returns>
        Task<bool> CreateCountry(List<CreateUpdateCountryDto> param);
    }
}
