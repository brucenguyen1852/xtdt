﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Country
{
    public interface IGeojsonCountryService : IApplicationService
    {
        /// <summary>
        /// insert geojson country
        /// </summary>
        /// <param name="listObj">list param goejson country</param>
        /// <returns></returns>
        Task<bool> UpdateGeometry(List<CreateUpdateGeojsonCountryDto> listObj);
        /// <summary>
        /// get geojson country by code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<CountryGeoJsonDto> GetGeojsonCountryByCode(string code);
        /// <summary>
        /// get children geojson country by code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<List<CountryGeoJsonDto>> GetGeojsonCountryByCodeChildren(string code);
    }
}
