﻿using IoT.HTKT.ManagementData;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.DetailActivity
{
    public class DetailActivityDto : AuditedEntityDto<Guid>
    {
        public string IdDirectoryActivity { get; set; } //Id của thư mục hoạt động
        public string IdDirectory { get; set; } //Id của lớp dữ liệu
        public string IdMainObject { get; set; } // Id đối tượng của lớp
        public string NameDirectoryActivity { get; set; } // Tên đối tượng
        public bool IsCheckImplementProperties { get; set; } = true; // kiểm tra có kết thừa properties của lớp hay không
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
        public DateTime? DateActivity { get; set; } // ngày tháng năm hoạt động
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
