﻿using IoT.HTKT.DetailActivity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.DetailActivity
{
    public interface IDetailActivityService : ICrudAppService< //Defines CRUD methods
            DetailActivityDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDetailActivityDto> //Used to create/update a book
    {
        Task<List<InfoActivityDetailDto>> GetListDetailActivity(string idMainObject, string idDirectory, string idDirectoryActivity);
        bool CheckExitsCode(string code, string idDirectoryAcitivty);
    }
}
