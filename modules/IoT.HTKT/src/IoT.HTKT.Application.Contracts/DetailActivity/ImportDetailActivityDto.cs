﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.DetailActivity
{
    public class ImportDetailActivityDto
    {
        public string DirectoryActivityId { get; set; }
        public string IdDirectory { get; set; } //Id của lớp dữ liệu
        public string IdMainObject { get; set; } // Id đối tượng của lớp
        public IFormFile Files { get; set; }
    }
}
