﻿using IoT.HTKT.Activity;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.DirectoryActivity
{
    public class CloneDiretoryActivityDto: CreateDirectoryActivityDto
    {
        public string IdImplement { get; set; }
    }
}
