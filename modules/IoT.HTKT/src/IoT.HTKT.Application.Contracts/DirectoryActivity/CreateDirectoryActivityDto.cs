﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Activity
{
    public class CreateDirectoryActivityDto
    {
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string ParentId { get; set; }
        public string Search { get; set; }
        public string IdDirectory { get; set; } // id lớp dữ liệu cho hoạt động
        public bool TypeDirectory { get; set; }
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
