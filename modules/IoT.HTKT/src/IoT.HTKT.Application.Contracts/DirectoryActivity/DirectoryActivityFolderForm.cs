﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.DirectoryActivity
{
    public class DirectoryActivityFolderForm
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
