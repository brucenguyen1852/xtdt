﻿using IoT.HTKT.PropertiesDirectoryActivity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Activity
{
    public interface IDirectoryActivityService : ICrudAppService< //Defines CRUD methods
            DirectoryActivityDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDirectoryActivityDto> //Used to create/update a book
    {
        Task<List<DirectoryActivityProperties>> GetListDirectoryActivityProperties(string idDirectory);
        Task<List<DirectoryActivityProperties>> GetListDirectoryActivityNotProperties(string idDirectory);
        Task<bool> DeleteFolder(string id);

        Task<bool> UpdateName(string id, string name, string idDirectory);

        Task<bool> UpdateNameFolder(string id, string name);
        Task<bool> CheckDeleteActivity(string id);

        Task<List<DirectoryActivityProperties>> GetListCountDirectoryActivity();
    }
}
