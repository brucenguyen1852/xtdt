﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace IoT.HTKT
{
    [DependsOn(
        typeof(HTKTDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class HTKTApplicationContractsModule : AbpModule
    {

    }
}
