﻿using IoT.HTKT.ManagementData;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Layer
{
    public class CloneDiretoryDto : CreateDirectoryDto
    {
        public string IdImplement { get; set; }
        public Location Location { get; set; }
        public int Zoom { get; set; }
        public string BoundMaptile { get; set; } //Tọa độ Matile theo vùng
        public string GroundMaptile { get; set; }

        public int OptionDirectory { get; set; } = 1;
    }
}
