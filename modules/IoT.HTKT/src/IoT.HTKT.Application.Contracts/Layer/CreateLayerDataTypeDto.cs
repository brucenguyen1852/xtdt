﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Layer
{
    public class CreateLayerDataTypeDto
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
