﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Layer
{
    public class DirectoryFormModel
    {
        public string Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string ParentId { get; set; }
        public string Search { get; set; }
        public bool TypeDirectory { get; set; }
    }
}
