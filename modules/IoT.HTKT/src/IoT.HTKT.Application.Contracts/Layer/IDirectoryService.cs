﻿using IoT.HTKT.PropertiesDirectory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Layer
{
    public interface IDirectoryService : ICrudAppService< //Defines CRUD methods
            DirectoryDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDirectoryDto> //Used to create/update a book
    {
        Task<List<DirectoryDto>> GetListDirectory();
        Task<bool> DeleteFolder(string id);
        Task<List<DirectoryProperties>> GetListDirectoryProperties();
        Task<List<DirectoryProperties>> GetListDirectoryNotProperties();
        Task<List<DirectoryProperties>> GetListDirectoryPropertiesHasMap();
        Task<bool> UpdateName(string id, string name);
        Task<bool> CheckDeleteLayer(string id);

        List<DirectoryDto> GetParentLayer(string id);
        Task<DirectoryDto> FinDirectoryById(string id);

        Task<List<DirectoryDto>> FindFileDirectory();

    }
}
