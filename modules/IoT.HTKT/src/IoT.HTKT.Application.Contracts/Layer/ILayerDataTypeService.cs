﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Layer
{
    public interface ILayerDataTypeService : ICrudAppService< //Defines CRUD methods
            LayerDataTypeDto, //Used to show 
            Guid, //Primary key of the  entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateLayerDataTypeDto> //Used to create/update a 
    {
        Task<List<LayerDataTypeDto>> GetListDataTypeAsync();
    }
}
