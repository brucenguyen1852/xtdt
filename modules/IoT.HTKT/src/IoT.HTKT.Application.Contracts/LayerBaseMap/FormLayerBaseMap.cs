﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.LayerBaseMap
{
    public class FormLayerBaseMap
    {
        public string Id { get; set; }
        public string NameBasMap { get; set; } // tên bản đồ nền
        public string Link { get; set; } // đường dẫn
        public int Order { get; set; } //thứ tự
        public string Image { get; set; } //hình ảnh đại điện
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
