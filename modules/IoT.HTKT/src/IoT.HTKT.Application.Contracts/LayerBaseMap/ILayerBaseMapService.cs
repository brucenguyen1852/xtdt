﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.LayerBaseMap
{
    public interface ILayerBaseMapService : ICrudAppService< //Defines CRUD methods
            LayerBaseMapDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateLayerBaseMapDto> //Used to create/update a book
    {
        Task<object> GetListLayerBaseMap(string Sorting, int SkipCount, int MaxResultCount);
        bool CheckExitsOrder(Guid id, int order);
    }
}
