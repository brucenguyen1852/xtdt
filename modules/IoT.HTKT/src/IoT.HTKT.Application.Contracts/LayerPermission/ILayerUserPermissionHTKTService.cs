﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.LayerPermission
{
    public interface ILayerUserPermissionHTKTService : ICrudAppService< //Defines CRUD methods
            LayerUserPermissionDto, //Used to show LayerUserPermission
            Guid, //Primary key of the LayerUserPermission entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateLayerUserPermissionDto> //Used to create/update a LayerUserPermission
    {
        Task<List<LayerUserPermissionDto>> GetListOfUserOrRole(string UserId, List<string> RoleId);
    }
}
