﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.LocationSetting
{
    public class CreateUpdateLocationSettingDto
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
