﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.LocationSetting
{
    public interface ILocationSettingService : ICrudAppService< //Defines CRUD methods
           LocationSettingDto, //Used to show LayerUserPermission
            Guid, //Primary key of the LayerUserPermission entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateLocationSettingDto> //Used to create/update a LayerUserPermission
    {
        Task<LocationSettingDto> CreateLocationCenter(CreateUpdateLocationSettingDto dto);
        Task<LocationSettingDto> UpdateLocationCenter(Guid id, CreateUpdateLocationSettingDto dto);
        LocationSettingDto GetLocationCenter();
    }

}
