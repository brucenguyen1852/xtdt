﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.LocationSetting
{
    public class LocationSettingDto : AuditedEntityDto<Guid>
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
