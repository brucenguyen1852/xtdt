﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class CreateUpdateDetailObjectDto
    {
        public string IdMainObject { get; set; }
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
        public string IdDirectory { get; set; } = string.Empty; //Id của thư mục
    }
}