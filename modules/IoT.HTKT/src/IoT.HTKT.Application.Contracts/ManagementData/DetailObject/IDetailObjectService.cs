﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.ManagementData
{
    public interface IDetailObjectService : ICrudAppService< //Defines CRUD methods
            DetailObjectDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateDetailObjectDto> //Used to create/update a PropertiesDirectory
    {
        Task<DetailObjectDto> GetDetailObjectByMainObjectId(string id);
        bool CheckDeleteProperties(List<string> mainId, string code);

        Task<MainObjectDto> UpdateExtendMainObject(FormManagementDataModel dto);

        Task<MainObjectDto> UpdatePropertiesMainObject(FormManagementDataModel dto);
    }
}
