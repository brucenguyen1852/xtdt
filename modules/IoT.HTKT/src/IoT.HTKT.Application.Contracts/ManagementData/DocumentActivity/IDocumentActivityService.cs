﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.ManagementData
{
    public interface IDocumentActivityService : ICrudAppService<
            DocumentActivityDto,
            Guid,
            PagedAndSortedResultRequestDto,
            CreateOrUpdateDocumentActivityDto>
    {
        Task<List<DocumentActivityDto>> GetAllFile();
        Task<List<DocumentActivityDto>> getListDocumentbyObjectMainId(Guid objectMainId);

        Task<List<DocumentActivityDto>> getListDocumentbyActivityId(string activityId);
    }
}
