﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class CreateOrUpdateDocumentObjectDto
    {
        public string Id { get; set; }
        public string IdMainObject { get; set; } // id của đối tượng
        public string NameDocument { get; set; } //Tên hoặc tự đề của file
        public string IconDocument { get; set; } //avatar hoặc biểu tượng của file
        public string UrlDocument { get; set; } //link url biểu tượng
        public string NameFolder { get; set; } //Thư mục của file
        public int OrderDocument { get; set; } //thứ tự sắp xếp
    }
}
