﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.ManagementData
{
    public class DocumentObjectDTO : AuditedEntityDto<Guid>
    {
        public string IdMainObject { get; set; } // id của đối tượng
        public string NameDocument { get; set; } //Tên hoặc tự đề của file
        public string IconDocument { get; set; } //avatar hoặc biểu tượng của file
        public string UrlDocument { get; set; } //link url biểu tượng
        public string NameFolder { get; set; } //Thư mục của file
        public int OrderDocument { get; set; } //thứ tự sắp xếp
    }
}
