﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.ManagementData
{
    public interface IDocumentObjectService : ICrudAppService<
            DocumentObjectDTO,
            Guid,
            PagedAndSortedResultRequestDto,
            CreateOrUpdateDocumentObjectDto>
    {
        Task<List<DocumentObjectDTO>> GetAllFile();
        Task<List<DocumentObjectDTO>> getListDocumentbyObjectMainId(Guid objectMainId);
        Task<bool> DeleteListObject(List<string> id);

        Task<List<DocumentObjectDTO>> UpdateFileDocumentMainObject(UpdateDocumentMainObjectFormData input);
    }
}
