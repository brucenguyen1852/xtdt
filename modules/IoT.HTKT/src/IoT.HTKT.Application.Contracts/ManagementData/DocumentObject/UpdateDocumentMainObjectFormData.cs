﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class UpdateDocumentMainObjectFormData
    {
        public string idMainObject { get; set; }

        public List<CreateOrUpdateDocumentObjectDto> data { get; set; }
    }
}
