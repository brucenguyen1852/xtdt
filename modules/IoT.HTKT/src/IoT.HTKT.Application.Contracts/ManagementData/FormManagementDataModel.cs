﻿using IoT.HTKT.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class FormManagementDataModel
    {
        public string Id { get; set; }

        public string IdDirectory { get; set; } //Id của thư mục

        //[Required(ErrorMessage ="Tên đối tượng là bắt buộc")]
        public string NameObject { get; set; } // Tên đối tượng

        public Geometry Geometry { get; set; }

        public PropertiesGeojson PropertiesGeojson { get; set; } // thuộc tính cơ bản của đối tượng (màu, độ mờ,... )

        public Object3D object3D { get; set; } // đói tượng 3d

        public bool IsCheckImplementProperties { get; set; }

        public string QRCodeObject { get; set; } // dữ liệu QRCode

        public List<PropertiesObject> ListProperties { get; set; } // Bảng thuộc tính

        public string District { get; set; } = string.Empty; // Quận/ Huyện

        public string WardDistrict { get; set; } = string.Empty; // Xã/ Phường
    }
}
