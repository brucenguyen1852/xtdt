﻿using IoT.HTKT.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class InfoObjectData
    {
        public string Id { get; set; }
        public string NameObject { get; set; } // Tên đối tượng
        public Geometry Geometry { get; set; }
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
        public string IdDirectory { get; set; }
        public PropertiesGeojson Properties { get; set; } = new PropertiesGeojson(); // properties geojson 2D
        public Object3D object3D { get; set; }
        public Dictionary<string, object> Tags { get; set; }
        public bool IsCheckImplementProperties { get; set; }
        public string QRCodeObject { get; set; }
        public int TotalCount { get; set; }
        public string District { get; set; } = string.Empty; // Quận/ Huyện

        public string WardDistrict { get; set; } = string.Empty; // Xã/ Phường
    }
    public class InfoObjectDataNew
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("nameObject")]
        public string NameObject { get; set; } // Tên đối tượng
        [JsonProperty("geometry")]
        public GeometryNew Geometry { get; set; }
        [JsonProperty("listProperties")]
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
        [JsonProperty("idDirectory")]
        public string IdDirectory { get; set; }
        [JsonProperty("properties")]
        public PropertiesGeojson Properties { get; set; } = new PropertiesGeojson(); // properties geojson 2D
        [JsonProperty("object3D")]
        public Object3D object3D { get; set; }
        [JsonProperty("tags")]
        public Dictionary<string, object> Tags { get; set; }
        [JsonProperty("isCheckImplementProperties")]
        public bool IsCheckImplementProperties { get; set; }
        [JsonProperty("qRCodeObject")]
        public string QRCodeObject { get; set; }

        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("district")]
        public string District { get; set; } = string.Empty; // Quận/ Huyện
        [JsonProperty("wardDistrict")]
        public string WardDistrict { get; set; } = string.Empty; // Xã/ Phường
    }
}
