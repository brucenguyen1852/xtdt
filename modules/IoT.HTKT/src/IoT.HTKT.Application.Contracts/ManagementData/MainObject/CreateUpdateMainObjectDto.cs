﻿using IoT.HTKT.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class CreateUpdateMainObjectDto
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string NameObject { get; set; } // Tên đối tượng
        public PropertiesGeojson PropertiesGeojson { get; set; } // thuộc tính cơ bản của đối tượng (màu, độ mờ,... )
        public Geometry Geometry { get; set; }
        public bool IsCheckImplementProperties { get; set; }
        public string QRCodeObject { get; set; } //  dữ liệu QRCode
        public Object3D object3D { get; set; } // đối tượng 3d

        public string District { get; set; }  // Quận/ Huyện

        public string WardDistrict { get; set; }  // Xã/ Phường


    }
}
