﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class FormUpdateQRCode
    {
        public string Id { get; set; }
        public string QRCodeObject { get; set; }
    }
}
