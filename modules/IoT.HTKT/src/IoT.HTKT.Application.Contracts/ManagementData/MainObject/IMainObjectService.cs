﻿using IoT.Common;
using IoT.HTKT.Object;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.ManagementData
{
    public interface IMainObjectService : ICrudAppService< //Defines CRUD methods
            MainObjectDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateMainObjectDto> //Used to create/update a PropertiesDirectory
    {
        object GetListObject(DataTableViewModel dataTableViewModel);

        Task<object> GetListObjectAll(string Sorting, int SkipCount, int MaxResultCount);

        Task<object> GetObject(string id);
        Task<object> GetObjectByPropertiesValue(string id, string search = "");
        Task<object> GetObjectExploit(string id);

        Task<bool> DeletedObject(string id);

        List<InfoObjectData> GetObjectByIdDriectory(string id);
        Task<List<InfoObjectData>> GetObjectByIdDriectoryKhaiThac(string id);
        Task<object> GetObjectByKeyWord(string keyword);
        Task<object> CreaterListMainObject(List<ListMainObject> listMainObject);
        Task<string> ExportToPdf(List<ObjectLocation> input);
        Task<MainObjectDto> UpdateNameMainObject(string id, string name);
        object GetNumberObjectByIdDriectory(string id, string search, int skipnumber, int countnumber, string code, string type);
        Task<bool> DeleteObjectMode3D(string id);

        Task<bool> UpdateQRCode(FormUpdateQRCode input);

        #region------Function maptile geoserver
        Task<int> GetCountByIdDriectory(string idDriectory);
        #endregion
        #region------Optimal load data-------
        Task<List<InfoObjectDataNew>> GetObjectPagingByIdDirectory(string id, int page);
        Task<bool> ChangeValueSearch();
        Task<bool> ChangeValueDetail();
        #endregion

        Task GetObjectGeoJsonAsync(string id);
    }
}
