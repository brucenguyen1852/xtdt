﻿using IoT.HTKT.Common;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.ManagementData
{
    public class MainObjectDto : AuditedEntityDto<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string NameObject { get; set; } // Tên đối tượng
        public Geometry Geometry { get; set; }
        public PropertiesGeojson Properties { get; set; } = new PropertiesGeojson(); // properties geojson 2D
        public Object3D object3D { get; set; }

        public string District { get; set; } // Quận/ Huyện

        public string WardDistrict { get; set; }  // Xã/ Phường
    }
}
