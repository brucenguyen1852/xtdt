﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Object
{
    public interface IObjectModelService : ICrudAppService< //Defines CRUD methods
            ObjectModelDto, //Used to show 
            Guid, //Primary key of the  entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateObjectModelDto> //Used to create/update a 
    {
        Task<List<ObjectModelDto>> GetListDataAsync();
    }
}
