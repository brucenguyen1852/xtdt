﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Object
{
    public class ObjectLocation
    {
        public string Name { get; set; }
        public string QRCode { get; set; }
    }
}
