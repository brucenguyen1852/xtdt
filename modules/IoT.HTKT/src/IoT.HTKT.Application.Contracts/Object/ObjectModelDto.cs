﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.Object
{
    public class ObjectModelDto : AuditedEntityDto<Guid>
    {
        public string IdObject { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ObjName { get; set; }
        public string ObjUrl { get; set; }
        public string ObjData { get; set; }
        public string TextureName { get; set; }
        public string TextureUrl { get; set; }
        public string TextureData { get; set; }
        public string Color { get; set; }
        public string ThumbnailName { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Coordinates { get; set; }
        public int Height { get; set; }
        public List<string> PlaceTypes { get; set; }
        public string PlaceTypeInfo { get; set; }
    }
}
