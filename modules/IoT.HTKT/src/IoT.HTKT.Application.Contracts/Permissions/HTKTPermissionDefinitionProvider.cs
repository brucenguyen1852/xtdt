﻿using IoT.HTKT.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace IoT.HTKT.Permissions
{
    public class HTKTPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(HTKTPermissions.GroupName, L("Permission:HTKT"));

            //// thêm phân quyền 
            var inforManagementDataPermission = myGroup.AddPermission(HTKTPermissions.ManagementData.Default, L("Permission:ManagementData"));
            //inforManagementDataPermission.AddChild(HTKTPermissions.ManagementDataView.Create, L("Permission:ManagementData.Create"));
            //inforManagementDataPermission.AddChild(HTKTPermissions.ManagementDataView.Edit, L("Permission:ManagementData.Edit"));
            //inforManagementDataPermission.AddChild(HTKTPermissions.ManagementDataView.Delete, L("Permission:ManagementData.Delete"));
            //var inforExploitPermission = myGroup.AddPermission(HTKTPermissions.Exploit.Default, L("Permission:Exploit"));
            var inforExploitPermission = myGroup.AddPermission(HTKTPermissions.Exploit.Default, L("Permission:Exploit"));

            var inforReportPermission = myGroup.AddPermission(HTKTPermissions.Report.Default, L("Permission:Report"));

            var inforAdministrationPermission = myGroup.AddPermission(HTKTPermissions.Administration.Default, L("Permission:Administration"));

            // phân quyền quản lý dữ liệu admin
            var AdminManagermentPermisson = inforAdministrationPermission.AddChild(HTKTPermissions.Administration.ManagementData, L("Permission:Administration.ManagementData"));
            //AdminManagermentPermisson.AddChild(HTKTPermissions.AdminManagementData.View, L("Permission:Administration.ManagementData.View"));
            //AdminManagermentPermisson.AddChild(HTKTPermissions.AdminManagementData.Create, L("Permission:Administration.ManagementData.Create"));
            //AdminManagermentPermisson.AddChild(HTKTPermissions.AdminManagementData.Edit, L("Permission:Administration.ManagementData.Edit"));
            //AdminManagermentPermisson.AddChild(HTKTPermissions.AdminManagementData.Delete, L("Permission:Administration.ManagementData.Delete"));

            // phân quyền quản lý hoạt động admin
            var inforTypeActive = inforAdministrationPermission.AddChild(HTKTPermissions.Administration.TypeActive, L("Permission:Administration.TypeActive"));
            //inforTypeActive.AddChild(HTKTPermissions.TypeActive.Create, L("Permission:Administration.TypeActive.Create"));
            //inforTypeActive.AddChild(HTKTPermissions.TypeActive.Edit, L("Permission:Administration.TypeActive.Edit"));
            //inforTypeActive.AddChild(HTKTPermissions.TypeActive.Delete, L("Permission:Administration.TypeActive.Delete"));

            // phân quyền bản đồ nền
            var inforLayerBaseMap = inforAdministrationPermission.AddChild(HTKTPermissions.Administration.LayerBaseMap, L("Permission:Administration.LayerBaseMap"));
            //inforLayerBaseMap.AddChild(HTKTPermissions.LayerBaseMap.Create, L("Permission:Administration.LayerBaseMap.Create"));
            //inforLayerBaseMap.AddChild(HTKTPermissions.LayerBaseMap.Edit, L("Permission:Administration.LayerBaseMap.Edit"));
            //inforLayerBaseMap.AddChild(HTKTPermissions.LayerBaseMap.Delete, L("Permission:Administration.LayerBaseMap.Delete"));

            inforAdministrationPermission.AddChild(HTKTPermissions.Administration.DataIndex, L("Permission:Administration.DataIndex"));
            //inforAdministrationPermission.AddChild(HTKTPermissions.Administration.ManagementLayerData, L("Permission:Administration.ManagementLayerData"));
            inforAdministrationPermission.AddChild(HTKTPermissions.Administration.RestoreData, L("Permission:Administration.RestoreData"));
            inforAdministrationPermission.AddChild(HTKTPermissions.Administration.BackupData, L("Permission:Administration.BackupData"));
            inforAdministrationPermission.AddChild(HTKTPermissions.Administration.MapCenter, L("Permission:Administration.MapCenter"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<HTKTResource>(name);
        }
    }
}