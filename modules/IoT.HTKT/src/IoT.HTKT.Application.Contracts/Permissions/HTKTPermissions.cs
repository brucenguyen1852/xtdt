﻿using Volo.Abp.Reflection;

namespace IoT.HTKT.Permissions
{
    public class HTKTPermissions
    {
        public const string GroupName = "HTKT";
        public static class ManagementData
        {
            public const string Default = GroupName + ".ManagementData";
        }

        public static class Exploit
        {
            public const string Default = GroupName + ".Exploit";
        }
        public static class Report
        {
            public const string Default = GroupName + ".Report";
        }

        public static class Administration
        {
            public const string Default = GroupName + ".Administration";
            public const string ManagementData = Default + ".ManagementData";
            public const string DataIndex = Default + ".DataIndex";
            public const string ManagementLayerData = Default + ".ManagementLayerData";
            ///public const string ManagementAccount = Default + ".ManagementData";
            public const string RestoreData = Default + ".RestoreData";
            public const string BackupData = Default + ".BackupData";
            public const string TypeActive = Default + ".TypeActive";
            public const string LayerBaseMap = Default + ".LayerBaseMap";
            public const string MapCenter = Default + ".MapCenter";
        }

        public static class AdminManagementData
        {
            public const string Default = GroupName + ".Administration.ManagementData";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
            public const string View = Default + ".View";
        }

        public static class TypeActive
        {
            public const string Default = GroupName + ".Administration.TypeActive";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }

        public static class LayerBaseMap
        {
            public const string Default = GroupName + ".Administration.LayerBaseMap";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }

        public static class ManagementDataView
        {
            public const string Default = GroupName;
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(HTKTPermissions));
        }
    }
}