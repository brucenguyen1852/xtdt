﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.PropertiesDefault
{
    public class CreatePropertiesDefaultDto
    {
        public string NameProperties { get; set; } // tên thuộc tính
        public string CodeProperties { get; set; } // mã thuộc tính
        public string TypeProperties { get; set; } // loại thuộc tính
        public bool IsShow { get; set; } // hiển thị
        public bool IsIndexing { get; set; } // chỉ mục
        public bool IsRequest { get; set; } // yêu cầu
        public bool IsView { get; set; } // chỉ xem
        public bool IsHistory { get; set; } // lịch sử
        public bool IsInheritance { get; set; } // Kế thừa
        public bool IsAsync { get; set; } // Đồng bộ
        public bool IsShowExploit { get; set; } //hiển thị khi hover vào icon page khaithac
        public bool IsShowSearchExploit { get; set; }//hiển thị khi tìm kiếm page khaithac
        public bool IsShowReference { get; set; } //hiển thị khi tham chiếu
        public string DefalutValue { get; set; } //Giá trị mặc định
        public string ShortDescription { get; set; } //mô tả
        public int TypeSystem { get; set; } // Thuộc tính hệ thống hay thường
        public int OrderProperties { get; set; } // thứ tự thuộc tính
    }
}
