﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.PropertiesDefault
{
    public interface IPropertiesDefaultAppService :
        ICrudAppService< //Defines CRUD methods
            PropertiesDefaultDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePropertiesDefaultDto> //Used to create/update a PropertiesDirectory
    {
        Task<List<PropertiesDefaultDto>> GetListProperties();
    }
}
