﻿using IoT.HTKT.ManagementData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IoT.HTKT.PropertiesDirectory
{
    public class CreateUpdatePropertiesDirectoryDto
    {
        public string IdDirectory { get; set; }
        public string NameTypeDirectory { get; set; } //Tên loại dữ liệu
        public string CodeTypeDirectory { get; set; } //Mã loại dữ liệu
        public int VerssionTypeDirectory { get; set; } //Phiên bản
        public string IdImplement { get; set; } // kế thừa
        public List<DetailProperties> ListProperties { get; set; } // Bảnh thuộc tính
        public string GroundMaptile { get; set; } //Matile theo vùng
        public string BoundMaptile { get; set; } //Tọa độ Matile theo vùng
        public Location Location { get; set; } // tọa độ
        public int Zoom { get; set; } // mức zoom
        public int OptionDirectory { get; set; } = 1;
        public int MinZoom { get; set; } = 0;
        public int MaxZoom { get; set; } = 0;
    }
}
