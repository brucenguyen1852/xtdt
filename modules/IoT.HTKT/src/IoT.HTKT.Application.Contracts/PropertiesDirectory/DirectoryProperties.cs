﻿using IoT.HTKT.Layer;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.PropertiesDirectory
{
    public class DirectoryProperties: DirectoryDto
    {
        public string NameTypeDirectory { get; set; } //Tên loại dữ liệu
        public string CodeTypeDirectory { get; set; } //Mã loại dữ liệu
        public int VerssionTypeDirectory { get; set; } //Phiên bản
        public string IdImplement { get; set; } // kế thừa
        public List<DetailProperties> ListProperties { get; set; } // Bảnh thuộc tính
        public string Type { get; set; }
        public int Count { get; set; }
        public string Image2D { get; set; } // ảnh đại điện
        public string IdDirectory { get; set; }
        public int OptionDirectory { get; set; } = 1;
        public int MinZoom { get; set; } = 0;
        public int MaxZoom { get; set; } = 0;
    }
}
