﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.PropertiesDirectory
{
    public interface IPropertiesDirectoryAppService :
        ICrudAppService< //Defines CRUD methods
            PropertiesDirectoryDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdatePropertiesDirectoryDto> //Used to create/update a PropertiesDirectory
    {
        Task<PropertiesDirectoryDto> GetAsyncById(string idDirectory);

        Task<List<PropertiesDirectoryDto>> GetListDirectoryAsync();

        Task<bool> DeleteDirectoryByIdDirectory(string id);

        Task<PropertiesDirectoryDto> UpdateListProperties(List<DetailProperties> list, string id);

        Task<List<PropertiesDirectoryDto>> GetListDirectoryByOptionLayerAsync(int optionLayer);
        Task<bool> UpdateDataModel();
    }
}
