﻿using IoT.HTKT.ManagementData;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.PropertiesDirectory
{
    public class PropertiesDirectoryDto : AuditedEntityDto<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string NameTypeDirectory { get; set; } //Tên loại dữ liệu
        public string CodeTypeDirectory { get; set; } //Mã loại dữ liệu
        public int VerssionTypeDirectory { get; set; } //Phiên bản
        public string IdImplement { get; set; } // kế thừa
        public List<DetailProperties> ListProperties { get; set; } // Bảnh thuộc tính
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
        public string GroundMaptile { get; set; } //Matile theo vùng
        public Location Location { get; set; } // tọa độ
        public int Zoom { get; set; } // mức zoom
        public int OptionDirectory { get; set; } = 1;
        public int MinZoom { get; set; } = 0;
        public int MaxZoom { get; set; } = 0;
        public string District { get; set; } = string.Empty; // Quận/ Huyện

        public string WardDistrict { get; set; } = string.Empty; // Xã/ Phường

    }

    public class DetailProperties
    {
        public string NameProperties { get; set; } // tên thuộc tính
        public string CodeProperties { get; set; } // mã thuộc tính
        public string TypeProperties { get; set; } // loại thuộc tính
        public bool IsShow { get; set; } // hiển thị
        public bool IsIndexing { get; set; } // chỉ mục
        public bool IsRequest { get; set; } // yêu cầu
        public bool IsView { get; set; } // chỉ xem
        public bool IsHistory { get; set; } // lịch sử
        public bool IsInheritance { get; set; } // Kế thừa
        public bool IsAsync { get; set; } // Đồng bộ
        public bool IsShowExploit { get; set; } //hiển thị khi hover vào icon page khaithac
        public bool IsShowSearchExploit { get; set; }//hiển thị khi tìm kiếm page khaithac
        public bool IsShowReference { get; set; } //hiển thị khi tham chiếu
        public string DefalutValue { get; set; } //Giá trị mặc định
        public string ShortDescription { get; set; } //mô tả
        public int TypeSystem { get; set; } // Thuộc tính hệ thống hay thường
        public int OrderProperties { get; set; } // thứ tự thuộc tính
    }
}
