﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.PropertiesDirectory
{
    public enum TypeProperties
    {
        System = 1,
        Subsystem = 2,
        Normal = 3
    }
}
