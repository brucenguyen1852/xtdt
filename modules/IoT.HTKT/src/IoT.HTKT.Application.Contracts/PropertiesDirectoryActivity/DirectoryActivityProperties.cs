﻿using IoT.HTKT.Activity;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.PropertiesDirectoryActivity
{
    public class DirectoryActivityProperties : DirectoryActivityDto
    {
        public string NameTypeDirectory { get; set; } //Tên loại dữ liệu
        public string CodeTypeDirectory { get; set; } //Mã loại dữ liệu
        public int VerssionTypeDirectory { get; set; } //Phiên bản
        public string IdImplement { get; set; } // kế thừa
        public List<DetailPropertiesActivity> ListProperties { get; set; } // Bảnh thuộc tính
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
