﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.PropertiesDirectoryStatistic
{
    public class CreateUpdatePropertiesDirectoryStatisticDto
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public int Count { get; set; }
    }
}
