﻿using IoT.HTKT.PropertiesDirectory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.PropertiesDirectoryStatistic
{
    public interface IPropertiesDirectoryStatisticService :
        ICrudAppService< //Defines CRUD methods
            PropertiesDirectoryStatisticDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdatePropertiesDirectoryStatisticDto> //Used to create/update a PropertiesDirectory
    {
        Task<PropertiesDirectoryStatisticDto> GetAsyncById(string idDirectory);
        Task<List<DirectoryProperties>> GetListDirectoryStatistic(List<string> lstShow);
        //Task<List<PropertiesDirectoryStatisticDto>> GetListDirectoryAsync();

        //Task<bool> UpdateDirectoryByIdDirectory(string id);
    }
}
