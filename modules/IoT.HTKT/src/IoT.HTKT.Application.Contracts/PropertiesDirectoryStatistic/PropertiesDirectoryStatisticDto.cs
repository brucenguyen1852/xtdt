﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.HTKT.PropertiesDirectoryStatistic
{
    public class PropertiesDirectoryStatisticDto : AuditedEntityDto<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public int Count { get; set; }
    }
}
