﻿using IoT.Common;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Country
{
    /// <summary>
    /// service country
    /// </summary>
    public class CountryService : ApplicationService, ICountryService
    {
        //interface country
        private readonly ICountryRepository _countryRepository;
        /// <summary>
        /// constructors CountryService
        /// </summary>
        /// <param name="countryRepository"> country repository</param>
        public CountryService(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }
        /// <summary>
        /// create country
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Authorize]
        public async Task<bool> CreateCountry(List<CreateUpdateCountryDto> param)
        {
            try
            {
                var country = param.Clone<List<Country>>();
                await _countryRepository.InsertManyAsync(country);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// get country
        /// </summary>
        /// <param name="code">code country</param>
        /// <param name="level">level country</param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<List<CountryDto>> GetCountry(string code, int level)
        {
            var dto = new List<CountryDto>();
            var countries = new List<Country>();
            try
            {
                if (level > 1) //get theo level and code
                {
                    countries = await _countryRepository.GetListAsync(null, false, null, null, level, null, code, null, null, null);
                }
                else // get theo level
                {
                    countries = await _countryRepository.GetListAsync(null, false, null, null, level, null, null, null, null, null);
                }
                dto = countries.Clone<List<CountryDto>>();
                return dto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
