﻿using IoT.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace IoT.HTKT.Country
{
    /// <summary>
    /// class geojson country service
    /// </summary>
    public class GeojsonCountryService : ApplicationService, IGeojsonCountryService
    {
        //interface geojson country repository
        private readonly IGeojsonCountryRepository _geojsonCountryRepository;
        //interface country repository
        private readonly ICountryRepository _countryRepository;
        /// <summary>
        /// constructors geojson country service
        /// </summary>
        /// <param name="geojsonCountryRepository"></param>
        /// <param name="countryRepository"></param>
        public GeojsonCountryService(IGeojsonCountryRepository geojsonCountryRepository, ICountryRepository countryRepository)
        {
            _geojsonCountryRepository = geojsonCountryRepository;
            _countryRepository = countryRepository;
        }
        /// <summary>
        /// get geojson country by code
        /// </summary>
        /// <param name="code">code country</param>
        /// <returns></returns>
        public async Task<CountryGeoJsonDto> GetGeojsonCountryByCode(string code)
        {
            try
            {
                var obj = await _geojsonCountryRepository.FindAsync(x => x.CodeCountry == code);
                return ObjectMapper.Map<GeojsonCountry, CountryGeoJsonDto>(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// get children geojson country by code 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<List<CountryGeoJsonDto>> GetGeojsonCountryByCodeChildren(string code)
        {
            try
            {
                int level = code.Length + 3;
                var obj = await _geojsonCountryRepository.GetListAsync(x => x.CodeCountry.Length == level && x.CodeCountry.Contains(code));
                var result = ObjectMapper.Map<List<GeojsonCountry>, List<CountryGeoJsonDto>>(obj);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// insert geojson country
        /// </summary>
        /// <param name="listObj"></param>
        /// <returns></returns>
        public async Task<bool> UpdateGeometry(List<CreateUpdateGeojsonCountryDto> listObj)
        {
            try
            {
                List<GeojsonCountry> geojsonCountry = new List<GeojsonCountry>();
                foreach (var item in listObj)
                {
                    geojsonCountry.Add(new GeojsonCountry() { IdCountry = item.IdCountry, Geometry = item.Geometry, CodeCountry = item.CodeCountry });
                }
                await _geojsonCountryRepository.InsertManyAsync(geojsonCountry);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
