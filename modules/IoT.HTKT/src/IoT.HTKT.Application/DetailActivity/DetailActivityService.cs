﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.DetailActivity
{
    public class DetailActivityService : CrudAppService<
            IoT.HTKT.ManageActivity.DetailActivity, //The Book entity
            DetailActivityDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDetailActivityDto>, //Used to create/update a book
        IDetailActivityService //implement the IBookAppService
    {
        private IRepository<IoT.HTKT.ManageActivity.DirectoryActivity> _directoryActivities;
        public DetailActivityService(IRepository<IoT.HTKT.ManageActivity.DetailActivity, Guid> repository,
                                     IRepository<IoT.HTKT.ManageActivity.DirectoryActivity> directoryActivities) : base(repository)
        {
            _directoryActivities = directoryActivities;
        }

        public override async Task<DetailActivityDto> CreateAsync(CreateDetailActivityDto input)
        {
            IoT.HTKT.ManageActivity.DetailActivity activityDetail = new IoT.HTKT.ManageActivity.DetailActivity()
            {
                IdDirectory = input.IdDirectory,
                IdDirectoryActivity = input.IdDirectoryActivity,
                IdMainObject = input.IdMainObject,
                NameDirectoryActivity = input.NameDirectoryActivity,
                DateActivity = input.DateActivity,
                ListProperties = input.ListProperties
            };

            var obj = await Repository.InsertAsync(activityDetail);

            var result = obj.Clone<DetailActivityDto>();
            return result;
        }

        public override async Task<DetailActivityDto> UpdateAsync(Guid id, CreateDetailActivityDto input)
        {
            var activityDetail = await Repository.GetAsync(id);

            if (activityDetail == null)
            {
                return null;
            }

            activityDetail.IdDirectory = input.IdDirectory;
            activityDetail.IdDirectoryActivity = input.IdDirectoryActivity;
            activityDetail.IdMainObject = input.IdMainObject;
            activityDetail.NameDirectoryActivity = input.NameDirectoryActivity;
            activityDetail.DateActivity = input.DateActivity;
            activityDetail.ListProperties = input.ListProperties;

            var obj = await Repository.UpdateAsync(activityDetail);

            var result = obj.Clone<DetailActivityDto>();

            return result;
        }

        public async Task<List<InfoActivityDetailDto>> GetListDetailActivity(string idMainObject, string idDirectory, string idDirectoryActivity)
        {
            List<InfoActivityDetailDto> result = new List<InfoActivityDetailDto>();

            var activitydetails = new List<IoT.HTKT.ManageActivity.DetailActivity>();
            activitydetails = Repository.Where(x => (!string.IsNullOrEmpty(x.IdDirectoryActivity) && x.IdDirectoryActivity.Contains(idDirectoryActivity))
                                                && (!string.IsNullOrEmpty(x.IdDirectory) && x.IdDirectory.Contains(idDirectory))
                                                && (!string.IsNullOrEmpty(x.IdMainObject) && x.IdMainObject.Contains(idMainObject))
                                                ).ToList();
            result = activitydetails.Clone<List<InfoActivityDetailDto>>();

            foreach (var item in result)
            {
                var guiId = Guid.Parse(item.IdDirectoryActivity);
                var activity = await this._directoryActivities.FindAsync(x => x.Id == guiId);

                if (activity != null)
                {
                    item.NameTypeDirectoryActivity = activity.Name;
                }
            }
            return result.OrderByDescending(x => x.DateActivity).ToList();
        }

        public bool CheckExitsCode(string code, string idDirectoryAcitivty)
        {
            var result = true;
            var lstActivityDetail = this.Repository.Where(x => x.IdDirectoryActivity == idDirectoryAcitivty);

            if (lstActivityDetail.Count() > 0)
            {
                foreach (var item in lstActivityDetail)
                {
                    var lstDetail = lstActivityDetail.Where(x => x.ListProperties.Any(x => x.CodeProperties == code)).ToList();
                    if (lstDetail.Count() > 0)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}
