﻿using IoT.Common;
using IoT.HTKT.PropertiesDirectoryActivity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.Activity
{
    public class DirectoryActivityService : CrudAppService<
            IoT.HTKT.ManageActivity.DirectoryActivity, //The Book entity
            DirectoryActivityDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDirectoryActivityDto>, //Used to create/update a book
        IDirectoryActivityService //implement the IBookAppService
    {
        private readonly IRepository<IoT.HTKT.ManageActivity.DetailActivity, Guid> _detailActivities;
        private readonly IRepository<IoT.HTKT.ManageActivity.PropertiesDirectoryActivity> _propertiesDirectoryActivities;
        private readonly IRepository<IoT.HTKT.Layer.PropertiesDirectoryStatistic, Guid> _propertiesDirectoryStatistics;

        public DirectoryActivityService(IRepository<IoT.HTKT.ManageActivity.DirectoryActivity, Guid> repository,
                                        IRepository<IoT.HTKT.ManageActivity.PropertiesDirectoryActivity> propertiesDirectoryActivities,
                                        IRepository<IoT.HTKT.ManageActivity.DetailActivity, Guid> detailActivities,
                                        IRepository<IoT.HTKT.Layer.PropertiesDirectoryStatistic, Guid> propertiesDirectoryStatistics) : base(repository)
        {
            _propertiesDirectoryActivities = propertiesDirectoryActivities;
            _detailActivities = detailActivities;
            _propertiesDirectoryStatistics = propertiesDirectoryStatistics;
        }

        public override async Task<DirectoryActivityDto> CreateAsync(CreateDirectoryActivityDto dto)
        {
            var dir = new IoT.HTKT.ManageActivity.DirectoryActivity()
            {
                IsDeleted = dto.IsDeleted,
                Name = dto.Name,
                Level = dto.Level,
                ParentId = dto.ParentId,
                Search = dto.Search,
                Type = dto.Type,
                IdDirectory = dto.IdDirectory
            };
            var obj = await Repository.InsertAsync(dir);
            var result = ObjectMapper.Map<IoT.HTKT.ManageActivity.DirectoryActivity, DirectoryActivityDto>(obj);
            return result;
        }

        public async Task<bool> DeleteFolder(string id)
        {
            var list = await Repository.GetListAsync();

            var lstChildrent = list.FindAll(x => x.ParentId == id);
            foreach (var item in lstChildrent)
            {
                await DeleteFolder(item.Id.ToString());
            }

            var guidId = Guid.Parse(id);
            await Repository.DeleteAsync(x => x.Id == guidId);
            await _propertiesDirectoryActivities.DeleteAsync(x => x.IdDirectoryActivity == id);
            return true;
        }

        public async Task<List<DirectoryActivityProperties>> GetListDirectoryActivityProperties(string idDirectory)
        {
            List<DirectoryActivityProperties> result = new List<DirectoryActivityProperties>();
            var directory = new List<IoT.HTKT.ManageActivity.DirectoryActivity>();

            if (!string.IsNullOrEmpty(idDirectory))
            {
                directory = Repository.Where(x => x.IdDirectory.Contains(idDirectory)).ToList();
            }
            else
            {
                directory = await Repository.GetListAsync();
            }

            foreach (var item in directory)
            {
                DirectoryActivityProperties directoryProperties = item.Clone<DirectoryActivityProperties>();
                if (item.Type == "file")
                {
                    var guiID = Guid.Parse(directoryProperties.Id.ToString());
                    var obj = await _propertiesDirectoryActivities.FindAsync(x => x.IdDirectoryActivity == directoryProperties.Id.ToString());
                    if (obj != null)
                    {
                        directoryProperties.CodeTypeDirectory = obj.CodeTypeDirectory;
                        directoryProperties.VerssionTypeDirectory = obj.VerssionTypeDirectory;
                        directoryProperties.IdImplement = obj.IdImplement;
                        var listProperties = obj.ListProperties.Clone<List<IoT.HTKT.PropertiesDirectoryActivity.DetailPropertiesActivity>>();

                        directoryProperties.ListProperties = listProperties;
                    }
                    directoryProperties.Type = "file";
                }
                else
                {
                    directoryProperties.Type = "folder";
                }
                result.Add(directoryProperties);
            }
            //var jsonDirectory = JsonConvert.SerializeObject(directory);
            //var result = JsonConvert.DeserializeObject<List<DirectoryDto>>(jsonDirectory);
            return result;
        }

        public async Task<bool> UpdateName(string id, string name, string idDirectory)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var objdirectory = await Repository.FindAsync(x => x.Id == new Guid(id));
                if (objdirectory != null)
                {
                    objdirectory.Name = name.Trim();
                    objdirectory.IdDirectory = idDirectory;
                    var obj = await Repository.UpdateAsync(objdirectory);
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateNameFolder(string id, string name)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var objdirectory = await Repository.FindAsync(x => x.Id == new Guid(id));
                if (objdirectory != null)
                {
                    objdirectory.Name = name.Trim();
                    var obj = await Repository.UpdateAsync(objdirectory);
                    return true;
                }
            }
            return false;
        }

        public async Task<List<DirectoryActivityProperties>> GetListDirectoryActivityNotProperties(string idDirectory)
        {
            List<DirectoryActivityProperties> result = new List<DirectoryActivityProperties>();
            var directory = new List<IoT.HTKT.ManageActivity.DirectoryActivity>();

            if (!string.IsNullOrEmpty(idDirectory))
            {
                directory = Repository.Where(x => x.IdDirectory == idDirectory).ToList();
            }
            else
            {
                directory = await Repository.GetListAsync();
            }

            foreach (var item in directory)
            {
                DirectoryActivityProperties directoryProperties = item.Clone<DirectoryActivityProperties>();
                if (item.Type == "file")
                {
                    directoryProperties.Type = "file";
                    var lstId = new List<string>();
                    if (!string.IsNullOrEmpty(directoryProperties.IdDirectory))
                    {
                        if (directoryProperties.IdDirectory.IndexOf("[") >= 0)
                            lstId = JsonConvert.DeserializeObject<List<string>>(directoryProperties.IdDirectory);
                        else lstId.Add(directoryProperties.IdDirectory);

                    }
                    var idConvert = directoryProperties.Id.ToString("D");
                    //var obj = await _propertiesDirectoryActivities.GetAsync(x=> x.IdDirectoryActivity == directoryProperties.Id.ToString("D") || x.IdDirectoryActivity == directoryProperties.IdDirectory);
                    var obj = await _propertiesDirectoryActivities.FindAsync(x => x.IdDirectoryActivity == idConvert || lstId.Contains(x.IdDirectoryActivity));
                    if (obj != null)
                    {
                        directoryProperties.CodeTypeDirectory = obj.CodeTypeDirectory;
                    }
                }
                else
                {
                    directoryProperties.Type = "folder";
                }
                result.Add(directoryProperties);
            }

            return result.OrderByDescending(x => x.Type).ToList();
        }

        public async Task<bool> CheckDeleteActivity(string id)
        {
            var result = true;
            var dem = await _detailActivities.CountAsync(x => x.IdDirectoryActivity == id);
            if (dem == 0)
            {
                var chil = Repository.Where(x => x.ParentId == id);
                if (chil.Count() > 0)
                {
                    foreach (var item in chil)
                    {
                        var checkExits = await CheckDeleteActivity(item.Id.ToString());
                        if (!checkExits)
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        public async Task<List<DirectoryActivityProperties>> GetListCountDirectoryActivity()
        {
            List<DirectoryActivityProperties> result = new List<DirectoryActivityProperties>();

            List<DirectoryActivityProperties> lstTemp = new List<DirectoryActivityProperties>(); // danh sách tạm

            var lstLayer = _propertiesDirectoryStatistics.OrderByDescending(x => x.Count).Skip(0).Take(10).ToList();

            var activities = Repository.Where(x => x.Type == "file").OrderByDescending(x => x.CreationTime).ToList();

            foreach (var item in activities)
            {
                //    if (result.Count() == 10)
                //    {
                //        break;
                //    }
                try
                {
                    DirectoryActivityProperties directoryProperties = item.Clone<DirectoryActivityProperties>();
                    directoryProperties.Type = "file";
                    var lstId = new List<string>();
                    if (!string.IsNullOrEmpty(directoryProperties.IdDirectory))
                    {
                        if (directoryProperties.IdDirectory.IndexOf("[") >= 0)
                            lstId = JsonConvert.DeserializeObject<List<string>>(directoryProperties.IdDirectory);
                        else lstId.Add(directoryProperties.IdDirectory);

                    }
                    var idConvert = directoryProperties.Id.ToString("D");

                    var obj = await _propertiesDirectoryActivities.FindAsync(x => x.IdDirectoryActivity == idConvert || lstId.Contains(x.IdDirectoryActivity));
                    if (obj != null)
                    {
                        directoryProperties.CodeTypeDirectory = obj.CodeTypeDirectory;
                    }

                    if (lstLayer != null)
                    {
                        if (lstLayer.Count() == 0)
                        {
                            result.Add(directoryProperties);
                        }
                        else
                        {
                            var lstLayerId = new List<string>(); // danh sách id layer của loại hoạt động
                            if (!string.IsNullOrEmpty(item.IdDirectory))
                            {
                                lstLayerId = JsonConvert.DeserializeObject<List<string>>(item.IdDirectory);
                            }
                            var LayerCurrentId = lstLayer.Select(x => x.IdDirectory).ToList(); // danh sách id layer được sử dụng nhiều nhất
                            bool checkExits = LayerCurrentId.AsParallel().Any(el => lstLayerId.AsParallel().Contains(el));
                            if (checkExits)
                            {
                                result.Add(directoryProperties);
                            }
                        }
                    }

                    lstTemp.Add(directoryProperties);
                }
                catch (Exception ex)
                {

                }
            }

            if (result.Count() < 10)
            {
                var another = 10 - result.Count();

                var anotherActivities2 = lstTemp.Where(x => !result.Any(y => y.Id == x.Id));

                var anotherActivities = lstTemp.Where(x => !result.Any(y => y.Id == x.Id)).Take(another).ToList();
                result.AddRange(anotherActivities);
            }

            return result.OrderByDescending(x => x.Type).ToList();

        }
    }
}
