﻿using IoT.HTKT.Localization;
using Volo.Abp.Application.Services;

namespace IoT.HTKT
{
    public abstract class HTKTAppService : ApplicationService
    {
        protected HTKTAppService()
        {
            LocalizationResource = typeof(HTKTResource);
            ObjectMapperContext = typeof(HTKTApplicationModule);
        }
    }
}
