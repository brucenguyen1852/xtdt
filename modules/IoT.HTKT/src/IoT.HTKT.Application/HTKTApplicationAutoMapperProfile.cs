﻿using AutoMapper;
using IoT.HTKT.Activity;
using IoT.HTKT.DetailActivity;
using IoT.HTKT.Layer;
using IoT.HTKT.LayerBaseMap;
using IoT.HTKT.LocationSetting;
using IoT.HTKT.ManagementData;
using IoT.HTKT.PropertiesDefault;
using System.Collections.Generic;
using Volo.Abp.AutoMapper;

namespace IoT.HTKT
{
    public class HTKTApplicationAutoMapperProfile : Profile
    {
        public HTKTApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
            CreateMap<IoT.HTKT.Layer.PropertiesDefault, PropertiesDefaultDto>();
            CreateMap<Directory, DirectoryDto>();
            CreateMap<MainObject, MainObjectDto>();
            CreateMap<DocumentObject, DocumentObjectDTO>();
            //CreateMap<IoT.QTSC.Layer.PropertiesDirectory, PropertiesDirectoryDto>();

            CreateMap<IoT.HTKT.ManageActivity.DirectoryActivity, DirectoryActivityDto>();
            //CreateMap<IoT.QTSC.ManageActivity.PropertiesDirectoryActivity, PropertiesDirectoryActivityDto>();
            CreateMap<IoT.HTKT.ManageActivity.DetailActivity, DetailActivityDto>();

            CreateMap<IoT.HTKT.SettingData.LayerBaseMap, LayerBaseMapDto>();
            CreateMap<DocumentActivity, DocumentActivityDto>();

            CreateMap<IoT.HTKT.LocationSetting.LocationSetting, LocationSettingDto>();

            CreateMap<IoT.HTKT.Country.GeojsonCountry, IoT.HTKT.Country.CountryGeoJsonDto>();
        }
    }
}