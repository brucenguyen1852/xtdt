﻿using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace IoT.HTKT
{
    [DependsOn(
        typeof(HTKTDomainModule),
        typeof(HTKTApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class HTKTApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAutoMapperObjectMapper<HTKTApplicationModule>();
            context.Services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<HTKTApplicationModule>(validate: true);
            });
        }
    }
}
