﻿using IoT.Common;
using IoT.HTKT.ManagementData;
using IoT.HTKT.PropertiesDirectory;
using IoT.HTKT.PropertiesDirectoryShared;
using IoT.HTKT.PropertiesDirectoryStatistic;
using IoT.HTKT.PropertiesSetting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.Layer
{
    public class DirectoryService : CrudAppService<
            Directory, //The Book entity
            DirectoryDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDirectoryDto>, //Used to create/update a book
        IDirectoryService //implement the IBookAppService
    {
        List<Directory> listDelete = new List<Directory>();
        private readonly IPropertiesDirectoryAppService _propertiesDirectoryAppService;
        //private readonly IPropertiesDirectoryStatisticService _propertiesDirectoryStatisticService;
        private readonly IPropertiesSettingService _propertiesSettingService;

        private readonly IRepository<MainObject, Guid> _mainObjects;
        private readonly IRepository<IoT.HTKT.Layer.PropertiesDirectory, Guid> _propertiesDirectory;
        public DirectoryService(IRepository<Directory, Guid> repository, IPropertiesDirectoryAppService propertiesDirectoryAppService,
            /*IPropertiesDirectoryStatisticService propertiesDirectoryStatisticService,*/ IPropertiesSettingService propertiesSettingService,
                     IRepository<MainObject, Guid> mainObjects, IRepository<IoT.HTKT.Layer.PropertiesDirectory, Guid> propertiesDirectory) : base(repository)
        {
            //_authorRepository = authorRepository;
            //GetPolicyName = MapPermissions.Maps.Default;
            //GetListPolicyName = MapPermissions.Maps.Default;
            //CreatePolicyName = MapPermissions.Maps.Create;
            //UpdatePolicyName = MapPermissions.Maps.Edit;
            //DeletePolicyName = MapPermissions.Maps.Create;
            //_currentUser = currentUser;
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
            //_propertiesDirectoryStatisticService = propertiesDirectoryStatisticService;
            _propertiesSettingService = propertiesSettingService;
            _mainObjects = mainObjects;
            _propertiesDirectory = propertiesDirectory;
        }
        public async Task<List<DirectoryDto>> GetListDirectory()
        {
            var directory = await Repository.GetListAsync();
            var jsonDirectory = JsonConvert.SerializeObject(directory);
            var result = JsonConvert.DeserializeObject<List<DirectoryDto>>(jsonDirectory);
            return result;
        }
        public override async Task<DirectoryDto> CreateAsync(CreateDirectoryDto dto)
        {
            var dir = new Directory()
            {
                IsDeleted = dto.IsDeleted,
                Name = dto.Name,
                Level = dto.Level,
                ParentId = dto.ParentId,
                Search = dto.Search,
                Type = dto.Type
            };
            var obj = await Repository.InsertAsync(dir);
            var result = ObjectMapper.Map<Directory, DirectoryDto>(obj);
            return result;
        }

        public async Task<bool> DeleteFolder(string id)
        {
            try
            {
                var list = await Repository.GetListAsync();

                var lstChildrent = list.FindAll(x => x.ParentId == id);
                foreach (var item in lstChildrent)
                {
                    await DeleteFolder(item.Id.ToString());
                }

                var guidId = Guid.Parse(id);
                await Repository.DeleteAsync(x => x.Id == guidId); // xoá directory
                await _propertiesDirectoryAppService.DeleteDirectoryByIdDirectory(id); // xoá properties của directory

                //await _propertiesDirectoryAppService.DeleteAsync(guidId);

                return true;
            }
            catch
            {
                return true;
            }
        }

        public async Task<List<DirectoryProperties>> GetListDirectoryProperties()
        {
            List<DirectoryProperties> result = new List<DirectoryProperties>();
            var directory = await Repository.GetListAsync();
            foreach (var item in directory)
            {
                DirectoryProperties directoryProperties = item.Clone<DirectoryProperties>();
                if (item.Type == "file")
                {
                    var obj = await _propertiesDirectoryAppService.GetAsyncById(directoryProperties.Id.ToString("D"));
                    if (obj != null)
                    {
                        directoryProperties.CodeTypeDirectory = obj.CodeTypeDirectory;
                        directoryProperties.VerssionTypeDirectory = obj.VerssionTypeDirectory;
                        directoryProperties.IdImplement = obj.IdImplement;
                        directoryProperties.ListProperties = obj.ListProperties;

                        var image = await _propertiesSettingService.FindPropertyByIdDirection(obj.IdDirectory);
                        if (image != null)
                        {
                            directoryProperties.Image2D = image.Image2D;
                        }
                    }
                    directoryProperties.Type = "file";
                }
                else
                {
                    directoryProperties.Type = "folder";
                }
                result.Add(directoryProperties);
            }

            return result;
        }

        public async Task<List<DirectoryProperties>> GetListDirectoryPropertiesHasMap()
        {
            List<DirectoryProperties> result = new List<DirectoryProperties>();
            var directory = await Repository.GetListAsync();
            var directoryfolder = directory.Where(x => x.Type == "folder").ToList();
            result = directoryfolder.Clone<List<DirectoryProperties>>();
            directory = directory.Where(x => x.Type == "file").ToList();
            //List<DirectoryProperties> result = directoryfolder.Clone<List<DirectoryProperties>>();
            var listOther = await _propertiesDirectoryAppService.GetListDirectoryByOptionLayerAsync((int)OptionLayer.GeoserverOther);

            var re = directory.Where(x => listOther.All(y => y.IdDirectory != x.Id.ToString("D"))).ToList();
            var result1 = re.Clone<List<DirectoryProperties>>();
            result.AddRange(result1);

            return result.OrderByDescending(x => x.Type).ToList();
        }

        public async Task<bool> UpdateName(string id, string name)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var objdirectory = await Repository.FindAsync(x => x.Id == new Guid(id));
                if (objdirectory != null)
                {
                    objdirectory.Name = name.Trim();
                    var obj = await Repository.UpdateAsync(objdirectory);
                    return true;
                }
            }
            return false;
        }

        public async Task<List<DirectoryProperties>> GetListDirectoryNotProperties()
        {
            List<DirectoryProperties> result = new List<DirectoryProperties>();
            var directory = await Repository.GetListAsync();
            result = directory.Clone<List<DirectoryProperties>>();

            return result.OrderByDescending(x => x.Type).ToList();
        }

        public async Task<bool> CheckDeleteLayer(string id)
        {
            var result = true;
            var dem = await _mainObjects.CountAsync(x => x.IdDirectory == id);
            if (dem == 0)
            {
                var chil = Repository.Where(x => x.ParentId == id);
                if (chil.Count() > 0)
                {
                    foreach (var item in chil)
                    {
                        var checkExits = await CheckDeleteLayer(item.Id.ToString());
                        if (!checkExits)
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        public List<DirectoryDto> GetParentLayer(string id)
        {
            var lstResult = new List<DirectoryDto>();

            var lstParent = new List<Directory>();
            var guiId = Guid.Parse(id);
            var layer = this.Repository.ToList().Find(x => x.Id == guiId);
            if (layer != null)
            {
                lstParent.Add(layer);
                lstParent = DQGetParentLayer(layer.ParentId, lstParent);
            }

            lstResult = lstParent.Clone<List<DirectoryDto>>();
            return lstResult.OrderBy(x => x.Level).ToList();
        }

        private List<Directory> DQGetParentLayer(string id, List<Directory> lstLayer)
        {
            try
            {
                var lstParent = lstLayer;
                var guiId = Guid.Parse(id);
                var layer = this.Repository.ToList().Find(x => x.Id == guiId);
                if (layer != null)
                {
                    lstParent.Add(layer);
                    lstParent = DQGetParentLayer(layer.ParentId, lstParent);
                }

                return lstParent;
            }

            catch (Exception ex)
            {
                return lstLayer;
            }
        }

        public async Task<DirectoryDto> FinDirectoryById(string id)
        {
            try
            {
                if (id.Length > 30)
                {
                    var guiId = Guid.Parse(id);
                    Directory obj = await Repository.FindAsync(x => x.Id == guiId);
                    return obj != null ? ObjectMapper.Map<Directory, DirectoryDto>(obj) : null;
                }
                return null;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<List<DirectoryDto>> FindFileDirectory()
        {
            try
            {
                var directory = Repository.Where(x => x.Type == "file").ToList();
                var jsonDirectory = JsonConvert.SerializeObject(directory);
                return JsonConvert.DeserializeObject<List<DirectoryDto>>(jsonDirectory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
