﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.Layer
{
    public class LayerDataTypeService : CrudAppService<
            LayerDataType, //The Book entity
            LayerDataTypeDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateLayerDataTypeDto>, //Used to create/update a book
        ILayerDataTypeService //implement the IBookAppService
    {
        public LayerDataTypeService(IRepository<LayerDataType, Guid> repository) : base(repository)
        {
        }

        public override async Task<LayerDataTypeDto> CreateAsync(CreateLayerDataTypeDto input)
        {
            IoT.HTKT.Layer.LayerDataType layerDataType = new Layer.LayerDataType();
            IoT.HTKT.Layer.LayerDataType layerData = new IoT.HTKT.Layer.LayerDataType()
            {
                Value = input.Value.Trim(),
                Text = input.Text.Trim(),
                IsDeleted = false
            };
            layerDataType = await Repository.InsertAsync(layerData);

            var json2 = JsonConvert.SerializeObject(layerDataType);
            var result = JsonConvert.DeserializeObject<LayerDataTypeDto>(json2);
            return result;
        }

        public async Task<List<LayerDataTypeDto>> GetListDataTypeAsync()
        {
            List<LayerDataTypeDto> layerDataTypeDtoDtos = new List<LayerDataTypeDto>();

            var objdirectory = await Repository.GetListAsync();
            if (objdirectory != null)
            {
                var json = JsonConvert.SerializeObject(objdirectory);
                layerDataTypeDtoDtos = JsonConvert.DeserializeObject<List<LayerDataTypeDto>>(json);
            }

            return layerDataTypeDtoDtos;
        }
    }
}
