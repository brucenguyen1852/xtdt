﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.LayerBaseMap
{
    public class LayerBaseMapService : CrudAppService<
            IoT.HTKT.SettingData.LayerBaseMap, //The Book entity
            LayerBaseMapDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateLayerBaseMapDto>, //Used to create/update a book
        ILayerBaseMapService //implement the IBookAppService
    {
        public LayerBaseMapService(IRepository<IoT.HTKT.SettingData.LayerBaseMap, Guid> repository
                                      ) : base(repository)
        {
        }

        public override async Task<LayerBaseMapDto> CreateAsync(CreateUpdateLayerBaseMapDto input)
        {
            var order = await this.Repository.CountAsync();
            var data = new IoT.HTKT.SettingData.LayerBaseMap()
            {
                NameBasMap = input.NameBasMap,
                Link = input.Link,
                Image = input.Image,
                Order = order + 1
            };

            var obj = await Repository.InsertAsync(data);

            var result = obj.Clone<LayerBaseMapDto>();
            return result;
        }

        public override async Task<LayerBaseMapDto> UpdateAsync(Guid id, CreateUpdateLayerBaseMapDto input)
        {
            var currentData = await Repository.GetAsync(id);

            if (currentData == null)
            {
                return null;
            }

            currentData.NameBasMap = input.NameBasMap;
            currentData.Link = input.Link;
            //currentData.Order = input.Order;
            currentData.Image = input.Image;

            var obj = await Repository.UpdateAsync(currentData);

            var result = obj.Clone<LayerBaseMapDto>();

            return result;
        }

        public async Task<object> GetListLayerBaseMap(string Sorting, int SkipCount, int MaxResultCount)
        {
            var lstResult = new List<LayerBaseMapDto>();

            var lstObject = new List<IoT.HTKT.SettingData.LayerBaseMap>();
            if (MaxResultCount > 0)
            {
                lstObject = Repository.Skip(SkipCount).Take(MaxResultCount).ToList();
            }
            else
            {
                lstObject = Repository.OrderBy(x => x.Order).ToList();
            }

            var total = await Repository.CountAsync();

            lstObject = lstObject.OrderBy(x => x.Order).ToList();

            lstResult = lstObject.Clone<List<LayerBaseMapDto>>();

            var obj = new
            {
                totalCount = total,
                items = lstResult
            };

            return obj;
        }

        public bool CheckExitsOrder(Guid id, int order)
        {
            var countLayerBaseMap = this.Repository.Count(x => x.Id != id && x.Order == order);
            if (countLayerBaseMap > 0)
            {
                return false;
            }

            return true;
        }

    }
}
