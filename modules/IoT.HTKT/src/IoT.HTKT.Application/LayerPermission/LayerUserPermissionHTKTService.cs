﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.LayerPermission
{
    public class LayerUserPermissionHTKTService : CrudAppService<
           LayerUserPermission, //The LayerUserPermission entity
           LayerUserPermissionDto, //Used to show LayerUserPermission
           Guid, //Primary key of the LayerUserPermission entity
           PagedAndSortedResultRequestDto, //Used for paging/sorting
           CreateLayerUserPermissionDto>, //Used to create/update a LayerUserPermission
       ILayerUserPermissionHTKTService //implement the ILayerUserPermissionService
    {

        public LayerUserPermissionHTKTService(IRepository<LayerUserPermission, Guid> repository) : base(repository)
        {
        }

        public override async Task<LayerUserPermissionDto> CreateAsync(CreateLayerUserPermissionDto input)
        {
            var data = new LayerUserPermission()
            {
                UserId = input.UserId,
                RoleId = input.RoleId,
                IdDirectory = input.IdDirectory,
                ListPermisson = input.ListPermisson
            };

            var obj = await Repository.InsertAsync(data);

            var result = obj.Clone<LayerUserPermissionDto>();
            return result;
        }

        public async Task<List<LayerUserPermissionDto>> GetListOfUserOrRole(string UserId, List<string> RoleId)
        {
            var lstResult = new List<LayerUserPermissionDto>();

            var lst = new List<LayerUserPermission>();
            if (!string.IsNullOrWhiteSpace(UserId) && RoleId.Count > 0)
            {
                lst = Repository.Where(x => x.UserId == UserId || RoleId.Contains(x.RoleId)).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(UserId))
            {
                lst = Repository.Where(x => x.UserId == UserId).ToList();
            }
            else if (RoleId.Count > 0)
            {
                lst = Repository.Where(x => RoleId.Contains(x.RoleId)).ToList();
            }
            lstResult = lst.Clone<List<LayerUserPermissionDto>>();
            return lstResult;

        }

        public override async Task<LayerUserPermissionDto> UpdateAsync(Guid id, CreateLayerUserPermissionDto input)
        {
            try
            {
                var currentData = await Repository.GetAsync(id);

                if (currentData == null)
                {
                    return null;
                }

                currentData.UserId = input.UserId;
                currentData.RoleId = input.RoleId;
                currentData.IdDirectory = input.IdDirectory;
                currentData.ListPermisson = input.ListPermisson;

                var obj = await Repository.UpdateAsync(currentData);

                var result = obj.Clone<LayerUserPermissionDto>();

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
