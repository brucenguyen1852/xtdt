﻿using IoT.Common;
using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.LocationSetting
{
    public class LocationSettingService : CrudAppService<
            LocationSetting, //The Book entity
            LocationSettingDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateLocationSettingDto>, //Used to create/update a book
        ILocationSettingService //implement the IBookAppService
    {
        public LocationSettingService(IRepository<LocationSetting, Guid> repository
                                ) : base(repository)
        {
        }

        public async Task<LocationSettingDto> CreateLocationCenter(CreateUpdateLocationSettingDto dto)
        {
            if (dto.Latitude != 0 && dto.Longitude != 0)
            {
                var record = this.Repository.ToList();
                if (record.Count() > 0)
                {
                    var locationCenter = record.FirstOrDefault();
                    locationCenter.Latitude = dto.Latitude;
                    locationCenter.Longitude = dto.Longitude;

                    var updateLocation = await this.Repository.UpdateAsync(locationCenter);

                    if (updateLocation != null)
                    {
                        LocationCenter.Lat = dto.Latitude;
                        LocationCenter.Lng = dto.Longitude;
                    }

                    return updateLocation.Clone<LocationSettingDto>();
                }

                var data = new LocationSetting()
                {
                    Latitude = dto.Latitude,
                    Longitude = dto.Longitude
                };

                var createLocation = await this.Repository.InsertAsync(data);

                if (createLocation != null)
                {
                    LocationCenter.Lat = dto.Latitude;
                    LocationCenter.Lng = dto.Longitude;
                }

                return createLocation.Clone<LocationSettingDto>();
            }

            return null;
        }

        public async Task<LocationSettingDto> UpdateLocationCenter(Guid id, CreateUpdateLocationSettingDto dto)
        {
            if (dto.Latitude != 0 && dto.Longitude != 0)
            {
                var locationCenter = await this.Repository.GetAsync(id);

                if (locationCenter != null)
                {
                    locationCenter.Latitude = dto.Latitude;
                    locationCenter.Longitude = dto.Longitude;

                    var result = await this.Repository.UpdateAsync(locationCenter);

                    if (result != null)
                    {
                        LocationCenter.Lat = dto.Latitude;
                        LocationCenter.Lng = dto.Longitude;
                    }

                    return result.Clone<LocationSettingDto>(); ;
                }
            }

            return null;
        }

        public LocationSettingDto GetLocationCenter()
        {
            LocationSettingDto center = new LocationSettingDto();

            if (LocationCenter.Lat != 0 && LocationCenter.Lng != 0)
            {
                center.Latitude = LocationCenter.Lat;
                center.Longitude = LocationCenter.Lng;

                return center;
            }

            var getLocation = this.Repository.ToList().FirstOrDefault();

            if (getLocation != null)
            {
                center.Latitude = getLocation.Latitude;
                center.Longitude = getLocation.Longitude;

                LocationCenter.Lat = getLocation.Latitude;
                LocationCenter.Lng = getLocation.Longitude;

                return center;
            }

            center.Latitude = 16.058567;
            center.Longitude = 108.060950;

            return center;
        }
    }
}
