﻿using IoT.Common;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.ManagementData
{
    [Authorize]
    public class DetailObjectService :
        CrudAppService<
            DetailObject, //The DetailObject entity
            DetailObjectDto, //Used to show DetailObject
            Guid, //Primary key of the DetailObject entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateDetailObjectDto>, //Used to create/update a DetailObject
        IDetailObjectService //implement the MainObjeIMainObjectServicect
    {
        public DetailObjectService(IRepository<DetailObject, Guid> repository)
            : base(repository)
        {

        }

        public override async Task<DetailObjectDto> CreateAsync(CreateUpdateDetailObjectDto input)
        {
            DetailObject detailObject = new DetailObject()
            {
                IdMainObject = input.IdMainObject,
                ListProperties = input.ListProperties,
                IdDirectory = input.IdDirectory
            };

            var obj = await Repository.InsertAsync(detailObject);

            var result = obj.Clone<DetailObjectDto>();
            return result;
        }

        public override async Task<DetailObjectDto> UpdateAsync(Guid id, CreateUpdateDetailObjectDto input)
        {
            var detailObject = await Repository.GetAsync(id);

            if (detailObject == null)
            {
                return null;
            }

            detailObject.IdMainObject = input.IdMainObject;
            detailObject.ListProperties = input.ListProperties;
            detailObject.IdDirectory = input.IdDirectory;

            var obj = await Repository.UpdateAsync(detailObject);

            var result = obj.Clone<DetailObjectDto>();

            return result;
        }

        public override Task DeleteAsync(Guid id)
        {
            return base.DeleteAsync(id);
        }

        public async Task<DetailObjectDto> GetDetailObjectByMainObjectId(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            var details = this.Repository.Where(x => x.IdMainObject == id).FirstOrDefault();
            //var detail = details.FindAll(x => x.IdMainObject == id).FirstOrDefault();

            var result = details.Clone<DetailObjectDto>();

            return result;
        }

        public bool CheckDeleteProperties(List<string> mainId, string code)
        {
            var result = true;

            if (mainId.Count() > 0)
            {
                var lstDetail = this.Repository.Where(x => mainId.Contains(x.IdMainObject) && x.ListProperties.Any(x => x.CodeProperties == code)).ToList();
                if (lstDetail.Count() > 0)
                {
                    result = false;
                }
            }

            return result;
        }

        public async Task<MainObjectDto> UpdateExtendMainObject(FormManagementDataModel dto)
        {
            var dtoResult = new MainObjectDto();

            var mainObject = await Repository.FindAsync(x => x.IdMainObject == dto.Id);

            if (mainObject == null)
            {
                return null;
            }
            foreach (var item in dto.ListProperties)
            {
                switch (item.TypeProperties)
                {
                    case "image":
                        var image = mainObject.ListProperties.Find(x => x.TypeProperties == "image");
                        if (image != null)
                        {
                            image.DefalutValue = item.DefalutValue;
                        }
                        else
                            mainObject.ListProperties.Add(item);
                        break;
                    case "link":
                        var link = mainObject.ListProperties.Find(x => x.TypeProperties == "link");
                        if (link != null)
                        {
                            link.DefalutValue = item.DefalutValue;
                        }
                        else
                            mainObject.ListProperties.Add(item);
                        break;
                    default:
                        break;
                }
            }

            var obj = await this.Repository.UpdateAsync(mainObject);
            dtoResult = obj.Clone<MainObjectDto>();

            return dtoResult;
        }

        public async Task<MainObjectDto> UpdatePropertiesMainObject(FormManagementDataModel dto)
        {
            var dtoResult = new MainObjectDto();

            var mainObject = await Repository.FindAsync(x => x.IdMainObject == dto.Id);

            if (mainObject == null)
            {
                return null;
            }

            var lstFile = mainObject.ListProperties.FindAll(x => x.TypeProperties == "image" || x.TypeProperties == "file" || x.TypeProperties == "link");

            foreach (var item in lstFile)
            {
                var index = dto.ListProperties.FindIndex(x => x.TypeProperties == item.TypeProperties);
                if (index != -1)
                {
                    dto.ListProperties.ElementAt(index).DefalutValue = item.DefalutValue;
                }
            }
            mainObject.ListProperties = dto.ListProperties;
            var obj = await this.Repository.UpdateAsync(mainObject);
            dtoResult = obj.Clone<MainObjectDto>();

            return dtoResult;
        }
    }
}
