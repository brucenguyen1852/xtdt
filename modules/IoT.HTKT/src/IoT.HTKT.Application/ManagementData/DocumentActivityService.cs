﻿
using IoT.HTKT.ManagementData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.ManagementData
{
    public class DocumentActivityService : CrudAppService<
            IoT.HTKT.ManagementData.DocumentActivity, //The DetailObject entity
            DocumentActivityDto, //Used to show DetailObject
            Guid, //Primary key of the DetailObject entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateOrUpdateDocumentActivityDto>,
            IDocumentActivityService
    {
        public DocumentActivityService(IRepository<DocumentActivity, Guid> repository)
            : base(repository)
        {

        }

        public async Task<List<DocumentActivityDto>> GetAllFile()
        {
            var listFile = await Repository.GetListAsync();
            var jsonFile = JsonConvert.SerializeObject(listFile);
            var result = JsonConvert.DeserializeObject<List<DocumentActivityDto>>(jsonFile);
            return result;
        }

        public async override Task<DocumentActivityDto> CreateAsync(CreateOrUpdateDocumentActivityDto input)
        {
            var obj = new DocumentActivity();
            var objectz = await Repository.FindAsync(x => x.Id == new Guid(input.Id));

            if (objectz != null)
            {
                objectz.IdMainObject = input.IdMainObject;
                objectz.NameDocument = input.NameDocument;
                objectz.IconDocument = input.IconDocument;
                objectz.UrlDocument = input.UrlDocument;
                objectz.NameFolder = input.NameFolder;
                objectz.OrderDocument = input.OrderDocument;
                objectz.IdActivity = input.IdActivity;
                obj = await Repository.UpdateAsync(objectz);
            }
            else
            {
                if (input.OrderDocument == 0)
                {
                    var check = Repository.Where(x => x.NameFolder == input.NameFolder && x.IdMainObject == input.IdMainObject)
                                    .OrderByDescending(x => x.OrderDocument).FirstOrDefault();
                    if (check != null)
                    {
                        input.OrderDocument = check.OrderDocument + 1;
                    }
                    else
                    {
                        input.OrderDocument = 1;
                    }
                }
                var file = new DocumentActivity()
                {
                    IdMainObject = input.IdMainObject,
                    NameDocument = input.NameDocument,
                    IconDocument = input.IconDocument,
                    UrlDocument = input.UrlDocument,
                    NameFolder = input.NameFolder,
                    OrderDocument = input.OrderDocument,
                    IdActivity = input.IdActivity
                };
                obj = await Repository.InsertAsync(file);
            }
            var jsonFile = JsonConvert.SerializeObject(obj);
            //var result = ObjectMapper.Map<DocumentActivity, DocumentActivityDto>(obj);
            var result = JsonConvert.DeserializeObject<DocumentActivityDto>(jsonFile);
            return result;
        }

        public async Task<List<DocumentActivityDto>> getListDocumentbyObjectMainId(Guid objectMainId)
        {
            //var listAllFile = await Repository.GetListAsync();

            var listFileForObjectId = Repository.Where(x => x.IdMainObject == objectMainId.ToString()).ToList();

            var jsonFile = JsonConvert.SerializeObject(listFileForObjectId);
            var result = JsonConvert.DeserializeObject<List<DocumentActivityDto>>(jsonFile);
            return result;
        }

        public async Task<List<DocumentActivityDto>> getListDocumentbyActivityId(string activityId)
        {
            var listFileForObjectId = Repository.Where(x => x.IdActivity == activityId).ToList();

            var jsonFile = JsonConvert.SerializeObject(listFileForObjectId);
            var result = JsonConvert.DeserializeObject<List<DocumentActivityDto>>(jsonFile);
            return result;
        }
    }
}
