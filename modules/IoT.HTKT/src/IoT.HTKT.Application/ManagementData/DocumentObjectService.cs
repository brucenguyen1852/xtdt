﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Volo.Abp.Application.Services;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using IoT.Common;
using Newtonsoft.Json;

namespace IoT.HTKT.ManagementData
{
    public class DocumentObjectService : CrudAppService<
            DocumentObject, //The DetailObject entity
            DocumentObjectDTO, //Used to show DetailObject
            Guid, //Primary key of the DetailObject entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateOrUpdateDocumentObjectDto>,
            IDocumentObjectService
    {
        public DocumentObjectService(IRepository<DocumentObject, Guid> repository)
            : base(repository)
        {

        }

        public async Task<List<DocumentObjectDTO>> GetAllFile()
        {
            var listFile = await Repository.GetListAsync();
            var jsonFile = JsonConvert.SerializeObject(listFile);
            var result = JsonConvert.DeserializeObject<List<DocumentObjectDTO>>(jsonFile);
            return result;
        }

        public async override Task<DocumentObjectDTO> CreateAsync(CreateOrUpdateDocumentObjectDto input)
        {
            var obj = new DocumentObject();
            var objectz = await Repository.FindAsync(x => x.Id == new Guid(input.Id));

            if (objectz != null)
            {
                objectz.IdMainObject = input.IdMainObject;
                objectz.NameDocument = input.NameDocument;
                objectz.IconDocument = input.IconDocument;
                objectz.UrlDocument = input.UrlDocument;
                objectz.NameFolder = input.NameFolder;
                objectz.OrderDocument = input.OrderDocument;
                obj = await Repository.UpdateAsync(objectz);
            }
            else
            {
                if (input.OrderDocument == 0)
                {
                    var check = Repository.Where(x => x.NameFolder == input.NameFolder && x.IdMainObject == input.IdMainObject)
                                    .OrderByDescending(x => x.OrderDocument).FirstOrDefault();
                    if (check != null)
                    {
                        input.OrderDocument = check.OrderDocument + 1;
                    }
                    else
                    {
                        input.OrderDocument = 1;
                    }
                }
                var file = new DocumentObject()
                {
                    IdMainObject = input.IdMainObject,
                    NameDocument = input.NameDocument,
                    IconDocument = input.IconDocument,
                    UrlDocument = input.UrlDocument,
                    NameFolder = input.NameFolder,
                    OrderDocument = input.OrderDocument
                };
                obj = await Repository.InsertAsync(file);
            }
            //var result = ObjectMapper.Map<DocumentObject, DocumentObjectDTO>(obj);
            var jsonFile = JsonConvert.SerializeObject(obj);
            var result = JsonConvert.DeserializeObject<DocumentObjectDTO>(jsonFile);
            return result;
        }

        public async Task<List<DocumentObjectDTO>> getListDocumentbyObjectMainId(Guid objectMainId)
        {
            //var listAllFile = await Repository.GetListAsync();

            var listFileForObjectId = Repository.Where(x => x.IdMainObject == objectMainId.ToString()).ToList();

            var jsonFile = JsonConvert.SerializeObject(listFileForObjectId);
            var result = JsonConvert.DeserializeObject<List<DocumentObjectDTO>>(jsonFile);
            return result;
        }

        public async Task<bool> DeleteListObject(List<string> id)
        {
            foreach (var item in id)
            {
                Guid newGuid = Guid.Parse(item);
                var document = await Repository.FirstAsync(x => x.Id == newGuid);
                await Repository.DeleteAsync(document);
            }
            return true;
        }

        public async Task<List<DocumentObjectDTO>> UpdateFileDocumentMainObject(UpdateDocumentMainObjectFormData input)
        {
            var lstOldFile = this.Repository.Where(x => x.IdMainObject == input.idMainObject);
            if (lstOldFile.Count() > 0)
            {
                await this.Repository.DeleteManyAsync(lstOldFile);
            }

            var lstDoc = new List<DocumentObject>();
            foreach (var item in input.data)
            {
                var document = new DocumentObject()
                {
                    NameDocument = item.NameDocument,
                    IdMainObject = item.IdMainObject,
                    IconDocument = item.IconDocument,
                    NameFolder = item.NameFolder,
                    UrlDocument = item.UrlDocument,
                    OrderDocument = item.OrderDocument
                };
                lstDoc.Add(document);
            }

            if (lstDoc.Count() > 0)
            {
                await this.Repository.InsertManyAsync(lstDoc);
            }

            var reuslt = lstDoc.Clone<List<DocumentObjectDTO>>();

            return reuslt;
        }
    }
}
