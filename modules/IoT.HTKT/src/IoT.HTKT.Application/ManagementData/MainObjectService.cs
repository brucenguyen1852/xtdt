﻿using IoT.Common;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Domain.Repositories;
using System.Data;
using System.IO;
using IoT.HTKT.Object;
using QRCoder;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.Globalization;
using Microsoft.AspNetCore.Authorization;
using System.Text.RegularExpressions;
using IoT.HTKT.Common;
using Newtonsoft.Json;
using System.Linq.Dynamic.Core;
using IoT.HTKT.PropertiesDirectoryActivity;
using IoT.HTKT.PropertiesDirectory;
using IoT.HTKT.PropertiesDefault;
using IoT.Geoserver.LayerGeoserver;
using Microsoft.Extensions.Configuration;
using IoT.Common.CacheMemory;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using IoT.HTKT.Layer;
using Microsoft.Extensions.Hosting;

namespace IoT.HTKT.ManagementData
{
    [Authorize]
    public class MainObjectService :
        CrudAppService<
            MainObject, //The MainObject entity
            MainObjectDto, //Used to show MainObject
            Guid, //Primary key of the MainObject entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateMainObjectDto>, //Used to create/update a MainObject
        IMainObjectService //implement the MainObjeIMainObjectServicect
    {
        public ILogger<MainObjectService> Logger { get; set; }

        private IConverter _converter;
        private readonly IRepository<DetailObject, Guid> _detailObjects;
        private readonly IPropertiesDirectoryAppService _propertiesDirectoryAppService;
        public IMongoCollection<MainObject> Collection { get; }
        private readonly ILayerToLayerService _layerToLayerService;
        private readonly IConfiguration _configuration;
        private readonly CacheMemoryDistributedService _cacheMemoryDistributedService;
        private readonly IDistributedCache _distributedCache;
        private readonly IDirectoryService _directoryService;
        private readonly IHostingEnvironment hostingEnvironment;
        public MainObjectService(IRepository<MainObject, Guid> repository,
                                 IRepository<DetailObject, Guid> detailObjects,
                                 IPropertiesDirectoryAppService propertiesDirectoryAppService,
                                 IConverter converter, ILayerToLayerService layerToLayerService,
                                  IConfiguration configuration, IDistributedCache distributedCache,
                                  IDirectoryService directoryService,
                                  IHostingEnvironment hostingEnvironment)
            : base(repository)
        {
            _detailObjects = detailObjects;
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
            _converter = converter;
            _layerToLayerService = layerToLayerService;
            _configuration = configuration;
            _distributedCache = distributedCache;
            _cacheMemoryDistributedService = new CacheMemoryDistributedService(_distributedCache);
            Logger = NullLogger<MainObjectService>.Instance;
            _directoryService = directoryService;
            this.hostingEnvironment = hostingEnvironment;
        }

        public override async Task<MainObjectDto> CreateAsync(CreateUpdateMainObjectDto input)
        {
            input.Geometry.Coordinates = Extensions.ConvertToListObject(input.Geometry.Coordinates);

            MainObject mainObject = new MainObject()
            {
                IdDirectory = input.IdDirectory,
                NameObject = input.NameObject,
                Geometry = input.Geometry,
                Properties = input.PropertiesGeojson,
                object3D = input.object3D,
                IsCheckImplementProperties = input.IsCheckImplementProperties,
                //QRCodeObject = input.QRCodeObject,
                SearchObject = CommonMethod.ConvertToUnSign(input.NameObject.ToLower().Trim()),
                District = input.District,
                WardDistrict = input.WardDistrict
            };

            var obj = await Repository.InsertAsync(mainObject);
            //add obj map tile geoserver
            await AddObjectMapTile(obj);
            //remove cache object
            await RemoveObjectOnCache(input.IdDirectory);
            var result = obj.Clone<MainObjectDto>();
            return result;
        }

        public override async Task<MainObjectDto> UpdateAsync(Guid id, CreateUpdateMainObjectDto input)
        {

            input.Geometry.Coordinates = Extensions.ConvertToListObject(input.Geometry.Coordinates);

            var mainObject = await Repository.GetAsync(id);

            if (mainObject == null)
            {
                return null;
            }

            mainObject.IdDirectory = input.IdDirectory;
            mainObject.NameObject = input.NameObject;
            mainObject.Geometry = input.Geometry;
            mainObject.Properties = input.PropertiesGeojson;
            mainObject.object3D = input.object3D;
            mainObject.IsCheckImplementProperties = input.IsCheckImplementProperties;
            mainObject.QRCodeObject = input.QRCodeObject;
            mainObject.SearchObject = CommonMethod.ConvertToUnSign(input.NameObject.ToLower().Trim());
            var obj = await Repository.UpdateAsync(mainObject);
            if (obj != null)
            {
                await UpdateObjectMaptile(obj);
            }
            var result = obj.Clone<MainObjectDto>();
            //remove cache object
            await RemoveObjectOnCache(input.IdDirectory);
            return result;
        }
        public override Task DeleteAsync(Guid id)
        {
            var mainObject = Repository.FirstOrDefault(x => x.Id == id);
            if (mainObject != null)
            {
                DeleteObjectMaptile(mainObject);
            }
            return base.DeleteAsync(id);
        }

        //public override async Task DeleteAsync(Guid id)
        //{
        //    var mainObject = await Repository.GetAsync(id);
        //    var check = base.DeleteAsync(id);
        //    await DeleteObjectMaptile(mainObject);
        //    return check;
        //}

        public object GetListObject(DataTableViewModel dataTableViewModel)
        {
            var lstResult = new List<InfoObjectData>();

            var total = 0;
            List<MainObject> lstObject = new List<MainObject>();

            lstObject = Repository.Where(x => x.IdDirectory == dataTableViewModel.search.regex).ToList();
            total = lstObject.Count();

            if (dataTableViewModel.search != null && !string.IsNullOrEmpty(dataTableViewModel.search.value))
            {
                if (string.IsNullOrEmpty(dataTableViewModel.Code))
                {
                    //lstObject = lstObject.Where(delegate (MainObject x)
                    //{
                    //    return (Extensions.ConvertToUnSign(x.NameObject.Trim().ToLower()).IndexOf(dataTableViewModel.search.value.Trim().ToLower(), StringComparison.CurrentCultureIgnoreCase) >= 0
                    //        || x.NameObject.Trim().ToLower().Contains(dataTableViewModel.search.value.Trim().ToLower())) && (x.IdDirectory == dataTableViewModel.search.regex);
                    //}).ToList();
                    lstObject = lstObject.Where(x => !string.IsNullOrEmpty(x.NameObject) && x.SearchObject.Contains(dataTableViewModel.search.value)).ToList();
                    total = lstObject.Count(); //lstObject.Count();
                                               //lstObject = lstObject.Skip(dataTableViewModel.start).Take(dataTableViewModel.length).ToList();
                }
                else
                {
                    var lstMainId = lstObject.ToList().Select(x => x.Id.ToString());
                    switch (dataTableViewModel.TypeProperties)
                    {
                        case "text":
                        case "string":
                        case "stringlarge":

                            //**************** tìm kiếm tuyệt đối
                            var uiactivitydetails = this._detailObjects.Where(x => (lstMainId != null && lstMainId.Contains(x.IdMainObject))
                                  && x.ListProperties.Any(y => y.CodeProperties == dataTableViewModel.Code
                                  && !string.IsNullOrEmpty(y.DefalutValue)
                                  && y.DefalutValue.ToLower().Contains(dataTableViewModel.search.value.Trim().ToLower())
                                  //|| CommonMethod.ConvertToUnSign(y.DefalutValue.ToLower()).Contains(search.ToLower())
                                  ));

                            //************* tìm kiếm tương đối
                            //var uiactivitydetails = this._detailObjects.Where(
                            //    delegate (DetailObject x)
                            //    {
                            //        return (
                            //                lstMainId != null && lstMainId.Contains(x.IdMainObject)
                            //                && x.ListProperties.Any(y => y.CodeProperties == code
                            //                && !string.IsNullOrEmpty(y.DefalutValue)
                            //                && CommonMethod.ConvertToUnSign(y.DefalutValue.ToLower()).IndexOf(search.ToLower(), StringComparison.CurrentCultureIgnoreCase) >= 0
                            //                )
                            //            );
                            //    }).Skip(skipnumber).Take(countnumber);

                            total = uiactivitydetails.Count();
                            lstMainId = uiactivitydetails.Select(x => x.IdMainObject).ToList();
                            break;
                        case "float":
                        case "int":
                            var uiactivitydetailsNumber = this._detailObjects.Where(x => (lstMainId != null && lstMainId.Contains(x.IdMainObject))
                                 && x.ListProperties.Any(y => y.CodeProperties == dataTableViewModel.Code && y.DefalutValue == dataTableViewModel.search.value.Trim()));

                            total = uiactivitydetailsNumber.Count();
                            lstMainId = uiactivitydetailsNumber.Select(x => x.IdMainObject).ToList();
                            break;
                    }

                    var lstGuid = lstMainId.Select(x => Guid.Parse(x)).ToList();
                    lstObject = lstObject.Where(x => lstGuid.Contains(x.Id)).ToList();
                }
            }
            if (dataTableViewModel.order[0].dir != "asc")
            {
                lstObject = lstObject.OrderByDescending(x => x.NameObject).ToList();
            }
            else
            {
                lstObject = lstObject.OrderBy(x => x.NameObject).ToList();
            }

            lstObject = lstObject.Skip(dataTableViewModel.start).Take(dataTableViewModel.length).ToList();
            lstResult = lstObject.Clone<List<InfoObjectData>>();
            //lstResult.Select((c, index) => { c.Geometry.Coordinates = Extensions.ConvertToListObject(lstObject[index].Geometry.Coordinates); return c; }).ToList();
            for (var im = 0; im < lstResult.Count; im++)
            {
                lstResult[im].Geometry.Coordinates = Extensions.ConvertToListObject(lstObject[im].Geometry.Coordinates);
            }


            var obj = new
            {
                totalCount = total,
                items = lstResult
            };

            return obj;
        }

        public async Task<object> GetListObjectAll(string Sorting, int SkipCount, int MaxResultCount)
        {
            var lstResult = new List<InfoObjectData>();

            //var lstObject = await Repository.GetListAsync();
            //var total = lstObject.Count();

            var lstObject = Repository.Skip(SkipCount).Take(MaxResultCount).ToList();

            lstResult = lstObject.Clone<List<InfoObjectData>>();

            foreach (var item in lstResult)
            {
                var detail = await _detailObjects.FindAsync(x => x.IdMainObject == item.Id.ToString());
                if (detail != null)
                {
                    item.ListProperties = detail.ListProperties;
                }
            }

            var obj = new
            {
                totalCount = 0,
                items = lstResult
            };

            return obj;
        }

        public async Task<object> GetObject(string id)
        {
            var lstResult = new InfoObjectData();
            var objectz = await Repository.GetAsync(x => x.Id == Guid.Parse(id));
            var detail = await _detailObjects.FindAsync(x => x.IdMainObject == objectz.Id.ToString());
            //Geometry a = objectz.Geometry;

            lstResult = objectz.Clone<InfoObjectData>();
            lstResult.Geometry.Coordinates = Extensions.ConvertToListObject(objectz.Geometry.Coordinates);
            if (detail != null) lstResult.ListProperties = detail.ListProperties;
            return lstResult;
        }

        // filter mainobject with properties value (typesystem  = 2)
        public async Task<object> GetObjectByPropertiesValue(string id, string search = "")
        {
            try
            {
                var lstResult = new InfoObjectData();
                var objectz = await Repository.GetAsync(x => x.Id == Guid.Parse(id));
                var detail = await _detailObjects.FindAsync(x => x.IdMainObject == objectz.Id.ToString() && x.ListProperties.Any(y => y.TypeSystem == 2 && y.DefalutValue.ToLower().Contains(search.ToLower())));
                //Geometry a = objectz.Geometry;
                if (detail == null)
                {
                    return null;
                }

                lstResult = objectz.Clone<InfoObjectData>();
                lstResult.Geometry.Coordinates = Extensions.ConvertToListObject(objectz.Geometry.Coordinates);
                if (detail != null) lstResult.ListProperties = detail.ListProperties;
                return lstResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<object> GetObjectExploit(string id)
        {
            var objectz = await Repository.GetAsync(x => x.Id == Guid.Parse(id));
            //Lấy thông tin của lớp objectz
            var listProperties = await _propertiesDirectoryAppService.GetAsyncById(objectz.IdDirectory);
            if (listProperties != null)
            {
                var detail = await _detailObjects.FindAsync(x => x.IdMainObject == objectz.Id.ToString("D"));
                var lstResult = new List<object>();
                if (detail != null)
                {

                    foreach (var item in listProperties.ListProperties)
                    {
                        var item2 = new PropertiesObject();
                        item2 = detail.ListProperties.FirstOrDefault(x => x.CodeProperties == item.CodeProperties);
                        //if (item2 != null && item.IsShowExploit)
                        if (item2 != null)
                        {
                            lstResult.Add(item2);
                        }
                    }
                    var result = new
                    {
                        idDirectory = objectz.IdDirectory,
                        listProperties = lstResult
                    };
                    return result;
                }
            }
            return null;
        }

        public async Task<bool> DeletedObject(string id)
        {

            var objectz = await Repository.GetAsync(x => x.Id == Guid.Parse(id));
            var detail = await _detailObjects.FindAsync(x => x.IdMainObject == objectz.Id.ToString());
            await _detailObjects.DeleteAsync(detail);
            await Repository.DeleteAsync(objectz);
            if (objectz != null)
            {
                await DeleteObjectMaptile(objectz);
            }
            //remove cache object
            await RemoveObjectOnCache(objectz.IdDirectory);
            return true;

        }

        public List<InfoObjectData> GetObjectByIdDriectory(string id)
        {
            var lstResult = new List<InfoObjectData>();
            List<MainObject> objectz = Repository.Where(x => x.IdDirectory == id).ToList();
            //var objectz = await Repository.GetListAsync();
            //objectz = objectz.Where(x => x.IdDirectory == id).ToList();
            lstResult = objectz.Clone<List<InfoObjectData>>().ToList();
            for (var i = 0; i < lstResult.Count(); i++)
            {
                if (lstResult[i].Geometry.Type != "Point")
                {
                    lstResult[i].Geometry.Coordinates = objectz[i].Geometry.Coordinates;
                }
            }
            return lstResult;
        }

        public async Task<List<InfoObjectData>> GetObjectByIdDriectoryKhaiThac(string id)
        {
            var lstResult = new List<InfoObjectData>();
            var propertiesDirectory = await _propertiesDirectoryAppService.GetAsyncById(id);
            if (propertiesDirectory.OptionDirectory == 1)
            {
                List<MainObject> objectz = Repository.Where(x => x.IdDirectory == id).ToList();
                lstResult = objectz.Clone<List<InfoObjectData>>().ToList();
                for (var i = 0; i < lstResult.Count(); i++)
                {
                    if (lstResult[i].Geometry.Type != "Point")
                    {
                        lstResult[i].Geometry.Coordinates = objectz[i].Geometry.Coordinates;
                    }
                }
            }
            return lstResult;
        }

        public async Task<object> CreaterListMainObject(List<ListMainObject> listMainObject)
        {
            List<MainObject> list = new List<MainObject>();
            List<string> listError = new List<string>();
            //remove cache object
            if (listMainObject.Count > 0) await RemoveObjectOnCache(listMainObject[0].IdDirectory);

            foreach (var item in listMainObject)
            {
                if (item.Geometry != null)
                {
                    item.Geometry.Coordinates = Extensions.ConvertToListObject(item.Geometry.Coordinates);
                    MainObject mainObject = new MainObject()
                    {
                        IdDirectory = item.IdDirectory,
                        NameObject = item.NameObject,
                        Geometry = item.Geometry,
                        Properties = item.Properties,
                        object3D = item.object3D,
                        Tags = item.Tags,
                        SearchObject = CommonMethod.ConvertToUnSign(item.NameObject.ToLower().Trim())
                    };
                    var insert = await Repository.InsertAsync(mainObject);
                    if (insert != null && insert.Id != null && insert.Geometry != null)
                    {
                        await AddObjectMapTile(insert);
                        DetailObject detailObject = new DetailObject()
                        {
                            IdMainObject = insert.Id.ToString("D"),
                            ListProperties = item.ListProperties
                        };
                        var insertDetail = await _detailObjects.InsertAsync(detailObject);
                        if (insertDetail != null && insertDetail.Id != null && insertDetail.ListProperties != null)
                        {
                            list.Add(insert);
                        }
                        else
                        {
                            listError.Add(insert.Id.ToString("D"));
                        }
                    }
                    else
                    {
                        listError.Add(insert.Id.ToString("D"));
                    }
                }
            }
            var obj = new
            {
                listobject = list,
                listerror = listError
            };
            return obj;
        }
        public async Task<object> GetObjectByKeyWord(string keyword)
        {
            var lstResult = new List<InfoObjectData>();
            //var objectz = await Repository.GetListAsync();
            if (!keyword.IsNullOrEmpty())
            {
                var objectz = Repository.Where(x => x.NameObject.ToLower().Contains(keyword.ToLower())).ToList();
                lstResult = objectz.Clone<List<InfoObjectData>>();
            }
            return lstResult;
        }

        public async Task<string> ExportToPdf(List<ObjectLocation> input)
        {
            var listObject = new List<ObjectLocation>();
            foreach (var item in input)
            {
                var obj = new ObjectLocation
                {
                    Name = string.IsNullOrEmpty(item.Name) ? "object" : item.Name,
                    QRCode = ""
                };

                string code = item.QRCode;

                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);

                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();
                        var urlQrcode = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                        obj.QRCode = urlQrcode;
                    }
                }
                listObject.Add(obj);
            }

            var globalSettings = new GlobalSettings
            {
                ColorMode = DinkToPdf.ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10, Bottom = 10 },
                DocumentTitle = "PDF Report",
                Out = (input.Count > 1) ? $"wwwroot/FileQRCode/DanhSachDoiTuong.pdf" : ($"wwwroot/FileQRCode/" + listObject[0].Name + ".pdf")
            };

            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = GetHTMLString(listObject),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "assets", "styles.css") },
            };

            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            //Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.dll");
            //pdf.GlobalSettings.Out = $"IoT.Web/wwwroot/FileQRCode/DanhSachDoiTuong.pdf";
            var file = _converter.Convert(pdf);

            var fileName = globalSettings.Out.Split("/")[globalSettings.Out.Split("/").Length - 1];

            return fileName;
        }

        public static string GetHTMLString(List<ObjectLocation> data)
        {
            var listObject = data;
            var sb = new StringBuilder();
            int i = 1;

            sb.Append(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h1>Danh sách các đối tượng</h1></div>
                                <table style='width: 100%; border-collapse: collapse;' align='center'>
                                    <tr style='text-align: center; background-color: green;color: #fff;font-weight: bold; padding-bottom: 35px;'>
                                        <th style='width: 10%; border: 1px solid gray;  padding: 15px; font-size: 22px;  text-align: center;'>STT</th>
                                        <th style='width: 60%; border: 1px solid gray;  padding: 15px; font-size: 22px;  text-align: center;'>Tên đối tượng</th>
                                        <th style='width: 30%; border: 1px solid gray;  padding: 15px; font-size: 22px;  text-align: center;'>QRCode</th>
                                    </tr>");
            foreach (var item in listObject)
            {
                sb.AppendFormat(@"<tr>
                                    <td style='border: 1px solid gray;  padding: 15px; font-size: 22px;  text-align: center;'>{0}</td>
                                    <td style='border: 1px solid gray;  padding: 15px; font-size: 22px;'>{1}</td>
                                    <td style=' padding: 3px;border: 1px solid gray;  padding: 15px; font-size: 22px;  text-align: center;'><img width='100' src='{2}'/></td>
                                  </tr>", i++, item.Name, item.QRCode);
            }
            sb.Append(@"
                                </table>
                            </body>
                        </html>");

            return sb.ToString();
        }

        //public async Task<string> ExportToPdf(List<ObjectLocation> input)
        //{
        //    var listObject = new List<ObjectLocation>();
        //    foreach(var item in input)
        //    {
        //        var obj = new ObjectLocation
        //        {
        //            Name = item.Name,
        //            QRCode = ""
        //        };

        //        string code = item.QRCode;

        //        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        //        QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
        //        QRCode qrCode = new QRCode(qrCodeData);

        //        using (Bitmap bitMap = qrCode.GetGraphic(20))
        //        {
        //            using (MemoryStream ms = new MemoryStream())
        //            {
        //                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        //                byte[] byteImage = ms.ToArray();
        //                var urlQrcode = "data:image/png;base64," + Convert.ToBase64String(byteImage);
        //                obj.QRCode = urlQrcode;
        //            }
        //        }
        //        listObject.Add(obj);
        //    }

        //    var globalSettings = new GlobalSettings
        //    {
        //        ColorMode = DinkToPdf.ColorMode.Color,
        //        Orientation = Orientation.Portrait,
        //        PaperSize = PaperKind.A4,
        //        Margins = new MarginSettings { Top = 10,Bottom=10 },
        //        DocumentTitle = "PDF Report",
        //        Out = $"wwwroot/FileQRCode/DanhSachDoiTuong.pdf"
        //    };

        //    var objectSettings = new ObjectSettings
        //    {
        //        PagesCount = true,
        //        HtmlContent = GetHTMLString(listObject),
        //        WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
        //    };

        //    var pdf = new HtmlToPdfDocument()
        //    {
        //        GlobalSettings = globalSettings,
        //        Objects = { objectSettings }
        //    };

        //    var file = _converter.Convert(pdf);

        //    var fileName = globalSettings.Out.Split("/")[globalSettings.Out.Split("/").Length - 1];

        //    return fileName;
        //}

        //private string GetHTMLString(List<ObjectLocation> data)
        //{
        //    var listObject = data;
        //    var sb = new StringBuilder();
        //    int i = 1;
        //    sb.Append(@"
        //                <html>
        //                    <head>
        //                    </head>
        //                    <body>
        //                        <div class='header'><h1>Danh sách các đối tượng</h1></div>
        //                              <div style='display: flex;text-align: center;flex-wrap: wrap;justify-content: center;' class='file-upload-content'>
        //                                <div style='width: 32%;position: relative;cursor: pointer;float: left;border: 1px solid gray;
        //                                    padding: 15px 0px;font-size: 22px;text-align: center;background-color: green;color: #fff;' 
        //                                    class='div-border div-title' align='center'>STT
        //                                </div>
        //                                <div style='width: 32%;position: relative;cursor: pointer;float: left;border: 1px solid gray;
        //                                    padding: 15px 0px;font-size: 22px;text-align: center;background-color: green;color: #fff;' 
        //                                    class='div-border div-title'>Tên đối tượng
        //                                </div>
        //                                <div style='width: 32%;position: relative;cursor: pointer;float: left;border: 1px solid gray;
        //                                    padding: 15px 0px;font-size: 22px;text-align: center;background-color: green;color: #fff;' 
        //                                    class='div-border div-title'>QRCode
        //                                </div>");
        //    foreach (var item in listObject)
        //    {
        //        string style2 = i == 10 ? "page-break-after : always;" : "";
        //        string style = "style='height: 90px;width: 32%;position: relative;cursor: pointer;float: left;border: 1px solid gray;padding: 12px 0px;"
        //                        + "font-size: 22px; text-align: center;'";
        //        sb.AppendFormat(@"<div style='width: 100%;display: flex;text-align: center;flex-wrap: wrap;justify-content: center;{4}'>  
        //                            <div class='div-table-col' {3}>{0}</div>
        //                            <div class='div-table-col' {3}>{1}</div>
        //                            <div class='div-table-col' {3}><img width='100px' src='{2}'/></div>
        //                         </div>", i++, item.Name, item.QRCode, style, style2);
        //    };
        //    sb.Append(@"
        //                        </table>
        //                    </body>
        //                </html>");

        //    return sb.ToString();
        //}

        public object GetNumberObjectByIdDriectory(string id, string search, int skipnumber, int countnumber, string code, string type)
        {
            var lstResult = new List<InfoObjectData>();
            IEnumerable<MainObject> objectz = new List<MainObject>();
            var queryObject = Repository.Where(x => x.IdDirectory == id).OrderBy(x => x.NameObject);

            if (!string.IsNullOrWhiteSpace(search))
            {
                if (string.IsNullOrEmpty(code))
                {
                    search = CommonMethod.ConvertToUnSign(search.ToLower().Trim());

                    //objectz = queryObject.Where(
                    //              delegate (MainObject x)
                    //              {
                    //                  return (
                    //                      (!string.IsNullOrEmpty(x.SearchObject) && Extensions.RemoveWhitespace(x.SearchObject.ToLower()).IndexOf(Extensions.RemoveWhitespace(search), StringComparison.CurrentCultureIgnoreCase) >= 0)
                    //                      ||
                    //                      (!string.IsNullOrEmpty(x.SearchObject) && x.SearchObject.Contains(search))
                    //                  );
                    //              }).OrderBy(x => x.NameObject).Skip(skipnumber).Take(countnumber).ToList();

                    objectz = queryObject.Where(x => !string.IsNullOrEmpty(x.NameObject) && x.SearchObject.Contains(search)).OrderBy(x => x.NameObject).Skip(skipnumber).Take(countnumber).ToList();
                }
                else
                {
                    var lstMainId = queryObject.ToList().Select(x => x.Id.ToString());
                    switch (type)
                    {
                        case "text":
                        case "string":
                        case "stringlarge":

                            //**************** tìm kiếm tuyệt đối
                            var uiactivitydetails = this._detailObjects.Where(x => (lstMainId != null && lstMainId.Contains(x.IdMainObject))
                                  && x.ListProperties.Any(y => y.CodeProperties == code
                                  && !string.IsNullOrEmpty(y.DefalutValue)
                                  && (y.DefalutValue.ToLower().Contains(search.ToLower()) || y.DefalutValue.ToLower().IndexOf(search.ToLower()) > -1)
                                  //|| CommonMethod.ConvertToUnSign(y.DefalutValue.ToLower()).Contains(search.ToLower())
                                  )).Skip(skipnumber).Take(countnumber);

                            //************* tìm kiếm tương đối
                            //var uiactivitydetails = this._detailObjects.Where(
                            //    delegate (DetailObject x)
                            //    {
                            //        return (
                            //                lstMainId != null && lstMainId.Contains(x.IdMainObject)
                            //                && x.ListProperties.Any(y => y.CodeProperties == code
                            //                && !string.IsNullOrEmpty(y.DefalutValue)
                            //                && CommonMethod.ConvertToUnSign(y.DefalutValue.ToLower()).IndexOf(search.ToLower(), StringComparison.CurrentCultureIgnoreCase) >= 0
                            //                )
                            //            );
                            //    }).Skip(skipnumber).Take(countnumber);

                            lstMainId = uiactivitydetails.Select(x => x.IdMainObject).ToList();
                            break;
                        case "float":
                        case "int":
                            var uiactivitydetailsNumber = this._detailObjects.Where(x => (lstMainId != null && lstMainId.Contains(x.IdMainObject))
                                 && x.ListProperties.Any(y => y.CodeProperties == code && y.DefalutValue == search)).Skip(skipnumber).Take(countnumber);
                            lstMainId = uiactivitydetailsNumber.Select(x => x.IdMainObject).ToList();
                            break;
                    }

                    var lstGuid = lstMainId.Select(x => Guid.Parse(x)).ToList();
                    objectz = queryObject.Where(x => lstGuid.Contains(x.Id));

                }
            }
            else
            {
                objectz = queryObject.Skip(skipnumber).Take(countnumber).ToList();
            }

            lstResult = objectz.Clone<List<InfoObjectData>>();
            int totalcount = Repository.Count(x => x.IdDirectory == id);
            //lstResult.Select(c => { c.TotalCount = totalcount; return c; }).ToList();
            return lstResult;
        }

        public async Task<MainObjectDto> UpdateNameMainObject(string id, string name)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(name))
            {
                return null;
            }

            var guiId = Guid.Parse(id);
            var mainObject = await Repository.GetAsync(guiId);

            if (mainObject == null)
            {
                return null;
            }

            mainObject.NameObject = name;
            mainObject.SearchObject = CommonMethod.ConvertToUnSign(name.ToLower().Trim());

            var obj = await Repository.UpdateAsync(mainObject);

            var result = obj.Clone<MainObjectDto>();
            if (!string.IsNullOrEmpty(mainObject.IdDirectory))
            {
                await RemoveObjectOnCache(mainObject.IdDirectory);
            }

            return result;
        }

        public async Task<bool> DeleteObjectMode3D(string id)
        {
            var guiId = Guid.Parse(id);
            var mainObject = await Repository.GetAsync(guiId);

            if (mainObject == null)
            {
                return false;
            }
            mainObject.object3D = null;

            var obj = await Repository.UpdateAsync(mainObject);
            if (obj != null)
            {
                await RemoveObjectOnCache(obj.IdDirectory);
                return true;
            }
            return false;
        }

        public async Task<bool> UpdateQRCode(FormUpdateQRCode input)
        {
            var guidId = Guid.Parse(input.Id);
            var mainObject = await Repository.GetAsync(guidId);

            if (mainObject == null)
            {
                return false;
            }

            mainObject.QRCodeObject = input.QRCodeObject;

            var obj = await Repository.UpdateAsync(mainObject);
            if (obj != null)
            {
                return true;
            }

            return false;
        }

        #region--------MapTile option--------
        //add object table postgres geoserver
        private async Task<bool> AddObjectMapTile(MainObject obj)
        {
            try
            {
                var propertiesDirectory = await _propertiesDirectoryAppService.GetAsyncById(obj.IdDirectory);
                if (obj != null && propertiesDirectory != null && propertiesDirectory.OptionDirectory == 2)
                {
                    var geo = JsonConvert.SerializeObject(obj.Geometry);
                    bool check = await _layerToLayerService.AddObjectTableAsync(obj.Id.ToString("D"), obj.IdDirectory, geo, obj.Geometry.Type, "");
                    if (check)
                    {
                        string bbox = await _layerToLayerService.UpdateBboxLayerGeoserverAsync(obj.IdDirectory);
                        var tags = new Dictionary<string, object>();
                        tags.Add("bounds", bbox);
                        propertiesDirectory.Tags = tags;
                        CreateUpdatePropertiesDirectoryDto param = propertiesDirectory.Clone<CreateUpdatePropertiesDirectoryDto>();
                        param.BoundMaptile = bbox;
                        await _propertiesDirectoryAppService.CreateAsync(param);
                    }
                    return check;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //update object
        private async Task<bool> UpdateObjectMaptile(MainObject obj)
        {
            try
            {
                var propertiesDirectory = await _propertiesDirectoryAppService.GetAsyncById(obj.IdDirectory);
                if (obj != null && propertiesDirectory != null && propertiesDirectory.OptionDirectory == 2)
                {
                    var geo = JsonConvert.SerializeObject(obj.Geometry);
                    bool check = await _layerToLayerService.UpdateObjectTableAsync(obj.Id.ToString("D"), obj.IdDirectory, geo, obj.Geometry.Type, "");
                    if (check)
                    {
                        string bbox = await _layerToLayerService.UpdateBboxLayerGeoserverAsync(obj.IdDirectory);
                        var tags = new Dictionary<string, object>();
                        tags.Add("bounds", bbox);
                        propertiesDirectory.Tags = tags;
                        CreateUpdatePropertiesDirectoryDto param = propertiesDirectory.Clone<CreateUpdatePropertiesDirectoryDto>();
                        param.BoundMaptile = bbox;
                        await _propertiesDirectoryAppService.CreateAsync(param);
                    }
                    return check;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //delete object
        private async Task<bool> DeleteObjectMaptile(MainObject obj)
        {
            try
            {
                var propertiesDirectory = await _propertiesDirectoryAppService.GetAsyncById(obj.IdDirectory);
                if (obj != null && propertiesDirectory != null && propertiesDirectory.OptionDirectory == 2)
                {
                    var geo = JsonConvert.SerializeObject(obj.Geometry);
                    return await _layerToLayerService.DeletebjectTableAsync(obj.Id.ToString("D"), obj.IdDirectory);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get count main object by IdDriectory
        public async Task<int> GetCountByIdDriectory(string idDriectory)
        {
            return Repository.Where(x => x.IdDirectory == idDriectory).Count();
        }
        #endregion

        #region-----------Optimal load data---------
        public async Task<List<InfoObjectDataNew>> GetObjectPagingByIdDirectory(string id, int page = 1)
        {
            string key = string.Format("driectory_{0}_{1}", id, page);
            //get object cache
            var objCache = await _cacheMemoryDistributedService.GetObjectCacheByKey(key);
            if (!string.IsNullOrEmpty(objCache))
            {
                List<InfoObjectDataNew> lstResult = JsonConvert.DeserializeObject<List<InfoObjectDataNew>>(objCache);
                return lstResult;
            }
            else
            {
                int pageSize = Convert.ToInt32(_configuration.GetSection("SettingApp:MaxRequest").Value);
                var skip = pageSize * (page - 1);
                List<MainObject> list = Repository.Where(x => x.IdDirectory == id).Skip(skip).Take(pageSize).ToList();
                List<InfoObjectDataNew> lstResult = list.Clone<List<InfoObjectDataNew>>().ToList();
                //set data object cache
                objCache = JsonConvert.SerializeObject(lstResult);
                await _cacheMemoryDistributedService.SetObjectCacheByKey(key, objCache);
                return lstResult;
            }
        }
        #endregion

        #region------------Cache--------------------
        private async Task<bool> RemoveObjectOnCache(string id)
        {
            try
            {
                double totalCount = Repository.Where(x => x.IdDirectory == id).Count();
                totalCount /= Convert.ToInt32(_configuration.GetSection("SettingApp:MaxRequest").Value);
                int count = Convert.ToInt32(Math.Ceiling(totalCount));
                string key = string.Format("driectory_{0}", id);
                await _cacheMemoryDistributedService.RemoveObjectCacheByKeyPage(key, count);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region-----------Change search---------
        public async Task<bool> ChangeValueSearch()
        {
            List<MainObject> obj = Repository.Where(x => x.NameObject != "" && x.NameObject != null && (x.SearchObject == "" || x.SearchObject == null)).Skip(0).Take(500).ToList();
            while (obj.Count > 0)
            {
                foreach (var item in obj)
                {
                    item.SearchObject = CommonMethod.ConvertToUnSign(item.NameObject.ToLower().Trim());
                }
                await Repository.UpdateManyAsync(obj);
                obj = Repository.Where(x => x.NameObject != "" && x.NameObject != null && (x.SearchObject == "" || x.SearchObject == null)).Skip(0).Take(500).ToList();
            }
            return true;
        }
        public async Task<bool> ChangeValueDetail()
        {
            Logger.LogInformation("--------Started get change detail Hòa------" + DateTime.Now.ToLongTimeString());
            var listIdDriectory = await _directoryService.FindFileDirectory();
            Logger.LogInformation("--------FindFileDirectory Hòa------" + DateTime.Now.ToLongTimeString());
            foreach (var item in listIdDriectory)
            {
                string id = item.Id.ToString("D");
                var listMain = Repository.Where(x => x.IdDirectory == id).ToList().Select(x => x.Id.ToString());
                Logger.LogInformation("--------listMain  Hòa------" + item.Id.ToString("D") + ":" + DateTime.Now.ToLongTimeString());
                var listDetail = _detailObjects.Where(x => listMain.Contains(x.IdMainObject)).ToList();
                Logger.LogInformation("--------listDetail  Hòa-------" + item.Id.ToString("D") + ":" + DateTime.Now.ToLongTimeString());
                if (listDetail.Count > 0)
                {
                    var listDetail1 = listDetail.Select(x => { x.IdDirectory = id; return x; });
                    await _detailObjects.UpdateManyAsync(listDetail1);
                    Logger.LogInformation("--------UpdateManyAsync  Hòa------" + item.Id.ToString("D") + ":" + DateTime.Now.ToLongTimeString());
                }
            }
            return true;
        }
        #endregion

        public async Task GetObjectGeoJsonAsync(string id)
        {
            var lst = Repository.Where(x => x.IdDirectory == id).ToList();

            DirectoryInfo folderPath = new DirectoryInfo(Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/LogImport"));
            FileInfo[] Files = folderPath.GetFiles();
            string p = "";
            foreach (FileInfo f in Files)
            {
                p = folderPath + "/" + f.Name;
                System.IO.File.Delete(p);
            }

            var serverFolderFile = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/LogImport");
            if (!System.IO.Directory.Exists(serverFolderFile))
            {
                System.IO.Directory.CreateDirectory(serverFolderFile);
            }

            string filePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/LogImport/exportLayer.txt");
            if (!System.IO.File.Exists(filePath))
            {
                TextWriter tw = new StreamWriter(filePath, true);
                var feature = new GeoJsonView();

                var ann = JsonConvert.SerializeObject(feature);

                var feateString = "{\"type\":\"FeatureCollection\",\"features\":[";
                tw.WriteLine(feateString);

                var dem = 0;

                foreach (var item in lst)
                {
                    var newObj = new FeatureView();
                    newObj.Type = "Feature";
                    var detail = await this._detailObjects.FindAsync(x => x.IdMainObject == item.Id.ToString());
                    newObj.Properties = new Dictionary<string, object>();
                    if (detail != null)
                    {
                        foreach (var property in detail.ListProperties)
                        {
                            newObj.Properties.Add(property.NameProperties, property.DefalutValue);
                        }
                    }
                    
                    newObj.Geometry = item.Geometry;
                    var newFeature = JsonConvert.SerializeObject(newObj);

                    newFeature = newFeature.Replace(",\"Empty\"", "");
                    if (dem != lst.Count() - 1)
                    {
                        newFeature += ",";
                    }

                    tw.WriteLine(newFeature);
                    dem++;
                }

                tw.WriteLine("]}");
                tw.Close();
            }
        }
    }

}
