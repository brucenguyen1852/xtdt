﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.Object
{
    public class ObjectModelService : CrudAppService<
            ObjectModel, //The Book entity
            ObjectModelDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateObjectModelDto>, //Used to create/update a book
        IObjectModelService //implement the IBookAppService
    {
        public ObjectModelService(IRepository<ObjectModel, Guid> repository) : base(repository)
        {
        }

        public override async Task<ObjectModelDto> CreateAsync(CreateObjectModelDto input)
        {
            IoT.HTKT.Object.ObjectModel objectModel = new Object.ObjectModel();
            IoT.HTKT.Object.ObjectModel objectModelInput = new IoT.HTKT.Object.ObjectModel()
            {
                IdObject = input.IdObject.Trim(),
                Name = input.Name,
                Type = input.Type,
                ObjName = input.ObjName,
                ObjUrl = input.ObjUrl,
                ObjData = input.ObjData,
                TextureName = input.TextureName,
                TextureUrl = input.TextureUrl,
                TextureData = input.TextureData,
                Color = input.Color,
                ThumbnailName = input.ThumbnailName,
                ThumbnailUrl = input.ThumbnailUrl,
                Coordinates = input.Coordinates,
                Height = input.Height,
                PlaceTypes = input.PlaceTypes,
                PlaceTypeInfo = input.PlaceTypeInfo
            };
            objectModel = await Repository.InsertAsync(objectModelInput);

            var json2 = JsonConvert.SerializeObject(objectModel);
            var result = JsonConvert.DeserializeObject<ObjectModelDto>(json2);
            return result;
        }

        public async Task<List<ObjectModelDto>> GetListDataAsync()
        {
            List<ObjectModelDto> objectModelDtos = new List<ObjectModelDto>();

            var objdirectory = await Repository.GetListAsync();
            if (objdirectory != null)
            {
                var json = JsonConvert.SerializeObject(objdirectory);
                objectModelDtos = JsonConvert.DeserializeObject<List<ObjectModelDto>>(json);
            }

            return objectModelDtos;
        }

    }
}
