﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.PropertiesDefault
{
    public class PropertiesDefaultAppService :
        CrudAppService<
            IoT.HTKT.Layer.PropertiesDefault, //The PropertiesDirectory entity
            PropertiesDefaultDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the PropertiesDirectory entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePropertiesDefaultDto>, //Used to create/update a PropertiesDirectory
        IPropertiesDefaultAppService //implement the IPropertiesDirectoryAppService
    {
        public PropertiesDefaultAppService(IRepository<IoT.HTKT.Layer.PropertiesDefault, Guid> repository)
            : base(repository)
        {

        }

        public override async Task<PropertiesDefaultDto> CreateAsync(CreatePropertiesDefaultDto input)
        {
            IoT.HTKT.Layer.PropertiesDefault country = new IoT.HTKT.Layer.PropertiesDefault()
            {
                NameProperties = input.NameProperties,
                CodeProperties = input.CodeProperties,
                TypeProperties = input.TypeProperties,
                IsShow = input.IsShow,
                IsIndexing = input.IsIndexing,
                IsRequest = input.IsRequest,
                IsView = input.IsView,
                IsHistory = input.IsHistory,
                IsInheritance = input.IsInheritance,
                IsAsync = input.IsAsync,
                DefalutValue = input.DefalutValue,
                ShortDescription = input.ShortDescription,
                TypeSystem = input.TypeSystem,
                OrderProperties = input.OrderProperties,
            };
            var obj = await Repository.InsertAsync(country);
            var result = ObjectMapper.Map<IoT.HTKT.Layer.PropertiesDefault, PropertiesDefaultDto>(obj);
            return result;
        }

        public async Task<List<PropertiesDefaultDto>> GetListProperties()
        {
            var obj = await Repository.GetListAsync();
            obj = obj.OrderBy(x => x.OrderProperties).ToList();
            var result = ObjectMapper.Map<List<IoT.HTKT.Layer.PropertiesDefault>, List<PropertiesDefaultDto>>(obj);
            return result;
        }
    }
}