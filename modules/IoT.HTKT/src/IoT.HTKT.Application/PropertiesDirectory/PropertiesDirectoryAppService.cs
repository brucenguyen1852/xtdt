﻿using IoT.Common;
using IoT.Geoserver.LayerGeoserver;
using IoT.HTKT.Layer;
using IoT.HTKT.ManagementData;
using IoT.HTKT.PropertiesDefault;
using IoT.HTKT.PropertiesDirectoryStatistic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.PropertiesDirectory
{
    public class PropertiesDirectoryAppService :
        CrudAppService<
            IoT.HTKT.Layer.PropertiesDirectory, //The PropertiesDirectory entity
            PropertiesDirectoryDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the PropertiesDirectory entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdatePropertiesDirectoryDto>, //Used to create/update a PropertiesDirectory
        IPropertiesDirectoryAppService //implement the IPropertiesDirectoryAppService
    {
        private readonly IPropertiesDefaultAppService _propertiesDefaultAppService;
        private readonly ILayerToLayerService _layerToLayerService;
        //private readonly IMainObjectService _mainObjectService;
        private readonly IRepository<MainObject, Guid> _mainObjects;
        private readonly IRepository<Directory, Guid> _directoryRepository;
        public PropertiesDirectoryAppService(IRepository<IoT.HTKT.Layer.PropertiesDirectory, Guid> repository,
            IPropertiesDefaultAppService propertiesDefaultAppService,
            ILayerToLayerService layerToLayerService, IRepository<MainObject, Guid> mainObjects,
            IRepository<Directory, Guid> directoryRepository)//,IMainObjectService mainObjectService)
            : base(repository)
        {
            _propertiesDefaultAppService = propertiesDefaultAppService;
            _layerToLayerService = layerToLayerService;
            //_mainObjectService = mainObjectService;
            _mainObjects = mainObjects;
            _directoryRepository = directoryRepository;
        }

        public override async Task<PropertiesDirectoryDto> CreateAsync(CreateUpdatePropertiesDirectoryDto input)
        {
            IoT.HTKT.Layer.PropertiesDirectory propertiesDirectory = new Layer.PropertiesDirectory();
            var checkid = await Repository.FindAsync(x => x.IdDirectory == input.IdDirectory);//await GetAsyncById(input.IdDirectory);
            if (checkid != null)
            {
                var jsonlist = JsonConvert.SerializeObject(input.ListProperties);
                List<IoT.HTKT.Layer.DetailProperties> lstProperties = JsonConvert.DeserializeObject<List<IoT.HTKT.Layer.DetailProperties>>(jsonlist);
                int optionGetLayer = checkid.OptionDirectory;
                //var json = JsonConvert.SerializeObject(checkid);
                //var country = JsonConvert.DeserializeObject<IoT.QTSC.Layer.PropertiesDirectory>(json);
                //var country2 = await Repository.FindAsync(x => x.IdDirectory == checkid.IdDirectory);
                checkid.NameTypeDirectory = input.NameTypeDirectory;
                checkid.CodeTypeDirectory = input.CodeTypeDirectory;
                checkid.VerssionTypeDirectory = input.VerssionTypeDirectory;
                checkid.IdImplement = input.IdImplement;
                checkid.ListProperties = lstProperties;
                checkid.GroundMaptile = input.GroundMaptile;
                checkid.Location = input.Location;
                checkid.Zoom = input.Zoom;
                checkid.OptionDirectory = input.OptionDirectory;
                checkid.MinZoom = input.MinZoom;
                checkid.MaxZoom = input.MaxZoom;
                if (checkid.Tags != null)
                {
                    string bounds = string.Empty;
                    if (checkid.Tags.ContainsKey("bounds"))
                    {
                        checkid.Tags["bounds"] = input.BoundMaptile;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(input.BoundMaptile))
                    {
                        var tags = new Dictionary<string, object>();
                        tags.Add("bounds", input.BoundMaptile);
                        checkid.Tags = tags;
                    }
                }

                propertiesDirectory = await Repository.UpdateAsync(checkid);
                if (propertiesDirectory != null && optionGetLayer == 1 && input.OptionDirectory == 2) // check change option 1 to option 2
                {
                    //call change option geoserver
                    propertiesDirectory = await SendDataGeoserver(propertiesDirectory.IdDirectory, propertiesDirectory.NameTypeDirectory, propertiesDirectory, false);
                }
            }
            else
            {
                var list = await _propertiesDefaultAppService.GetListProperties();
                var json = input.ListProperties.Count > 1 ? JsonConvert.SerializeObject(input.ListProperties) : JsonConvert.SerializeObject(list);
                List<IoT.HTKT.Layer.DetailProperties> detailProperties = JsonConvert.DeserializeObject<List<IoT.HTKT.Layer.DetailProperties>>(json);

                var tags = new Dictionary<string, object>();

                IoT.HTKT.Layer.PropertiesDirectory country = new IoT.HTKT.Layer.PropertiesDirectory()
                {
                    IdDirectory = input.IdDirectory,
                    NameTypeDirectory = input.NameTypeDirectory,
                    CodeTypeDirectory = input.CodeTypeDirectory,
                    VerssionTypeDirectory = input.VerssionTypeDirectory,
                    IdImplement = input.IdImplement,
                    ListProperties = detailProperties,
                    GroundMaptile = input.GroundMaptile,
                    Location = input.Location,
                    Zoom = input.Zoom,
                    OptionDirectory = input.OptionDirectory,
                    MinZoom = input.MinZoom,
                    MaxZoom = input.MaxZoom
                };

                if (!string.IsNullOrEmpty(input.BoundMaptile))
                {
                    tags.Add("bounds", input.BoundMaptile);
                    country.Tags = tags;
                }

                propertiesDirectory = await Repository.InsertAsync(country);
                if (propertiesDirectory != null && country.OptionDirectory == 2)
                {
                    propertiesDirectory = await SendDataGeoserver(input.IdDirectory, input.NameTypeDirectory, propertiesDirectory, true);
                    //CreateLayerToLayerDto createLayerToLayerDto = new CreateLayerToLayerDto();
                    //createLayerToLayerDto.IdDirectory = input.IdDirectory;
                    //createLayerToLayerDto.LayerGeoserver = input.NameTypeDirectory;
                    //createLayerToLayerDto.Status = 0;
                    //object check = await _layerToLayerService.CreateToLayerGeoserverAsync(createLayerToLayerDto);
                    //var propertyInfo = (bool)(check.GetType().GetProperty("status")).GetValue(check, null);
                    ////var value = (bool)propertyInfo.GetValue(check, null);
                    ////dynamic obj = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(check));
                    //if (propertyInfo)
                    //{
                    //    propertiesDirectory.GroundMaptile = (string)(check.GetType().GetProperty("result")).GetValue(check, null);
                    //    tags.Add("bounds", "[]");
                    //    propertiesDirectory.Tags = tags;
                    //    propertiesDirectory = await Repository.UpdateAsync(propertiesDirectory);
                    //}
                }
            }

            var json2 = JsonConvert.SerializeObject(propertiesDirectory);
            var result = JsonConvert.DeserializeObject<PropertiesDirectoryDto>(json2);
            return result;
        }

        public async Task<bool> DeleteDirectoryByIdDirectory(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var objdirectory = await Repository.FindAsync(x => x.IdDirectory == id);
                if (objdirectory == null)
                {
                    objdirectory = await Repository.FindAsync(x => x.Id == new Guid(id));
                }
                if (objdirectory != null)
                {
                    await Repository.DeleteAsync(objdirectory);
                    return true;
                }
            }
            return false;
        }

        public async Task<PropertiesDirectoryDto> GetAsyncById(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var objdirectory = await Repository.FindAsync(x => x.IdDirectory == id);
                if (objdirectory == null)
                {
                    objdirectory = await Repository.FindAsync(x => x.Id == new Guid(id));
                }
                var json = JsonConvert.SerializeObject(objdirectory);
                PropertiesDirectoryDto result = JsonConvert.DeserializeObject<PropertiesDirectoryDto>(json);
                return result;
            }
            return null;
        }

        public async Task<List<PropertiesDirectoryDto>> GetListDirectoryAsync()
        {
            List<PropertiesDirectoryDto> propertiesDirectoryDtos = new List<PropertiesDirectoryDto>();

            var lstdirectory = await _directoryRepository.GetListAsync();
            var lstDirectoryId = lstdirectory.Select(x => x.Id.ToString());

            var objdirectory = await Repository.GetListAsync(x => lstDirectoryId.Contains(x.IdDirectory));
            if (objdirectory != null)
            {
                var json = JsonConvert.SerializeObject(objdirectory);
                propertiesDirectoryDtos = JsonConvert.DeserializeObject<List<PropertiesDirectoryDto>>(json);
            }

            return propertiesDirectoryDtos;
        }

        public async Task<PropertiesDirectoryDto> UpdateListProperties(List<DetailProperties> list, string id)
        {
            IoT.HTKT.Layer.PropertiesDirectory propertiesDirectory = new Layer.PropertiesDirectory();
            var checkid = await Repository.FindAsync(x => x.IdDirectory == id);
            if (checkid != null)
            {
                List<Layer.DetailProperties> listClone = new List<Layer.DetailProperties>();
                foreach (var item in list)
                {
                    var checkProperties = checkid.ListProperties.Find(x => x.CodeProperties == item.CodeProperties);
                    if (checkProperties == null)
                    {
                        var obj = item.Clone<Layer.DetailProperties>();
                        listClone.Add(obj);
                    }
                }
                checkid.ListProperties.AddRange(listClone);
                propertiesDirectory = await Repository.UpdateAsync(checkid);
            }
            var result = propertiesDirectory.Clone<PropertiesDirectoryDto>();
            return result;
        }

        //public async Task<PropertiesDirectoryDto> UpdateMaptileAndBbox(CreateUpdatePropertiesDirectoryDto input)
        //{

        //}

        //send data geoserver layer
        private async Task<Layer.PropertiesDirectory> SendDataGeoserver(string idDirectory, string nameTypeDirectory, Layer.PropertiesDirectory propertiesDirectory, bool checkCreate)
        {
            try
            {
                CreateLayerToLayerDto createLayerToLayerDto = new CreateLayerToLayerDto();
                createLayerToLayerDto.IdDirectory = idDirectory;
                createLayerToLayerDto.LayerGeoserver = nameTypeDirectory;
                createLayerToLayerDto.Status = 0;
                object check = checkCreate ? await _layerToLayerService.CreateToLayerGeoserverAsync(createLayerToLayerDto) : await SendDataChangeOptionGeoserver(createLayerToLayerDto);
                var propertyInfo = (bool)(check.GetType().GetProperty("status")).GetValue(check, null);
                if (propertyInfo)
                {
                    var tags = new Dictionary<string, object>();
                    propertiesDirectory.GroundMaptile = (string)(check.GetType().GetProperty("result")).GetValue(check, null);
                    tags.Add("bounds", "[]");
                    propertiesDirectory.Tags = tags;
                    propertiesDirectory = await Repository.UpdateAsync(propertiesDirectory);
                }
                return propertiesDirectory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task<object> SendDataChangeOptionGeoserver(CreateLayerToLayerDto param)
        {
            try
            {
                int count = await _mainObjects.CountAsync(x => x.IdDirectory == param.IdDirectory);
                object obj = new { status = false, result = "" };
                if (count > 0)
                {
                    int skip = 0, page = 1, pageSize = 500;

                    while (skip < count)
                    {
                        skip = pageSize * (page - 1);
                        IQueryable<MainObject> queryMainObject = await _mainObjects.GetQueryableAsync();
                        var list = queryMainObject.AsQueryable().Where(x => x.IdDirectory == param.IdDirectory).Skip(skip).Take(pageSize).ToList(); //_mainObjectService.GetListObjectAll("", skip, pageSize);
                        var listObjGeoserver = list.Clone<List<MainObjectGeoserver>>();
                        obj = await _layerToLayerService.ChangeToLayerGeoserverAsync(param, listObjGeoserver);
                        skip += list.Count();
                        page++;
                    }
                }
                else
                {
                    obj = await _layerToLayerService.ChangeToLayerGeoserverAsync(param, new List<MainObjectGeoserver>());
                    //var layerGeoserver = await _layerToLayerService.FindGetLink(param.IdDirectory);
                    //if ( !string.IsNullOrEmpty(layerGeoserver))
                    //{
                    //    obj = new { status = true, result = layerGeoserver };
                    //}
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<PropertiesDirectoryDto>> GetListDirectoryByOptionLayerAsync(int optionLayer)
        {
            try
            {
                if (optionLayer > -1)
                {
                    var objdirectory = Repository.Where(x => x.OptionDirectory == optionLayer).ToList();
                    var json = JsonConvert.SerializeObject(objdirectory);
                    List<PropertiesDirectoryDto> result = JsonConvert.DeserializeObject<List<PropertiesDirectoryDto>>(json);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateDataModel()
        {
            var objdirectory = await Repository.GetListAsync();
            await Repository.UpdateManyAsync(objdirectory);
            return true;
        }
    }
}
