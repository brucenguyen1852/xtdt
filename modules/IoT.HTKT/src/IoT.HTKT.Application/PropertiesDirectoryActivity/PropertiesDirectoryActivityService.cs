﻿using IoT.HTKT.Activity;
using IoT.HTKT.PropertiesDefault;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.PropertiesDirectoryActivity
{
    public class PropertiesDirectoryActivityService : CrudAppService<
            IoT.HTKT.ManageActivity.PropertiesDirectoryActivity, //The Book entity
            PropertiesDirectoryActivityDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePropertiesDirectoryActivityDto>, //Used to create/update a book
        IPropertiesDirectoryActivityService //implement the IBookAppService
    {
        private readonly IPropertiesDefaultAppService _propertiesDefaultAppService;
        private readonly IDirectoryActivityService _directoryActivityService;


        public PropertiesDirectoryActivityService(IRepository<IoT.HTKT.ManageActivity.PropertiesDirectoryActivity, Guid> repository,
                                                 IPropertiesDefaultAppService propertiesDefaultAppService,
                                                 IDirectoryActivityService directoryActivityService) : base(repository)
        {
            _propertiesDefaultAppService = propertiesDefaultAppService;
            _directoryActivityService = directoryActivityService;
        }

        public override async Task<PropertiesDirectoryActivityDto> CreateAsync(CreatePropertiesDirectoryActivityDto input)
        {
            try
            {
                IoT.HTKT.ManageActivity.PropertiesDirectoryActivity propertiesDirectory = new IoT.HTKT.ManageActivity.PropertiesDirectoryActivity();
                var checkid = await GetAsyncById(input.IdDirectoryActivity);
                if (checkid != null)
                {
                    var jsonlist = JsonConvert.SerializeObject(input.ListProperties);
                    List<IoT.HTKT.ManageActivity.DetailPropertiesActivity> lstProperties = JsonConvert.DeserializeObject<List<IoT.HTKT.ManageActivity.DetailPropertiesActivity>>(jsonlist);
                    //var json = JsonConvert.SerializeObject(checkid);
                    //var country = JsonConvert.DeserializeObject<IoT.QTSC.Layer.PropertiesDirectory>(json);
                    var country2 = await Repository.FindAsync(x => x.IdDirectoryActivity == checkid.IdDirectoryActivity);
                    country2.NameTypeDirectory = input.NameTypeDirectory;
                    country2.CodeTypeDirectory = input.CodeTypeDirectory;
                    country2.VerssionTypeDirectory = input.VerssionTypeDirectory;
                    country2.ListProperties = lstProperties;
                    propertiesDirectory = await Repository.UpdateAsync(country2);
                }
                else
                {
                    var list = await _propertiesDefaultAppService.GetListProperties();
                    var json = input.ListProperties.Count > 1 ? JsonConvert.SerializeObject(input.ListProperties) : JsonConvert.SerializeObject(list);
                    List<IoT.HTKT.ManageActivity.DetailPropertiesActivity> detailProperties = JsonConvert.DeserializeObject<List<IoT.HTKT.ManageActivity.DetailPropertiesActivity>>(json);
                    IoT.HTKT.ManageActivity.PropertiesDirectoryActivity country = new IoT.HTKT.ManageActivity.PropertiesDirectoryActivity()
                    {
                        IdDirectoryActivity = input.IdDirectoryActivity,
                        NameTypeDirectory = input.NameTypeDirectory,
                        CodeTypeDirectory = input.CodeTypeDirectory,
                        VerssionTypeDirectory = input.VerssionTypeDirectory,
                        IdImplement = input.IdImplement,
                        ListProperties = detailProperties.Where(x => x.TypeSystem != 1 && x.TypeSystem != 3 && x.CodeProperties != "Title" && x.CodeProperties != "Name" && x.CodeProperties != "Map").ToList()
                    };

                    var dem = 0;
                    // reset order properties default
                    foreach (var item in country.ListProperties)
                    {
                        item.OrderProperties = dem + 1;
                        dem++;
                    }
                    if (!string.IsNullOrWhiteSpace(country.IdImplement))
                    {
                        country.ListProperties = detailProperties;
                    }
                    propertiesDirectory = await Repository.InsertAsync(country);
                }


                var json2 = JsonConvert.SerializeObject(propertiesDirectory);
                var result = JsonConvert.DeserializeObject<PropertiesDirectoryActivityDto>(json2);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteDirectoryByIdDirectory(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var objdirectory = await Repository.FindAsync(x => x.IdDirectoryActivity == id);
                    if (objdirectory == null)
                    {
                        objdirectory = await Repository.FindAsync(x => x.Id == new Guid(id));
                    }
                    if (objdirectory != null)
                    {
                        await Repository.DeleteAsync(objdirectory);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

        public async Task<PropertiesDirectoryActivityDto> GetAsyncById(string idDirectory)
        {
            try
            {
                if (!string.IsNullOrEmpty(idDirectory))
                {
                    var aomm = Repository.ToList();
                    var objdirectory = await Repository.FindAsync(x => x.IdDirectoryActivity == idDirectory);

                    var guid = Guid.Parse(idDirectory);
                    var activity = await this._directoryActivityService.GetAsync(guid);
                    if (objdirectory == null)
                    {
                        objdirectory = await Repository.FindAsync(x => x.Id == new Guid(idDirectory));
                    }
                    var json = JsonConvert.SerializeObject(objdirectory);

                    PropertiesDirectoryActivityDto result = JsonConvert.DeserializeObject<PropertiesDirectoryActivityDto>(json);
                    if (activity != null)
                    {
                        result.IdDirectory = activity.IdDirectory;
                    }
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
        }

        public async Task<List<PropertiesDirectoryActivityDto>> GetListDirectoryAsync()
        {
            List<PropertiesDirectoryActivityDto> propertiesDirectoryDtos = new List<PropertiesDirectoryActivityDto>();
            try
            {
                var objdirectory = await Repository.GetListAsync();
                if (objdirectory != null)
                {
                    var json = JsonConvert.SerializeObject(objdirectory);
                    propertiesDirectoryDtos = JsonConvert.DeserializeObject<List<PropertiesDirectoryActivityDto>>(json);
                }

                return propertiesDirectoryDtos;
            }
            catch (Exception ex)
            {
                //throw ex;
                return propertiesDirectoryDtos;
            }
        }
    }
}
