﻿using IoT.Common;
using IoT.HTKT.Layer;
using IoT.HTKT.PropertiesDirectory;
using IoT.HTKT.PropertiesSetting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.PropertiesDirectoryStatistic
{
    public class PropertiesDirectoryStatisticService :
    CrudAppService<
        IoT.HTKT.Layer.PropertiesDirectoryStatistic, //The PropertiesDirectory entity
        PropertiesDirectoryStatisticDto, //Used to show PropertiesDirectory
        Guid, //Primary key of the PropertiesDirectory entity
        PagedAndSortedResultRequestDto, //Used for paging/sorting
        CreateUpdatePropertiesDirectoryStatisticDto>, //Used to create/update a PropertiesDirectory
    IPropertiesDirectoryStatisticService //implement the IPropertiesDirectoryAppService
    {
        private readonly IPropertiesSettingService _propertiesSettingService;
        private readonly IDirectoryService _directoryService;
        public PropertiesDirectoryStatisticService(IRepository<IoT.HTKT.Layer.PropertiesDirectoryStatistic, Guid> repository,
                                                   IPropertiesSettingService propertiesSettingService, IDirectoryService directoryService)
            : base(repository)
        {
            _propertiesSettingService = propertiesSettingService;
            _directoryService = directoryService;
        }

        public override async Task<PropertiesDirectoryStatisticDto> CreateAsync(CreateUpdatePropertiesDirectoryStatisticDto input)
        {
            IoT.HTKT.Layer.PropertiesDirectoryStatistic propertiesDirectory = new Layer.PropertiesDirectoryStatistic();
            var checkid = await Repository.FindAsync(x => x.IdDirectory == input.IdDirectory);
            if (checkid != null)
            {
                checkid.Count += 1;
                propertiesDirectory = await Repository.UpdateAsync(checkid);
            }
            else
            {
                IoT.HTKT.Layer.PropertiesDirectoryStatistic country = new IoT.HTKT.Layer.PropertiesDirectoryStatistic()
                {
                    IdDirectory = input.IdDirectory,
                    Count = 1,
                };
                propertiesDirectory = await Repository.InsertAsync(country);
            }
            var json2 = JsonConvert.SerializeObject(propertiesDirectory);
            var result = JsonConvert.DeserializeObject<PropertiesDirectoryStatisticDto>(json2);
            return result;
        }

        public async Task<PropertiesDirectoryStatisticDto> GetAsyncById(string idDirectory)
        {
            if (!string.IsNullOrEmpty(idDirectory))
            {
                var objdirectory = await Repository.FindAsync(x => x.IdDirectory == idDirectory);
                if (objdirectory == null)
                {
                    objdirectory = await Repository.FindAsync(x => x.Id == new Guid(idDirectory));
                }
                var json = JsonConvert.SerializeObject(objdirectory);
                PropertiesDirectoryStatisticDto result = JsonConvert.DeserializeObject<PropertiesDirectoryStatisticDto>(json);
                return result;
            }
            return null;
        }

        public async Task<List<DirectoryProperties>> GetListDirectoryStatistic(List<string> lstShow)
        {
            List<DirectoryProperties> result = new List<DirectoryProperties>();

            var lst = Repository.OrderByDescending(x => x.Count).Skip(0).Take(20).ToList();
            //lst = lst.OrderByDescending(x => x.Count).ToList();
            if (lstShow.Count == 0)
            {
                foreach (var item in lst)
                {
                    var directory = await _directoryService.FinDirectoryById(item.IdDirectory);
                    if (directory != null)
                    {
                        DirectoryProperties directoryProperties = new DirectoryProperties();
                        directoryProperties.Count = item.Count;
                        directoryProperties.IdDirectory = item.IdDirectory;
                        directoryProperties.Name = directory.Name;
                        directoryProperties.Id = directory.Id;
                        var image = await _propertiesSettingService.FindPropertyByIdDirection(item.IdDirectory);
                        if (image != null)
                        {
                            directoryProperties.Image2D = image.Image2D;
                        }


                        result.Add(directoryProperties);
                    }
                    if (result.Count >= 10)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        //public Task<bool> UpdateDirectoryByIdDirectory(string id)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
