﻿using IoT.Geoserver.LayerGeoserver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.PropertiesSetting
{
    public class PropertiesSettingService :
        CrudAppService<
            IoT.HTKT.Layer.PropertiesSetting, //The PropertiesDirectory entity
            PropertiesSettingDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the PropertiesDirectory entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePropertiesSettingDto>, //Used to create/update a PropertiesDirectory
        IPropertiesSettingService //implement the IPropertiesDirectoryAppService
    {
        private ILayerToLayerService _layerToLayerService;
        public PropertiesSettingService(IRepository<IoT.HTKT.Layer.PropertiesSetting, Guid> repository, ILayerToLayerService layerToLayerService)
            : base(repository)
        {
            _layerToLayerService = layerToLayerService;
        }

        public override async Task<PropertiesSettingDto> CreateAsync(CreatePropertiesSettingDto input)
        {
            IoT.HTKT.Layer.PropertiesSetting country = new IoT.HTKT.Layer.PropertiesSetting()
            {
                IdDirectory = input.IdDirectory,
                MinZoom = input.MinZoom,
                MaxZoom = input.MaxZoom,
                Image2D = input.Image2D,
                Object3D = input.Object3D,
                Texture3D = input.Texture3D,
                Stroke = input.Stroke,
                StrokeWidth = input.StrokeWidth,
                StrokeOpacity = double.Parse(input.StrokeOpacity, System.Globalization.CultureInfo.InvariantCulture),
                StyleStroke = input.StyleStroke,
                Fill = input.Fill,
                FillOpacity = double.Parse(input.FillOpacity, System.Globalization.CultureInfo.InvariantCulture)
            };
            var obj = await Repository.InsertAsync(country);
            var json2 = JsonConvert.SerializeObject(obj);
            var result = JsonConvert.DeserializeObject<PropertiesSettingDto>(json2);
            return result;
        }

        public async Task<PropertiesSettingDto> FindPropertyByIdDirection(string id)
        {
            var findObj = await Repository.FirstOrDefaultAsync(x => x.IdDirectory == id);
            var json2 = JsonConvert.SerializeObject(findObj);
            var result = JsonConvert.DeserializeObject<PropertiesSettingDto>(json2);
            return result;
        }

        public async Task<List<PropertiesSettingDto>> getAllProperties()
        {
            var listFile = await Repository.GetListAsync();
            var jsonFile = JsonConvert.SerializeObject(listFile);
            var result = JsonConvert.DeserializeObject<List<PropertiesSettingDto>>(jsonFile);
            return result;
        }

        public async Task<PropertiesSettingDto> UpdateProperty(CreatePropertiesSettingDto input)
        {
            //return null;
            try
            {
                //add style update layer geoserver
                StyleLayerToLayerDto style = new StyleLayerToLayerDto()
                {
                    idLayer = input.IdDirectory,
                    fill = input.Fill,
                    stroke = input.Stroke,
                    fillO = double.Parse(input.FillOpacity, System.Globalization.CultureInfo.InvariantCulture),
                    strokeW = double.Parse(input.StrokeOpacity, System.Globalization.CultureInfo.InvariantCulture)
                };

                var findObj = await Repository.FirstOrDefaultAsync(x => x.IdDirectory == input.IdDirectory);
                if (findObj == null)
                {
                    //Tao moi
                    IoT.HTKT.Layer.PropertiesSetting propertiesSetting = new IoT.HTKT.Layer.PropertiesSetting()
                    {
                        IdDirectory = input.IdDirectory,
                        MinZoom = input.MinZoom,
                        MaxZoom = input.MaxZoom,
                        Image2D = input.Image2D,
                        Object3D = input.Object3D,
                        Texture3D = input.Texture3D,
                        Stroke = input.Stroke,
                        StrokeWidth = input.StrokeWidth,
                        StrokeOpacity = double.Parse(input.StrokeOpacity, System.Globalization.CultureInfo.InvariantCulture),
                        StyleStroke = input.StyleStroke,
                        Fill = input.Fill,
                        FillOpacity = double.Parse(input.FillOpacity, System.Globalization.CultureInfo.InvariantCulture)
                    };

                    var tags = new Dictionary<string, object>();
                    tags.Add("icon", input.ImageIcon);
                    propertiesSetting.Tags = tags;

                    var obj = await Repository.InsertAsync(propertiesSetting);
                    var json2 = JsonConvert.SerializeObject(obj);
                    var result = JsonConvert.DeserializeObject<PropertiesSettingDto>(json2);
                    //add style update layer geoserver
                    await _layerToLayerService.UpdateStyleLayerGeoserverAsync(style);
                    return result;
                }
                else
                {
                    findObj.MinZoom = input.MinZoom;
                    findObj.MaxZoom = input.MaxZoom;
                    findObj.Image2D = input.Image2D;
                    findObj.Object3D = input.Object3D;
                    findObj.Texture3D = input.Texture3D;
                    findObj.Stroke = input.Stroke;
                    findObj.StrokeWidth = input.StrokeWidth;
                    findObj.StrokeOpacity = double.Parse(input.StrokeOpacity, System.Globalization.CultureInfo.InvariantCulture);
                    findObj.StyleStroke = input.StyleStroke;
                    findObj.Fill = input.Fill;
                    findObj.FillOpacity = double.Parse(input.FillOpacity, System.Globalization.CultureInfo.InvariantCulture);

                    if (findObj.Tags != null)
                    {
                        string bounds = string.Empty;
                        if (findObj.Tags.ContainsKey("icon"))
                        {
                            findObj.Tags["icon"] = input.ImageIcon;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(input.ImageIcon))
                        {
                            var tags = new Dictionary<string, object>();
                            tags.Add("icon", input.ImageIcon);
                            findObj.Tags = tags;
                        }
                    }

                    var obj = await Repository.UpdateAsync(findObj);
                    var json2 = JsonConvert.SerializeObject(obj);
                    var result = JsonConvert.DeserializeObject<PropertiesSettingDto>(json2);
                    //add style update layer geoserver
                    await _layerToLayerService.UpdateStyleLayerGeoserverAsync(style);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
