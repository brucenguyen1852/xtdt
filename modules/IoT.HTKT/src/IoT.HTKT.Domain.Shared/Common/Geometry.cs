﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Common
{
    public class Geometry
    {
        /// <summary>
        /// Type (Polygon or MultiLineString)
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Coordinates
        /// </summary>
        [JsonProperty("coordinates")]
        public IList<object> Coordinates { get; set; }
    }
    public class GeometryNew
    {
        /// <summary>
        /// Type (Polygon or MultiLineString)
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Coordinates
        /// </summary>
        [JsonProperty("coordinates")]
        public List<object> Coordinates { get; set; }
    }
}
