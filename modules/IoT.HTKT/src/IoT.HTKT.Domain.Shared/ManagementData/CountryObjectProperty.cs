﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class CountryObjectProperty
    {
        [JsonProperty("district")]
        public List<string> District { get; set; }
        [JsonProperty("wardDistrict")]
        public List<string> WardDistrict { get; set; }
    }
}
