﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class Object3D
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("objectUrl")]
        public string ObjectUrl { get; set; }

        [JsonProperty("texture")]
        public string Texture { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("scale")]
        public double Scale { get; set; }

        [JsonProperty("bearing")]
        public double Bearing { get; set; }

        [JsonProperty("elevation")]
        public double Elevation { get; set; }

        [JsonProperty("height")]
        public int Height { set; get; }

        [JsonProperty("coordinates")]
        public List<Location> Coordinates { set; get; }

        [JsonProperty("status")]
        public bool Status { set; get; }

        [JsonProperty("type")]
        public string Type { get; set; } = string.Empty;
    }

    public class Location
    {
        [JsonProperty("lng")]
        public double Lng { get; set; }

        [JsonProperty("lat")]
        public double Lat { set; get; }
    }
}
