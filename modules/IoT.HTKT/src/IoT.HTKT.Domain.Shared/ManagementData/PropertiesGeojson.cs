﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class PropertiesGeojson
    {
        [JsonProperty("image2D")]
        public string Image2D { get; set; } // ảnh đại điện
        [JsonProperty("stroke")]
        public string Stroke { get; set; }  // màu viền
        [JsonProperty("strokeWidth")]
        public int StrokeWidth { get; set; } // độ rộng của viền
        [JsonProperty("strokeOpacity")]
        public double StrokeOpacity { get; set; } // độ mờ của viền 100% / 100
        [JsonProperty("styleStroke")]
        public string StyleStroke { get; set; } // Kiểu đường
        [JsonProperty("fill")]
        public string Fill { get; set; } //màu của vùng
        [JsonProperty("fillOpacity")]
        public double FillOpacity { get; set; } //độ mờ của vùng 100% / 100
    }
}
