﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.ManagementData
{
    public class PropertiesObject
    {
        [JsonProperty("nameProperties")]
        public string NameProperties { get; set; } // tên thuộc tính
        [JsonProperty("codeProperties")]
        public string CodeProperties { get; set; } // mã thuộc tính
        [JsonProperty("typeProperties")]
        public string TypeProperties { get; set; } // loại thuộc tính
        [JsonProperty("isShow")]
        public bool IsShow { get; set; } // hiển thị
        [JsonProperty("isView")]
        public bool IsView { get; set; } // chỉ xem
        [JsonProperty("defalutValue")]
        public string DefalutValue { get; set; } //Giá trị
        [JsonProperty("orderProperties")]
        public int OrderProperties { get; set; } // thứ tự thuộc tính
        [JsonProperty("typeSystem")]
        public int TypeSystem { get; set; } // Thuộc tính hệ thống hay thường
    }
}
