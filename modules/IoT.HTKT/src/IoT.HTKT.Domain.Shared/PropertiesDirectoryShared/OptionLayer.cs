﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.PropertiesDirectoryShared
{
    public enum OptionLayer
    {
        Geojson = 1,
        Geoserver = 2,
        GeoserverOther = 3
    }
}
