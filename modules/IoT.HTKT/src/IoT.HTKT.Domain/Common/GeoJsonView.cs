﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.HTKT.Common
{
    public class GeoJsonView
    {
        /// <summary>
        /// Geojson
        /// </summary>
        public GeoJsonView()
        {
            Type = "FeatureCollection";
            Features = new List<FeatureView>();
        }

        /// <summary>
        /// Type
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Features
        /// </summary>
        [JsonProperty("features")]
        public IList<FeatureView> Features { get; set; }
    }
}
