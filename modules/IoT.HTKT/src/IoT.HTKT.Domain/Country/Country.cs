﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.Country
{
    public class Country : AuditedAggregateRoot<Guid>
    {
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string Code { get; set; }
        public string Search { get; set; }
        public string FullCode { get; set; }
    }
}
