﻿using IoT.Common;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.Country
{
    public class GeojsonCountry : AuditedAggregateRoot<Guid>
    {
        public string IdCountry { get; set; }
        public string CodeCountry { get; set; }
        //public string IdPlaceMap { get; set; }
        [BsonElement("geometry")]
        public Geometry Geometry { get; set; } // geojson 2D
    }
}
