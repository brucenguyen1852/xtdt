﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.Country
{
    public interface ICountryRepository: IRepository<Country, Guid>
    {
        Task<List<Country>> GetListAsync(
            string filterText = null,
            bool? isDeleted = null,
            string name = null,
            string description = null,
            int? level = null,
            string type = null,
            string code = null,
            string search = null,
            string fullCode = null,
            string sorting = null,
            CancellationToken cancellationToken = default
        );
    }
}
