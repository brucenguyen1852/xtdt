﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Repositories;

namespace IoT.HTKT.Country
{
    public interface IGeojsonCountryRepository : IRepository<GeojsonCountry, Guid>
    {

    }
}
