﻿namespace IoT.HTKT
{
    public static class HTKTDbProperties
    {
        public static string DbTablePrefix { get; set; } = "HTKT";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "HTKT";
    }
}
