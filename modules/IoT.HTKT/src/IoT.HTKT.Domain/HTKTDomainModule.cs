﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace IoT.HTKT
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(HTKTDomainSharedModule)
    )]
    public class HTKTDomainModule : AbpModule
    {

    }
}
