﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.Layer
{
    public class Directory : AuditedAggregateRoot<Guid>
    {
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string ParentId { get; set; }
        public string Search { get; set; }
    }
}
