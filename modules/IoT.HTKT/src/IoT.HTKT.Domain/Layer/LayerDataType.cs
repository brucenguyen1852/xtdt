﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.Layer
{
    public class LayerDataType : AuditedAggregateRoot<Guid>
    {
        public bool IsDeleted { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
