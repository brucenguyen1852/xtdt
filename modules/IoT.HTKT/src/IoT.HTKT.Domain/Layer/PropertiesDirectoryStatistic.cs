﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.Layer
{
    public class PropertiesDirectoryStatistic : AuditedAggregateRoot<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public int Count { get; set; }
    }
}
