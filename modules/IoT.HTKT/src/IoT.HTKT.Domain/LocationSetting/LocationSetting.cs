﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.LocationSetting
{
    public class LocationSetting : AuditedAggregateRoot<Guid>
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
