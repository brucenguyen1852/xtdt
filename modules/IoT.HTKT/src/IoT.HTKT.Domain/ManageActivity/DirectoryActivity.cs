﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.ManageActivity
{
    public class DirectoryActivity : AuditedAggregateRoot<Guid>
    {
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string ParentId { get; set; }
        public string Search { get; set; }
        public string IdDirectory { get; set; } // id lớp dữ liệu cho hoạt động
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
