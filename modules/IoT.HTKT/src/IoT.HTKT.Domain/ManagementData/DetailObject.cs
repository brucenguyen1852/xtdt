﻿using IoT.HTKT.Layer;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.ManagementData
{
    public class DetailObject : AuditedAggregateRoot<Guid>
    {
        public string IdMainObject { get; set; }
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
        public string IdDirectory { get; set; } = string.Empty; //Id của thư mục
    }
}
