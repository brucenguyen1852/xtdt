﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.ManagementData
{
    public class DocumentActivity : AuditedAggregateRoot<Guid>
    {
        public string IdMainObject { get; set; } // id của đối tượng
        public string IdActivity { get; set; } // id của hoạt động
        public string NameDocument { get; set; } //Tên hoặc tự đề của file
        public string IconDocument { get; set; } //avatar hoặc biểu tượng của file
        public string UrlDocument { get; set; } //link url biểu tượng
        public string NameFolder { get; set; } //Thư mục của file
        public int OrderDocument { get; set; } //thứ tự sắp xếp
        public Dictionary<string, object> Tags { get; set; } // properties mở rộng
    }
}
