﻿using IoT.HTKT.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.ManagementData
{
    public class MainObject : AuditedAggregateRoot<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string NameObject { get; set; } // Tên đối tượng
        public Geometry Geometry { get; set; } // geojson 2D
        public bool IsCheckImplementProperties { get; set; } = true; // kiểm tra có kết thừa properties của lớp hay không
        public PropertiesGeojson Properties { get; set; } = new PropertiesGeojson(); // properties geojson 2D
        public Object3D object3D { get; set; } //đối tượng 3D
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
        public string SearchObject { get; set; } = string.Empty;
        public string QRCodeObject { get; set; } = string.Empty;

        public string District { get; set; } // Quận/ Huyện

        public string WardDistrict { get; set; } // Xã/ Phường
    }
}
