﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.HTKT.SettingData
{
    public class LayerBaseMap : AuditedAggregateRoot<Guid>
    {
        public string NameBasMap { get; set; } // tên bản đồ nền
        public string Link { get; set; } // đường dẫn
        public int Order { get; set; } //thứ tự
        public string Image { get; set; } //hình ảnh đại điện
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
