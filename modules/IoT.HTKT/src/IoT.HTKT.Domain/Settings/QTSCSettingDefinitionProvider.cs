﻿using Volo.Abp.Settings;

namespace IoT.HTKT.Settings
{
    public class QTSCSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from QTSCSettings class.
             */
        }
    }
}