﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace IoT.HTKT
{
    [DependsOn(
        typeof(HTKTApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class HTKTHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "HTKT";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(HTKTApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
