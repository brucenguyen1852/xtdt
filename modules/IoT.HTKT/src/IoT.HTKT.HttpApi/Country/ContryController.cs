﻿using IoT.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Extensions = IoT.Common.Extensions;

namespace IoT.HTKT.Country
{
    /// <summary>
    /// country controller
    /// </summary>
    public class ContryController : HTKTController
    {
        /// <summary>
        /// interface country service
        /// </summary>
        private readonly ICountryService _contryService;
        /// <summary>
        /// interface geojson country service
        /// </summary>
        private readonly IGeojsonCountryService _geosjonContryService;
        /// <summary>
        /// interface configuration
        /// </summary>
        private readonly IConfiguration _configuration;
        /// <summary>
        /// constructor country controller
        /// </summary>
        /// <param name="contryService"></param>
        /// <param name="configuration"></param>
        /// <param name="geosjonContryService"></param>
        public ContryController(ICountryService contryService, IConfiguration configuration, IGeojsonCountryService geosjonContryService)
        {
            _contryService = contryService;
            _configuration = configuration;
            _geosjonContryService = geosjonContryService;
        }
        /// <summary>
        /// update data country map4 to database
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("update-data-country")]
        public async Task<ActionResult> UpdateDataCountry(string code)
        {
            try
            {
                var endpoint = _configuration.GetValue<string>("SettingMap:Map.EndPoint");
                var uri = "/map/country/children/" + code;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(endpoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = new HttpResponseMessage();
                    response = await client.GetAsync(uri).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        dynamic obj = JObject.Parse(result);
                        if (obj.code == "ok")
                        {
                            List<CreateUpdateCountryDto> listCreate = new List<CreateUpdateCountryDto>();
                            foreach (var item in obj.result)
                            {
                                string search = Utils.ConvertTiengVietToAscii(Convert.ToString(item.name));
                                listCreate.Add(new CreateUpdateCountryDto()
                                {
                                    code = item.code,
                                    type = item.type,
                                    name = item.name,
                                    description = item.description,
                                    level = item.level,
                                    search = search,
                                    fullCode = item.code
                                });
                            }
                            bool check = await _contryService.CreateCountry(listCreate);
                        }
                    }
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// get country by level or code
        /// </summary>
        /// <param name="level"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("get-country-by-level-or-code")]
        public async Task<ActionResult> GetCountryByLevelOrCode(int level, string code)
        {
            try
            {
                var country = await _contryService.GetCountry(code, level);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, country));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// update geojson country map4 to database
        /// </summary>
        /// <param name="level"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("update-data-geojson-country")]
        public async Task<ActionResult> UpdateDataGeojsonCountry(int level,string code)
        {
            try
            {
                var country = await _contryService.GetCountry(code, level);
                string endpoint = _configuration.GetValue<string>("SettingMap:Map.EndPoint");
                string uri = string.Empty;
                List<CreateUpdateGeojsonCountryDto> listCreate = new List<CreateUpdateGeojsonCountryDto>();
                foreach (var item in country)
                {
                    uri = string.Format("/map/country/geometry?code={0}", item.code);
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(endpoint);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpResponseMessage response = new HttpResponseMessage();
                        response = await client.GetAsync(uri).ConfigureAwait(false);
                        if (response.IsSuccessStatusCode)
                        {
                            var result = await response.Content.ReadAsStringAsync();
                            dynamic obj = JObject.Parse(result);
                            if (obj.code == "ok")
                            {
                                var geometry = new Geometry();
                                //var a = obj.result;
                                //var b = obj.result.features;
                                //var c = JsonConvert.SerializeObject(obj.result.features[0].geometry);
                                var geometrytemp = JsonConvert.DeserializeObject<Geometry>(JsonConvert.SerializeObject(obj.result.features[0].geometry));
                                geometry.coordinates = Extensions.ConvertToListObject(geometrytemp.coordinates);
                                geometry.type = geometrytemp.type;
                                listCreate.Add(new CreateUpdateGeojsonCountryDto()
                                {
                                    IdCountry = item.Id.ToString(),
                                    CodeCountry = item.code,
                                    Geometry = geometry
                                });
                            }
                        }
                    }
                }
                await _geosjonContryService.UpdateGeometry(listCreate);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
        
        /// <summary>
        /// get geojson country by code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("get-geojson-country-by-code")]
        public async Task<ActionResult> GetGeojsonCountryByCode(string code)
        {
            try
            {
                var country = await _geosjonContryService.GetGeojsonCountryByCode(code);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, country));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
        /// <summary>
        /// get children geojson country by code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("children/get-geojson-country-by-code")]
        public async Task<ActionResult> GetGeojsonCountryByCodeChildren(string code)
        {
            try
            {
                var country = await _geosjonContryService.GetGeojsonCountryByCodeChildren(code);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, country));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
