﻿using ClosedXML.Excel;
using IoT.Common;
using IoT.HTKT.ManagementData;
using IoT.HTKT.PropertiesDirectoryActivity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.DetailActivity
{
    [RemoteService]
    [Route("api/htkt/detailActivity")]
    public class DetailActivityController : HTKTController
    {
        private readonly IDetailActivityService _detailActivityService;
        private readonly IPropertiesDirectoryActivityService _propertiesDirectoryActivityService;

        public DetailActivityController(IDetailActivityService detailActivityService,
                                        IPropertiesDirectoryActivityService propertiesDirectoryActivityService)
        {
            _detailActivityService = detailActivityService;
            _propertiesDirectoryActivityService = propertiesDirectoryActivityService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idMainObject"></param>
        /// <param name="idDirectory"></param>
        /// <returns></returns>
        [HttpGet("get-list-detail-activity")]
        public async Task<ActionResult> GetListDetailActivity(string idMainObject, string idDirectory)
        {
            try
            {
                var result = await this._detailActivityService.GetListDetailActivity(idMainObject, idDirectory, string.Empty);
                var obj = new
                {
                    items = result,
                    totalResult = result.Count
                };

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create-activity-detail")]
        public async Task<ActionResult> Create(FormDetailActivityModel param)
        {
            try
            {
                DateTime? dateActivity = null;
                try
                {
                    if (!string.IsNullOrWhiteSpace(param.DateActivity))
                    {
                        dateActivity = DateTime.ParseExact(param.DateActivity, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                }
                catch
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Dữ liệu ngày tháng không hợp lệ", "null"));
                }

                var activityDetailDto = new CreateDetailActivityDto()
                {
                    IdDirectory = param.IdDirectory,
                    IdMainObject = param.IdMainObject,
                    IdDirectoryActivity = param.IdDirectoryActivity,
                    NameDirectoryActivity = param.NameDirectoryActivity,
                    DateActivity = dateActivity,
                    ListProperties = param.ListProperties
                };

                var createActivityDetail = await _detailActivityService.CreateAsync(activityDetailDto);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, createActivityDetail));

            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("update-activity-detail")]
        public async Task<ActionResult> Edit(FormDetailActivityModel param)
        {
            try
            {
                if (string.IsNullOrEmpty(param.Id))
                {
                    return Ok(new { code = "error", message = "Không tìm thấy hoạt động", result = "null" });
                }

                DateTime? dateActivity = null;
                try
                {
                    if (!string.IsNullOrWhiteSpace(param.DateActivity))
                    {
                        dateActivity = DateTime.ParseExact(param.DateActivity, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                }
                catch
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Dữ liệu ngày tháng không hợp lệ", "null"));
                }

                var guiId = Guid.Parse(param.Id);

                var activityDetailDto = new CreateDetailActivityDto()
                {
                    IdDirectory = param.IdDirectory,
                    IdMainObject = param.IdMainObject,
                    IdDirectoryActivity = param.IdDirectoryActivity,
                    NameDirectoryActivity = param.NameDirectoryActivity,
                    DateActivity = dateActivity,
                    ListProperties = param.ListProperties
                };

                var editActivityDetail = await _detailActivityService.UpdateAsync(guiId, activityDetailDto);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, editActivityDetail));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete-activity-detail")]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var guid = Guid.Parse(id);

                    await _detailActivityService.DeleteAsync(guid);

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Id không được bỏ trống", false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="idDirectoryAcitivty"></param>
        /// <returns></returns>
        [HttpGet("check-delete-properties")]
        public ActionResult CheckDeleteProperties(string code, string idDirectoryAcitivty)
        {
            try
            {
                if (string.IsNullOrEmpty(idDirectoryAcitivty))
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "IdDirectoryAcitivty không được bỏ trống", false));
                }

                var result = _detailActivityService.CheckExitsCode(code, idDirectoryAcitivty);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        #region import 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [HttpPost("importdata")]
        public async Task<ActionResult> ImportData([FromForm] ImportDetailActivityDto Input)
        {
            try
            {
                if (string.IsNullOrEmpty(Input.DirectoryActivityId) || string.IsNullOrEmpty(Input.IdDirectory) || string.IsNullOrEmpty(Input.IdMainObject))
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Id không được bỏ trống", false));
                }

                if (Input.Files == null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "File không được bỏ trống", false));
                }

                //var guidIdDirectoryActivity = Guid.Parse(Input.DirectoryActivityId);

                var propertiesDirectoryActivity = await this._propertiesDirectoryActivityService.GetAsyncById(Input.DirectoryActivityId);

                if (propertiesDirectoryActivity == null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy thuộc tính của hoạt động", false));
                }

                var lstProperties = propertiesDirectoryActivity.ListProperties;

                string fileExtension = Path.GetExtension(Input.Files.FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    using (var stream = new MemoryStream())
                    {
                        await Input.Files.CopyToAsync(stream);

                        using (ExcelPackage package = new ExcelPackage(stream))
                        {
                            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                            ExcelWorksheet workSheet = package.Workbook.Worksheets.ElementAt(0);
                            int totalRows = workSheet.Dimension.Rows;
                            int totalCols = workSheet.Dimension.Columns;

                            int totalEmpty = 0;

                            var lstColProperties = new List<CodePropertiesImport>();
                            var lstCreate = new List<CreateDetailActivityDto>();

                            for (int i = 1; i <= totalRows; i++)
                            {
                                if (i == 2)
                                {
                                    // lấy vị trí cột của từng thuộc tính dựa vào code
                                    for (int j = 4; j <= totalCols; j++)
                                    {
                                        var proPerty = workSheet.Cells[i, j].Value == null ? "" : workSheet.Cells[i, j].Value.ToString();
                                        if (string.IsNullOrEmpty(proPerty))
                                        {
                                            break;
                                        }
                                        var detailProperties = lstProperties.Find(x => x.CodeProperties == proPerty);
                                        if (detailProperties != null)
                                        {
                                            if (detailProperties.TypeProperties != "image" && detailProperties.TypeProperties != "link" && detailProperties.TypeProperties != "file")
                                            {
                                                lstColProperties.Add(new CodePropertiesImport { Col = j, Code = proPerty });
                                            }
                                        }

                                    }
                                }

                                if (i > 2)
                                {
                                    try
                                    {
                                        var STT = workSheet.Cells[i, 1].Value == null ? "" : workSheet.Cells[i, 1].Value.ToString();
                                        var Name = workSheet.Cells[i, 2].Value == null ? "" : workSheet.Cells[i, 2].Value.ToString();
                                        var Date = workSheet.Cells[i, 3].Value == null ? "" : workSheet.Cells[i, 3].Value.ToString();

                                        if (string.IsNullOrEmpty(STT) || string.IsNullOrEmpty(Name)) // kiểm tra nếu không có data thì thoát 
                                        {
                                            totalEmpty++;
                                            if (totalEmpty > 3)
                                            {
                                                break;
                                            }
                                        }

                                        var lstpropertiesValue = new List<PropertiesObject>();
                                        if (lstColProperties.Count > 0) // set value thuộc tính của hoạt động
                                        {
                                            // import thuộc tính 
                                            ImportProperties(ref lstColProperties, ref lstpropertiesValue, ref workSheet, lstProperties, i);
                                        }

                                        var dateActivty = DateTime.Now;
                                        try
                                        {
                                            dateActivty = (!string.IsNullOrEmpty(Date) ? DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture) : DateTime.Now);
                                        }
                                        catch
                                        {
                                            dateActivty = DateTime.Now;
                                        }

                                        var activityDetailDto = new CreateDetailActivityDto()
                                        {
                                            IdDirectory = Input.IdDirectory,
                                            IdMainObject = Input.IdMainObject,
                                            IdDirectoryActivity = Input.DirectoryActivityId,
                                            NameDirectoryActivity = Name,
                                            DateActivity = dateActivty,
                                            ListProperties = lstpropertiesValue
                                        };

                                        await this._detailActivityService.CreateAsync(activityDetailDto);

                                        lstCreate.Add(activityDetailDto);
                                    }
                                    catch
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Nhập file hoạt động thành công", true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        private void ImportProperties(ref List<CodePropertiesImport> lstColProperties, // danh sách cột của từng thuộc tính 
                                      ref List<PropertiesObject> lstpropertiesValue, // danh sách thuộc tính trả về
                                      ref ExcelWorksheet workSheet, // excel
                                      List<DetailPropertiesActivity> lstProperties, // danh sách thông tin chi tiết của từng thuộc tính trong hệ thống
                                      int row) // hàng
        {
            foreach (var item in lstColProperties)
            {
                var valueProperty = workSheet.Cells[row, item.Col].Value == null ? "" : workSheet.Cells[row, item.Col].Value.ToString(); // value thuộc tính trong file import
                var proPertyDetai = lstProperties.Find(x => x.CodeProperties == item.Code); // thông tin chi tiết thuộc tính trong hệ thống

                if (proPertyDetai != null)
                {
                    if (proPertyDetai.TypeProperties != "image" && proPertyDetai.TypeProperties != "link" && proPertyDetai.TypeProperties != "file")
                    {
                        if (proPertyDetai.TypeProperties == "checkbox" || proPertyDetai.TypeProperties == "list" || proPertyDetai.TypeProperties == "radiobutton")
                        {
                            // nếu thuộc tính là dạng danh sách như (list, checkbox, radio) thì covert về đúng định dạng
                            var lstItemValue = JsonConvert.DeserializeObject<List<ItemValueProperties>>(proPertyDetai.DefalutValue);
                            if (proPertyDetai.TypeProperties == "checkbox")
                            {
                                var arr = valueProperty.Split(',');
                                var value = string.Empty;
                                foreach (var itemValue in arr)
                                {
                                    var cheeck = lstItemValue.Find(x => x.Name == itemValue);
                                    if (cheeck != null)
                                    {
                                        value += "," + cheeck.Code;
                                    }
                                }
                                valueProperty = value.Substring(1);
                            }
                            else
                            {
                                foreach (var itemValue in lstItemValue)
                                {
                                    itemValue.Checked = false;
                                    if (itemValue.Name == valueProperty)
                                    {
                                        itemValue.Checked = true;
                                    }
                                }

                                valueProperty = JsonConvert.SerializeObject(lstItemValue);
                            }
                        }

                        // lưu thuộc tính value vào
                        lstpropertiesValue.Add(new PropertiesObject
                        {
                            NameProperties = proPertyDetai.NameProperties,
                            CodeProperties = proPertyDetai.CodeProperties,
                            DefalutValue = valueProperty,
                            OrderProperties = proPertyDetai.OrderProperties,
                            IsShow = proPertyDetai.IsShow,
                            IsView = proPertyDetai.IsView,
                            TypeProperties = proPertyDetai.TypeProperties,
                            TypeSystem = proPertyDetai.TypeSystem
                        });

                    }
                }
            }

            // set value cho những thuộc tính còn lại 
            var lst = lstpropertiesValue.ToList();
            //var AnotherProperties = lstProperties.FindAll(x => x.TypeProperties == "image" || x.TypeProperties == "link" || x.TypeProperties == "file");
            var AnotherProperties = lstProperties.FindAll(x => !lst.Select(y => y.TypeProperties).Contains(x.TypeProperties) && x.TypeSystem == 3);

            foreach (var item in AnotherProperties)
            {
                lstpropertiesValue.Add(new PropertiesObject
                {
                    NameProperties = item.NameProperties,
                    CodeProperties = item.CodeProperties,
                    DefalutValue = string.IsNullOrEmpty(item.DefalutValue) ? string.Empty : item.DefalutValue,
                    OrderProperties = item.OrderProperties,
                    IsShow = item.IsShow,
                    IsView = item.IsView,
                    TypeProperties = item.TypeProperties,
                    TypeSystem = item.TypeSystem
                });

            }
        }

        #endregion

        #region export first
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idMainObject"></param>
        /// <param name="idDirectory"></param>
        /// <param name="idDirectoryActivity"></param>
        /// <returns></returns>
        [HttpGet("exportdata")]
        public async Task<ActionResult> ExportData(string idMainObject, string idDirectory, string idDirectoryActivity)
        {
            var Messageee = "";
            try
            {
                Messageee += "start# ";
                DataTable dt = await GetData(idMainObject, idDirectory, idDirectoryActivity);
                string fileName = "Danh_sach_hoat_dong.xlsx";

                // create avatars directory if not exist
                var serverAvatarsPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/HoatDong");

                if (!System.IO.Directory.Exists(serverAvatarsPath))
                {
                    System.IO.Directory.CreateDirectory(serverAvatarsPath);
                }

                string oldPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/HoatDong/" + fileName);
                if (System.IO.File.Exists(oldPath))
                {
                    System.IO.File.Delete(oldPath);
                }

                Messageee += "WorkBOOk# ";
                using (XLWorkbook wb = new XLWorkbook())
                {
                    Messageee += "Add DataTable in worksheet";
                    //Add DataTable in worksheet  
                    var wsDep = wb.Worksheets.Add(dt);

                    for (int i = 1; i <= dt.Columns.Count; i++)
                    {
                        wsDep.Cell(1, i).Style.Fill.SetBackgroundColor(XLColor.GreenPigment);
                        wsDep.Cell(2, i).Style.Fill.SetBackgroundColor(XLColor.GreenPigment);
                    }

                    Messageee += "/AdjustToContents# ";
                    wsDep.Columns().AdjustToContents();

                    Messageee += "/SAVES# ";
                    wb.SaveAs(oldPath);

                    Messageee += "/ASVEt# ";
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        private async Task<DataTable> GetData(string idMainObject, string idDirectory, string idDirectoryActivity)
        {
            //Creating DataTable  
            DataTable dt = new DataTable();
            var xlistdata = await this._detailActivityService.GetListDetailActivity(idMainObject, idDirectory, string.Empty);
            var propertiesDirectoryActivity = await this._propertiesDirectoryActivityService.GetAsyncById(idDirectoryActivity);
            xlistdata = xlistdata.Where(x => x.IdDirectoryActivity == idDirectoryActivity).ToList();
            //Setiing Table Name  
            dt.TableName = "Danh_sach_hoat_dong";

            var arrayCollumExits = new List<string>(); // danh sách kiểm tra những tên collum đã tồn tại
            //Add Columns  
            dt.Columns.Add("STT", typeof(string));
            dt.Columns.Add("Tên hoạt động của dữ liệu", typeof(string));
            dt.Columns.Add("Ngày tháng hoạt động.", typeof(string));

            arrayCollumExits.Add("STT");
            arrayCollumExits.Add("Tên hoạt động của dữ liệu");
            arrayCollumExits.Add("Ngày tháng hoạt động.");

            if (propertiesDirectoryActivity == null)
            {
                return null;
            }

            if (xlistdata.Count == 0)
            {
                return null;
            }

            DataRow row = dt.NewRow();
            row["STT"] = "";
            row["Tên hoạt động của dữ liệu"] = "NameDirectoryActivity";
            row["Ngày tháng hoạt động."] = "DateActivity";

            var lstProperties = propertiesDirectoryActivity.ListProperties.Where(x => x.TypeSystem != 1 && x.TypeProperties != "file").OrderBy(x => x.OrderProperties).ToList(); // danh sách thuộc tính của lớp hoạt động

            foreach (var item in lstProperties)
            {
                if (item.TypeProperties != "file")
                {
                    var keyName = item.NameProperties;
                    if (arrayCollumExits.Contains(item.NameProperties))
                    {
                        keyName += "_Info";
                    }

                    //arrayCollumExits.Add(keyName);

                    dt.Columns.Add(keyName, typeof(string));
                    row[keyName] = item.CodeProperties;
                }
            }

            dt.Rows.Add(row);

            var stt = 1;
            //Add Rows in DataTable 
            foreach (var item in xlistdata)
            {
                DataRow rowItem = dt.NewRow();
                rowItem["STT"] = stt.ToString();
                rowItem["Tên hoạt động của dữ liệu"] = string.IsNullOrEmpty(item.NameDirectoryActivity) ? string.Empty : item.NameDirectoryActivity;
                rowItem["Ngày tháng hoạt động."] = item.DateActivity?.AddDays(1).ToString("dd/MM/yyyy");

                //var valueProperties = item.ListProperties.Where(x => x.TypeSystem != 1 && x.TypeProperties != "file").OrderBy(x => x.OrderProperties);

                foreach (var item2 in lstProperties)
                {
                    try
                    {
                        var keyName = item2.NameProperties;
                        if (arrayCollumExits.Contains(item2.NameProperties))
                        {
                            keyName += "_Info";
                        }

                        var property = item.ListProperties.Find(x => x.CodeProperties == item2.CodeProperties); // tìm thuộc tính của hoạt động trong lớp thuộc tính

                        if (property != null)
                        {
                            if (item2.TypeProperties == "checkbox" || item2.TypeProperties == "list" || item2.TypeProperties == "radiobutton")
                            {
                                rowItem[keyName] = string.Empty;
                                try
                                {
                                    var lstItems = JsonConvert.DeserializeObject<List<ItemValueProperties>>(item2.DefalutValue);

                                    if (lstItems != null)
                                    {
                                        if (string.IsNullOrEmpty(property.DefalutValue))
                                        {
                                            rowItem[keyName] = property.DefalutValue;
                                        }
                                        else
                                        {
                                            var lstValueDefault = property.DefalutValue.Split(",");
                                            var valueItem = lstItems.FindAll(x => lstValueDefault.Contains(x.Code));

                                            if (valueItem != null && valueItem.Count > 0)
                                            {
                                                if (item2.TypeProperties != "checkbox")
                                                {
                                                    rowItem[keyName] = valueItem.FirstOrDefault().Name;
                                                }
                                                else
                                                {
                                                    rowItem[keyName] = string.Join(",", valueItem.Select(x => x.Name));
                                                }
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                    rowItem[keyName] = property.DefalutValue;
                                }
                            }
                            else
                            {
                                try
                                {
                                    switch (item2.TypeProperties)
                                    {
                                        case "text":
                                            string sText = HTML.HtmlToPlainText(property.DefalutValue);
                                            rowItem[keyName] = sText.ReplaceFirst("\n", "", StringComparison.Ordinal);
                                            break;
                                        case "image":
                                            if (string.IsNullOrEmpty(property.DefalutValue))
                                            {
                                                rowItem[keyName] = string.Empty;
                                            }
                                            else
                                            {
                                                var array = JsonConvert.DeserializeObject<List<ImageLinkUrl>>(property.DefalutValue);
                                                rowItem[keyName] = string.Join(Environment.NewLine, array.Select(x => x.url));
                                            }
                                            break;
                                        case "link":
                                            if (string.IsNullOrEmpty(property.DefalutValue))
                                            {
                                                rowItem[keyName] = string.Empty;
                                            }
                                            else
                                            {
                                                var arrayLink = JsonConvert.DeserializeObject<List<ImageLinkUrl>>(property.DefalutValue);
                                                rowItem[keyName] = string.Join(Environment.NewLine, arrayLink.Select(x => x.url));
                                            }
                                            break;
                                        default:
                                            rowItem[keyName] = property.DefalutValue;
                                            break;
                                    }
                                }
                                catch
                                {
                                    rowItem[keyName] = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            rowItem[keyName] = string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        var keyName = item2.NameProperties;
                        if (arrayCollumExits.Contains(item2.NameProperties))
                        {
                            keyName += "_Info";
                        }

                        rowItem[keyName] = string.Empty;
                    }
                }

                dt.Rows.Add(rowItem);

                //dt.Rows.Add(item.Name, item.Code, item.StreetName, item.CityName, item.DistrictName,
                //            item.WardName, item.Address, item.ButtressSingle, item.ButtressDouble, item.StopLine, item.RouteCount,
                //            item.RouteCode, item.HasBusStopHome, item.Description, item.Status, item.BusStopType, item.ProjectName, item.Latitude, item.Longitude);
                stt++;
            }

            dt.AcceptChanges();
            return dt;
        }
        #endregion
    }

    public class CodePropertiesImport
    {
        public int Col { get; set; }
        public string Code { get; set; }
    }

    public class ImageLinkUrl
    {
        public string id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }

    public class ItemValueProperties
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("checked")]
        public bool Checked { get; set; }

        [JsonProperty("action")]
        public string Aciton { get; set; }
    }
}
