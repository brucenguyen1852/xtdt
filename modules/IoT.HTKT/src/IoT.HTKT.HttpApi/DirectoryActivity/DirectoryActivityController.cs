﻿using IoT.Common;
using IoT.HTKT.Common;
using IoT.HTKT.DetailActivity;
using IoT.HTKT.DirectoryActivity;
using IoT.HTKT.PropertiesDirectoryActivity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.Activity
{
    [RemoteService]
    [Route("api/htkt/directoryactivity")]
    public class DirectoryActivityController : HTKTController
    {
        private readonly IDirectoryActivityService _directoryActivityService;
        private readonly IPropertiesDirectoryActivityService _PropertiesDirectoryActivityService;
        private readonly IDetailActivityService _detailActivityService;

        public DirectoryActivityController(IDirectoryActivityService directoryActivityService,
                                           IPropertiesDirectoryActivityService PropertiesDirectoryActivityService,
                                           IDetailActivityService detailActivityService)
        {
            _directoryActivityService = directoryActivityService;
            _PropertiesDirectoryActivityService = PropertiesDirectoryActivityService;
            _detailActivityService = detailActivityService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDirectory"></param>
        /// <returns></returns>
        [HttpGet("get-list-new")]
        public async Task<ActionResult> GetListNew(string idDirectory = "")
        {
            try
            {
                var list = await _directoryActivityService.GetListDirectoryActivityProperties(idDirectory);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDirectory"></param>
        /// <returns></returns>
        [HttpGet("get-list-new-not-properties")]
        public async Task<ActionResult> GetListNewNotProperties(string idDirectory = "")
        {
            try
            {
                var list = await _directoryActivityService.GetListDirectoryActivityNotProperties(idDirectory);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("create-folder")]
        public async Task<ActionResult> CreateFolder(CreateDirectoryActivityDto dto)
        {
            try
            {
                var create = new CreateDirectoryActivityDto();
                create.IsDeleted = false;
                create.Name = dto.Name;
                create.ParentId = dto.ParentId;
                create.Level = dto.Level;
                create.Type = dto.TypeDirectory ? TypeDirectory.FOLDER : TypeDirectory.FILE;
                create.Search = "";
                create.IdDirectory = dto.IdDirectory;
                var rs = await _directoryActivityService.CreateAsync(create);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, rs));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete-folder")]
        public async Task<ActionResult> DeleteFolder(string id)
        {
            try
            {
                var lsUsed = await this._detailActivityService.GetListDetailActivity(string.Empty, string.Empty, id);
                if (lsUsed.Count > 0)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Loại hoạt động đang được sử dụng không được xóa", false));
                }
                else
                {
                    var rs = await _directoryActivityService.DeleteFolder(id);
                    if (rs)
                    {
                        return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "xóa thành công", true));
                    }
                    else
                    {
                        return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "xóa thất bại", false));
                    }
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="idDirectory"></param>
        /// <returns></returns>
        [HttpGet("update-name")]
        public async Task<ActionResult> UpdateName(string id, string name, string idDirectory)
        {
            try
            {
                var rs = await _directoryActivityService.UpdateName(id, name, idDirectory);
                if (rs)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Cập nhật thành công", true));
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Cập nhật thất bại", false));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update-name-folder")]
        public async Task<IActionResult> UpdateNameFolder(DirectoryActivityFolderForm input)
        {
            try
            {
                var result = await _directoryActivityService.UpdateNameFolder(input.Id, input.Name);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cloneDiretoryDto"></param>
        /// <returns></returns>
        [HttpPost("clone-directory")]
        public async Task<ActionResult> CloneDirectoryProperties(CloneDiretoryActivityDto cloneDiretoryDto)
        {
            try
            {
                var create = new CreateDirectoryActivityDto();
                create.IsDeleted = false;
                create.Name = cloneDiretoryDto.Name;
                create.ParentId = cloneDiretoryDto.ParentId;
                create.Level = cloneDiretoryDto.Level;
                create.Type = cloneDiretoryDto.TypeDirectory ? TypeDirectory.FOLDER : TypeDirectory.FILE;
                create.Search = "";
                var rs = await _directoryActivityService.CreateAsync(create);
                if (rs != null)
                {
                    var objClone = await _PropertiesDirectoryActivityService.GetAsyncById(cloneDiretoryDto.IdImplement);
                    objClone.Id = Guid.NewGuid();
                    objClone.IdDirectoryActivity = rs.Id.ToString("D");
                    objClone.IdImplement = cloneDiretoryDto.IdImplement;
                    objClone.NameTypeDirectory = cloneDiretoryDto.Name;
                    objClone.CodeTypeDirectory = Utils.ConvertTiengVietToAscii(cloneDiretoryDto.Name).ToUpper();
                    objClone.CodeTypeDirectory = objClone.CodeTypeDirectory.Replace(" ", "");
                    CreatePropertiesDirectoryActivityDto data = objClone.Clone<CreatePropertiesDirectoryActivityDto>();
                    var result = await _PropertiesDirectoryActivityService.CreateAsync(data);
                    if (result != null)
                    {
                        return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                    }
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, string.Empty, false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("check-delete-activity")]
        public async Task<ActionResult> CheckDeleteActive(string id)
        {
            try
            {
                var result = await _directoryActivityService.CheckDeleteActivity(id);
                if (result)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, string.Empty, result));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list-count")]
        public async Task<ActionResult> GetListCount()
        {
            try
            {
                var list = await _directoryActivityService.GetListCountDirectoryActivity();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
