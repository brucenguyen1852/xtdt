﻿using IoT.Common.File;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using IoT.Common.FileApp;
using IoT.Common.FileNetApp;
using IoT.Common;

namespace IoT.HTKT.File
{
    [RemoteService]
    [Route("api/htkt/file")]
    public class FileController : HTKTController
    {
        private IConfiguration configuration;
        private IHttpClientFactory httpClientFactory;
        private FileService _fileService;
        private FileAppService _fileAppService;
        private FileNetAppService _fileNetAppService;
        private string SettingApp;
        public FileController(IConfiguration _configuration, IHttpClientFactory _httpClientFactory)
        {
            configuration = _configuration;
            httpClientFactory = _httpClientFactory;
            _fileService = new FileService(configuration, httpClientFactory);
            _fileAppService = new FileAppService(configuration);
            _fileNetAppService = new FileNetAppService(configuration);
            SettingApp = configuration.GetSection("SettingUploadFile:TypeUpload").Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("image")]
        public async Task<ActionResult> UploadImageAsync(IFormFile file)
        {
            FileView result;
            try
            {
                switch (SettingApp)
                {
                    case "AppInfo":
                        var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
                        result = await _fileAppService.UploadFileAsync(file, TypeFile.Image, urlHost);
                        break;
                    case "StorageNetApp":
                        result = await _fileNetAppService.UploadFileAsync(file, TypeFile.Image);
                        break;
                    default:
                        result = await _fileService.UploadFileAsync(file, TypeFile.Image);
                        break;
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("deleteimage")]
        public async Task<ActionResult> DeleteImageAsync([FromBody] FileDelete file)
        {
            try
            {
                bool check = false;
                switch (SettingApp)
                {
                    case "AppInfo":
                        var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
                        check = await _fileAppService.DeleteFileAsync(file.Url, TypeFile.Image);
                        break;
                    case "StorageNetApp":
                        check = await _fileNetAppService.DeleteFileAsync(file.Url, TypeFile.Image);
                        break;
                    default:
                        check = await _fileService.DeleteFileAsync(file.Url);
                        break;
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, check));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("object")]
        public async Task<ActionResult> UploadObjectAsync(IFormFile file)
        {
            FileView result;
            try
            {
                switch (SettingApp)
                {
                    case "AppInfo":
                        var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
                        result = await _fileAppService.UploadFileAsync(file, TypeFile.Obj, urlHost);
                        break;
                    case "StorageNetApp":
                        result = await _fileNetAppService.UploadFileAsync(file, TypeFile.Obj);
                        break;
                    default:
                        result = await _fileService.UploadFileAsync(file, TypeFile.Obj);
                        break;
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("texture")]
        public async Task<ActionResult> UploadTextureAsync(IFormFile file)
        {
            FileView result;
            try
            {

                switch (SettingApp)
                {
                    case "AppInfo":
                        var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
                        result = await _fileAppService.UploadFileAsync(file, TypeFile.Texture, urlHost);
                        break;
                    case "StorageNetApp":
                        result = await _fileNetAppService.UploadFileAsync(file, TypeFile.Texture);
                        break;
                    default:
                        result = await _fileService.UploadFileAsync(file, TypeFile.Texture);
                        break;
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("video")]
        [RequestSizeLimit(2147483647)]
        public async Task<ActionResult> UploadVideoAsync([FromForm] IFormFile file)
        {
            //FileView result;
            try
            {
                var result = await _fileService.UploadFileAsync(file, TypeFile.Video);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet("deletevideo")]
        public async Task<ActionResult> DeleteVideoAsync(string url)
        {
            try
            {
                var result = await _fileService.DeleteFileAsync(url);
                if (!result)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không thành công", false));
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("document")]
        [RequestSizeLimit(2147483647)]
        public async Task<ActionResult> UploadDocumentAsync([FromForm] IFormFile file)
        {
            FileView result;
            try
            {
                switch (SettingApp)
                {
                    case "AppInfo":
                        var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
                        result = await _fileAppService.UploadFileAsync(file, TypeFile.Document, urlHost);
                        break;
                    case "StorageNetApp":
                        result = await _fileNetAppService.UploadFileAsync(file, TypeFile.Document);
                        break;
                    default:
                        result = await _fileService.UploadFileAsync(file, TypeFile.Document);
                        break;
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("deletedocument")]
        public async Task<ActionResult> DeleteDocumentAsync([FromBody] FileDelete file)
        {
            try
            {
                bool check = false;
                switch (SettingApp)
                {
                    case "AppInfo":
                        var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
                        check = await _fileAppService.DeleteFileAsync(file.Url, TypeFile.Document);
                        break;
                    case "StorageNetApp":
                        check = await _fileNetAppService.DeleteFileAsync(file.Url, TypeFile.Document);
                        break;
                    default:
                        check = await _fileService.DeleteFileAsync(file.Url);
                        break;
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thành công", true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        //[HttpPost("image-icon")]
        //public async Task<ActionResult> UploadImageIconAsync(IFormFile file)
        //{
        //    FileViewIcon result;
        //    FileView resultNew;
        //    try
        //    {
        //        switch (SettingApp)
        //        {
        //            case "AppInfo":
        //                var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
        //                resultNew = await _fileAppService.UploadFileAsync(file, TypeFile.Image, urlHost);
        //                result = new FileViewIcon() { Id = resultNew.Id, Name = resultNew.Name, UrlIcon = resultNew.Url, UrlMap = resultNew.Url };
        //                break;
        //            case "StorageNetApp":
        //                //resultNew = await _fileNetAppService.UploadFileAsync(file, TypeFile.Image);
        //                resultNew = await _fileNetAppService.UploadFileAsync(file, TypeFile.Image);
        //                result = new FileViewIcon() { Id = resultNew.Id, Name = resultNew.Name, UrlIcon = resultNew.Url };
        //                resultNew = await _fileNetAppService.UploadFileAsync(file, TypeFile.Image);
        //                result.UrlMap = resultNew.Url;
        //                break;
        //            default:
        //                resultNew = await _fileService.UploadFileAsync(file, TypeFile.Image);
        //                result = new FileViewIcon() { Id = resultNew.Id, Name = resultNew.Name, UrlIcon = resultNew.Url };
        //                resultNew = await _fileService.UploadFileAsync(file, TypeFile.Image);
        //                result.UrlMap = resultNew.Url;
        //                //resultNew = await _fileService.UploadFileAsync(file, TypeFile.Document);
        //                break;
        //        }
        //        //resultNew = await _fileService.UploadFileAsync(file, TypeFile.Image);
        //        //result = new FileViewIcon() { Id = resultNew.Id, Name = resultNew.Name, UrlIcon = resultNew.Url };
        //        //resultNew = await _fileService.UploadFileAsync(file, TypeFile.Image);
        //        //result.UrlMap = resultNew.Url;
        //        return Ok(new { code = "ok", message = "", result = result });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Ok(new { code = "error", message = ex.Message, result = "null" });
        //    }
        //}
        //[HttpPost("deleteimage-icon")]
        //public async Task<ActionResult> DeleteImageIconAsync([FromBody] FileDeleteIcon file)
        //{
        //    try
        //    {
        //        bool check = false;
        //        switch (SettingApp)
        //        {
        //            case "AppInfo":
        //                //var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
        //                check = await _fileAppService.DeleteFileAsync(file.UrlMap, TypeFile.Image);
        //                break;
        //            case "StorageNetApp":
        //                check = await _fileNetAppService.DeleteFileAsync(file.UrlIcon, TypeFile.Image);
        //                check = await _fileNetAppService.DeleteFileAsync(file.UrlMap, TypeFile.Image);
        //                break;
        //            default:
        //                check = await _fileService.DeleteFileAsync(file.UrlIcon);
        //                check = await _fileService.DeleteFileAsync(file.UrlMap);
        //                //result = await _fileService.UploadFileAsync(file, TypeFile.Document);
        //                break;
        //        }
        //        //var check = await _fileService.DeleteFileAsync(file.UrlIcon);
        //        //var check1 = await _fileService.DeleteFileAsync(file.UrlMap);
        //        return Ok(new { code = "ok", message = "", result = check });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Ok(new { code = "error", message = ex.Message, result = false });
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("image-icon-app")]
        public async Task<ActionResult> UploadImageIconAsync(IFormFile file)
        {
            FileViewIcon result;
            FileView resultNew;
            try
            {
                var urlHost = string.Format("{0}://{1}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value);
                resultNew = await _fileAppService.UploadFileAsync(file, TypeFile.Image, urlHost);
                result = new FileViewIcon() { Id = resultNew.Id, Name = resultNew.Name, UrlIcon = resultNew.Url, UrlMap = resultNew.Url };
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("deleteimage-icon-app")]
        public async Task<ActionResult> DeleteImageIconAsync([FromBody] FileDeleteIcon file)
        {
            try
            {
                bool check = false;
                check = await _fileAppService.DeleteFileAsync(file.UrlMap, TypeFile.Image);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, check));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("test")]
        public ActionResult Test()
        {
            return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
        }

    }
}
