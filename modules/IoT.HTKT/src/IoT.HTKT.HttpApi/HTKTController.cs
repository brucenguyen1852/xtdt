﻿using IoT.HTKT.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace IoT.HTKT
{
    public abstract class HTKTController : AbpController
    {
        protected HTKTController()
        {
            LocalizationResource = typeof(HTKTResource);
        }
    }
}
