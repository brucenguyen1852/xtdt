﻿using Localization.Resources.AbpUi;
using IoT.HTKT.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace IoT.HTKT
{
    [DependsOn(
        typeof(HTKTApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class HTKTHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(HTKTHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<HTKTResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
