﻿using ClosedXML.Excel;
using IoT.Common;
using IoT.HTKT.Common;
using IoT.HTKT.DetailActivity;
using IoT.HTKT.Layer;
using IoT.HTKT.ManagementData;
using IoT.HTKT.PropertiesDirectory;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.Directory
{
    [RemoteService]
    [Route("api/htkt/directory")]
    public class DirectoryController : HTKTController
    {
        private readonly IDirectoryService _directoryService;
        private readonly IPropertiesDirectoryAppService _propertiesDirectoryAppService;

        //private readonly IMainObjectService _mainObjectService;
        //private readonly IDetailObjectService _detailObjectService;
        public DirectoryController(IDirectoryService directoryService,
                                   IPropertiesDirectoryAppService propertiesDirectoryAppService)
        {
            _directoryService = directoryService;
            _propertiesDirectoryAppService = propertiesDirectoryAppService;

            //_mainObjectService = mainObjectService;
            //_detailObjectService = detailObjectService;
        }
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList()
        {
            try
            {
                var result = await _directoryService.GetListDirectory();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("create-folder")]
        public async Task<ActionResult> CreateFolder(CreateDirectoryDto dto)
        {
            try
            {
                var create = new CreateDirectoryDto();
                create.IsDeleted = false;
                create.Name = dto.Name;
                create.ParentId = dto.ParentId;
                create.Level = dto.Level;
                create.Type = dto.TypeDirectory ? TypeDirectory.FOLDER : TypeDirectory.FILE;
                create.Search = "";
                var rs = await _directoryService.CreateAsync(create);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, rs));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("update-name")]
        public async Task<ActionResult> UpdateName(DirectoryFormModel dto)
        {
            try
            {
                var rs = await _directoryService.UpdateName(dto.Id, dto.Name);
                if (rs)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Cập nhật thành công", true));
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Cập nhật thất bại", false));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete-folder")]
        public async Task<ActionResult> DeleteFolder(string id)
        {
            try
            {
                var rs = await _directoryService.DeleteFolder(id);
                if (rs)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "xóa thành công", true));
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "xóa thất bại", false));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list-new")]
        public async Task<ActionResult> GetListNew()
        {
            try
            {
                var list = await _directoryService.GetListDirectoryProperties();

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list-layer-has-map")]
        public async Task<ActionResult> GetListLayerHasMap()
        {
            try
            {
                var list = await _directoryService.GetListDirectoryPropertiesHasMap();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list-new-layer")]
        public async Task<ActionResult> GetListNewLayer()
        {
            try
            {
                var list = await _directoryService.GetListDirectoryNotProperties();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cloneDiretoryDto"></param>
        /// <returns></returns>
        [HttpPost("clone-directory")]
        public async Task<ActionResult> CloneDirectoryProperties(CloneDiretoryDto cloneDiretoryDto)
        {
            try
            {
                var create = new CreateDirectoryDto();
                create.IsDeleted = false;
                create.Name = cloneDiretoryDto.Name;
                create.ParentId = cloneDiretoryDto.ParentId;
                create.Level = cloneDiretoryDto.Level;
                create.Type = cloneDiretoryDto.TypeDirectory ? TypeDirectory.FOLDER : TypeDirectory.FILE;
                create.Search = "";
                var rs = await _directoryService.CreateAsync(create);
                if (rs != null)
                {
                    var objClone = await _propertiesDirectoryAppService.GetAsyncById(cloneDiretoryDto.IdImplement);
                    objClone.Id = Guid.NewGuid();
                    objClone.IdDirectory = rs.Id.ToString("D");
                    objClone.IdImplement = cloneDiretoryDto.IdImplement;
                    objClone.NameTypeDirectory = cloneDiretoryDto.Name;
                    objClone.CodeTypeDirectory = Utils.ConvertTiengVietToAscii(cloneDiretoryDto.Name).ToUpper();
                    objClone.CodeTypeDirectory = objClone.CodeTypeDirectory.Replace(" ", "");
                    objClone.Location = cloneDiretoryDto.Location;
                    objClone.Zoom = cloneDiretoryDto.Zoom;
                    objClone.GroundMaptile = cloneDiretoryDto.GroundMaptile;
                    objClone.OptionDirectory = cloneDiretoryDto.OptionDirectory;

                    CreateUpdatePropertiesDirectoryDto data = objClone.Clone<CreateUpdatePropertiesDirectoryDto>();
                    data.BoundMaptile = cloneDiretoryDto.BoundMaptile;

                    var result = await _propertiesDirectoryAppService.CreateAsync(data);
                    if (result != null)
                    {
                        return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                    }
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("check-delete-layer")]
        public async Task<ActionResult> CheckDeleteLayer(string id)
        {
            try
            {
                var checkExits = await _directoryService.CheckDeleteLayer(id);
                if (checkExits)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, checkExits));
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, string.Empty, false));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-list-parent-directory-name")]
        public ActionResult GetListDirectoryTree(string id)
        {
            try
            {
                var lstParentName = new List<DirectoryDto>();
                lstParentName = this._directoryService.GetParentLayer(id);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, lstParentName));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }
    }
}
