﻿using IoT.Common;
using IoT.HTKT.PropertiesDirectory;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.Layer
{
    [RemoteService]
    [Route("api/HTKT/LayerDataType")]
    public class LayerDataTypeController : HTKTController
    {
        private readonly ILayerDataTypeService _layerDataTypeService;

        public LayerDataTypeController(ILayerDataTypeService layerDataTypeService)
        {
            _layerDataTypeService = layerDataTypeService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult> Create(CreateLayerDataTypeDto param)
        {
            try
            {
                var obj = await _layerDataTypeService.CreateAsync(param);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList()
        {
            try
            {
                var obj = await _layerDataTypeService.GetListDataTypeAsync();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
