﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.LayerBaseMap
{
    [RemoteService]
    [Route("api/htkt/layerBaseMap")]
    public class LayerBaseMapController : HTKTController
    {
        private readonly ILayerBaseMapService _layerBaseMapService;
        public LayerBaseMapController(ILayerBaseMapService layerBaseMapService)
        {
            _layerBaseMapService = layerBaseMapService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sorting"></param>
        /// <param name="SkipCount"></param>
        /// <param name="MaxResultCount"></param>
        /// <returns></returns>
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList(string Sorting, int SkipCount, int MaxResultCount)
        {
            try
            {
                var result = await _layerBaseMapService.GetListLayerBaseMap(Sorting, SkipCount, MaxResultCount);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create-layer-base-map")]
        public async Task<ActionResult> CreateLayerBaseMap(FormLayerBaseMap param)
        {
            try
            {
                var dto = new CreateUpdateLayerBaseMapDto()
                {
                    NameBasMap = param.NameBasMap,
                    Link = param.Link,
                    //Order = param.Order,
                    Image = param.Image
                };

                var createDto = await _layerBaseMapService.CreateAsync(dto);
                if (createDto != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, createDto));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Thêm mới thất bại", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("update-layer-base-map")]
        public async Task<ActionResult> UpdateLayerBaseMap(FormLayerBaseMap param)
        {
            try
            {
                if (string.IsNullOrEmpty(param.Id))
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Id không được bỏ trống", "null"));
                }

                var dto = new CreateUpdateLayerBaseMapDto()
                {
                    NameBasMap = param.NameBasMap,
                    Link = param.Link,
                    Order = param.Order,
                    Image = param.Image
                };

                var idGuid = Guid.Parse(param.Id);
                var updateDto = await _layerBaseMapService.UpdateAsync(idGuid, dto);

                if (updateDto != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, updateDto));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Cập nhật không thành công", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpGet("check-exits-order")]
        public ActionResult CheckOrderLayerBaseMap(string id, int order)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    var check = _layerBaseMapService.CheckExitsOrder(Guid.NewGuid(), order);
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Id không được bỏ trống", check));
                    //return new { code = "ok", type = check };
                }
                else
                {
                    var check = _layerBaseMapService.CheckExitsOrder(Guid.Parse(id), order);
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, check));
                    //return new { code = "ok", type = check };
                }
            }
            catch(Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message,false));
            }
        }
    }
}
