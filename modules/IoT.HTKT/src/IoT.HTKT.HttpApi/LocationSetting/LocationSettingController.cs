﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.LocationSetting
{
    [RemoteService]
    [Route("api/htkt/location-setting")]
    public class LocationSettingController : HTKTController
    {
        private readonly ILocationSettingService _locationSettingService;
        public LocationSettingController(ILocationSettingService locationSettingService)
        {
            _locationSettingService = locationSettingService;
        }

        [HttpGet("get-location-center")]
        public ActionResult GetLocationCenter()
        {
            try
            {
                var location = _locationSettingService.GetLocationCenter();

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, location));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        [HttpPost("create-location-center")]
        public async Task<ActionResult> UpdateLocationCenterAsync(CreateUpdateLocationSettingDto input)
        {
            try
            {
                var createDto = await _locationSettingService.CreateLocationCenter(input);
                if (createDto != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, createDto));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Thêm mới thất bại", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        [HttpPost("update-location-center")]
        public async Task<ActionResult> UpdateLocationCenterAsync(Guid id, CreateUpdateLocationSettingDto input)
        {
            try
            {
                var updateDto = await _locationSettingService.UpdateLocationCenter(id, input);
                if (updateDto != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, updateDto));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Cập nhật thất bại", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}

