﻿using IoT.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.ManagementData
{
    [RemoteService]
    [Route("api/HTKT/DocumentActivity")]
    public class DocumentActivityController : HTKTController
    {
        private readonly IDocumentActivityService _documentActivityService;
        private readonly IMainObjectService _mainObjectService;

        public DocumentActivityController(IDocumentActivityService documentActivityService,
            IMainObjectService mainObjectService)
        {
            _documentActivityService = documentActivityService;
            _mainObjectService = mainObjectService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-all-file")]
        public async Task<ActionResult> GetAllFile()
        {
            try
            {
                var list = await _documentActivityService.GetAllFile();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
                //return Ok(new { code = "ok", message = "", result = list, total = list.Count });
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-file-by-objectId")]
        public async Task<ActionResult> GetFileByObjectId(string id)
        {
            try
            {
                var objectMainId = Guid.Parse(id);

                var objectMain = await _mainObjectService.GetAsync(objectMainId);

                if (objectMain == null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy đối tượng", "null"));
                }
                var listDocument = await _documentActivityService.getListDocumentbyObjectMainId(objectMainId);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, listDocument));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-file-by-DetailActivityId")]
        public async Task<ActionResult> GetFileByActivitytId(string id)
        {
            try
            {
                var listDocument = await _documentActivityService.getListDocumentbyActivityId(id);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, listDocument));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="Files"></param>
        /// <returns></returns>
        [HttpPost("add-file")]
        public async Task<ActionResult> AddFile([FromForm] CreateOrUpdateDocumentActivityDto input, IFormFile Files)
        {
            try
            {
                if (Files != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads");
                    if (!System.IO.Directory.Exists(serverFilePath))
                    {
                        System.IO.Directory.CreateDirectory(serverFilePath);
                    }

                    var fileName = $"{Guid.NewGuid()}{Path.GetExtension(Files.FileName)}";
                    var path = Path.Combine(serverFilePath, fileName);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Files.CopyTo(stream);
                    }

                    var obj = new CreateOrUpdateDocumentActivityDto();
                    obj.IdMainObject = input.IdMainObject;
                    obj.IconDocument = input.IconDocument;
                    obj.NameDocument = input.NameDocument;
                    obj.UrlDocument = "/FileUploads/" + fileName;

                    var result = await _documentActivityService.CreateAsync(obj);

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thêm file thành công", result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "file chưa tồn tại", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add-file-document")]
        public async Task<ActionResult> AddFileDocument(List<CreateOrUpdateDocumentActivityDto> input)
        {
            try
            {
                foreach (var i in input)
                {
                    var result = await _documentActivityService.CreateAsync(i);
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thêm file thành công", "ok"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete-file")]
        public async Task<ActionResult> DeleteFile(string id)
        {
            try
            {
                var fileId = Guid.Parse(id);
                var file = await _documentActivityService.GetAsync(fileId);
                var arrFile = file.UrlDocument.Split("/");
                var fileName = arrFile[arrFile.Length - 1];
                if (file != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads/");

                    var lll = Path.Combine(serverFilePath, fileName);

                    if (System.IO.File.Exists(Path.Combine(serverFilePath, fileName)))
                    {
                        System.IO.File.Delete(Path.Combine(serverFilePath, fileName));
                    }
                }
                await _documentActivityService.DeleteAsync(fileId);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Xóa file thành công", true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        [HttpPost("change-file")]
        public async Task<ActionResult> changeFile(string id, CreateOrUpdateDocumentActivityDto input, IFormFile Files)
        {
            try
            {
                var IdOldFile = Guid.Parse(id);
                var OldFile = await _documentActivityService.GetAsync(IdOldFile);
                var OldFileArr = OldFile.UrlDocument.Split("/");
                var OldFileName = OldFileArr[OldFileArr.Length - 1];

                if (OldFile != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads/");

                    var lll = Path.Combine(serverFilePath, OldFileName);

                    if (System.IO.File.Exists(Path.Combine(serverFilePath, OldFileName)))
                    {
                        System.IO.File.Delete(Path.Combine(serverFilePath, OldFileName));
                    }

                    await _documentActivityService.DeleteAsync(IdOldFile);
                }

                if (Files != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads");
                    if (!System.IO.Directory.Exists(serverFilePath))
                    {
                        System.IO.Directory.CreateDirectory(serverFilePath);
                    }

                    var newFileName = $"{Guid.NewGuid()}{Path.GetExtension(Files.FileName)}";
                    var path = Path.Combine(serverFilePath, newFileName);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Files.CopyTo(stream);
                    }

                    var obj = new CreateOrUpdateDocumentActivityDto();
                    obj.IdMainObject = input.IdMainObject;
                    obj.IconDocument = input.IconDocument;
                    obj.NameDocument = input.NameDocument;
                    obj.UrlDocument = "/FileUploads/" + newFileName;

                    var result = await _documentActivityService.CreateAsync(obj);

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thay đổi file thành công", result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "File không tồn tại. Thêm mới file thành công", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
