﻿using IoT.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.ManagementData
{
    [RemoteService]
    [Route("api/HTKT/DocumentObject")]
    public class DocumentObjectController : HTKTController
    {
        private readonly IDocumentObjectService _documentObjectService;
        private readonly IMainObjectService _mainObjectService;

        public DocumentObjectController(IDocumentObjectService documentObjectService,
            IMainObjectService mainObjectService)
        {
            _documentObjectService = documentObjectService;
            _mainObjectService = mainObjectService;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-all-file")]
        public async Task<ActionResult> GetAllFile()
        {
            try
            {
                var list = await _documentObjectService.GetAllFile();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-file-by-objectId")]
        public async Task<ActionResult> GetFileByObjectId(string id)
        {
            try
            {
                var objectMainId = Guid.Parse(id);

                var objectMain = await _mainObjectService.GetAsync(objectMainId);

                if (objectMain == null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy đối tượng", "null"));
                }
                var listDocument = await _documentObjectService.getListDocumentbyObjectMainId(objectMainId);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, listDocument));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="Files"></param>
        /// <returns></returns>
        [HttpPost("add-file")]
        public async Task<ActionResult> AddFile([FromForm] CreateOrUpdateDocumentObjectDto input, IFormFile Files)
        {
            try
            {
                if (Files != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads");
                    if (!System.IO.Directory.Exists(serverFilePath))
                    {
                        System.IO.Directory.CreateDirectory(serverFilePath);
                    }

                    var fileName = $"{Guid.NewGuid()}{Path.GetExtension(Files.FileName)}";
                    var path = Path.Combine(serverFilePath, fileName);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Files.CopyTo(stream);
                    }

                    var obj = new CreateOrUpdateDocumentObjectDto();
                    obj.IdMainObject = input.IdMainObject;
                    obj.IconDocument = input.IconDocument;
                    obj.NameDocument = input.NameDocument;
                    obj.UrlDocument = "/FileUploads/" + fileName;

                    var result = await _documentObjectService.CreateAsync(obj);

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thêm file thành công", result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "file chưa tồn tại", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add-file-document")]
        public async Task<ActionResult> AddFileDocument(List<CreateOrUpdateDocumentObjectDto> input)
        {
            try
            {
                foreach (var i in input)
                {
                    var result = await _documentObjectService.CreateAsync(i);
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thêm file thành công", "ok"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        [HttpPost("delete-file-list-id")]
        public async Task<ActionResult> DeleteListId(List<string> listId)
        {
            try
            {
                var check = await _documentObjectService.DeleteListObject(listId);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thêm file thành công", check));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete-file")]
        public async Task<ActionResult> DeleteFile(string id)
        {
            try
            {
                var fileId = Guid.Parse(id);
                var file = await _documentObjectService.GetAsync(fileId);
                var arrFile = file.UrlDocument.Split("/");
                var fileName = arrFile[arrFile.Length - 1];
                if (file != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads/");

                    var lll = Path.Combine(serverFilePath, fileName);

                    if (System.IO.File.Exists(Path.Combine(serverFilePath, fileName)))
                    {
                        System.IO.File.Delete(Path.Combine(serverFilePath, fileName));
                    }
                }
                await _documentObjectService.DeleteAsync(fileId);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Xóa file thành công", true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <param name="Files"></param>
        /// <returns></returns>
        [HttpPost("change-file")]
        public async Task<ActionResult> changeFile(string id, CreateOrUpdateDocumentObjectDto input, IFormFile Files)
        {
            try
            {
                var IdOldFile = Guid.Parse(id);
                var OldFile = await _documentObjectService.GetAsync(IdOldFile);
                var OldFileArr = OldFile.UrlDocument.Split("/");
                var OldFileName = OldFileArr[OldFileArr.Length - 1];

                if (OldFile != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads/");

                    var lll = Path.Combine(serverFilePath, OldFileName);

                    if (System.IO.File.Exists(Path.Combine(serverFilePath, OldFileName)))
                    {
                        System.IO.File.Delete(Path.Combine(serverFilePath, OldFileName));
                    }

                    await _documentObjectService.DeleteAsync(IdOldFile);
                }

                if (Files != null)
                {
                    var serverFilePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileUploads");
                    if (!System.IO.Directory.Exists(serverFilePath))
                    {
                        System.IO.Directory.CreateDirectory(serverFilePath);
                    }

                    var newFileName = $"{Guid.NewGuid()}{Path.GetExtension(Files.FileName)}";
                    var path = Path.Combine(serverFilePath, newFileName);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Files.CopyTo(stream);
                    }

                    var obj = new CreateOrUpdateDocumentObjectDto();
                    obj.IdMainObject = input.IdMainObject;
                    obj.IconDocument = input.IconDocument;
                    obj.NameDocument = input.NameDocument;
                    obj.UrlDocument = "/FileUploads/" + newFileName;

                    var result = await _documentObjectService.CreateAsync(obj);

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thay đổi file thành công", result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "File không tồn tại. Thêm mới file thành công", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update-file-document-main-object")]
        public async Task<ActionResult> UpdateFileDocumentMainObject(UpdateDocumentMainObjectFormData input)
        {
            try
            {
                if (string.IsNullOrEmpty(input.idMainObject))
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "id null", false));
                }

                var lstResult = await this._documentObjectService.UpdateFileDocumentMainObject(input);
                if (lstResult != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Cập nhật thành công", true));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData,string.Empty, false));
            }
            catch(Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
