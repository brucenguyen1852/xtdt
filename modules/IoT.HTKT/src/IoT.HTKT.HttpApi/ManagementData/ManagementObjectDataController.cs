﻿using IoT.Common;
using IoT.HTKT.DetailActivity;
using IoT.HTKT.Object;
using IoT.HTKT.PropertiesDirectory;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.ManagementData
{
    [RemoteService]
    [Route("api/HTKT/ManagementData")]
    public class ManagementObjectDataController : HTKTController
    {
        private readonly IMainObjectService _mainObjectService;
        private readonly IDetailObjectService _detailObjectService;
        private readonly IDocumentObjectService _documentObjectService;
        private readonly IPropertiesDirectoryAppService _propertiesDirectoryAppService;
        public ManagementObjectDataController(IMainObjectService mainObjectService,
                                              IDetailObjectService detailObjectService,
                                              IDocumentObjectService documentObjectService,
                                              IPropertiesDirectoryAppService propertiesDirectoryAppService)
        {
            _mainObjectService = mainObjectService;
            _detailObjectService = detailObjectService;
            _documentObjectService = documentObjectService;
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTableViewModel"></param>
        /// <returns></returns>
        [HttpPost("get-list-datatable")]
        public ActionResult GetList([FromForm] DataTableViewModel dataTableViewModel)
        {
            try
            {
                var result = _mainObjectService.GetListObject(dataTableViewModel);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create-object")]
        public async Task<ActionResult> CreateObject(FormManagementDataModel param)
        {
            try
            {
                var mainObjectDto = new CreateUpdateMainObjectDto()
                {
                    IdDirectory = param.IdDirectory,
                    NameObject = param.NameObject,
                    Geometry = param.Geometry,
                    PropertiesGeojson = param.PropertiesGeojson,
                    IsCheckImplementProperties = param.IsCheckImplementProperties,
                    QRCodeObject = !string.IsNullOrEmpty(param.QRCodeObject) ? param.QRCodeObject : string.Empty,
                    District = param.District,
                    WardDistrict = param.WardDistrict
                };

                if (param.object3D != null)
                {
                    mainObjectDto.object3D = param.object3D;
                }

                var createMainObject = await _mainObjectService.CreateAsync(mainObjectDto);

                if (createMainObject != null)
                {
                    var detailObjectDto = new CreateUpdateDetailObjectDto()
                    {
                        IdMainObject = createMainObject.Id.ToString(),
                        ListProperties = param.ListProperties,
                        IdDirectory = createMainObject.IdDirectory
                    };

                    InfoObjectData resultdata = new InfoObjectData()
                    {
                        Id = createMainObject.Id.ToString(),
                        Geometry = createMainObject.Geometry,
                        IdDirectory = createMainObject.IdDirectory,
                        NameObject = createMainObject.NameObject,
                        object3D = createMainObject.object3D,
                        Properties = createMainObject.Properties,
                    };

                    var createDetailObject = await _detailObjectService.CreateAsync(detailObjectDto);

                    if (createDetailObject != null)
                    {
                        resultdata.ListProperties = createDetailObject.ListProperties;

                        return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, resultdata));
                    }
                    else
                    {
                        return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Thêm mới main object không thành công", "null"));
                    }
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Thêm mới main object không thành công", "null"));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("update-object")]
        public async Task<ActionResult> UpdateObject(FormManagementDataModel param)
        {
            try
            {
                InfoObjectData resultdata = new InfoObjectData();

                if (!string.IsNullOrEmpty(param.Id))
                {
                    var mainObjectDto = new CreateUpdateMainObjectDto()
                    {
                        IdDirectory = param.IdDirectory,
                        NameObject = param.NameObject,
                        Geometry = param.Geometry,
                        PropertiesGeojson = param.PropertiesGeojson,
                        IsCheckImplementProperties = param.IsCheckImplementProperties,
                        QRCodeObject = !string.IsNullOrEmpty(param.QRCodeObject) ? param.QRCodeObject : string.Empty
                    };

                    if (param.object3D != null)
                    {
                        mainObjectDto.object3D = param.object3D;
                    }

                    var idGuid = Guid.Parse(param.Id);
                    var updateMainObject = await _mainObjectService.UpdateAsync(idGuid, mainObjectDto);

                    if (updateMainObject != null)
                    {
                        var detailObject = await this._detailObjectService.GetDetailObjectByMainObjectId(param.Id);
                        if (detailObject != null)
                        {
                            var detailObjectDto = new CreateUpdateDetailObjectDto()
                            {
                                IdMainObject = param.Id,
                                IdDirectory = param.IdDirectory,
                                ListProperties = param.ListProperties
                            };

                            resultdata = new InfoObjectData()
                            {
                                Id = updateMainObject.Id.ToString(),
                                Geometry = updateMainObject.Geometry,
                                IdDirectory = updateMainObject.IdDirectory,
                                NameObject = updateMainObject.NameObject,
                                object3D = updateMainObject.object3D,
                                Properties = updateMainObject.Properties,
                            };

                            var updateDetailObject = await _detailObjectService.UpdateAsync(detailObject.Id, detailObjectDto);

                            if (updateDetailObject != null)
                            {
                                resultdata.ListProperties = updateDetailObject.ListProperties;
                                resultdata.Geometry.Coordinates = IoT.Common.Extensions.ConvertToListObject(resultdata.Geometry.Coordinates);

                                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, resultdata));
                            }
                            else
                            {
                                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Cập nhật detail object không thành công", "null"));
                            }
                        }
                        else
                        {
                            return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy detail object", "null"));
                        }
                    }
                    else
                    {
                        return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Cập nhật object không thành công", "null"));
                    }
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Id không được bỏ trống", "null"));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete-object")]
        public async Task<ActionResult> DeleteObject(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var guid = Guid.Parse(id);

                    await _mainObjectService.DeleteAsync(guid);

                    var detailObject = await this._detailObjectService.GetDetailObjectByMainObjectId(id);

                    if (detailObject != null)
                    {
                        await _detailObjectService.DeleteAsync(detailObject.Id);
                    }

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Id không được bỏ trống", false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        // upload qrcode main object
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("update-qrcode-object")]
        public async Task<ActionResult> UpdateQRCode(FormUpdateQRCode param)
        {
            try
            {
                if (!string.IsNullOrEmpty(param.Id))
                {
                    var check = await this._mainObjectService.UpdateQRCode(param);

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, check));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Id không được bỏ trống", false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sorting"></param>
        /// <param name="SkipCount"></param>
        /// <param name="MaxResultCount"></param>
        /// <returns></returns>
        [HttpGet("get-list-all")]
        private async Task<ActionResult> GetListAll(string Sorting, int SkipCount, int MaxResultCount)
        {
            try
            {
                var result = await _mainObjectService.GetListObjectAll(Sorting, SkipCount, MaxResultCount);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-object")]
        public async Task<ActionResult> GetObject(string id)
        {
            try
            {
                var result = await _mainObjectService.GetObject(id);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-object-exploit")]
        public async Task<ActionResult> GetObjectExploit(string id)
        {
            try
            {
                var result = await _mainObjectService.GetObjectExploit(id);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("deleted-object")]
        public async Task<ActionResult> DeletedObject(string id)
        {
            try
            {
                var result = await _mainObjectService.DeletedObject(id);
                if (result != false)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không thành công", result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("export-object")]
        public async Task<ActionResult> ExportAsync(string id)
        {
            try
            {
                var result = await _mainObjectService.GetObject(id);
                if (result != null)
                {
                    var infoObjectData = result.As<InfoObjectData>();
                    byte[] fileContents;
                    using (var stream = new MemoryStream())
                    {
                        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                        ExcelPackage Ep = new ExcelPackage(stream);
                        OfficeOpenXml.ExcelRange aRange;
                        //Microsoft.Office.Interop.Excel.Range excelCells =(Microsoft.Office.Interop.Excel.Range)excelWorksheet.get_Range("A3", "K3");
                        ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Object");
                        //aRange = Sheet.SelectedRange("A1", "E100");
                        Sheet.Cells[1, 1, 1, 3].Merge = true;
                        Sheet.Cells[1, 1, 1, 3].Value = "Thông tin chi tiết đối tượng " + "'" + infoObjectData.NameObject + "'";
                        Sheet.Cells[1, 1, 1, 3].AutoFitColumns();

                        //Sheet.Cells[1, 1, 1, 3].Style
                        Sheet.Cells[1, 1, 1, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        Sheet.Cells[1, 1, 1, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        Sheet.Cells[1, 1, 1, 3].Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                        Sheet.Cells[1, 1, 1, 3].Style.Font.Bold = true;
                        Sheet.Cells[1, 1, 1, 3].Style.Font.Size = 18;
                        Sheet.Cells[1, 1, 1, 3].Style.Border.BorderAround(ExcelBorderStyle.Medium);  //Dotted
                        Sheet.Cells[2, 1].Value = "STT";
                        Sheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.Medium);  //Dotted
                        Sheet.Cells[2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        Sheet.Cells[2, 2].Value = "Tên thuộc tính";
                        Sheet.Cells[2, 2].Style.Border.BorderAround(ExcelBorderStyle.Medium);  //Dotted
                        Sheet.Cells[2, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        Sheet.Cells[2, 3].Value = "Giá trị";
                        Sheet.Cells[2, 3].Style.Border.BorderAround(ExcelBorderStyle.Medium);  //Dotted
                        Sheet.Cells[2, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        // set header
                        Sheet.Cells[2, 1, 2, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        Sheet.Cells[2, 1, 2, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        Sheet.Cells[2, 1, 2, 3].Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                        Sheet.Cells[2, 1, 2, 3].Style.Font.Bold = true;
                        Sheet.Cells[2, 1, 2, 3].Style.Font.Italic = true;
                        Sheet.Cells[2, 1, 2, 3].Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        Sheet.Cells[2, 1, 2, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        int line = 2;
                        int stt = 1;

                        var directoryInfo = await this._propertiesDirectoryAppService.GetAsyncById(infoObjectData.IdDirectory);
                        var lstProperties = directoryInfo.ListProperties.Where(x => x.TypeSystem != 1).OrderBy(x => x.OrderProperties).ToList();

                        var listDocument = await _documentObjectService.getListDocumentbyObjectMainId(Guid.Parse(id));

                        foreach (var item in lstProperties)
                        {
                            var checkedProperty = infoObjectData.ListProperties.Find(x => x.CodeProperties == item.CodeProperties);

                            Sheet.Cells[line + 1, 1].Value = stt++;
                            Sheet.Cells[line + 1, 2].Value = item.NameProperties;

                            if (item.CodeProperties == "Title")
                            {
                                Sheet.Cells[line + 1, 2].Value = L["Title"].Value;
                            }
                            //if (item.CodeProperties == "Name")
                            //{
                            //    Sheet.Cells[line + 1, 2].Value = L["Name"].Value;
                            //}

                            if (item.CodeProperties == "Description")
                            {
                                Sheet.Cells[line + 1, 2].Value = L["Description"].Value;
                            }

                            if (item.CodeProperties == "Name")
                            {
                                Sheet.Cells[line + 1, 2].Value = L["Name"].Value;
                            }

                            var value = string.Empty;

                            //Sheet.Cells[line + 1, 3].Value = value;

                            if (item.TypeProperties == "link" || item.TypeProperties == "image")
                            {
                                if (checkedProperty != null)
                                {
                                    item.DefalutValue = checkedProperty.DefalutValue;
                                }

                                if (!string.IsNullOrEmpty(item.DefalutValue))
                                {
                                    var array = JsonConvert.DeserializeObject<List<ImageLinkUrl>>(item.DefalutValue);
                                    value = string.Join(Environment.NewLine, array.Select(x => x.url));
                                }

                            }
                            else if (item.TypeProperties == "checkbox" || item.TypeProperties == "list" || item.TypeProperties == "radiobutton")
                            {
                                try
                                {
                                    var valueListDefault = new List<string>();
                                    if (checkedProperty != null)
                                    {
                                        if (!string.IsNullOrEmpty(checkedProperty.DefalutValue))
                                        {
                                            valueListDefault = checkedProperty.DefalutValue.Split(",").ToList();
                                        }
                                    }

                                    var lstItems = JsonConvert.DeserializeObject<List<ItemValueProperties>>(item.DefalutValue);
                                    if (lstItems != null)
                                    {
                                        var valueItem = lstItems.FindAll(x => valueListDefault.Contains(x.Code));

                                        //var valueItem = lstItems.FindAll(x => x.Checked == true);

                                        if (valueItem != null)
                                        {
                                            if (item.TypeProperties != "checkbox")
                                            {
                                                value = valueItem.FirstOrDefault().Name;
                                            }
                                            else
                                            {
                                                value = string.Join(",", valueItem.Select(x => x.Name));
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                    value = item.DefalutValue;
                                }
                            }
                            else
                            {
                                switch (item.TypeProperties)
                                {
                                    case "file":
                                        if (listDocument != null && listDocument.Count > 0)
                                        {
                                            var lstNameFile = listDocument.FindAll(x => x.NameFolder == item.CodeProperties);
                                            if (lstNameFile.Count > 0)
                                            {
                                                value = string.Join(Environment.NewLine, lstNameFile.Select(x => x.UrlDocument));
                                            }
                                        }
                                        break;
                                    case "geojson":
                                        try
                                        {
                                            value = JsonConvert.SerializeObject(infoObjectData.Geometry);

                                            //var abc = IoT.Common.Extensions.ConvertToListObject(infoObjectData.Geometry.Coordinates);

                                            //if (infoObjectData.Geometry.Type == "Point")
                                            //{
                                            //    value = string.Format("POINT({0},{1})", infoObjectData.Geometry.Coordinates.ElementAt(0), infoObjectData.Geometry.Coordinates.ElementAt(1));
                                            //}
                                            //else
                                            //{

                                            //}
                                        }
                                        catch
                                        {
                                            value = string.Empty;
                                        }

                                        break;
                                    case "bool":
                                        if (checkedProperty != null)
                                        {
                                            item.DefalutValue = checkedProperty.DefalutValue;
                                        }

                                        value = item.DefalutValue == "true" ? "Có" : "Không";
                                        break;
                                    default:

                                        if (checkedProperty != null)
                                        {
                                            item.DefalutValue = checkedProperty.DefalutValue;
                                        }

                                        value = item.DefalutValue;
                                        break;
                                }
                            }

                            Sheet.Cells[line + 1, 3].Value = value;
                            Sheet.Cells[line + 1, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            line++;
                        }
                        Sheet.Cells["A:AZ"].AutoFitColumns();
                        //Sheet.Column(2).Width = 50;
                        Sheet.Column(2).AutoFit(30);
                        Sheet.Column(3).AutoFit(70);
                        fileContents = Ep.GetAsByteArray();

                    }
                    var namefile = string.Format((!string.IsNullOrEmpty(infoObjectData.NameObject) ? infoObjectData.NameObject : "thong_tin_doi_tuong") + ".xlsx", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    return File(
                        fileContents: fileContents,
                        contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        fileDownloadName: namefile
                    );
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Format(string.Format("<ul>{0}</ul>", "Không tìm thấy dữ liệu")), false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-object-by-IdDirectory")]
        public ActionResult GetObjectByIdDirectory(string id)
        {
            try
            {
                var result = _mainObjectService.GetObjectByIdDriectory(id);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-object-by-IdDirectory-KhaiThac")]
        public async Task<ActionResult> GetObjectByIdDirectoryKhaiThac(string id)
        {
            try
            {
                var result = await _mainObjectService.GetObjectByIdDriectoryKhaiThac(id);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="skipnumber"></param>
        /// <param name="countnumber"></param>
        /// <param name="search"></param>
        /// <param name="code"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet("get-number-object-by-IdDirectory")]
        public ActionResult GetNumberObjectByIdDirectory(string id, int skipnumber, int countnumber, string search, string code, string type)
        {
            try
            {
                var result = _mainObjectService.GetNumberObjectByIdDriectory(id, string.IsNullOrWhiteSpace(search) ? "" : search.Trim(), skipnumber, countnumber, code, type);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyWord"></param>
        /// <returns></returns>
        [HttpGet("get-object-by-keyword")]
        public async Task<ActionResult> GetObjectByKeyWord(string keyWord)
        {
            try
            {
                if (!keyWord.IsNullOrWhiteSpace())
                {
                    var result = await _mainObjectService.GetObjectByKeyWord(keyWord);
                    if (result != null)
                    {
                        return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                    }
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="listMainObject"></param>
        /// <returns></returns>
        [HttpPost("update-list-object")]
        [RequestSizeLimit(2147483647)]
        public async Task<ActionResult> UpdateListObject(List<ListMainObject> listMainObject)
        {
            try
            {
                var listobj = await _mainObjectService.CreaterListMainObject(listMainObject);
                if (listobj != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, listobj));
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("export-to-pdf")]
        public async Task<ActionResult> ExportToPdf(List<ObjectLocation> input)
        {
            try
            {
                var result = await _mainObjectService.ExportToPdf(input);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="directoryid"></param>
        /// <returns></returns>
        [HttpGet("check-delete-properties")]
        public ActionResult CheckDeleteProperties(string code, string directoryid)
        {
            try
            {
                if (string.IsNullOrEmpty(directoryid))
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
                }

                var lstMain = _mainObjectService.GetObjectByIdDriectory(directoryid);
                var lstMainId = lstMain.Select(x => x.Id).ToList();
                var check = _detailObjectService.CheckDeleteProperties(lstMainId, code);

                if (check)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, check));
                }
                else
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, string.Empty, check));
                }
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("update-extend-main-object")]
        public async Task<ActionResult> UpdateExtendMainObject([FromBody] FormManagementDataModel dto)
        {
            try
            {
                var result = await this._detailObjectService.UpdateExtendMainObject(dto);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Cập nhật dữ liệu thành công", result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("update-properties-main-object")]
        public async Task<ActionResult> UpdatePropertiesMainObject([FromBody] FormManagementDataModel dto)
        {
            try
            {
                if (string.IsNullOrEmpty(dto.Id))
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
                }

                var updateNameObj = await _mainObjectService.UpdateNameMainObject(dto.Id, dto.NameObject);

                var result = await this._detailObjectService.UpdatePropertiesMainObject(dto);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Cập nhật dữ liệu thành công", result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet("get-object-paging-by-IdDirectory")]
        public async Task<ActionResult> GetObjectPagingByIdDirectory(string id, int page)
        {
            try
            {
                List<InfoObjectDataNew> result = await _mainObjectService.GetObjectPagingByIdDirectory(id, page);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, JsonConvert.SerializeObject(result)));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-total-count-by-IdDirectory")]
        public async Task<ActionResult> GetTotalCountPagingByIdDirectory(string id)
        {
            try
            {
                int count = await _mainObjectService.GetCountByIdDriectory(id);
                if (count > 0)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, count));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", 0));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, 0));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("change-search-name-object")]
        public async Task<ActionResult> ChangeSearchName()
        {
            try
            {
                bool count = await _mainObjectService.ChangeValueSearch();

                if (count)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, count));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("change-search-detail")]
        public async Task<ActionResult> ChangeValueDetail()
        {
            try
            {
                bool count = await _mainObjectService.ChangeValueDetail();
                if (count)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, count));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("delete-mode-3d-by-IdObject")]
        public async Task<ActionResult> DeleteMode3DByObjectId([FromBody] string id)
        {
            try
            {
                bool check = await _mainObjectService.DeleteObjectMode3D(id);
                if (check)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, check));
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", false));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        [HttpGet("export-geojson")]
        public async Task<ActionResult> ExportGeojson(string id)
        {
            try
            {
                var domain = $"{this.Request.Scheme}://{this.Request.Host}";
                string filePath = domain + "/LogImport/exportLayer.txt";
                await this._mainObjectService.GetObjectGeoJsonAsync(id);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, filePath));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, string.Empty));
            }
        }
    }
}
