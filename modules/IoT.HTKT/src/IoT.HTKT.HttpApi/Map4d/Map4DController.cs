﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.Map4d
{
    [RemoteService]
    [Route("/api/htkt/map4d")]
    public class Map4DController : HTKTController
    {
        private IConfiguration configuration;
        public Map4DController(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-object-in-geometry")]
        public async Task<ActionResult> GetListObjectInGeometry()
        {
            try
            {
                var key = configuration.GetSection("Map4d:Key").Value;
                var geometry = configuration.GetSection("Map4d:Geometry").Value;
                var url = "https://api-private.map4d.vn/app/object/object-in-geometry?Key=" + key;
                HttpClient client = new HttpClient();
                StringContent httpContent = new StringContent(geometry, System.Text.Encoding.UTF8, "application/json");
                var response = await client.PostAsync(url, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    return Ok(result);
                    //return Ok(true);
                }
                return Ok(JsonConvert.SerializeObject(CommonResponse.CreateResponse(ResponseCodes.ErrorData,"Lỗi hệ thống", "null")));
            }
            catch (Exception ex)
            {
                return Ok(JsonConvert.SerializeObject(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null")));
            }
        }
    }
}
