﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.Object
{
    [RemoteService]
    [Route("api/HTKT/ObjectModel")]
    public class ObjectModelController : HTKTController
    {
        private readonly IObjectModelService _objectModelService;

        public ObjectModelController(IObjectModelService objectModelService)
        {
            _objectModelService = objectModelService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult> Create(List<CreateObjectModelDto> param)
        {
            try
            {
                foreach (var i in param)
                {
                    var obj = await _objectModelService.CreateAsync(i);
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, string.Empty));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList()
        {
            try
            {
                var obj = await _objectModelService.GetListDataAsync();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
