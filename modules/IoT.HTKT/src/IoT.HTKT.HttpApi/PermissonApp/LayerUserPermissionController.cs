﻿using IoT.Common;
using IoT.HTKT.LayerPermission;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.PermissonApp
{
    [RemoteService]
    [Route("api/htkt/permissionApp/layer_user_permission")]
    public class LayerUserPermissionController : HTKTController
    {
        private readonly ILayerUserPermissionHTKTService _layerUserPermissionService;
        public LayerUserPermissionController(ILayerUserPermissionHTKTService layerUserPermissionService)
        {
            _layerUserPermissionService = layerUserPermissionService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult> CreateFolder(CreateLayerUserPermissionDto dto)
        {
            try
            {
                var create = new CreateLayerUserPermissionDto();
                create.UserId = dto.UserId;
                create.RoleId = dto.RoleId;
                create.IdDirectory = dto.IdDirectory;
                create.ListPermisson = dto.ListPermisson;
                var rs = await _layerUserPermissionService.CreateAsync(create);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, rs));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [HttpGet("get-by-User-Or-Role")]
        public async Task<ActionResult> GetListOfUserOrRole(string UserId, string RoleId)
        {
            try
            {
                if (!string.IsNullOrEmpty(UserId) || !string.IsNullOrEmpty(RoleId))
                {
                    List<string> lst = new List<string>() {
                        RoleId
                    };
                    var result = await _layerUserPermissionService.GetListOfUserOrRole(UserId, lst);

                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Lỗi dữ liệu đầu vào", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
