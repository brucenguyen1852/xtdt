﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.PropertiesDefault
{
    [RemoteService]
    [Route("api/HTKT/PropertiesDefault")]
    public class PropertiesDefaultController : HTKTController //, IPropertiesDefaultAppService
    {
        private readonly IPropertiesDefaultAppService _propertiesDefaultAppService;

        public PropertiesDefaultController(IPropertiesDefaultAppService propertiesDefaultAppService)
        {
            _propertiesDefaultAppService = propertiesDefaultAppService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create-list")]
        public async Task<ActionResult> CreateList(List<CreatePropertiesDefaultDto> param)
        {
            try
            {
                List<PropertiesDefaultDto> list = new List<PropertiesDefaultDto>();
                foreach (var item in param)
                {
                    CreatePropertiesDefaultDto createPropertiesDefaultDto = new CreatePropertiesDefaultDto()
                    {
                        NameProperties = item.NameProperties,
                        CodeProperties = item.CodeProperties,
                        TypeProperties = item.TypeProperties,
                        IsShow = item.IsShow,
                        IsIndexing = item.IsIndexing,
                        IsRequest = item.IsRequest,
                        IsView = item.IsView,
                        IsHistory = item.IsHistory,
                        IsInheritance = item.IsInheritance,
                        IsAsync = item.IsAsync,
                        DefalutValue = item.DefalutValue,
                        ShortDescription = item.ShortDescription,
                        TypeSystem = item.TypeSystem,
                        OrderProperties = item.OrderProperties,
                    };
                    var obj = await _propertiesDefaultAppService.CreateAsync(createPropertiesDefaultDto);
                    list.Add(obj);
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList(/*List<CreatePropertiesDefaultDto> param*/)
        {
            try
            {
                var obj = await _propertiesDefaultAppService.GetListProperties();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        //public Task<PropertiesDefaultDto> CreateAsync(PropertiesDefaultDto input)
        //{
        //    return _propertiesDefaultAppService.CreateAsync(input);
        //}

        //public Task DeleteAsync(Guid id)
        //{
        //    return _propertiesDefaultAppService.DeleteAsync(id);
        //}

        //public Task<PropertiesDefaultDto> GetAsync(Guid id)
        //{
        //    return _propertiesDefaultAppService.GetAsync(id);
        //}

        //public Task<PagedResultDto<PropertiesDefaultDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        //{
        //    return _propertiesDefaultAppService.GetListAsync(input);
        //}

        //public Task<PropertiesDefaultDto> UpdateAsync(Guid id, PropertiesDefaultDto input)
        //{
        //    return _propertiesDefaultAppService.UpdateAsync(id, input);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public bool GetAsync()
        {
            return true;
        }

    }
}
