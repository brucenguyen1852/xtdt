﻿using IoT.Common;
using IoT.HTKT.PropertiesDefault;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.PropertiesDirectory
{
    [RemoteService]
    [Route("api/HTKT/PropertiesDirectory")]
    public class PropertiesDirectoryController : HTKTController
    {
        private readonly IPropertiesDirectoryAppService _propertiesDirectoryAppService;

        public PropertiesDirectoryController(IPropertiesDirectoryAppService propertiesDirectoryAppService)
        {
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult> Create(CreateUpdatePropertiesDirectoryDto param)
        {
            try
            {
                var obj = await _propertiesDirectoryAppService.CreateAsync(param);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstId"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task<ActionResult> Delete(List<string> lstId)
        {
            try
            {
                foreach (var i in lstId)
                {
                    var obj = await _propertiesDirectoryAppService.DeleteDirectoryByIdDirectory(i);
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thành công", string.Empty));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList()
        {
            try
            {
                var obj = await _propertiesDirectoryAppService.GetListDirectoryAsync();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-directoryById")]
        public async Task<ActionResult> GetDirectoryById(string id)
        {
            try
            {
                var obj = await _propertiesDirectoryAppService.GetAsyncById(id);
                if (obj != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, "[]"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("update-new-properties")]
        public async Task<ActionResult> UpdateNewProperties(CreateUpdatePropertiesDirectoryDto param)
        {
            try
            {
                var properties = await _propertiesDirectoryAppService.UpdateListProperties(param.ListProperties, param.IdDirectory);
                if (properties != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, properties));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData,string.Empty, "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("updateModel")]
        public async Task<ActionResult> UpdateModel()
        {
            try
            {
                var re = await _propertiesDirectoryAppService.UpdateDataModel();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, re));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }
    }
}
