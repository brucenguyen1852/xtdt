﻿using IoT.Common;
using IoT.HTKT.PropertiesDirectory;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.PropertiesDirectoryActivity
{
    [RemoteService]
    [Route("api/HTKT/PropertiesDirectoryActivity")]
    public class PropertiesDirectoryActivityController  : HTKTController
    {
        private readonly IPropertiesDirectoryActivityService _PropertiesDirectoryActivityService;

        public PropertiesDirectoryActivityController(IPropertiesDirectoryActivityService PropertiesDirectoryActivityService)
        {
            _PropertiesDirectoryActivityService = PropertiesDirectoryActivityService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult> Create(CreatePropertiesDirectoryActivityDto param)
        {
            try
            {
                var obj = await _PropertiesDirectoryActivityService.CreateAsync(param);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstId"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task<ActionResult> Delete(List<string> lstId)
        {
            try
            {
                foreach (var i in lstId)
                {
                    var obj = await _PropertiesDirectoryActivityService.DeleteDirectoryByIdDirectory(i);
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Thành công", string.Empty));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList()
        {
            try
            {
                var obj = await _PropertiesDirectoryActivityService.GetListDirectoryAsync();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-directoryById")]
        public async Task<ActionResult> GetDirectoryById(string id)
        {
            try
            {
                var obj = await _PropertiesDirectoryActivityService.GetAsyncById(id);
                if (obj != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, "[]"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
