﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.HTKT.PropertiesDirectoryStatistic
{
    [RemoteService]
    [Route("api/HTKT/PropertiesDirectoryStatistic")]
    public class PropertiesDirectoryStatisticController : HTKTController
    {
        private readonly IPropertiesDirectoryStatisticService _propertiesDirectoryAppService;

        public PropertiesDirectoryStatisticController(IPropertiesDirectoryStatisticService propertiesDirectoryAppService)
        {
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult> Create(CreateUpdatePropertiesDirectoryStatisticDto param)
        {
            try
            {
                var obj = await _propertiesDirectoryAppService.CreateAsync(param);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, string.Empty));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstIdShow"></param>
        /// <returns></returns>
        [HttpPost("get-list-directory-statistic")]
        public async Task<ActionResult> GetListDirectoryStatistic(List<string> lstIdShow)
        {
            try
            {
                var lst = await _propertiesDirectoryAppService.GetListDirectoryStatistic(lstIdShow);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, lst));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
