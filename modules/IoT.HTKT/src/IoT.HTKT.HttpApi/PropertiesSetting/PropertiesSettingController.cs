﻿using IoT.HTKT.PropertiesDefault;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Volo.Abp;
using Microsoft.AspNetCore.Http;
using System.IO;
using IoT.Common;

namespace IoT.HTKT.PropertiesSetting
{
    [RemoteService]
    [Route("api/HTKT/PropertiesSetting")]
    public class PropertiesSettingController : HTKTController
    {
        private readonly IPropertiesSettingService _propertiesSettingService;

        public PropertiesSettingController(IPropertiesSettingService propertiesSettingService)
        {
            _propertiesSettingService = propertiesSettingService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllPropertySettings")]
        public async Task<IActionResult> GetAllPropertires()
        {
            try
            {
                var list = await _propertiesSettingService.getAllProperties();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getByIdDictionary")]
        public async Task<IActionResult> GetByIdDictionary(string id)
        {
            try
            {
                var result = await _propertiesSettingService.FindPropertyByIdDirection(id);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<IActionResult> Create(CreatePropertiesSettingDto param)
        {
            try
            {
                var result = await _propertiesSettingService.CreateAsync(param);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromForm] CreatePropertiesSettingDto param, List<IFormFile> Files)
        {
            //return Ok();
            try
            {
                var obj = new CreatePropertiesSettingDto();
                obj.IdDirectory = param.IdDirectory;
                obj.MinZoom = param.MinZoom;
                obj.MaxZoom = param.MaxZoom;
                obj.Stroke = param.Stroke;
                obj.StrokeWidth = param.StrokeWidth;
                obj.StrokeOpacity = param.StrokeOpacity;
                obj.StyleStroke = param.StyleStroke;
                obj.Fill = param.Fill;
                obj.FillOpacity = param.FillOpacity;
                obj.Image2D = param.Image2D;
                obj.Object3D = (param.Object3D != "undefined" && param.Object3D != null) ? param.Object3D : string.Empty;
                obj.Texture3D = param.Texture3D;
                obj.ImageIcon = param.ImageIcon;

                foreach (var file in Files)
                {
                    if (param.Image2D.Contains(file.FileName))
                    {
                        var serverPathimage2D = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileImage2D");
                        if (!System.IO.Directory.Exists(serverPathimage2D))
                        {
                            System.IO.Directory.CreateDirectory(serverPathimage2D);
                        }

                        var fileNameImage2D = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
                        var pathImage2D = Path.Combine(serverPathimage2D, fileNameImage2D);

                        using (var stream = new FileStream(pathImage2D, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        obj.Image2D = "/FileImage2D/" + fileNameImage2D;
                    }
                    else if (param.Object3D.Contains(file.FileName))
                    {
                        var serverPathObject3D = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/FileObject3D");
                        if (!System.IO.Directory.Exists(serverPathObject3D))
                        {
                            System.IO.Directory.CreateDirectory(serverPathObject3D);
                        }

                        var fileNameObject3D = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
                        var pathObject3D = Path.Combine(serverPathObject3D, fileNameObject3D);

                        using (var stream = new FileStream(pathObject3D, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        obj.Object3D = "/FileObject3D/" + fileNameObject3D;
                    }
                    else if (param.Texture3D.Contains(file.FileName))
                    {
                        var serverPathTexture3D = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/Texture3D");
                        if (!System.IO.Directory.Exists(serverPathTexture3D))
                        {
                            System.IO.Directory.CreateDirectory(serverPathTexture3D);
                        }

                        var fileNameTexture3D = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
                        var pathTexture3D = Path.Combine(serverPathTexture3D, fileNameTexture3D);

                        using (var stream = new FileStream(pathTexture3D, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        obj.Texture3D = "/Texture3D/" + fileNameTexture3D;
                    }
                }

                var result = await _propertiesSettingService.UpdateProperty(obj);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, "Update thanh cong", result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
