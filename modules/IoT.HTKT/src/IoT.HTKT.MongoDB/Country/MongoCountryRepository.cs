﻿using IoT.HTKT.MongoDB;
using System;
using System.Collections.Generic;

using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;
using MongoDB.Driver.Linq;
using MongoDB.Driver;
using System.Linq;

namespace IoT.HTKT.Country
{
    public class MongoCountryRepository : MongoDbRepository<HTKTMongoDbContext, Country, Guid>, ICountryRepository
    {
        public MongoCountryRepository(IMongoDbContextProvider<HTKTMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
        public async Task<List<Country>> GetListAsync(string filterText = null, bool? isDeleted = null, string name = null, string description = null, int? level = null, string type = null, string code = null, string search = null, string fullCode = null, string sorting = null,  CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetMongoQueryableAsync(cancellationToken)), filterText, isDeleted, name, description, level, type, code, search, fullCode);
            string GetDefaultSorting = (string.IsNullOrWhiteSpace(sorting) ? "code asc" : sorting);
            query = query.OrderBy(GetDefaultSorting);
            return await query.As<IMongoQueryable<Country>>()
                //.PageBy<Country, IMongoQueryable<Country>>(skipCount, maxResultCount)
                .ToListAsync(GetCancellationToken(cancellationToken));
        }
        protected virtual IQueryable<Country> ApplyFilter(
            IQueryable<Country> query,
            string filterText,
            bool? isDeleted = null,
            string name = null,
            string description = null,
            int? level = null,
            string type = null,
            string code = null,
            string search = null,
            string fullCode = null)
        {
            return query
                .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.Name.Contains(filterText) || e.Description.Contains(filterText) || e.Type.Contains(filterText) || e.Code.Contains(filterText) || e.Search.Contains(filterText) || e.FullCode.Contains(filterText))
                    .WhereIf(isDeleted.HasValue, e => e.IsDeleted == isDeleted)
                    .WhereIf(!string.IsNullOrWhiteSpace(name), e => e.Name.Contains(name))
                    .WhereIf(!string.IsNullOrWhiteSpace(description), e => e.Description.Contains(description))
                    .WhereIf(level.HasValue, e => e.Level == level)
                    .WhereIf(!string.IsNullOrWhiteSpace(type), e => e.Type.Contains(type))
                    .WhereIf(!string.IsNullOrWhiteSpace(code), e => e.Code.Contains(code))
                    .WhereIf(!string.IsNullOrWhiteSpace(search), e => e.Search.Contains(search))
                    .WhereIf(!string.IsNullOrWhiteSpace(fullCode), e => e.FullCode.Contains(fullCode));
        }
    }
}
