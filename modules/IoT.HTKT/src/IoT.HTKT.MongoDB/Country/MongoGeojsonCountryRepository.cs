﻿using IoT.HTKT.MongoDB;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace IoT.HTKT.Country
{
    public class MongoGeojsonCountryRepository : MongoDbRepository<HTKTMongoDbContext, GeojsonCountry, Guid>, IGeojsonCountryRepository
    {
        public MongoGeojsonCountryRepository(IMongoDbContextProvider<HTKTMongoDbContext> dbContextProvider)
           : base(dbContextProvider)
        {
        }
    }
}
