﻿using IoT.HTKT.Layer;
using IoT.HTKT.LayerPermission;
using IoT.HTKT.ManageActivity;
using IoT.HTKT.ManagementData;
using IoT.HTKT.Object;
using IoT.HTKT.SettingData;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;
namespace IoT.HTKT.MongoDB
{
    [ConnectionStringName(HTKTDbProperties.ConnectionStringName)]
    public class HTKTMongoDbContext : AbpMongoDbContext, IHTKTMongoDbContext
    {
        /* Add mongo collections here. Example:
         * public IMongoCollection<Question> Questions => Collection<Question>();
         */
        public IMongoCollection<Directory> Directorys => Collection<Directory>();
        public IMongoCollection<PropertiesDirectory> PropertiesDirectorys => Collection<PropertiesDirectory>();
        public IMongoCollection<PropertiesSetting> PropertiesSettings => Collection<PropertiesSetting>();
        public IMongoCollection<MainObject> MainObjects => Collection<MainObject>();
        public IMongoCollection<DetailObject> DetailObjects => Collection<DetailObject>();
        public IMongoCollection<PropertiesDefault> PropertiesDefaults => Collection<PropertiesDefault>();
        public IMongoCollection<DocumentObject> DocumentObjects => Collection<DocumentObject>();
        public IMongoCollection<LayerDataType> LayerDataTypes => Collection<LayerDataType>();
        public IMongoCollection<ObjectModel> ObjectModels => Collection<ObjectModel>();
        public IMongoCollection<PropertiesDirectoryStatistic> PropertiesDirectoryStatistics => Collection<PropertiesDirectoryStatistic>();

        public IMongoCollection<DirectoryActivity> DirectoryActivities => Collection<DirectoryActivity>();
        public IMongoCollection<PropertiesDirectoryActivity> PropertiesDirectoryActivities => Collection<PropertiesDirectoryActivity>();

        public IMongoCollection<DetailActivity> DetailActivities => Collection<DetailActivity>();

        public IMongoCollection<LayerBaseMap> LayerBaseMaps => Collection<LayerBaseMap>();
        public IMongoCollection<DocumentActivity> DocumentActivities => Collection<DocumentActivity>();
        public IMongoCollection<LayerUserPermission> LayerUserPermissions => Collection<LayerUserPermission>();

        public IMongoCollection<IoT.HTKT.LocationSetting.LocationSetting> LocationSettings => Collection<IoT.HTKT.LocationSetting.LocationSetting>();

        public IMongoCollection<IoT.HTKT.Country.Country> Country => Collection<IoT.HTKT.Country.Country>();
        public IMongoCollection<IoT.HTKT.Country.GeojsonCountry> GeojsonCountry => Collection<IoT.HTKT.Country.GeojsonCountry>();
        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigureHTKT();
        }
    }
}