﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace IoT.HTKT.MongoDB
{
    public static class HTKTMongoDbContextExtensions
    {
        public static void ConfigureHTKT(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new HTKTMongoModelBuilderConfigurationOptions(
                HTKTDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}