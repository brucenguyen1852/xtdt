﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace IoT.HTKT.MongoDB
{
    public class HTKTMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public HTKTMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}