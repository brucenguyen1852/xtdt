﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.HTKT.MongoDB
{
    [ConnectionStringName(HTKTDbProperties.ConnectionStringName)]
    public interface IHTKTMongoDbContext : IAbpMongoDbContext
    {
        /* Define mongo collections here. Example:
         * IMongoCollection<Question> Questions { get; }
         */
    }
}
