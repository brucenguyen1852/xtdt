﻿using AutoMapper;

namespace IoT.HTKT.Web
{
    public class HTKTWebAutoMapperProfile : Profile
    {
        public HTKTWebAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}