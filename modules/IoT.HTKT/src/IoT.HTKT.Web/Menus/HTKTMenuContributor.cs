﻿using IoT.HTKT.Localization;
using IoT.HTKT.Permissions;
using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace IoT.HTKT.Web.Menus
{
    public class HTKTMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            var l = context.GetLocalizer<HTKTResource>();
            //Add main menu items.
            var identityMenuItem = new ApplicationMenuItem(HTKTMenus.Prefix, l["Menu:HTKT"], icon: "fa fa-group");
            if (await context.IsGrantedAsync(HTKTPermissions.Administration.ManagementData))
            {
                identityMenuItem.AddItem(new ApplicationMenuItem(HTKTMenus.Layer, l["Menu:Layer"], url: "~/HTKT/Layer"));
            }
            if (await context.IsGrantedAsync(HTKTPermissions.Administration.TypeActive))
            {
                identityMenuItem.AddItem(new ApplicationMenuItem(HTKTMenus.Activity, l["Menu:Activity"], url: "~/HTKT/Activity"));
            }
            if (await context.IsGrantedAsync(HTKTPermissions.Administration.LayerBaseMap))
            {
                identityMenuItem.AddItem(new ApplicationMenuItem(HTKTMenus.LayerBaseMap, l["Menu:LayerBaseMap"], url: "~/HTKT/LayerBaseMap"));
            }
            if (await context.IsGrantedAsync(HTKTPermissions.Administration.MapCenter))
            {
                identityMenuItem.AddItem(new ApplicationMenuItem(HTKTMenus.MapCenter, l["Menu:MapCenter"], url: "~/HTKT/LocationCenterSetting"));
            }
            //context.Menu.AddItem(new ApplicationMenuItem(HTKTMenus.Prefix, displayName: "HTKT", "~/HTKT", icon: "fa fa-globe"));
            var identityMenuItem2 = new ApplicationMenuItem(HTKTMenus.ManagementLayer, l["Menu:ManagementLayer"], "~/HTKT/ManagementLayer", icon: "fas fa-layer-group");
            identityMenuItem2.Order = 1;
            identityMenuItem.Order = 4;

            context.Menu.AddItem(identityMenuItem);

            if (await context.IsGrantedAsync(HTKTPermissions.ManagementData.Default))
            {
                context.Menu.AddItem(identityMenuItem2);
            }
            //return Task.CompletedTask;
        }
    }
}