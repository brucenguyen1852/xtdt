﻿namespace IoT.HTKT.Web.Menus
{
    public class HTKTMenus
    {
        public const string Prefix = "HTKT";

        //Add your menu items here...
        //public const string Home = Prefix + ".MyNewMenuItem";
        public const string Layer = Prefix + ".Layer";
        public const string Activity = Prefix + ".Activity";
        public const string ManagementLayer = Prefix + ".ManagementLayer";
        public const string LayerBaseMap = Prefix + ".LayerBaseMap";
        public const string MapCenter = Prefix + ".MapCenter";
    }
}