using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoT.HTKT.LayerPermission;
using IoT.PermissionApp.ManagerIdentity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Users;

namespace IoT.HTKT.Web.Pages.HTKT.Activity
{
    [Authorize]
    public class IndexModel : AbpPageModel
    {
        private readonly ILayerUserPermissionHTKTService _layerUserPermissionService;

        private readonly IManagerIdentityService _managerIdentityService;
        [BindProperty]
        public string UserId { get; set; }
        [BindProperty]
        public List<string> RoleId { get; set; } = new List<string>();
        [BindProperty]
        public List<PermissionDetail> ListPermissons { get; set; } = new List<PermissionDetail>();
        [BindProperty]
        public List<PermissionByDirectory> ListLayer { get; set; } = new List<PermissionByDirectory>();
        [BindProperty]
        public List<string> ListIdLayer { get; set; } = new List<string>();
        [BindProperty]
        public Boolean IsAdmin { get; set; } = false;

        public IndexModel(ILayerUserPermissionHTKTService layerUserPermissionService, IManagerIdentityService managerIdentityService)
        {
            _layerUserPermissionService = layerUserPermissionService;
            _managerIdentityService = managerIdentityService;
        }
        public virtual async Task<IActionResult> OnGet()
        {
            UserId = CurrentUser.Id.ToString();

            var guild = Guid.Parse(UserId);
            var rolelst = await _managerIdentityService.GetRolesAsync(guild);
            RoleId = rolelst.Items.Select(x => x.Id.ToString()).ToList();
            foreach (var iz in RoleId)
            {
                var check = await _managerIdentityService.GetByIdAsync(Guid.Parse(iz));
                if (check != null && check.IsStatic)
                {
                    IsAdmin = true;
                }
            }

            var lstpermission = await _layerUserPermissionService.GetListOfUserOrRole(UserId, RoleId);
            foreach (var i in lstpermission)
            {
                ListPermissons = ListPermissons.Concat(i.ListPermisson).ToList();
                var checklayer = ListLayer.Where(x => x.IdDirectory == i.IdDirectory).FirstOrDefault();
                if (checklayer == null)
                {
                    ListIdLayer.Add(i.IdDirectory);
                    var obj = new PermissionByDirectory
                    {
                        IdDirectory = i.IdDirectory,
                        ListPermisson = i.ListPermisson.Select(x => x.KeyName).ToList()
                    };
                    ListLayer.Add(obj);
                }
                else
                {
                    checklayer.ListPermisson.AddRange(i.ListPermisson.Select(x => x.KeyName).ToList());
                    checklayer.ListPermisson = checklayer.ListPermisson.Distinct().ToList();

                }
            }
            return await Task.FromResult<IActionResult>(Page());
        }
    }
}
