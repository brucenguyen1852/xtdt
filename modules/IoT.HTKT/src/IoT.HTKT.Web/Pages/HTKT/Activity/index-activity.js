﻿var TreeView = {
    GLOBAL: {
        idForcus: "",
        propertyFile: {
            avatar2D: '',
            object3D: '',
            texture3D: ''
        },
        ArrayTree: [],
        propertyData: null,
        isAction: -1,
        ListIdShow: []
    },
    CONSTS: {
        URL_GETLISTDIRECTORY: "/api/htkt/directoryactivity/get-list-new-not-properties",
        URL_CREATEFOLDER_ACTIVITY: "/api/htkt/directoryactivity/create-folder",
        URL_CLONEPROPERTIESDIRECTORY: "/api/htkt/directoryactivity/clone-directory",
        URL_DANH_SACH_LAYER: "/api/htkt/directory/get-list-new-layer",
        URL_CHECK_SO_THU_TU: "",
        URL_CHECK_DELETE: "/api/htkt/directoryactivity/check-delete-activity",
        URL_UPDATE_NAME_FOLDER: "/api/htkt/directoryactivity/update-name-folder",
        URL_GETLISTDIRECTORY_COUNT: "/api/htkt/directoryactivity/get-list-count"
    },
    SELECTORS: {
        iconDatabase: "fa fa-database",
        modalFolder: ".exampleModal-folder",
        modalLayer: ".Modal-layer",
        inputmodalFolder: ".exampleModal-folder input",
        inputmodalLayer: ".Modal-layer input",
        addLayer: "#addLayer",
        addFolder: "#addFolder",
        searchTree: ".search-tree",
        btnSaveFolderLayer: "#btnSaveFolderLayer",
        btnSaveFolder: "#btnSaveFolder",
        btnDeleteFolder: "#btnDeleteFolder",
        listGroupItem: ".list-group-item",
        treeViewShow: ".tree-view",
        layerdata: ".layerdata",
        radiosLayer: "input[name='optionsLayer']",
        radiosLayerChecked: "input[name='optionsLayer']:checked",
        inputName: "#nameLayer",
        inputnameImplement: "#NameImplement",
        content_left_sidebar: '.map-side-form .scroll-content',
        // Gentree
        tree_i_o: ".tree-o-i",
        item_tree_i_o: ".item-li",
        toggle_side_bar: ".menuMap #hideTree",
        sideForm: ".map-side-form",

        menu: ".menuMap",
        action_menu: ".action-menu",
        content_section: ".content-section",
        black_drop_content: ".black-drop-content",
        toogle_content_section: ".toogle-content-section",
        btn_close_content_section: ".btn-close-content",
        inputNameFolder: "#nameFolder",
        div2: "#div2",
        content_count_directory: ".content-count-directory",
        item_count: ".file-upload-content .item-count-ten",
        dataName: "#DataName",
        dataCode: "#DataCode",

        select_lop_du_lieu_create_layer: "#idDirectory-model-layer"
    },
    init: function () {
        TreeView.setUpEvent();
        TreeView.Splitter();

        TreeView.GetDataTree();
        TreeView.EventNewTree();
        TreeView.OpenCurrentData();
        TreeView.GetDirectoryCount();
    },
    OpenCurrentData: function () {
        var url = new URL(window.location.href);
        var checkid = url.searchParams.get("id");
        if (checkid !== null) {
            thisId = checkid;
            setTimeout(function () {
                $(TreeView.SELECTORS.content_left_sidebar).animate({
                    scrollTop: $('#item-li-' + checkid).offset().top - $(TreeView.SELECTORS.content_left_sidebar).offset().top
                }, 'slow');

                $('#item-li-' + checkid).click();
                $('#item-li-' + checkid).addClass('active');
                tableConfig.GLOBAL.idDirectory = checkid;
                TreeView.ShowInfoData();

            }, 500)

            var parent = $('#item-li-' + checkid).parent().attr('data-parent');

            if (parent != "null") {
                var chil = parseInt($('#item-li-' + parent).attr('data-chil'));
                if (chil > 0) {
                    $('#tree-ul-' + parent).addClass('open');
                    $('#tree-ul-' + parent).slideDown();
                    OpenTree($('#item-li-' + parent));
                }
            }
        }
        else {
            $(TreeView.SELECTORS.tree_i_o + " " + TreeView.SELECTORS.item_tree_i_o).eq(0).click();
        }

        function OpenTree(element) {
            var parent = $(element).parent().attr('data-parent');
            var icon = $(element).find('.icon-menu-tree');
            if (icon.length > 0) {
                icon.addClass('open')
            }

            if (parent != "null") {
                var chil = parseInt($('#item-li-' + parent).attr('data-chil'));
                if (chil > 0) {
                    $('#tree-ul-' + parent).addClass('open');
                    $('#tree-ul-' + parent).slideDown();

                    OpenTree($('#item-li-' + parent));
                }
            }
        }
    },
    setUpEvent: function () {
        $(TreeView.SELECTORS.searchTree).on('keyup', TreeView.delay(function () {
            //var keyword = $(TreeView.SELECTORS.searchTree).val().toLowerCase();
            //$("#tree").remove();
            //$("#divTree").append('<div id="tree"></div>');
            //var result = TreeView.ReturnInforItemTree(item, keyword);
            //TreeView.GetInforTreeView();
        }, 1000));

        // đóng mở sidebar
        $(TreeView.SELECTORS.toggle_side_bar).on('click', function () {
            let svgtext = '';
            if ($(TreeView.SELECTORS.sideForm).hasClass("show")) {
                $(TreeView.SELECTORS.sideForm).removeClass("show");
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <path class="a" style="fill: none;" d="M24,0H0V24H24Z" transform="translate(6)" />
                                <path class="b" style="fill: var(--primary)" d="M26.172,11H4v2H26.172l-5.364,5.364,1.414,1.414L30,12,22.222,4.222,20.808,5.636Z" transform="translate(-4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 18)" />
                            </svg>`;
                $(this).attr("data-original-title", l("ManagementLayer:OpenPanel"));
                $('.div-content').addClass('toogle'); // sidebar toogle
            } else {
                $(TreeView.SELECTORS.sideForm).addClass("show");
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <g transform="translate(1988 32)">
                                    <g transform="translate(-1988 -32)">
                                        <path style="fill: none;" class="a" d="M0,0H24V24H0Z"></path>
                                        <path style="fill: var(--primary)" class="b" d="M7.828,11H30v2H7.828l5.364,5.364-1.414,1.414L4,12l7.778-7.778,1.414,1.414Z"></path>
                                    </g>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -28)"></rect>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -14)"></rect>
                                </g>
                            </svg>`;
                $(this).attr("data-original-title", l("ManagementLayer:ClosePanel"));

                $('.div-content').removeClass('toogle'); // sidebar toogle
            }
            $(this).html('');
            $(this).html(svgtext);

        });
        // click menu
        $(TreeView.SELECTORS.menu).on('click', TreeView.SELECTORS.action_menu, function () {

            var typeAction = $(this).attr('data-type');
            $(TreeView.SELECTORS.action_menu).removeClass('isForcus');
            $(this).addClass('isForcus');

            // disabled nút action
            $(TreeView.SELECTORS.action_menu).prop('disabled', true);
            $(this).prop('disabled', false);
            $(TreeView.SELECTORS.action_menu).addClass('disabled');
            $(this).removeClass('disabled');

            $(TreeView.SELECTORS.content_section).removeClass('open');
            let tree = TreeView.getTreeView(thisId);

            TreeView.GLOBAL.isAction = parseInt(typeAction); // set trạng thái action
            switch (typeAction) {
                case "1": // thêm mới folder
                    $(TreeView.SELECTORS.inputNameFolder).val("");

                    TreeView.clearLabelError();
                    $(TreeView.SELECTORS.modalFolder).addClass("open");
                    $(TreeView.SELECTORS.black_drop_content).show();

                    $(TreeView.SELECTORS.treeViewShow).text("(" + tree + ")");
                    $(this).html(`<svg id="Component_211_2" data-name="Component 211 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_6542" data-name="Path 6542" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_6543" data-name="Path 6543" d="M12.414,5H21a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414ZM11,12H8v2h3v3h2V14h3V12H13V9H11Z" fill="var(--primary)"/>
                                    </svg>`);
                    $(TreeView.SELECTORS.modalFolder + " .content-section-header span").text(l('Layer:AddNewFolder')); // thay đổi tên title
                    break;
                case "2": // sửa thư mục
                    $(TreeView.SELECTORS.inputNameFolder).val("");

                    var folder = TreeView.GLOBAL.ArrayTree.find(x => x.id == TreeView.GLOBAL.idForcus);
                    if (folder != undefined) {
                        $(TreeView.SELECTORS.inputNameFolder).val(folder.name);

                        TreeView.clearLabelError();
                        $(TreeView.SELECTORS.modalFolder).addClass("open");
                        $(TreeView.SELECTORS.black_drop_content).show();
                        $(TreeView.SELECTORS.treeViewShow).text("(" + tree + ")");

                        $(this).html(`<svg id="Component_209_2" data-name="Component 209 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path id="Path_6610" data-name="Path 6610" d="M0,0H24V24H0Z" fill="none"/>
                                        <path id="Path_6611" data-name="Path 6611" d="M9.243,19H21v2H3V16.757l9.9-9.9L17.142,11.1,9.242,19Zm5.07-13.556,2.122-2.122a1,1,0,0,1,1.414,0l2.829,2.829a1,1,0,0,1,0,1.414L18.556,9.686,14.314,5.444Z" fill="var(--primary)"/>
                                    </svg>`);

                        $(TreeView.SELECTORS.modalFolder + " .content-section-header span").text(l('Layer:EditNameFolder')); // thay đổi tên title
                    }
                    break;
                case "3": // thêm mới loại hoạt động
                    // reset form
                    $(TreeView.SELECTORS.inputName).val("");
                    $(TreeView.SELECTORS.select_lop_du_lieu_create_layer).val("").trigger("change.select2");
                    $(TreeView.SELECTORS.select_lop_du_lieu_create_layer).parent().find('.placeholder').attr('style', "");

                    $(TreeView.SELECTORS.radiosLayer + '[value="1"]').trigger('click');
                    TreeView.clearLabelError();
                    //-----------------------------------------------------------------------

                    $(TreeView.SELECTORS.modalLayer).addClass("open");
                    $(TreeView.SELECTORS.black_drop_content).show();

                    $(TreeView.SELECTORS.treeViewShow).text("(" + tree + ")");

                    $(this).html(`<svg id="Component_172_2" data-name="Component 172 – 2" xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20">
                          <path id="Path_2629" data-name="Path 2629" d="M16,2l5,5V21.008a.993.993,0,0,1-.993.992H3.993A1,1,0,0,1,3,21.008V2.992A.993.993,0,0,1,3.993,2Zm-5,9H8v2h3v3h2V13h3V11H13V8H11Z" transform="translate(-3 -2)" fill="var(--primary)"/>
                        </svg>`); // set hightlight icon
                    break;
                case "6": // xóa
                    var id = TreeView.GLOBAL.idForcus;
                    if (id != "") {
                        var level = TreeView.GLOBAL.ArrayTree.find(x => x.id == id); // Tìm kiếm directory
                        if (level != undefined && level.level > 0) {
                            var textinfor = (level.type === "file") ? l("Layer:AreYouDeleteActivityType") : l("Layer:AreYouDeleteFolder");
                            swal({
                                title: l("Layer:Notification"),
                                text: textinfor,
                                icon: "warning",
                                buttons: [
                                    l("Layer:Cancel"),
                                    l("Layer:Delete")
                                ],
                                dangerMode: true,
                            }).then(function (isConfirm) {

                                if (isConfirm) {
                                    if (TreeView.CheckDeleteLayer(id)) {
                                        TreeView.DeleteFolderActivity(id, level.type);
                                    }
                                    else {
                                        if (level.type === "file") {
                                            abp.notify.error(l("Activity:CheckDeleteActivityFail"));
                                        } else {
                                            abp.notify.error(l("Activity:CheckDeleteFolderFail"));
                                        }
                                        TreeView.CloseContentSection(); // reset action
                                    }
                                }
                                else {
                                    TreeView.CloseContentSection(); // reset action
                                }
                            });

                            $(this).html(`<svg id="Component_210_2" data-name="Component 210 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                          <path id="Path_6554" data-name="Path 6554" d="M0,0H24V24H0Z" fill="none"/>
                                          <path id="Path_6555" data-name="Path 6555" d="M20,7V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H2V5H22V7Zm-9,3v7h2V10ZM7,2H17V4H7Z" fill="var(--primary)"/>
                                        </svg>`);
                        }
                    }
                    break;
                default:
            }
        })

        // đóng form section
        $(TreeView.SELECTORS.toogle_content_section).on('click', function () {
            TreeView.CloseContentSection();
        })

        // đóng form section
        $(TreeView.SELECTORS.btn_close_content_section).on('click', function () {
            TreeView.CloseContentSection();
        })

        $(TreeView.SELECTORS.inputmodalLayer).on('keyup', function () {
            $(this).parent().find('.lable-error').remove();
            $(this).parent().removeClass('has-error');

        })
        // select thua ke
        $(TreeView.SELECTORS.layerdata).on('change', function () {
            $(this).parent().find('.lable-error').remove();
            $(this).parent().removeClass('has-error');
        })
        // save layer
        $(TreeView.SELECTORS.btnSaveFolderLayer).on('click', function () {
            if (TreeView.checkFormLayerInfor()) {
                var name = $('#nameLayer').val();
                let type = $(TreeView.SELECTORS.radiosLayerChecked).val();
                if (type == "2") {
                    let idimplement = $(TreeView.SELECTORS.layerdata).val();
                    if (idimplement !== "0") {
                        var dto = {
                            idDirectory: JSON.stringify($(TreeView.SELECTORS.select_lop_du_lieu_create_layer).val()),
                            name: name,
                            parentId: parentId,
                            level: level,
                            typeDirectory: false,
                            idImplement: idimplement,
                        }
                        TreeView.CloneLayeredForCurrentLayer(dto);
                    }
                } else {

                    var dto = {
                        name: name,
                        parentId: parentId,
                        level: level,
                        typeDirectory: false,
                        idDirectory: JSON.stringify($(TreeView.SELECTORS.select_lop_du_lieu_create_layer).val())
                    }

                    // set loader trước khi lưu
                    abp.ui.setBusy(TreeView.SELECTORS.modalLayer);
                    $(TreeView.SELECTORS.modalLayer).find('.toogle-content-section').hide();

                    setTimeout(function () {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            url: TreeView.CONSTS.URL_CREATEFOLDER_ACTIVITY,
                            async: false,
                            data: JSON.stringify(dto),
                            success: function (res) {
                                if (res.code == "ok") {
                                    var result = res.result;
                                    var code = xoa_dau(result.name).replace(/\s/g, '');

                                    var dto2 = {
                                        "idDirectoryActivity": result.id,
                                        "nameTypeDirectory": result.name,
                                        "codeTypeDirectory": code.toUpperCase(),
                                        "verssionTypeDirectory": 0,
                                        "idImplement": null,
                                        "listProperties": []
                                    };
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        url: "/api/HTKT/PropertiesDirectoryActivity/create",
                                        data: JSON.stringify(dto2),
                                        success: function (result) {
                                            if (result.code == "ok") {
                                                abp.notify.success(l("ManagementLayer:AddActivitySuccess"));
                                                let pathName = document.location.pathname;
                                                history.replaceState(null, null, pathName + "?id=" + dto2.idDirectoryActivity);

                                                TreeView.GetDataTree(dto2.idDirectoryActivity);

                                                TreeView.TriggerForcusToTreeNode(dto2.idDirectoryActivity);


                                            } else {
                                                abp.notify.error(l("ManagementLayer:AddActivityFailed"));
                                            }

                                            TreeView.CloseContentSection(); // reset action

                                            abp.ui.clearBusy(TreeView.SELECTORS.modalLayer);
                                            $(TreeView.SELECTORS.modalLayer).find('.toogle-content-section').show();
                                        },
                                        error: function () {
                                        }
                                    });
                                }
                                else {
                                    abp.notify.error(l("ManagementLayer:AddActivityFailed"));
                                }
                            },
                            error: function () {
                            }
                        });
                    }, 500);
                }
            }
        });
        // save folder
        $(TreeView.SELECTORS.btnSaveFolder).on('click', function (e) {
            e.preventDefault();
            if (TreeView.checkFormInfor()) {
                if (TreeView.GLOBAL.isAction == 2) // trạng thái sửa thư mục
                {
                    TreeView.UpdateNameFolder(TreeView.GLOBAL.idForcus);
                }
                else {
                    var name = $('#nameFolder').val();
                    var dto = {
                        name: name,
                        parentId: parentId,
                        level: level,
                        typeDirectory: true
                    }

                    if (dto.parentId == undefined) {
                        var tree = TreeView.GLOBAL.ArrayTree.find(x => x.level == 0);
                        if (tree != undefined) {
                            dto.parentId = tree.id;
                            dto.level = tree.level + 1;
                        }
                    }

                    if (dto.parentId != undefined) {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            url: TreeView.CONSTS.URL_CREATEFOLDER_ACTIVITY,
                            beforeSend: function () {
                                abp.ui.setBusy(TreeView.SELECTORS.modalFolder);
                                $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').hide();
                            },
                            data: JSON.stringify(dto),
                            success: function (res) {
                                if (res.code == "ok") {
                                    var result = res.result;
                                    abp.notify.success(l("Layer:AddFolderSuccessfully"));
                                    TreeView.GetDataTree(result.id);
                                    TreeView.TriggerForcusToTreeNode(result.id);
                                }
                                else {
                                    console.log(res);
                                }

                                TreeView.CloseContentSection(); // reset action
                            },
                            complete: function () {
                                abp.ui.clearBusy(TreeView.SELECTORS.modalFolder);
                                $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').show();
                            },
                            error: function () {
                                //$(TreeView.SELECTORS.modalFolder).modal('hide');
                            }
                        });
                    }
                }
            }
        });

        // key up form new folder
        $(TreeView.SELECTORS.inputmodalFolder).on('keyup', function () {
            $(this).parent().find('.lable-error').remove();
            $(this).parent().removeClass('has-error');
        });

        $(TreeView.SELECTORS.inputAvatar2D).on('change', function () {
            var url = $(this).val();
            //console.log(url);
            $('#imgAvatar2D').attr('src', url);
        });

        //Ẩn bảng property
        $('.toggle-detail-property').click(function () {
            $('.detail-property-header').css('display', 'none');
            $('.toggle-detail-property').css('display', 'none');
            $('.detail-property-content').css('display', 'none');
            widthModalRight = $('.modal-info-right').width();
            $('.detail-property1.detail-property-collapse').animate({
                width: widthModalRight
            }, 200);
        });

        //show bang property
        $('.li-modal-data-right').click(function () {
            if (tableConfig.GLOBAL.idDirectory != "" && $('.detail-property1.detail-property-collapse').width() < 100) {
                $('.detail-property-header').css('display', 'block');
                $('.toggle-detail-property').css('display', 'flex');
                $('.detail-property-content').css('display', 'block');
                $('.detail-property1.detail-property-collapse').animate({
                    width: 375
                }, 200);
            }
        });

        $('.btn-save-properties').on('click', function () {
            if (tableConfig.GLOBAL.idDirectory != '') {
                TreeView.UpdateProperty();
            } else {
                swal({
                    title: l("Layer:Notification"),
                    text: l("Layer:PleaseSelectActivityType"),
                    icon: "error",
                    button: l("Layer:Close"),
                });
            }
        });

        // ------------ select 2 clone activity----------------
        $(TreeView.SELECTORS.layerdata).select2()
            .on("select2:opening", function () {
                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff; color: var(--primary);");
            })
            .on("select2:closing", function () {
                var value = $(this).val();
                if (value != "0") {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                }
                else {
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            })
            .on('select2:select', function () {
                var value = $(this).val();
                if (value != "0") {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                }
                else {
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            });
        //----------------------------------------------------

        $(TreeView.SELECTORS.layerdata).prop("disabled", true);
        $(TreeView.SELECTORS.radiosLayer).on("change", function () {
            let check = $(this).val();
            if (check === "2") {
                $(TreeView.SELECTORS.layerdata).prop("disabled", false);
            } else {
                $(TreeView.SELECTORS.layerdata).prop("disabled", true);
            }
            TreeView.getAllLayerforClone();
        });

        //$('.toggle-detail-property').trigger('click');
        window.onresize = resize;

        $(TreeView.SELECTORS.content_count_directory).on('click', TreeView.SELECTORS.item_count, function () {
            var id = $(this).attr('id');

            var checkPermission = TreeView.CheckPermissionLayer(id);
            if (checkPermission) {
                var folderParent = TreeView.GLOBAL.ArrayTree.find(x => x.id == id);
                if (folderParent != undefined) {
                    if (folderParent.level > 0) {
                        TreeView.TriggerForcusToTreeNode(id);

                    }
                }

                //$(TreeView.SELECTORS.content_left_sidebar).animate({
                //    scrollTop: $('#item-li-' + id).offset().top - $(TreeView.SELECTORS.content_left_sidebar).offset().top
                //}, 'slow');
            }
        });

        //---auto code property
        $(TreeView.SELECTORS.dataName).on('change keyup', function () {
            //config.GLOBAL.dataName = $(this).val();
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }
            $(TreeView.SELECTORS.dataCode).val(value);
        });

        $(TreeView.SELECTORS.dataCode).on('change keyup', function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
                $(TreeView.SELECTORS.dataCode).val(value);
            }
        });
    },
    Splitter: function () {
    },
    // check validate
    checkFormInfor: function () {
        TreeView.clearLabelError();
        let check = true;
        let inputName = $('#nameFolder').val(); // tên thư mục
        if (!validateText(inputName, "text", 0, 0)) {
            insertError($('#nameFolder'), "other");
            check = false;
            TreeView.showLabelError('#nameFolder', jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:FolderName")));
        }

        let checkHtml = CheckedHtmlEntities(TreeView.SELECTORS.modalFolder);
        if (check) {
            check = checkHtml;
        }

        return check;
    },
    // check validate
    checkFormLayerInfor: function () {
        TreeView.clearLabelError();
        //let check = true;
        //if ($('#nameLayer').val() == '') {
        //    check = false;
        //    TreeView.showLabelError("input[name='Name-Layer']", "Vui lòng nhập tên!");
        //}
        //return check;
        let check = true;
        let inputName = $(TreeView.SELECTORS.inputName).val();
        if (!validateText(inputName, "text", 0, 0)) {
            insertError($(TreeView.SELECTORS.inputName), "other");
            check = false;
            TreeView.showLabelError(TreeView.SELECTORS.inputName, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:ActivityTypeName")));
        }

        if ($(TreeView.SELECTORS.radiosLayerChecked).val() == "2") {
            let radios = $(TreeView.SELECTORS.layerdata).val();
            if (radios == "0") {
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.layerdata, l("Layer:PleaseSelectInheritActivityType"));
            }
        }

        if ($(TreeView.SELECTORS.select_lop_du_lieu_create_layer).val() == "") {
            TreeView.showLabelError(TreeView.SELECTORS.select_lop_du_lieu_create_layer, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:DataLayer")));
            check = false;
        }

        let checkHtml = CheckedHtmlEntities(TreeView.SELECTORS.modalLayer);
        if (check) {
            check = checkHtml;
        }

        return check;
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group").addClass('has-error');
        $(element).parents(".form-group").append("<lable class='lable-error' style='color:red;'>" + text + "</lable>");
    },
    clearLabelError: function () {
        $('.form-group').find(".lable-error").remove();
        $('.form-group').removeClass("has-error");
    },

    // treeview
    delay: function (callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    },
    GetChildByParentId: function (id) {
        var lst = listFolder.filter(x => x.parentId == id);
        return lst;
    },
    GetAllLayerIdByIdParent: function (id) {
        var lst = TreeView.GetChildByParentId(id);
        var lstId = [];
        $.each(lst, function (i, obj) {
            if (obj.type == "folder") {
                lstForlerDelete.push(obj);
                var node = obj.nodes;
                if (obj.nodes != undefined && obj.nodes != null) {
                    for (var i = 0; i < node.length; i++) {
                        if (node[i].type == "folder") {
                            lstForlerDelete.push(node[i]);
                            var lstchild = TreeView.GetAllLayerIdByIdParent(node[i].id);
                            if (lstchild.length > 0) {
                                lstId.concat(lstchild);
                            }
                        } else if (node[i].type == "file") {
                            lstId.push(node[i].id);
                        }
                    }
                }
            } else if (obj.type == "file") {
                lstId.push(obj.id);
            }
        });
        return lstId;
    },
    getListLayerDataType: function () {
        $.ajax({
            type: "GET",
            url: '/api/HTKT/LayerDataType/get-list',
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok" && data.result != null && data.result.length > 0) {
                    tableConfig.GLOBAL.select = [];
                    $.each(data.result, function (i, obj) {
                        tableConfig.GLOBAL.select.push({ values: obj.value, text: obj.text });
                    });
                }
                $('.spinner').css('display', 'none');
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    setHideOrShowFolder: function (check) {
        if (check) {
            $(TreeView.SELECTORS.addFolder).attr("disabled", true);
            $(TreeView.SELECTORS.addFolder).css("cursor", 'no-drop');
        } else {
            $(TreeView.SELECTORS.addFolder).removeAttr("disabled");
            $(TreeView.SELECTORS.addFolder).css("cursor", 'pointer');
        }
    },
    getTreeView: function (id) {
        let viewtree = "";
        if (TreeView.GLOBAL.ArrayTree.length > 0) {
            let parentid = null;
            let obj = TreeView.GLOBAL.ArrayTree.find(x => x.id == id);
            if (typeof obj != "undefined" && obj != null) {
                parentid = obj.parentId;
                viewtree = obj.name;
            }
            while (parentid != null) {
                obj = TreeView.GLOBAL.ArrayTree.find(x => x.id == parentid);
                if (typeof obj != "undefined" && obj != null) {
                    parentid = obj.parentId;
                    viewtree = obj.name + " > " + viewtree;
                }
            }
        }
        return viewtree;
    },

    //Layer
    getAllLayerforClone: function () {
        if (listFolder.length > 0) {
            $(TreeView.SELECTORS.layerdata).children().remove();
            //let html = '<option value="0" data-select2-id="3">---chọn lớp---</option>';
            let listLayer = listFolder.filter(x => x.type == "file" && x.isDeleted == false);
            var option = new Option("", "0", false, false);
            $(TreeView.SELECTORS.layerdata).append(option).trigger('change');
            $.each(listLayer, function (i, obj) {
                let name = obj.name + ' (' + obj.codeTypeDirectory + ')';
                //html = html + name;
                var option = new Option(name, obj.id, false, false);
                $(TreeView.SELECTORS.layerdata).append(option).trigger('change');
            });

            $(TreeView.SELECTORS.layerdata).parent().find('.placeholder').attr('style', "");

            //$(TreeView.SELECTORS.layerdata).append(html);
        }
    },
    CloneLayeredForCurrentLayer: function (dto) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: TreeView.CONSTS.URL_CLONEPROPERTIESDIRECTORY,
            async: true,
            data: JSON.stringify(dto),
            success: function (re) {
                //console.log(re);
                if (re.code === "ok") {

                    abp.notify.success(l("Layer:CreateDataLayerSuccessfully"));
                    let pathName = document.location.pathname;
                    history.replaceState(null, null, pathName + "?id=" + re.result.idDirectoryActivity);

                    TreeView.GetDataTree(re.result.idDirectoryActivity);
                    TreeView.TriggerForcusToTreeNode(re.result.idDirectoryActivity);

                }
                else {
                    abp.notify.error(l("Layer:CreateDataLayerFailed"));
                }

                TreeView.CloseContentSection();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + ":" + errorThrown);
            }
        });
    },

    //Property Setting
    UpdateProperty: function (type) {
        // Upload file
        var formData = new FormData();
        if (type == "info") {
            if (typeof TreeView.GLOBAL.propertyData !== "undefined") {
                formData.append("IdDirectory", tableConfig.GLOBAL.idDirectory);
                formData.append("MinZoom", TreeView.GLOBAL.propertyData.minZoom);
                formData.append("MaxZoom", TreeView.GLOBAL.propertyData.maxZoom);
                formData.append("Stroke", TreeView.GLOBAL.propertyData.stroke);
                formData.append("StrokeWidth", TreeView.GLOBAL.propertyData.strokeWidth);
                formData.append("StrokeOpacity", TreeView.GLOBAL.propertyData.strokeOpacity);
                formData.append("StyleStroke", TreeView.GLOBAL.propertyData.styleStroke);
                formData.append("Fill", TreeView.GLOBAL.propertyData.fill);
                formData.append("FillOpacity", TreeView.GLOBAL.propertyData.fillOpacity);
            } else {
                formData.append("MinZoom", $('#min-zoom').val());
                formData.append("MaxZoom", $('#max-zoom').val());
                formData.append("Stroke", $('#strokeinput').val());
                formData.append("StrokeWidth", $('#stroke-width').val());
                formData.append("StrokeOpacity", $('#rangeMax-strokeopacity').val() / 100);
                formData.append("StyleStroke", $('#style-stroke').val());
                formData.append("Fill", $('#fillInput').val());
                formData.append("FillOpacity", $('#rangeMax-fillOpactity').val() / 100);
            }
            let url = datainfo.SendFileImage($(datainfo.SELECTORS.fileImage)[0].files[0]);
            formData.append("Image2D", url);
            url = datainfo.SendFileObj($(datainfo.SELECTORS.inputObj)[0].files[0]);
            formData.append("Object3D", url);
            url = datainfo.SendFileImage($(datainfo.SELECTORS.file3D)[0].files[0]);
            formData.append("Texture3D", url);
        } else {
            formData.append("IdDirectory", tableConfig.GLOBAL.idDirectory);
            formData.append("MinZoom", $('#min-zoom').val());
            formData.append("MaxZoom", $('#max-zoom').val());
            formData.append("Stroke", $('#strokeinput').val());
            formData.append("StrokeWidth", $('#stroke-width').val());
            formData.append("StrokeOpacity", $('#rangeMax-strokeopacity').val() / 100);
            formData.append("StyleStroke", $('#style-stroke').val());
            formData.append("Fill", $('#fillInput').val());
            formData.append("FillOpacity", $('#rangeMax-fillOpactity').val() / 100);
            if (typeof TreeView.GLOBAL.propertyData !== "undefined") {
                formData.append("Image2D", TreeView.GLOBAL.propertyData.image2D);
                formData.append("Object3D", TreeView.GLOBAL.propertyData.object3D);
                formData.append("Texture3D", TreeView.GLOBAL.propertyData.texture3D);
            } else {
                let url = datainfo.SendFileImage($(datainfo.SELECTORS.fileImage)[0].files[0]);
                formData.append("Image2D", url);
                url = datainfo.SendFileObj($(datainfo.SELECTORS.inputObj)[0].files[0]);
                formData.append("Object3D", url);
                url = datainfo.SendFileImage($(datainfo.SELECTORS.file3D)[0].files[0]);
                formData.append("Texture3D", url);
            }
        }
        $.ajax({
            type: "POST",
            async: false,
            processData: false,
            contentType: false,
            url: "/api/HTKT/PropertiesSetting/update",
            data: formData,
            success: function (data) {
                if (data.code == "ok") {
                    $('.toggle-detail-property').trigger('click');

                    abp.notify.success(l("Layer:SaveInfoSuccessfully"));
                }
                else {
                    console.log(data)
                }
            },
            error: function (err) {
                $('.toggle-detail-property').trigger('click');
            }
        });


    },
    getProperty: function () {
        var urls = window.location.origin + '/api/HTKT/PropertiesSetting/getByIdDictionary';
        $.ajax({
            type: "GET",
            url: urls,
            data: {
                id: tableConfig.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json",
            success: function (data) {
                if (data.code == "ok") {
                    if (data.result != null) {
                        $('#min-zoom').val(data.result.minZoom);
                        $('#max-zoom').val(data.result.maxZoom);

                        $('#strokeText').val(data.result.stroke);
                        $('#strokeinput').val(data.result.stroke);
                        $('#strokeColor').css('background', data.result.stroke);

                        $('#stroke-width').val(data.result.strokeWidth);

                        //do mo vien
                        const newValue1 = parseInt(data.result.strokeOpacity * 100);
                        $('#thumbMax-strokeopacity').css('left', newValue1 + '%');
                        $('#max-strokeopacity').html(newValue1 + '.00%');
                        $('#line-strokeopacity').css({
                            'right': (100 - newValue1) + '%'
                        });
                        $('#rangeMax-strokeopacity').val(newValue1);
                        $('#style-stroke').val(data.result.styleStroke);

                        $('#fillText').val(data.result.fill);
                        $('#fillInput').val(data.result.fill);
                        $('#fillColor').css('background', data.result.fill);

                        //do mo cua vung
                        const newValue2 = parseInt(data.result.fillOpacity * 100);
                        $('#thumbMax-fillOpactity').css('left', newValue2 + '%');
                        $('#max-fillOpactity').html(newValue2 + '.00%');
                        $('#line-fillOpactity').css({
                            'right': (100 - newValue2) + '%'
                        });
                        $('#rangeMax-fillOpactity').val(newValue2);
                        $(datainfo.SELECTORS.img_avatar).attr('src', data.result.image2D);
                        $(datainfo.SELECTORS.img_3D).attr('src', data.result.texture3D);
                        $(datainfo.SELECTORS.nameObject).attr('src', data.result.object3D);
                        TreeView.GLOBAL.propertyData = data.result;
                    }
                }
            },
        });
    },

    // new Tree
    GetDataTree: function () {
        $.ajax({
            type: "GET",
            url: TreeView.CONSTS.URL_GETLISTDIRECTORY,
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    $(TreeView.SELECTORS.searchTree).val(""); // reset search
                    listFolder = data;

                    if (TVisAdmin.toLowerCase() == "true") {
                        TreeView.GLOBAL.ArrayTree = data;
                    } else {
                        TreeView.GLOBAL.ListIdShow = TreeView.GetListIdLayerPermission(data);
                        TreeView.GLOBAL.ArrayTree = data.filter(x => TreeView.GLOBAL.ListIdShow.includes(x.id) || x.level == "0");
                    }

                    var parent = data.find(x => x.level == "0");
                    var treedynamic = TreeView.FillDynaTree(parent);
                    var tree = [];
                    tree.push(treedynamic);

                    var html = ``;

                    for (var i = 0; i < tree.length; i++) {
                        var name = tree[i].title;
                        var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;

                        if (tree[i].icon == "" || tree[i].icon == null) {
                            /* icon = `<i class="fa ${tree[i].isFolder ? "fa-folder-open" : "fa-file"} text-brand"></i>`;*/
                            if (tree[i].isFolder) {
                                icon = `<svg id="Group_1232" data-name="Group 1232" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                              <path id="Path_388" data-name="Path 388" d="M0,0H24V24H0Z" fill="none"/>
                              <path id="Path_389" data-name="Path 389" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H4V19l2-8H22.5l-2.31,9.243a1,1,0,0,1-.97.757Z" fill="var(--primary)"/>
                            </svg>`;
                            }
                            else {
                                icon = `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                <defs></defs>
                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z"></path>
                                <path class="b " style="fill:var(--primary)" d="M21,9V20.993A1,1,0,0,1,20.007,22H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,4,2H14V8a1,1,0,0,0,1,1Zm0-2H16V2Z"></path>
                            </svg>`;
                            }
                        }

                        if (tree[i].children.length > 0) {
                            html += `<li data-folder="${tree[i].isFolder}" data-id="${tree[i].key}" data-level="${tree[i].level}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${encodeURIComponent(name.toLowerCase())}" data-parent="${tree[i].parentId}">
                                    <a href="javascript:;" class="item-li active"  data-level="${tree[i].level}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                        <span class="pull-left-container">
                                            <i class="fa fa-angle-right pull-left icon-menu-tree open"></i>
                                        </span>
                                        <span class="title-item">${icon} <span class="name-layer-item"> ${name} (${tree[i].children.length})</span></span>

                                    <ul class="tree-ul open ul-border" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:block;">`;


                            html += TreeView.DataTreeGen(tree[i]);

                            html += `
                                    </ul>
                                </a>
                            </li>`;
                        }
                        else {
                            html += `<li data-folder="${tree[i].isFolder}" data-id="${tree[i].key}" data-level="${tree[i].level}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${encodeURIComponent(name.toLowerCase())}"  data-parent="${tree[i].parentId}">
                                        <a href="javascript:;" class="item-li"  data-level="${tree[i].level}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                        <span class="title-item">${icon} <span class="name-layer-item"> ${name}</span></span></a>
                                    </li>`;
                        }
                    }

                    //var html = TreeView.DataTreeGen(treedynamic);

                    $(`.tree-o-i`).html(html);

                    $(`.item-li`).each(function () {
                        if ($(this).find('.title-item').text() == "") {
                            $(this).remove();
                        }

                        //var isFolder = $(this).attr('data-type');
                        //var id = $(this).attr('data-id');

                        //if (isFolder != "true") {
                        //    var checkbox = `<label class="container-checkbox">
                        //                  <input type="checkbox">
                        //                  <span class="checkmark" id="checkmark-${id}" data-id="${id}"></span>
                        //                </label>`;

                        //    $(this).parent().prepend(checkbox);
                        //}
                    });
                }
                else {
                    console.log(res);
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    FillDynaTree: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            level: data.level,
            icon: null,
            parentId: data.parentId
        };

        var parent = TreeView.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    obj.icon = data.image2D;
                }
            }

            obj.key = parent.id;

            var lstChil = TreeView.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = TreeView.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            } else {
                obj.children = [];
            }
            return obj;
        }

    },
    DataTreeGen: function (data) {
        //$('.title').html(data.title);

        var tree = data.children;

        var html = ``;

        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;
            if (tree[i].icon == "" || tree[i].icon == null) {
                /* icon = `<i class="fa ${tree[i].isFolder ? "fa-folder-open" : "fa-file"} text-brand"></i>`;*/
                if (tree[i].isFolder) {
                    icon = `<svg id="Group_1232" data-name="Group 1232" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                              <path id="Path_388" data-name="Path 388" d="M0,0H24V24H0Z" fill="none"/>
                              <path id="Path_389" data-name="Path 389" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H4V19l2-8H22.5l-2.31,9.243a1,1,0,0,1-.97.757Z" fill="var(--primary)"/>
                            </svg>`;
                }
                else {
                    icon = `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                <defs></defs>
                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z"></path>
                                <path class="b " style="fill:var(--primary)" d="M21,9V20.993A1,1,0,0,1,20.007,22H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,4,2H14V8a1,1,0,0,0,1,1Zm0-2H16V2Z"></path>
                            </svg>`;
                }
            }

            if (tree[i].children.length > 0) {
                html += `<li  data-id="${tree[i].key}"  id="tree-item-${tree[i].key}" class="treeview" style="height: auto; " data-search="${encodeURIComponent(name.toLowerCase())}" data-parent="${tree[i].parentId}"
                              data-level="${tree[i].level}" data-folder="${tree[i].isFolder}" data-chil="${tree[i].children.length}">

                                    <a href="javascript:;" id="item-li-${tree[i].key}" class="item-li"  data-level="${tree[i].level}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                        <span class="pull-left-container">
                                            <i class="fa fa-angle-right pull-left icon-menu-tree" id="arrow-item-${tree[i].key}"></i>
                                        </span>
                                        <span class="title-item">${icon} <span class="name-layer-item"> ${name} (${tree[i].children.length})</span></span>

                            <ul class="tree-ul" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:none;">`;

                html += TreeView.DataTreeGen(tree[i]);

                html += `
                            </ul>
                        </a>
                    </li>`;
            }
            else {
                html += `<li  data-id="${tree[i].key}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${encodeURIComponent(name.toLowerCase())}"  data-parent="${tree[i].parentId}"
                              data-folder="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-level="${tree[i].level}" >

                            <a href="javascript:;" id="item-li-${tree[i].key}" class="item-li"  data-level="${tree[i].level}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                            <span class="pull-left-container"></span>
                            <span class="title-item">${icon} <span class="name-layer-item"> ${name}</span></span></a>
                        </li>`;
            }
        }

        return html;
    },
    EventNewTree: function () {
        $(TreeView.SELECTORS.tree_i_o).on('click', TreeView.SELECTORS.item_tree_i_o, function () {
            var id = $(this).attr('data-id');
            var folder = $(this).attr('data-type');
            var levelLayer = parseInt($(this).attr('data-level'));
            var checkAction = true;

            var checkPermission = TreeView.CheckPermissionLayer(id);

            TreeView.BtnActionPermission(checkPermission);

            if (checkPermission) {
                if (!folder || folder == "false") {
                    checkAction = TreeView.CheckActionLayer();
                    // $(TreeView.SELECTORS.div2).addClass('open');
                }
                else {
                    checkAction = TreeView.CheckActionFolder();
                    // $(TreeView.SELECTORS.div2).removeClass('open');
                }

                if (checkAction)// kiểm tra action hiện tại
                {
                    if (!folder || folder == "false") {
                        $(TreeView.SELECTORS.div2).show();
                        $('.content-count-directory').hide();
                    }
                    else {
                        $(TreeView.SELECTORS.div2).hide();
                        $('.content-count-directory').show();
                    }

                    parenWillFocus = $(this).parent().attr("data-parent");
                    let pathName = document.location.pathname;

                    model = TreeView.GLOBAL.ArrayTree.find(x => x.id == id);
                    if (model != null) {
                        thisId = model.id;
                        parentId = model.id;
                        level = model.level + 1;
                    }
                    tableConfig.GLOBAL.idDirectory = id;
                    TreeView.GLOBAL.idForcus = id;
                    if (folder != "true") {
                        history.replaceState(null, null, pathName + "?id=" + id);
                        TreeView.setHideOrShowFolder(true);
                        TreeView.ShowInfoData();
                        TreeView.DisabledActionForLayer();
                    }
                    else {
                        history.replaceState(null, null, pathName);
                        TreeView.setHideOrShowFolder(false);
                        tableConfig.resetTable();
                        TreeView.DisabledActionForFolder(levelLayer);
                    }

                    TreeView.ResetAction(); // reset lại action nếu đang ở trạng thái action
                }
            }
            else {
                $(TreeView.SELECTORS.div2).hide();
                $('.content-count-directory').show();
            }

            $(TreeView.SELECTORS.item_tree_i_o).removeClass('forcus');
            $(this).addClass('forcus');

            $(TreeView.SELECTORS.item_tree_i_o).removeClass('active');
            $(this).addClass('active');

            if (!$(this).next().hasClass('open')) {
                $(this).next().addClass('open');
                $(this).next().slideToggle("slow");

                var element = $(this).find('.icon-menu-tree');
                if (element.length > 0) {
                    element.addClass('open')
                }
            }
            else {
                if (levelLayer != 0) {
                    $(this).next().slideToggle("slow");
                    $(this).next().removeClass('open');
                    var element = $(this).find('.icon-menu-tree');
                    if (element.length > 0) {
                        element.removeClass('open')
                    }
                }
            }
        });

        $(TreeView.SELECTORS.searchTree).on('keyup', delay(function () {
            $('.treeview-not-result').remove();

            $('.tree-o-i .treeview').hide();
            var value = $(this).val().toLowerCase().trim();
            if (value != "") {
                $('.tree-o-i .treeview').each(function () {
                    var id = $(this).attr('data-id');
                    var name = decodeURIComponent($(this).attr('data-search'));
                    var parent = $(this).attr('data-parent');
                    var level = parseInt($(this).attr('data-level'));
                    var folder = $(this).attr('data-type');
                    var chil = parseInt($(this).attr('data-chil'));
                    if (level == 0) {
                        $(this).show();
                    }
                    if (xoa_dau(name.trim().toLowerCase()).includes(xoa_dau(value.trim().toLowerCase()))) {
                        $(this).show();

                        if (chil > 0) {
                            $('#arrow-item-' + id).addClass('open');

                            $('#tree-ul-' + id).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + id).slideDown();
                            $('#tree-ul-' + id + " li").show();
                        }

                        if (parent != "null") {
                            $('#arrow-item-' + parent).addClass('open');

                            $('#tree-ul-' + parent).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + parent).slideDown();

                            search_tree('#tree-item-' + parent, value);
                        }
                    }
                });

                var countNotResult = $(" .treeview").filter(function () {
                    return $(this).css('display') == 'none'
                }).length;

                if (($(".treeview").length - 1) == countNotResult) {
                    $('.tree-o-i .treeview').hide();
                    var li = `<div class="treeview-not-result"><span>Không tìm thấy kết quả tìm kiếm</span></div>`;
                    $('.tree-o-i').append(li);
                }
            }
            else {
                //$(TreeView.SELECTORS.tree_i_o + ' .treeview').show();
                TreeView.ReloadTree();
                $(TreeView.SELECTORS.content_left_sidebar).scrollTop(10);
                if (TreeView.GLOBAL.idForcus != "") {

                    TreeView.TriggerForcusToTreeNode(TreeView.GLOBAL.idForcus);

                    $('#item-li-' + TreeView.GLOBAL.idForcus).addClass('forcus');
                    $('#tree-item-' + TreeView.GLOBAL.idForcus).find('.icon-tree-' + TreeView.GLOBAL.idForcus).addClass('open'); // mở open icon
                    $('#tree-ul-' + TreeView.GLOBAL.idForcus).addClass('open'); // mở nhánh con
                    $('#tree-ul-' + TreeView.GLOBAL.idForcus).slideDown();
                    $('#tree-ul-' + TreeView.GLOBAL.idForcus + " li").show();
                }
            }
        }, 500));
    },
    ShowInfoData: function () {
        tableConfig.GLOBAL.oldLayerFocus = [];

        $.ajax({
            type: "GET",
            url: '/api/HTKT/PropertiesDirectoryActivity/get-directoryById',
            data: {
                id: tableConfig.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok") {
                    TreeView.clearLabelError();

                    $('#DataName').val(data.result.nameTypeDirectory);
                    $('#h5NameData').html(l("Layer:ActiveType:") + data.result.nameTypeDirectory);
                    $('#DataCode').val(data.result.codeTypeDirectory);

                    var idselected = '';
                    try {
                        idSelected = JSON.parse(data.result.idDirectory);
                    } catch {
                        if (data.result.idDirectory != null && data.result.idDirectory != "") {
                            idSelected = JSON.parse('["' + data.result.idDirectory + '"]');
                        } else {
                            idSelected = null;
                        }
                    }

                    if (idSelected != null && idSelected != "") {
                        $(tableConfig.SELECTORS.select_lop_du_lieu).parent().find('.placeholder').attr('style', 'transform: scale(0.8) translateY(-24px);background: #fff;');
                    }
                    else {
                        $(tableConfig.SELECTORS.select_lop_du_lieu).parent().find('.placeholder').attr('style', '');
                    }

                    $(tableConfig.SELECTORS.select_lop_du_lieu).val(idSelected).trigger("change.select2");

                    if (TVisAdmin.toLowerCase() != "true") {
                        tableConfig.GLOBAL.oldLayerFocus = idSelected.filter(x => !TVlstIdLayer.includes(x)); // gán old layer
                    }

                    $('#Version').val(data.result.verssionTypeDirectory);

                    if (data.result.idImplement != "" && data.result.idImplement != null) {
                        $(TreeView.SELECTORS.inputnameImplement).parent().find('.placeholder').attr('style', 'transform: scale(0.8) translateY(-24px);background: #fff;');
                    }
                    else {
                        $(TreeView.SELECTORS.inputnameImplement).parent().find('.placeholder').attr('style', '');
                    }

                    let nameImplement = listFolder.find(x => x.id == data.result.idImplement);
                    nameImplement = typeof nameImplement !== "undefined" && nameImplement !== null ? nameImplement.name + " (" + nameImplement.codeTypeDirectory + ")" : ""
                    $(TreeView.SELECTORS.inputnameImplement).val(nameImplement);
                    var result = [];
                    $.each(data.result.listProperties, function (i, obj) {
                        var element = {
                            NameProperties: obj.nameProperties,
                            CodeProperties: obj.codeProperties,
                            TypeProperties: obj.typeProperties,
                            IsShow: obj.isShow,
                            IsIndexing: obj.isIndexing,
                            IsRequest: obj.isRequest,
                            IsView: obj.isView,
                            IsHistory: obj.isHistory,
                            IsInheritance: obj.isInheritance,
                            IsAsync: obj.isAsync,
                            IsShowExploit: obj.isShowExploit,
                            DefalutValue: obj.defalutValue,
                            ShortDescription: obj.shortDescription,
                            TypeSystem: obj.typeSystem,
                            OrderProperties: obj.orderProperties
                        };
                        result.push(element);
                    });
                    tableConfig.GLOBAL.table = result;
                    tableConfig.addTbodyTr();
                    tableConfig.setEventAffter();
                }
                else {
                    console.log(data)
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    TriggerForcusToTreeNode: function (id) {
        thisId = id;
        //$(TreeView.SELECTORS.tree_i_o + ' .treeview').hide();
        $(TreeView.SELECTORS.tree_i_o + ' .treeview').each(function () {
            var idlayer = $(this).attr('data-id');
            var parent = $(this).attr('data-parent');
            var level = parseInt($(this).attr('data-level'));

            if (level == 0) {
                $(this).show();
            }

            if (id == idlayer) {
                $(this).show();
                if (parent != "null") {
                    $('#tree-ul-' + parent).addClass('open');
                    $('#arrow-item-' + parent).addClass('open');
                    $('#tree-ul-' + parent).slideDown();

                    //$($(this).find('.icon-menu-tree')).addClass('open');
                    forcus_tree('#tree-item-' + parent, parent);
                }
            }
        });

        $(TreeView.SELECTORS.content_left_sidebar).scrollTop(10);
        var activity = TreeView.GLOBAL.ArrayTree.find(x => x.id == id);
        if (activity != undefined && activity.level > 0) {

            $(TreeView.SELECTORS.content_left_sidebar).animate({
                scrollTop: $('#item-li-' + id).offset().top - $(TreeView.SELECTORS.content_left_sidebar).offset().top
            }, 'slow');


            $('#item-li-' + id).click();
        }

        function forcus_tree(element, id) {
            $(element).show();
            var idlayer = $(element).attr('data-id');
            var parent = $(element).attr('data-parent');
            var level = parseInt($(element).attr('data-level'));

            if (level == 0) {
                $(this).show();
            }

            if (idlayer != undefined) {
                if (idlayer == id) {
                    if (parent != "null") {
                        // hiển thị ul của cây
                        $('#tree-ul-' + parent).slideDown();
                        $('#tree-ul-' + parent).addClass('open');
                        //$('#arrow-item-' + parent).addClass('open');

                        forcus_tree('#tree-item-' + parent, parent);
                    }
                }
            }

        }
    },
    CheckDeleteLayer: function (id) {
        var result = true;
        $.ajax({
            type: "GET",
            url: TreeView.CONSTS.URL_CHECK_DELETE,
            data: {
                id: id
            },
            async: false,
            contentType: "application/json",
            success: function (res) {
                if (res.code == "ok") {
                    result = res.result;
                }
                else {
                    result = false;

                    console.log(res)
                }
             
            },
        });

        return result;
    },

    // xoa hoat dong
    DeleteFolderActivity: function (id, type) {
        lstForlerDelete = [];
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: "/api/htkt/directoryactivity/delete-folder",
            data: { id: id },
            success: function (result) {
                if (result.code == "ok" && result.result)
                {
                    var dto2 = TreeView.GetAllLayerIdByIdParent(id);
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: "/api/HTKT/PropertiesDirectoryActivity/delete",
                        data: JSON.stringify(dto2),
                        success: function (result) {

                            if (result.code == "ok") {
                                if (type === "file") {
                                    abp.notify.success(l("Activity:DeleteActivitySuccessfully"));
                                }
                                else {
                                    abp.notify.success(l("Layer:DeleteFolderSuccessfully"));
                                }

                                TreeView.GetDataTree();

                                $(`#item-li-${parenWillFocus}`).trigger("click");
                                let pathName = document.location.pathname.split('?')[0];
                                history.replaceState(null, null, pathName);
                            }
                            else {
                                if (type === "file") {
                                    abp.notify.error(l("Activity:DeleteActivityFailed"));
                                } else {
                                    abp.notify.error(l("Layer:DeleteFolderFailed"));
                                }

                                TreeView.GetDataTree();

                            }

                            TreeView.CloseContentSection();
                        },
                        error: function () {
                            TreeView.CloseContentSection();
                        }
                    });
                } else {
                    abp.notify.error(result.message);
                }

                TreeView.CloseContentSection();
            },
            error: function () {
                TreeView.CloseContentSection();
            }
        });
    },
    // update name thư mục
    UpdateNameFolder: function (id) {
        var dto = {
            id: id,
            name: $(TreeView.SELECTORS.inputNameFolder).val()
        }
        $.ajax({
            type: "POST",
            url: TreeView.CONSTS.URL_UPDATE_NAME_FOLDER,
            data: JSON.stringify(dto),
            contentType: "application/json",
            beforeSend: function () {
                abp.ui.setBusy(TreeView.SELECTORS.modalFolder);
                $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').hide();
            },
            success: function (res) {
                if (res.code == "ok") {

                    abp.notify.success(l("Layer:UpdateNameFolderSuccessfully"));

                } else {
                    abp.notify.error(l("Layer:UpdateNameFolderFailed"));
                }

                TreeView.GetDataTree(id);

                TreeView.TriggerForcusToTreeNode(id);


                TreeView.CloseContentSection();
            },
            complete: function () {
                abp.ui.clearBusy(TreeView.SELECTORS.modalFolder);
                $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').show();
            }
        });
    },

    CloseContentSection: function () {
        if (TreeView.GLOBAL.isAction > -1) {
            TreeView.GLOBAL.isAction = -1;
            $(TreeView.SELECTORS.content_section).removeClass('open');
            $(TreeView.SELECTORS.black_drop_content).hide();
            $(TreeView.SELECTORS.action_menu).removeClass('isForcus');

            var layer = TreeView.GLOBAL.ArrayTree.find(x => x.id == TreeView.GLOBAL.idForcus);
            if (layer.type != "folder") {
                TreeView.DisabledActionForLayer();
            }
            else {
                TreeView.DisabledActionForFolder(layer.level);
            }
            TreeView.ResetIconMenu();
        }
    },
    // những button layer ko được dùng
    DisabledActionForLayer: function () {
        if (TreeView.GLOBAL.isAction == -1) {
            $(TreeView.SELECTORS.action_menu).each(function () {
                var type = $(this).attr('data-type');
                switch (type) {
                    case "1": // thêm thư mục
                        $(this).prop('disabled', true);
                        $(this).addClass('disabled');
                        break;
                    case "2": // sửa thư mục
                        $(this).prop('disabled', true);
                        $(this).addClass('disabled');
                        break;
                    case "3": // thêm mới layer
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "4": // cấu hình
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "5": // phan quyền
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "6": // xóa
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    default:
                }
            })
        }
    },

    // những button folder ko được dùng
    DisabledActionForFolder: function (level) {
        if (TreeView.GLOBAL.isAction == -1) {
            $(TreeView.SELECTORS.action_menu).each(function () {
                var type = $(this).attr('data-type');
                switch (type) {
                    case "1": // thêm thư mục
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "2": // sửa thư mục
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "3": // thêm mới layer
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "4": // cấu hình
                        $(this).prop('disabled', true);
                        $(this).addClass('disabled');
                        break;
                    case "5": // phan quyền
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "6": // xóa
                        if (level != 0) {
                            $(this).prop('disabled', false);
                            $(this).removeClass('disabled');
                        }
                        else {
                            $(this).prop('disabled', true);
                            $(this).addClass('disabled');
                        }
                        break;
                    default:
                }
            })
        }
    },

    // kiểm tra action layer có hợp lệ
    CheckActionLayer: function () {
        var check = true;
        switch (TreeView.GLOBAL.isAction) {
            case 1: // thêm mới thư mục
                check = false;
                break;
            case 2: // sửa thư mục
                check = false;
                break;
            case 3: //thêm mới layer
                break;
            case 4: // setting layer
                break;
            case 5: // phân quyền
                break;
            case 6: //xóa
                break;
            default:
        }

        return check;
    },

    // kiếm tra action folder có hợp lệ
    CheckActionFolder: function () {
        var check = true;
        switch (TreeView.GLOBAL.isAction) {
            case 1: // thêm mới thư mục
                break;
            case 2: // sửa thư mục
                break;
            case 3: //thêm mới layer
                break;
            case 4: // setting layer
                check = false;
                break;
            case 5: // phân quyền
                break;
            case 6: //xóa
                break;
            default:
        }

        return check;
    },
    ResetAction: function () {
        switch (TreeView.GLOBAL.isAction) {
            case 1: // thêm mới folder
                break;
            case 2: // sửa tên foler
                $(TreeView.SELECTORS.inputNameFolder).val("");
                var folder = TreeView.GLOBAL.ArrayTree.find(x => x.id == TreeView.GLOBAL.idForcus);
                if (folder != undefined) {
                    $(TreeView.SELECTORS.inputNameFolder).val(folder.name);
                }
                break;
            case 3: //thêm mới layer
                break;
            case 4: // cập nhật cấu hình layer
                break;
            case 5: // phân quyền
                PermissionLayer.GetPermissionDirectory(); // hiển thị lại phân quyền
                break;
            case 6: // xóa đối tương hoặc thư mục
                break;
            default:
        }
    },
    // reset icon menu
    ResetIconMenu: function () {
        $(TreeView.SELECTORS.action_menu).each(function () {
            var type = $(this).attr('data-type');
            switch (type) {
                case "1":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                <path id="Path_6541" data-name="Path 6541" d="M12.414,5H21a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414ZM4,5V19H20V7H11.586l-2-2Zm7,7V9h2v3h3v2H13v3H11V14H8V12Z" transform="translate(-2 -3)" fill="rgba(0,0,0,0.56)"></path>
                            </svg>`);
                    break;
                case "2":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="18" height="17.971" viewBox="0 0 18 17.971">
                                        <path id="Path_6611" data-name="Path 6611" d="M5,19H6.414l9.314-9.314L14.314,8.272,5,17.586Zm16,2H3V16.757L16.435,3.322a1,1,0,0,1,1.414,0l2.829,2.829a1,1,0,0,1,0,1.414L9.243,19H21ZM15.728,6.858l1.414,1.414,1.414-1.414L17.142,5.444,15.728,6.858Z" transform="translate(-3 -3.029)" fill="rgba(0,0,0,0.56)"></path>
                                    </svg>`);
                    break;
                case "3": // thêm mới layer
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <defs>
                            </defs>
                            <path class="a" d="M0,0H24V24H0Z" style="fill:none;"></path>
                            <path class="b" style="fill: rgba(0,0,0,0.56);" d="M15,4H5V20H19V8H15ZM3,2.992A1,1,0,0,1,4,2H16l5,5V20.993A1,1,0,0,1,20.007,22H3.993A1,1,0,0,1,3,21.008ZM11,11V8h2v3h3v2H13v3H11V13H8V11Z"></path>
                        </svg>`);
                    break;
                case "4": // cấu hình
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path id="Path_33" data-name="Path 33" d="M2,12a10.036,10.036,0,0,1,.316-2.5A3,3,0,0,0,4.99,4.867a9.99,9.99,0,0,1,4.335-2.5,3,3,0,0,0,5.348,0,9.99,9.99,0,0,1,4.335,2.5A3,3,0,0,0,21.683,9.5a10.075,10.075,0,0,1,0,5.007,3,3,0,0,0-2.675,4.629,9.99,9.99,0,0,1-4.335,2.505,3,3,0,0,0-5.348,0A9.99,9.99,0,0,1,4.99,19.133,3,3,0,0,0,2.315,14.5,10.056,10.056,0,0,1,2,12Zm4.8,3a5,5,0,0,1,.564,3.524,8.007,8.007,0,0,0,1.3.75,5,5,0,0,1,6.67,0,8.007,8.007,0,0,0,1.3-.75,5,5,0,0,1,3.334-5.774,8.126,8.126,0,0,0,0-1.5,5,5,0,0,1-3.335-5.774,7.989,7.989,0,0,0-1.3-.75,5,5,0,0,1-6.669,0,7.99,7.99,0,0,0-1.3.75A5,5,0,0,1,4.034,11.25a8.126,8.126,0,0,0,0,1.5A4.993,4.993,0,0,1,6.805,15ZM12,15a3,3,0,1,1,3-3A3,3,0,0,1,12,15Zm0-2a1,1,0,1,0-1-1A1,1,0,0,0,12,13Z" transform="translate(0)" fill="rgba(0,0,0,0.56)"></path>
                                </svg>`);
                    break;
                case "5": // phan quyền
                    $(this).html(`<svg id="Group_2870" data-name="Group 2870" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path id="Path_6550" data-name="Path 6550" d="M0,0H24V24H0Z" fill="none"></path>
                                    <path id="Path_6551" data-name="Path 6551" d="M12,14v2a6,6,0,0,0-6,6H4a8,8,0,0,1,8-8Zm0-1a6,6,0,1,1,6-6A6,6,0,0,1,12,13Zm0-2A4,4,0,1,0,8,7,4,4,0,0,0,12,11Zm2.6,7.812a3.51,3.51,0,0,1,0-1.623l-.992-.573,1-1.732.992.573A3.5,3.5,0,0,1,17,14.645V13.5h2v1.145a3.492,3.492,0,0,1,1.405.812l.992-.573,1,1.732-.992.573a3.51,3.51,0,0,1,0,1.622l.992.573-1,1.732-.992-.573a3.5,3.5,0,0,1-1.4.812V22.5H17V21.355a3.5,3.5,0,0,1-1.4-.812l-.992.573-1-1.732.992-.572ZM18,19.5A1.5,1.5,0,1,0,16.5,18,1.5,1.5,0,0,0,18,19.5Z" fill="rgba(0,0,0,0.56)"></path>
                                </svg>`);
                    break;
                case "6":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <defs>
                                </defs>
                                <path class="a" d="M0,0H24V24H0Z" style="fill:none;"></path>
                                <path class="b" style="fill: rgba(0,0,0,0.56);" d="M20,7V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H2V5H22V7ZM6,7V20H18V7ZM7,2H17V4H7Zm4,8h2v7H11Z"></path>
                            </svg>`);
                    break;
                default:
            }
        })
    },

    // get dữ liệu được sử dụng
    GetDirectoryCount: function () {
        $.ajax({
            type: "GET",
            url: TreeView.CONSTS.URL_GETLISTDIRECTORY_COUNT,
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //console.log(data);
                if (data.code == "ok") {
                    var text = '';
                    let order = 1;

                    data.result.sort(function (a, b) {
                        try {
                            var lstLayer1 = JSON.parse(a.idDirectory);
                            var lstLayer2 = JSON.parse(b.idDirectory);

                            if (lstLayer1 == null) {
                                return 1;
                            }
                            else {
                                if (lstLayer2 == null) {
                                    return -1;
                                }
                            }

                            var a1 = lstLayer1.length, b1 = lstLayer2.length;

                            if (a1 == b1) return 0;
                        }
                        catch (err) { return -1; }
                        return a1 > b1 ? -1 : 1;
                    });

                    if (data.result.length > 10) {
                        data.result = data.result.slice(0, 10);
                    }

                    var lstDataCount = data.result;

                    if (TVisAdmin.toLowerCase() == "false") // tài khoản đăng nhập không phải admin
                    {
                        lstDataCount = [];

                        var checkDataPermission = data.result.filter(x => TVlstIdLayer.some(y => y == x.id)); // lấy danh sách lớp được phân quyền theo tài khoản
                        if (checkDataPermission != undefined && checkDataPermission.length < 10) {
                            var dem = 10 - checkDataPermission.length;

                            $.each(checkDataPermission, function (index, obj) {
                                lstDataCount.push({
                                    id: obj.id,
                                    idDirectory: obj.id,
                                    image2D: obj.image2D,
                                    isDeleted: obj.isDeleted,
                                    level: obj.level,
                                    name: obj.name
                                });
                            });

                            $.each(TreeView.GLOBAL.ArrayTree, function (index, obj) {
                                if (dem == 0) {
                                    return;
                                }

                                if (obj.type.toLowerCase() != "folder") {
                                    var checkDuplicate = lstDataCount.find(x => x.id == obj.id); // kiểm tra danh sach hiển thị đã co hay chưa
                                    if (checkDuplicate == undefined) {
                                        var directory = TreeView.GLOBAL.ListIdShow.find(x => x == obj.id);
                                        if (directory != undefined) {
                                            lstDataCount.push({
                                                id: obj.id,
                                                idDirectory: obj.id,
                                                image2D: obj.image2D,
                                                isDeleted: obj.isDeleted,
                                                level: obj.level,
                                                name: obj.name
                                            });
                                            dem--;
                                        }
                                    }
                                }
                            });
                        }
                    }

                    $.each(lstDataCount, function (i, obj) {
                        var directory = obj;
                        if (directory != undefined) {
                            var classname = "";
                            if (order <= 5) {
                                classname += " div-border-bottom"
                            }
                            if (order % 5 !== 0) {
                                classname += " div-border-right"
                            }
                            text += `<div class="item-count-ten div-border ${classname} position-relative" id="${directory.id}" data-name="${directory.name}" >
                                        <div class="content-layer">
                                            ${obj.image2D != "" && obj.image2D != null ?
                                    `<img src="${obj.image2D}" class="image-avatar">` :
                                    `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lightning-fill svg-avatar" viewBox="0 0 16 16">
                                                <path d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                            </svg>`}
                                            <div class="name-avatar">
                                                <span style="font-weight: 500;">${directory.name}</span>
                                            </div>
                                        </div>
                                </div>`;
                            $(".file-upload-content").html('');
                            $(".file-upload-content").append(text);
                            order++;
                        }
                    });
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    ReloadTree: function () {
        $(TreeView.SELECTORS.searchTree).val(""); // reset search
        var parent = TreeView.GLOBAL.ArrayTree.find(x => x.level == "0");
        var treedynamic = TreeView.FillDynaTree(parent);
        var tree = [];
        tree.push(treedynamic);

        var html = '';
        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;

            if (tree[i].icon == "" || tree[i].icon == null) {
                /* icon = `<i class="fa ${tree[i].isFolder ? "fa-folder-open" : "fa-file"} text-brand"></i>`;*/
                if (tree[i].isFolder) {
                    icon = `<svg id="Group_1232" data-name="Group 1232" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                              <path id="Path_388" data-name="Path 388" d="M0,0H24V24H0Z" fill="none"/>
                              <path id="Path_389" data-name="Path 389" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H4V19l2-8H22.5l-2.31,9.243a1,1,0,0,1-.97.757Z" fill="var(--primary)"/>
                            </svg>`;
                }
                else {
                    icon = `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                <defs></defs>
                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z"></path>
                                <path class="b " style="fill:var(--primary)" d="M21,9V20.993A1,1,0,0,1,20.007,22H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,4,2H14V8a1,1,0,0,0,1,1Zm0-2H16V2Z"></path>
                            </svg>`;
                }
            }

            if (tree[i].children.length > 0) {
                html += `<li data-folder="${tree[i].isFolder}" data-id="${tree[i].key}" data-level="${tree[i].level}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${name}" data-parent="${tree[i].parentId}">
                                    <a href="javascript:;" class="item-li active"  data-level="${tree[i].level}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                        <span class="pull-left-container">
                                            <i class="fa fa-angle-right pull-left icon-menu-tree open"></i>
                                        </span>
                                        <span class="title-item">${icon} <span class="name-layer-item"> ${name} (${tree[i].children.length})</span></span>

                                    <ul class="tree-ul open ul-border" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:block;">`;


                html += TreeView.DataTreeGen(tree[i]);

                html += `
                                    </ul>
                                </a>
                            </li>`;
            }
            else {
                html += `<li data-folder="${tree[i].isFolder}" data-id="${tree[i].key}" data-level="${tree[i].level}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${name}"  data-parent="${tree[i].parentId}">
                                        <a href="javascript:;" class="item-li"  data-level="${tree[i].level}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                        <span class="title-item">${icon} <span class="name-layer-item"> ${name}</span></span></a>
                                    </li>`;
            }
        }

        $(`.tree-o-i`).html('');
        $(`.tree-o-i`).html(html);

        $(`.item-li`).each(function () {
            if ($(this).find('.title-item').text() == "") {
                $(this).remove();
            }

            var isFolder = $(this).attr('data-type');
            var id = $(this).attr('data-id');
        });
    },

    GetListIdLayerPermission: function (data) {
        var lst = data.filter(function (obj, index) {
            if (obj.idDirectory != "" && obj.idDirectory != null && obj.idDirectory != "null") {
                try {
                    if (obj.idDirectory.indexOf("[") == -1) {
                        obj.idDirectory = `[${obj.idDirectory}]`;
                    }

                    var lstIdDirectory = JSON.parse(obj.idDirectory);
                    return lstIdDirectory.some(r => TVlstIdLayer.indexOf(r) >= 0);
                }
                catch (err) {
                    return false;
                }
            }
            return false;
        });

        var lstcheck = [];
        $.each(lst, function (i, obj) {
            let listid = TreeView.getListIdParent(obj, data);
            lstcheck = lstcheck.concat(listid);
        });

        return lstcheck;
    },
    getListIdParent: function (obj, data) {
        let lst = [obj.id];
        if (obj.parentId != "" && obj.parentId != null) {
            let check = data.find(x => x.id == obj.parentId);
            if (check != null && check != undefined) {
                let lstcheck = TreeView.getListIdParent(check, data);
                lst = lst.concat(lstcheck);
            }
        }
        return lst;
    },
    CheckPermissionLayer: function (id) {
        var check = true;

        if (TVisAdmin.toLowerCase() == "false") {
            var layer = TreeView.GLOBAL.ArrayTree.find(x => x.id == id);

            if (layer != undefined) {
                if (layer.idDirectory != "" && layer.idDirectory != null && layer.idDirectory != "null") {
                    try {
                        if (layer.idDirectory.indexOf("[") == -1) {
                            layer.idDirectory = `[${layer.idDirectory}]`;
                        }

                        var lstIdDirectory = JSON.parse(layer.idDirectory);
                        check = lstIdDirectory.some(r => TVlstIdLayer.indexOf(r) >= 0);
                    }
                    catch (err) { check = false; }
                }
            }
        }

        return check;
    },
    BtnActionPermission: function (isShow) {
        if (isShow) {
            $('.action-menu').show();
            $('.right-line-horizel-primary').show();
            $('.right-line-horizel').show();
        }
        else {
            $('.action-menu').hide();
            $('.right-line-horizel-primary').hide();
            $('.right-line-horizel').hide();
        }
    }
}

function resize() {
    TreeView.Splitter();
}

$(function () {
    TreeView.getListLayerDataType();
    TreeView.init();
    //datainfo.init();
    tableConfig.GLOBAL.table = [{
        //ID: "1",
        NameProperties: "Jacob",
        CodeProperties: "Thornton",
        TypeProperties: "int",
        IsShow: true,
        IsIndexing: true,
        IsRequest: true,
        IsView: true,
        IsHistory: true,
        IsInheritance: true,
        IsAsync: true,
        IsShowExploit: true,
        DefalutValue: "",
        ShortDescription: "",
        TypeSystem: 1,
        OrderProperties: 1,
    },
    {
        //ID: "1",
        NameProperties: "Jacob",
        CodeProperties: "Thornton",
        TypeProperties: "string",
        IsShow: true,
        IsIndexing: true,
        IsRequest: true,
        IsView: true,
        IsHistory: true,
        IsInheritance: true,
        IsAsync: true,
        IsShowExploit: true,
        DefalutValue: "",
        ShortDescription: "",
        TypeSystem: 2,
        OrderProperties: 2,
    },
    {
        //ID: "1",
        NameProperties: "Jacob",
        CodeProperties: "Thornton",
        TypeProperties: "geojson",
        IsShow: true,
        IsIndexing: true,
        IsRequest: true,
        IsView: true,
        IsHistory: true,
        IsInheritance: true,
        IsAsync: true,
        IsShowExploit: true,
        DefalutValue: "",
        ShortDescription: "",
        TypeSystem: 3,
        OrderProperties: 3,
    }];
    //tableConfig.GLOBAL.select = [{
    //    values: "bool",
    //    text: "Có/Không",
    //},
    //{
    //    values: "text",
    //    text: "Văn bản",
    //}, {
    //    values: "float",
    //    text: "Số thực",
    //}, {
    //    values: "int",
    //    text: "Số",
    //}, {
    //    values: "list",
    //    text: "Danh sách",
    //}, {
    //    values: "date",
    //    text: "Ngày tháng",
    //}, {
    //    values: "string",
    //    text: "Chuỗi",
    //}, {
    //    values: "stringlarge",
    //    text: "Chuỗi lớn",
    //}, {
    //    values: "geojson",
    //    text: "Bản đồ",
    //    }];
    $(TreeView.SELECTORS.dataName).on('change', function () {
        tableConfig.GLOBAL.dataName = $(this).val();
    });

    $(TreeView.SELECTORS.dataCode).on('change', function () {
        tableConfig.GLOBAL.dataCode = $(this).val();
    });

    $(TreeView.SELECTORS.dataVersion).on('change', function () {
        tableConfig.GLOBAL.dataVersion = $(this).val();
    });

    //tableConfig.getDefault();
    tableConfig.GLOBAL.table = [];
    tableConfig.init();
});

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
};

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    str = str.replace(/\s/g, '');
    return str;
}

function CheckedHtmlEntities(form) {
    var check = true;
    $(`${form} input[type=text]`).each(function () {
        var value = $(this).val();
        let inputName = $(this).attr('data-name');
        var status = $(this).attr("data-required");
        if (status != undefined) {
            status = status.toLowerCase()
        }

        if ($(this).val() != ""/* && status == "true"*/) {
            let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;
            if (re.test(value)) {
                $(this).parents(".form-group-modal").find('.lable-error').remove();
                insertError($(this), "other");
                TreeView.showLabelError(this, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), inputName));
                check = false;
            }
        }
    });
    return check;
}

function search_tree(element, value) {
    if ($(element).css("display") == "none") {
        $(element).show();
        var id = $(element).attr('data-id');
        var name = decodeURIComponent($(element).attr('data-search'));
        var parent = $(element).attr('data-parent');
        var level = parseInt($(this).attr('data-level'));
        if (level == 0) {
            $(this).show();
        }
        if (name != undefined) {
            //if (name.toLowerCase().includes(value))
            {
                if (parent != "null") {
                    // hiển thị ul của cây
                    $('#arrow-item-' + parent).addClass('open');
                    $('#tree-ul-' + parent).addClass('open'); // mở nhánh con
                    $('#tree-ul-' + parent).slideDown();

                    search_tree('#tree-item-' + parent, value);
                }
            }
        }
    }
}