﻿var l = abp.localization.getResource('HTKT');
var tableConfig = {
    GLOBAL: {
        table: [],
        select: [],
        dataName: '',
        dataCode: '',
        dataVersion: 0,
        idDirectory: '',
        listTypeList: [],
        previousSelect: '',
        rowSelected: null,
        ArrayLayer: [],
        oldLayerFocus: [] // value layer current khi focus loại hoạt động
    },
    CONSTS: {
        URL_AJAXSAVE: '/api/HTKT/PropertiesDirectoryActivity/create',
        URL_AJAXGETDEFAULTDATA: '/api/HTKT/PropertiesDirectoryActivity/get-directoryById',
        URL_AJAXGETDEFAULTDATAFIRST: '/api/HTKT/PropertiesDefault/get-list',
        URL_AJAXGETLISTDIRECTORY: '/api/htkt/directory/get-list',
        URL_CHECK_DELETE_PROPERTIES: "/api/HTKT/detailActivity/check-delete-properties"
    },
    SELECTORS: {
        tbodySystem: ".table-properties-system tbody",
        tbody: ".table-properties tbody",
        tabletrtd: ".table-properties tr td",
        tabletr: ".table-properties tr",
        textinsert: "input.textinsert",
        eidiinput: ".editinput",
        editselect: ".editselect",
        addRow: ".AddRow",
        headingOne: ".panel-heading a",
        // headingTwo: "#headingTwo a",
        btnSave: '.SaveProperties',
        dataName: '#DataName',
        dataCode: '#DataCode',
        version: '#Version',
        select_lop_du_lieu: ".danh-sach-lop-du-lieu",
        collapseOne: "#collapseOne",
        iconHeadingOne: "#headingOne a i",
        collapseTwo: "#collapseTwo",
        iconHeadingTwo: "#headingTwo a i",
        modalList: ".exampleModal-list",
        form_input_properties_basic: ".form-input-properties"
    },
    init: function () {
        tableConfig.addTbodyTr();
        //tableConfig.setDatatable();
        tableConfig.setUpEvent();
        tableConfig.setUpEventModalList();
        this.SelectedLayer();
    },
    //add data vào table
    addTbodyTr: function () {
        $(tableConfig.SELECTORS.tbody).children().remove();
        $(tableConfig.SELECTORS.tbodySystem).children().remove();
        let hproperties = "";
        let hsystem = "";
        $.each(tableConfig.GLOBAL.table, function (i, obj) {

            if (obj.TypeSystem == 3 || obj.TypeSystem == 2) {
                if (obj.TypeProperties == "bool") {
                    obj.IsRequest = false;
                }

                let html = `<tr data-count="${i}" data-system="${obj.TypeSystem}">
                                <th style="width:2%;text-align: center;">${(i + 1)}</th>
                                <td style="width:10%" data-code="NameProperties"><div class="editinput">${obj.NameProperties}</div></td>
                                <td style="width:10%" data-code="CodeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : "input-default"}">${obj.CodeProperties}</div></td>
                                <td style="width:10%" data-code="TypeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : "input-default"}">${(tableConfig.GLOBAL.select.find(x => x.values == obj.TypeProperties).text)}</div></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShow"><input type="checkbox" class="editcheckbox" ${(obj.IsShow ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShow ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsIndexing"><input type="checkbox" class="editcheckbox" ${(obj.IsIndexing ? "checked" : "")}> <span class="checkboxmark ${(obj.IsIndexing ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsRequest"><input type="checkbox" class="editcheckbox" ${obj.TypeProperties == "bool" ? "" : obj.IsRequest ? "checked" : ""}> <span class="checkboxmark ${obj.TypeProperties == "bool" ? "noteditcheckbox" : ""}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsView"><input type="checkbox" class="editcheckbox" ${(obj.IsView ? "checked" : "")}> <span class="checkboxmark ${(obj.IsView ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsHistory"><input  type="checkbox" class="editcheckbox" ${(obj.IsHistory ? "checked" : "")}> <span class="checkboxmark ${(obj.IsHistory ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsInheritance"><input  type="checkbox" class="editcheckbox" ${(obj.IsInheritance ? "checked" : "")}> <span class="checkboxmark ${(obj.IsInheritance ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsAsync"><input type="checkbox" class="editcheckbox" ${(obj.IsAsync ? "checked" : "")}> <span class="checkboxmark ${(obj.IsAsync ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox ${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "unselectable" : ""}" style="width:2%;text-align: center;position: relative;" data-code="DefalutValue"><div class="editinput">${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "" : obj.DefalutValue}</div></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="ShortDescription"><div class="editinput">${obj.ShortDescription}</div></td>
                                ${obj.TypeSystem == 2 ? '<td style="width: 5%"></td>' : tableConfig.showButtonRow(obj, i, tableConfig.GLOBAL.table.length)}
                            </tr>`;
                hproperties = hproperties + html;
            }
            if (obj.TypeSystem == 1) {
                let html = `<tr class="notedit" data-count="${i}" data-system="${obj.TypeSystem}">
                                <th style="width:2%;text-align: center;">${(i + 1)}</th>
                                <td style="width:10%" data-code="NameProperties"><div>${obj.NameProperties}</div></td>
                                <td style="width:10%" data-code="CodeProperties"><div>${obj.CodeProperties}</div></td>
                                <td style="width:10%" data-code="TypeProperties"><div>${(tableConfig.GLOBAL.select.find(x => x.values == obj.TypeProperties).text)}</div></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShow"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsShow ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsShow ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsIndexing"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsIndexing ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsIndexing ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsRequest"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsRequest ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsRequest ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsView"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsView ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsView ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsHistory"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsHistory ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsHistory ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsInheritance"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsInheritance ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsInheritance ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsAsync"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsAsync ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsAsync ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="ShowExploit"><input type="checkbox" class="editcheckbox" ${(obj.IsExploit ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShowExploit ? "check" : "")}" data-id=""></span></td>
                                <td style="width:10%" data-code="DefalutValue"><div>${obj.DefalutValue}</div></td>
                                <td style="width:10%" data-code="ShortDescription"><div >${obj.ShortDescription}</div></td>
                                <td style="width: 5%"></td>
                            </tr>`;
                hsystem = hsystem + html;
            }
        });
        $(tableConfig.SELECTORS.tbody).append(hproperties);
        $(tableConfig.SELECTORS.tbodySystem).append(hsystem);
    },
    setUpEvent: function () {
        tableConfig.setEventAffter();
        $(tableConfig.SELECTORS.addRow).on("click", function () {
            let check = tableConfig.GLOBAL.table.filter(x => x.CodeProperties.trim() == "" || x.NameProperties.trim() == "");
            if (check.length == 0) {
                //$(tableConfig.SELECTORS.btnSave).attr("disabled", true);
                let count = (tableConfig.GLOBAL.table.length + 1);
                var data = {
                    NameProperties: "",
                    CodeProperties: "",
                    TypeProperties: "string",
                    IsShow: true,
                    IsIndexing: false,
                    IsRequest: false,
                    IsView: false,
                    IsHistory: false,
                    IsInheritance: false,
                    IsAsync: false,
                    IsShowExploit: false,
                    DefalutValue: "",
                    ShortDescription: "",
                    TypeSystem: 3,
                    OrderProperties: count,
                }
                tableConfig.GLOBAL.table.push(data);
                tableConfig.addTbodyTr();
                tableConfig.setEventAffter();
                setTimeout(function () {
                    $('.table-properties tr[data-count="' + (tableConfig.GLOBAL.table.length - 1) + '"] td[data-code="NameProperties"] .editinput').click();
                    $('.table-properties tr[data-count="' + (tableConfig.GLOBAL.table.length - 1) + '"] td[data-code="NameProperties"] .textinsert').focus();
                }, 500);
            }
            else {
                swal({
                    title: l("Layer:Notification"),
                    text: l("Layer:PropertyDataCannotEmpty"),
                    icon: "error",
                    button: l("Layer:Close"),
                });
            }
        });
        $(tableConfig.SELECTORS.headingOne).on("click", function () {
            var name = $(this).attr('data-name');
            if ($(this).hasClass("open")) {
                $(this).removeClass("open");
                $(this).find('.icon-colspan').html(`<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="width:24px; height:24px;transform: translate(-1%, 4%);"
	                                    viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                                <style type="text/css">
	                                .st0{fill:#FFFFFF;}
                                </style>
                                <path id="Path_6613" class="st0" d="M4.24,1.93h16c0.55,0,1,0.45,1,1v16c0,0.55-0.45,1-1,1h-16c-0.55,0-1-0.45-1-1v-16
	                                C3.24,2.37,3.69,1.93,4.24,1.93z M5.24,3.93v14h14v-14H5.24z M7.24,9.93h10v2h-10V9.93z M13.24,5.93v10h-2v-10H13.24z"/>
                 </svg>`);
                $(this).parent().parent().find(".panel-collapse").removeClass("show");
            } else {
                $(this).addClass("open");
                $(this).find('.icon-colspan').html(`<svg id="Component_212_2" data-name="Component 212 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="width:24px; height:24px;">
                                                        <path id="Path_6612" data-name="Path 6612" d="M0,0H24V24H0Z" fill="none"></path>
                                                        <path id="Path_6613" data-name="Path 6613" d="M4,3H20a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H4a1,1,0,0,1-1-1V4A1,1,0,0,1,4,3ZM5,5V19H19V5Zm2,6H17v2H7Z" fill="#fff"></path>
                                                    </svg> `);

                $(this).parent().parent().find(".panel-collapse").addClass("show");
            }
        });

        $(tableConfig.SELECTORS.btnSave).on('click', function (e) {
            tableConfig.setOrder();
            if ($(tableConfig.SELECTORS.dataName).val().length > 100) {
                swal({
                    title: l("Layer:Notification"),
                    text: l("Layer:ActiviTypeNameMax100"),
                    icon: "warning",
                    button: l("Layer:Close"),
                });
            } else {
                if (thisId === undefined || thisId === null) {
                    swal({
                        title: l("Layer:Notification"),
                        text: l("Layer:PleaseSelectDirectoryOrLayer"),
                        icon: "warning",
                        button: l("Layer:Close"),
                    });
                } else {
                    if (tableConfig.GLOBAL.idDirectory != "" && tableConfig.checkFormValidate()) {
                        let check = tableConfig.GLOBAL.table.filter(x => x.CodeProperties.trim() == "" || x.NameProperties.trim() == "");
                        if (check.length == 0) {
                            var object = {
                                IdDirectoryActivity: tableConfig.GLOBAL.idDirectory,
                                NameTypeDirectory: $(tableConfig.SELECTORS.dataName).val(),
                                CodeTypeDirectory: $(tableConfig.SELECTORS.dataCode).val(),
                                //VerssionTypeDirectory: parseFloat($(tableConfig.SELECTORS.version).val()),
                                IdImplement: "",
                                ListProperties: tableConfig.GLOBAL.table
                            };

                            var arrayLayer = $(tableConfig.SELECTORS.select_lop_du_lieu).val().concat(tableConfig.GLOBAL.oldLayerFocus);
                            abp.ui.setBusy();
                            setTimeout(function () {
                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    //contentType: false,
                                    //processData: false,
                                    contentType: "application/json-patch+json",
                                    async: false,
                                    url: tableConfig.CONSTS.URL_AJAXSAVE,
                                    data: JSON.stringify(object),
                                    success: function (data) {
                                        if (data.code == "ok") {
                                            $.ajax({
                                                type: "GET",
                                                contentType: "application/json-patch+json",
                                                async: false,
                                                url: "/api/htkt/directoryactivity/update-name",
                                                data: {
                                                    id: object.IdDirectoryActivity,
                                                    name: object.NameTypeDirectory,
                                                    idDirectory: JSON.stringify(arrayLayer)
                                                },
                                                success: function (res) {
                                                    if (res.code == "ok" && res.result) {
                                                        abp.notify.success(l("Activity:UpdateActivitySuccesfully"));

                                                        var children = parseInt($('#tree-item-' + object.IdDirectoryActivity).attr('data-chil'));

                                                        $('#tree-item-' + object.IdDirectoryActivity).find('.name-layer-item').eq(0).text(object.NameTypeDirectory + (children > 0 ? ` (${children})` : ""));

                                                        //setTimeout(function () {
                                                        //    location.href = "/HTKT/Activity?id=" + tableConfig.GLOBAL.idDirectory;
                                                        //}, 500)
                                                    }
                                                    else {
                                                        console.log(res);
                                                    }
                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
                                                    console.error(messageErorr);
                                                }
                                            });

                                        } else {
                                            abp.notify.error(l("Activity:UpdateActivityFailed"));
                                        }
                                        abp.ui.clearBusy();
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        //let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
                                        console.error(jqXHR + ":" + errorThrown);
                                        abp.ui.clearBusy();
                                    }
                                });
                            }, 1000)

                        } else {
                            swal({
                                title: l("Layer:Notification"),
                                text: l("Layer:PropertyDataCannotEmpty"),
                                icon: "error",
                                button: l("Layer:Close"),
                            });
                        }
                    }
                }
            }
        });

        $(document).on('click', '.sidebar-toggle', function () {
            setTimeout(function () {
                TreeView.initSplitter();
            }, 300);
        });

        $('#div2').on('keyup', 'input[type=text]', function () {

            if ($(this).val() != "") {
                tableConfig.clearLabelError($(this).parent());
            }
        });

        $('.map-view').on('change', 'select', function () {

            if ($(this).val() != "") {
                tableConfig.clearLabelError($(this).parent());
            }
            else {
                if (TVisAdmin.toLowerCase() != "true") // nếu ko phải là admin, thì kiểm tra old value layer
                {
                    if (tableConfig.GLOBAL.oldLayerFocus.length == 0) // nếu không có thì trả về lỗi
                    {
                        TreeView.showLabelError(this, l("Layer:DataCannotEmptyAndValid"));
                    }
                }
                else {
                    TreeView.showLabelError(this, l("Layer:DataCannotEmptyAndValid"));
                }
            }
        })

        $('.div-headingTwo').on('click', '.checkboxmark', function () {
            if (!$(this).hasClass('noteditcheckbox')) {
                if (!$(this).hasClass('check')) {
                    $(this).addClass('check');
                    $(this).parent().find("input").trigger('click');
                } else {
                    $(this).removeClass('check');
                    $(this).parent().find("input").trigger('click');
                }
            }
        });

    },
    clearText: function () {
        $(tableConfig.SELECTORS.eidiinput).show();
        $(tableConfig.SELECTORS.textinsert).remove();
        $(tableConfig.SELECTORS.editselect).remove();
    },
    //get or set data list table
    getOrSetData: function (row, cell, check, val) {
        if (check) {//set
            tableConfig.GLOBAL.table[row][cell] = val;
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        } else {//get
            return tableConfig.GLOBAL.table[row][cell];
        }
    },
    //sort list with order
    sortListOrder: function () {
        tableConfig.GLOBAL.table.sort(function (a, b) {
            return parseInt(a.OrderProperties) - parseInt(b.OrderProperties);
        });
    },
    //event reset sau khi add data table
    setEventAffter: function () {
        $(tableConfig.SELECTORS.tabletrtd).on("click", 'div.editinput', function () {
            if (!$(this).parent().parent().hasClass("notedit") && !$(this).parent().hasClass("unselectable")) {
                tableConfig.clearText();
                $(this).hide();
                let code = $(this).parent().attr("data-code");
                let row = Number($(this).parents("tr").attr("data-count"));
                let val = tableConfig.getOrSetData(row, code, false, "");
                switch (code) {
                    case "TypeProperties":
                        let html = "<select class='form-control editselect'>";
                        $.each(tableConfig.GLOBAL.select, function (i, obj) {
                            html += "<option value=" + obj.values + " " + (obj.values == val ? "selected" : "") + ">" + obj.text + "</option>";
                        });
                        html += "</select>";
                        $(this).parent().append(html);
                        break;
                    case "CodeProperties":
                        $(this).parent().append('<input type="text" class="form-control textinsert inputCodeProperties" value="' + val + '">');
                        break;
                    case "NameProperties":
                        $(this).parent().append('<input type="text" class="form-control textinsert inputNameProperties" value="' + val + '">');
                        break;
                    default:
                        $(this).parent().append('<input type="text" class="form-control textinsert inputProperties" value="' + val + '">');
                        break;
                }
            }
        });
        $(tableConfig.SELECTORS.tabletrtd).on("focusout", ".textinsert", function () {

            var data = $(this).val();
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));//$(this).parent().parent().index();
            if (code == "CodeProperties") {
                let array = JSON.parse(JSON.stringify(tableConfig.GLOBAL.table));
                let check = array.find(x => x.CodeProperties.toLocaleLowerCase() != "" && x.CodeProperties.toLocaleLowerCase() == data.toLocaleLowerCase() && x.OrderProperties != row + 1);
                if (check != null && check != undefined && data.trim() != "" && data.trim() != null) {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("Layer:AttributeCodeExists"),
                        icon: "error",
                        button: l("Layer:Close"),
                    });
                    $(this).val('');
                    return;
                }
            }
            tableConfig.getOrSetData(row, code, true, data);

            var NamePropertyInputEmpty = jQuery("td[data-code='NameProperties'] .editinput").filter(function () {
                return $(this).text().trim() === "";
            });

            var CodePropertiesInputEmpty = jQuery("td[data-code='CodeProperties'] .editinput").filter(function () {
                return $(this).text().trim() === "";
            });

            //if (NamePropertyInputEmpty.length === 0 && CodePropertiesInputEmpty.length === 0) {
            //    $(tableConfig.SELECTORS.btnSave).removeAttr("disabled");
            //}
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".editcheckbox", function () {
            let data = $(this).is(":checked");
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));
            tableConfig.getOrSetData(row, code, true, data);
        });
        $(tableConfig.SELECTORS.tabletrtd).on("change", ".editselect", function () {
            let data = $(this).find(":selected").val();
            let dataText = $(this).find(":selected").text();
            let selectInAvail = false;


            $("td[data-code='TypeProperties'] .editinput").each(function (i, e) {
                if ($(e).text().trim() == dataText && (data == "image" || data == "geojson" || data == "link" || data == "maptile") && selectInAvail == false) {
                    swal({
                        title: l("Layer:Notification"),
                        text: l("DataTypeExist{0}", dataText),
                        icon: "warning",
                        button: l("Layer:Close")
                    });
                    $(".editselect").val("text");
                    selectInAvail = true;
                }
            });

            if (!selectInAvail) {
                let code = $(this).parent().attr("data-code");
                let row = Number($(this).parents("tr").attr("data-count"));
                tableConfig.getOrSetData(row, code, true, data);
                let td = $('tr[data-count="' + row + '"]')[0].querySelectorAll("td");
                let tdaction = td[td.length - 1];
                if ($(this).val() == "list" || $(this).val() == "checkbox" || $(this).val() == "radiobutton") {
                    tableConfig.GLOBAL.listTypeList = [];
                    tableConfig.GLOBAL.rowSelected = tableConfig.GLOBAL.table[row];
                    $(tableConfig.SELECTORS.modalList).modal('show');
                    $(td[td.length - 3]).addClass("unselectable");
                    //$(tdaction).append(`<button type="button" class="btn btn-info showList" style="padding:3px 5px;font-size:13px; margin-left: 2px;"> <i class="fa fa fa-list" aria-hidden="true"></i></button>`);
                } else {
                    if (tableConfig.GLOBAL.previousSelect == "list" || tableConfig.GLOBAL.previousSelect == "checkbox" || tableConfig.GLOBAL.previousSelect == "radiobutton") {
                        tableConfig.getOrSetData(row, "DefalutValue", true, "");
                        $(tdaction).children(".showList").remove();
                        $(td[td.length - 3]).removeClass("unselectable");
                    }
                }
            }
        });
        $(tableConfig.SELECTORS.tabletrtd).on("focus", ".editselect", function () {
            tableConfig.GLOBAL.previousSelect = '';
            tableConfig.GLOBAL.previousSelect = this.value;
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".deleteRow", function () {

            let row = Number($(this).parents("tr").attr("data-count"));
            if (tableConfig.GLOBAL.table[row] != undefined) {

                var message = "";

                if (!tableConfig.CheckDeleteProperties(tableConfig.GLOBAL.table[row].CodeProperties)) {
                    message = l("Layer:WarningPropertiesIsUse")
                }

                var text = document.createElement("div");
                var htmlMessage = `<span style="display:block;">${l("Layer:AreYouDeleteThisProperty")}</span> <span style="color:red; font-style: italic; font-size:12px;">${message}</span>`;
                text.innerHTML = htmlMessage;

                swal({
                    title: l("Layer:Notification"),
                    html: true,
                    content: text,
                    icon: "warning",
                    buttons: [
                        l("Layer:Close"),
                        l("Layer:Agree")
                    ],
                }).then((value) => {
                    if (value) {
                        tableConfig.GLOBAL.table.splice(row, 1);

                        tableConfig.setOrder();

                        tableConfig.addTbodyTr();
                        tableConfig.setEventAffter();
                    }
                });
            }
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".downRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            var objfind = tableConfig.GLOBAL.table[row];
            var objfindNext = tableConfig.GLOBAL.table[row + 1];
            if (objfind.OrderProperties < tableConfig.GLOBAL.table.length && typeof objfindNext !== "undefined"
                && objfindNext !== null && objfindNext.TypeSystem == 3) {
                var orderCurrent = objfind.OrderProperties;
                tableConfig.GLOBAL.table[row].OrderProperties = objfindNext.OrderProperties;
                tableConfig.GLOBAL.table[row + 1].OrderProperties = orderCurrent;
            }
            tableConfig.sortListOrder();
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".upRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            var objfind = tableConfig.GLOBAL.table[row];
            var objfindBack = tableConfig.GLOBAL.table[row - 1];
            if (objfind.OrderProperties > 0 && objfind.OrderProperties <= tableConfig.GLOBAL.table.length
                && typeof objfindBack !== "undefined" && objfindBack !== null && objfindBack.TypeSystem == 3) {
                var orderCurrent = objfind.OrderProperties;
                tableConfig.GLOBAL.table[row].OrderProperties = objfindBack.OrderProperties;
                tableConfig.GLOBAL.table[row - 1].OrderProperties = orderCurrent;
            }
            tableConfig.sortListOrder();
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        });

        //$(tableConfig.SELECTORS.tabletrtd).on("keyup", ".inputCodeProperties", function () {
        //    // Our regex
        //    // a-z => allow all lowercase alphabets
        //    // A-Z => allow all uppercase alphabets
        //    // 0-9 => allow all numbers
        //    // @ => allow @ symbol
        //    var regex = /^[a-zA-Z0-9_]+$/;
        //    // This is will test the value against the regex
        //    // Will return True if regex satisfied
        //    if (regex.test(this.value) !== true)
        //        //alert if not true
        //        //alert("Invalid Input");

        //        // You can replace the invalid characters by:
        //        this.value = this.value.replace(/[^a-zA-Z0-9_]+/, '');
        //});


        $(tableConfig.SELECTORS.tabletrtd).on("change keyup", ".inputCodeProperties", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }
            this.value = value;
        });
        $(tableConfig.SELECTORS.tabletrtd).on("change keyup", ".inputNameProperties", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }

            let row = Number($(this).parents("tr").attr("data-count"));
            let array = JSON.parse(JSON.stringify(tableConfig.GLOBAL.table));
            let check = array.find(x => x.CodeProperties.toLocaleLowerCase() != "" && x.CodeProperties.toLocaleLowerCase() == value.toLocaleLowerCase() && x.OrderProperties != row + 1);
            if (check != null && check != undefined) {
                value += "1";
            }
            tableConfig.GLOBAL.table[row]["CodeProperties"] = value;
            $(this).parents("tr").find("td[data-code='CodeProperties'] .editinput").text(value);
        });
    },
    //hiển thị các nút ở hoạt động
    showButtonRow: function (obj, count, length) {
        let html = "";
        if (obj.TypeSystem == 2 || obj.TypeSystem == 1) {
            html += '<td style="width:86px"></td>';
        } else {
            html += '<td style="width:110px;">';
            html += `<div class="d-flex w-100" style="place-content: flex-end;">`
            if (obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") {
                html += `<button type="button" class="btn btn-info showList" style="padding:3px 3px;font-size:13px;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 14 14"><defs><style>.a{fill:none;}.b{fill:#fff;}</style></defs><path class="a" d="M0,0H14V14H0Z"/><path class="b" d="M2,12.167H7.833v1.167H2ZM2,8.083H13.667V9.25H2ZM2,4H13.667V5.167H2Zm9.333,8.167v-1.75H12.5v1.75h1.75v1.167H12.5v1.75H11.333v-1.75H9.583V12.167Z" transform="translate(-0.833 -2.667)"/></svg>
                        </button>`;
            }

            html += '<button type = "button" class="btn btn-info downRow" style = "padding:3px 3px;font-size:13px; margin-left: 2px;" > <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 18 18"><defs><style>.a{fill:none;}.b{fill:#fff;}</style></defs><g transform="translate(18 18) rotate(180)"><path class="a" d="M0,0H18V18H0Z"/><path class="b" d="M9.614,6.392V14H8.386V6.392l-3.3,3.352-.869-.884L9,4l4.778,4.861-.869.884Z"/></g></svg></button >';
            html += '<button type = "button" class="btn btn-info upRow" style = "padding:3px 3px;font-size:13px; margin-left: 2px;" > <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 18 18"><defs><style>.a{fill:none;}.b{fill:#fff;}</style></defs><path class="a" d="M0,0H18V18H0Z"/><path class="b" d="M9.614,6.392V14H8.386V6.392l-3.3,3.352-.869-.884L9,4l4.778,4.861-.869.884Z"/></svg></button >';
            html += '<button type = "button" class="btn btn-danger deleteRow" style = "padding:3px 3px;font-size:13px;margin-left: 2px;" > <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 18 18"><defs><style>.a{fill:none;}.b{fill:#fff;}</style></defs><path class="a" d="M0,0H18V18H0Z"/><path class="b" d="M3.4,6.2H14.6v9.1a.7.7,0,0,1-.7.7H4.1a.7.7,0,0,1-.7-.7ZM5.5,4.1V2.7A.7.7,0,0,1,6.2,2h5.6a.7.7,0,0,1,.7.7V4.1H16V5.5H2V4.1Zm1.4-.7v.7h4.2V3.4ZM6.9,9v4.2H8.3V9ZM9.7,9v4.2h1.4V9Z"/></svg></button >';
            html += `</div>`
            html += '</td > ';
        }
        return html;
    },
    getDefault: function () {
        //$.ajax({
        //    type: "GET",
        //    contentType: "application/json-patch+json",
        //    async: false,
        //    url: tableConfig.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=Chưa có',
        //    data: {
        //    },
        //    success: function (data) {
        //        if (data.code == "ok" && data.result.listProperties != undefined) {
        //            var result = [];
        //            $.each(data.result.listProperties, function (i, obj) {
        //                var element = {
        //                    NameProperties: obj.nameProperties,
        //                    CodeProperties: obj.codeProperties,
        //                    TypeProperties: obj.typeProperties,
        //                    IsShow: obj.isShow,
        //                    IsIndexing: obj.isIndexing,
        //                    IsRequest: obj.isRequest,
        //                    IsView: obj.isView,
        //                    IsHistory: obj.isHistory,
        //                    IsInheritance: obj.isInheritance,
        //                    IsAsync: obj.isAsync,
        //                    DefalutValue: obj.defalutValue,
        //                    ShortDescription: obj.shortDescription,
        //                    TypeSystem: obj.typeSystem,
        //                    OrderProperties: obj.orderProperties
        //                };
        //                result.push(element);
        //            });
        //            tableConfig.GLOBAL.table = result;
        //        } else {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: tableConfig.CONSTS.URL_AJAXGETDEFAULTDATAFIRST,
            data: {
            },
            success: function (data) {
                if (data.code == "ok" && data.result != null) {
                    var result = [];
                    $.each(data.result, function (i, obj) {
                        var element = {
                            NameProperties: obj.nameProperties,
                            CodeProperties: obj.codeProperties,
                            TypeProperties: obj.typeProperties,
                            IsShow: obj.isShow,
                            IsIndexing: obj.isIndexing,
                            IsRequest: obj.isRequest,
                            IsView: obj.isView,
                            IsHistory: obj.isHistory,
                            IsInheritance: obj.isInheritance,
                            IsAsync: obj.isAsync,
                            IsShowExploit: obj.isShowExploit,
                            DefalutValue: obj.defalutValue,
                            ShortDescription: obj.shortDescription,
                            TypeSystem: obj.typeSystem,
                            OrderProperties: obj.orderProperties
                        };
                        result.push(element);
                    });
                    tableConfig.GLOBAL.table = result;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
                ViewMap.showLoading(false);
            }
        });
        //        }
        //    },
        //    error: function (jqXHR, textStatus, errorThrown) {
        //        let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
        //        console.log(messageErorr);
        //        ViewMap.showLoading(false);
        //    }
        //});
    },
    resetTable: function () {
        tableConfig.GLOBAL.idDirectory = '';
        tableConfig.GLOBAL.table = [];
        tableConfig.addTbodyTr();
        $('#DataName').val("");
        $('#h5NameData').html("");
        $('#DataCode').val("");
        $('#Version').val(0);
        $(tableConfig.SELECTORS.select_lop_du_lieu).val("").trigger("change.select2");
    },
    setUpEventModalList: function () {
        $('#codeList').on("keyup", function () {
            // Our regex
            // a-z => allow all lowercase alphabets
            // A-Z => allow all uppercase alphabets
            // 0-9 => allow all numbers
            // @ => allow @ symbol
            var regex = /^[a-zA-Z0-9_]+$/;
            // This is will test the value against the regex
            // Will return True if regex satisfied
            if (regex.test(this.value) !== true)
                //alert if not true
                //alert("Invalid Input");

                // You can replace the invalid characters by:
                this.value = this.value.replace(/[^a-zA-Z0-9_]+/, '');
        });

        $('#nameList').on("keyup", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            ""
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                this.value = this.value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }

            $('#codeList').val(value);

        });

        $('#btnSaveList').on('click', function () {
            if ($('#codeList').val().trim() !== "" && $('#nameList').val().trim() !== "") {
                let check = tableConfig.GLOBAL.listTypeList.find(x => x.code === $('#codeList').val().trim()); // kiểm tra trùng code
                if (check != null & check != undefined) // nếu trùng
                {
                    if ($('#idList').val().trim() === "") // check trạng thái đang là edit hay create
                    {
                        // nếu là trạng thái create thì thông báo trùng code
                        swal({
                            title: l("Layer:Notification"),
                            text: l("Layer:AttributeCodeExists"),
                            icon: "error",
                            button: l("Layer:Close"),
                        });
                    }
                    else {
                        if ($('#idList').val().trim() != check.id) // nếu là edit thì check id
                        {
                            // không trùng id thì thông báo trùng code
                            swal({
                                title: l("Layer:Notification"),
                                text: l("Layer:AttributeCodeExists"),
                                icon: "error",
                                button: l("Layer:Close"),
                            });
                        }
                        else {
                            // không trùng id thì update list
                            let object = tableConfig.GLOBAL.listTypeList.find(x => x.id === $('#idList').val().trim());
                            object.name = $('#nameList').val().trim();
                            object.code = $('#codeList').val().trim();
                            swal({
                                title: l("Layer:Notification"),
                                text: l("Layer:PropertyChangeSusscessfully"),
                                icon: "success",
                                button: l("Layer:Close"),
                            });
                        }
                    }
                }
                else // nếu k trùng code
                {
                    if ($('#idList').val().trim() === "") // nếu đang là trạng thái create
                    {
                        // thêm mới 1 item vào list
                        let id = tableConfig.uuidv4();
                        let object = {
                            id: id,
                            name: $('#nameList').val().trim(),
                            code: $('#codeList').val().trim(),
                            checked: false,
                            action: tableConfig.getActionList(id)
                        };

                        tableConfig.GLOBAL.listTypeList.push(object);
                    }
                    else // nếu là trạng thái edit thì update list
                    {
                        let object = tableConfig.GLOBAL.listTypeList.find(x => x.id === $('#idList').val().trim());
                        object.name = $('#nameList').val().trim();
                        object.code = $('#codeList').val().trim();
                        swal({
                            title: l("Layer:Notification"),
                            text: l("Layer:PropertyChangeSusscessfully"),
                            icon: "success",
                            button: l("Layer:Close"),
                        });
                    }
                }

                $('#codeList').val('');
                $('#nameList').val('');
                $('#idList').val('');
                tableConfig.updateTable();

                //if (check != null & check != undefined) {
                //    swal({
                //        title: l("Layer:Notification"),
                //        text: l("Layer:AttributeCodeExists"),
                //        icon: "error",
                //        button: l("Layer:Close"),
                //    });
                //} else {
                //    if ($('#idList').val().trim() === "") {
                //        let id = tableConfig.uuidv4();
                //        let object = {
                //            id: id,
                //            name: $('#nameList').val().trim(),
                //            code: $('#codeList').val().trim(),
                //            checked: false,
                //            action: tableConfig.getActionList(id)
                //        };

                //        tableConfig.GLOBAL.listTypeList.push(object);
                //    } else {
                //        let object = tableConfig.GLOBAL.listTypeList.find(x => x.id === $('#idList').val().trim());
                //        object.name = $('#nameList').val().trim();
                //        object.code = $('#codeList').val().trim();
                //    }

                //    $('#codeList').val('');
                //    $('#nameList').val('');
                //    $('#idList').val('');
                //    tableConfig.updateTable();
                //}
            }
        });

        $(document).on('click', '.btn-deleteTable-list', function () {
            swal({
                title: l("Layer:Notification"),
                text: l("Layer:AreYouDeleteThisProperty"),
                icon: "warning",
                buttons: [
                    'Đóng',
                    'Đồng ý'
                ],
            }).then((value) => {
                if (value) {
                    let id = $(this).attr('delete-for');
                    tableConfig.GLOBAL.listTypeList = JSON.parse(JSON.stringify(tableConfig.GLOBAL.listTypeList.filter(x => x.id !== id)));
                    tableConfig.updateTable();
                }
            });
        });

        $(document).on('click', '.btn-editTable-list', function () {
            let id = $(this).attr('edit-for');
            let object = tableConfig.GLOBAL.listTypeList.find(x => x.id === id);
            $('#codeList').val(object.code);
            $('#nameList').val(object.name);
            $('#idList').val(object.id);
        });

        $(document).on('click', '.showList', function () {
            $('#idList').val('');
            $('#nameList').val('');
            $('#codeList').val('');
            let row = Number($(this).parents("tr").attr("data-count"));
            tableConfig.GLOBAL.rowSelected = tableConfig.GLOBAL.table[row];
            $(tableConfig.SELECTORS.modalList).modal('show');
        });

        $(tableConfig.SELECTORS.modalList).on('show.bs.modal', function () {
            $('#codeList').val('');
            $('#nameList').val('');
            $('#idList').val('');
            if (tableConfig.GLOBAL.rowSelected != null && tableConfig.GLOBAL.rowSelected.DefalutValue != "" && tableConfig.GLOBAL.rowSelected.DefalutValue != null) {
                try {
                    tableConfig.GLOBAL.listTypeList = JSON.parse(tableConfig.GLOBAL.rowSelected.DefalutValue);
                }
                catch {

                }
            }
            tableConfig.updateTable();
        });

        $(tableConfig.SELECTORS.modalList).on('hidden.bs.modal', function () {
            tableConfig.GLOBAL.rowSelected.DefalutValue = JSON.stringify(tableConfig.GLOBAL.listTypeList);
            tableConfig.GLOBAL.listTypeList = [];
        });
    },
    updateTable: function () {
        $('.div-list table tbody').html('');
        var test = '';
        if (tableConfig.GLOBAL.listTypeList.length > 0) {
            var count = 0;
            for (var i = 0; i < tableConfig.GLOBAL.listTypeList.length; i++) {
                //if (tableConfig.GLOBAL.listTypeList[i].status) {
                count++;
                test += `<tr>
                                                                            <th scope="row">${count}</th>
                                                                            <td>${tableConfig.GLOBAL.listTypeList[i].name}</td>
                                                                            <td>${tableConfig.GLOBAL.listTypeList[i].code}</td>
                                                                            <td style="display:flex;">${tableConfig.GLOBAL.listTypeList[i].action}</td>
                                                                        </tr>`;
                //}
            }
        }
        if (test != "") {
            $('.div-list table tbody').append(test);
        } else {
            $('.div-list table tbody').append(`<tr>
                                                                                    <th scope="row" colspan="4">Không có dữ liệu</th>
                                                                                </tr>`);
        }

        //text += `</tr>`;
        //$('.div-list table thead').html('');
        //$('.div-list table thead').append(text);
    },
    //new Guid Id
    uuidv4: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    getActionList: function (id) {
        return `<a href="#" title="Xóa" class="delete btn btn-danger btn-xs btn-deleteTable-list" delete-for="${id}"><i class="fa fa-trash-o"></i></a>
                        <a href="#" class= "btn btn-primary btn-xs btn-editTable-list" edit-for="${id}" title = "Sửa" > <i class="fa fa-edit"></i></a>`;
    },

    // danh sach lop du lieu
    SelectedLayer: function () {
        $.ajax({
            url: tableConfig.CONSTS.URL_AJAXGETLISTDIRECTORY,
            method: "GET",
            async: false,
            success: function (res)
            {
                if (res.code == "ok")
                {
                    var result = res.result;
                    $(tableConfig.SELECTORS.select_lop_du_lieu).html('');

                    var $option = "<option value = ''>-- Chọn lớp dữ liệu --</option>";
                    $option = "";
                    $.each(result, function (index, item) {
                        var disabled = true;

                        if (TVisAdmin.toLowerCase() != 'true') {
                            if (jQuery.inArray(item.id, TVlstIdLayer) == -1) {
                                disabled = false;
                            }
                        }

                        if (item.type == "file") {
                            $option += `<option value="${item.id}" ${disabled == true ? "" : "disabled locked='locked'"}>${item.name}</option>`;

                            // $option += '<option value="' + item.id + '">' + item.name + '</option>';
                        }
                    });

                    $(tableConfig.SELECTORS.select_lop_du_lieu).html($option);

                    $(TreeView.SELECTORS.select_lop_du_lieu_create_layer).html($option);

                    tableConfig.GLOBAL.ArrayLayer = result;
                }
            }
        });

        //---------------------- select 2  table layer --------------------------------------------------
        $(tableConfig.SELECTORS.select_lop_du_lieu).select2({
            //language: Host + "/libs/select2/js/i18n/vi.js",
            allowClear: true,
            width: '100%',
            templateSelection: formatState,
            matcher: custom_search_select2
        }).on("select2:opening", function (event) {
            $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
        }).on("select2:closing", function (event) {

            if (TVisAdmin.toLowerCase() != "true") // nếu ko phải là admin, thì kiểm tra old value layer
            {
                if (tableConfig.GLOBAL.oldLayerFocus.length == 0) // nếu không có thì trả về lỗi
                {
                    if ($(this).val() == "") {
                        $('#div2 .select2-selection').css('border', '');
                        $(this).parent().find('.placeholder').attr('style', "");
                    }
                }
            }
            else {
                if ($(this).val() == "") {
                    $('#div2 .select2-selection').css('border', '');
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            }

        }).on('select2:unselecting', function (e) {
            // before removing tag we check option element of tag and 
            // if it has property 'locked' we will create error to prevent all select2 functionality
            if ($(e.params.args.data.element).attr('locked')) {
                e.select2.pleaseStop();
            }
        });
        ///--------------------------------end select 2 table layer ---------------------------------------------


        //---------------- select2 create modal layer -------------------------------------------
        $(TreeView.SELECTORS.select_lop_du_lieu_create_layer).select2({
            //language: Host + "/libs/select2/js/i18n/vi.js",
            allowClear: false,
            width: '100%',
            templateSelection: formatState,
            matcher: custom_search_select2
        }).on("select2:opening", function (event) {
            $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
        }).on("select2:closing", function (event) {
            if ($(this).val() == "") {
                $(TreeView.SELECTORS.modalLayer + '.select2-selection').css('border', '');
                $(this).parent().find('.placeholder').attr('style', "");
            }

        }).on('select2:unselecting', function (e) {
            // before removing tag we check option element of tag and 
            // if it has property 'locked' we will create error to prevent all select2 functionality
            if ($(e.params.args.data.element).attr('locked')) {
                e.select2.pleaseStop();
            }
        });;
        //---------------- end select2 create modal layer -----------------------------------
    },
    checkFormValidate: function () {
        TreeView.clearLabelError();
        let check = true;
        let inputName = $(tableConfig.SELECTORS.dataName).val();
        if (!validateText(inputName, "text", 0, 0)) {
            //insertError($(tableConfig.SELECTORS.dataName), "other");
            check = false;
            TreeView.showLabelError(tableConfig.SELECTORS.dataName, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:ActivityTypeName")));
        }

        let code = $(tableConfig.SELECTORS.dataCode).val();
        if (!validateText(code, "text", 0, 0)) {
            //insertError($(tableConfig.SELECTORS.dataCode), "other");
            check = false;
            TreeView.showLabelError(tableConfig.SELECTORS.dataCode, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:AcvitityTypeCode")));
        }

        if ($(tableConfig.SELECTORS.select_lop_du_lieu).val() == "") {
            var checkOldLayer = true;
            if (TVisAdmin.toLowerCase() != "true") // nếu ko phải là admin, thì kiểm tra old value layer
            {
                if (tableConfig.GLOBAL.oldLayerFocus.length == 0) // nếu không có thì trả về lỗi
                {
                    checkOldLayer = false;
                }
            }
            else {
                checkOldLayer = false;
            }

            if (!checkOldLayer) {
                TreeView.showLabelError(tableConfig.SELECTORS.select_lop_du_lieu, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:DataLayer")));
                check = false;
            }
        }

        if (check) {
            check = tableConfig.CheckedHtmlEntities('#vp-search-map');
        }

        let checkHtml = CheckedHtmlEntities(tableConfig.SELECTORS.form_input_properties_basic);
        if (check) {
            check = checkHtml;
        }
        return check;
    },
    CheckedHtmlEntities: function (form) {
        var check = true;
        let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;

        $(`${form} input[type=text]`).each(function () {
            var value = $(this).val();
            var status = $(this).attr("data-required");
            if (status != undefined) {
                status = status.toLowerCase()
            }

            if ($(this).val() != "" && status != "true") {
                if (re.test(value)) {
                    insertError($(this), "other");
                    TreeView.showLabelError(this, "Dữ liệu nhập vào không hợp lệ");
                    check = false;
                }
            }
        });

        if (check) // pass input thông tin thì check input thuộc tính
        {
            let checkTable = true;
            $.each(tableConfig.GLOBAL.table, function (i, obj) {
                if (re.test(obj.NameProperties) || re.test(obj.CodeProperties)) {
                    checkTable = false;
                }
            });

            if (!checkTable) {
                check = checkTable;
                swal({
                    title: "Thông báo",
                    text: "Dữ liệu thuộc tính Không hợp lệ",
                    icon: "error",
                    button: "Đóng",
                });
            }
        }

        return check;
    },
    clearLabelError: function (element) {
        $(element).find(".lable-error").remove();
        $(element).removeClass("has-error");
    },
    setOrder: function () {
        tableConfig.GLOBAL.table.sortOn("OrderProperties");

        for (var i = 0; i < tableConfig.GLOBAL.table.length; i++) {
            tableConfig.GLOBAL.table[i].OrderProperties = i + 1;
        }
    },
    reSetData: function () {
        tableConfig.GLOBAL.table = [];
        $(tableConfig.SELECTORS.tbody + " tr").each(function (indexTr, obj) {
            var order = indexTr;
            var system = $(this).attr('data-system')
            var data = {
                NameProperties: "",
                CodeProperties: "",
                TypeProperties: "string",
                IsShow: true,
                IsIndexing: false,
                IsRequest: false,
                IsView: false,
                IsHistory: false,
                IsInheritance: false,
                IsAsync: false,
                IsShowExploit: false,
                DefalutValue: "",
                ShortDescription: "",
                TypeSystem: isNaN(parseInt(system)) ? 3 : parseInt(system),
                OrderProperties: order + 1,
            }

            var lstTd = $(this).find('td');

            $(lstTd).each(function () {
                var code = $(this).attr("data-code");
                switch (code) {
                    case "NameProperties":
                        data.NameProperties = $(this).find(">:first-child").text();
                        break;
                    case "CodeProperties":
                        data.CodeProperties = $(this).find('>:first-child').text();
                        break;
                    case "TypeProperties":
                        var type = tableConfig.GLOBAL.select.find(x => x.text === $(this).find('>:first-child').text());
                        if (type != undefined) {
                            data.TypeProperties = type.values;
                        }
                        break;
                    case "IsShow":
                        data.IsShow = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "IsIndexing":
                        data.IsIndexing = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "IsRequest":
                        data.IsRequest = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "IsView":
                        data.IsView = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "IsHistory":
                        data.IsHistory = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "IsInheritance":
                        data.IsInheritance = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "IsAsync":
                        data.IsAsync = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "IsShowExploit":
                        data.IsShowExploit = $(this).find('.editcheckbox').is(':checked');
                        break;
                    case "DefalutValue":
                        break;
                    case "ShortDescription":
                        break;
                    default:
                }
            })

            tableConfig.GLOBAL.table.push(data);
        })
    },
    CheckDeleteProperties: function (code) {
        var result = true;
        $.ajax({
            type: "GET",
            url: tableConfig.CONSTS.URL_CHECK_DELETE_PROPERTIES,
            data: {
                code: code,
                idDirectoryAcitivty: tableConfig.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json",
            success: function (data) {
                if (data.code == "ok") {
                    result = data.result;
                }
            },
        });

        return result;
    }
    ////get data table
    //getDatatable
}

Array.prototype.sortOn = function (key) {
    this.sort(function (a, b) {
        if (a[key] < b[key]) {
            return -1;
        } else if (a[key] > b[key]) {
            return 1;
        }
        return 0;
    });
}

function custom_search_select2(params, data) {
    let search = "", text = "";
    if (typeof params.term !== "undefined") {
        search = params.term.trim().replace(/ /g, ""); // xóa khoảng trắng
        search = search.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
        search = removeVietnameseTones(search).toUpperCase(); // xóa dấu
    }
    if ($.trim(search) === '') {
        return data;
    }
    if (typeof data.text === 'undefined') {
        return null;
    } else {
        text = data.text.trim().replace(/ /g, ""); // xóa khoảng trắng
        text = text.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
        text = removeVietnameseTones(text).toUpperCase(); // xóa dấu
    }
    if (text.toLowerCase().indexOf(search.toLowerCase()) > -1) {
        var modifiedData = $.extend({}, data, true);
        //modifiedData.text += ' (matched)';
        return modifiedData;
    }
    return null;
}

function formatState(tag, container) {
    var $option = $(tableConfig.SELECTORS.select_lop_du_lieu + ' option[value="' + tag.id + '"]');
    if ($option.attr('locked')) {
        $(container).addClass('locked-tag');
        tag.locked = true;
    }
    return tag.text;
};