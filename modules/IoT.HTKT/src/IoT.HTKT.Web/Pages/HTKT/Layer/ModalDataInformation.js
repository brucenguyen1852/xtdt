﻿var l = abp.localization.getResource('HTKT');
var datainfo = {
    GLOBAL: {
        statusShow: false
    },
    CONSTS: {
        URL_SEND_IMAGE2D: "/api/htkt/file/image-icon-app",
        URL_DELETE_IMAGE2D: "/api/htkt/file/deleteimage-icon-app"
    },
    SELECTORS: {
        img_avatar: '#img-avatar',
        fileImage: '#fileImage',
        edit_avatar: '.edit-avatar',
        inputObj: "#choosenFile-obj",
        img_3D: '#img-3D',
        file3D: '#file3D',
        edit_3D: '.edit-3D',
        btnSave: '.btn-save-properties-info',
        nameObject: '.nameObject',
        inforSetting: "#pills-body-info",
    },
    init: function () {
        datainfo.setEvent();

    },
    setEvent: function () {
        $('.li-modal-data-right').on('click', function () {
            if (config.GLOBAL.idDirectory != "" && datainfo.GLOBAL.statusShow) {
                let step = $(this).attr("data-li");
                $('.li-modal-data-right').removeClass("active");
                $(this).addClass("active");
                $('.section-info').css('display', 'none');
                switch (step) {
                    case "step-info":
                        $('.step-info').css('display', 'block');
                        break;
                    case "step-config":
                        $('.step-config').css('display', 'block');
                        break;
                }
            }
        });

        // texture 
        $(datainfo.SELECTORS.file3D).change(function () {
            $(this).parents().closest('.row').find('.lable-error').remove();
            var files = $(this)[0].files;
            if (files != null && files.length > 0) {
                // check size file
                if (files[0].size <= sizeFiles.sizeImgNormal) {
                    var exp = files[0].name.substring(files[0].name.lastIndexOf(".") + 1).toLowerCase();
                    if (exp == "png" || exp == "jpg" || exp == "jpeg") {
                        readURL(this, datainfo.SELECTORS.img_3D);
                    } else {
                        $(this).parents().closest('.row').append(`<label class="col-md-12 lable-error text-danger mt-2">${jQuery.validator.format(l("Layer:ValidFormat"), l('Layer:3DTextureImage'))}</label>`);
                        $(datainfo.SELECTORS.img_3D).attr('src', "/images/anhMacDinh.png");
                        $(this).val(null);
                    }
                } else {
                    $(this).parents().closest('.row').append(`<label class="col-md-12 lable-error text-danger mt-2">${l('ManagementLayer:ValidSizeFileImgNormal')}</label>`);
                    $(this).val(null);
                    $(datainfo.SELECTORS.img_3D).attr('src', "/images/anhMacDinh.png");
                }
            }
        });

        // icon
        $(datainfo.SELECTORS.fileImage).change(function () {
            $(this).parents().closest('.row').find('.lable-error').remove();
            var files = $(this)[0].files;
            if (files != null && files.length > 0) {
                // check size file
                if (files[0].size <= sizeFiles.sizeIcon) {
                    var exp = files[0].name.substring(files[0].name.lastIndexOf(".") + 1).toLowerCase();
                    if (exp == "png" || exp == "jpg" || exp == "jpeg") {
                        readURL(this, datainfo.SELECTORS.img_avatar);

                        $(this).parents().closest('.row').find('.lable-error').remove();
                    }
                    else {
                        $(this).parents().closest('.row').append(`<label class="col-md-12 lable-error text-danger mt-2">${jQuery.validator.format(l("Layer:ValidFormat"), l('Layer:ImageIcon'))}</label>`);
                        $(this).val(null);
                        $(datainfo.SELECTORS.img_avatar).attr('src', "/images/anhMacDinh.png");
                    }
                }
                else {
                    $(this).parents().closest('.row').append(`<label class="col-md-12 lable-error text-danger mt-2">${l("ManagementLayer:ValidSizeFileIcon")}</label>`);
                    $(this).val(null);
                    $(datainfo.SELECTORS.img_avatar).attr('src', "/images/anhMacDinh.png");
                }
            }
        });

        $(datainfo.SELECTORS.edit_avatar).click(() => {
            $(datainfo.SELECTORS.fileImage).trigger("click");
        });

        $(datainfo.SELECTORS.edit_3D).click(() => {
            $(datainfo.SELECTORS.file3D).trigger("click");
        });

        $(datainfo.SELECTORS.btnSave).on('click', function () {
            toastr.remove();
            //if ($(datainfo.SELECTORS.file3D)[0].files.length > 0)
            {
                TreeView.clearLabelError();
                if (config.GLOBAL.idDirectory != '') {
                    abp.ui.setBusy(TreeView.SELECTORS.modal_setting_layer);

                    setTimeout(function () {
                        TreeView.UpdateProperty("info");
                    }, 500)
                } else {
                    swal({
                        title: l("Layer:Notification"),
                        text: l("Layer:PleaseSelectLayer"),
                        icon: "error",
                        button: l("Layer:Close"),
                    });
                }
            } 
            //else
            //    abp.notify.warn(l("ManagementLayer:FileExitsFormat"));
        });

        function readURL(input, img) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(img).attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    },
    SendFileImage: function (file) {
        //let url = '';
        let url = null;
        if (file != undefined && file != null) {
            var formData = new FormData();
            formData.append('file', file);
            $.ajax({
                //url: "/api/qtsc/file/image",
                url: datainfo.CONSTS.URL_SEND_IMAGE2D,
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                async: false,
                success: function (data) {
                    if (data.code == "ok" && data.result != null) {
                        url = data.result;
                    }
                },
                error: function (jqXHR) {
                },
                complete: function (jqXHR, status) {
                }
            });
        }
        return url;
    },
    SendFileObj: function (file) {
        let url = '';
        if (file != undefined && file != null) {
            var formData = new FormData();
            formData.append('file', file);
            $.ajax({
                url: "/api/htkt/file/object",
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                async: false,
                success: function (data) {
                    if (data.code == "ok" && data.result != null) {
                        url = data.result.url;
                    }
                },
                error: function (jqXHR) {
                },
                complete: function (jqXHR, status) {
                }
            });
        }
        return url;
    },
    DeleteFileImage: function (urlMap, urlIcon) {
        //var urlEnCode = encodeURIComponent(url);
        var obj = {
            urlIcon: encodeURIComponent(urlIcon),
            urlMap: encodeURIComponent(urlMap)
        }
        var check = false;
        $.ajax({
            url: datainfo.CONSTS.URL_DELETE_IMAGE2D,
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.code == "ok") {
                    check = data.result;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + ": " + errorThrown);
            }
        });
        return check;
    }
};