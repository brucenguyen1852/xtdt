﻿var optionLayer = {
    GLOBAL: {
    },
    CONSTS: {
        URL_POST_CREATE_LAYER: "/api/LayerToLayer/LayerGeo/CreateLayer",
    },
    SELECTORS: {
        radioOptionLayer: "input[name='optionLayer']",
        radioOptionLayerCheck: "input[name='optionLayer']:checked",
        inputUrlMaptile: ".tablelayer #UrlMaptile",
        inputBoundsMaptileLayer: "#inputBoundsMaptileLayer",
        radioOptionLayerCreate: "input[name='optionLayerCreate']",
        radioOptionLayerCreateCheck: "input[name='optionLayerCreate']:checked",
        optionLayerCheck: "input[name='optionLayer'][value='VAL']",
    },
    init: function () {
        this.setEvent();
    },

    setEvent: function () {
        //Check layer with option
        $(this.SELECTORS.radioOptionLayer).on("change", function () {

            let value = $(optionLayer.SELECTORS.radioOptionLayerCheck).val();
            switch (value) {
                case "2":
                    // option 2 (map tile geoserver auto,infor geojson)
                    $(optionLayer.SELECTORS.inputUrlMaptile).attr("readonly", "readonly");
                    $(optionLayer.SELECTORS.inputUrlMaptile).prop("disabled", true);
                    //$(optionLayer.SELECTORS.inputBoundsMaptileLayer).val("[]");
                    $(optionLayer.SELECTORS.inputUrlMaptile).parent().find("label").removeClass("required");

                    if ($(optionLayer.SELECTORS.inputUrlMaptile).val() != "") {
                        $(optionLayer.SELECTORS.inputUrlMaptile).parent().find('.placeholder').attr('style', 'transform: scale(0.8) translateY(-24px);background: #fff;');
                    }
                    else {
                        $(optionLayer.SELECTORS.inputUrlMaptile).parent().find('.placeholder').attr('style', '');
                    }
                    break;
                case "3":
                    // option 3 (map tile and infor geoserver other)
                    $(optionLayer.SELECTORS.inputUrlMaptile).removeAttr("readonly");
                    $(optionLayer.SELECTORS.inputUrlMaptile).prop("disabled", false);
                    $(optionLayer.SELECTORS.inputUrlMaptile).parent().find("label").addClass("required");

                    if ($(optionLayer.SELECTORS.inputUrlMaptile).val() != "") {
                        $(optionLayer.SELECTORS.inputUrlMaptile).parent().find('.placeholder').attr('style', 'transform: scale(0.8) translateY(-24px);background: #fff;');
                    }
                    else {
                        $(optionLayer.SELECTORS.inputUrlMaptile).parent().find('.placeholder').attr('style', '');
                    }
                    break;
                default:
                    // option 1,0 (map tile and infor geojson)
                    $(optionLayer.SELECTORS.inputUrlMaptile).removeAttr("readonly");
                    $(optionLayer.SELECTORS.inputUrlMaptile).prop("disabled", false);
                    $(optionLayer.SELECTORS.inputUrlMaptile).parent().find("label").removeClass("required");
                    break;
            }
        });
        //form create layer
        $(this.SELECTORS.radioOptionLayerCreate).on("change", function () {
            let value = $(optionLayer.SELECTORS.radioOptionLayerCreateCheck).val();
            TreeView.clearLabelError();
            switch (value) {
                case "2":
                    // option 2 (map tile geoserver auto,infor geojson)
                    $(optionLayer.SELECTORS.inputBoundsMaptileLayer).val("[]");
                    $(optionLayer.SELECTORS.inputBoundsMaptileLayer).parent().find('.placeholder').attr('style', 'transform: scale(0.8) translateY(-24px) !important; background: #fff;');

                    $(TreeView.SELECTORS.inputGroundMaptileLayer).val("");
                    $(TreeView.SELECTORS.inputGroundMaptileLayer).attr("readonly", "readonly");
                    $(TreeView.SELECTORS.inputGroundMaptileLayer).prop("disabled", true);

                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).attr("readonly", "readonly");
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).prop("disabled", true);

                    $(TreeView.SELECTORS.inputGroundMaptileLayer).parent().find("label").removeClass("required");

                    break;
                case "3":
                    // option 3 (map tile and infor geoserver other)
                    $(optionLayer.SELECTORS.inputBoundsMaptileLayer).val("");
                    $(optionLayer.SELECTORS.inputBoundsMaptileLayer).parent().find('.placeholder').attr('style', '');

                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).attr("readonly", "readonly");
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).prop("disabled", true);

                    $(TreeView.SELECTORS.inputGroundMaptileLayer).removeAttr("readonly");
                    $(TreeView.SELECTORS.inputGroundMaptileLayer).prop("disabled", false);

                    $(TreeView.SELECTORS.inputGroundMaptileLayer).parent().find("label").addClass("required");
                    break;
                default:
                    // option 1,0 (map tile and infor geojson)
                    $(TreeView.SELECTORS.inputGroundMaptileLayer).val("");
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).val(bounds);
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).removeAttr("readonly");
                    $(TreeView.SELECTORS.inputGroundMaptileLayer).prop("disabled", false);

                    $(TreeView.SELECTORS.inputGroundMaptileLayer).removeAttr("readonly");
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).prop("disabled", false);
                    $(TreeView.SELECTORS.inputGroundMaptileLayer).parent().find("label").removeClass("required");
                    break;
            }
        });
    },

    //#region---Option 2---
    //call creat layer and data
    createLayerToLayer: function (data) {
        var param = {
            idDirectory: data.id,
            layerGeoserver: data.nameTypeDirectory,
            status: 0,
            tags: null
        }
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: this.CONSTS.URL_POST_CREATE_LAYER,
            data: JSON.stringify(param),
            success: function (result) {
                if (result.code == "ok") {
                    console.log(result);
                } else {
                    swal({
                        title: l("Layer:Notification"),
                        text: l("Layer:CreateDataLayerFailed"),
                        icon: "error",
                        button: l("Layer:Close"),
                    }).then((value) => {
                    });
                }
            },
            error: function () {
            }
        });
    },
    //set option layer form edit
    setEditOptionLayer: function (val) {
        var str = optionLayer.SELECTORS.optionLayerCheck.replace("VAL", val);
        $(str).prop("checked", true);

        if (val === 1 || val === 2) {//hide option 3
            this.hideAndShowOption("3");
            if (val === 2) {
                $(optionLayer.SELECTORS.inputUrlMaptile).attr("readonly", "readonly");
            }
        } else {//hide option 1,2
            this.hideAndShowOption("1,2");
        }
    },
    hideAndShowOption: function (optionHide) {
        $(optionLayer.SELECTORS.radioOptionLayer).each(function () {
            let val = $(this).val();
            if (optionHide.indexOf(val) >= 0) {
                $(this).attr("disabled", "disabled");
            } else {
                $(this).removeAttr("disabled");
            }
        });
    }
    //#endregion 
}