﻿var TreeView = {
    GLOBAL: {
        propertyFile: {
            avatar2D: '',
            object3D: '',
            texture3D: ''
        },
        propertyData: null,
        checkRadiosLayerMap: false,
        propertiesSetting: {
            MinZoom: 0,
            MaxZoom: 22,
            Image2D: "",
            Stroke: "#8b5b5b",
            StrokeWidth: 2,
            StrokeOpacity: 1,
            StyleStroke: "",
            Fill: "#8b5b5b",
            FillOpacity: 0.3,
        },
        ArrayTree: [],
        lstLayerPermission: [],
        lstIdLayer: [],
        ListIdShow: [],
        isAdmin: "",
        isAction: -1,
        arrayDirectoryClone: [] // dữ liệu hiển thị option layer clone
    },
    CONSTS: {
        URL_GETLISTDIRECTORY: "/api/htkt/directory/get-list-new-layer",
        URL_CLONEPROPERTIESDIRECTORY: "/api/htkt/directory/clone-directory",
        //URL_GETLISTDIRECTORY: "/api/khaithac/khaiThacDirectory/get-list-layer-has-setting",
        URL_CHECK_DELETE: "/api/htkt/directory/check-delete-layer",
        URL_GETLISTDIRECTORY_COUNT: '/api/HTKT/PropertiesDirectoryStatistic/get-list-directory-statistic',
        URL_UPDATE_NAME_FOLDER: "/api/htkt/directory/update-name",
        URL_GET_LIST_DIRECTORY_CLONE: "/api/HTKT/PropertiesDirectory/get-list" // api lấy danh sách layer (name, code) show option layer clone
    },
    SELECTORS: {
        iconDatabase: "fa fa-database",
        modalFolder: ".exampleModal-folder",
        modalLayer: ".Modal-layer",
        inputmodalFolder: ".exampleModal-folder input",
        inputmodalLayer: ".Modal-layer input",
        addLayer: "#addLayer",
        addFolder: "#addFolder",
        editFolder: "#editFolder",
        searchTree: "#searchTree",
        btnSaveFolderLayer: "#btnSaveFolderLayer",
        btnSaveFolder: "#btnSaveFolder",
        btnDeleteFolder: "#btnDeleteFolder",
        listGroupItem: ".list-group-item",
        treeViewShow: ".tree-view",
        layerdata: ".layerdata",
        radiosLayer: "input[name='optionsLayer']",
        radiosLayerChecked: "input[name='optionsLayer']:checked",
        inputName: "#nameLayer",
        inputnameImplement: "#NameImplement",
        radiosLayerMap: "input[name='optionsLayerMap']",
        inputGroundMaptileLayer: "#inputGroundMaptileLayer",
        inputBoundsMaptileLayer: "#inputBoundsMaptileLayer",
        inputUrlMaptile: "#UrlMaptile",
        inputBoundMaptile: "#BoundMaptile",
        inputLatLayer: "#latLayer",
        inputLngLayer: "#lngLayer",
        inputZoomLayer: "#Zoom-Layer",
        inputFileOject3D: "#choosenFile-obj",
        btnPermission: "#addPermissionLayer",
        InforData: "#InforData",
        btnSetting: "#SettingLayer",
        optionCreatLayer: "input[name ='optionLayerCreate']",
        optionCreatLayerchecked: "input[name ='optionLayerCreate']:checked",
        toggle_side_bar: ".menuMap #hideTree",
        sideForm: ".map-side-form",

        menu: ".menuMap",
        action_menu: ".action-menu",
        content_section: ".content-section",
        black_drop_content: ".black-drop-content",
        toogle_content_section: ".toogle-content-section",
        btn_close_content_section: ".btn-close-content",
        inputNameFolder: "#nameFolder",

        modal_setting_layer: "#modal-setting-layer",
        file_object_content: ".file-object-content",
        btn_delete_objec3d: ".action-file-obj",
        dataName: "#DataName",
        dataCode: "#DataCode",
    },
    init: function () {
        this.GLOBAL.lstLayerPermission = TVlstLayerPermission;
        this.GLOBAL.lstIdLayer = TVlstIdLayer;
        this.GLOBAL.isAdmin = TVisAdmin;
        TreeView.setUpEvent();
        TreeLayer.init();
        TreeView.GetDirectoryCount();

        TreeView.GetDirectoryCloneOption();
    },
    setUpEvent: function () {
        // đóng mở sidebar
        $(TreeView.SELECTORS.toggle_side_bar).on('click', function () {
            let svgtext = '';
            if ($(TreeView.SELECTORS.sideForm).hasClass("show")) {
                $(TreeView.SELECTORS.sideForm).removeClass("show");
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <path class="a" style="fill: none;" d="M24,0H0V24H24Z" transform="translate(6)" />
                                <path class="b" style="fill: var(--primary)" d="M26.172,11H4v2H26.172l-5.364,5.364,1.414,1.414L30,12,22.222,4.222,20.808,5.636Z" transform="translate(-4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 18)" />
                            </svg>`;
                $(this).attr("data-original-title", l("ManagementLayer:OpenPanel"));

                $('.div-content').addClass('toogle'); // sidebar toogle
            } else {
                $(TreeView.SELECTORS.sideForm).addClass("show");
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <g transform="translate(1988 32)">
                                    <g transform="translate(-1988 -32)">
                                        <path style="fill: none;" class="a" d="M0,0H24V24H0Z"></path>
                                        <path style="fill: var(--primary)" class="b" d="M7.828,11H30v2H7.828l5.364,5.364-1.414,1.414L4,12l7.778-7.778,1.414,1.414Z"></path>
                                    </g>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -28)"></rect>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -14)"></rect>
                                </g>
                            </svg>`;
                $(this).attr("data-original-title", l("ManagementLayer:ClosePanel"));

                $('.div-content').removeClass('toogle'); // sidebar toogle
            }
            $(this).html('');
            $(this).html(svgtext);
        });

        // click menu
        $(TreeView.SELECTORS.menu).on('click', TreeView.SELECTORS.action_menu, function () {
            var typeAction = $(this).attr('data-type');
            $(TreeView.SELECTORS.action_menu).removeClass('isForcus');
            $(this).addClass('isForcus');

            // disabled nút action
            $(TreeView.SELECTORS.action_menu).prop('disabled', true);
            $(this).prop('disabled', false);
            $(TreeView.SELECTORS.action_menu).addClass('disabled');
            $(this).removeClass('disabled');

            $(TreeView.SELECTORS.content_section).removeClass('open');
            let tree = TreeView.getTreeView(thisId);

            TreeView.GLOBAL.isAction = parseInt(typeAction); // set trạng thái action
            switch (typeAction) {
                case "1": // thêm mới folder
                    $(TreeView.SELECTORS.inputNameFolder).val("");

                    TreeView.clearLabelError();
                    $(TreeView.SELECTORS.modalFolder).addClass("open");
                    $(TreeView.SELECTORS.black_drop_content).show();

                    $(TreeView.SELECTORS.treeViewShow).text("(" + tree + ")");

                    $(this).html(`<svg id="Component_211_2" data-name="Component 211 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_6542" data-name="Path 6542" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_6543" data-name="Path 6543" d="M12.414,5H21a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414ZM11,12H8v2h3v3h2V14h3V12H13V9H11Z" fill="var(--primary)"/>
                                    </svg>`);
                    $(TreeView.SELECTORS.modalFolder + " .content-section-header span").text(l('Layer:AddNewFolder')); // thay đổi tên title
                    break;
                case "2": // sửa tên foler
                    $(TreeView.SELECTORS.inputNameFolder).val("");
                    var folder = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == TreeLayer.GLOBAL.DirectoryForcus);
                    if (folder != undefined) {
                        $(TreeView.SELECTORS.inputNameFolder).val(folder.name);

                        TreeView.clearLabelError();
                        $(TreeView.SELECTORS.modalFolder).addClass("open");
                        $(TreeView.SELECTORS.black_drop_content).show();
                        $(TreeView.SELECTORS.treeViewShow).text("(" + tree + ")");

                        $(this).html(`<svg id="Component_209_2" data-name="Component 209 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path id="Path_6610" data-name="Path 6610" d="M0,0H24V24H0Z" fill="none"/>
                                        <path id="Path_6611" data-name="Path 6611" d="M9.243,19H21v2H3V16.757l9.9-9.9L17.142,11.1,9.242,19Zm5.07-13.556,2.122-2.122a1,1,0,0,1,1.414,0l2.829,2.829a1,1,0,0,1,0,1.414L18.556,9.686,14.314,5.444Z" fill="var(--primary)"/>
                                    </svg>`);

                        $(TreeView.SELECTORS.modalFolder + " .content-section-header span").text(l('Layer:EditNameFolder')); // thay đổi tên title
                    }
                    break;
                case "3": //thêm mới layer
                    TreeView.ClearInputFormAddLayer();
                    TreeView.clearLabelError();
                    $(TreeView.SELECTORS.modalLayer).addClass("open");
                    $(TreeView.SELECTORS.black_drop_content).show();

                    $(TreeView.SELECTORS.treeViewShow).text("(" + tree + ")");

                    $(this).html(`<svg id="Component_172_2" data-name="Component 172 – 2" xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20">
                          <path id="Path_2629" data-name="Path 2629" d="M16,2l5,5V21.008a.993.993,0,0,1-.993.992H3.993A1,1,0,0,1,3,21.008V2.992A.993.993,0,0,1,3.993,2Zm-5,9H8v2h3v3h2V13h3V11H13V8H11Z" transform="translate(-3 -2)" fill="var(--primary)"/>
                        </svg>`); // set hightlight icon

                    break;
                case "4": // cập nhật cấu hình layer
                    TreeView.clearLabelError();
                    $(`${TreeView.SELECTORS.modal_setting_layer} .title-li-tab .nav-item .nav-link`).eq(0).click(); // mặc định mở tab cấu hình
                    $(TreeView.SELECTORS.modal_setting_layer).addClass("open");

                    $(TreeView.SELECTORS.black_drop_content).show();

                    $(this).html(`<svg id="Component_183_2" data-name="Component 183 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                          <path id="Path_6394" data-name="Path 6394" d="M0,0H24V24H0Z" fill="none"/>
                          <path id="Path_6395" data-name="Path 6395" d="M5.334,4.545A9.99,9.99,0,0,1,8.876,2.5a4,4,0,0,0,6.248,0,9.99,9.99,0,0,1,3.542,2.048,4,4,0,0,0,3.125,5.409,10.043,10.043,0,0,1,0,4.09,4,4,0,0,0-3.125,5.41A9.99,9.99,0,0,1,15.124,21.5a4,4,0,0,0-6.248,0,9.99,9.99,0,0,1-3.542-2.047,4,4,0,0,0-3.125-5.409,10.043,10.043,0,0,1,0-4.091A4,4,0,0,0,5.334,4.546ZM13.5,14.6a3,3,0,1,0-4.1-1.1,3,3,0,0,0,4.1,1.1Z" fill="var(--primary)"/>
                        </svg>`);  // set hightlight icon

                    break;
                case "5": // phân quyền
                    TreeView.clearLabelError();
                    if (TreeLayer.GLOBAL.DirectoryForcus != "") {
                        PermissionLayer.ResetModal();
                        PermissionLayer.GetPermissionDirectory();
                        $(PermissionLayer.SELECTORS.modalPermisison).addClass('open');
                        $(TreeView.SELECTORS.black_drop_content).show();

                        $(this).html(`<svg id="Group_3181" data-name="Group 3181" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_6656" data-name="Path 6656" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_6657" data-name="Path 6657" d="M12,14v8H4a8,8,0,0,1,8-8Zm0-1a6,6,0,1,1,6-6A6,6,0,0,1,12,13Zm2.6,5.812a3.51,3.51,0,0,1,0-1.623l-.992-.573,1-1.732.992.573A3.5,3.5,0,0,1,17,14.645V13.5h2v1.145a3.492,3.492,0,0,1,1.405.812l.992-.573,1,1.732-.992.573a3.51,3.51,0,0,1,0,1.622l.992.573-1,1.732-.992-.573a3.5,3.5,0,0,1-1.4.812V22.5H17V21.355a3.5,3.5,0,0,1-1.4-.812l-.992.573-1-1.732.992-.572ZM18,17a1,1,0,1,0,1,1A1,1,0,0,0,18,17Z" fill="var(--primary)"/>
                                    </svg>`);// set hightlight icon
                    }
                    break;
                case "6": // xóa đối tương hoặc thư mục
                    var id = TreeLayer.GLOBAL.DirectoryForcus;

                    if (id != "") {
                        var level = listFolder.find(x => x.id == id); // Tìm kiếm directory
                        if (level != undefined && level.level > 0) {
                            var textinfor = (level.type === "file") ? l("Layer:AreYouDeleteDataLayer") : l("Layer:AreYouDeleteFolder");
                            swal({
                                title: l("Layer:Notification"),
                                text: textinfor,
                                icon: "warning",
                                buttons: [
                                    l("Layer:Cancel"),
                                    l("Layer:Agree")
                                ],
                                dangerMode: true,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    if (TreeView.CheckDeleteLayer(id)) {
                                        lstForlerDelete = [];
                                        TreeView.DeleteFolderLayer(id, level.type);
                                    }
                                    else {
                                        if (level.type === "file") {
                                            abp.notify.error(l("Layer:CheckDeleteLayerFail"));
                                        } else {
                                            abp.notify.error(l("Layer:CheckDeleteFolderFail"));
                                        }
                                        TreeView.CloseContentSection(); // reset action
                                    }
                                }
                                else {
                                    TreeView.CloseContentSection(); // reset action
                                }
                            })
                        }

                        $(this).html(`<svg id="Component_210_2" data-name="Component 210 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                          <path id="Path_6554" data-name="Path 6554" d="M0,0H24V24H0Z" fill="none"/>
                                          <path id="Path_6555" data-name="Path 6555" d="M20,7V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H2V5H22V7Zm-9,3v7h2V10ZM7,2H17V4H7Z" fill="var(--primary)"/>
                                        </svg>`);
                    }
                    break;
                default:
            }
        })

        // đóng form section
        $(TreeView.SELECTORS.toogle_content_section).on('click', function () {
            TreeView.CloseContentSection();
        })

        // đóng form section
        $(TreeView.SELECTORS.btn_close_content_section).on('click', function () {
            TreeView.CloseContentSection();
        })

        $(TreeView.SELECTORS.searchTree).on('keyup', TreeView.delay(function () {
            var keyword = $(TreeView.SELECTORS.searchTree).val().toLowerCase();
            $("#tree").remove();
            $("#divTree").append('<div id="tree"></div>');
            var result = TreeView.ReturnInforItemTree(item, keyword);
            TreeView.GetInforTreeView();
        }, 1000));
        // save layer
        $(TreeView.SELECTORS.btnSaveFolderLayer).on('click', function () {
            if (TreeView.checkFormLayerInfor()) {
                var name = $('#nameLayer').val();
                if (name.length < 100) {
                    var urlmap = '';
                    var bounds = '';
                    //if ($(TreeView.SELECTORS.radiosLayerMap).prop("checked")) {
                    urlmap = $(TreeView.SELECTORS.inputGroundMaptileLayer).val();
                    bounds = $(TreeView.SELECTORS.inputBoundsMaptileLayer).val();
                    //};
                    let type = $(TreeView.SELECTORS.radiosLayerChecked).val();
                    if (type == "2") {
                        let idimplement = $(TreeView.SELECTORS.layerdata).val();
                        if (idimplement !== "0") {
                            var dto = {
                                name: name,
                                parentId: parentId,
                                level: level,
                                typeDirectory: false,
                                idImplement: idimplement,
                                location: {
                                    "lat": ($(TreeView.SELECTORS.inputLatLayer).val() != "" ? $(TreeView.SELECTORS.inputLatLayer).val() : 0),
                                    "lng": ($(TreeView.SELECTORS.inputLngLayer).val() != "" ? $(TreeView.SELECTORS.inputLngLayer).val() : 0)
                                },
                                zoom: ($(TreeView.SELECTORS.inputZoomLayer).val() != "" ? $(TreeView.SELECTORS.inputZoomLayer).val() : 15),
                                boundMaptile: bounds,
                                groundMaptile: urlmap,
                                optionDirectory:Number($(TreeView.SELECTORS.optionCreatLayerchecked).val())
                            }
                            abp.ui.setBusy(TreeView.SELECTORS.modalLayer);
                            TreeView.CloneLayeredForCurrentLayer(dto);
                        }
                    } else {

                        var dto = {
                            name: name,
                            parentId: parentId,
                            level: level,
                            typeDirectory: false
                        }

                        // set loader trước khi lưu
                        abp.ui.setBusy(TreeView.SELECTORS.modalLayer);
                        $(TreeView.SELECTORS.modalLayer).find('.toogle-content-section').hide();

                        setTimeout(function () {
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                url: "/api/htkt/directory/create-folder",
                                async: false,
                                data: JSON.stringify(dto),
                                success: function (res) {
                                    if (res.code == "ok") {
                                        var result = res.result;
                                        var locationDto = {
                                            "lat": 0,
                                            "lng": 0
                                        };
                                        if ($(TreeView.SELECTORS.inputLatLayer).val() != "" && $(TreeView.SELECTORS.inputLngLayer).val() != "") {
                                            locationDto.lat = $(TreeView.SELECTORS.inputLatLayer).val();
                                            locationDto.lng = $(TreeView.SELECTORS.inputLngLayer).val()
                                        }
                                        // tạo phân quyền
                                        TreeView.AddPermissionUserCreateFolder(result.id);

                                        //////////
                                        var dto2 = {
                                            "idDirectory": result.id,
                                            "nameTypeDirectory": result.name,
                                            "codeTypeDirectory": xoa_dau(result.name).toUpperCase(),
                                            "verssionTypeDirectory": 0,
                                            "idImplement": null,
                                            "listProperties": [],
                                            "groundMaptile": urlmap,
                                            "location": locationDto,
                                            "boundMaptile": bounds,
                                            "zoom": ($(TreeView.SELECTORS.inputZoomLayer).val() != "" ? $(TreeView.SELECTORS.inputZoomLayer).val() : 15),
                                            "optionDirectory": Number($(TreeView.SELECTORS.optionCreatLayerchecked).val())
                                        };
                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: "application/json; charset=utf-8",
                                            url: "/api/HTKT/PropertiesDirectory/create",
                                            data: JSON.stringify(dto2),
                                            success: function (result) {
                                                if (result.code == "ok") {

                                                    abp.notify.success(l("Layer:CreateDataLayerSuccessfully"));

                                                    TreeView.GLOBAL.lstIdLayer.push(dto2.idDirectory);
                                                    let arr = TreeView.AddPermissionLayerOrFolder(dto2.idDirectory);
                                                    TreeView.GLOBAL.lstLayerPermission.push(arr);
                                                    TreeLayer.GetListLayer(dto2.idDirectory);
                                                    TreeView.CloseContentSection();
                                                } else {
                                                    abp.notify.error(l("Layer:CreateDataLayerFailed"));
                                                    TreeView.CloseContentSection();
                                                }

                                                // clear loader
                                                abp.ui.clearBusy(TreeView.SELECTORS.modalLayer);
                                                $(TreeView.SELECTORS.modalLayer).find('.toogle-content-section').show();


                                                TreeView.GetDirectoryCloneOption(); // reset comboobx layer kế thừa
                                            },
                                            error: function () {
                                            }
                                        });
                                    }
                                    else {
                                        console.log(res)
                                    }
                                },
                                error: function () {
                                }
                            });
                        }, 500)
                    }
                } else {
                    swal({
                        title: l("Layer:Notification"),
                        text: l("Layer:MaxLength100Char"),
                        icon: "error",
                        button: l("Layer:Close"),
                    });
                }
            }
        });
        // save folder
        $(TreeView.SELECTORS.btnSaveFolder).on('click', function () {
            if (TreeView.checkFormInfor()) // check valid add folder
            {
                if (TreeView.GLOBAL.isAction == 2) // trạng thái sửa thư mục
                {
                    TreeView.UpdateNameFolder(TreeLayer.GLOBAL.DirectoryForcus);
                }
                else {
                    var name = $(TreeView.SELECTORS.inputNameFolder).val();
                    var dto = {
                        name: name,
                        parentId: parentId,
                        level: level,
                        typeDirectory: true
                    }
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: "/api/htkt/directory/create-folder",
                        beforeSend: function () {
                            abp.ui.setBusy(TreeView.SELECTORS.modalFolder);
                            $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').hide();
                        },
                        data: JSON.stringify(dto),
                        success: function (res) {
                            if (res.code == "ok") {
                                var result = res.result;
                                abp.notify.success(l("Layer:AddFolderSuccessfully"));

                                //TreeView.ReloadTree(result.id);
                                TreeView.GLOBAL.lstIdLayer.push(result.id);
                                let arr = TreeView.AddPermissionLayerOrFolder(result.id);
                                TreeView.GLOBAL.lstLayerPermission.push(arr);
                                TreeLayer.GetListLayer(result.id);
                                // tạo phân quyền
                                TreeView.AddPermissionUserCreateFolder(result.id);
                                TreeView.CloseContentSection();
                            }
                            else {
                                abp.notify.error(l("Layer:AddFolderFailed"));
                                console.log(res)
                            }
                        },
                        complete: function () {
                            abp.ui.clearBusy(TreeView.SELECTORS.modalFolder);
                            $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').show();
                        },
                        error: function () {
                            $(TreeView.SELECTORS.modalFolder).modal('hide');
                        }
                    });
                }
            }
        });

        $(TreeView.SELECTORS.inputAvatar2D).on('change', function () {
            var url = $(this).val();
            //console.log(url);
            $('#imgAvatar2D').attr('src', url);
        });

        //Ẩn bảng property
        $('.toggle-detail-property').click(function () {
            $('.detail-property-header').css('display', 'none');
            $('.toggle-detail-property').css('display', 'none');
            $('.detail-property-content').css('display', 'none');
            widthModalRight = $('.modal-info-right').width();
            $('.detail-property1.detail-property-collapse').animate({
                width: widthModalRight
            }, 200);
            $('.detail-property1.detail-property-collapse').css('height', '100px');
            $(".b1").removeClass('fill-color-svg');
            $('#InforData .detail-property-body').css('height', '100%');
        });

        //show bang property
        $('.li-modal-data-right').click(function () {
            if (config.GLOBAL.idDirectory != "" && $('.detail-property1.detail-property-collapse').width() < 100 && datainfo.GLOBAL.statusShow) {
                $('.detail-property-header').css('display', 'block');
                $('.toggle-detail-property').css('display', 'flex');
                $('.detail-property-content').css('display', 'block');
                $('.detail-property1.detail-property-collapse').animate({
                    width: 410
                }, 200);
                $('.detail-property1.detail-property-collapse').css('height', 'calc(100% - 22px)');
                $('#InforData .detail-property-body').css('height', 'calc(100% - 40px)');
                $(".b1").removeClass('fill-color-svg');
                $(this).find(".b1").addClass('fill-color-svg');
            } else {
                if (config.GLOBAL.idDirectory != "") {
                    let obj = listFolder.find(x => x.id == config.GLOBAL.idDirectory);
                    if (obj != null && obj != undefined && obj.type == "file") {
                        $(".b1").removeClass('fill-color-svg');
                        $(this).find(".b1").addClass('fill-color-svg');
                    }
                }
            }
        });


        //Thuộc tính màu viền
        $('#strokeText').on('click', function () {
            $('#strokeinput').trigger('click');
        });

        //-----------------------sự kiện click ra ngoài input text màu của thuộc tính đường----------------------------------------------------------------
        $('#strokeText').on('focusout', function () {

            if ($(this).val().indexOf("#") >= 0) {
                $('#strokeinput').val($(this).val());
                $('#strokeColor').css('background-color', $(this).val());
            }
        });

        $('#strokeText').on('paste', function (e) {
            var pastedData = e.originalEvent.clipboardData.getData('text');
            if (pastedData.indexOf("#") >= 0) {
                $('#strokeinput').val(pastedData);
                $('#strokeColor').css('background-color', pastedData);
            }
        });

        //------------------------------------------------------------------

        $('#strokeColor').on('click', function () {
            $('#strokeinput').trigger('click');
        });

        $('#strokeinput').on('change', function () {
            $('#strokeText').val($(this).val());
            $('#strokeColor').css('background', $(this).val());
        });

        //-------------------------------------------------------------------------
        //Thuộc tính màu của vùng
        $('#fillText').on('click', function () {
            $('#fillInput').trigger('click');
        });

        //-----------------------sự kiện click ra ngoài input text màu của thuộc tính đường----------------------------------------------------------------
        $('#fillText').on('focusout', function () {

            if ($(this).val().indexOf("#") >= 0) {
                $('#fillInput').val($(this).val());
                $('#fillColor').css('background-color', $(this).val());
            }
        });

        $('#fillText').on('paste', function (e) {
            var pastedData = e.originalEvent.clipboardData.getData('text');
            if (pastedData.indexOf("#") >= 0) {
                $('#fillInput').val(pastedData);
                $('#fillColor').css('background-color', pastedData);
            }
        });

        $('#fillColor').on('click', function () {
            $('#fillInput').trigger('click');
        });
        $('#fillInput').on('change', function () {
            $('#fillText').val($(this).val());
            $('#fillColor').css('background', $(this).val());
        });

        //Ảnh đại diện
        $('#inputAvatar2D').on('change', function () {
            var files = $(this)[0].files;
            if (files != null && files.length > 0) {
                if (files[0].size <= 512000) {
                    var exp = files[0].name.substring(files[0].name.lastIndexOf(".") + 1).toLowerCase();
                    if (exp == "png" || exp == "jpg" || exp == "jpeg") {
                        var fReader = new FileReader();
                        fReader.readAsDataURL($(this)[0].files[0]);
                        fReader.onloadend = function (event) {
                            $('#imgAvatar2D').attr('src', event.target.result);
                            TreeView.GLOBAL.propertyFile.avatar2D = event.target.result;
                        }
                    }
                    else {
                        $(this).parents().closest('.row').append(`<label class="col-md-12 lable-error">asdasd</label>`);
                    }
                }
            }
        });

        //Đối tượng 3D
        $('#inputObject3D').on('change', function () {
            //   console.log($(this)[0].files[0].name);
            $('#text-object3d').text($(this)[0].files[0].name);
            var fReader = new FileReader();
            fReader.readAsDataURL($(this)[0].files[0]);
            fReader.onloadend = function (event) {
                //console.log(event.target.result);
                TreeView.GLOBAL.propertyFile.object3D = event.target.result;
            }
        });

        //Đối tượng 3D
        $(TreeView.SELECTORS.inputFileOject3D).on('change', function () {
            var files = $(this)[0].files;
            if (files != null && files.length > 0) {
                // check size file
                if (files[0].size <= sizeFiles.sizeFileObj) {
                    var exp = files[0].name.substring(files[0].name.lastIndexOf(".") + 1).toLowerCase();
                    if (exp == "obj") {
                        var fReader = new FileReader();
                        fReader.readAsDataURL($(this)[0].files[0]);
                        fReader.onloadend = function (event) {
                            $(`.content-errors-file-obj`).hide();
                            // hiển thị tên file
                            var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${files[0].name}" data-toggle="tooltip" data-placement="bottom">${files[0].name}</div>
                            <div class="action-file-obj" title="Xóa" data-toggle="tooltip" data-placement="bottom"><img src="/images/close_cricle.svg" /></div>`; // hiển thị tên file đã được chọn
                            $(TreeView.SELECTORS.file_object_content).html(html);
                            $(TreeView.SELECTORS.file_object_content).show();
                        }
                    }
                    else {
                        $(TreeView.SELECTORS.file_object_content).hide();
                        var html = `<div class="text-danger" title="${jQuery.validator.format(l("Layer:ValidFormat"), 'File')}" data-toggle="tooltip" data-placement="bottom">${jQuery.validator.format(l("Layer:ValidFormat"), 'File')}</div>`;
                        $(`.content-errors-file-obj`).html(html);
                        $(`.content-errors-file-obj`).show();
                        $(TreeView.SELECTORS.inputFileOject3D).val(null).trigger("change");
                    }
                }
                else {
                    var html = `<div class="text-danger lable-error" title="${jQuery.validator.format(l("Layer:ValidFormat"), 'File')}" data-toggle="tooltip" data-placement="bottom">${l("ManagementLayer:ValidSizeFileObj")}</div>`;
                    $(`.content-errors-file-obj`).html(html);
                    $(`.content-errors-file-obj`).show();
                    $(TreeView.SELECTORS.inputFileOject3D).val(null).trigger("change");
                }
            }
        });

        $(`${TreeView.SELECTORS.modal_setting_layer}`).on('click', TreeView.SELECTORS.btn_delete_objec3d, function () {
            $(TreeView.SELECTORS.file_object_content).html('');
            $(TreeView.SELECTORS.inputFileOject3D).val(null).trigger("change");
            $(TreeView.SELECTORS.file_object_content).hide();
        });

        //Ảnh đại diện
        $('#inputTexture3D').on('change', function () {
            // console.log($(this)[0].files[0]);
            var fReader = new FileReader();
            fReader.readAsDataURL($(this)[0].files[0]);
            fReader.onloadend = function (event) {
                $('#imgTexture3D').attr('src', event.target.result);
                TreeView.GLOBAL.propertyFile.texture3D = event.target.result;
            }
        });

        //Độ mờ của viền
        $('#rangeMax-strokeopacity').on('input', function (e) {
            const newValue = parseInt(e.target.value);
            $('#thumbMax-strokeopacity').css('left', newValue + '%');
            $('#max-strokeopacity').html(newValue + '.00%');
            $('#line-strokeopacity').css({
                'right': (100 - newValue) + '%'
            });
            //let array = mapdata.updateMainObject();
            //let geojson = mapdata.getDataGeojsonByProperties([array]);
            //mapdata.showMoreDataGeojson(JSON.stringify(geojson));
        });

        //Độ mờ của vùng
        $('#rangeMax-fillOpactity').on('input', function (e) {
            const newValue = parseInt(e.target.value);
            $('#thumbMax-fillOpactity').css('left', newValue + '%');
            $('#max-fillOpactity').html(newValue + '.00%');
            $('#line-fillOpactity').css({
                'right': (100 - newValue) + '%'
            });
            //let array = mapdata.updateMainObject();
            //let geojson = mapdata.getDataGeojsonByProperties([array]);
            //mapdata.showMoreDataGeojson(JSON.stringify(geojson));
        });

        $('.btn-save-properties').on('click', function () {
            if (config.GLOBAL.idDirectory != '') {
                if ($('#stroke-width').val() >= "0") {
                    abp.ui.setBusy(TreeView.SELECTORS.modal_setting_layer);

                    setTimeout(function () {
                        TreeView.UpdateProperty();
                    }, 500);
                } else {
                    swal({
                        title: l("Layer:Notification"),
                        text: l("Layer:DataInvalid"),
                        icon: "error",
                        button: l("Layer:Close"),
                    }).then((value) => {
                        if (!($('#min-zoom').val() >= "0")) {
                            $('#min-zoom').focus();
                        } else if (!($('#max-zoom').val() >= "0")) {
                            $('#max-zoom').focus();
                        } else if (!($('#stroke-width').val() >= "0")) {
                            $('#stroke-width').focus();
                        }
                    });;
                }
            } else {
                swal({
                    title: l("Layer:Notification"),
                    text: l("Layer:PleaseSelectLayer"),
                    icon: "error",
                    button: l("Layer:Close"),
                });
            }
        });

        $(document).on('click', '.list-group-item', function () {
            var type = $(this).attr('data-type');
            if (type == "file") {
                TreeView.getProperty();
            } else {
                $('.toggle-detail-property').trigger('click');
                $('.li-modal-data-right').removeClass("active");
            }
        });

        // select 2
        $(TreeView.SELECTORS.layerdata).select2({
            language: "vi"
        })
            .on("select2:opening", function () {
                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff; color: var(--primary);");
            })
            .on("select2:closing", function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                }
                else {
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            })
            .on('select2:select', function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                }
                else {
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            });
        //---------------------------------------------

        $(TreeView.SELECTORS.layerdata).prop("disabled", true);
        $(TreeView.SELECTORS.radiosLayer).on("change", function () {
            let check = $(this).val();
            if (check === "2") {
                $(TreeView.SELECTORS.layerdata).prop("disabled", false);
            } else {
                $(TreeView.SELECTORS.layerdata).prop("disabled", true);
            }
            TreeView.getAllLayerforClone();
        });

        $(TreeView.SELECTORS.radiosLayerMap).on("click", function () {
            if (TreeView.GLOBAL.checkRadiosLayerMap === false) {
                $(TreeView.SELECTORS.inputGroundMaptileLayer).prop("readonly", false);
                $(TreeView.SELECTORS.inputBoundsMaptileLayer).prop("readonly", false);
                if ($(TreeView.SELECTORS.inputBoundsMaptileLayer).val() == "") {
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).val(bounds);
                }
                $(optionLayer.SELECTORS.radioOptionLayerCreate).trigger("change");
            } else {
                $(TreeView.SELECTORS.inputGroundMaptileLayer).prop("readonly", true);
                $(TreeView.SELECTORS.inputBoundsMaptileLayer).prop("readonly", true);
                $(TreeView.SELECTORS.radiosLayerMap).prop("checked", false);
                if ($(TreeView.SELECTORS.inputBoundsMaptileLayer).val() != "") {
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).parent().find('.placeholder').css({ "transform": "scale(0.8) translateY(-24px)", "background": "#FFF" });
                }
                else {
                    $(TreeView.SELECTORS.inputBoundsMaptileLayer).parent().find('.placeholder').removeAttr('style');
                }
            }

            TreeView.GLOBAL.checkRadiosLayerMap = !TreeView.GLOBAL.checkRadiosLayerMap;
        });

        // nhập input create layer
        $(TreeView.SELECTORS.modalLayer).on('keyup', 'input', function () {
            $(this).parent().find('.lable-error').remove();
            $(this).parent().removeClass('has-error');

        });

        $(TreeView.SELECTORS.modalLayer).on('change', 'select', function () {
            $(this).parent().find('.lable-error').remove();
            $(this).parent().removeClass('has-error');

        });

        //$('.toggle-detail-property').trigger('click');

        $(TreeView.SELECTORS.inputmodalFolder).on('keyup', function () {
            $(this).parent().find('.lable-error').remove();
            $(this).parent().removeClass('has-error');

        });
        window.onresize = resize;
        //---auto code property
        $(TreeView.SELECTORS.dataName).on('change keyup', function () {
            config.GLOBAL.dataName = $(this).val();
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }
            $(TreeView.SELECTORS.dataCode).val(value);
        });

        $(TreeView.SELECTORS.dataCode).on('change keyup', function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
                $(TreeView.SELECTORS.dataCode).val(value);
            }
        });

        $(TreeLayer.SELECTORS.minZoom).on("change", function () {
            if (Number($(TreeLayer.SELECTORS.minZoom).val()) > Number($(TreeLayer.SELECTORS.maxZoom).val()) && $(TreeLayer.SELECTORS.maxZoom).val() !== "") {
                $(TreeLayer.SELECTORS.minZoom).val($(TreeLayer.SELECTORS.maxZoom).val());   // set mức zoom nhỏ nhất = mức zoom lớn nhất
            }
        })

        $(TreeLayer.SELECTORS.maxZoom).on("change", function () {
            if (Number($(TreeLayer.SELECTORS.maxZoom).val()) < Number($(TreeLayer.SELECTORS.minZoom).val())) {
                $(TreeLayer.SELECTORS.maxZoom).val($(TreeLayer.SELECTORS.minZoom).val());  // set mức zoom lớn nhất = mức zoom nhỏ nhất
            }
        })

        //$(TreeView.SELECTORS.dataVersion).on('change', function () {
        //    config.GLOBAL.dataVersion = $(this).val();
        //});
    },
    Splitter: function () {
        //console.log('z');
        TreeView.initSplitter();
    },
    // check validate add new folder
    checkFormInfor: function () {
        TreeView.clearLabelError();
        let check = true;

        if (!validateText($(TreeView.SELECTORS.inputNameFolder).val(), "text", 0, 0)) {
            check = false;
            TreeView.showLabelError("input[name='Name']", jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:FolderName")));
            $(TreeView.SELECTORS.inputNameFolder).focus();
        }

        let checkHtml = CheckedHtmlEntities(TreeView.SELECTORS.modalFolder);
        if (check) {
            check = checkHtml;
        }
        return check;
    },
    // check validate
    checkFormLayerInfor: function () {
        TreeView.clearLabelError();

        let check = true;
        let inputName = $(TreeView.SELECTORS.inputName).val();
        if (!validateText(inputName, "text", 0, 0)) {
            insertError($(TreeView.SELECTORS.inputName), "other");
            check = false;
            TreeView.showLabelError(TreeView.SELECTORS.inputName, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:DataLayerName")));
        }
        if ($(TreeView.SELECTORS.radiosLayerChecked).val() == "2") {
            let radios = $(TreeView.SELECTORS.layerdata).val();
            if (radios == "0") {
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.layerdata, l("Layer:PleaseSelectLayerInherit"));
            }
        }
        if ($(TreeView.SELECTORS.radiosLayerMap).prop("checked")) {
            if (!validateText($(TreeView.SELECTORS.inputGroundMaptileLayer).val(), "url", 0, 0)) {
                insertError($(TreeView.SELECTORS.inputGroundMaptileLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputGroundMaptileLayer, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:MaptileDatabyRegion")));
            }

            if (!validateText($(TreeView.SELECTORS.inputBoundsMaptileLayer).val(), "text", 0, 0)) {
                insertError($(TreeView.SELECTORS.inputBoundsMaptileLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputBoundsMaptileLayer, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:MaptileCoordinates")));
            }
        }

        let inputLat = $(TreeView.SELECTORS.inputLatLayer).val();
        if (validateText(inputLat, "text", 0, 0)) {
            var valueLat = parseFloat(inputLat);

            if (isNaN(valueLat)) {
                insertError($(TreeView.SELECTORS.inputLatLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputLatLayer, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Latitude")));
            }
            else {
                if (valueLat < 0 || valueLat > 90) {
                    insertError($(TreeView.SELECTORS.inputLatLayer), "other");
                    check = false;
                    TreeView.showLabelError(TreeView.SELECTORS.inputLatLayer, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Latitude")));
                }
            }
        }

        let inputLng = $(TreeView.SELECTORS.inputLngLayer).val();
        if (validateText(inputLng, "text", 0, 0)) {
            var valueLng = parseFloat(inputLng);

            if (isNaN(valueLng)) {
                insertError($(TreeView.SELECTORS.inputLngLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputLngLayer, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Longitude")));
            }
            else {
                if (valueLng < 0 || valueLng > 180) {
                    insertError($(TreeView.SELECTORS.inputLngLayer), "other");
                    check = false;
                    TreeView.showLabelError(TreeView.SELECTORS.inputLngLayer, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Longitude")));
                }
            }
        }

        let inputZoom = $(TreeView.SELECTORS.inputZoomLayer).val();
        if (validateText(inputZoom, "text", 0, 0)) {
            var valueZoom = parseFloat(inputZoom);
            if (isNaN(valueZoom)) {
                insertError($(TreeView.SELECTORS.inputZoomLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputZoomLayer, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:ZoomLevel")));
            }
            
            if (valueZoom < 3 || valueZoom > 22) {
                insertError($(TreeView.SELECTORS.inputZoomLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputZoomLayer, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:ZoomLevel")));
            }
        }

        let checkboxMapTile = $(TreeView.SELECTORS.optionCreatLayerchecked).val()
        if (checkboxMapTile == "3") {
            if ($(TreeView.SELECTORS.inputGroundMaptileLayer).val() === "" || typeof $(TreeView.SELECTORS.inputGroundMaptileLayer).val() === "undefined") {
                insertError($(TreeView.SELECTORS.inputGroundMaptileLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputGroundMaptileLayer, l("Layer:MaptileLink"));
            }else if (!validateText($(TreeView.SELECTORS.inputGroundMaptileLayer).val(), "url", 0, 0)) {
                insertError($(TreeView.SELECTORS.inputGroundMaptileLayer), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputGroundMaptileLayer, l("Layer:MaptileError"));
            }
        }

        let checkHtml = CheckedHtmlEntities(TreeView.SELECTORS.modalLayer);
        if (check) {
            check = checkHtml;
        }

        return check;
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group").addClass('has-error');
        $(element).parents(".form-group").append("<lable class='lable-error' style='color:red;'>" + text + "</lable>");
    },
    clearLabelError: function () {
        $('.form-group').find(".lable-error").remove();
        $('.form-group').removeClass("has-error");
    },

    // splitter
    initSplitter: function () {
        window_width = window.innerWidth;
        window_width = $('.noiDung').width();
        splitter = document.getElementById("splitter");
        cont1 = document.getElementById("div1");
        cont2 = document.getElementById("div2");
        var dx = cont1.clientWidth;
        splitter.style.marginLeft = dx + "px";
        dx += splitter.clientWidth;
        cont2.style.marginLeft = dx + "px";
        dx = window_width - dx;
        cont2.style.width = (dx - 30) + "px";
        splitter.addEventListener("mousedown", TreeView.spMouseDown);
    },
    spMouseDown: function (e) {
        splitter.removeEventListener("mousedown", TreeView.spMouseDown);
        window.addEventListener("mousemove", TreeView.spMouseMove);
        window.addEventListener("mouseup", TreeView.spMouseUp);
        last_x = e.clientX;
    },
    spMouseUp: function (e) {
        window.removeEventListener("mousemove", TreeView.spMouseMove);
        window.removeEventListener("mouseup", TreeView.spMouseUp);
        splitter.addEventListener("mousedown", TreeView.spMouseDown);
        TreeView.resetPosition(last_x);
    },
    spMouseMove: function (e) {
        TreeView.resetPosition(e.clientX);
    },
    resetPosition: function (nowX) {
        var dx = nowX - last_x;
        dx += cont1.clientWidth;
        cont1.style.width = dx + "px";
        splitter.style.marginLeft = dx + "px";
        dx += splitter.clientWidth;
        cont2.style.marginLeft = dx + "px";
        dx = window_width - dx;
        cont2.style.width = (dx /*- (270 + 375)*/) + "px";
        last_x = nowX;
    },

    // treeview
    FormatTree: function (a) {
        var tree = [],
            mappedArr = {},
            arrElem,
            mappedElem;
        for (var i = 0, len = a.length; i < len; i++) {
            arrElem = a[i];
            //let code = a[i].codeTypeDirectory !== null && a[i].codeTypeDirectory.length > 1 ? a[i].codeTypeDirectory : "";
            //arrElem.text = "<div><span>" + a[i].name + "</span><span>" + code +"</span></div>";
            arrElem.text = a[i].name;
            if (arrElem.type == "folder") {
                arrElem.icon = "fa fa-folder"
            }
            if (arrElem.type == "file") {
                arrElem.icon = "fa fa-database"
            }
            arrElem.id = a[i].id;
            mappedArr[arrElem.id] = arrElem;
            mappedArr[arrElem.id]['nodes'] = [];
        }


        for (var id in mappedArr) {
            if (mappedArr.hasOwnProperty(id)) {
                mappedElem = mappedArr[id];
                if (mappedElem.parentId && typeof mappedArr[mappedElem['parentId']] !== "undefined") {
                    if (typeof mappedArr[mappedElem['parentId']]['nodes'] !== "undefined") {
                        mappedArr[mappedElem['parentId']]['nodes'] = [];
                    }
                    mappedArr[mappedElem['parentId']]['nodes'].push(mappedElem);
                }
                else {
                    tree.push(mappedElem);
                }
            }
        }
        return tree;

    },
    ReturnInforItemTree: function (item, keyword) {
        if (keyword == "") {
            $('#tree').bstreeview({ data: JSON.stringify(json) });
        } else if (item.text.toLowerCase().includes(keyword)) {
            $('#tree').bstreeview({ data: JSON.stringify([item]) });
            return item;
        } else if (item.nodes.length != 0) {
            var i, j, k;
            var result = null;
            for (i = 0; result == null && i < item.nodes.length; i++) {
                if (item.nodes[i].text.toLowerCase().includes(keyword)) {
                    result = item.nodes[i];
                }
                if (item.nodes[i].nodes.length != 0) {
                    for (j = 0; result == null && j < item.nodes[i].nodes.length; j++) {
                        if (item.nodes[i].nodes[j].text.toLowerCase().includes(keyword)) {
                            result = item.nodes[i].nodes[j];
                        }
                        if (item.nodes[i].nodes[j].nodes.length != 0) {
                            for (k = 0; result == null && k < item.nodes[i].nodes[j].nodes.length; k++) {
                                if (item.nodes[i].nodes[j].nodes[k].text.toLowerCase().includes(keyword)) {
                                    result = item.nodes[i].nodes[j].nodes[k];
                                }
                                if (item.nodes[i].nodes[j].nodes[k].nodes.length != 0) {
                                    for (z = 0; result == null && z < item.nodes[i].nodes[j].nodes[k].nodes.length; z++) {
                                        if (item.nodes[i].nodes[j].nodes[k].nodes[z].text.toLowerCase().includes(keyword)) {
                                            result = item.nodes[i].nodes[j].nodes[k].nodes[z];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //result = ReturnInforItemTree(item.nodes[i], keyword);
            }
            $('#tree').bstreeview({ data: JSON.stringify([result]) });
            return result;
        }
        return null;
    },
    GetInforTreeView: function () {
        $(TreeView.SELECTORS.listGroupItem).on('click', function () {
            if ($('#InforData').width() == 0) {
                $('#InforData').animate({
                    width: 372,
                }, 200);
            }

            $(TreeView.SELECTORS.listGroupItem).css('background-color', 'white');
            var idSelected = thisId;
            let id = $(this).attr('id');
            $('#' + id).css('background-color', '#dee2e6');
            model = listFolder.find(x => x.id == id);
            if (model != null) {
                thisId = model.id;
                parentId = model.id;
                level = model.level + 1;
            }
            var type = $(this).attr('data-type');
            if (type == "file") {
                TreeView.setHideOrShowFolder(true);
                config.GLOBAL.idDirectory = id;
                $.ajax({
                    type: "GET",
                    url: '/api/HTKT/PropertiesDirectory/get-directoryById',
                    data: {
                        id: config.GLOBAL.idDirectory
                    },
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.code == "ok") {
                            $('#DataName').val(data.result.nameTypeDirectory);
                            $('#h5NameData').html(l("Layer:Name") + ": " + data.result.nameTypeDirectory);
                            $('#DataCode').val(data.result.codeTypeDirectory);
                            $('#Version').val(data.result.verssionTypeDirectory);
                            if (data.result.location == null) {
                                $('#DataLat').val("");
                                $('#DataLng').val("");
                            }
                            else {
                                $('#DataLat').val(data.result.location.lat);
                                $('#DataLng').val(data.result.location.lng);
                            }

                            if (data.result.zoom != null) {
                                $('#DataZoom').val(data.result.zoom);
                            }
                            else {
                                $('#DataZoom').val("");
                            }

                            if (data.result.minZoom != null) {
                                $('#MinZoom').val(data.result.minZoom);
                            }
                            else {
                                $('#MinZoom').val("");
                            }

                            if (data.result.maxZoom != null) {
                                $('#MaxZoom').val(data.result.maxZoom);
                            }
                            else {
                                $('#MaxZoom').val("");
                            }

                            let nameImplement = listFolder.find(x => x.id == data.result.idImplement);
                            nameImplement = typeof nameImplement !== "undefined" && nameImplement !== null ? nameImplement.name + " (" + nameImplement.codeTypeDirectory + ")" : ""
                            $(TreeView.SELECTORS.inputnameImplement).val(nameImplement);
                            $(TreeView.SELECTORS.inputUrlMaptile).val(data.result.groundMaptile);
                            var tags = data.result.tags;

                            if (tags != null) {
                                $(TreeView.SELECTORS.inputBoundMaptile).val(tags["bounds"]);
                            }
                            else {
                                //$(TreeView.SELECTORS.inputBoundMaptile).val(bounds);
                                $(TreeView.SELECTORS.inputBoundMaptile).val("");
                            }

                            var result = [];
                            $.each(data.result.listProperties, function (i, obj) {
                                var element = {
                                    NameProperties: obj.nameProperties,
                                    CodeProperties: obj.codeProperties,
                                    TypeProperties: obj.typeProperties,
                                    IsShow: obj.isShow,
                                    IsIndexing: obj.isIndexing,
                                    IsRequest: obj.isRequest,
                                    IsView: obj.isView,
                                    IsHistory: obj.isHistory,
                                    IsInheritance: obj.isInheritance,
                                    IsAsync: obj.isAsync,
                                    IsShowExploit: obj.isShowExploit,
                                    IsShowSearchExploit: obj.isShowSearchExploit,
                                    IsShowReference: obj.isShowReference,
                                    DefalutValue: obj.defalutValue,
                                    ShortDescription: obj.shortDescription,
                                    TypeSystem: obj.typeSystem,
                                    OrderProperties: obj.orderProperties
                                };
                                result.push(element);
                            });
                            config.GLOBAL.table = result;
                            config.addTbodyTr();
                            config.setEventAffter();
                        }
                        else {
                            console.log(data)
                        }
                    },
                    error: function () {
                        alert("Lỗi ajax");
                    }
                });
            }
            else {
                TreeView.setHideOrShowFolder(false);
                config.resetTable();
            }
            //    }
            //}
            if ($(config.SELECTORS.collapseOne).hasClass("collapse show")) {
                $(config.SELECTORS.collapseOne).removeClass('show');
                $(config.SELECTORS.iconHeadingOne).html(`<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="width:24px; height:24px;transform: translate(-1%, 4%);"
	                                                            viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                                                        <style type="text/css">
	                                                        .st0{fill:#FFFFFF;}
                                                        </style>
                                                        <path id="Path_6613" class="st0" d="M4.24,1.93h16c0.55,0,1,0.45,1,1v16c0,0.55-0.45,1-1,1h-16c-0.55,0-1-0.45-1-1v-16
	                                                        C3.24,2.37,3.69,1.93,4.24,1.93z M5.24,3.93v14h14v-14H5.24z M7.24,9.93h10v2h-10V9.93z M13.24,5.93v10h-2v-10H13.24z"/>
                                                        </svg>`);
                //$(config.SELECTORS.iconHeadingOne).removeClass('fa-minus').addClass('fa-plus');
            }
            if (!$(config.SELECTORS.collapseTwo).hasClass("collapse show")) {
                $(config.SELECTORS.collapseTwo).addClass('show');
                $(config.SELECTORS.iconHeadingTwo).html(`<svg id="Component_212_2" data-name="Component 212 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="width:24px; height:24px;">
                                                        <path id="Path_6612" data-name="Path 6612" d="M0,0H24V24H0Z" fill="none"></path>
                                                        <path id="Path_6613" data-name="Path 6613" d="M4,3H20a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H4a1,1,0,0,1-1-1V4A1,1,0,0,1,4,3ZM5,5V19H19V5Zm2,6H17v2H7Z" fill="#fff"></path>
                                                    </svg>`);
                //$(config.SELECTORS.iconHeadingTwo).removeClass('fa-plus').addClass('fa-minus');
            }
        });
    },
    delay: function (callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    },
    ReloadTree: function (id = '') {
        $.ajax({
            type: "GET",
            url: TreeView.CONSTS.URL_GETLISTDIRECTORY,
            data: {
            },
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    listFolder = data;
                    json = TreeView.FormatTree(data);
                    item = json[0];
                    $("#tree").remove();
                    $("#divTree").append('<div id="tree"></div>');
                    //$('#tree').bstreeview({ data: JSON.stringify(json) });
                    TreeView.GetInforTreeView();
                    if (id == '') {
                        $('#' + item.id).trigger('click');
                    } else {
                        var array = [];
                        array.push(id);
                        var lst = listFolder.find(x => x.id == id);
                        if (lst != undefined) {
                            idtemporary = lst.parentId;
                            var level = lst.level;
                            for (var i = level - 1; i >= 0; i--) {
                                var lst2 = listFolder.find(x => x.id == idtemporary);
                                if (lst2 != undefined) {
                                    array.push(lst2.id);
                                    idtemporary = lst2.parentId;
                                }
                            }
                        }
                        for (var i = array.length - 1; i >= 0; i--) {
                            $('#' + array[i]).trigger('click');
                        };
                    }
                }
                else {
                    console.log(res);
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    AddPermissionLayerOrFolder: function (id) {
        var array = [];
        array.idDirectory = id;
        array.listPermisson = [];
        for (var i = 0; i < PermissionLayer.GLOBAL.ArrayPermission.length; i++) {
            array.listPermisson.push(PermissionLayer.GLOBAL.ArrayPermission[i].keyName)
        }
        return array;
    },
    GetChildByParentId: function (id) {
        var lst = listFolder.filter(x => x.parentId == id);
        return lst;
    },
    GetAllLayerIdByIdParent: function (id) {
        var lst = TreeView.GetChildByParentId(id);
        var lstId = [];
        $.each(lst, function (i, obj) {
            if (obj.type == "folder") {
                lstForlerDelete.push(obj);
                var node = obj.nodes;
                for (var i = 0; i < node.length; i++) {
                    if (node[i].type == "folder") {
                        lstForlerDelete.push(node[i]);
                        var lstchild = TreeView.GetAllLayerIdByIdParent(node[i].id);
                        if (lstchild.length > 0) {
                            lstId.concat(lstchild);
                        }
                    } else if (node[i].type == "file") {
                        lstId.push(node[i].id);
                    }
                }
            } else if (obj.type == "file") {
                lstId.push(obj.id);
            }
        });
        return lstId;
    },
    getListLayerDataType: function () {
        $.ajax({
            type: "GET",
            url: '/api/HTKT/LayerDataType/get-list',
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok" && data.result != null && data.result.length > 0) {
                    config.GLOBAL.select = [];
                    $.each(data.result, function (i, obj) {
                        config.GLOBAL.select.push({ values: obj.value, text: obj.text });
                    });
                }
                $('.spinner').css('display', 'none');
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    setHideOrShowFolder: function (check) {
        if (check) {
            $(TreeView.SELECTORS.addFolder).attr("disabled", true);
        } else {
            $(TreeView.SELECTORS.addFolder).removeAttr("disabled");
        }
    },
    getTreeView: function (id) {
        let viewtree = "";
        if (listFolder.length > 0) {
            let parentid = null;
            let obj = listFolder.find(x => x.id == id);
            if (typeof obj != "undefined" && obj != null) {
                parentid = obj.parentId;
                viewtree = obj.name;
            }
            while (parentid != null && parentid != "") {
                obj = listFolder.find(x => x.id == parentid);
                if (typeof obj != "undefined" && obj != null) {
                    parentid = obj.parentId;
                    viewtree = obj.name + " > " + viewtree;
                }
            }
        }
        return viewtree;
    },

    //Layer
    getAllLayerforClone: function () {
        if (listFolder.length > 0) {
            $(TreeView.SELECTORS.layerdata).children().remove();
            //let html = '<option value="0" data-select2-id="3">---chọn lớp---</option>';
            let listLayer = TreeView.GLOBAL.arrayDirectoryClone;
            var option = new Option("", "0", false, false);
            $(TreeView.SELECTORS.layerdata).append(option).trigger('change');
            $.each(listLayer, function (i, obj) {

                let name = obj.nameTypeDirectory + ' (' + obj.codeTypeDirectory + ')';
                //html = html + name;
                var option = new Option(name, obj.id, false, false);
                $(TreeView.SELECTORS.layerdata).append(option).trigger('change');
            });

            $(TreeView.SELECTORS.layerdata).parent().find('.placeholder').attr('style', "");
            //$(TreeView.SELECTORS.layerdata).append(html);
        }
    },
    CloneLayeredForCurrentLayer: function (dto) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: TreeView.CONSTS.URL_CLONEPROPERTIESDIRECTORY,
            async: true,
            data: JSON.stringify(dto),
            success: function (re) {
                abp.ui.clearBusy(TreeView.SELECTORS.modalLayer);
                //console.log(re);
                if (re.code === "ok") {
                    abp.notify.success(l("Layer:CreateDataLayerSuccessfully"));

                    TreeView.GLOBAL.lstIdLayer.push(re.result.idDirectory);
                    let arr = TreeView.AddPermissionLayerOrFolder(re.result.idDirectory);
                    TreeView.GLOBAL.lstLayerPermission.push(arr);
                    TreeLayer.GetListLayer(re.result.idDirectory);
                    // tạo phân quyền
                    TreeView.AddPermissionUserCreateFolder(re.result.idDirectory);

                    TreeView.CloseContentSection();

                    TreeView.GetDirectoryCloneOption(); // reset comboobx layer kế thừa
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                abp.ui.clearBusy(TreeView.SELECTORS.modalLayer);
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },

    //Property Setting
    UpdateProperty: function (type) {
        // Upload file
        var formData = new FormData();
        if (type == "info") {
            formData.append("IdDirectory", config.GLOBAL.idDirectory);
            if (typeof TreeView.GLOBAL.propertyData !== "undefined" && TreeView.GLOBAL.propertyData !== null && TreeView.GLOBAL.propertyData !== "") {
                formData.append("MinZoom", TreeView.GLOBAL.propertyData.minZoom);
                formData.append("MaxZoom", TreeView.GLOBAL.propertyData.maxZoom);
                formData.append("Stroke", TreeView.GLOBAL.propertyData.stroke);
                formData.append("StrokeWidth", TreeView.GLOBAL.propertyData.strokeWidth);
                formData.append("StrokeOpacity", TreeView.GLOBAL.propertyData.strokeOpacity);
                formData.append("StyleStroke", TreeView.GLOBAL.propertyData.styleStroke);
                formData.append("Fill", TreeView.GLOBAL.propertyData.fill);
                formData.append("FillOpacity", TreeView.GLOBAL.propertyData.fillOpacity);
            } else {
                formData.append("MinZoom", $('#min-zoom').val() >= "0" ? $('#min-zoom').val() : "0");
                formData.append("MaxZoom", $('#max-zoom').val() >= "0" ? $('#max-zoom').val() : "22");
                formData.append("Stroke", $('#strokeinput').val());
                formData.append("StrokeWidth", $('#stroke-width').val() >= "0" ? $('#stroke-width').val() : "1");
                formData.append("StrokeOpacity", $('#rangeMax-strokeopacity').val() / 100);
                formData.append("StyleStroke", $('#style-stroke').val());
                formData.append("Fill", $('#fillInput').val());
                formData.append("FillOpacity", $('#rangeMax-fillOpactity').val() / 100);
            }

            if ($(datainfo.SELECTORS.fileImage)[0].files.length > 0) {
                let url = datainfo.SendFileImage($(datainfo.SELECTORS.fileImage)[0].files[0]);
                if (url != null) {
                    var urlMap = url.urlMap;
                    formData.append("Image2D", urlMap);
                    formData.append("ImageIcon", url.urlIcon);
                    if (typeof TreeView.GLOBAL.propertyData !== "undefined" && TreeView.GLOBAL.propertyData !== null && TreeView.GLOBAL.propertyData !== "") {
                        // xoa image cũ
                        var oldImage2D = TreeView.GLOBAL.propertyData.image2D;
                        var oldImageIcon = "";
                        if (TreeView.GLOBAL.propertyData.tags != null) {
                            oldImageIcon = TreeView.GLOBAL.propertyData.tags["icon"];
                        }
                        var deleteimage = datainfo.DeleteFileImage(oldImage2D, oldImageIcon);
                    }
                }
            } else {
                formData.append("Image2D", TreeView.GLOBAL.propertyData != null ? TreeView.GLOBAL.propertyData.image2D : "");
                if (TreeView.GLOBAL.propertyData.tags != undefined && TreeView.GLOBAL.propertyData.tags != null && TreeView.GLOBAL.propertyData.tags != "null") {
                    formData.append("ImageIcon", TreeView.GLOBAL.propertyData.tags.icon);
                }
                else {
                    formData.append("ImageIcon", "");
                }
            }

            if ($(datainfo.SELECTORS.inputObj)[0].files.length > 0) {
                url = datainfo.SendFileObj($(datainfo.SELECTORS.inputObj)[0].files[0]);
                formData.append("Object3D", url);
            } else {
                formData.append("Object3D", TreeView.GLOBAL.propertyData != null ? (TreeView.GLOBAL.propertyData.object3D != "undefined" && TreeView.GLOBAL.propertyData.object3D != "null") ? TreeView.GLOBAL.propertyData.object3D : "" : "");
            }

            if ($(datainfo.SELECTORS.file3D)[0].files.length > 0) {
                url = datainfo.SendFileImage($(datainfo.SELECTORS.file3D)[0].files[0]);
                formData.append("Texture3D", url.urlMap);
                var deleteimage = datainfo.DeleteFileImage("", url.urlIcon);
            } else {
                formData.append("Texture3D", TreeView.GLOBAL.propertyData != null ? TreeView.GLOBAL.propertyData.texture3D : "");
            }

        } else {
            formData.append("IdDirectory", config.GLOBAL.idDirectory);
            formData.append("MinZoom", $('#min-zoom').val());
            formData.append("MaxZoom", $('#max-zoom').val());
            formData.append("Stroke", $('#strokeinput').val());
            formData.append("StrokeWidth", $('#stroke-width').val());
            formData.append("StrokeOpacity", $('#rangeMax-strokeopacity').val() / 100);
            formData.append("StyleStroke", $('#style-stroke').val());
            formData.append("Fill", $('#fillInput').val());
            formData.append("FillOpacity", $('#rangeMax-fillOpactity').val() / 100);

            if (typeof TreeView.GLOBAL.propertyData !== "undefined" && TreeView.GLOBAL.propertyData !== null && TreeView.GLOBAL.propertyData !== "") {
                formData.append("Image2D", TreeView.GLOBAL.propertyData.image2D);
                formData.append("Object3D", TreeView.GLOBAL.propertyData.object3D);
                formData.append("Texture3D", TreeView.GLOBAL.propertyData.texture3D);
                if (TreeView.GLOBAL.propertyData.tags != undefined && TreeView.GLOBAL.propertyData.tags != null) {
                    formData.append("ImageIcon", TreeView.GLOBAL.propertyData.tags.icon);
                }
            } else {
                let url = datainfo.SendFileImage($(datainfo.SELECTORS.fileImage)[0].files[0]);
                if (url != null) {
                    var urlMap = url.urlMap;
                    formData.append("Image2D", urlMap);
                    formData.append("ImageIcon", url.urlIcon);

                    // xoa image cũ
                    var oldImage2D = TreeView.GLOBAL.propertyData.image2D;
                    var oldImageIcon = "";
                    if (TreeView.GLOBAL.propertyData.tags != null) {
                        oldImageIcon = TreeView.GLOBAL.propertyData.tags["icon"];
                    }

                    var deleteimage = datainfo.DeleteFileImage(oldImage2D, oldImageIcon);
                }
                url = datainfo.SendFileObj($(datainfo.SELECTORS.inputObj)[0].files[0]);
                formData.append("Object3D", url);
                url = datainfo.SendFileImage($(datainfo.SELECTORS.file3D)[0].files[0]);
                formData.append("Texture3D", url);
            }
        }

        $.ajax({
            type: "POST",
            async: false,
            processData: false,
            contentType: false,
            url: "/api/HTKT/PropertiesSetting/update",
            data: formData,
            success: function (data) {
                $('.toggle-detail-property').trigger('click');
                $(TreeView.SELECTORS.inputFileOject3D).val("");
                if (data.code == "ok") {
                    TreeView.GLOBAL.propertyData = data.result;

                    abp.notify.success(l("Layer:LayerSettingSuccessully"));
                }
                else {
                    abp.notify.error(l("Layer:SaveDataFailed"));
                }

                abp.ui.clearBusy(TreeView.SELECTORS.modal_setting_layer);

                TreeView.CloseContentSection();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error update: " + errorThrown)
                $('.toggle-detail-property').trigger('click');
                abp.ui.clearBusy(TreeView.SELECTORS.modal_setting_layer);
            }
        });


    },
    getProperty: function () {
        var urls = window.location.origin + '/api/HTKT/PropertiesSetting/getByIdDictionary';
        $.ajax({
            type: "GET",
            url: urls,
            data: {
                id: config.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json",
            success: function (data) {
                if (data.code == "ok") {
                    if (data.result != null) {
                        TreeView.GLOBAL.propertyData = null;

                        $('#min-zoom').val(data.result.minZoom);
                        $('#max-zoom').val(data.result.maxZoom);

                        $('#strokeText').val(data.result.stroke);
                        $('#strokeinput').val(data.result.stroke);
                        $('#strokeColor').css('background', data.result.stroke);

                        $('#stroke-width').val(data.result.strokeWidth);

                        //do mo vien
                        const newValue1 = parseInt(data.result.strokeOpacity * 100);
                        $('#thumbMax-strokeopacity').css('left', newValue1 + '%');
                        $('#max-strokeopacity').html(newValue1 + '.00%');
                        $('#line-strokeopacity').css({
                            'right': (100 - newValue1) + '%'
                        });
                        $('#rangeMax-strokeopacity').val(newValue1);
                        $('#style-stroke').val(data.result.styleStroke == null || data.result.styleStroke == "" ? "straightline" : data.result.styleStroke);

                        $('#fillText').val(data.result.fill);
                        $('#fillInput').val(data.result.fill);
                        $('#fillColor').css('background', data.result.fill);

                        //do mo cua vung
                        const newValue2 = parseInt(data.result.fillOpacity * 100);
                        $('#thumbMax-fillOpactity').css('left', newValue2 + '%');
                        $('#max-fillOpactity').html(newValue2 + '.00%');
                        $('#line-fillOpactity').css({
                            'right': (100 - newValue2) + '%'
                        });
                        $('#rangeMax-fillOpactity').val(newValue2);

                        $(datainfo.SELECTORS.img_avatar).attr('src', data.result.image2D !== null && data.result.image2D !== "null" ? data.result.image2D : "/images/anhMacDinh.png");
                        $(datainfo.SELECTORS.img_3D).attr('src', data.result.texture3D !== null && data.result.texture3D !== "null" ? data.result.texture3D : "/images/anhMacDinh.png");

                        if (data.result.object3D != null && data.result.object3D != "" && data.result.object3D != "null" && data.result.object3D != "undefined") {
                            var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${data.result.object3D}" data-toggle="tooltip" data-placement="bottom">${data.result.object3D}</div>
                            <div class="action-file-obj" title="Xóa" data-toggle="tooltip" data-placement="bottom"><img src="/images/close_cricle.svg" /></div>`; // hiển thị tên file đã được chọn
                            $(TreeView.SELECTORS.file_object_content).html(html);
                            $(TreeView.SELECTORS.file_object_content).show();
                        }
                        else {
                            $(TreeView.SELECTORS.file_object_content).hide();
                            $(TreeView.SELECTORS.inputFileOject3D).val(null).trigger("change");
                        }

                        $(datainfo.SELECTORS.nameObject).attr('src', data.result.object3D !== null && data.result.object3D !== "null" ? data.result.object3D : "");
                        $(datainfo.SELECTORS.nameObject).html(data.result.object3D !== null && data.result.object3D !== "null" ? data.result.object3D : "");
                        TreeView.GLOBAL.propertyData = data.result;
                    }
                    else {
                        $('#min-zoom').val(TreeView.GLOBAL.propertiesSetting.MinZoom);
                        $('#max-zoom').val(TreeView.GLOBAL.propertiesSetting.MaxZoom);

                        $('#strokeText').val(TreeView.GLOBAL.propertiesSetting.Stroke);
                        $('#strokeinput').val(TreeView.GLOBAL.propertiesSetting.Stroke);
                        $('#strokeColor').css('background', TreeView.GLOBAL.propertiesSetting.Stroke);

                        $('#stroke-width').val(TreeView.GLOBAL.propertiesSetting.StrokeWidth);

                        //do mo vien
                        const newValue1 = parseInt(TreeView.GLOBAL.propertiesSetting.StrokeOpacity * 100);
                        $('#thumbMax-strokeopacity').css('left', newValue1 + '%');
                        $('#max-strokeopacity').html(newValue1 + '.00%');
                        $('#line-strokeopacity').css({
                            'right': (100 - newValue1) + '%'
                        });
                        $('#rangeMax-strokeopacity').val(newValue1);
                        $('#style-stroke').val(TreeView.GLOBAL.propertiesSetting.StyleStroke == null || TreeView.GLOBAL.propertiesSetting.StyleStroke == "" ? "straightline" : TreeView.GLOBAL.propertiesSetting.StyleStroke);

                        $('#fillText').val(TreeView.GLOBAL.propertiesSetting.Fill);
                        $('#fillInput').val(TreeView.GLOBAL.propertiesSetting.Fill);
                        $('#fillColor').css('background', TreeView.GLOBAL.propertiesSetting.Fill);

                        //do mo cua vung
                        const newValue2 = parseInt(TreeView.GLOBAL.propertiesSetting.FillOpacity * 100);
                        $('#thumbMax-fillOpactity').css('left', newValue2 + '%');
                        $('#max-fillOpactity').html(newValue2 + '.00%');
                        $('#line-fillOpactity').css({
                            'right': (100 - newValue2) + '%'
                        });
                        $('#rangeMax-fillOpactity').val(newValue2);
                        $(datainfo.SELECTORS.img_avatar).attr('src', "/images/anhMacDinh.png");
                        $(datainfo.SELECTORS.img_3D).attr('src', "/images/anhMacDinh.png");
                        $(datainfo.SELECTORS.nameObject).attr('src', "");
                        $(datainfo.SELECTORS.nameObject).html('');
                        $(TreeView.SELECTORS.file_object_content).hide();
                        $(TreeView.SELECTORS.inputFileOject3D).val(null).trigger("change");
                        TreeView.GLOBAL.propertyData = {
                            id: "",
                            idDirectory: config.GLOBAL.idDirectory,
                            fill: TreeView.GLOBAL.propertiesSetting.Fill,
                            fillOpacity: TreeView.GLOBAL.propertiesSetting.FillOpacity,
                            image2D: TreeView.GLOBAL.propertiesSetting.Image2D,
                            maxZoom: TreeView.GLOBAL.propertiesSetting.MaxZoom,
                            minZoom: TreeView.GLOBAL.propertiesSetting.MinZoom,
                            stroke: TreeView.GLOBAL.propertiesSetting.Stroke,
                            strokeOpacity: TreeView.GLOBAL.propertiesSetting.StrokeOpacity,
                            strokeWidth: TreeView.GLOBAL.propertiesSetting.StrokeWidth,
                            styleStroke: TreeView.GLOBAL.propertiesSetting.StyleStroke,
                            object3D: "",
                            texture3D: "",
                            tags: null
                        };
                    }
                }
            },
        });
    },

    // Add permission user
    AddPermissionUserCreateFolder: function (idDirectory) {
        var lstKeyPermission = [];
        var lstUser = [];

        for (var i = 0; i < PermissionLayer.GLOBAL.ArrayPermission.length; i++) {
            var objPermisson = {
                keyName: PermissionLayer.GLOBAL.ArrayPermission[i].keyName
            };

            lstKeyPermission.push(objPermisson);
        }

        var usePermission = {
            id: currentUserId,
            listPermission: lstKeyPermission
        }

        lstUser.push(usePermission);

        var dto = {
            userPermission: lstUser,
            rolePermission: [],
            idDirectory: idDirectory,
        };

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(dto),
            async: true,
            url: PermissionLayer.CONSTS.URL_SAVE_PERMISSION_LAYER,
            success: function (data) {

            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    },

    // kiểm tra xóa thư mục và layer
    CheckDeleteLayer: function (id) {
        var result = true;
        $.ajax({
            type: "GET",
            url: TreeView.CONSTS.URL_CHECK_DELETE,
            data: {
                id: id
            },
            async: false,
            contentType: "application/json",
            success: function (res) {
                if (res.code == "ok") {
                    result = res.result;
                }
                else {
                    result = false;
                    console.log(res)
                }
            },
        });

        return result;
    },

    // xóa thư mục và layer
    DeleteFolderLayer: function (id, type) {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: "/api/htkt/directory/delete-folder",
            data: { id: id },
            success: function (res) {
                if (res.code == "ok" && res.result) {
                    var dto2 = TreeView.GetAllLayerIdByIdParent(id);
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: "/api/HTKT/PropertiesDirectory/delete",
                        data: JSON.stringify(dto2),
                        success: function (result) {
                            if (result.code == "ok") {
                                if (type === "file") {
                                    abp.notify.success(l("Layer:DeleteDataLayerSuccessfully"));
                                }
                                else {
                                    abp.notify.success(l("Layer:DeleteFolderSuccessfully"));
                                }
                            }
                            else {
                                if (type === "file") {
                                    abp.notify.error(l("Layer:DeleteDataLayerFailed"));
                                } else {
                                    abp.notify.error(l("Layer:DeleteFolderFailed"));
                                }
                            }

                            TreeView.CloseContentSection();

                            var parentid = "";
                            //var oldActivity = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == id);
                            //if (oldActivity != undefined) {
                            //    parentid = oldActivity.parentId;
                            //}

                            let pathName = document.location.pathname.split('?')[0];
                            history.replaceState(null, null, pathName);
                            $('#div-parent-tree').click();
                            TreeLayer.GetListLayer(parentid);
                        },
                        error: function () {
                            TreeView.CloseContentSection();
                        }
                    });
                } else {
                    abp.notify.error(l("Layer:DeleteFolderFailed"));
                    TreeView.CloseContentSection();
                }

                TreeView.GetDirectoryCloneOption(); // reset comboobx layer kế thừa

            },
            error: function () {
                TreeView.CloseContentSection();
            }
        });
    },

    // update name thư mục
    UpdateNameFolder: function (id) {
        var dto = {
            id: id,
            name: $(TreeView.SELECTORS.inputNameFolder).val()
        }
        $.ajax({
            type: "POST",
            url: TreeView.CONSTS.URL_UPDATE_NAME_FOLDER,
            data: JSON.stringify(dto),
            contentType: "application/json",
            beforeSend: function () {
                abp.ui.setBusy(TreeView.SELECTORS.modalFolder);
                $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').hide();
            },
            success: function (data) {
                if (data.code == "ok") {

                    abp.notify.success(l("Layer:UpdateNameFolderSuccessfully"));

                } else {
                    abp.notify.error(l("Layer:UpdateNameFolderFailed"));
                }

                TreeLayer.GetListLayer(id);
                TreeView.CloseContentSection();
            },
            complete: function () {
                abp.ui.clearBusy(TreeView.SELECTORS.modalFolder);
                $(TreeView.SELECTORS.modalFolder).find('.toogle-content-section').show();
            }
        });
    },

    GetDirectoryCount: function () {
        $.ajax({
            type: "POST",
            url: TreeView.CONSTS.URL_GETLISTDIRECTORY_COUNT,
            data: JSON.stringify([]),
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //console.log(data);
                if (data.code == "ok") {
                    var text = '';
                    let order = 1;

                    var lstDataCount = data.result;

                    if (TVisAdmin.toLowerCase() == "false") // tài khoản đăng nhập không phải admin
                    {
                        lstDataCount = [];
                        var checkDataPermission = data.result.filter(x => TVlstIdLayer.some(y => y == x.id)); // lấy danh sách lớp được phân quyền theo tài khoản
                        if (checkDataPermission != undefined && checkDataPermission.length < 10) {
                            var dem = 10 - checkDataPermission.length;

                            $.each(checkDataPermission, function (index, obj) {
                                lstDataCount.push({
                                    id: obj.id,
                                    idDirectory: obj.id,
                                    image2D: obj.image2D,
                                    isDeleted: obj.isDeleted,
                                    level: obj.level,
                                    name: obj.name
                                });
                            });

                            $.each(TreeLayer.GLOBAL.ArrayTree, function (index, obj) {
                                if (dem == 0) {
                                    return;
                                }

                                if (obj.type.toLowerCase() != "folder") {
                                    var checkDuplicate = lstDataCount.find(x => x.id == obj.id); // kiểm tra danh sach hiển thị đã co hay chưa
                                    if (checkDuplicate == undefined) {
                                        var directory = TVlstIdLayer.find(x => x == obj.id);
                                        if (directory != undefined) {
                                            lstDataCount.push({
                                                id: obj.id,
                                                idDirectory: obj.id,
                                                image2D: obj.image2D,
                                                isDeleted: obj.isDeleted,
                                                level: obj.level,
                                                name: obj.name
                                            });
                                            dem--;
                                        }
                                    }
                                }
                            });
                        }
                    }

                    $.each(lstDataCount, function (i, obj) {

                        var directory = obj;

                        if (directory != undefined) {
                            var classname = "";
                            if (order <= 5) {
                                classname += " div-border-bottom"
                            }
                            if (order % 5 !== 0) {
                                classname += " div-border-right"
                            }


                            var arr = [];
                            var perMission = TreeView.GLOBAL.lstLayerPermission.find(x => x.idDirectory == directory.id);
                            if (perMission != null && perMission != undefined) {
                                arr = perMission.listPermisson;
                            }

                            text += `<div class="item-count-ten div-border ${classname} position-relative" data-id="${directory.id}" data-name="${directory.name}" 
                                            data-create="${arr.includes("PermissionLayer.CREATE")}"
                                            data-edit="${arr.includes("PermissionLayer.EDIT")}"
                                            data-delete="${arr.includes("PermissionLayer.DELETE")}"
                                            data-permission="${arr.includes("PermissionLayer.PERMISSION")}">
                                        <div class="content-layer">
                                            ${obj.image2D != "" && obj.image2D != null ?
                                    `<img src="${obj.image2D}" class="image-avatar">` :
                                    `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lightning-fill svg-avatar" viewBox="0 0 16 16">
                                                            <path d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                                        </svg>`}
                                            <div class="name-avatar">
                                                <span style="font-weight: 500;">${directory.name}</span>
                                            </div>
                                        </div>
                                </div>`;
                            $(".file-upload-content").html('');
                            $(".file-upload-content").append(text);
                            order++;
                        }
                    });
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    CloseContentSection: function () {
        if (TreeView.GLOBAL.isAction > -1) {
            $(TreeView.SELECTORS.content_section).removeClass('open');
            $(TreeView.SELECTORS.black_drop_content).hide();
            $(TreeView.SELECTORS.action_menu).removeClass('isForcus');

            var directory = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == TreeLayer.GLOBAL.DirectoryForcus); // tìm directory đang được focus
            TreeView.GLOBAL.isAction = -1;
            if (directory != undefined) // nếu ko có thì click vào thư mục gốc
            {
                if (!directory.type || directory.type == "false" || directory.type == "folder") // kiểm tra có phải là thư mục hay k
                {
                    TreeView.DisabledActionForFolder(directory.level);
                }
                else {
                    TreeView.DisabledActionForLayer();
                }
            }
            else {
                $(TreeLayer.SELECTORS.divParentTree).click();
            }
            TreeView.ResetIconMenu();
        }
    },
    ClearInputFormAddLayer: function () {
        $(TreeView.SELECTORS.modalLayer + ' input[name="Name-Layer"]').val("");
        $(TreeView.SELECTORS.modalLayer + ' input[name="Lng-Layer"]').val("");
        $(TreeView.SELECTORS.modalLayer + ' input[name="Lat-Layer"]').val("");
        $(TreeView.SELECTORS.modalLayer + ' input[name="Zoom-Layer"]').val("");
        $(TreeView.SELECTORS.inputBoundsMaptileLayer).val("");
        $(TreeView.SELECTORS.radiosLayer + 'input[value="1"]').trigger('click');
        $(TreeView.SELECTORS.radiosLayerMap).prop("checked", false);
        TreeView.GLOBAL.checkRadiosLayerMap = false;
        //$(TreeView.SELECTORS.inputGroundMaptileLayer).prop("readonly", true);
        $(TreeView.SELECTORS.inputGroundMaptileLayer).val("");
        $(TreeView.SELECTORS.optionCreatLayer + 'input[value="1"]').trigger('click');
        // $(TreeView.SELECTORS.radiosLayerMap).prop("checked", false);

        $(TreeView.SELECTORS.inputBoundsMaptileLayer).val(bounds);
        //$(TreeView.SELECTORS.inputBoundsMaptileLayer).prop("readonly", true);
    },

    // những button layer ko được dùng
    DisabledActionForLayer: function () {
        if (TreeView.GLOBAL.isAction == -1) {
            $(TreeView.SELECTORS.action_menu).each(function () {
                var type = $(this).attr('data-type');
                switch (type) {
                    case "1": // thêm thư mục
                        $(this).prop('disabled', true);
                        $(this).addClass('disabled');
                        break;
                    case "2": // sửa thư mục
                        $(this).prop('disabled', true);
                        $(this).addClass('disabled');
                        break;
                    case "3": // thêm mới layer
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "4": // cấu hình
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "5": // phan quyền
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "6": // xóa
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    default:
                }
            })
        }
    },

    // những button folder ko được dùng
    DisabledActionForFolder: function (level) {
        if (TreeView.GLOBAL.isAction == -1) {
            $(TreeView.SELECTORS.action_menu).each(function () {
                var type = $(this).attr('data-type');
                switch (type) {
                    case "1": // thêm thư mục
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "2": // sửa thư mục
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "3": // thêm mới layer
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "4": // cấu hình
                        $(this).prop('disabled', true);
                        $(this).addClass('disabled');
                        break;
                    case "5": // phan quyền
                        $(this).prop('disabled', false);
                        $(this).removeClass('disabled');
                        break;
                    case "6": // xóa
                        if (level > 0) {
                            $(this).prop('disabled', false);
                            $(this).removeClass('disabled');
                        }
                        else {
                            $(this).prop('disabled', true);
                            $(this).addClass('disabled');
                        }
                        break;
                    default:
                }
            })
        }
    },

    // kiểm tra action layer có hợp lệ
    CheckActionLayer: function () {
        var check = true;
        switch (TreeView.GLOBAL.isAction) {
            case 1: // thêm mới thư mục
                check = false;
                break;
            case 2: // sửa thư mục
                check = false;
                break;
            case 3: //thêm mới layer
                break;
            case 4: // setting layer
                break;
            case 5: // phân quyền
                break;
            case 6: //xóa
                break;
            default:
        }

        return check;
    },

    // kiếm tra action folder có hợp lệ
    CheckActionFolder: function () {
        var check = true;
        switch (TreeView.GLOBAL.isAction) {
            case 1: // thêm mới thư mục
                break;
            case 2: // sửa thư mục
                break;
            case 3: //thêm mới layer
                break;
            case 4: // setting layer
                check = false;
                break;
            case 5: // phân quyền
                break;
            case 6: //xóa
                break;
            default:
        }

        return check;
    },

    // kiểm tra action hiện tại so với phân quyền của lớp
    CheckPermission: function (_create, _edit, _delete, _permission, _type) {
        var check = true;
        if (_type == false || _type == "false") {
            switch (TreeView.GLOBAL.isAction) {
                case 1: // thêm mới thư mục
                    check = _create;
                    break;
                case 2: // sửa thư mục
                    check = _edit;
                    break;
                case 3: //thêm mới layer
                    check = _create;
                    break;
                case 4: // setting layer
                    check = _edit;
                    break;
                case 5: // phân quyền
                    check = _permission;
                    break;
                case 6: //xóa
                    check = _delete;
                    break;
                default:
            }
        }

        if (check == false || check == "false") {
            check = false
        }

        if (check == true || check == "true") {
            check = true
        }


        if (!check) {
            if (_type == false || _type == "false") {
                swal({
                    title: l('ManagementLayer:Notification'),
                    text: l('Bạn không có quyển sử dụng chức năng tại thư mục/lớp này'),
                    icon: "warning",
                    button: l('ManagementLayer:Close'),
                });
            }
        }

        return check;
    },

    ResetAction: function () {
        switch (TreeView.GLOBAL.isAction) {
            case 1: // thêm mới folder
                break;
            case 2: // sửa tên foler
                $(TreeView.SELECTORS.inputNameFolder).val("");
                var folder = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == TreeLayer.GLOBAL.DirectoryForcus);
                if (folder != undefined) {
                    $(TreeView.SELECTORS.inputNameFolder).val(folder.name);
                }
                break;
            case 3: //thêm mới layer
                break;
            case 4: // cập nhật cấu hình layer
                break;
            case 5: // phân quyền
                PermissionLayer.GetPermissionDirectory(); // hiển thị lại phân quyền
                break;
            case 6: // xóa đối tương hoặc thư mục
                break;
            default:
        }
    },
    // reset icon menu
    ResetIconMenu: function () {
        $(TreeView.SELECTORS.action_menu).each(function () {
            var type = $(this).attr('data-type');
            switch (type) {
                case "1":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                <path id="Path_6541" data-name="Path 6541" class="b" d="M12.414,5H21a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414ZM4,5V19H20V7H11.586l-2-2Zm7,7V9h2v3h3v2H13v3H11V14H8V12Z" transform="translate(-2 -3)" fill="rgba(0,0,0,0.56)"></path>
                            </svg>`);
                    break;
                case "2":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="18" height="17.971" viewBox="0 0 18 17.971">
                                        <path id="Path_6611"  class="b" data-name="Path 6611" d="M5,19H6.414l9.314-9.314L14.314,8.272,5,17.586Zm16,2H3V16.757L16.435,3.322a1,1,0,0,1,1.414,0l2.829,2.829a1,1,0,0,1,0,1.414L9.243,19H21ZM15.728,6.858l1.414,1.414,1.414-1.414L17.142,5.444,15.728,6.858Z" transform="translate(-3 -3.029)" fill="rgba(0,0,0,0.56)"></path>
                                    </svg>`);
                    break;
                case "3": // thêm mới layer
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <defs>
                            </defs>
                            <path class="a" d="M0,0H24V24H0Z" style="fill:none;"></path>
                            <path class="b" style="fill: rgba(0,0,0,0.56);" d="M15,4H5V20H19V8H15ZM3,2.992A1,1,0,0,1,4,2H16l5,5V20.993A1,1,0,0,1,20.007,22H3.993A1,1,0,0,1,3,21.008ZM11,11V8h2v3h3v2H13v3H11V13H8V11Z"></path>
                        </svg>`);
                    break;
                case "4": // cấu hình
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path id="Path_33"  class="b" data-name="Path 33" d="M2,12a10.036,10.036,0,0,1,.316-2.5A3,3,0,0,0,4.99,4.867a9.99,9.99,0,0,1,4.335-2.5,3,3,0,0,0,5.348,0,9.99,9.99,0,0,1,4.335,2.5A3,3,0,0,0,21.683,9.5a10.075,10.075,0,0,1,0,5.007,3,3,0,0,0-2.675,4.629,9.99,9.99,0,0,1-4.335,2.505,3,3,0,0,0-5.348,0A9.99,9.99,0,0,1,4.99,19.133,3,3,0,0,0,2.315,14.5,10.056,10.056,0,0,1,2,12Zm4.8,3a5,5,0,0,1,.564,3.524,8.007,8.007,0,0,0,1.3.75,5,5,0,0,1,6.67,0,8.007,8.007,0,0,0,1.3-.75,5,5,0,0,1,3.334-5.774,8.126,8.126,0,0,0,0-1.5,5,5,0,0,1-3.335-5.774,7.989,7.989,0,0,0-1.3-.75,5,5,0,0,1-6.669,0,7.99,7.99,0,0,0-1.3.75A5,5,0,0,1,4.034,11.25a8.126,8.126,0,0,0,0,1.5A4.993,4.993,0,0,1,6.805,15ZM12,15a3,3,0,1,1,3-3A3,3,0,0,1,12,15Zm0-2a1,1,0,1,0-1-1A1,1,0,0,0,12,13Z" transform="translate(0)" fill="rgba(0,0,0,0.56)"></path>
                                </svg>`);
                    break;
                case "5": // phan quyền
                    $(this).html(`<svg id="Group_2870" data-name="Group 2870" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path id="Path_6550" data-name="Path 6550" d="M0,0H24V24H0Z" fill="none"></path>
                                    <path id="Path_6551"  class="b" data-name="Path 6551" d="M12,14v2a6,6,0,0,0-6,6H4a8,8,0,0,1,8-8Zm0-1a6,6,0,1,1,6-6A6,6,0,0,1,12,13Zm0-2A4,4,0,1,0,8,7,4,4,0,0,0,12,11Zm2.6,7.812a3.51,3.51,0,0,1,0-1.623l-.992-.573,1-1.732.992.573A3.5,3.5,0,0,1,17,14.645V13.5h2v1.145a3.492,3.492,0,0,1,1.405.812l.992-.573,1,1.732-.992.573a3.51,3.51,0,0,1,0,1.622l.992.573-1,1.732-.992-.573a3.5,3.5,0,0,1-1.4.812V22.5H17V21.355a3.5,3.5,0,0,1-1.4-.812l-.992.573-1-1.732.992-.572ZM18,19.5A1.5,1.5,0,1,0,16.5,18,1.5,1.5,0,0,0,18,19.5Z" fill="rgba(0,0,0,0.56)"></path>
                                </svg>`);
                    break;
                case "6":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <defs>
                                </defs>
                                <path class="a" d="M0,0H24V24H0Z" style="fill:none;"></path>
                                <path class="b" style="fill: rgba(0,0,0,0.56);" d="M20,7V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H2V5H22V7ZM6,7V20H18V7ZM7,2H17V4H7Zm4,8h2v7H11Z"></path>
                            </svg>`);
                    break;
                default:
            }
        })
    },

    // danh sách layer clone vào select option
    GetDirectoryCloneOption: function () {
        $.ajax({
            type: "GET",
            url: TreeView.CONSTS.URL_GET_LIST_DIRECTORY_CLONE,
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //console.log(data);
                if (data.code == "ok") {
                    TreeView.GLOBAL.arrayDirectoryClone = data.result;
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
}

function CheckedHtmlEntities(form) {
    var check = true;
    $(`${form} input[type=text]`).each(function () {
        var value = $(this).val();
        let inputName = $(this).attr('data-name');
        var status = $(this).attr("data-required");
        if (status != undefined) {
            status = status.toLowerCase()
        }

        if ($(this).val() != ""/* && status == "true"*/) {
            let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;
            if (re.test(value)) {
                $(this).parents(".form-group-modal").find('.lable-error').remove();
                insertError($(this), "other");
                TreeView.showLabelError(this, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), inputName));
                check = false;
            }
        }
    });
    return check;
}