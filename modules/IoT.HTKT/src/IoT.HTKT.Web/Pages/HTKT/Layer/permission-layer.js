﻿var l = abp.localization.getResource('HTKT');
var maxResultSelect2 = 10;
var PermissionLayer = {
    GLOBAL: {
        ArrayUser: [],
        ArrayRole: [],
        isDrawSelect2: false,
        ArrayPermission: [],
        UserIdFocus: "",
        RoleIdFocus: "",
        DataImplement: null
    },
    CONSTS: {
        //URL_GET_LIST_USER: "/api/identity/users",
        //URL_GET_LIST_ROLE: "/api/identity/roles",
        URL_GET_LIST_USER: "/api/permissionApp/manager_identity/get-list-user",
        URL_GET_LIST_ROLE: "/api/permissionApp/manager_identity/get-list-role",
        URL_GET_LIST_PERMISSION_LAYER: "/api/permissionApp/permission-layer/get-list-all",
        URL_SAVE_PERMISSION_LAYER: "/api/permissionApp/layer_user_permission/create-many",
        URL_GET_PERMISSION_LAYER: "/api/permissionApp/layer_user_permission/get-permission-layer-by-id",
        URL_GET_PERMISSION_LAYER_IMPLEMENT: "/api/permissionApp/layer_user_permission/get-permission-layer-implement-by-id"
    },
    SELECTORS: {
        modalPermisison: "#permissionModal",
        search_radio: "input[name='search-modal-radio']",
        btn_add_permission: "#addPermissionLayer",
        user_selected: "#user-selected",
        role_selected: "#role-selected",
        content_user_list: "#user-list-selected",
        content_role_list: "#role-list-selected",
        content_permission_layer: ".list-permission-layer",
        check_permission: ".check-permission",
        check_box_implement_parent: "#isImplement",
        btn_save_permission: ".save-permission",
        remove_user: ".remove-user",
        remove_role: ".remove-role",
        user_item_list: ".user-item-selected",
        role_item_list: ".role-item-selected"
    },
    init: function () {
        PermissionLayer.setEvent();
        PermissionLayer.GetListUser();
        PermissionLayer.GetListRole();
        PermissionLayer.GetListPermissionLayer();

    },
    setEvent: function () {
        // radio button tìm kiếm theo
        $(PermissionLayer.SELECTORS.search_radio).on('change', function () {
            if ($(this).val() == "ROLE") {
                PermissionLayer.GLOBAL.UserIdFocus = "";
                $(".body-user").addClass('hidden');
                $(".body-role").removeClass('hidden');
            }
            else {
                $(".body-user").removeClass('hidden');
                $(".body-role").addClass('hidden');
                PermissionLayer.GLOBAL.RoleIdFocus = "";
            }

            PermissionLayer.ResetFocus();
            PermissionLayer.CustomSearchSelected();
        });

        // btn lưu phân quyền
        $(PermissionLayer.SELECTORS.btn_save_permission).on('click', function () {
            if (TreeLayer.GLOBAL.DirectoryForcus != "") {
                PermissionLayer.SavePermissionToLayer();
            }
        })

        // checkbox thừa kế phân quyền
        $(PermissionLayer.SELECTORS.check_box_implement_parent).on('change', function () {
            //if (PermissionLayer.GLOBAL.UserIdFocus != "" || PermissionLayer.GLOBAL.RoleIdFocus != "")
            {
                if ($(this).is(':checked')) {
                    var currentLayer = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == TreeLayer.GLOBAL.DirectoryForcus);
                    var parent = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == currentLayer.parentId);
                    if (parent != undefined /*&& parent.level != 0*/) {
                        PermissionLayer.GetPermissionImplementDirectory(parent.id);
                    }
                }
                else {
                    PermissionLayer.GLOBAL.DataImplement = null;
                }

                //PermissionLayer.ResetFocus();
            }
        })

        // xóa user ra khỏi phân quyền
        $(PermissionLayer.SELECTORS.content_user_list).on('click', PermissionLayer.SELECTORS.remove_user, function () {
            var id = $(this).attr('data-id');
            PermissionLayer.GLOBAL.ArrayUser = jQuery.grep(PermissionLayer.GLOBAL.ArrayUser, function (value) {
                return value.id != id;
            });
            $(PermissionLayer.SELECTORS.user_selected).val(JSON.stringify(PermissionLayer.GLOBAL.ArrayUser.map(x => x.id)));
            $(this).parent().remove();
        });

        // xóa role ra khỏi phân quyền
        $(PermissionLayer.SELECTORS.content_role_list).on('click', PermissionLayer.SELECTORS.remove_role, function () {
            var id = $(this).attr('data-id');
            PermissionLayer.GLOBAL.ArrayRole = jQuery.grep(PermissionLayer.GLOBAL.ArrayRole, function (value) {
                return value.id != id;
            });
            $(PermissionLayer.SELECTORS.role_selected).val(JSON.stringify(PermissionLayer.GLOBAL.ArrayRole.map(x => x.id)));
            $(this).parent().remove();
        });

        // chọn phân quyền
        $(PermissionLayer.SELECTORS.content_permission_layer).on('change', PermissionLayer.SELECTORS.check_permission, function () {
            if ($(PermissionLayer.SELECTORS.search_radio + ":checked").val() == "USER") {
                if (PermissionLayer.GLOBAL.UserIdFocus == "") {
                    swal({
                        title: l('ManagementLayer:Notification'),
                        text: l('Bạn phải chọn tài khoản trong danh sách để phân quyền'),
                        icon: "error",
                        button: l('ManagementLayer:Close'),
                    });

                    $(this).prop('checked', false);
                }
            }
            else {
                if (PermissionLayer.GLOBAL.RoleIdFocus == "") {
                    swal({
                        title: l('ManagementLayer:Notification'),
                        text: l('Bạn phải chọn vai trò trong danh sách để phân quyền'),
                        icon: "error",
                        button: l('ManagementLayer:Close'),
                    });

                    $(this).prop('checked', false);
                }
            }

            var key = $(this).attr('id');
            if (PermissionLayer.GLOBAL.UserIdFocus != "") {
                var userPermission = PermissionLayer.GLOBAL.ArrayUser.find(x => x.id == PermissionLayer.GLOBAL.UserIdFocus); // kiểm tra user có nằm trong ds đã lưu
                if (userPermission != undefined) {
                    if ($(this).is(':checked')) {
                        var objPermission = {
                            keyName: key
                        }

                        userPermission.listPermission.push(objPermission);
                    }
                    else {
                        userPermission.listPermission = jQuery.grep(userPermission.listPermission, function (permission) {
                            return permission.keyName != key;
                        });
                    }
                }
            }

            if (PermissionLayer.GLOBAL.RoleIdFocus != "") {
                var rolePermission = PermissionLayer.GLOBAL.ArrayRole.find(x => x.id == PermissionLayer.GLOBAL.RoleIdFocus); // kiểm tra user có nằm trong ds đã lưu
                if (rolePermission != undefined) {
                    if ($(this).is(':checked')) {
                        var objPermission = {
                            keyName: key
                        }

                        rolePermission.listPermission.push(objPermission);
                    }
                    else {
                        rolePermission.listPermission = jQuery.grep(rolePermission.listPermission, function (permission) {
                            return permission.keyName != key;
                        });
                    }
                }
            }
        })

        // click user đã có trong danh sác
        $(PermissionLayer.SELECTORS.content_user_list).on('click', PermissionLayer.SELECTORS.user_item_list, function () {
            // xét lại forcus của role
            PermissionLayer.ResetFocus();

            /********************************************/
            var id = $(this).attr('data-id');
            $(this).addClass('isForcus');

            var userPermission = PermissionLayer.GLOBAL.ArrayUser.find(x => x.id == id); // kiểm tra user có nằm trong ds đã lưu
            if (userPermission != undefined) {
                PermissionLayer.GLOBAL.UserIdFocus = id; // gán user focus

                $(PermissionLayer.SELECTORS.check_permission).each(function () // checked quyền đã có
                {
                    $(this).prop('checked', false);
                    if (userPermission.listPermission.find(x => x.keyName == $(this).attr('id')) != undefined) {
                        $(this).prop('checked', true);
                    }
                });
            }
        })

        // click role đã có trong danh sách
        $(PermissionLayer.SELECTORS.content_role_list).on('click', PermissionLayer.SELECTORS.role_item_list, function () {
            // xét lại forcus của user
            PermissionLayer.ResetFocus();

            /********************************************/
            var id = $(this).attr('data-id');
            $(this).addClass('isForcus');

            var rolePermission = PermissionLayer.GLOBAL.ArrayRole.find(x => x.id == id); // kiểm tra role có nằm trong ds đã lưu
            if (rolePermission != undefined) {
                PermissionLayer.GLOBAL.RoleIdFocus = id; // gán role focus

                $(PermissionLayer.SELECTORS.check_permission).each(function () // checked quyền đã có
                {
                    $(this).prop('checked', false);
                    if (rolePermission.listPermission.find(x => x.keyName == $(this).attr('id')) != undefined) {
                        $(this).prop('checked', true);
                    }
                });
            }
        })

    },
    GetListUser: function () {
        let branch_all = [];

        function formatResult(state) {
            if (!state.id) {
                var btn = $('<div class="text-right"></div>')
                return btn;
            }

            var isSelected = PermissionLayer.GLOBAL.ArrayUser.findIndex(x => x.id == state.id);
            var classElement = "user";

            branch_all.push(state.id);
            var id = 'state' + state.id;
            //var checkbox = $('<div class="checkbox"><input id="' + id + '" class="check-' + classElement + '" type="checkbox" ' + (isSelected > -1 ? 'checked' : '') + '><label for="checkbox1">' + state.text + '</label></div>', { id: id });
            var checkbox = $(`<div class="custom-checkbox custom-control checkbox">
                                <input type="checkbox" data-val="true" id="${id}" name="Name_${id}" value="true" class="custom-control-input check-${classElement}" ${isSelected > -1 ? 'checked' : ''}>
                                <label class="custom-control-label" for="Name_${id}">${state.text}</label>
                            </div>`);
            return checkbox;
        }

        function formatState(state) {
            setTimeout(function () {
                PermissionLayer.CustomSearchSelected();
            }, 10)
            return '';
        };

        var url = PermissionLayer.CONSTS.URL_GET_LIST_USER;

        PermissionLayer.GLOBAL.isDrawSelect2 = true;
        let optionSelect2 = {
            templateResult: formatResult,
            templateSelection: formatState,
            closeOnSelect: false,
            /*placeholder: l('ChoosenUser'),*/
            width: '100%',
            ajax: {
                url: url,
                async: true,
                data: function (params) {
                    if (params.page == undefined) {
                        params.page = 1;
                    }
                    var query = {
                        Filter: params.term != undefined ? params.term.replace(/\s\s+/g, ' ').trim() : "",
                        SkipCount: (params.page - 1) * maxResultSelect2 || 0,
                        MaxResultCount: maxResultSelect2
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (res, params) {
                    if (res.code == "ok") {
                        var data = res.result;
                        if (data != null || data != undefined) {
                            data.items = data.items.filter(x => x.userName != "admin");
                            let results = data.items.map((item, index) => {
                                return {
                                    id: item.id,
                                    text: `${item.name} (${item.userName})`,
                                }
                            });

                            var more = true;
                            if (params.page != undefined) {
                                more = ((params.page * maxResultSelect2) < data.totalCount ? true : false);
                            }

                            return {
                                results: results,
                                pagination: {
                                    more: more
                                }
                            };
                        }
                    }
                    else {
                        console.log(res)
                        return null;
                    }
                },
                cache: true
            }
        };

        let $select2 = $(PermissionLayer.SELECTORS.user_selected).select2(optionSelect2);

        $select2.on("select2:selecting", function (event) {
        });

        $select2.on("select2:unselect", function (event) {
            $(PermissionLayer.SELECTORS.modalPermisison + ' .select2-selection').css('border', '1px solid var(--primary)');
        });

        $select2.on("select2:select", function (event) {
            // thêm recored vào lis trụ
            /*  CreateHoatDongTruAnten.AddElementListTru(event.params.data.id, event.params.data.text);*/
            var indexExits = PermissionLayer.GLOBAL.ArrayUser.findIndex(x => x.id == event.params.data.id);

            if (indexExits == -1) {
                var objUser = {
                    id: event.params.data.id,
                    name: event.params.data.text,
                    listPermission: []
                };
                PermissionLayer.GLOBAL.ArrayUser.push(objUser);

                PermissionLayer.AddElementUser(event.params.data.id, event.params.data.text);
            }

            // check trạng thái nút kế thừa
            if ($(PermissionLayer.SELECTORS.check_box_implement_parent).is(':checked')) {
                var currentLayer = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == TreeLayer.GLOBAL.DirectoryForcus);

                var parent = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == currentLayer.parentId);
                if (parent != undefined) {
                    PermissionLayer.GetPermissionImplementDirectory(parent.id);
                }
            }

            $("#state" + event.params.data.id).prop('checked', true);

            $(PermissionLayer.SELECTORS.modalPermisison + ' .select2-selection').css('border', '1px solid var(--primary)');
        });

        $select2.on("select2:unselecting", function (event) {
            var $pr = $('#' + event.params.args.data._resultId).parent();

            PermissionLayer.GLOBAL.ArrayUser = jQuery.grep(PermissionLayer.GLOBAL.ArrayUser, function (value) {
                return value.id != event.params.args.data.id;
            });

            PermissionLayer.RemoveElementUser(event.params.args.data.id);
            scrollTop = $pr.prop('scrollTop');
            PermissionLayer.ResetFocus();
            $("#state" + event.params.args.data.id).prop('checked', false);
        });

        $select2.on("select2:opening", function (event) {
            $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff; color: var(--primary);");
        });

        $select2.on("select2:closing", function (event) {

            $('.select2-results__options').html('');

            $(PermissionLayer.SELECTORS.modalPermisison + ' .select2-selection').css('border', '');
            $(this).parent().find('.placeholder').attr('style', "");
        });

        $(document).on('change', '.check-user', function () {
            if ($(this).prop('checked')) {
                $(this).prop("checked", false)
            } else {
                $(this).prop("checked", true)
            }
        })
    },
    GetListRole: function () {
        let branch_all = [];

        function formatResult(state) {
            if (!state.id) {
                var btn = $('<div class="text-right"></div>')
                return btn;
            }

            var isSelected = PermissionLayer.GLOBAL.ArrayRole.findIndex(x => x.id == state.id);
            var classElement = "role";

            branch_all.push(state.id);
            var id = 'state' + state.id;
            //var checkbox = $('<div class="checkbox"><input id="' + id + '" class="check-' + classElement + '" type="checkbox" ' + (isSelected > -1 ? 'checked' : '') + '><label for="checkbox1">' + state.text + '</label></div>', { id: id });
            var checkbox = $(`<div class="custom-checkbox custom-control checkbox">
                                <input type="checkbox" data-val="true" id="${id}" name="Name_${id}" value="true" class="custom-control-input check-${classElement}" ${isSelected > -1 ? 'checked' : ''}>
                                <label class="custom-control-label" for="Name_${id}">${state.text}</label>
                            </div>`);
            return checkbox;
        }

        function formatState(state) {

            setTimeout(function () {
                PermissionLayer.CustomSearchSelected();
            }, 10)
            return '';
        };

        var url = PermissionLayer.CONSTS.URL_GET_LIST_ROLE;
        PermissionLayer.GLOBAL.isDrawSelect2 = true;
        let optionSelect2 = {
            templateResult: formatResult,
            templateSelection: formatState,
            closeOnSelect: false,
            //placeholder: l('ChoosenRole'),
            width: '100%',
            ajax: {
                url: url,
                async: true,
                data: function (params) {
                    if (params.page == undefined) {
                        params.page = 1;
                    }

                    var query = {
                        Filter: params.term != undefined ? params.term.replace(/\s\s+/g, ' ').trim() : "",
                        SkipCount: (params.page - 1) * maxResultSelect2 || 0,
                        MaxResultCount: maxResultSelect2
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (res, params) {
                    if (res.code == "ok") {
                        var data = res.result;
                        if (data != null || data != undefined) {
                            data.items = data.items.filter(x => x.name != "admin");
                            let results = data.items.map((item, index) => {
                                return {
                                    id: item.id,
                                    text: `${item.name}`,
                                }
                            });

                            var more = true;
                            if (params.page != undefined) {
                                more = ((params.page * maxResultSelect2) < data.totalCount ? true : false);
                            }

                            return {
                                results: results,
                                pagination: {
                                    more: more
                                }
                            };
                        }
                    }
                    else {
                        console.log(res);
                        return null;
                    }
                },
                cache: true
            }
        };

        let $select2 = $(PermissionLayer.SELECTORS.role_selected).select2(optionSelect2);

        $select2.on("select2:selecting", function (event) {

        });

        $select2.on("select2:unselect", function (event) {
            $(PermissionLayer.SELECTORS.modalPermisison + ' .select2-selection').css('border', '1px solid var(--primary)'); // set lại place hover label
        });

        $select2.on("select2:select", function (event) {
            // thêm recored vào lis trụ
            /*  CreateHoatDongTruAnten.AddElementListTru(event.params.data.id, event.params.data.text);*/

            var rolePermission = PermissionLayer.GLOBAL.ArrayRole.find(x => x.id == event.params.data.id);
            if (rolePermission == undefined) {
                var objRolePermission = {
                    id: event.params.data.id,
                    name: event.params.data.text,
                    listPermission: []
                };

                PermissionLayer.GLOBAL.ArrayRole.push(objRolePermission);
            }
            PermissionLayer.AddElementRole(event.params.data.id, event.params.data.text);

            // check trạng thái nút kế thừa
            if ($(PermissionLayer.SELECTORS.check_box_implement_parent).is(':checked')) {
                var currentLayer = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == TreeLayer.GLOBAL.DirectoryForcus);

                var parent = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == currentLayer.parentId);
                if (parent != undefined) {
                    PermissionLayer.GetPermissionImplementDirectory(parent.id);
                }
            }

            $("#state" + event.params.data.id).prop('checked', true);

            $(PermissionLayer.SELECTORS.modalPermisison + ' .select2-selection').css('border', '1px solid var(--primary)'); // set lại place hover label
        });

        $select2.on("select2:unselecting", function (event) {
            var $pr = $('#' + event.params.args.data._resultId).parent();

            PermissionLayer.GLOBAL.ArrayRole = jQuery.grep(PermissionLayer.GLOBAL.ArrayRole, function (value) {
                return value.id != event.params.args.data.id;
            });

            PermissionLayer.RemoveElementRole(event.params.args.data.id);

            scrollTop = $pr.prop('scrollTop');

            PermissionLayer.ResetFocus();

            $("#state" + event.params.args.data.id).prop('checked', false);

        });

        $select2.on("select2:opening", function (event) {
            $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff; color: var(--primary);"); // set lại place hover label
        });

        $select2.on("select2:closing", function (event) {
            $('.select2-results__options').html('');

            $(PermissionLayer.SELECTORS.modalPermisison + ' .select2-selection').css('border', ''); // set lại place hover label
            $(this).parent().find('.placeholder').attr('style', "");
        });

        $(document).on('change', '.check-role', function () {
            if ($(this).prop('checked')) {
                $(this).prop("checked", false)
            } else {
                $(this).prop("checked", true)
            }
        })
    },
    AddElementUser: function (id, text) {
        if (currentUserId != "") // không hiển thị user đang đăng nhập
        {
            if (id != currentUserId) {
                var html = `<li id="user_selected_${id}" class="user-item-selected" data-id="${id}">
                                <span class="name">${text}</span>
                                <span data-id="${id}" class="remove-user">
                                    <svg id="Group_2839" data-name="Group 2839" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                        <path id="Path_6512" data-name="Path 6512" d="M0,0H24V24H0Z" fill="none"/>
                                        <path id="Path_6513" data-name="Path 6513" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-11.414L9.172,7.757,7.757,9.172,10.586,12,7.757,14.828l1.415,1.415L12,13.414l2.828,2.829,1.415-1.415L13.414,12l2.829-2.828L14.828,7.757Z" fill="var(--primary)"/>
                                    </svg>
                                </span>
                             </li>`;
            }
        }

        $(PermissionLayer.SELECTORS.content_user_list).append(html);

        $(PermissionLayer.SELECTORS.user_item_list).removeClass('isForcus');
        $(`#user_selected_${id}`).addClass('isForcus');

        PermissionLayer.GLOBAL.UserIdFocus = id;
        $(PermissionLayer.SELECTORS.check_permission).prop('checked', false);

        if ($(PermissionLayer.SELECTORS.check_box_implement_parent).is(":checked")) // nếu checkbox kế thừa đang check thì click vào user
        {
            setTimeout(function () {
                $(`#user_selected_${id}`).trigger('click');
            }, 500)
        }
    },
    RemoveElementUser: function (id) {
        $(PermissionLayer.SELECTORS.user_item_list).each((index, element) => {
            var idtru = $(element).attr("data-id");
            if (idtru == id) {
                $(element).remove();
                return;
            }

            //$(document).on("click", "#all-branch", function () {
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected} > option`).not(':first').prop("selected", true);// Select All Options
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected}`).trigger("change")
            //    $(".select2-results__option").not(':first').attr("aria-selected", true);
            //    $("[id^=state]").prop("checked", true);
            //    //$(window).scroll();
            //});

            //$(document).on("click", "#clear-branch", function () {
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected} > option`).not(':first').prop("selected", false);
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected}`).trigger("change");
            //    $(".select2-results__option").not(':first').attr("aria-selected", false);
            //    $("[id^=state]").prop("checked", false);
            //    //$(window).scroll();
            //});
        });
    },
    AddElementRole: function (id, text) {
        var html = `   <li id="role_selected_${id}" class="role-item-selected" data-id="${id}">
                            <span class="name">${text}</span>
                            <span data-id="${id}" class="remove-role">
                                <svg id="Group_2839" data-name="Group 2839" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                    <path id="Path_6512" data-name="Path 6512" d="M0,0H24V24H0Z" fill="none"/>
                                    <path id="Path_6513" data-name="Path 6513" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-11.414L9.172,7.757,7.757,9.172,10.586,12,7.757,14.828l1.415,1.415L12,13.414l2.828,2.829,1.415-1.415L13.414,12l2.829-2.828L14.828,7.757Z" fill="rgba(0,0,0,0.38)"/>
                                </svg>
                            </span>
                        </li>`;

        $(PermissionLayer.SELECTORS.content_role_list).append(html);

        $(PermissionLayer.SELECTORS.role_item_list).removeClass('isForcus');
        $(`#role_selected_${id}`).addClass('isForcus');

        PermissionLayer.GLOBAL.RoleIdFocus = id; // forcus vào
        $(PermissionLayer.SELECTORS.check_permission).prop('checked', false); // set lại checkbox permission

        if ($(PermissionLayer.SELECTORS.check_box_implement_parent).is(":checked")) // nếu checkbox kế thừa đang check thì click vào role
        {
            setTimeout(function () {
                $(`#role_selected_${id}`).trigger('click');
            }, 500)
        }
    },
    RemoveElementRole: function (id) {
        $('.role-item-selected').each((index, element) => {
            var idtru = $(element).attr("data-id");
            if (idtru == id) {
                $(element).remove();
                return;
            }

            //$(document).on("click", "#all-branch", function () {
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected} > option`).not(':first').prop("selected", true);// Select All Options
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected}`).trigger("change")
            //    $(".select2-results__option").not(':first').attr("aria-selected", true);
            //    $("[id^=state]").prop("checked", true);
            //    //$(window).scroll();
            //});

            //$(document).on("click", "#clear-branch", function () {
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected} > option`).not(':first').prop("selected", false);
            //    $(`${CreateHoatDongTruAnten.SELECTORS.TruIdSelected}`).trigger("change");
            //    $(".select2-results__option").not(':first').attr("aria-selected", false);
            //    $("[id^=state]").prop("checked", false);
            //    //$(window).scroll();
            //});
        });
    },
    CustomSearchSelected: function () {
        $('.select2-selection__rendered .select2-selection__choice').remove();
        $('.select2-selection--multiple .select2-selection__rendered').after('');
        //var hole = "";
        //if ($(`${PermissionLayer.SELECTORS.search_radio}:checked`).val() == "ROLE") {
        //    hole = l('ChoosenRole');
        //}
        //else {
        //    hole = l('ChoosenUser');
        //}
        //$('.select2-search__field').attr('placeholder', hole);
    },
    ResetModal: function () {
        PermissionLayer.GLOBAL.ArrayRole = [];
        PermissionLayer.GLOBAL.ArrayUser = [];
        $(PermissionLayer.SELECTORS.content_user_list).html('');
        $(PermissionLayer.SELECTORS.content_role_list).html('');

        $(PermissionLayer.SELECTORS.check_permission).prop('checked', false);
        $(PermissionLayer.SELECTORS.check_box_implement_parent).prop('checked', false);

        $('#USER_Radio').click();

        PermissionLayer.GLOBAL.DataImplement = null;
    },
    ResetFocus: function () {
        PermissionLayer.GLOBAL.RoleIdFocus = "";
        PermissionLayer.GLOBAL.UserIdFocus = "";

        $(PermissionLayer.SELECTORS.role_item_list).removeClass('isForcus');
        $(PermissionLayer.SELECTORS.user_item_list).removeClass('isForcus');
        $(PermissionLayer.SELECTORS.check_permission).prop('checked', false);
        //$(PermissionLayer.SELECTORS.check_box_implement_parent).prop('checked', false);
    },
    GetListPermissionLayer: function () {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: PermissionLayer.CONSTS.URL_GET_LIST_PERMISSION_LAYER,
            data: {
                search: ""
            },
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    if (data != null && data != undefined) {
                        var html = ``;
                        $(PermissionLayer.SELECTORS.content_permission_layer).html(html);
                        var lst = data.items;
                        PermissionLayer.GLOBAL.ArrayPermission = lst;

                        lst.sort(function (a, b) {
                            var nameA = a.keyName.toUpperCase(); // ignore upper and lowercase
                            var nameB = b.keyName.toUpperCase(); // ignore upper and lowercase
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }

                            // names must be equal
                            return 0;
                        });

                        var b = lst[1];
                        lst[1] = lst[2];
                        lst[2] = b;

                        for (var i = 0; i < lst.length; i++) {
                            html += `<div class="custom-checkbox custom-control mb-2">
                                    <input type="checkbox" data-val="true" id="${lst[i].keyName}" name="${lst[i].keyName}" value="true" class="custom-control-input check-permission">
                                    <label class="custom-control-label" for="${lst[i].keyName}">${lst[i].name}</label>
                                </div>`;
                        }

                        $(PermissionLayer.SELECTORS.content_permission_layer).html(html);
                    }
                }
                else {
                    console.log(res)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    },
    GetListPermissionLayerByUserCurrent: function (idDirectory) {
        if (idDirectory != "" && idDirectory != undefined) {
            if (TreeView.GLOBAL.lstLayerPermission.length > 0) {
                var layerUserPermission = TreeView.GLOBAL.lstLayerPermission.find(x => x.idDirectory == idDirectory);
                if (layerUserPermission != undefined) {
                    var $option = ``;
                    $(PermissionLayer.SELECTORS.content_permission_layer).html($option);
                    for (var i = 0; i < layerUserPermission.listPermisson.length; i++) {
                        var permission = PermissionLayer.GLOBAL.ArrayPermission.find(x => x.keyName == layerUserPermission.listPermisson[i]);
                        if (permission != undefined) {
                            $option += `<div class="custom-checkbox custom-control mb-2">
                                    <input type="checkbox" data-val="true" id="${permission.keyName}" name="${permission.keyName}" value="true" class="custom-control-input check-permission">
                                    <label class="custom-control-label" for="${permission.keyName}">${permission.name}</label>
                                </div>`;
                        }
                    }

                    $(PermissionLayer.SELECTORS.content_permission_layer).html($option);
                }
            }
        }
    },
    SavePermissionToLayer: function () {
        PermissionLayer.GLOBAL.ArrayUser = jQuery.grep(PermissionLayer.GLOBAL.ArrayUser, function (value) { // bo qua user login
            return value.id != currentUserId;
        });

        var dto = {
            userPermission: PermissionLayer.GLOBAL.ArrayUser,
            rolePermission: PermissionLayer.GLOBAL.ArrayRole,
            idDirectory: TreeLayer.GLOBAL.DirectoryForcus,
        };

        dto.userPermission = jQuery.grep(dto.userPermission, function (value) {
            return value.listPermission.length != 0;
        });

        dto.rolePermission = jQuery.grep(dto.rolePermission, function (value) {
            return value.listPermission.length != 0;
        });

        var layer = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == TreeLayer.GLOBAL.DirectoryForcus);
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(dto),
            beforeSend: function () {
                abp.ui.setBusy(PermissionLayer.SELECTORS.modalPermisison);
            },
            async: true,
            url: PermissionLayer.CONSTS.URL_SAVE_PERMISSION_LAYER,
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    if (data) {
                        abp.notify.success(l("Layer:AddLayerPermissionSuccess", layer != undefined ? layer.name : ""));

                    }
                    else {
                        abp.notify.error(l("Layer:AddLayerPermissionFailed", layer != undefined ? layer.name : ""));
                    }
                }
                TreeView.CloseContentSection();
            },
            complete: function () {
                abp.ui.clearBusy(PermissionLayer.SELECTORS.modalPermisison)
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    },
    GetPermissionDirectory: function (parentid) {
        PermissionLayer.ResetModal();
        if (parentid != undefined && PermissionLayer.GLOBAL.DataImplement != null) {
            PermissionLayer.setImplementParent(PermissionLayer.GLOBAL.DataImplement);
        }
        else {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                data: {
                    idDirectory: parentid != undefined ? parentid : TreeLayer.GLOBAL.DirectoryForcus
                },
                url: PermissionLayer.CONSTS.URL_GET_PERMISSION_LAYER,
                success: function (res) {
                    if (res.code == "ok") {
                        if (res.result != null) {
                            var data = res.result;

                            if (parentid == undefined) {
                                // set value current array user
                                PermissionLayer.GLOBAL.ArrayUser = data.listUser;

                                var $option = ``;
                                $(PermissionLayer.SELECTORS.user_selected).html($option);
                                for (var i = 0; i < data.listUser.length; i++) {
                                    PermissionLayer.AddElementUser(data.listUser[i].id, data.listUser[i].name);

                                    $option += ` <option value="${data.listUser[i].id}">
                                                ${data.listUser[i].name}
                                            </option>`;
                                }

                                $(PermissionLayer.SELECTORS.user_selected).html($option);
                                $(PermissionLayer.SELECTORS.user_selected).val([]);
                                if (data.listUser.length > 0) {
                                    $(PermissionLayer.SELECTORS.user_selected).val(data.listUser.map(x => x.id));
                                }


                                // set value current array role
                                PermissionLayer.GLOBAL.ArrayRole = data.listRole;
                                $option = ``;
                                $(PermissionLayer.SELECTORS.role_selected).html($option);

                                for (var i = 0; i < data.listRole.length; i++) {
                                    PermissionLayer.AddElementRole(data.listRole[i].id, data.listRole[i].name);
                                    $option += ` <option value="${data.listRole[i].id}">
                                                ${data.listRole[i].name}
                                        </option>`;
                                }

                                $(PermissionLayer.SELECTORS.role_selected).html($option);
                                $(PermissionLayer.SELECTORS.role_selected).val([]);
                                if (data.listRole.length > 0) {
                                    $(PermissionLayer.SELECTORS.role_selected).val(data.listRole.map(x => x.id));
                                }
                            }
                            else {
                                PermissionLayer.GLOBAL.DataImplement = data;
                                if (data.listUser.length == 0 && data.listRole.length == 0) {
                                    swal({
                                        title: l('ManagementLayer:Notification'),
                                        text: l('ParentKeyNameIsNull'),
                                        icon: "warning",
                                        button: l('ManagementLayer:Close'),
                                    });
                                }
                                else {
                                    PermissionLayer.setImplementParent(data);
                                }
                            }

                            PermissionLayer.ResetFocus();
                        }
                    }
                    else {
                        console.log(res);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
    },
    GetPermissionImplementDirectory: function (parentid) {
        if (parentid != undefined && PermissionLayer.GLOBAL.DataImplement != null) {
            PermissionLayer.setImplementParent(PermissionLayer.GLOBAL.DataImplement);
        }
        else {

            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                data: {
                    idDirectory: parentid
                },
                url: PermissionLayer.CONSTS.URL_GET_PERMISSION_LAYER_IMPLEMENT,
                success: function (res) {
                    if (res.code == "ok") {
                        if (res.result != null) {
                            var data = res.result;

                            PermissionLayer.GLOBAL.DataImplement = data;
                            if (data.listUser.length == 0 && data.listRole.length == 0) {
                                swal({
                                    title: l('ManagementLayer:Notification'),
                                    text: l('ParentKeyNameIsNull'),
                                    icon: "warning",
                                    button: l('ManagementLayer:Close'),
                                });
                            }
                            else {
                                PermissionLayer.setImplementParent(data);
                            }

                            //PermissionLayer.ResetFocus();
                        }
                    }
                    else {
                        console.log(res);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
    },
    setImplementParent: function (data) {
        // thêm phân quyền thừa kế cho từng user
        var lstUserCompare = data.listUser.filter(x => PermissionLayer.GLOBAL.ArrayUser.findIndex(y => y.id == x.id) != -1);
        if (lstUserCompare.length > 0) {
            for (var i = 0; i < lstUserCompare.length; i++) {
                var indexUserPermission = PermissionLayer.GLOBAL.ArrayUser.findIndex(x => x.id == lstUserCompare[i].id); // tìm user trong thư mục parent
                if (indexUserPermission != -1) {

                    var lstPermissionCurrent = PermissionLayer.GLOBAL.ArrayUser[indexUserPermission].listPermission; // quyền user hiện tại
                    var lstPermissionCompare = lstUserCompare[i].listPermission; // quyền user của thư mục parent

                    jQuery.grep(lstPermissionCompare, function (value) {
                        var dif = lstPermissionCurrent.find(x => x.keyName == value.keyName);
                        if (dif == undefined) {
                            lstPermissionCurrent.push(value);
                        }
                    });

                    PermissionLayer.GLOBAL.ArrayUser[indexUserPermission].listPermission = lstPermissionCurrent;
                }
            }
        }

        // thêm phân quyền thừa kế cho từng role
        var lstRoleCompare = data.listRole.filter(x => PermissionLayer.GLOBAL.ArrayRole.findIndex(y => y.id == x.id) != -1);
        if (lstRoleCompare.length > 0) {
            for (var i = 0; i < lstRoleCompare.length; i++) {
                var indexRolePermission = PermissionLayer.GLOBAL.ArrayRole.findIndex(x => x.id == lstRoleCompare[i].id); // tìm role trong thư mục parent
                if (indexRolePermission != -1) {

                    var lstPermissionCurrent = PermissionLayer.GLOBAL.ArrayRole[indexRolePermission].listPermission; // quyền role hiện tại
                    var lstPermissionCompare = lstRoleCompare[i].listPermission; // quyền role của thư mục parent

                    jQuery.grep(lstPermissionCompare, function (value) {
                        var dif = lstPermissionCurrent.find(x => x.keyName == value.keyName);
                        if (dif == undefined) {
                            lstPermissionCurrent.push(value);
                        }
                    });

                    PermissionLayer.GLOBAL.ArrayRole[indexRolePermission].listPermission = lstPermissionCurrent;
                }
            }
        }

        $('#user_selected_' + PermissionLayer.GLOBAL.UserIdFocus).trigger('click');
        $('#role_selected_' + PermissionLayer.GLOBAL.RoleIdFocus).trigger('click');
    },
    checkFormPermission: function () {
        var check = true;

        if ($(PermissionLayer.SELECTORS.search_radio + ":checked").val() == "USER") // nếu màn hình là chọn tài khoản
        {
            check = PermissionLayer.CheckValidUser();

            if (!check) {
                return check;
            }

            var checkPermissionRole = PermissionLayer.GLOBAL.ArrayRole.filter(x => x.listPermission.length == 0);

            if (checkPermissionRole.length > 0) {
                check = false;
                var lstName = checkPermissionRole.map(x => x.name);

                swal({
                    title: l('ManagementLayer:Notification'),
                    text: jQuery.validator.format(l('Vai trò [{0}] phải check chọn ít nhất một quyền'), lstName.toString()),
                    icon: "error",
                    button: l('ManagementLayer:Close'),
                });

                return check;
            }

            //check = PermissionLayer.CheckValidRole();

            //if (!check) {
            //    return check;
            //}
        }

        // check role
        if ($(PermissionLayer.SELECTORS.search_radio + ":checked").val() == "ROLE") // nếu màn hình là chọn vai trò
        {
            check = PermissionLayer.CheckValidRole();

            if (!check) {
                return check;
            }

            var checkPermissionUser = PermissionLayer.GLOBAL.ArrayUser.filter(x => x.listPermission.length == 0);

            if (checkPermissionUser.length > 0) {
                var lstName = checkPermissionUser.map(x => x.name);
                check = false;
                swal({
                    title: l('ManagementLayer:Notification'),
                    text: jQuery.validator.format(l('Tài khoản [{0}] phải check chọn ít nhất một quyền'), lstName.toString()),
                    icon: "error",
                    button: l('ManagementLayer:Close'),
                });

                return check;
            }

            //check = PermissionLayer.CheckValidUser();

            //if (!check) {
            //    return check;
            //}
        }

        return check;
    },
    CheckValidRole: function () {
        var check = true;
        if (PermissionLayer.GLOBAL.ArrayRole.length == 0) {
            check = false;
            swal({
                title: l('ManagementLayer:Notification'),
                text: l('Phải chọn ít nhất một vai trò'),
                icon: "error",
                button: l('ManagementLayer:Close'),
            });

            return check;
        }
        else {
            var checkPermissionRole = PermissionLayer.GLOBAL.ArrayRole.filter(x => x.listPermission.length == 0);

            if (checkPermissionRole.length > 0) {
                check = false;
                var lstName = checkPermissionRole.map(x => x.name);

                swal({
                    title: l('ManagementLayer:Notification'),
                    text: jQuery.validator.format(l('Vai trò [{0}] phải check chọn ít nhất một quyền'), lstName.toString()),
                    icon: "error",
                    button: l('ManagementLayer:Close'),
                });

                return check;
            }
        }

        return check;
    },
    CheckValidUser: function () {
        var check = true;
        if (PermissionLayer.GLOBAL.ArrayUser.length == 0) // kiểm tra array user
        {
            check = false;
            swal({
                title: l('ManagementLayer:Notification'),
                text: l('Bạn phải chọn ít nhất 1 tài khoản'),
                icon: "error",
                button: l('ManagementLayer:Close'),
            });

            return check;
        }
        else {
            var checkPermissionUser = PermissionLayer.GLOBAL.ArrayUser.filter(x => x.listPermission.length == 0);

            if (checkPermissionUser.length > 0) {
                var lstName = checkPermissionUser.map(x => x.name);
                check = false;
                swal({
                    title: l('ManagementLayer:Notification'),
                    text: jQuery.validator.format(l('Tài khoản [{0}] phải check chọn ít nhất một quyền'), lstName.toString()),
                    icon: "error",
                    button: l('ManagementLayer:Close'),
                });

                return check;
            }
        }

        return check;
    }
};