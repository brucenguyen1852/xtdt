﻿mn_selected = "mn_map_layer";
var l = abp.localization.getResource('HTKT');
var config = {
    GLOBAL: {
        table: [],
        select: [],
        dataName: '',
        dataCode: '',
        dataVersion: 0,
        idDirectory: '',
        listTypeList: [],
        previousSelect: '',
        rowSelected: null
    },
    CONSTS: {
        URL_AJAXSAVE: '/api/htkt/PropertiesDirectory/create',
        URL_AJAXGETDEFAULTDATA: '/api/htkt/PropertiesDirectory/get-directoryById',
        URL_AJAXGETDEFAULTDATAFIRST: '/api/htkt/PropertiesDefault/get-list',
        URL_CHECK_DELETE_PROPERTIES: "/api/HTKT/ManagementData/check-delete-properties"
    },
    SELECTORS: {
        tbodySystem: ".table-properties-system tbody",
        tbody: ".table-properties tbody",
        tabletrtd: ".table-properties tr td",
        tabletr: ".table-properties tr",
        textinsert: "input.textinsert",
        eidiinput: ".editinput",
        editselect: ".editselect",
        addRow: ".AddRow",
        headingOne: ".panel-heading a",
        // headingTwo: "#headingTwo a",
        btnSave: '.SaveProperties',
        dataName: '#DataName',
        dataCode: '#DataCode',
        version: '#Version',
        lat: "#DataLat",
        lng: "#DataLng",
        zoom: "#DataZoom",
        collapseOne: "#collapseOne",
        iconHeadingOne: "#headingOne a i",
        collapseTwo: "#collapseTwo",
        iconHeadingTwo: "#headingTwo a i",
        modalList: ".exampleModal-list",
        NameImplement: "#IdNameImplement",
        content_input_form: ".content-input-form"
    },
    init: function () {
        config.addTbodyTr();
        //config.setDatatable();
        config.setUpEvent();
        config.setUpEventModalList();
        var url = new URL(window.location.href);
        var checkid = url.searchParams.get("id");
        if (checkid !== null) TreeView.ReloadTree(checkid);
    },
    //add data vào table
    addTbodyTr: function () {
        $(config.SELECTORS.tbody).children().remove();
        $(config.SELECTORS.tbodySystem).children().remove();
        let hproperties = "";
        let hsystem = "";
        $.each(config.GLOBAL.table, function (i, obj) {
            if (obj.TypeSystem == 3 || obj.TypeSystem == 2) {
                if (obj.TypeProperties == "bool") {
                    obj.IsRequest = false;
                }

                //if (obj.CodeProperties !== "Name") {
                var html = `<tr data-count="${i}">
                                <th style="width:2%;text-align: center;">${(i + 1)}</th>
                                <td style="width:10%" data-code="NameProperties"><div class="editinput">${obj.NameProperties}</div></td>
                                <td style="width:10%" data-code="CodeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : "input-default"}">${obj.CodeProperties}</div></td>
                                <td style="width:10%" data-code="TypeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : "input-default"}">${(config.GLOBAL.select.find(x => x.values == obj.TypeProperties).text)}</div></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShow">
                                    <input type="checkbox" class="editcheckbox" ${(obj.IsShow ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShow ? "check" : "")}" data-id=""></span>
                                </td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsIndexing"><input type="checkbox" class="editcheckbox" ${(obj.IsIndexing ? "checked" : "")}> <span class="checkboxmark ${(obj.IsIndexing ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsRequest"><input type="checkbox" class="editcheckbox"} ${obj.TypeProperties == "bool" ? "" : obj.IsRequest ? "checked" : ""} > <span class="checkboxmark ${obj.TypeProperties == "bool" ? "noteditcheckbox" : ""}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsView"><input type="checkbox" class="editcheckbox" ${(obj.IsView ? "checked" : "")}> <span class="checkboxmark ${(obj.IsView ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsHistory"><input  type="checkbox" class="editcheckbox" ${(obj.IsHistory ? "checked" : "")}> <span class="checkboxmark ${(obj.IsHistory ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsInheritance"><input  type="checkbox" class="editcheckbox" ${(obj.IsInheritance ? "checked" : "")}> <span class="checkboxmark ${(obj.IsInheritance ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsAsync"><input type="checkbox" class="editcheckbox" ${(obj.IsAsync ? "checked" : "")}> <span class="checkboxmark ${(obj.IsAsync ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowExploit"><input type="checkbox" id="showExploit-${i}" class="editcheckbox" ${(obj.IsShowExploit || obj.CodeProperties == "Name" ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShowExploit ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowSearchExploit"><input type="checkbox" id="showExploit-${i}" class="editcheckbox" ${(obj.IsShowSearchExploit || obj.CodeProperties == "Name" ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShowSearchExploit ? "check" : "")}" data-id=""></span></td>
                                    <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowReference"><input type="checkbox" class="editcheckbox" ${(obj.IsShowReference || obj.CodeProperties == "Name" ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShowReference ? "check" : "")}" data-id=""></span></td>
                                <td style="width:10%" class="${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "unselectable" : ""}" data-code="DefalutValue"><div class="editinput">${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "" : obj.DefalutValue}</div></td>
                                <td style="width:10%" data-code="ShortDescription"><div class="editinput">${obj.ShortDescription}</div></td>
                                ${obj.TypeSystem == 2 ? '<td style="width: 6%"></td>' : config.showButtonRow(obj, i, config.GLOBAL.table.length)}
                            </tr>`;
                hproperties = hproperties + html;
                //} else {
                //    var html = `<tr data-count="${i}">
                //                <th style="width:2%;text-align: center;">${(i + 1)}</th>
                //                <td style="width:10%" data-code="NameProperties"><div class="editinput">${obj.NameProperties}</div></td>
                //                <td style="width:10%" data-code="CodeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : ""}">${obj.CodeProperties}</div></td>
                //                <td style="width:10%" data-code="TypeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : ""}">${(config.GLOBAL.select.find(x => x.values == obj.TypeProperties).text)}</div></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShow"><input type="checkbox" class="editcheckbox" ${(obj.IsShow ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShow ? "check" : "")}" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsIndexing"><input type="checkbox" class="editcheckbox" ${(obj.IsIndexing ? "checked" : "")}> <span class="checkboxmark ${(obj.IsIndexing ? "check" : "")}" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsRequest"><input type="checkbox" class="editcheckbox" ${(obj.IsRequest ? "checked" : "")}> <span class="checkboxmark ${(obj.IsRequest ? "check" : "")}" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsView"><input type="checkbox" class="editcheckbox" ${(obj.IsView ? "checked" : "")}> <span class="checkboxmark ${(obj.IsView ? "check" : "")}" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsHistory"><input  type="checkbox" class="editcheckbox" ${(obj.IsHistory ? "checked" : "")}> <span class="checkboxmark ${(obj.IsHistory ? "check" : "")}" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsInheritance"><input  type="checkbox" class="editcheckbox" ${(obj.IsInheritance ? "checked" : "")}> <span class="checkboxmark ${(obj.IsInheritance ? "check" : "")}" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsAsync"><input type="checkbox" class="editcheckbox" ${(obj.IsAsync ? "checked" : "")}> <span class="checkboxmark ${(obj.IsAsync ? "check" : "")}" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowExploit"><input type="checkbox" id="showExploit-${i}" class="editcheckbox" checked> <span class="checkboxmark check" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowSearchExploit"><input type="checkbox" id="showExploit-${i}" class="editcheckbox" checked> <span class="checkboxmark check" data-id=""></span></td>
                //                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowReference"><input type="checkbox" class="editcheckbox" ${(obj.IsShowReference ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShowReference ? "check" : "")}" data-id=""></span></td>
                //                <td style="width:10%" class="${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "unselectable" : ""}" data-code="DefalutValue"><div class="editinput">${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "" : obj.DefalutValue}</div></td>
                //                <td style="width:10%" data-code="ShortDescription"><div class="editinput">${obj.ShortDescription}</div></td>
                //                ${obj.TypeSystem == 2 ? '<td style="width: 5%"></td>' : config.showButtonRow(obj, i, config.GLOBAL.table.length)}
                //            </tr>`;
                //    hproperties = hproperties + html;
                //}
            }
            if (obj.TypeSystem == 1) {
                let html = `<tr class="notedit" data-count="${i}">
                                <th style="width:2%;text-align: center;">${(i + 1)}</th>
                                <td style="width:10%" data-code="NameProperties"><div>${obj.NameProperties}</div></td>
                                <td style="width:10%" data-code="CodeProperties"><div>${obj.CodeProperties}</div></td>
                                <td style="width:10%" data-code="TypeProperties"><div>${(config.GLOBAL.select.find(x => x.values == obj.TypeProperties).text)}</div></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShow"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsShow ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsShow ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsIndexing"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsIndexing ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsIndexing ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsRequest"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsRequest ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsRequest ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsView"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsView ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsView ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsHistory"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsHistory ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsHistory ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsInheritance"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsInheritance ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsInheritance ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsAsync"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsAsync ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsAsync ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowExploit"><input disabled type="checkbox" class="editcheckbox" id="showExploit-${i}" ${(obj.IsShowExploit ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsShowExploit ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowSearchExploit"><input disabled type="checkbox" class="editcheckbox" id="showExploit-${i}" ${(obj.IsShowSearchExploit ? "checked" : "")}> <span class="noteditcheckbox checkboxmark ${(obj.IsShowSearchExploit ? "check" : "")}" data-id=""></span></td>
                                <td class="table-checkbox" style="width:2%;text-align: center;position: relative;" data-code="IsShowReference"><input type="checkbox" class="editcheckbox" ${(obj.IsShowReference ? "checked" : "")}> <span class="checkboxmark ${(obj.IsShowReference ? "check" : "")}" data-id=""></span></td>
                                <td style="width:10%" data-code="DefalutValue"><div>${obj.DefalutValue}</div></td>
                                <td style="width:10%" data-code="ShortDescription"><div >${obj.ShortDescription}</div></td>
                                <td style="width: 6%"></td>
                            </tr>`;
                hsystem = hsystem + html;
            }
        });
        $(config.SELECTORS.tbody).append(hproperties);
        $(config.SELECTORS.tbodySystem).append(hsystem);
    },
    setUpEvent: function () {
        config.setEventAffter();
        $(config.SELECTORS.addRow).on("click", function () {
            let check = config.GLOBAL.table.filter(x => x.CodeProperties.trim() == "" || x.NameProperties.trim() == "");
            if (check.length == 0) {
                let count = (config.GLOBAL.table.length + 1);
                var data = {
                    NameProperties: "",
                    CodeProperties: "",
                    TypeProperties: "string",
                    IsShow: true,
                    IsIndexing: false,
                    IsRequest: false,
                    IsView: false,
                    IsHistory: false,
                    IsInheritance: false,
                    IsAsync: false,
                    IsShowExploit: false,
                    IsShowSearchExploit: false,
                    IsShowReference: false,
                    DefalutValue: "",
                    ShortDescription: "",
                    TypeSystem: 3,
                    OrderProperties: count,
                }
                config.GLOBAL.table.push(data);
                config.addTbodyTr();
                config.setEventAffter();
                setTimeout(function () {
                    $('.table-properties tr[data-count="' + (config.GLOBAL.table.length - 1) + '"] td[data-code="NameProperties"] .editinput').click();
                    $('.table-properties tr[data-count="' + (config.GLOBAL.table.length - 1) + '"] td[data-code="NameProperties"] .textinsert').focus();
                }, 500);
            }
            else {
                swal({
                    title: l("Layer:Notification"),
                    text: l("Layer:AttributeDataCannotEmpty"),
                    icon: "error",
                    button: l("Layer:Close"),
                });
            }
        });
        $(config.SELECTORS.headingOne).on("click", function () {
            if (!$(this).hasClass("open")) {
                $(this).addClass('open');
                $(this).find('.icon-colspan').html(`<svg id="Component_212_2" data-name="Component 212 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="width:24px; height:24px;">
                                                        <path id="Path_6612" data-name="Path 6612" d="M0,0H24V24H0Z" fill="none"></path>
                                                        <path id="Path_6613" data-name="Path 6613" d="M4,3H20a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H4a1,1,0,0,1-1-1V4A1,1,0,0,1,4,3ZM5,5V19H19V5Zm2,6H17v2H7Z" fill="#fff"></path>
                                                    </svg> `);
                $(this).parent().parent().find(".panel-collapse").addClass("show");
            }
            else {
                $(this).removeClass('open');

                $(this).find('.icon-colspan').html(`<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="width:24px; height:24px;transform: translate(-1%, 4%);"
	                                                         viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                                                        <style type="text/css">
	                                                        .st0{fill:#FFFFFF;}
                                                        </style>
                                                        <path id="Path_6613" class="st0" d="M4.24,1.93h16c0.55,0,1,0.45,1,1v16c0,0.55-0.45,1-1,1h-16c-0.55,0-1-0.45-1-1v-16
	                                                        C3.24,2.37,3.69,1.93,4.24,1.93z M5.24,3.93v14h14v-14H5.24z M7.24,9.93h10v2h-10V9.93z M13.24,5.93v10h-2v-10H13.24z"/>
                                                        </svg>`);
                $(this).parent().parent().find(".panel-collapse").removeClass("show");
            }
            //if ($(this).find("img").hasClass("fa-plus")) {
            //    $(this).find("img").removeClass("fa-plus");
            //    $(this).find("img").addClass("fa-minus");
            //    $(this).find("img").attr("src", "/images/Icon-Minus.svg");
            //    $(this).parent().parent().find(".panel-collapse").addClass("show");
            //} else {
            //    $(this).find("img").removeClass("fa-minus");
            //    $(this).find("img").addClass("fa-plus");
            //    $(this).find("img").attr("src", "/images/Icon-Plus.svg");
            //    $(this).parent().parent().find(".panel-collapse").removeClass("show");
            //}
        });

        $(config.SELECTORS.btnSave).on('click', function (e) {
            var status = true;
            var checkZoom = true;
            TreeView.clearLabelError();
            if ($(config.SELECTORS.zoom).val() === "") {
                TreeView.showLabelError(config.SELECTORS.zoom, jQuery.validator.format(l("ManagementLayer:DefaultDataIsValid"), l("Layer:ZoomLevel")));
            }
            if ($(TreeLayer.SELECTORS.minZoom).val() === "" && $(TreeLayer.SELECTORS.maxZoom).val() !== "") {
                $(TreeLayer.SELECTORS.minZoom).val("3");
            }
            else if ($(TreeLayer.SELECTORS.maxZoom).val() === "" && $(TreeLayer.SELECTORS.minZoom).val() !== "") {
                $(TreeLayer.SELECTORS.maxZoom).val("22");
            }

            if (Number($(config.SELECTORS.zoom).val()) < Number($(TreeLayer.SELECTORS.minZoom).val()) || Number($(config.SELECTORS.zoom).val()) > Number($(TreeLayer.SELECTORS.maxZoom).val()))
            {
                if ($(TreeLayer.SELECTORS.minZoom).val() !== "" && $(TreeLayer.SELECTORS.maxZoom).val() !== "") {
                    if (Number($(TreeLayer.SELECTORS.minZoom).val()) > Number($(TreeLayer.SELECTORS.maxZoom).val())) {
                        TreeView.showLabelError(TreeLayer.SELECTORS.minZoom, jQuery.validator.format(l("Layer:MinValueCannotLargerThanMaxValue"), l("Layer:ZoomLevel")));
                        checkZoom = false;
                    }

                    if ($(config.SELECTORS.zoom).val() !== "") {
                        TreeView.showLabelError(config.SELECTORS.zoom, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:ZoomLevel")));
                        checkZoom = false;
                    }
                }
            }

            if (checkZoom) {
                config.setOrder();
                $.each(config.GLOBAL.table, function (i, obj) {
                    let check = config.GLOBAL.table.filter(x => x.CodeProperties != "" && x.CodeProperties == obj.CodeProperties);
                    if (check.length >= 2) {
                        status = false;
                    }
                });

                if (status) {
                    if (config.GLOBAL.idDirectory != "" && config.checkFormLayerInfor()) {
                        let check = config.GLOBAL.table.filter(x => x.CodeProperties.trim() == "" || x.NameProperties.trim() == "");
                        if (check.length == 0) {
                            var location = {
                                Lat: 0,
                                Lng: 0
                            };

                            if ($(config.SELECTORS.lat).val() != "" && $(config.SELECTORS.lng).val() != "") {
                                location.Lat = $(config.SELECTORS.lat).val();
                                location.Lng = $(config.SELECTORS.lng).val()
                            }

                            var object = {
                                IdDirectory: config.GLOBAL.idDirectory,
                                NameTypeDirectory: $(config.SELECTORS.dataName).val(),
                                CodeTypeDirectory: xoa_dau($(config.SELECTORS.dataCode).val()).toUpperCase(),
                                VerssionTypeDirectory: parseFloat($(config.SELECTORS.version).val()),
                                IdImplement: $(config.SELECTORS.NameImplement).val(),
                                ListProperties: config.GLOBAL.table,
                                GroundMaptile: $(TreeView.SELECTORS.inputUrlMaptile).val(),
                                BoundMaptile: $(TreeView.SELECTORS.inputBoundMaptile).val(),
                                Location: location,
                                Zoom: $(config.SELECTORS.zoom).val(),
                                OptionDirectory: $(TreeLayer.SELECTORS.optionLayer).val(),
                                MinZoom: $(TreeLayer.SELECTORS.minZoom).val() != "" ? parseInt($(TreeLayer.SELECTORS.minZoom).val()) : 0,
                                MaxZoom: $(TreeLayer.SELECTORS.maxZoom).val() != "" ? parseInt($(TreeLayer.SELECTORS.maxZoom).val()) : 0
                            };

                            abp.ui.setBusy();
                            setTimeout(function () {
                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    //contentType: false,
                                    //processData: false,
                                    contentType: "application/json-patch+json",
                                    async: false,
                                    url: config.CONSTS.URL_AJAXSAVE,
                                    data: JSON.stringify(object),
                                    success: function (data) {
                                        if (data.code == "ok") {
                                            var dtoUpdate = {
                                                id: config.GLOBAL.idDirectory,
                                                name: object.NameTypeDirectory
                                            };
                                            $(TreeView.SELECTORS.inputUrlMaptile).val(data.result.groundMaptile);
                                            var tags = data.result.tags;

                                            if (tags != null) {
                                                $(TreeView.SELECTORS.inputBoundMaptile).val(tags["bounds"]);
                                            }
                                            else {
                                                $(TreeView.SELECTORS.inputBoundMaptile).val("[]");
                                            }

                                            $.ajax({
                                                type: "POST",
                                                contentType: "application/json-patch+json",
                                                async: false,
                                                url: "/api/htkt/directory/update-name",
                                                data: JSON.stringify(dtoUpdate),
                                                success: function (data) {
                                                    if (data.code == "ok") {
                                                        if (data.result) {
                                                            abp.notify.success(l("Layer:UpdateDataSuccesfully"));
                                                            var children = parseInt($('#tree-item-' + dtoUpdate.id).attr('data-chil'));

                                                            $('#tree-item-' + dtoUpdate.id).find('.name-layer-item').eq(0).text(dtoUpdate.name + (children > 0 ? ` (${children})` : ""));

                                                        }
                                                    }
                                                    else {
                                                        abp.notify.success(l("Layer:UpdateDataFailed"));
                                                    }
                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    //let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
                                                    console.error(jqXHR + ":" + errorThrown);
                                                }
                                            });

                                        } else {
                                            abp.notify.error(l("Layer:UpdateDataFailed"));
                                        }

                                        abp.ui.clearBusy();

                                        TreeView.GetDirectoryCloneOption(); // reset comboobx layer kế thừa
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.error(jqXHR + ":" + errorThrown);
                                        abp.ui.clearBusy();
                                    }
                                });
                            }, 1000);
                        } else {
                            swal({
                                title: l("Layer:Notification"),
                                text: l("Layer:AttributeDataCannotEmpty"),
                                icon: "error",
                                button: l("Layer:Close"),
                            });
                        }
                    }
                } else {
                    swal({
                        title: l("Layer:Notification"),
                        text: l("Layer:AttributeCodeDataCannotDuplicate"),
                        icon: "error",
                        button: l("Layer:Close"),
                    });
                }
            }
        });

        $(document).on('click', '.sidebar-toggle', function () {
            setTimeout(function () {
                TreeView.initSplitter();
            }, 300);
        });

        $(TreeView.SELECTORS.inputUrlMaptile).on('keyup', function () {
            if ($(TreeView.SELECTORS.inputUrlMaptile).val().trim() === "") {
                $(TreeView.SELECTORS.inputUrlMaptile).parents(".form-group").find(".lable-error").remove();
                $(TreeView.SELECTORS.inputUrlMaptile).parents(".form-group").removeClass("has-error");
            }
        });

        $('.div-headingTwo').on('click', '.checkboxmark', function () {
            if (!$(this).hasClass('noteditcheckbox')) {
                if (!$(this).hasClass('check')) {
                    $(this).addClass('check');
                    $(this).parent().find("input").trigger('click');
                } else {
                    $(this).removeClass('check');
                    $(this).parent().find("input").trigger('click');
                }
            }
        });
    },
    // check validate
    checkFormLayerInfor: function () {
        config.clearLabelErrorTableLayer();
        let check = true;
        let inputName = $(config.SELECTORS.dataName).val();
        if (!validateText(inputName, "text", 0, 0)) {
            //insertError($(config.SELECTORS.dataName), "other");
            check = false;
            TreeView.showLabelError(config.SELECTORS.dataName, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:DataTypeName")));
        }
        if (!validateText($(config.SELECTORS.dataCode).val(), "text", 0, 0)) {
            //insertError($(config.SELECTORS.dataCode), "other");
            check = false;
            TreeView.showLabelError(config.SELECTORS.dataCode, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:DataTypeCode")));
        }

        if (!validateText($(config.SELECTORS.version).val(), "number", 0, 0)) {
            //insertError($(config.SELECTORS.version), "other");
            check = false;
            
            if ($(config.SELECTORS.version).val() != "") {
                if (isNaN(parseInt($(config.SELECTORS.version).val()))) // kiểm tra kiểu số
                {
                    TreeView.showLabelError(config.SELECTORS.version, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Version")));
                }
                else {
                    if (parseInt($(config.SELECTORS.version).val()) < 0) // kiểm tra số nguyên dương
                    {
                        TreeView.showLabelError(config.SELECTORS.version, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Version")));
                    }
                }
            }
            else {

                TreeView.showLabelError(config.SELECTORS.version, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:Version")));
            }
        }

        if ($(TreeView.SELECTORS.inputUrlMaptile).val().trim() != "") {
            if (!validateText($(TreeView.SELECTORS.inputUrlMaptile).val(), "url", 0, 0)) {
                //insertError($(TreeView.SELECTORS.inputUrlMaptile), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputUrlMaptile, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:MaptilePath")));
            }

            if (!validateText($(TreeView.SELECTORS.inputBoundMaptile).val(), "text", 0, 0)) {
                //insertError($(TreeView.SELECTORS.inputBoundMaptile), "other");
                check = false;
                TreeView.showLabelError(TreeView.SELECTORS.inputBoundMaptile, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("Layer:MaptileCoordinates")));
            }
        }

        let inputLat = $(config.SELECTORS.lat).val();
        if (validateText(inputLat, "text", 0, 0)) {
            var valueLat = parseFloat(inputLat);

            if (isNaN(valueLat)) {
                insertError($(config.SELECTORS.lat), "other");
                check = false;
                TreeView.showLabelError(config.SELECTORS.lat, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Latitude")));
            }
            else {
                if (valueLat < 0 || valueLat > 90) {
                    insertError($(config.SELECTORS.lat), "other");
                    check = false;
                    TreeView.showLabelError(config.SELECTORS.lat, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Latitude")));
                }
            }
        }


        let inputLng = $(config.SELECTORS.lng).val();
        if (validateText(inputLng, "text", 0, 0)) {
            var valueLng = parseFloat(inputLng);

            if (isNaN(valueLng)) {
                insertError($(config.SELECTORS.lng), "other");
                check = false;
                TreeView.showLabelError(config.SELECTORS.lng, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Longitude")));
            }
            else {
                if (valueLng < 0 || valueLng > 180) {
                    insertError($(config.SELECTORS.lng), "other");
                    check = false;
                    TreeView.showLabelError(config.SELECTORS.lng, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:Longitude")));
                }
            }
        }

        let inputZoom = $(config.SELECTORS.zoom).val();
        if (!validateText(inputZoom, "number", 0, 0)) {
            inputZoom = Number(inputZoom);
            if (inputZoom > 22 || inputZoom < 3) {
                insertError($(config.SELECTORS.zoom), "other");
                check = false;
                TreeView.showLabelError(config.SELECTORS.zoom, jQuery.validator.format(l("ManagementLayer:DefaultDataIsValid"), l("Layer:ZoomLevel")));
            }
        } else {
            inputZoom = Number(inputZoom);
            if (inputZoom > 22 || inputZoom < 3) {
                insertError($(config.SELECTORS.zoom), "other");
                check = false;
                TreeView.showLabelError(config.SELECTORS.zoom, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:ZoomLevel")));
            }
        }

        let inputMinZoom = $(TreeLayer.SELECTORS.minZoom).val();
        inputMinZoom = Number(inputMinZoom);
        if (inputMinZoom < 3 && $(TreeLayer.SELECTORS.minZoom).val() !== "") {
            insertError($(TreeLayer.SELECTORS.minZoom), "other");
            check = false;
            TreeView.showLabelError(TreeLayer.SELECTORS.minZoom, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:ZoomLevel")));
        }

        let inputMaxZoom = $(TreeLayer.SELECTORS.maxZoom).val();
        inputMaxZoom = Number(inputMaxZoom);
        if (inputMaxZoom > 22) {
            insertError($(TreeLayer.SELECTORS.maxZoom), "other");
            check = false;
            TreeView.showLabelError(TreeLayer.SELECTORS.maxZoom, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("Layer:ZoomLevel")));
        }

        let checkHtml = CheckedHtmlEntities(config.SELECTORS.content_input_form);
        if (check) {
            check = checkHtml;
        }

        return check;
    },
    clearLabelErrorTableLayer: function () {
        $('.form-group.tablelayer').find(".lable-error").remove();
        $('.form-group.tablelayer').removeClass("has-error");
    },
    clearText: function () {
        $(config.SELECTORS.eidiinput).show();
        $(config.SELECTORS.textinsert).remove();
        $(config.SELECTORS.editselect).remove();
    },
    //get or set data list table
    getOrSetData: function (row, cell, check, val) {
        if (check) {//set
            config.GLOBAL.table[row][cell] = val;
            config.addTbodyTr();
            config.setEventAffter();
        } else {//get
            return config.GLOBAL.table[row][cell];
        }
    },
    //sort list with order
    sortListOrder: function () {
        config.GLOBAL.table.sort(function (a, b) {
            return parseInt(a.OrderProperties) - parseInt(b.OrderProperties);
        });
    },
    //event reset sau khi add data table
    setEventAffter: function () {
        $(config.SELECTORS.tabletrtd).on("click", 'div.editinput', function () {
            if (!$(this).parent().parent().hasClass("notedit") && !$(this).parent().hasClass("unselectable")) {
                config.clearText();
                $(this).hide();
                let code = $(this).parent().attr("data-code");
                let row = Number($(this).parents("tr").attr("data-count"));
                let val = config.getOrSetData(row, code, false, "");

                switch (code) {
                    case "TypeProperties":
                        let html = "<select class='form-control editselect'>";
                        $.each(config.GLOBAL.select, function (i, obj) {
                            html += "<option value=" + obj.values + " " + (obj.values == val ? "selected" : "") + ">" + obj.text + "</option>";
                        });
                        html += "</select>";
                        $(this).parent().append(html);
                        break;
                    case "CodeProperties":
                        $(this).parent().append('<input type="text" class="form-control textinsert inputCodeProperties" value="' + val + '">');
                        break;
                    case "NameProperties":
                        $(this).parent().append('<input type="text" class="form-control textinsert inputNameProperties" value="' + val + '">');
                        break;
                    default:
                        $(this).parent().append('<input type="text" class="form-control textinsert inputProperties" value="' + val + '">');
                        break;
                }
            }
        });
        $(config.SELECTORS.tabletrtd).on("focusout", ".textinsert", function () {
            var data = $(this).val().trim();
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));//$(this).parent().parent().index();
            if (code == "CodeProperties") {
                let array = JSON.parse(JSON.stringify(config.GLOBAL.table));
                let check = array.find(x => x.CodeProperties.toLocaleLowerCase() != "" && x.CodeProperties.toLocaleLowerCase() == data.toLocaleLowerCase() && x.OrderProperties != row + 1);
                if (check != null && check != undefined) {
                    $(this).val('');
                    swal({
                        title: l("Layer:Notification"),
                        text: l("Layer:AttributeCodeExists"),
                        icon: "error",
                        button: l("Layer:Close"),
                    });
                    return;
                }
            }
            config.getOrSetData(row, code, true, data);
        });

        $(config.SELECTORS.tabletrtd).on("click", ".editcheckbox", function () {
            let data = $(this).is(":checked");
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));
            if (code == "IsIndexing") {
                let obj = config.GLOBAL.table[row];
                if (obj != null && obj != undefined) {
                    if (obj.TypeProperties != "text" && obj.TypeProperties != "string" && obj.TypeProperties != "stringlarge"
                        && obj.TypeProperties != "float" && obj.TypeProperties != "int") {
                        swal({
                            title: l("Layer:Notification"),
                            text: l("Layer:WarningIndex"),
                            icon: "warning",
                            button: l("Layer:Close"),
                        });
                        $(this).prop("checked", false);
                        return false;
                    }
                }
            } else if (code == "IsShowExploit" || code == "IsShowSearchExploit") {
                let obj = config.GLOBAL.table[row];
                if (obj != null && obj != undefined) {
                    if (obj.TypeProperties == "link" || obj.TypeProperties == "image" || obj.TypeProperties == "file") {
                        swal({
                            title: l("Layer:Notification"),
                            text: l("Layer:WarningShowExploit"),
                            icon: "warning",
                            button: l("Layer:Close"),
                        });
                        $(this).prop("checked", false);
                        return false;
                    }
                }
            }
            config.getOrSetData(row, code, true, data);
        });
        $(config.SELECTORS.tabletrtd).on("change", ".editselect", function () {
            let data = $(this).find(":selected").val();
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));
            config.getOrSetData(row, code, true, data);
            let td = $('tr[data-count="' + row + '"]')[0].querySelectorAll("td");
            let tdaction = td[td.length - 1];
            if ($(this).val() == "list" || $(this).val() == "checkbox" || $(this).val() == "radiobutton") {
                config.GLOBAL.listTypeList = [];
                config.GLOBAL.rowSelected = config.GLOBAL.table[row];
                $(config.SELECTORS.modalList).modal('show');
                $(td[td.length - 3]).addClass("unselectable");
                //$(tdaction).append(`<button type="button" class="btn btn-info showList" style="padding:3px 5px;font-size:13px; margin-left: 2px;"> <i class="fa fa fa-list" aria-hidden="true"></i></button>`);
            } else {
                if (config.GLOBAL.previousSelect == "list" || config.GLOBAL.previousSelect == "checkbox" || config.GLOBAL.previousSelect == "radiobutton") {
                    config.getOrSetData(row, "DefalutValue", true, "");
                    $(tdaction).children(".showList").remove();
                    $(td[td.length - 3]).removeClass("unselectable");
                }
            }
        });
        $(config.SELECTORS.tabletrtd).on("focus", ".editselect", function () {
            config.GLOBAL.previousSelect = '';
            config.GLOBAL.previousSelect = this.value;
        });
        $(config.SELECTORS.tabletrtd).on("click", ".deleteRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            if (config.GLOBAL.table[row] != undefined) {
                var message = "";
                if (!config.CheckDeleteProperties(config.GLOBAL.table[row].CodeProperties)) {
                    message = l("Layer:WarningPropertiesIsUse")
                }

                var text = document.createElement("div");
                var htmlMessage = `<span style="display:block;">${l("Layer:AreYouDeleteThisProperty")}</span> <span style="color:red; font-style: italic; font-size:12px;">${message}</span>`;
                text.innerHTML = htmlMessage;
                swal({
                    title: l("Layer:Notification"),
                    html: true,
                    content: text,
                    //title: l("Layer:AreYouDeleteThisProperty"),
                    //text: message,
                    icon: "warning",
                    buttons: [
                        l("Layer:Close"),
                        l("Layer:Agree")
                    ],
                }).then((value) => {
                    if (value) {
                        config.GLOBAL.table.splice(row, 1);
                        config.setOrder(); // sắp xếp lại order
                        config.addTbodyTr();
                        config.setEventAffter();
                    }
                });


            }
        });
        $(config.SELECTORS.tabletrtd).on("click", ".downRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            var objfind = config.GLOBAL.table[row];
            var objfindNext = config.GLOBAL.table[row + 1];
            if (objfind.OrderProperties < config.GLOBAL.table.length && typeof objfindNext !== "undefined"
                && objfindNext !== null && objfindNext.TypeSystem == 3) {
                var orderCurrent = objfind.OrderProperties;
                config.GLOBAL.table[row].OrderProperties = objfindNext.OrderProperties;
                config.GLOBAL.table[row + 1].OrderProperties = orderCurrent;
            }
            config.sortListOrder();
            config.addTbodyTr();
            config.setEventAffter();
        });
        $(config.SELECTORS.tabletrtd).on("click", ".upRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            var objfind = config.GLOBAL.table[row];
            var objfindBack = config.GLOBAL.table[row - 1];
            if (objfind.OrderProperties > 0 && objfind.OrderProperties <= config.GLOBAL.table.length
                && typeof objfindBack !== "undefined" && objfindBack !== null && objfindBack.TypeSystem == 3) {
                var orderCurrent = objfind.OrderProperties;
                config.GLOBAL.table[row].OrderProperties = objfindBack.OrderProperties;
                config.GLOBAL.table[row - 1].OrderProperties = orderCurrent;
            }
            config.sortListOrder();
            config.addTbodyTr();
            config.setEventAffter();
        });

        $(config.SELECTORS.tabletrtd).on("change keyup", ".inputCodeProperties", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }
            this.value = value;
        });
        $(config.SELECTORS.tabletrtd).on("change keyup", ".inputNameProperties", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }

            let row = Number($(this).parents("tr").attr("data-count"));
            let array = JSON.parse(JSON.stringify(config.GLOBAL.table));
            let check = array.find(x => x.CodeProperties.toLocaleLowerCase() != "" && x.CodeProperties.toLocaleLowerCase() == value.toLocaleLowerCase() && x.OrderProperties != row + 1);
            if (check != null && check != undefined) {
                value += "1";
            }
            config.GLOBAL.table[row]["CodeProperties"] = value;
            $(this).parents("tr").find("td[data-code='CodeProperties'] .editinput").text(value);
        });
    },
    //hiển thị các nút ở hoạt động
    showButtonRow: function (obj, count, length) {
        let html = "";
        if (obj.TypeSystem == 2 || obj.TypeSystem == 1) {
            html += '<td style="width:86px"></td>';
        } else {
            html += '<td style="width:86px">';
            html += `<div class="d-flex w-100" style="place-content: flex-end;">`
            if (obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") {
                html += `<button type="button" class="btn btn-info showList" style="padding:3px 3px;font-size:13px; margin-left: 2px;"> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 14 14"><defs></defs><path class="a" style="fill:none;" d="M0,0H14V14H0Z"/><path class="b" style="fill:#FFF;" d="M2,12.167H7.833v1.167H2ZM2,8.083H13.667V9.25H2ZM2,4H13.667V5.167H2Zm9.333,8.167v-1.75H12.5v1.75h1.75v1.167H12.5v1.75H11.333v-1.75H9.583V12.167Z" transform="translate(-0.833 -2.667)"/></svg></button>`;
            }
            html += '<button type = "button" class="btn btn-info downRow" style = "padding:3px 3px;font-size:13px; margin-left: 2px;" > <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 18 18"><defs></defs><g transform="translate(18 18) rotate(180)"><path class="a" style="fill:none;" d="M0,0H18V18H0Z"/><path class="b" style="fill:#FFF;" d="M9.614,6.392V14H8.386V6.392l-3.3,3.352-.869-.884L9,4l4.778,4.861-.869.884Z"/></g></svg></button >';
            html += '<button type = "button" class="btn btn-info upRow" style = "padding:3px 3px;font-size:13px; margin-left: 2px;" > <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 18 18"><defs></defs><path class="a" style="fill:none;" d="M0,0H18V18H0Z"/><path class="b" style="fill:#FFF;" d="M9.614,6.392V14H8.386V6.392l-3.3,3.352-.869-.884L9,4l4.778,4.861-.869.884Z"/></svg></button >';

            // kiểm tra phân quyền, nếu user có phân quyền edit thì mới sử dụng được chức năng xóa thuộc tính
            var checkPermission = true;
            if (TVisAdmin.toLocaleLowerCase() != "true") {
                var indexPermission = TVlstLayerPermission.findIndex(x => x.idDirectory == config.GLOBAL.idDirectory);
                if (indexPermission == -1) {
                    checkPermission = false;
                }
                else {
                    if (!TVlstLayerPermission[indexPermission].listPermisson.some(x => x == "PermissionLayer.EDIT")) {
                        checkPermission = false;
                    }
                }
            }

            if (checkPermission) {
                html += `<button type = "button" class="btn btn-danger deleteRow" style = "padding:3px 3px;font-size:13px;margin-left: 2px;" > <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" style="display: block;margin: auto;" viewBox="0 0 18 18"><defs></defs><path class="a" style="fill:none;" d="M0,0H18V18H0Z"/><path class="b"  style="fill:#FFF;" d="M3.4,6.2H14.6v9.1a.7.7,0,0,1-.7.7H4.1a.7.7,0,0,1-.7-.7ZM5.5,4.1V2.7A.7.7,0,0,1,6.2,2h5.6a.7.7,0,0,1,.7.7V4.1H16V5.5H2V4.1Zm1.4-.7v.7h4.2V3.4ZM6.9,9v4.2H8.3V9ZM9.7,9v4.2h1.4V9Z"/></svg></button >`;
            }

            html += `</div>`
            html += '</td > ';
        }
        return html;
    },
    getDefault: function () {
        //$.ajax({
        //    type: "GET",
        //    contentType: "application/json-patch+json",
        //    async: false,
        //    url: config.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=Chưa có',
        //    data: {
        //    },
        //    success: function (data) {
        //        if (data.code == "ok" && data.result.listProperties != undefined) {
        //            var result = [];
        //            $.each(data.result.listProperties, function (i, obj) {
        //                var element = {
        //                    NameProperties: obj.nameProperties,
        //                    CodeProperties: obj.codeProperties,
        //                    TypeProperties: obj.typeProperties,
        //                    IsShow: obj.isShow,
        //                    IsIndexing: obj.isIndexing,
        //                    IsRequest: obj.isRequest,
        //                    IsView: obj.isView,
        //                    IsHistory: obj.isHistory,
        //                    IsInheritance: obj.isInheritance,
        //                    IsAsync: obj.isAsync,
        //                    DefalutValue: obj.defalutValue,
        //                    ShortDescription: obj.shortDescription,
        //                    TypeSystem: obj.typeSystem,
        //                    OrderProperties: obj.orderProperties
        //                };
        //                result.push(element);
        //            });
        //            config.GLOBAL.table = result;
        //        } else {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: config.CONSTS.URL_AJAXGETDEFAULTDATAFIRST,
            data: {
            },
            success: function (data) {
                if (data.code == "ok" && data.result != null) {
                    var result = [];
                    $.each(data.result, function (i, obj) {
                        var element = {
                            NameProperties: obj.nameProperties,
                            CodeProperties: obj.codeProperties,
                            TypeProperties: obj.typeProperties,
                            IsShow: obj.isShow,
                            IsIndexing: obj.isIndexing,
                            IsRequest: obj.isRequest,
                            IsView: obj.isView,
                            IsHistory: obj.isHistory,
                            IsInheritance: obj.isInheritance,
                            IsAsync: obj.isAsync,
                            IsShowExploit: obj.isShowExploit,
                            IsShowSearchExploit: obj.isShowSearchExploit,
                            DefalutValue: obj.defalutValue,
                            ShortDescription: obj.shortDescription,
                            TypeSystem: obj.typeSystem,
                            OrderProperties: obj.orderProperties
                        };
                        result.push(element);
                    });
                    config.GLOBAL.table = result;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
                ViewMap.showLoading(false);
            }
        });
        //        }
        //    },
        //    error: function (jqXHR, textStatus, errorThrown) {
        //        let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
        //        console.log(messageErorr);
        //        ViewMap.showLoading(false);
        //    }
        //});
    },
    resetTable: function () {
        config.GLOBAL.idDirectory = '';
        config.GLOBAL.table = [];
        config.addTbodyTr();
        $('#DataName').val("");
        $('#h5NameData').html("");
        $('#DataCode').val("");
        $('#Version').val(0);
        $('#NameImplement').val('');
        $(TreeView.SELECTORS.inputUrlMaptile).val("");
        $(TreeView.SELECTORS.inputBoundMaptile).val("");
        $(config.SELECTORS.lat).val("");
        $(config.SELECTORS.lng).val("");
        $(config.SELECTORS.zoom).val("");
    },
    setUpEventModalList: function () {
        $('#codeList').on("keyup", function () {
            // Our regex
            // a-z => allow all lowercase alphabets
            // A-Z => allow all uppercase alphabets
            // 0-9 => allow all numbers
            // @ => allow @ symbol
            var regex = /^[a-zA-Z0-9_]+$/;
            // This is will test the value against the regex
            // Will return True if regex satisfied
            if (regex.test(this.value) !== true)
                //alert if not true
                //alert("Invalid Input");

                // You can replace the invalid characters by:
                this.value = this.value.replace(/[^a-zA-Z0-9_]+/, '');
        });

        $('#nameList').on("keyup", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                this.value = this.value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }

            $('#codeList').val(value);

        });

        $('#btnSaveList').on('click', function (e) {
            if ($('#codeList').val().trim() !== "" && $('#nameList').val().trim() !== "")
            {
                let checkExitsCode = config.GLOBAL.listTypeList.find(x => x.code === $('#codeList').val().trim());

                if ($('#idList').val() == "") // nếu là create
                {
                    checkExitsCode = config.GLOBAL.listTypeList.find(x => x.code === $('#codeList').val().trim());
                    if (checkExitsCode != undefined)
                    {
                        swal({
                            title: l("Layer:Notification"),
                            text: l("Layer:PropertyCodeExist"),
                            icon: "warning",
                            button: l("Layer:Close"),
                        });

                        $('#codeList').val('');
                        $('#nameList').val('');
                        $('#idList').val('');

                        return;
                    }

                    let id = config.uuidv4();
                    let object = {
                        id: id,
                        name: $('#nameList').val().trim(),
                        code: $('#codeList').val().trim(),
                        checked: false,
                        action: config.getActionList(id)
                    };
                    config.GLOBAL.listTypeList.push(object);

                }
                else // nếu là edit
                {
                    checkExitsCode = config.GLOBAL.listTypeList.find(x => x.code === $('#codeList').val().trim() && x.id != $('#idList').val());
                    if (checkExitsCode != undefined) {
                        swal({
                            title: l("Layer:Notification"),
                            text: l("Layer:PropertyCodeExist"),
                            icon: "warning",
                            button: l("Layer:Close"),
                        });

                        $('#codeList').val('');
                        $('#nameList').val('');
                        $('#idList').val('');

                        return;
                    }

                    let object = config.GLOBAL.listTypeList.find(x => x.id === $('#idList').val().trim());
                    object.name = $('#nameList').val().trim();
                    object.code = $('#codeList').val().trim();
                }


                $('#codeList').val('');
                $('#nameList').val('');
                $('#idList').val('');
                config.updateTable();
               
            }
        });

        $(document).on('click', '.btn-deleteTable-list', function () {
            swal({
                title: l("Layer:Notification"),
                text: l("Layer:AreYouDeleteThisProperty"),
                icon: "warning",
                buttons: [
                    l("Layer:Close"),
                    l("Layer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    let id = $(this).attr('delete-for');
                    config.GLOBAL.listTypeList = JSON.parse(JSON.stringify(config.GLOBAL.listTypeList.filter(x => x.id !== id)));
                    config.updateTable();
                }
            });
        });

        $(document).on('click', '.btn-editTable-list', function () {
            let id = $(this).attr('edit-for');
            let object = config.GLOBAL.listTypeList.find(x => x.id === id);
            $('#codeList').val(object.code);
            $('#nameList').val(object.name);
            $('#idList').val(object.id);
        });

        $(document).on('click', '.showList', function () {
            $('#codeList').val('');
            $('#nameList').val('');
            $('#idList').val('');
            let row = Number($(this).parents("tr").attr("data-count"));
            config.GLOBAL.rowSelected = config.GLOBAL.table[row];
            $(config.SELECTORS.modalList).modal('show');
        });

        $(config.SELECTORS.modalList).on('show.bs.modal', function () {
            $('#codeList').val('');
            $('#nameList').val('');
            $('#idList').val('');
            if (config.GLOBAL.rowSelected != null && config.GLOBAL.rowSelected.DefalutValue != "" && config.GLOBAL.rowSelected.DefalutValue != null) {
                try {
                    config.GLOBAL.listTypeList = JSON.parse(config.GLOBAL.rowSelected.DefalutValue);
                }
                catch (e) {

                }
            }
            config.updateTable();
        });

        $(config.SELECTORS.modalList).on('hidden.bs.modal', function () {
            config.GLOBAL.rowSelected.DefalutValue = JSON.stringify(config.GLOBAL.listTypeList);
            config.GLOBAL.listTypeList = [];
        });


        //Không cho phep nhập các ký tự đặc biệt
        $(config.SELECTORS.modalList).on('keyup', '#codeList', function () {
            $(this).val($(this).val().replace(/[^a-zA-Z0-9_]/g, ""));
        });

        //$(config.SELECTORS.modalList).on('keyup', '#nameList', function () {
        //    $(this).val($(this).val().replace(/[^a-zA-Z0-9_]/g, ""));
        //});

        $(config.SELECTORS.modalList).on('paste', '#codeList', function () {
            $(this).val($(this).val().replace(/[^a-zA-Z0-9_]/g, ""));
        });

        //$(config.SELECTORS.modalList).on('paste', '#nameList', function () {
        //    $(this).val($(this).val().replace(/[^a-zA-Z0-9_]/g, ""));
        //});
    },
    updateTable: function () {
        $('.div-list table tbody').html('');
        var test = '';
        if (config.GLOBAL.listTypeList.length > 0) {
            var count = 0;
            for (var i = 0; i < config.GLOBAL.listTypeList.length; i++) {
                //if (config.GLOBAL.listTypeList[i].status) {
                count++;
                test += `<tr>
                            <th scope="row">${count}</th>
                            <td class="text-center">${config.GLOBAL.listTypeList[i].name}</td>
                            <td class="text-center">${config.GLOBAL.listTypeList[i].code}</td>
                            <td>${config.getActionList(config.GLOBAL.listTypeList[i].id)}</td>
                        </tr>`;
                //}
            }
        }
        if (test != "") {
            $('.div-list table tbody').append(test);
        } else {
            $('.div-list table tbody').append(`<tr>
                <th scope="row" colspan="4">${l("Layer:NoData")}</th>
            </tr>`);
        }

        //text += `</tr>`;
        //$('.div-list table thead').html('');
        //$('.div-list table thead').append(text);
    },
    //new Guid Id
    uuidv4: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    getActionList: function (id) {
        return `<a href="#" title="${l('Layer:Delete')}" class="delete btn btn-danger btn-xs btn-deleteTable-list mr-1" delete-for="${id}"><i class="fa fa-trash-o"></i></a>
                <a href="#" class= "btn btn-primary btn-xs btn-editTable-list" edit-for="${id}" title = "${l('Layer:Edit')}"> <i class="fa fa-edit"></i></a>`;
    },
    setOrder: function () {
        config.GLOBAL.table.sortOn("OrderProperties");

        for (var i = 0; i < config.GLOBAL.table.length; i++) {
            config.GLOBAL.table[i].OrderProperties = i + 1;
        }
    },
    CheckDeleteProperties: function (code) {
        var result = true;
        $.ajax({
            type: "GET",
            url: config.CONSTS.URL_CHECK_DELETE_PROPERTIES,
            data: {
                code: code,
                directoryid: config.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json",
            success: function (res) {
                if (res.code == "ok") {
                    result = res.result;
                }
                else {
                    result = false;
                    console.log(res);
                }
            },
        });

        return result;
    }
    ////get data table
    //getDatatable
}

Array.prototype.sortOn = function (key) {
    this.sort(function (a, b) {
        if (a[key] < b[key]) {
            return -1;
        } else if (a[key] > b[key]) {
            return 1;
        }
        return 0;
    });
}