﻿var isCurrentForcus = true;
var TreeLayer = {
    GLOBAL: {
        ArrayTree: [],
        ArrayNodeCheck: [],
        DirectoryForcus: "",
    },
    CONSTS: {
        URL_GETLISTDIRECTORY: "/api/htkt/directory/get-list-new-layer",
        //URL_GET_PROPERTIES_DIRECTORY: "/api/app/khaiThacPropertiesDirectory"
    },
    SELECTORS: {
        tree: "#checkTree",
        dyntree_node_text: ".dynatree-title",
        search_tree: ".search-tree",
        nameFolderParent: ".name-folder-parent",
        divParentTree: "#div-parent-tree",
        content_left_sidebar: '.map-side-form .scroll-content',
        optionLayerCheck: "input[name='optionLayer'][value='VAL']",
        optionLayer: "input[name='optionLayer']:checked",
        minZoom: "input[id='MinZoom']",
        maxZoom: "input[id='MaxZoom']",
        content_input_form: "#div2",
        content_count_directory: ".content-count-directory",
        item_count: ".item-count-ten"
    },
    init: function () {
        TreeLayer.setEvent();
        TreeLayer.GetListLayer();
    },
    setEvent: function () {
        $(TreeLayer.SELECTORS.divParentTree).on('click', function () {
            var id = $(this).attr('data-id');

            let pathName = document.location.pathname;
            history.replaceState(null, null, pathName);

            
            var getListPermission = TVlstLayerPermission.find(x => x.idDirectory == id);

            var created = false;
            var deleted = false;
            var edit = false;
            var permission = false;
            $(PermissionLayer.SELECTORS.check_box_implement_parent).prop('disabled', true);
            if (TVisAdmin == "True") {
                created = true;
                deleted = false;
                edit = true;
                permission = true;
            }
            else {
                if (getListPermission != undefined) {
                    for (var i = 0; i < getListPermission.listPermisson.length; i++) {
                        switch (getListPermission.listPermisson[i]) {
                            case "PermissionLayer.CREATE":
                                created = true;
                                break;
                            case "PermissionLayer.EDIT":
                                edit = true;
                                break;
                            case "PermissionLayer.DELETE":
                                deleted = true;
                                break;
                            case "PermissionLayer.PERMISSION":
                                permission = true;
                                break;
                            default:
                        }
                    }
                }
            }
            //config.GLOBAL.idDirectory = id;
            var checkAction = TreeView.CheckActionFolder();
            if (checkAction) {
                TreeLayer.GLOBAL.DirectoryForcus = id;
                thisId = id;
                parentId = id;
                level = 1;
                $('.item-li').css('background-color', 'transparent');
                $(this).css('background-color', '#ffffff13');

                TreeView.DisabledActionForFolder(0);

                $(config.SELECTORS.btnSave).prop('disabled', true);
                datainfo.GLOBAL.statusShow = false;
                config.resetTable();
                $('.item-li').removeClass('forcus');
                $(this).addClass('forcus');

                $(TreeLayer.SELECTORS.content_input_form).hide();
                $(TreeLayer.SELECTORS.content_count_directory).show();

                TreeLayer.showButton(created, edit, deleted, permission, true);

                TreeView.ResetAction();
            }
        });

        $(TreeLayer.SELECTORS.tree).on('click', TreeLayer.SELECTORS.dyntree_node_text, function () {
            var folder = $(this).parent().attr('data-folder');
            //var id = $(this).attr('data-id');
            if (folder != "true") {
                $(this).parent().children().eq(1).click();
            }
        })

        $(".tree-o-i").on('click', '.item-li', function () {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');

            var created = $(this).attr('data-create');
            var deleted = $(this).attr('data-delete');
            var edit = $(this).attr('data-edit');
            var permission = $(this).attr('data-permission');

            if (TreeView.GLOBAL.isAdmin == "False") {
                PermissionLayer.GetListPermissionLayerByUserCurrent(id);
            }
            $(PermissionLayer.SELECTORS.check_box_implement_parent).prop('disabled', false);
            var checkPermisson = TreeView.CheckPermission(created, edit, deleted, permission, TVisAdmin.toLowerCase());

            if (checkPermisson) {
                var checkAction = true;
                if (!type || type == "false") {
                    checkAction = TreeView.CheckActionLayer();
                }
                else {
                    checkAction = TreeView.CheckActionFolder();
                }

                let pathName = document.location.pathname;

                if (checkAction) {
                    $(TreeLayer.SELECTORS.divParentTree).css('background-color', 'transparent');
                    if (id != null && id != undefined) {

                        TreeView.clearLabelError();
                        //if ($(`#checkmark-${id}`).hasClass('check')) {

                        if (TreeLayer.GLOBAL.DirectoryForcus != id) {
                            $('.item-li').removeClass('forcus');
                            $(this).addClass('forcus');

                            config.GLOBAL.idDirectory = id;
                            TreeLayer.GLOBAL.DirectoryForcus = id;
                            thisId = id;
                            parentId = id;
                            let layer = listFolder.find(x => x.id == id);
                            level = layer.level + 1;
                            $('.item-li').css('background-color', 'transparent');
                            $(this).css('background-color', '#ffffff13');
                            if (!type || type == "false") // kiểm tra layer hay là folder
                            {
                                TreeView.getProperty();
                                TreeLayer.GetInfoTree();

                                $(config.SELECTORS.btnSave).prop('disabled', false);
                                datainfo.GLOBAL.statusShow = true;

                                $(TreeLayer.SELECTORS.content_input_form).show();
                                $(TreeLayer.SELECTORS.content_count_directory).hide();

                                PermissionLayer.ResetModal();
                            } else {
                                config.resetTable();
                                $(config.SELECTORS.btnSave).prop('disabled', true);

                                $(TreeLayer.SELECTORS.content_input_form).hide();
                                $(TreeLayer.SELECTORS.content_count_directory).show();
                            }

                            TreeView.ResetAction(); // reset lại action nếu đang ở trạng thái action
                        }

                        if (!$(this).next().hasClass('open')) {
                            $(this).next().addClass('open');
                            $(this).next().slideToggle("slow");

                            var element = $(this).find('.icon-menu-tree');
                            if (element.length > 0) {
                                element.addClass('open')
                            }
                        }
                        else {
                            $(this).next().slideToggle("slow");
                            $(this).next().removeClass('open');
                            let element = $(this).find('.icon-menu-tree');
                            if (element.length > 0) {
                                element.removeClass('open')
                            }
                        }

                        if (!type || type == "false") {
                            history.replaceState(null, null, pathName + "?id=" + id);
                            TreeView.DisabledActionForLayer();
                        }
                        else {
                            history.replaceState(null, null, pathName);
                            TreeView.DisabledActionForFolder(1);
                        }

                        //permission//
                        TreeLayer.showButton(created, edit, deleted, permission, type);
                    }
                }
            }
        });

        $(TreeLayer.SELECTORS.content_count_directory).on('click', TreeLayer.SELECTORS.item_count, function () {
            var id = $(this).attr('data-id');
            var created = $(this).attr('data-create');
            var deleted = $(this).attr('data-delete');
            var edit = $(this).attr('data-edit');
            var permission = $(this).attr('data-permission');
            let pathName = document.location.pathname;

            history.replaceState(null, null, pathName + "?id=" + id);

            var folderParent = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == id);
            if (folderParent != undefined) {
                if (folderParent.level > 0) {
                    $(TreeLayer.SELECTORS.content_input_form).show();
                    $(TreeLayer.SELECTORS.content_count_directory).hide();
                    config.GLOBAL.idDirectory = id;
                    TreeLayer.GLOBAL.DirectoryForcus = id;
                    thisId = id;
                    parentId = id;

                    TreeView.getProperty();
                    TreeLayer.GetInfoTree();

                    TreeView.DisabledActionForLayer();

                    $(TreeLayer.SELECTORS.content_left_sidebar).animate({
                        scrollTop: $('#item-li-' + id).offset().top - $(TreeLayer.SELECTORS.content_left_sidebar).offset().top
                    }, 'slow');

                    // focus tới node cây
                    $('.item-li').removeClass('forcus');
                    $('#item-li-' + id).addClass('forcus');
                    $('#item-li-' + id).next().addClass('open');
                    $('#item-li-' + id).next().slideDown("slow");

                    var element = $('#item-li-' + id).find('.icon-menu-tree');
                    if (element.length > 0) {
                        element.addClass('open')
                    }

                    $(config.SELECTORS.btnSave).prop('disabled', false);
                    datainfo.GLOBAL.statusShow = true;
                    // mở cây
                    $('.tree-o-i .treeview').each(function () {
                        //search_tree(this, value);
                        var idlayer = $(this).attr('data-id');
                        var parent = $(this).attr('data-parent');
                        var level = parseInt($(this).attr('data-level'));

                        if (level == 0) {
                            $(this).show();
                        }

                        if (id == idlayer) {
                            $(this).show();
                            if (parent != "null") {
                                $('#tree-ul-' + parent).addClass('open');
                                $('#arrow-item-' + parent).addClass('open');
                                $('#tree-ul-' + parent).slideDown();

                                $($(this).find('.icon-menu-tree')).addClass('open');
                                forcus_tree('#tree-item-' + parent, parent);
                            }
                        }
                    });
                }

                //permission//
                TreeLayer.showButton(created, edit, deleted, permission, "false");
            }

            $(TreeLayer.SELECTORS.content_left_sidebar).animate({
                scrollTop: $('#item-li-' + id).offset().top - $(TreeLayer.SELECTORS.content_left_sidebar).offset().top
            }, 'slow');

        });

        $(TreeLayer.SELECTORS.search_tree).on('keyup', delay(function () {
            $('.treeview-not-result').remove();
            $('.tree-o-i .treeview').hide();
            $(TreeLayer.SELECTORS.divParentTree).show();

            var value = $(this).val().toLowerCase().trim();
            if (value != "") {
                $('.tree-o-i .treeview').each(function () {
                    //search_tree(this, value);
                    var id = $(this).attr('data-id');
                    var name = decodeURIComponent($(this).attr('data-search'));
                    var parent = $(this).attr('data-parent');
                    var level = parseInt($(this).attr('data-level'));
                    var folder = $(this).attr('data-type');
                    var chil = parseInt($(this).attr('data-chil'));
                    if (level == 0) {
                        $(this).show();
                    }
                    if (xoa_dau(name.trim().toLowerCase()).includes(xoa_dau(value.trim().toLowerCase()))) {
                        $(this).show();

                        if (chil > 0) {
                            $('#tree-item-' + id).find('.icon-tree-' + id).addClass('open'); // mở open icon
                            $('#tree-ul-' + id).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + id).slideDown();
                            $('#tree-ul-' + id + " li").show();
                        }

                        if (parent != "null") {
                            $('#tree-item-' + parent).find('.icon-tree-' + parent).addClass('open'); // mở open icon
                            $('#tree-ul-' + parent).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + parent).slideDown();

                            search_tree('#tree-item-' + parent, value);
                        }
                    }
                });

                var countNotResult = $(" .treeview").filter(function () {
                    return $(this).css('display') == 'none'
                }).length;
                if (($(".treeview").length - 1) == countNotResult) {
                    $(TreeLayer.SELECTORS.divParentTree).hide();
                    var li = `<div class="treeview-not-result"><span>${l("ManagementLayer:NoSearchResult")}</span></div>`;
                    $('.tree-o-i').append(li);
                }
            }
            else {
                TreeLayer.ReloadTree();

                $(TreeLayer.SELECTORS.content_left_sidebar).scrollTop(10);

                $(TreeLayer.SELECTORS.divParentTree).show();
                $(TreeLayer.SELECTORS.treeview).show();
                if (TreeLayer.GLOBAL.DirectoryForcus != "") {

                    TreeLayer.TriggerForcusToTreeNode(TreeLayer.GLOBAL.DirectoryForcus);

                    $('#item-li-' + TreeLayer.GLOBAL.DirectoryForcus).addClass('forcus');
                    $('#tree-item-' + TreeLayer.GLOBAL.DirectoryForcus).find('.icon-tree-' + TreeLayer.GLOBAL.DirectoryForcus).addClass('open'); // mở open icon
                    $('#tree-ul-' + TreeLayer.GLOBAL.DirectoryForcus).addClass('open'); // mở nhánh con
                    $('#tree-ul-' + TreeLayer.GLOBAL.DirectoryForcus).slideDown();
                    $('#tree-ul-' + TreeLayer.GLOBAL.DirectoryForcus + " li").show();
                }
                else {
                    $(TreeLayer.SELECTORS.divParentTree).click();
                }
            }
        }, 500));
    },
    showButton: function (_create, _edit, _delete, _permission, _type) {
        if (TreeView.GLOBAL.isAdmin == "False") {

            if (_type == false || _type == "false") {
                if (_create == "true" || _create == true) {
                    $(TreeView.SELECTORS.addFolder).css('display', 'inline-flex');
                    $(TreeView.SELECTORS.addLayer).css('display', 'inline-flex');

                    // show right line
                    $('.right-line-horizel[data-for="addLayer"]').show();
                    $('.right-line-horizel[data-for="addFolder"]').show();

                } else {
                    $(TreeView.SELECTORS.addFolder).css('display', 'none'); // thêm mới thư mục
                    $(TreeView.SELECTORS.addLayer).css('display', 'none'); // thêm mới layer

                    // hide right line
                    $('.right-line-horizel[data-for="addLayer"]').hide();
                    $('.right-line-horizel[data-for="addFolder"]').hide();
                }

                if (_edit == "true" || _edit == true) {
                    $(config.SELECTORS.btnSave).css('display', 'block'); // nút lưu
                    $(config.SELECTORS.addRow).css('display', 'block'); // nút thêm một thuộc tính
                    $(TreeView.SELECTORS.InforData).css('display', 'inline-flex');
                    $(TreeView.SELECTORS.btnSetting).css('display', 'inline-flex'); // nút cấu hình dữ liệu
                    $(TreeView.SELECTORS.editFolder).css('display', 'inline-flex'); // nút sửa tên thư mục

                    // show right line
                    if ($(TreeView.SELECTORS.addLayer).css('display') != "none" || $(TreeView.SELECTORS.btnSetting).css('display') != "none") // nếu một trong 2 btn thêm mới layer và setting layer đang hiển thị, thì show line
                    {
                        $('.right-line-horizel-primary[data-for="SettingLayer"]').show();
                    }

                    // show right line
                    if ($(TreeView.SELECTORS.addFolder).css('display') != "none" || $(TreeView.SELECTORS.editFolder).css('display') != "none") {// nếu một trong 2 btn thêm mới thưc mục và edit thư mục đang hiển thị, thì show line
                        $('.right-line-horizel-primary[data-for="editFolder"]').show();
                    }


                } else {
                    $(config.SELECTORS.btnSave).css('display', 'none');
                    $(config.SELECTORS.addRow).css('display', 'none');
                    $(TreeView.SELECTORS.InforData).css('display', 'none');
                    $(TreeView.SELECTORS.btnSetting).css('display', 'none'); // nút cấu hình dữ liệu
                    $(TreeView.SELECTORS.editFolder).css('display', 'none'); // nút sửa tên thư mục

                    // hide right line
                    if ($(TreeView.SELECTORS.addLayer).css('display') == "none" && $(TreeView.SELECTORS.btnSetting).css('display') == "none") // nếu 2 btn thêm mới layer và setting layer đang ẩn, thì ẩn line
                    {
                        $('.right-line-horizel-primary[data-for="SettingLayer"]').hide();
                    }

                    if ($(TreeView.SELECTORS.addFolder).css('display') == "none" && $(TreeView.SELECTORS.editFolder).css('display') == "none") // nếu 2 btn thêm mới thư mục và edit thư mục đang ẩn, thì ẩn line
                    {
                        $('.right-line-horizel-primary[data-for="editFolder"]').hide();
                    }

                }

                if (_delete == "true" || _delete == true) {
                    $(TreeView.SELECTORS.btnDeleteFolder).css('display', 'inline-flex');
                } else {
                    $(TreeView.SELECTORS.btnDeleteFolder).css('display', 'none');
                }

                if (_permission == "true" || _permission == true) {
                    $(TreeView.SELECTORS.btnPermission).css('display', 'inline-flex');

                    // show right line
                    $('.right-line-horizel[data-for="addPermissionLayer"]').show();

                } else {
                    $(TreeView.SELECTORS.btnPermission).css('display', 'none');

                    // hide right line
                    $('.right-line-horizel[data-for="addPermissionLayer"]').hide();
                }
            } else {
                if (_create == "true" || _create == true) {
                    $(TreeView.SELECTORS.addFolder).css('display', 'inline-flex');
                    $(TreeView.SELECTORS.addLayer).css('display', 'inline-flex');

                    // show right line
                    $('.right-line-horizel[data-for="addLayer"]').show();
                    $('.right-line-horizel[data-for="addFolder"]').show();

                } else {
                    $(TreeView.SELECTORS.addFolder).css('display', 'none');
                    $(TreeView.SELECTORS.addLayer).css('display', 'none');

                    // hide right line
                    $('.right-line-horizel[data-for="addLayer"]').hide();
                    $('.right-line-horizel[data-for="addFolder"]').hide();
                }

                if (_edit == "true" || _edit == true) {
                    $(TreeView.SELECTORS.editFolder).css('display', 'inline-flex'); // nút sửa tên thư mục
                    $(TreeView.SELECTORS.btnSetting).css('display', 'inline-flex'); // nút cấu hình dữ liệu

                    // show right line
                    if ($(TreeView.SELECTORS.addLayer).css('display') != "none" || $(TreeView.SELECTORS.btnSetting).css('display') != "none") // nếu một trong 2 btn thêm mới layer và setting layer đang hiển thị, thì show line
                    {
                        $('.right-line-horizel-primary[data-for="SettingLayer"]').show();
                    }

                    // show right line
                    if ($(TreeView.SELECTORS.addFolder).css('display') != "none" || $(TreeView.SELECTORS.editFolder).css('display') != "none") {// nếu một trong 2 btn thêm mới thưc mục và edit thư mục đang hiển thị, thì show line
                        $('.right-line-horizel-primary[data-for="editFolder"]').show();
                    }
                }
                else {
                    $(TreeView.SELECTORS.editFolder).css('display', 'none'); // nút sửa tên thư mục
                    $(TreeView.SELECTORS.btnSetting).css('display', 'none'); // nút cấu hình dữ liệu

                    // hide right line
                    if ($(TreeView.SELECTORS.addLayer).css('display') == "none" && $(TreeView.SELECTORS.btnSetting).css('display') == "none") // nếu 2 btn thêm mới layer và setting layer đang ẩn, thì ẩn line
                    {
                        $('.right-line-horizel-primary[data-for="SettingLayer"]').hide();
                    }

                    if ($(TreeView.SELECTORS.addFolder).css('display') == "none" && $(TreeView.SELECTORS.editFolder).css('display') == "none") // nếu 2 btn thêm mới thư mục và edit thư mục đang ẩn, thì ẩn line
                    {
                        $('.right-line-horizel-primary[data-for="editFolder"]').hide();
                    }
                }

                if (_delete == "true" || _delete == true) {
                    $(TreeView.SELECTORS.btnDeleteFolder).css('display', 'inline-flex');
                } else {
                    $(TreeView.SELECTORS.btnDeleteFolder).css('display', 'none');
                }

                if (_permission == "true" || _permission == true) {
                    $(TreeView.SELECTORS.btnPermission).css('display', 'inline-flex');

                    // show right line
                    $('.right-line-horizel[data-for="addPermissionLayer"]').show();
                } else {
                    $(TreeView.SELECTORS.btnPermission).css('display', 'none');

                    // hide right line
                    $('.right-line-horizel[data-for="addPermissionLayer"]').hide();
                }

                $(config.SELECTORS.btnSave).css('display', 'none'); // nút lưu
                $(config.SELECTORS.addRow).css('display', 'none'); // nút thêm một thuộc tính
                $(TreeView.SELECTORS.InforData).css('display', 'none');
            }
        } else {
            $(TreeView.SELECTORS.addFolder).css('display', 'inline-flex');
            $(TreeView.SELECTORS.addLayer).css('display', 'inline-flex');
            // show right line
            $('.right-line-horizel[data-for="addLayer"]').show();
            $('.right-line-horizel[data-for="addFolder"]').show();

            $(config.SELECTORS.btnSave).css('display', 'block'); // nút lưu
            $(config.SELECTORS.addRow).css('display', 'block'); // nút thêm một thuộc tính

            $(TreeView.SELECTORS.InforData).css('display', 'inline-flex');
            $(TreeView.SELECTORS.btnSetting).css('display', 'inline-flex'); // nút cấu hình dữ liệu
            $(TreeView.SELECTORS.editFolder).css('display', 'inline-flex'); // nút sửa tên thư mục
            $(TreeView.SELECTORS.btnDeleteFolder).css('display', 'inline-flex');
            $(TreeView.SELECTORS.btnPermission).css('display', 'inline-flex');

            // show right line
            if ($(TreeView.SELECTORS.addLayer).css('display') != "none" || $(TreeView.SELECTORS.btnSetting).css('display') != "none") // nếu một trong 2 btn thêm mới layer và setting layer đang hiển thị, thì show line
            {
                $('.right-line-horizel-primary[data-for="SettingLayer"]').show();
            }

            $('.right-line-horizel[data-for="addPermissionLayer"]').show();

            if ($(TreeView.SELECTORS.addFolder).css('display') != "none" || $(TreeView.SELECTORS.editFolder).css('display') != "none") {// nếu một trong 2 btn thêm mới thưc mục và edit thư mục đang hiển thị, thì show line
                $('.right-line-horizel-primary[data-for="editFolder"]').show();
            }
        }
    },
    GetListIdLayerPermission: function (data) {
        var lst = data.filter(x => TreeView.GLOBAL.lstIdLayer.indexOf(x.id) >= 0);
        var lstcheck = [];
        $.each(lst, function (i, obj) {
            let listid = TreeLayer.getListIdParent(obj, data);
            lstcheck = lstcheck.concat(listid);
        });

        TreeView.GLOBAL.ListIdShow = [...new Set(lstcheck)];
    },
    getListIdParent: function (obj, data) {
        let lst = [obj.id];
        if (obj.parentId != "" && obj.parentId != null) {
            let check = data.find(x => x.id == obj.parentId);
            if (check != null && check != undefined) {
                let lstcheck = TreeLayer.getListIdParent(check, data);
                lst = lst.concat(lstcheck);
            }
        }
        return lst;
    },
    GetListLayer: function (idfocus) {
        $.ajax({
            type: "GET",
            url: TreeLayer.CONSTS.URL_GETLISTDIRECTORY,
            data: {
            },
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    if (data.length > 0) {
                        $(TreeLayer.SELECTORS.search_tree).val(""); // reset search
                        //TreeLayer.GLOBAL.ArrayTree = data;
                        if (TreeView.GLOBAL.isAdmin == "True") {
                            TreeLayer.GLOBAL.ArrayTree = data;
                        } else {
                            TreeLayer.GetListIdLayerPermission(data);
                            TreeLayer.GLOBAL.ArrayTree = data.filter(x => TreeView.GLOBAL.ListIdShow.includes(x.id) || x.level == "0");
                        }
                        listFolder = TreeLayer.GLOBAL.ArrayTree;
                        var parent = TreeLayer.GLOBAL.ArrayTree.find(x => x.level == "0");
                        var treedynamic = TreeLayer.FillDynaTree(parent);

                        var count = TreeLayer.GLOBAL.ArrayTree.filter(x => x.level == "1");
                        $(TreeLayer.SELECTORS.nameFolderParent).text(` ${parent.name} (${count.length})`);
                        $(TreeLayer.SELECTORS.divParentTree).attr("data-id", parent.id);
                        config.GLOBAL.idDirectory = parent.id;
                        TreeLayer.GLOBAL.DirectoryForcus = parent.id;
                        thisId = parent.id;
                        parentId = parent.id;
                        level = 1;

                        let html = '';
                        html = TreeLayer.DataTreeGen(treedynamic, false);

                        $(`.tree-o-i`).html(html);

                        $(`.item-li`).each(function () {
                            if ($(this).find('.title-item').text() == "") {
                                $(this).remove();
                            }
                            var isFolder = $(this).attr('data-type');
                            var id = $(this).attr('data-id');

                        });

                        if (idfocus != undefined && idfocus != "") {
                            TreeLayer.TriggerForcusToTreeNode(idfocus);
                        }
                        else {
                            var url = new URL(window.location.href);
                            var checkid = url.searchParams.get("id");

                            TreeLayer.TriggerForcusToTreeNode(checkid);
                        }
                    }
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    //dynatree
    FillDynaTree: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            icon: null,
            level: data.level,
            parentId: data.parentId
        };

        var parent = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    obj.icon = data.image2D;
                }
            }

            obj.key = parent.id;

            var lstChil = TreeLayer.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = TreeLayer.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    // autoGen
    DataTreeGen: function (data, isShow) {
        //$('.title').html(data.title);

        var tree = data.children;

        var html = ``;

        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;
            if (tree[i].icon == "" || tree[i].icon == null) {
                /* icon = `<i class="fa ${tree[i].isFolder ? "fa-folder-open" : "fa-file"} text-brand"></i>`;*/
                if (tree[i].isFolder) {
                    icon = `<svg id="Group_1232" data-name="Group 1232" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                              <path id="Path_388" data-name="Path 388" d="M0,0H24V24H0Z" fill="none"/>
                              <path id="Path_389" data-name="Path 389" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H4V19l2-8H22.5l-2.31,9.243a1,1,0,0,1-.97.757Z" fill="var(--primary)"/>
                            </svg>`;
                }
                else {
                    icon = `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                <defs></defs>
                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z"></path>
                                <path class="b " style="fill:var(--primary)" d="M21,9V20.993A1,1,0,0,1,20.007,22H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,4,2H14V8a1,1,0,0,0,1,1Zm0-2H16V2Z"></path>
                            </svg>`;
                }
            }
            var arr = [];
            var obj = TreeView.GLOBAL.lstLayerPermission.find(x => x.idDirectory == tree[i].key);
            if (obj != null && obj != undefined) {
                arr = obj.listPermisson;
            }

            if (tree[i].children.length > 0) {
                html += `<li data-id="${tree[i].key}" data-folder="${tree[i].isFolder}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${encodeURIComponent(name.toLowerCase())}" data-parent="${tree[i].parentId}" 
                            data-type="${tree[i].isFolder}" data-level="${tree[i].level}" data-chil="${tree[i].children.length}">

                            <a href="javascript:;" class="item-li" id="item-li-${tree[i].key}" data-type="${tree[i].isFolder}" 
                                    data-chil="${tree[i].children.length}" data-id="${tree[i].key}" data-create="${arr.includes("PermissionLayer.CREATE")}" 
                                    data-edit="${arr.includes("PermissionLayer.EDIT")}" data-delete="${arr.includes("PermissionLayer.DELETE")}" 
                                    data-permission="${arr.includes("PermissionLayer.PERMISSION")}">
                                <span class="pull-left-container">
                                    <i class="fa fa-angle-right pull-left icon-menu-tree ${isShow ? "open" : ""} icon-tree-${tree[i].key}"></i>
                                </span>
                                <span class="title-item">${icon} <span class="name-layer-item">${name} (${tree[i].children.length})</span></span>

                                <ul class="tree-ul ${isShow ? "open" : ""}" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" ${isShow ? "" : "style=\"display: none;\""}>`;

                html += TreeLayer.DataTreeGen(tree[i], isShow);

                html += `
                                </ul>
                            </a>
                        </li>`;
            }
            else {
                html += `<li data-id="${tree[i].key}" data-folder="${tree[i].isFolder}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${encodeURIComponent(name.toLowerCase())}"  data-parent="${tree[i].parentId}"
                            data-type="${tree[i].isFolder}" data-level="${tree[i].level}" data-chil="${tree[i].children.length}">

                                <a href="javascript:;" class="item-li" id="item-li-${tree[i].key}" data-type="${tree[i].isFolder}" 
                                        data-chil="${tree[i].children.length}" data-id="${tree[i].key}" data-create="${arr.includes("PermissionLayer.CREATE")}" 
                                        data-edit="${arr.includes("PermissionLayer.EDIT")}" data-delete="${arr.includes("PermissionLayer.DELETE")}" 
                                        data-permission="${arr.includes("PermissionLayer.PERMISSION")}">
                                 <span class="pull-left-container">
                                    
                                </span>
                                <span class="title-item">${icon} <span class="name-layer-item"> ${name}</span></span></a>    
                           </li>`;
            }
        }

        return html;
    },
    GetPropertiesDirectory: function (id) {
        var result = null;
        $.ajax({
            type: "GET",
            url: TreeLayer.CONSTS.URL_GET_PROPERTIES_DIRECTORY + "/" + id + "/asyncById",
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                result = data;
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });

        return result;
    },
    GetInfoTree: function () {
        if ($('#InforData').width() == 0) {
            $('#InforData').animate({
                width: 372,
            }, 200);
        }

        //$(TreeView.SELECTORS.listGroupItem).css('background-color', 'white');
        //var idSelected = thisId;
        //TreeView.setHideOrShowFolder(true);
        //config.GLOBAL.idDirectory = id;
        $.ajax({
            type: "GET",
            url: '/api/HTKT/PropertiesDirectory/get-directoryById',
            data: {
                id: config.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok") {
                    $('#DataName').val(data.result.nameTypeDirectory);
                    $('#h5NameData').html(l("Layer:Name") + ": " + data.result.nameTypeDirectory);
                    $('#DataCode').val(data.result.codeTypeDirectory);
                    $('#Version').val(data.result.verssionTypeDirectory);
                    if (data.result.location == null) {
                        $('#DataLat').val("");
                        $('#DataLng').val("");
                    }
                    else {
                        $('#DataLat').val(data.result.location.lat);
                        $('#DataLng').val(data.result.location.lng);
                    }

                    if (data.result.zoom != null) {
                        $('#DataZoom').val(data.result.zoom);
                    }
                    else {
                        $('#DataZoom').val("");
                    }

                    if (data.result.minZoom != null && data.result.minZoom != 0) {
                        $('#MinZoom').val(data.result.minZoom);
                    }
                    else {
                        $('#MinZoom').val("");
                    }

                    if (data.result.maxZoom != null && data.result.maxZoom != 0) {
                        $('#MaxZoom').val(data.result.maxZoom);
                    }
                    else {
                        $('#MaxZoom').val("");
                    }

                    //$(TreeLayer.SELECTORS.minZoom).val(data.result.minZoom);
                    //$(TreeLayer.SELECTORS.maxZoom).val(data.result.maxZoom);
                    ///set radio option layer
                    optionLayer.setEditOptionLayer(data.result.optionDirectory);
                    //var str = TreeLayer.SELECTORS.optionLayerCheck.replace("VAL", data.result.optionDirectory);
                    //$(str).prop("checked", true);

                    // kế thừa
                    if (data.result.idImplement != "" && data.result.idImplement != null) {
                        $(config.SELECTORS.NameImplement).parent().find('.placeholder').attr('style', 'transform: scale(0.8) translateY(-24px);background: #fff;');
                    }
                    else {
                        $(config.SELECTORS.NameImplement).parent().find('.placeholder').attr('style', '');
                    }

                    // maptile
                    if (data.result.groundMaptile != "" && data.result.groundMaptile != null) {
                        $(TreeView.SELECTORS.inputUrlMaptile).parent().find('.placeholder').attr('style', 'transform: scale(0.8) translateY(-24px);background: #fff;');
                    }
                    else {
                        $(TreeView.SELECTORS.inputUrlMaptile).parent().find('.placeholder').attr('style', '');
                    }

                    // nếu phương thức load dữ liệu không phải là 1 thì readonly đường dẫn maptile
                    if (data.result.optionDirectory === 2) {
                        $(TreeView.SELECTORS.inputUrlMaptile).prop("readonly", true);
                    }
                    else {
                        $(TreeView.SELECTORS.inputUrlMaptile).prop("readonly", false);
                    }

                    $(config.SELECTORS.NameImplement).val(data.result.idImplement);
                    let nameImplement = listFolder.find(x => x.id == data.result.idImplement);
                    nameImplement = typeof nameImplement !== "undefined" && nameImplement !== null ? nameImplement.name + " (" + nameImplement.codeTypeDirectory + ")" : ""
                    $(TreeView.SELECTORS.inputnameImplement).val(nameImplement);
                    $(TreeView.SELECTORS.inputUrlMaptile).val(data.result.groundMaptile);
                    var tags = data.result.tags;

                    if (tags != null) {
                        $(TreeView.SELECTORS.inputBoundMaptile).val(tags["bounds"]);
                    }
                    else {
                        //$(TreeView.SELECTORS.inputBoundMaptile).val(bounds);
                        $(TreeView.SELECTORS.inputBoundMaptile).val("");
                    }

                    var result = [];
                    $.each(data.result.listProperties, function (i, obj) {
                        var element = {
                            NameProperties: obj.nameProperties,
                            CodeProperties: obj.codeProperties,
                            TypeProperties: obj.typeProperties,
                            IsShow: obj.isShow,
                            IsIndexing: obj.isIndexing,
                            IsRequest: obj.isRequest,
                            IsView: obj.isView,
                            IsHistory: obj.isHistory,
                            IsInheritance: obj.isInheritance,
                            IsAsync: obj.isAsync,
                            IsShowExploit: obj.isShowExploit,
                            IsShowSearchExploit: obj.isShowSearchExploit,
                            IsShowReference: obj.isShowReference,
                            DefalutValue: obj.defalutValue,
                            ShortDescription: obj.shortDescription,
                            TypeSystem: obj.typeSystem,
                            OrderProperties: obj.orderProperties
                        };
                        result.push(element);
                    });
                    config.GLOBAL.table = result;
                    var lsttable = config.GLOBAL.table.filter(x => x.TypeProperties != "text" && x.TypeProperties != "string" && x.TypeProperties != "stringlarge" && x.TypeProperties != "float" && x.TypeProperties != "int");
                    $.each(lsttable, function (i, obj) {
                        obj.IsIndexing = false;
                    });
                    config.addTbodyTr();
                    config.setEventAffter();
                }
                else {
                    console.log(data)
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
        if ($(config.SELECTORS.collapseOne).hasClass("collapse show")) {
            $(config.SELECTORS.collapseOne).removeClass('show');
            //$(config.SELECTORS.iconHeadingOne).removeClass('fa-minus').addClass('fa-plus');
            $(config.SELECTORS.iconHeadingOne).html(`<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="width:24px; height:24px;transform: translate(-1%, 4%);"
	                                                         viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                                                        <style type="text/css">
	                                                        .st0{fill:#FFFFFF;}
                                                        </style>
                                                        <path id="Path_6613" class="st0" d="M4.24,1.93h16c0.55,0,1,0.45,1,1v16c0,0.55-0.45,1-1,1h-16c-0.55,0-1-0.45-1-1v-16
	                                                        C3.24,2.37,3.69,1.93,4.24,1.93z M5.24,3.93v14h14v-14H5.24z M7.24,9.93h10v2h-10V9.93z M13.24,5.93v10h-2v-10H13.24z"/>
                                                        </svg>`);
        }
        if (!$(config.SELECTORS.collapseTwo).hasClass("collapse show")) {
            $(config.SELECTORS.collapseTwo).addClass('show');

            $(config.SELECTORS.iconHeadingTwo).html(`<svg id="Component_212_2" data-name="Component 212 – 2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="width:24px; height:24px;">
                                                        <path id="Path_6612" data-name="Path 6612" d="M0,0H24V24H0Z" fill="none"></path>
                                                        <path id="Path_6613" data-name="Path 6613" d="M4,3H20a1,1,0,0,1,1,1V20a1,1,0,0,1-1,1H4a1,1,0,0,1-1-1V4A1,1,0,0,1,4,3ZM5,5V19H19V5Zm2,6H17v2H7Z" fill="#fff"></path>
                                                    </svg>`);
            //$(config.SELECTORS.iconHeadingTwo).removeClass('fa-plus').addClass('fa-minus');
        }
    },
    TriggerForcusToTreeNode: function (id) {
        if (id == undefined || id == null) {
            $(TreeLayer.SELECTORS.divParentTree).trigger("click");
        }
        else {
            var folderParent = TreeLayer.GLOBAL.ArrayTree.find(x => x.id == id);
            if (folderParent != undefined) {
                if (folderParent.level > 0) {

                    $('#item-li-' + id).click();

                    //$(TreeLayer.SELECTORS.tree_i_o + ' .TreeLayer').hide();
                    $('.tree-o-i .treeview').each(function () {
                        //search_tree(this, value);
                        var idlayer = $(this).attr('data-id');
                        var parent = $(this).attr('data-parent');
                        var level = parseInt($(this).attr('data-level'));

                        if (level == 0) {
                            $(this).show();
                        }

                        if (id == idlayer) {
                            $(this).show();
                            if (parent != "null") {
                                $('#tree-ul-' + parent).addClass('open');
                                $('#arrow-item-' + parent).addClass('open');
                                $('#tree-ul-' + parent).slideDown();

                                $($(this).find('.icon-menu-tree')).addClass('open');
                                forcus_tree('#tree-item-' + parent, parent);
                            }
                        }
                    });

                    $(TreeLayer.SELECTORS.content_left_sidebar).scrollTop(10);
                    setTimeout(function () {
                        $(TreeLayer.SELECTORS.content_left_sidebar).animate({
                            scrollTop: $('#item-li-' + id).offset().top - $(TreeLayer.SELECTORS.content_left_sidebar).offset().top
                        }, 'slow');
                    }, 500);

                    //PermissionLayer.GetListPermissionLayerByUserCurrent(id);
                }
            }
        }
    },
    ReloadTree: function () {
        $(TreeLayer.SELECTORS.search_tree).val(""); // reset search
        var parent = TreeLayer.GLOBAL.ArrayTree.find(x => x.level == "0");
        var treedynamic = TreeLayer.FillDynaTree(parent);

        var html = TreeLayer.DataTreeGen(treedynamic);

        $(`.tree-o-i`).html('');
        $(`.tree-o-i`).html(html);

        $(`.item-li`).each(function () {
            if ($(this).find('.title-item').text() == "") {
                $(this).remove();
            }

            var isFolder = $(this).attr('data-type');
            var id = $(this).attr('data-id');

            //if (isFolder != "true") {
            //    var checkbox = `<label class="container-checkbox">                                             
            //                      <span class="checkmark" id="checkmark-${id}" data-id="${id}"></span>
            //                    </label>`;

            //    $(this).parent().prepend(checkbox);
            //}
        });
    },

    // mở lớp truyền vào
    OpenCurrentData: function () {

        var url = new URL(window.location.href);
        var checkid = url.searchParams.get("id");
        if (checkid !== null) {
            thisId = checkid;
            if (!$(`#tree-ul-${checkid}`).hasClass('open')) {
                setTimeout(function () {
                    $(TreeLayer.SELECTORS.content_left_sidebar).animate({
                        scrollTop: $('#item-li-' + checkid).offset().top - $(TreeLayer.SELECTORS.content_left_sidebar).offset().top
                    }, 'slow');

                    $('#item-li-' + checkid).click();
                    $('#item-li-' + checkid).addClass('active');
                    config.GLOBAL.idDirectory = checkid;
                    //TreeView.ShowInfoData();

                }, 500)

                var parent = $('#item-li-' + checkid).parent().attr('data-parent');

                if (parent != "null") {
                    var chil = parseInt($('#item-li-' + parent).attr('data-chil'));
                    if (chil > 0) {
                        $('#tree-ul-' + parent).addClass('open');
                        $('#tree-ul-' + parent).slideDown();
                        OpenTree($('#item-li-' + parent));
                    }
                }
            }
            else {
                $(TreeView.SELECTORS.tree_i_o + " " + TreeView.SELECTORS.item_tree_i_o).eq(0).click();
            }
        }

        function OpenTree(element) {
            var parent = $(element).parent().attr('data-parent');
            var icon = $(element).find('.icon-menu-tree');
            if (icon.length > 0) {
                icon.addClass('open')
            }

            if (parent != "null") {
                var chil = parseInt($('#item-li-' + parent).attr('data-chil'));
                if (chil > 0) {
                    $('#tree-ul-' + parent).addClass('open');
                    $('#tree-ul-' + parent).slideDown();

                    OpenTree($('#item-li-' + parent));
                }
            }
        }
    },

    //htmlEntities: function (str) {
    //    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    //},
}

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    str = str.replace(/\s/g, '');
    return str;
}

function forcus_tree(element, id) {
    $(element).show();
    var idlayer = $(element).attr('data-id');
    var parent = $(element).attr('data-parent');
    var level = parseInt($(element).attr('data-level'));

    if (level == 0) {
        $(this).show();
    }

    if (idlayer != undefined) {
        if (idlayer == id) {
            if (parent != "null") {
                // hiển thị ul của cây
                $('#tree-ul-' + parent).slideDown();
                $('#tree-ul-' + parent).addClass('open');
                $('#arrow-item-' + parent).addClass('open');

                forcus_tree('#tree-item-' + parent, parent);
            }
        }
    }

}

function search_tree(element, value) {
    if ($(element).css("display") == "none") {
        $(element).show();
        var id = $(element).attr('data-id');
        var name = decodeURIComponent($(element).attr('data-search'));
        var parent = $(element).attr('data-parent');
        var level = parseInt($(this).attr('data-level'));
        if (level == 0) {
            $(this).show();
        }

        if (name != undefined) {
            //if (name.toLowerCase().includes(value))
            {
                if (parent != "null") {
                    // hiển thị ul của cây
                    $('#tree-item-' + parent).find('.icon-tree-' + parent).addClass('open'); // mở open icon
                    $('#tree-ul-' + parent).addClass('open'); // mở nhánh con
                    $('#tree-ul-' + parent).slideDown();

                    search_tree('#tree-item-' + parent, value);
                }
            }
        }
    }
}