﻿mn_selected = "mn_layerbasemap";

var l = abp.localization.getResource('HTKT');
var _layerBaseMapAppService = ioT.hTKT.layerBaseMap;
var _permissionsModal = new abp.ModalManager(abp.appPath + 'HTKTPermissions/LayerBaseMap');

var LayerBaseMap =
{
    /*
        * Global Objects
     */
    GLOBAL:
    {
        table: "",
        ImageChoosen: '',
        OldImage: '',
        ArrayMap: []
    },

    /*
        * CONSTANTS  Objects
     */
    CONSTANTS:
    {
        URL_UPLOAD_IMAGE: '/api/htkt/file/image',
        URL_DELETE_IMAGE: '/api/htkt/file/deleteimage',
        URL_CREATE: '/api/htkt/layerBaseMap/create-layer-base-map',
        URL_UPDATE: '/api/htkt/layerBaseMap/update-layer-base-map',
        URL_DELETE: '/api/app/layer-base-map/',
        URL_GET_LIST_MAP: '/api/htkt/layerBaseMap/get-list',
        URL_CHECK_ORDER: "/api/htkt/layerBaseMap/check-exits-order"
    },

    /*
        * SELECTORS OBJECT
     */
    SELECTORS:
    {
        table: '.table',
        content_Input: ".content-input",
        btn_Save: "#btn-save",
        btn_Image: ".open-image",
        image_layer_map: "#img-layer-map",
        btn_edit: ".edit",
        btn_delete: ".delete",
        btn_huy_edit: "#btn-huy-edit",
        // form
        Input_Form: ".form-body",
        Input_Id: "#idLayerBaseMap",
        Input_Name: "#nameLayerBaseMap",
        Input_Link: "#linkLayerBaseMap",
        Input_Order: "#orderLayerBaseMap",
        Input_Image: "#image",
        btn_edit_row: ".edit-row",
        btn_delete_row: ".delete-row"
    },
    /*
        * Init function
     */
    init: function () {
        this.setCollumsTalbe();
        this.setDataTable();
        this.setUpEvent();
    },
    /*
       * Set up event
    */
    setUpEvent: function () {
        $(LayerBaseMap.SELECTORS.btn_Save).on('click', function () {
            if (LayerBaseMap.checkFormInfor()) {
                var checkDeleteImage = true;
                //if (abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Create') || abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Edit')) // có phân quyền thêm mới và cập nhật
                if (abp.auth.isGranted('HTKT.Administration.LayerBaseMap')) // có phân quyền
                {
                    if (LayerBaseMap.GLOBAL.OldImage != "" && $(LayerBaseMap.SELECTORS.Input_Image).val() != "") {
                        checkDeleteImage = LayerBaseMap.DeleteFileImage(LayerBaseMap.GLOBAL.OldImage);
                    }

                    //if (checkDeleteImage)
                    {
                        var checkUploadImage = {
                            check: true,
                            url: LayerBaseMap.GLOBAL.OldImage
                        };

                        if ($(LayerBaseMap.SELECTORS.Input_Image).val() != "") {
                            //if (!abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Create') && $(LayerBaseMap.SELECTORS.Input_Id).val() == "") {
                            if (!abp.auth.isGranted('HTKT.Administration.LayerBaseMap') && $(LayerBaseMap.SELECTORS.Input_Id).val() == "") {
                                return;
                            }
                            else {
                                checkUploadImage = LayerBaseMap.UploadFileImage();
                            }
                        }

                        if (checkUploadImage.check) {
                            var url = LayerBaseMap.CONSTANTS.URL_CREATE;
                            var message = l('ManagementLayer:AddNew');

                            //if (!abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Create')) // kiểm phân quyền thêm mới
                            if (!abp.auth.isGranted('HTKT.Administration.LayerBaseMap')) // kiểm tra phân quyền
                            {
                                url = '';
                                message = '';
                            }

                            if ($(LayerBaseMap.SELECTORS.Input_Id).val() != "") {
                                url = LayerBaseMap.CONSTANTS.URL_UPDATE;
                                message = l('LayerBaseMap:Edit');

                                //if (!abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Edit') && !abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Create')) // kiểm tra phân quyền
                                if (!abp.auth.isGranted('HTKT.Administration.LayerBaseMap')) // kiểm tra phân quyền
                                {
                                    url = '';
                                    message = '';
                                }
                            }

                            var dto = {
                                id: $(LayerBaseMap.SELECTORS.Input_Id).val(),
                                nameBasMap: $(LayerBaseMap.SELECTORS.Input_Name).val(),
                                link: $(LayerBaseMap.SELECTORS.Input_Link).val(),
                                order: $(LayerBaseMap.SELECTORS.Input_Order).val(),
                                image: checkUploadImage.url
                            }

                            if (url != "") {
                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    contentType: "application/json; charset=utf-8",
                                    url: url,
                                    data: JSON.stringify(dto),
                                    success: function (result) {
                                        if (result.code == "ok") {
                                            abp.notify.success(message + " " + l('ManagementLayer:BaseMapSuccessfully'));

                                            // reset form input
                                            LayerBaseMap.clearLabelError();

                                            LayerBaseMap.ResetFormInput();

                                            if (!$(LayerBaseMap.SELECTORS.btn_huy_edit).hasClass('hidden')) {
                                                $(LayerBaseMap.SELECTORS.btn_huy_edit).addClass('hidden');
                                            }

                                            LayerBaseMap.GLOBAL.table.ajax.reload();
                                            //$('input').val('');
                                        }
                                        else {
                                            abp.notify.error(message + l('ManagementLayer:BaseMapFailed'));
                                        }
                                    },
                                    error: function () {
                                    }
                                });
                            }
                        }
                    }
                }
            }
        });

        $(LayerBaseMap.SELECTORS.Input_Form).on('keyup change', 'input', function () {
            //LayerBaseMap.clearLabelError();
            if ($(this).val() != "") {
                $(this).parents(".form-group").find(".lable-error").remove();
                $(this).parents(".form-group").removeClass("has-error");
            }
        });

        // thay đổi hình
        $(LayerBaseMap.SELECTORS.Input_Image).on('change', function () {
            var files = $(this)[0].files;
            if (this.files.length > 0) {
                if (files[0].size <= sizeFiles.sizeImgNormal) {
                    var exp = files[0].name.substring(files[0].name.lastIndexOf(".") + 1).toLowerCase();
                    if (exp == "png" || exp == "jpg" || exp == "jpeg" || exp == "gif") {
                        LayerBaseMap.GLOBAL.ImageChoosen = $(this).val();
                        $('.error-image').addClass('hidden');
                        LayerBaseMap.readURL(this, LayerBaseMap.SELECTORS.image_layer_map);
                    }
                    else {
                        $('.error-image').removeClass('hidden');
                        $('.error-image').html(jQuery.validator.format(l("Layer:ValidFormat"), 'File'));
                    }
                }
                else {
                    $('.error-image').removeClass('hidden');
                    $('.error-image').html(l('ManagementLayer:ValidSizeFileImgNormal'));
          
                }
            }
            else {
                $('.error-image').removeClass('hidden');
                $(LayerBaseMap.SELECTORS.image_layer_map).attr('src', "");
                $('.error-image').html(l("ManagementLayer:PleaseSelectImage"));
            }
        });

        // chọn hình
        $(LayerBaseMap.SELECTORS.btn_Image).on('click', function () {
            $(LayerBaseMap.SELECTORS.Input_Image).trigger('click');
        })

        // hủy cập nhật
        $(LayerBaseMap.SELECTORS.btn_huy_edit).on('click', function () {
            swal({
                title: l('ManagementLayer:Notification'),
                text: l('ManagementLayer:AreYouCancel'),
                icon: "warning",
                buttons: [l('ManagementLayer:Close'), l('ManagementLayer:Agree')]
            }).then((value) => {
                if (value) {
                    $(this).addClass('hidden');
                    LayerBaseMap.ResetFormInput();
                }
            });
        })

        $(document).bind("ajaxSend", function () {
            $('button').prop('disabled', true);
        }).bind("ajaxComplete", function () {

            $('button').prop('disabled', false);
        });

        // sửa hàng
        $(LayerBaseMap.SELECTORS.table).on('click', LayerBaseMap.SELECTORS.btn_edit_row, function () {
            var id = $(this).attr('data-id');
            $(LayerBaseMap.SELECTORS.btn_huy_edit).removeClass('hidden');
            LayerBaseMap.clearLabelError();
            $('.error-image').addClass('hidden');

            var dataLayer = LayerBaseMap.GLOBAL.ArrayMap.find(x => x.id == id);

            LayerBaseMap.setDataToForm(dataLayer);
        });

        // xóa hàng
        $(LayerBaseMap.SELECTORS.table).on('click', LayerBaseMap.SELECTORS.btn_delete_row, function () {
            var id = $(this).attr('data-id');

            LayerBaseMap.ResetFormInput();
            var dataLayer = LayerBaseMap.GLOBAL.ArrayMap.find(x => x.id == id);
            swal({
                title: l("Layer:Notification"),
                text: l("LayerBaseMap:AreYouDeleteLayerBaseMap"),
                icon: "warning",
                buttons: [
                    l("Layer:Cancel"),
                    l("Layer:Delete")
                ],
                dangerMode: true,
            }).then(function (isConfirm) {

                if (isConfirm) {
                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: LayerBaseMap.CONSTANTS.URL_DELETE + id,
                        success: function (result) {
                            LayerBaseMap.DeleteFileImage(dataLayer.image);
                            abp.notify.success(l("ManagementLayer:DeleteBaseMapSuccessfully"));
                            LayerBaseMap.GLOBAL.table.ajax.reload();

                        },
                        error: function () {
                        }
                    });
                }

            });
            
        });
    },
    checkFormInfor: function () {
        LayerBaseMap.clearLabelError();
        let check = true;
        //if (!validateText(inputName, "text", 0, 0)) {

        if ($(LayerBaseMap.SELECTORS.Input_Name).val() == '') {
            check = false;

            LayerBaseMap.showLabelError(LayerBaseMap.SELECTORS.Input_Name, $(LayerBaseMap.SELECTORS.Input_Name).attr('data-required'));
        }

        var inputLink = $(LayerBaseMap.SELECTORS.Input_Link).val();
        if (inputLink == "") {
            LayerBaseMap.showLabelError(LayerBaseMap.SELECTORS.Input_Link, $(LayerBaseMap.SELECTORS.Input_Link).attr('data-required'));
        }
        else {
            if (!validateText(inputLink, "urlhttps", 0, 0)) {
                check = false;
                LayerBaseMap.showLabelError(LayerBaseMap.SELECTORS.Input_Link, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("ManagementLayer:Path")));
            }
        }

        if (LayerBaseMap.GLOBAL.ImageChoosen == '') {
            check = false;
            $('.error-image').removeClass('hidden');
            //LayerBaseMap.showLabelError("div[class='profile-avatar']", "Vui lòng chọn hình ảnh!");
        }

        var checHtml = LayerBaseMap.CheckedHtmlEntities();
        if (check) {
            check = checHtml;
        }

        return check;
    },
    readURL(input, img) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(img).attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group").addClass('has-error');

        $(element).parents(".form-group").append("<lable class='col-md-12 lable-error' style='color:red; margin-left: -14px;'>" + text + "</lable>");
    },
    clearLabelError: function () {
        $('.form-group').removeClass("has-error");
        $('.form-group').find(".lable-error").remove();
    },
    setCollumsTalbe: function () {
        // action
        abp.ui.extensions.entityActions.get("laybasemap").addContributor(
            function (actionList) {
                return actionList.addManyTail(
                    [
                        {
                            text: l('Edit'),
                            visible: abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Edit'),
                            action: function (data) {
                                $(LayerBaseMap.SELECTORS.btn_huy_edit).removeClass('hidden');
                                LayerBaseMap.clearLabelError();
                                $('.error-image').addClass('hidden');

                                var id = data.record.id;

                                var dataLayer = LayerBaseMap.GLOBAL.ArrayMap.find(x => x.id == id);

                                LayerBaseMap.setDataToForm(dataLayer);
                            }
                        },
                        {
                            text: l('Delete'),
                            visible: abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Delete'),
                            confirmMessage: function (data) {
                                //return l('RoleDeletionConfirmationMessage', data.record.name);
                            },
                            action: function (data) {
                                var id = data.record.id;
                                LayerBaseMap.ResetFormInput();
                                var dataLayer = LayerBaseMap.GLOBAL.ArrayMap.find(x => x.id == id);

                                $.ajax({
                                    type: "DELETE",
                                    dataType: 'json',
                                    contentType: "application/json; charset=utf-8",
                                    url: LayerBaseMap.CONSTANTS.URL_DELETE + id,
                                    success: function (result) {
                                        LayerBaseMap.DeleteFileImage(dataLayer.image);

                                        swal({
                                            title: l("ManagementLayer:Notification"),
                                            text: l("ManagementLayer:DeleteBaseMapSuccessfully"),
                                            icon: "success",
                                            button: l("ManagementLayer:Close"),
                                        });

                                        LayerBaseMap.GLOBAL.table.ajax.reload();

                                    },
                                    error: function () {
                                    }
                                });
                            }
                        }
                    ]
                );
            }
        );

        // collums
        abp.ui.extensions.tableColumns.get("laybasemap").addContributor(
            function (columnList) {
                columnList.addManyTail(
                    [
                        {
                            title: l('ManagementLayer:No.'),
                            data: "STT",
                            render: function (data, type, row, index) {
                                var pageInfo = LayerBaseMap.GLOBAL.table.page.info();
                                var num = pageInfo.start + (index.row + 1);
                                return num;
                            },
                            orderable: false,
                            width: 30,
                            className: 'not-sorting'
                        },
                        {
                            title: l('ManagementLayer:LayerBaseMapName'),
                            data: "nameBasMap",
                            width: 150,
                            render: function (data, type, row) {

                                return `<span>${LayerBaseMap.htmlEntities(data)}</span>`;
                            },
                            orderable: false,
                        },
                        {
                            title: l('ManagementLayer:Path'),
                            data: "link",
                            render: function (data, type, row) {

                                return `<span class="break-line">${data}</span>`;
                            },
                            orderable: false,
                        },
                        {
                            title: l('ManagementLayer:Image'),
                            data: "image",
                            render: function (data, type, row) {

                                var result = `<img src='${data}' style="width: 48px;" />`;

                                return result;
                            },
                            orderable: false,
                            width: 150
                        },
                        {
                            title: l('Actions'),
                            render: function (data, type, row) {
                                var result = '';
                                //if (abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Edit')){
                                    result += `<a class="edit-row" data-id="${row.id}" href="javascript:;" style="margin-right: 5px;"><svg id="Component_97_1" data-name="Component 97 – 1" xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                                                  <g id="Group_3211" data-name="Group 3211" transform="translate(-1591 -247)">
                                                    <rect id="Rectangle_3430" data-name="Rectangle 3430" width="36" height="36" rx="4" transform="translate(1591 247)" fill="#00559a"/>
                                                    <g id="Group_2917" data-name="Group 2917" transform="translate(1600 256)">
                                                      <path id="Path_6610" data-name="Path 6610" d="M0,0H18V18H0Z" fill="none"/>
                                                      <path id="Path_6611" data-name="Path 6611" d="M4.551,15.419h1.1l7.224-7.226-1.1-1.1L4.551,14.322Zm12.41,1.552H3V13.679L13.42,3.256a.775.775,0,0,1,1.1,0l2.194,2.195a.776.776,0,0,1,0,1.1L7.842,15.419h9.119ZM12.872,6l1.1,1.1,1.1-1.1-1.1-1.1L12.872,6Z" transform="translate(-0.98 -0.99)" fill="#fff"/>
                                                    </g>
                                                  </g>
                                            </svg></a>`;
                                //}
                                //if (abp.auth.isGranted('HTKT.Administration.LayerBaseMap.Delete')) {
                                    result += `<a class="delete-row" data-id="${row.id}" href="javascript:;"><svg id="Component_94_1" data-name="Component 94 – 1" xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                                                    <rect id="Rectangle_3431" data-name="Rectangle 3431" width="36" height="36" rx="4" fill="red"/>
                                                    <g id="Group_2931" data-name="Group 2931" transform="translate(9 9)">
                                                    <path id="Path_6552" data-name="Path 6552" d="M0,0H18V18H0Z" fill="none"/>
                                                    <path id="Path_6553" data-name="Path 6553" d="M14.9,5.584V15.621a.717.717,0,0,1-.717.717H4.151a.717.717,0,0,1-.717-.717V5.584H2V4.151H16.338V5.584Zm-10.037,0V14.9h8.6V5.584ZM5.584,2h7.169V3.434H5.584ZM8.452,7.735H9.886v5.018H8.452Z" transform="translate(-0.169 -0.169)" fill="#fff"/>
                                                    </g>
                                                </svg></a>`;
                                //}
                                return result;
                            },
                            orderable: false,
                            width: 100
                        },
                    ]
                );
            },
            0 //adds as the first contributor
        );
    },
    setDataTable: function () {
        LayerBaseMap.GLOBAL.table = $(LayerBaseMap.SELECTORS.table).DataTable(abp.libs.datatables.normalizeConfiguration({
            //order: [[2, "asc"]],
            lengthMenu:
                [
                    [5, 10, 25, 50, 100],
                    ['5', '10', '25', '50', '100']
                ],
            searching: false,
            processing: true,
            scrollX: true,
            serverSide: true,
            paging: true,
            order: [[0, "asc"]],
            scrollCollapse: true,
            headerCallback: function (thead, data, start, end, display) {
                $(thead).closest('thead').find('th').addClass('custom-head-table');
                $('#DataTables_Table_0_length').html(l("LayerBaseMap:TotalRow") + ": " + data.length);
            },
            createdRow: function (row, data, index) {

            },
            //ajax: abp.libs.datatables.createAjax(_testKhaiThacService.getList, ""),
            ajax: {
                url: LayerBaseMap.CONSTANTS.URL_GET_LIST_MAP,
                type: 'GET',
                data: function (d) {
                    //d.filter = $('input.page-search-filter-text').val();
                    d.sorting = d.columns[d.order[0].column].name + " " + d.order[0].dir;
                    d.skipCount = d.start;
                    d.maxResultCount = d.length;

                    delete d.columns;
                    delete d.draw;
                    delete d.length;
                    delete d.order;
                    delete d.search;
                },
                //"dataSrc": "items",
                dataFilter: function (data) {
                    var json = jQuery.parseJSON(data);
                    var result = json;

                    var jsonResult = {};
                    if (result.code == "ok") {
                        var rs = result.result;
                        jsonResult.recordsTotal = rs.totalCount;
                        jsonResult.recordsFiltered = rs.totalCount;
                        jsonResult.data = rs.items;
                        LayerBaseMap.GLOBAL.ArrayMap = rs.items;
                    }
                    else {
                        console.log(json)
                    }
                    return JSON.stringify(jsonResult); // return JSON string
                },
                complete: function (data) {

                }
            },
            columnDefs: abp.ui.extensions.tableColumns.get("laybasemap").columns.toArray(),
        }));
    },
    setDataToForm: function (data) {
        $(LayerBaseMap.SELECTORS.Input_Name).val(data.nameBasMap); // data name
        $(LayerBaseMap.SELECTORS.Input_Link).val(data.link); // data link
        $(LayerBaseMap.SELECTORS.Input_Order).val(data.order); // data order
        $(LayerBaseMap.SELECTORS.Input_Id).val(data.id); // data id
        $(LayerBaseMap.SELECTORS.image_layer_map).attr('src', data.image); // data image
        LayerBaseMap.GLOBAL.ImageChoosen = data.image;
        LayerBaseMap.GLOBAL.OldImage = data.image;
    },
    ResetFormInput: function () {
        $(LayerBaseMap.SELECTORS.Input_Name).val(''); // data name
        $(LayerBaseMap.SELECTORS.Input_Link).val(''); // data link
        $(LayerBaseMap.SELECTORS.Input_Order).val(''); // data order
        $(LayerBaseMap.SELECTORS.Input_Id).val(''); // data id
        $(LayerBaseMap.SELECTORS.Input_Image).val('');
        $(LayerBaseMap.SELECTORS.image_layer_map).attr('src', document.location.origin + '/images/anhMacDinh.png'); // data image
        LayerBaseMap.GLOBAL.ImageChoosen = '';
        LayerBaseMap.GLOBAL.OldImage = '';
    },
    UploadFileImage: function () {
        var obj = {
            check: false,
            url: ""
        };

        var formData = new FormData();
        formData.append('file', $(LayerBaseMap.SELECTORS.Input_Image)[0].files[0]);

        $.ajax({
            url: LayerBaseMap.CONSTANTS.URL_UPLOAD_IMAGE,
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            async: false,
            success: function (res) {
                if (res.code == "ok") {
                    obj.check = true;
                    obj.url = res.result.url;
                }
                else {
                    check = false;
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:DeleteBaseMapFailed"),
                        icon: "error",
                        button: l("ManagementLayer:Close"),
                    })
                }
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });

        return obj;
    },
    DeleteFileImage: function (url) {
        var urlEnCode = encodeURIComponent(url);
        var check = false;
        $.ajax({
            url: LayerBaseMap.CONSTANTS.URL_DELETE_IMAGE,
            type: 'POST',
            data: JSON.stringify({ url: urlEnCode }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (res) {
                if (res.code == "ok") {
                    check = res.result;
                }
                else {
                    console.log(res)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + ": " + errorThrown);
            }
        });
        return check;
    },
    htmlEntities: function (str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    },
    CheckedHtmlEntities: function () {
        var check = true;
        $('.form-body input[type=text]').each(function () {
            var value = $(this).val();
            let inputName = $(this).attr('data-name');
            if ($(this).val() != "") {
                let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;
                if (re.test(value)) {
                    $(this).parent(".form-group").append("<lable class='col-md-12 lable-error' style='color:red; margin-left: -14px;'>+ " + jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), inputName) + " +</lable>");
                    check = false;
                }
            }
        });
        return check;
    },
    CheckOrder: function () {
        var check = true;
        $.ajax({
            url: LayerBaseMap.CONSTANTS.URL_CHECK_ORDER,
            type: 'GET',
            data: {
                id: $(LayerBaseMap.SELECTORS.Input_Id).val(),
                order: $(LayerBaseMap.SELECTORS.Input_Order).val()
            },
            async: false,
            success: function (res) {
                if (res.code == "ok") {
                    check = res.result;
                }
                else {
                    check = false;
                    console.log(res);
                }
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });

        return check;
    }
};

/*
* Page loaded
*/
$(document).ready(function () {
    LayerBaseMap.init();
});