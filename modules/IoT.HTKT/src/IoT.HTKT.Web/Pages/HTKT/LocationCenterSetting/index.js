﻿mn_selected = "mn_layerbasemap";

var l = abp.localization.getResource('HTKT');

var LocationCenter =
{
    /*
        * Global Objects
     */
    GLOBAL:
    {
        isEdit: false,
        lat: 0,
        lng : 0
    },

    /*
        * CONSTANTS  Objects
     */
    CONSTANTS:
    {
        GET_LOCATION_CENTER: '/api/htkt/location-setting/get-location-center',
        UPDATE_LOCATION: '/api/htkt/location-setting/create-location-center'
    },

    /*
        * SELECTORS OBJECT
     */
    SELECTORS:
    {
        Input_Lat: 'input[name="LatCenter"]',
        Input_Lng: 'input[name="LngCenter"]',
        btn_save: '#btn-save',
        btn_cancel: '#btn-cancel',
        btn_edit: '#btn-edit',
        Input_Form: '.form-body',
        group_submit:".group-submit"
    },
    /*
        * Init function
     */
    init: function () {
        this.GetCurrentLocation();
        this.setUpEvent();
    },
    /*
       * Set up event
    */
    setUpEvent: function () {
        $(LocationCenter.SELECTORS.btn_save).on('click', function () {
            if (LocationCenter.checkFormInfor()) {
                LocationCenter.UpdateLocation();
            }
        });

        $(LocationCenter.SELECTORS.Input_Form).on('keyup change', 'input', function () {
            let value = this.value;
            var regex = /^[0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = value.replace(/[a-zA-Z]+/, ''); // xóa chữ
                value = value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                this.value = value;
            }

            if ($(this).val() != "") {
                $(this).parents(".form-group").find(".lable-error").remove();
                $(this).parents(".form-group").removeClass("has-error");
            }
        });

        $(LocationCenter.SELECTORS.Input_Form).on('focusout', 'input', function () {
            let value = this.value;
            var regex = /^[0-9_]+$/;
            if (regex.test(this.value) !== true) {
                value = value.replace(/[a-zA-Z]+/, ''); // xóa chữ
                value = value.replace(/ /g, ""); // xóa khoảng trắng
                value = value.replace(/[~`!@@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                this.value = value;
            }

            value = isNaN(parseFloat(value)) ? '' : parseFloat(value).toFixed(6);
            this.value = value;
        });

        $(LocationCenter.SELECTORS.btn_cancel).on('click', function () {
            LocationCenter.GLOBAL.isEdit = !LocationCenter.GLOBAL.isEdit;
            LocationCenter.ReloadForm();
            $(LocationCenter.SELECTORS.Input_Lat).val(LocationCenter.GLOBAL.lat).trigger('change');
            $(LocationCenter.SELECTORS.Input_Lng).val(LocationCenter.GLOBAL.lng).trigger('change');

        });

        $(LocationCenter.SELECTORS.btn_edit).on('click', function () {
            LocationCenter.GLOBAL.isEdit = !LocationCenter.GLOBAL.isEdit;
            LocationCenter.ReloadForm();
            LocationCenter.GLOBAL.lat = $(LocationCenter.SELECTORS.Input_Lat).val();
            LocationCenter.GLOBAL.lng = $(LocationCenter.SELECTORS.Input_Lng).val();
        });
    },
    checkFormInfor: function () {
        var check = true;
        LocationCenter.clearLabelError();

        if ($(LocationCenter.SELECTORS.Input_Lat).val() == '') {
            check = false;

            LocationCenter.showLabelError(LocationCenter.SELECTORS.Input_Lat, $(LocationCenter.SELECTORS.Input_Lat).attr('data-required'));
        }
        else {
            var floatLat = parseFloat($(LocationCenter.SELECTORS.Input_Lat).val());

            if (!(floatLat >= 8.4 && floatLat <= 23.6)) {
                check = false;
                LocationCenter.showLabelError(LocationCenter.SELECTORS.Input_Lat, "Vĩ độ chỉ được nằm trong khoảng từ 8.4 đến 23.6");
            }
        }

        if ($(LocationCenter.SELECTORS.Input_Lng).val() == '') {
            check = false;

            LocationCenter.showLabelError(LocationCenter.SELECTORS.Input_Lng, $(LocationCenter.SELECTORS.Input_Lng).attr('data-required'));
        }
        else {
            var floatLat = parseFloat($(LocationCenter.SELECTORS.Input_Lng).val());

            if (!(floatLat >= 102 && floatLat <= 118)) {
                check = false;
                LocationCenter.showLabelError(LocationCenter.SELECTORS.Input_Lng, "Kinh độ chỉ được nằm trong khoảng từ 102 đến 118");
            }
        }

        return check;
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group").addClass('has-error');

        $(element).parents(".form-group").append("<lable class='col-md-12 lable-error' style='color:red; margin-left: -14px;'>" + text + "</lable>");
    },
    clearLabelError: function () {
        $('.form-group').removeClass("has-error");
        $('.form-group').find(".lable-error").remove();
    },
    ResetFormInput: function () {
        $(LocationCenter.SELECTORS.Input_Lat).val(''); // data name
        $(LocationCenter.SELECTORS.Input_Lng).val(''); // data link
    },
    htmlEntities: function (str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    },
    CheckedHtmlEntities: function () {
        var check = true;
        $('.form-body input[type=text]').each(function () {
            var value = $(this).val();
            let inputName = $(this).attr('data-name');
            if ($(this).val() != "") {
                let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;
                if (re.test(value)) {
                    $(this).parent(".form-group").append("<lable class='col-md-12 lable-error' style='color:red; margin-left: -14px;'>+ " + jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), inputName) + " +</lable>");
                    check = false;
                }
            }
        });
        return check;
    },
    GetCurrentLocation: function () {
        $.ajax({
            type: "GET",
            url: LocationCenter.CONSTANTS.GET_LOCATION_CENTER,
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok") {
                    $(LocationCenter.SELECTORS.Input_Lat).val(data.result.latitude);
                    $(LocationCenter.SELECTORS.Input_Lng).val(data.result.longitude);
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    UpdateLocation: function () {
        var data = {
            latitude: $(LocationCenter.SELECTORS.Input_Lat).val(),
            longitude: $(LocationCenter.SELECTORS.Input_Lng).val()
        };

        $.ajax({
            url: LocationCenter.CONSTANTS.UPDATE_LOCATION,
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            async: true,
            success: function (res) {
                if (res.code == 'ok') {
                    abp.notify.success(l("Lưu thành công"));
                }
                else {
                    abp.notify.success(l("Lưu không thành công"));
                }

                LocationCenter.GLOBAL.isEdit = !LocationCenter.GLOBAL.isEdit;
                LocationCenter.ReloadForm();
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });
    },
    ReloadForm: function () {
        if (LocationCenter.GLOBAL.isEdit) {
            $(LocationCenter.SELECTORS.group_submit).removeClass('hide');
            $(LocationCenter.SELECTORS.btn_edit).addClass('hide');

            $(LocationCenter.SELECTORS.Input_Lat).attr('readonly', false);
            $(LocationCenter.SELECTORS.Input_Lng).attr('readonly', false);
        }
        else {
            $(LocationCenter.SELECTORS.group_submit).addClass('hide');
            $(LocationCenter.SELECTORS.btn_edit).removeClass('hide');

            $(LocationCenter.SELECTORS.Input_Lat).attr('readonly', true);
            $(LocationCenter.SELECTORS.Input_Lng).attr('readonly', true);
        }
    }
};

/*
* Page loaded
*/
$(document).ready(function () {
    LocationCenter.init();
});