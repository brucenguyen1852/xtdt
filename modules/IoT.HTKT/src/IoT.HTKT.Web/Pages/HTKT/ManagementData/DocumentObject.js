﻿var l = abp.localization.getResource('HTKT');
var documentObject = {
    GLOBAL: {
        lstDocument: [],
        lstOrder: [],
        lstIcon: { 'word': 'fa fa-file-word-o', 'excel': 'fa fa-file-excel-o"', 'ppt': 'fa fa-file-powerpoint-o', 'pdf': 'fa fa-file-pdf-o', 'file': 'fa fa-file-o' }
    },
    CONSTS: {

    },
    SELECTORS: {
        listFileUpload: ".list-file-upload",
        liListFile: ".li-list-file",
        iconUpload: ".file-upload-content .upload-file-icon",
        divBorderFile: ".div-border-file",
        fileActive: ".file-active",
        btnDownload: "#btn-download-document",
        btnView: "#btn-view-document",
        btnDeleted: "#btn-deleted-document"
    },
    init: function () {
        documentObject.setEvent();
    },
    setEvent: function () {
        $(documentObject.SELECTORS.listFileUpload).on('click', documentObject.SELECTORS.iconUpload, function () {
            let code = $(this).attr('data-code');
            modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
            $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val(code);
            $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).attr("disabled", "disabled");
            $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("show");
        });

        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).on("click", modalCreateFolderLayer.SELECTORS.btnSaveFile, function () {
            var elementForm = modalCreateFolderLayer.SELECTORS.modalFileUploadInfor;

            if (!Activity.GLOBAL.checkModalActivity && !modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer && modalCreateFolderLayer.checkFile(elementForm)) {
                let thutu = Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim());
                let iddirectoryproperties = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim();
                var checkthutu = documentObject.GLOBAL.lstOrder.find(x => x.NameFolder == iddirectoryproperties && x.ListOrder.includes(thutu));
                let id = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputIdFile}`).val();
                if ((typeof id == "undefined" || id.length <= 0) && typeof checkthutu == "undefined") {
                    let file = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files;
                    modalCreateFolderLayer.SendFileDocument(file[0]);
                    let array = [];
                    let item = {
                        id: mapdata.uuidv4(),
                        idMainObject: mapdata.GLOBAL.idMainObject,
                        nameDocument: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val().trim(),
                        iconDocument: '',
                        urlDocument: modalCreateFolderLayer.GLOBAL.urlUploadFile,
                        nameFolder: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim(),
                        orderDocument: Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim())
                    };
                    array.push(item);
                    documentObject.UpdateFileDocument(array);
                    $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("hide");
                } else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:No.AlreadyExists"),
                        icon: "error",
                        button: l("ManagementLayer:Close"),
                    }).then((value) => {
                    });
                }
            }
        });

        $(documentObject.SELECTORS.listFileUpload).on('click', documentObject.SELECTORS.divBorderFile, function () {
            $(documentObject.SELECTORS.divBorderFile).removeClass("file-active");
            $(this).addClass("file-active");
        });

        $(mapdata.SELECTORS.inforData).on('click', documentObject.SELECTORS.btnDownload, function () {
            let url = $(documentObject.SELECTORS.fileActive).attr('data-url');
            if (url != undefined) {
                let array = url.split('.');
                let typelink = array[array.length - 1].toLowerCase();
                if (typelink.includes('pdf')) {
                    var arrayurl = url.split('/');
                    documentObject.downloadFilePDF(url, arrayurl[arrayurl.length - 1]);
                } else {
                    window.open(url, '_blank');
                }
            }
        });

        $(mapdata.SELECTORS.inforData).on('click', documentObject.SELECTORS.btnDeleted, function () {
            let id = $(documentObject.SELECTORS.fileActive).attr('id');
            let url = $(documentObject.SELECTORS.fileActive).attr('data-url');
            if (id != undefined && url != undefined) {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:AreYouWantToDeleteThisFile"),
                    icon: "warning",
                    buttons: [
                        l("ManagementLayer:Close"),
                        l("ManagementLayer:Agree")
                    ],
                }).then((value) => {
                    if (value) {
                        let listid = []; listid.push(id);
                        var checkdata = documentObject.DeletedFileById(listid);
                        if (checkdata) {
                            modalCreateFolderLayer.DeleteFileDocument(url);
                        }
                        documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                        var index = modalCreateFolderLayer.GLOBAL.listObjectFile.findIndex(x => x.id == id);
                        modalCreateFolderLayer.GLOBAL.listObjectFile.splice(index, 1);
                        modalCreateFolderLayer.updateTable2();
                    }
                });
            }
        });

        $(mapdata.SELECTORS.inforData).on('click', documentObject.SELECTORS.btnView, function () {
            let url = $(documentObject.SELECTORS.fileActive).attr('data-url');
            if (url != undefined) {
                let array = url.split('.');
                let typelink = array[array.length - 1].toLowerCase();
                if (typelink.includes('doc') || typelink.includes('ppt') || typelink.includes('xls')) {
                    let url2 = encodeURIComponent(url);
                    window.open("https://view.officeapps.live.com/op/embed.aspx?src=" + url2 + "&embedded=true", '_blank');
                } else {
                    if (typelink.includes('pdf')) {
                        window.open(url, '_blank');
                    } else {
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:FileFormatCannotViewedPleaseDownload"),
                            icon: "warning",
                            button: l("ManagementLayer:Close"),
                        }).then((value) => {

                        });
                    }
                }
            }
        });
    },
    DeletedFileById: function (listid) {
        var check = false;
        $.ajax({
            type: "POST",
            dataType: 'json',
            processData: false,
            contentType: "application/json-patch+json",
            async: false,
            url: "/api/HTKT/DocumentObject/delete-file-list-id",
            data: JSON.stringify(listid),
            success: function (data) {
                if (data.code == "ok") {
                    check = true;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
        return check;
    },
    GetFileByObjectMainId: function (ObjectMainid, callback) {
        let id = ObjectMainid;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + "/api/HTKT/DocumentObject/get-file-by-objectId?Id=" + id,
            data: {
            },
            success: callback
        });
    },
    updateList: function (data) {
        //console.log(data)
        if (data.code === "ok") {
            documentObject.GLOBAL.lstOrder = [];
            documentObject.GLOBAL.lstDocument = data.result;
            $(documentObject.SELECTORS.liListFile).html('');
            $.each(documentObject.GLOBAL.lstDocument, function (i, obj) {
                let icon = documentObject.getTypeUrl(obj.urlDocument);
                let string = ``;
                string += `<div class="div-border-file" data-code-parent="${obj.nameFolder}" id="${obj.id}" data-url="${obj.urlDocument}" data-name="${obj.nameDocument}">
                                    <div class="div-file" style="text-align: left;">
                                        <i class="${icon}" aria-hidden="true"></i>
                                        <span style="font-weight: 900;">${obj.nameDocument}</span>
                                    </div>
                                </div>`;
                $('.file-upload-content[data-code="' + obj.nameFolder + '"]').children(documentObject.SELECTORS.liListFile).append(string);
                let check = documentObject.GLOBAL.lstOrder.find(x => x.NameFolder == obj.nameFolder);
                if (check == undefined || check == null) {
                    let object = {
                        NameFolder: obj.nameFolder,
                        ListOrder: []
                    };
                    object.ListOrder.push(Number(obj.orderDocument));
                    documentObject.GLOBAL.lstOrder.push(object);
                } else {
                    check.ListOrder.push(obj.orderDocument);
                }

                var objFile = {
                    id: obj.id,
                    name: obj.nameDocument,
                    order: obj.orderDocument,
                    nameProperties: obj.nameFolder,
                    url: obj.urlDocument,
                    idDirectoryProperties: obj.nameFolder
                };

                modalCreateFolderLayer.GLOBAL.listObjectFile.push(objFile);
            });
        }
    },
    showFileEdit: function (data) {
        if (data.code === "ok") {
            modalCreateFolderLayer.GLOBAL.listObjectFile = [];
            $.each(data.result, function (i, obj) {
                var item = {
                    id: obj.id,
                    name: obj.nameDocument,
                    idDirectoryProperties: obj.nameFolder,
                    url: obj.urlDocument,
                    order: obj.orderDocument
                };
                modalCreateFolderLayer.GLOBAL.listObjectFile.push(item);
                modalCreateFolderLayer.updateTable2();
            });
        }
    },
    downloadFilePDF: function (url, fileName) {
        $('.spinner').css('display', 'block');
        var req = new XMLHttpRequest();
        req.open("GET", url, true);
        req.responseType = "blob";
        req.onload = function () {
            //Convert the Byte Data to BLOB object.
            var blob = new Blob([req.response], { type: "application/octetstream" });

            //Check the Browser type and download the File.
            var isIE = false || !!document.documentMode;
            if (isIE) {
                window.navigator.msSaveBlob(blob, fileName);
            } else {
                var url = window.URL || window.webkitURL;
                link = url.createObjectURL(blob);
                var a = document.createElement("a");
                a.setAttribute("download", fileName);
                a.setAttribute("href", link);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            }
            $('.spinner').css('display', 'none');
        };
        req.send();
    },
    getTypeUrl: function (url) {
        let string = documentObject.GLOBAL.lstIcon.file;
        let array = url.split('.');
        let typelink = array[array.length - 1].toLowerCase();
        if (typelink.includes('doc')) {
            string = documentObject.GLOBAL.lstIcon.word;
        } else if (typelink.includes('ppt')) {
            string = documentObject.GLOBAL.lstIcon.ppt;
        }
        else if (typelink.includes('xls')) {
            string = documentObject.GLOBAL.lstIcon.excel;
        } else if (typelink.includes('pdf')) {
            string = documentObject.GLOBAL.lstIcon.pdf;
        }
        return string;
    },
    UpdateFileDocument: function (array) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: '/api/HTKT/DocumentObject/add-file-document',
            data: JSON.stringify(array),
            success: function (data) {
                if (data.code == "ok") {
                    documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                } else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:UpdateDocumentFailed"),
                        icon: "error",
                        button: l("ManagementLayer:Close"),
                    })
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    }
};