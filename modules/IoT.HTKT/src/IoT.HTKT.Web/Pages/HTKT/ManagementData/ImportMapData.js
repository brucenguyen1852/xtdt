﻿var l = abp.localization.getResource('HTKT');

var importmapdata = {
    GLOBAL: {
        dataGeojson: null,
        lstProperties: null,
        listMainObjetAndProperties: [],
        listMapLayerAndGeojson: {},
        listForMapProperties: [],
        countMapSend: {
            SendMap: 0,
            Reponse: 0,
            ReponseSucces: 0,
        },
        listErrorReponse: [],
        listCheckNewProperties: [],
        listPropertiesNew: [],
        lstSelected: [],
        fileArray: [],

        //Excel //
        listDataExcel: [],
        listTitleExcel: [],
        listMapLayerAndExcel: {},
        listForMapExcelProperties: [],
        dataExcelGeojson: null,
        listColumnRow: [],
        listErrorColumnRow: []
    },
    CONSTS: {
        NOTPROPERTIES: 'geojson,COLLECTION',
        URL_AJAXGETDEFAULTDATA: '/api/HTKT/PropertiesDirectory/get-directoryById',
        URL_SAVELISTOBJECT: '/api/HTKT/ManagementData/update-list-object',
        URL_URLSAVEPROPERTIESLAYER: '/api/HTKT/PropertiesDirectory/update-new-properties',
        LIMITSENDAJAX: 500,
    },
    SELECTORS: {
        btnConvertGeojson: ".btn-convert-geojson",
        geojsonWkt: ".geojson-wkt",
        fileGeojson: "#file-geojson",
        btn_close_content_geojson: ".close-import-geojson",
        content_geojson: ".model-geojson",
        content_file_geojson_upload: ".model-geojson  .file-upload-content",
        btn_delete_file_geojson: '.action-file-obj',
        error_content_fileGeojson_input: ".model-geojson .content-errors-file-geojson",
        modelPropertiesImport: ".model-properties-import",
        modalBodyFolderLayerImport: "#modalBodyFolderLayer-import",
        checkClone: ".isCheckCloneInfor",
        commonBackNext: ".common-back-next",
        munberObject: ".munberObject",
        munberObjectCurrent: ".munberObjectCurrent",
        //setting
        propertyStrokeText: "#strokeText-import",
        propertyStrokeWidth: "#stroke-width-import",
        propertyStrokeOpacity: "#rangeMax-stroke-import",
        propertyFillText: "#fillText-import",
        propertyFillOpacity: "#rangeMax-fill-import",
        divStrokeImportAll: "#strokeText-import, #div-stroke-import",
        divFillImportAll: "#fillText-import, #div-fill-import",
        divStrokeImport: "#div-stroke-import",
        divFillImport: "#div-fill-import",
        colorStrokeColorImport: "#strokeColor-import",
        colorFillColorImport: "#fillColor-import",
        thumbMaxStrokeImport: "#thumbMax-stroke-import",
        maxStrokeSImport: "#max-stroke-import",
        lineStrokeImport: "#line-stroke-import",
        thumbMaxFillImport: "#thumbMax-fill-import",
        maxFillSImport: "#max-fill-import",
        lineFillImport: "#line-fill-import",
        hasError: ".form-group.has-error",

        btnSaveImport: ".btn-save-import",
        //properties import
        //munberObjectCurrent: ".munberObjectCurrent",
        backObject: ".back-object",
        nextObject: ".next-object",
        //modalLayer: ".detail-property-new .content-map-properties .modal-layer",
        modalLayer: ".detail-property-new #modalBodyFolderLayer-import .modal-layer",
        modelGeojson: ".model-geojson",
        btncancelImport: ".model-properties-import .btn-cancel",
        liIndex: ".li-index",
        loading: "#loading-map",

        //map geojson properties
        modelMapProperties: ".model-map-properties-geojson",
        contentMapProperties: ".model-map-properties-geojson .content-map-properties",
        btncancelMapProperties: ".model-map-properties-geojson .btn-cancel",
        btnSaveNewGeojson: ".model-map-properties-geojson .btn-save-import-new-geojson",
        fromMapLayerGeojson: ".model-map-properties-geojson .map-layer-geojson",
        //new geojson properties
        modelVewProperties: ".model-new-properties-geojson",
        contentNewProperties: ".model-new-properties-geojson .content-new-properties",
        btnSaveNewProperties: ".model-new-properties-geojson .btn-save-new-properties",
        inputNewProperties: ".model-new-properties-geojson .content-new-properties input",
        btncancelNewProperties: ".model-new-properties-geojson .btn-cancel",

        //import excel
        model_excel: ".model-import-properties-execl",
        input_file_excel: ".model-import-properties-execl #imput-file-excel",
        error_content_execl_input: ".model-import-properties-execl .content-errors-file-execl",
        content_file_excel_upload: ".model-import-properties-execl .file-excel-content",
        btn_cancel_model_excel: ".model-import-properties-execl .btn-cancel",
        btn_next_excel: ".model-import-properties-execl .btn-next-excel",
        btn_clear_action_file_excel: ".action-file-obj",
        content_file_excel_upload: ".model-import-properties-execl .file-upload-content",

        //import excel
        //uploadFileExcel: ".upload-File-Excel",
        //inputExcel: "#file-excel",
        //modelMapPropertiesExcel: ".model-map-properties-excel",
        //btnSaveNewExcel: ".model-map-properties-excel .btn-save-import-new-excel",
        //contentExcelProperties: ".model-map-properties-excel .content-map-properties",
        //fromMapLayerExcel: ".model-map-properties-excel .map-layer-excel",
        //btncancelNewPropertiesExcel: ".model-map-properties-excel .btn-cancel",

    },
    init: function () {
        importmapdata.setEvent();
    },
    setEvent: function () {
        $(importmapdata.SELECTORS.geojsonWkt).on("change", function () {
            let values = $(importmapdata.SELECTORS.geojsonWkt).val().trim();
            if (typeof values !== "undefined" && values !== "") {
                $(importmapdata.SELECTORS.fileGeojson).val("");
            }

            if (values != "" || $(importmapdata.SELECTORS.fileGeojson).val().trim() != "" || importmapdata.GLOBAL.fileArray.length > 0) {
                $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', false);

                // Xóa file
                $(importmapdata.SELECTORS.content_file_geojson_upload).html('');
                $(importmapdata.SELECTORS.fileGeojson).val(null).trigger("change");
                $(importmapdata.SELECTORS.content_file_geojson_upload).addClass('hidden');

            }
            else {
                $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);
            }

        });
        $(importmapdata.SELECTORS.fileGeojson).on("change", function () {
            $(importmapdata.SELECTORS.error_content_fileGeojson_input).addClass('hidden');
            //$(importmapdata.SELECTORS.content_file_geojson_upload).addClass('hidden');
            if ($(this)[0].files.length > 0) {
                importmapdata.GLOBAL.fileArray = $(this)[0].files;
            }
            var file = importmapdata.GLOBAL.fileArray[0];

            var htmlError = `<div class="text-danger" style="font-style:italic;">${l("ManagementLayer:FileNotCorrrectFormat")}</div>`; // hiển thị thông báo file không đúng định dạng

            if (file != undefined) {
                var exp = file.name.split('.')[1];
                if (exp != "json" && exp != "geojson") {
                    $(importmapdata.SELECTORS.error_content_fileGeojson_input).html(htmlError);
                    $(importmapdata.SELECTORS.error_content_fileGeojson_input).removeClass('hidden');

                    importmapdata.GLOBAL.fileArray = [];
                }
                else {
                    if (file.size > 52428800) {
                        const dt = new DataTransfer();
                        $(this)[0].files = dt.files;
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:FileSizeCannotLarger50mb"),
                            icon: "warning",
                            button: l("ManagementLayer:Close"),
                        });

                        $(importmapdata.SELECTORS.error_content_fileGeojson_input).html(htmlError);
                        $(importmapdata.SELECTORS.error_content_fileGeojson_input).removeClass('hidden');

                        importmapdata.GLOBAL.fileArray = [];

                        return;
                    } else {
                        let values = $(importmapdata.SELECTORS.fileGeojson).val();
                        if (typeof values !== "undefined" && values !== "") {
                            $(importmapdata.SELECTORS.geojsonWkt).val("");
                        }

                        // hiển thị tên file
                        var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${file.name}" data-toggle="tooltip" data-placement="bottom">${file.name}</div>
                            <div class="action-file-obj" title="Xóa" data-toggle="tooltip" data-placement="bottom"><img src="/images/close_cricle.svg" /></div>`; // hiển thị tên file đã được chọn
                        $(importmapdata.SELECTORS.content_file_geojson_upload).html(html);
                        $(importmapdata.SELECTORS.content_file_geojson_upload).removeClass('hidden');

                        importmapdata.clearLabelError();
                    }

                    if ($(importmapdata.SELECTORS.geojsonWkt).val().trim() != "" || $(importmapdata.SELECTORS.fileGeojson).val().trim() != "" || importmapdata.GLOBAL.fileArray.length > 0) {
                        $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', false);
                    }
                    else {
                        $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);
                    }
                }
            }
        });
        $(importmapdata.SELECTORS.content_file_geojson_upload).on('click', importmapdata.SELECTORS.btn_delete_file_geojson, function () {
            $(importmapdata.SELECTORS.content_file_geojson_upload).html('');
            importmapdata.GLOBAL.fileArray = [];
            $(importmapdata.SELECTORS.fileGeojson).val(null).trigger("change");
            $(importmapdata.SELECTORS.content_file_geojson_upload).addClass('hidden');
            $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);
        });

        $(importmapdata.SELECTORS.btnConvertGeojson).on("click", function () {
            var data = $(importmapdata.SELECTORS.geojsonWkt).val();
            var file = $(importmapdata.SELECTORS.fileGeojson).val();
            if ((typeof data !== undefined && data.trim() !== "") || (typeof file !== undefined && file.trim() !== "") || importmapdata.GLOBAL.fileArray.length > 0) {
                importmapdata.importGeojsonData();
                $(importmapdata.SELECTORS.modelPropertiesImport + " " + Info_Properties_Main_Object.SELECTORS.checkbox_config).prop('checked', true);
                Config_Main_Object.showDefaultProperties(importmapdata.SELECTORS.modelPropertiesImport); // hiển thị cấu hình mặc định
            } else {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:PleaseEnterData"),
                    icon: "error",
                    buttons: l("ManagementLayer:Close"),
                });
            }
        });
        $(importmapdata.SELECTORS.checkClone).on("click", function () {
            if ($(importmapdata.SELECTORS.checkClone).prop('checked')) {
                $(importmapdata.SELECTORS.commonBackNext).attr("disabled", true);
            }
            else {
                $(importmapdata.SELECTORS.commonBackNext).removeAttr("disabled");
            }
        });

        $(importmapdata.SELECTORS.btnSaveImport).on("click", function () {
            if ($(importmapdata.SELECTORS.checkClone).prop('checked')) {
                if (Info_Properties_Main_Object.checkFormInput(importmapdata.SELECTORS.modalBodyFolderLayerImport) && mapdata.GLOBAL.idDirectory.trim() != "") {
                    importmapdata.setClonePropertiesAndSetting();
                    let count = Math.ceil(importmapdata.GLOBAL.listMainObjetAndProperties.length / importmapdata.CONSTS.LIMITSENDAJAX);
                    var listobject = [];
                    for (var i = 0; i < count; i++) {
                        listobject = importmapdata.getListObjectPaging(importmapdata.GLOBAL.listMainObjetAndProperties, (i + 1), importmapdata.CONSTS.LIMITSENDAJAX);
                        importmapdata.saveListObject(listobject);
                    }
                }
            } else {
                if (Info_Properties_Main_Object.checkFormInput(importmapdata.SELECTORS.modalBodyFolderLayerImport) && mapdata.GLOBAL.idDirectory.trim() != "") {
                    importmapdata.savePropertiesOnList();
                    if (importmapdata.GLOBAL.listMainObjetAndProperties.length === importmapdata.GLOBAL.dataGeojson.length) {
                        //importmapdata.saveListObject(importmapdata.GLOBAL.listMainObjetAndProperties);
                        let count = Math.ceil(importmapdata.GLOBAL.listMainObjetAndProperties.length / importmapdata.CONSTS.LIMITSENDAJAX);
                        var listobject = [];
                        for (var i = 0; i < count; i++) {
                            listobject = importmapdata.getListObjectPaging(importmapdata.GLOBAL.listMainObjetAndProperties, (i + 1), importmapdata.CONSTS.LIMITSENDAJAX);
                            importmapdata.saveListObject(listobject);
                        }
                    }
                    else {
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: `Không được để trống thông tin của ${importmapdata.GLOBAL.dataGeojson.length - importmapdata.GLOBAL.listMainObjetAndProperties.length} đối tượng còn lại`,
                            icon: "error",
                            buttons: l("ManagementLayer:Close"),
                        });
                    }
                }
            }
            importmapdata.fitBoundGeometry(importmapdata.GLOBAL.listMainObjetAndProperties);
            menuLeft.resizeMenuLeft();
            $(importmapdata.SELECTORS.checkClone).prop('checked', false);
        });
        $(importmapdata.SELECTORS.backObject).on("click", function () {
            if (importmapdata.checkformmodal()) {
                let count = $(importmapdata.SELECTORS.munberObjectCurrent).text();
                importmapdata.savePropertiesOnList();
                importmapdata.setCountNextBack(true);
                importmapdata.setMenuNextBack();
                importmapdata.showDataProperties((Number(count) - 1));
            }
        });
        $(importmapdata.SELECTORS.nextObject).on("click", function () {
            if (importmapdata.checkformmodal()) {
                let count = $(importmapdata.SELECTORS.munberObjectCurrent).text();
                importmapdata.savePropertiesOnList();
                importmapdata.setCountNextBack(false);
                importmapdata.setMenuNextBack();
                importmapdata.showDataProperties((Number(count) + 1));
            }
        });

        // thoat input nhap chuoi wkt/geojson
        $(importmapdata.SELECTORS.btncancelImport).on("click", function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("AreYouExitAction"),
                icon: "warning",
                buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                dangerMode: false,
            }).then((val) => {
                if (val) {
                    importmapdata.showOrHideModelImport(false);
                    mapdata.setOrRestBtnInsert(false);
                    mapdata.setOrRestBtnEidt(false);
                    mapdata.clearAllDraw();
                    importmapdata.GLOBAL.listMainObjetAndProperties = [];
                    $(importmapdata.SELECTORS.munberObjectCurrent).text(1);
                    $(importmapdata.SELECTORS.backObject).attr("disabled", true);
                    $(importmapdata.SELECTORS.checkClone).prop('checked', false);
                    mapdata.GLOBAL.selectbtn = "insert";
                }
            });
        });
        $(importmapdata.SELECTORS.modelPropertiesImport).on("keyup", ".modal-layer", function () {
            $(this).parents(".form-group-modal").find(".label-error").remove();
            $(this).parents(".form-group-modal").removeClass("has-error");
        });

        //New map properties
        $(importmapdata.SELECTORS.btncancelMapProperties).on("click", function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("AreYouExitAction"),
                icon: "warning",
                buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                dangerMode: false,
            }).then((val) => {
                if (val) {
                    importmapdata.showOrHideModelImport(false);
                    mapdata.setOrRestBtnInsert(false);
                    mapdata.setOrRestBtnEidt(false);
                    mapdata.clearAllDraw();
                    importmapdata.showOrHideMapProperties(false);
                    mapdata.GLOBAL.selectbtn = "insert";
                }
            });
        });
        $(importmapdata.SELECTORS.btnSaveNewGeojson).on("click", function () {
            importmapdata.GLOBAL.lstSelected = [];
            let status = false;
            let statusRequeset = true;
            mapdata.clearLabelError();
            $(importmapdata.SELECTORS.fromMapLayerGeojson + " .modal-layer").each(function () {
                let id = $(this).data("id").toLowerCase();
                let selected = $(this).data("required");
                let idmap = $(this).val();
                var inputName = $(this).attr('data-name');
                if (selected && (idmap == "" || idmap == null)) {
                    if (statusRequeset) {
                        statusRequeset = false;
                    }
                    mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                    //$(this).append("<label class='label-error' style='color:red'>" + jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName) + "</label>");
                }
                importmapdata.GLOBAL.listMapLayerAndGeojson[id] = idmap;

                ////
                let object = importmapdata.GLOBAL.lstSelected.find(x => x == idmap);
                if (object !== null && object !== undefined && typeof (object) !== "undefined" && object !== "") {
                    status = true;
                } else if (idmap !== "") {
                    importmapdata.GLOBAL.lstSelected.push(idmap);
                }
            });
            if (statusRequeset) {
                if (status) {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:MultiFeidDuplicateDataType"),
                        icon: "warning",
                        buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                        dangerMode: false,
                    }).then((val) => {
                        if (val) {
                            importmapdata.setupDataMapLayerGeojson();
                            //console.log(importmapdata.GLOBAL.listMapLayerAndGeojson);
                            importmapdata.fitBoundGeometry(importmapdata.GLOBAL.dataGeojson);
                        }
                    });
                } else {
                    importmapdata.setupDataMapLayerGeojson();
                    //console.log(importmapdata.GLOBAL.listMapLayerAndGeojson);
                    importmapdata.fitBoundGeometry(importmapdata.GLOBAL.dataGeojson);
                }
                menuLeft.resizeMenuLeft();
            }
        });
        $(importmapdata.SELECTORS.btnSaveNewProperties).on("click", function () {
            if ($(importmapdata.SELECTORS.inputNewProperties + ":checked").length > 0) {
                importmapdata.createLayerPropertiesAuto();
                importmapdata.setupDataNewProperties();
                importmapdata.fitBoundGeometry(importmapdata.GLOBAL.dataGeojson);
                menuLeft.resizeMenuLeft();
            }
            //$(importmapdata.SELECTORS.fromMapLayerGeojson).each(function () {
            //    let id = $(this).data("id").toLowerCase();
            //    let idmap = $(this).find("select").val();
            //    importmapdata.GLOBAL.listMapLayerAndGeojson[id] = idmap;

            //})
            //importmapdata.setupDataMapLayerGeojson();
            //console.log(importmapdata.GLOBAL.listMapLayerAndGeojson);
        });
        $(importmapdata.SELECTORS.btncancelNewProperties).on("click", function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("AreYouExitAction"),
                icon: "warning",
                buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                dangerMode: false,
            }).then((val) => {
                if (val) {
                    importmapdata.showOrHideModelImport(false);
                    mapdata.setOrRestBtnInsert(false);
                    mapdata.setOrRestBtnEidt(false);
                    mapdata.clearAllDraw();
                    importmapdata.showOrHideNewProperties(false);
                    mapdata.GLOBAL.selectbtn = "insert";
                }
            });
        });

        // thoat import geojson
        $(importmapdata.SELECTORS.btn_close_content_geojson).on('click', function () {
            // cancel trạng thái thêm mới
            //mapdata.setOrRestBtnInsert(false);
            if (importmapdata.CheckUploadGeojsonInputFormBeforeExitsAction()) {
                mapdata.clearAllDraw();
                modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);

                $(importmapdata.SELECTORS.fileGeojson).val("");
                $(importmapdata.SELECTORS.geojsonWkt).val("");
                importmapdata.GLOBAL.fileArray = [];
                importmapdata.CloseContentImportGeoJson();
                mapdata.ResetInsertMainOjbect();
                mapdata.clearLabelError();
            }
            else {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("AreYouExitAction"),
                    icon: "warning",
                    buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                    dangerMode: false,
                }).then((val) => {
                    if (val) {
                        mapdata.clearAllDraw();
                        modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                        $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);

                        $(importmapdata.SELECTORS.fileGeojson).val("");
                        $(importmapdata.SELECTORS.geojsonWkt).val("");
                        importmapdata.GLOBAL.fileArray = [];
                        importmapdata.CloseContentImportGeoJson();
                        mapdata.ResetInsertMainOjbect();
                        mapdata.clearLabelError();
                    }
                });
            }
            
        });

        $(importmapdata.SELECTORS.modelGeojson).on('keyup', delay(function () {
            mapdata.clearLabelError();
            let values = $(importmapdata.SELECTORS.geojsonWkt).val().trim();
            if (typeof values !== "undefined" && values !== "") {
                $(importmapdata.SELECTORS.fileGeojson).val("");
            }

            if (values != "" || $(importmapdata.SELECTORS.fileGeojson).val().trim() != "" || importmapdata.GLOBAL.fileArray.length > 0) {
                $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', false);
            }
            else {
                $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);
            }
        }, 300));

        //----------execl----------
        $(importmapdata.SELECTORS.input_file_excel).on('change', function () {
            $(importmapdata.SELECTORS.error_content_execl_input).addClass('hidden');
            $(importmapdata.SELECTORS.content_file_excel_upload).addClass('hidden');
            var file = $(this)[0].files[0];
            var htmlError = `<div class="text-danger" style="font-style:italic;">${l("ManagementLayer:FileNotCorrrectFormat")}</div>`; // hiển thị thông báo file không đúng định dạng
            if (file != undefined) {
                var exp = file.name.split('.');
                if (exp[exp.length - 1] != "xlsx") {
                    $(importmapdata.SELECTORS.error_content_execl_input).html(htmlError);
                    $(importmapdata.SELECTORS.error_content_execl_input).removeClass('hidden');
                    $(importmapdata.SELECTORS.btn_next_excel).prop('disabled', true);
                    importmapdata.GLOBAL.listTitleExcel = [];
                    importmapdata.GLOBAL.listDataExcel = []; 
                }
                else {
                    if (file.size > 52428800) {
                        const dt = new DataTransfer();
                        $(this)[0].files = dt.files;
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:FileSizeCannotLarger50mb"),
                            icon: "warning",
                            button: l("ManagementLayer:Close"),
                        });

                        $(importmapdata.SELECTORS.error_content_execl_input).html(htmlError);
                        $(importmapdata.SELECTORS.error_content_execl_input).removeClass('hidden');

                        return;
                    } else {
                        // hiển thị tên file
                        var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${file.name}" data-toggle="tooltip" data-placement="bottom">${file.name}</div>
                            <div class="action-file-obj" title="Xóa" data-toggle="tooltip" data-placement="bottom"><img src="/images/close_cricle.svg" /></div>`; // hiển thị tên file đã được chọn
                        $(importmapdata.SELECTORS.content_file_excel_upload).html(html);
                        $(importmapdata.SELECTORS.content_file_excel_upload).removeClass('hidden');


                        if (this.files.length > 0) {
                            importmapdata.GLOBAL.listTitleExcel = [];
                            importmapdata.GLOBAL.listDataExcel = [];
                            var file = this.files[0];
                            var ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
                            try {
                                if (ext === "xlsx" || ext === "xls") {
                                    var reader = new FileReader();
                                    if (reader.readAsBinaryString) {
                                        reader.onload = function (e) {
                                            //GetTableFromExcel(e.target.result);
                                            abp.ui.setBusy(importmapdata.SELECTORS.model_excel);
                                            let data = e.target.result;
                                            importmapdata.getDataExcel(data);
                                            //var workbook = XLSX.read(data, {
                                            //    type: 'binary'
                                            //});

                                            ////get the name of First Sheet.
                                            //var Sheet = workbook.SheetNames[0];

                                            ////Read all rows from First Sheet into an JSON array.
                                            //var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[Sheet]);
                                            //excelRows = excelRows.filter(x => x.COLLECTION !== "" && x.COLLECTION !== undefined && x.COLLECTION !== null);
                                            //if (excelRows.length > 0) {
                                            //    var cell = XLSX.utils.cell_set_number_format(workbook.Sheets[Sheet]);
                                            //    var arrayColumn = _buildColumnsArray(cell['!ref']);
                                            //    getTitleColumn(cell, arrayColumn);
                                            //    //importmapdata.GLOBAL.listTitleExcel = Object.keys(excelRows[0]);
                                            //    importmapdata.GLOBAL.listDataExcel = excelRows;
                                            //    let status = importmapdata.convertExceltoGeojson(importmapdata.GLOBAL.listDataExcel);
                                            //    if (status) {
                                            //        importmapdata.showOrHideModelExcel(true);
                                            //        importmapdata.createrFormExcelProperties();
                                            //    } else {
                                            //        importmapdata.GLOBAL.listTitleExcel = [];
                                            //        importmapdata.GLOBAL.listDataExcel = [];
                                            //    }
                                            //}
                                            //console.log(excelRows);
                                        };
                                        reader.readAsBinaryString(file);
                                    } else {
                                        //For IE Browser.
                                        reader.onload = function (e) {
                                            abp.ui.setBusy(importmapdata.SELECTORS.model_excel);
                                            var data = "";
                                            var bytes = new Uint8Array(e.target.result);
                                            for (var i = 0; i < bytes.byteLength; i++) {
                                                data += String.fromCharCode(bytes[i]);
                                            }
                                            importmapdata.getDataExcel(data);

                                            ////GetTableFromExcel(data);
                                            //var workbook = XLSX.read(data, {
                                            //    type: 'binary'
                                            //});

                                            ////get the name of First Sheet.
                                            //var Sheet = workbook.SheetNames[0];

                                            ////Read all rows from First Sheet into an JSON array.
                                            //var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[Sheet]);
                                            //excelRows = excelRows.filter(x => x.COLLECTION !== "" && x.COLLECTION !== undefined && x.COLLECTION !== null);
                                            //if (excelRows.length > 0) {
                                            //    var cell = XLSX.utils.cell_set_number_format(workbook.Sheets[Sheet]);
                                            //    var arrayColumn = _buildColumnsArray(cell['!ref']);
                                            //    getTitleColumn(cell, arrayColumn);
                                            //    //importmapdata.GLOBAL.listTitleExcel = Object.keys(excelRows[0]);
                                            //    importmapdata.GLOBAL.listDataExcel = excelRows;
                                            //    let status = importmapdata.convertExceltoGeojson(importmapdata.GLOBAL.listDataExcel);
                                            //    if (status) {
                                            //        importmapdata.showOrHideModelExcel(true);
                                            //        importmapdata.createrFormExcelProperties();
                                            //    } else {
                                            //        importmapdata.GLOBAL.listTitleExcel = [];
                                            //        importmapdata.GLOBAL.listDataExcel = [];
                                            //    }
                                            //}
                                            //console.log(excelRows);
                                        };
                                        reader.readAsArrayBuffer(file);
                                    }

                                }
                                else {
                                    swal({
                                        title: "Thông báo",
                                        text: `File không đúng định dạng`,
                                        icon: "warning",
                                        button: "Đóng"
                                    });

                                    $(this).val(null);
                                }
                            } catch (e) {
                                swal({
                                    title: "Thông báo",
                                    text: `Lỗi! Không thể import file excel`,
                                    icon: "warning",
                                    button: "Đóng"
                                });

                                $(this).val(null);
                            }
                        }
                    }

                }
            }
        });

        // đóng content import excel
        $(importmapdata.SELECTORS.btn_cancel_model_excel).on('click', function () {
            
            let check = $(importmapdata.SELECTORS.btn_next_excel).attr("disabled");
            if (typeof check === "undefined") {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("AreYouExitAction"),
                    icon: "warning",
                    buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                    dangerMode: false,
                }).then((val) => {
                    if (val) {
                        importmapdata.CloseContentInputGeoJson();
                        mapdata.ResetInsertMainOjbect();
                        importmapdata.clearExcelData();
                        mapdata.GLOBAL.selectbtn = "insert";
                    }
                });
            } else {
                importmapdata.CloseContentInputGeoJson();
                mapdata.ResetInsertMainOjbect();
                importmapdata.clearExcelData();
                mapdata.GLOBAL.selectbtn = "insert";
            }


        })
        $(importmapdata.SELECTORS.btn_next_excel).on('click', function () {
            if ($(importmapdata.SELECTORS.input_file_excel)[0].files !== null && importmapdata.GLOBAL.listTitleExcel.length > 0) {
                importmapdata.GLOBAL.dataGeojson = null;
                importmapdata.showOrHideMapProperties(true);
                importmapdata.createrFormExcelProperties();
            }
        });
        $(importmapdata.SELECTORS.content_file_excel_upload).on('click', importmapdata.SELECTORS.btn_clear_action_file_excel, function () {
            $(importmapdata.SELECTORS.content_file_excel_upload).html('');
            importmapdata.GLOBAL.fileArray = [];
            $(importmapdata.SELECTORS.input_file_excel).val(null).trigger("change");
            $(importmapdata.SELECTORS.content_file_excel_upload).addClass('hidden');
            $(importmapdata.SELECTORS.btn_next_excel).prop('disabled', true);
        });
    },
    checkJson: function (data) {
        var check;
        var re = null;
        try {
            var feature = JSON.parse(data);
            check = true;
        } catch (e) {
            check = false;
        }

        if (check) {
            map.data.remove(re);
        }
        return check;
    },
    drawWKTGeojson: function (data) {
        if (typeof data !== "undefined") {
            let typeDraw = data.type.toLowerCase();
            let coordinates = data.coordinates;
            let iLatLng = []
            switch (typeDraw) {
                case "point":
                    mapdata.setMarkerDraw(coordinates[1], coordinates[0]);
                    break;
                case "linestring":
                    iLatLng = [];
                    $.each(coordinates, function (i, obj) {
                        let latLng = { lat: obj[1], lng: obj[0] };
                        mapdata.createMarkerDrawLoDat(obj[1], obj[0], 1)
                        iLatLng.push(latLng);
                    });
                    mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, true);
                    mapdata.setDragPoligonMarker(mapdata.GLOBAL.listDistance.listMarkerDistance);
                    break;
                case "polygon":
                    iLatLng = [];
                    let TLatLng = [];
                    for (var i = 0; i < coordinates.length; i++) {
                        iLatLng = [];
                        for (var j = 0; j < coordinates[i].length; j++) {
                            let latLng = { lat: coordinates[i][j][1], lng: coordinates[i][j][0] };
                            if (j < coordinates[i].length - 1) {
                                mapdata.createMarkerDrawLoDat(coordinates[i][j][1], coordinates[i][j][0], 2)
                            }
                            iLatLng.push(latLng);
                        }
                        TLatLng.push(iLatLng);
                    }
                    mapdata.createPolygonArea(TLatLng);
                    mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                    break;
                default: break;
            }
        }
    },
    importGeojsonData: function () {
        var data = $(importmapdata.SELECTORS.geojsonWkt).val();
        var file = $(importmapdata.SELECTORS.fileGeojson).val();
        var geojson;
        if ((typeof data !== undefined && data.trim() !== "")) {
            if (!importmapdata.checkJson(data)) {
                importmapdata.convertWKTtoGeojson(data);
            } else {
                importmapdata.convertDataGeojson(JSON.parse(data));
                if (importmapdata.GLOBAL.dataGeojson !== null) {
                    let check = importmapdata.ListCheckGeojsonPattern(importmapdata.GLOBAL.dataGeojson);
                    if (check) {
                        importmapdata.checkFormatGeoJson();
                    } else
                        importmapdata.checkMenuGeojson();
                }
            }
        } else {
            var files = importmapdata.GLOBAL.fileArray[0];        // $(importmapdata.SELECTORS.fileGeojson)[0].files[0];
            var fileReader = new FileReader();
            fileReader.onload = function (progressEvent) {
                importmapdata.convertFileToGeojson(fileReader.result);
                //importmapdata.checkMenuGeojson();
            };
            fileReader.readAsText(files, "UTF-8");
        }
    },
    convertFileToGeojson: function (data) {
        if (importmapdata.checkJson(data)) {
            importmapdata.convertDataGeojson(JSON.parse(data));
            if (importmapdata.GLOBAL.dataGeojson !== null) {
                let check = importmapdata.ListCheckGeojsonPattern(importmapdata.GLOBAL.dataGeojson);
                if (check) {
                    importmapdata.checkFormatGeoJson();
                } else
                    importmapdata.checkMenuGeojson();
            }
        } else {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:FileDataNotInCorrectFormat"),
                icon: "error",
                buttons: l("ManagementLayer:Close"),
            });
        }
    },
    convertDataGeojson: function (data) {
        if (data !== null && typeof data !== "undefined") {
            if (data.features !== null && typeof data.features !== "undefined" && data.features.length > 0) {
                importmapdata.GLOBAL.dataGeojson = data.features.filter(x => x.geometry !== null);
            } else if (data.length > 0 && data[0].geometry !== null && typeof data[0].geometry !== "undefined") {
                importmapdata.GLOBAL.dataGeojson = data;
            } else if (data.geometry !== null && typeof data.geometry !== "undefined") {
                let listobj = [];
                listobj.push(data);
                importmapdata.GLOBAL.dataGeojson = listobj;
            } else {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:DataNotInCorrectFormat"),
                    icon: "error",
                    buttons: l("ManagementLayer:Close"),
                });
            }
        }
    },
    convertWKTtoGeojson: function (values) {
        importmapdata.checkFormatGeoJson();
        let listobj = [];
        let data = values.split("\n");
        try {
            $.each(data, function () {
                let geojson = Terraformer.WKT.parse(this);
                let feature = {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": geojson.type,
                        "coordinates": geojson.coordinates
                    }
                }
                listobj.push(feature);
                //console.log(geojson);
            });
            importmapdata.GLOBAL.dataGeojson = listobj;
        }
        catch (err) {
            //swal({
            //    title: l("ManagementLayer:Notification"),
            //    text: l("ManagementLayer:WKTDataIsNotValid"),
            //    icon: "error",
            //    buttons: l("ManagementLayer:Close"),
            //});
        }
    },
    checkFormatGeoJson: function () {
        mapdata.clearLabelError();
        $(importmapdata.SELECTORS.error_content_fileGeojson_input).addClass('hidden')

        var textarea = $(importmapdata.SELECTORS.geojsonWkt).val();
        var file = $(importmapdata.SELECTORS.fileGeojson)[0].files[0];

        if (textarea != "") {
            insertError($(importmapdata.SELECTORS.geojsonWkt), "other");
            mapdata.showLabelError(importmapdata.SELECTORS.geojsonWkt, l("ManagementLayer:DataNotInCorrectFormat"));
        }

        if (file != undefined) {
            var htmlError = `<div class="text-danger" style="font-style:italic;">${l("ManagementLayer:DataNotInCorrectFormat")}`; // hiển thị thông báo file không đúng định dạng
            $(importmapdata.SELECTORS.error_content_fileGeojson_input).html(htmlError);
            $(importmapdata.SELECTORS.error_content_fileGeojson_input).removeClass('hidden');
        }
    },
    showOrHideModelImport: function (check) {
        if (check) {
            importmapdata.getPropertiesDirectory();
            $(importmapdata.SELECTORS.liIndex).trigger("click");
            if (importmapdata.GLOBAL.dataGeojson !== null && importmapdata.GLOBAL.dataGeojson.length > 1) {
                $(importmapdata.SELECTORS.munberObjectCurrent).text("1");
                importmapdata.setMenuNextBack();
                //swal({
                //    title: l("ManagementLayer:Notification"),
                //    text: l("ManagementLayer:CopyFirstInfoForAllObject{0}", importmapdata.GLOBAL.dataGeojson.length),
                //    icon: "warning",
                //    buttons: [
                //        l("ManagementLayer:Close"),
                //        l("ManagementLayer:Agree")
                //    ],
                //}).then((value) => {
                //    if (value) {
                //        $(importmapdata.SELECTORS.checkClone).trigger("click");
                //    } else {
                //        importmapdata.setMenuNextBack();
                //    }
                //});
                $(importmapdata.SELECTORS.munberObject).text(importmapdata.GLOBAL.dataGeojson.length);
            } else {
                $(importmapdata.SELECTORS.backObject).attr("disabled", true);
                $(importmapdata.SELECTORS.nextObject).attr("disabled", true);
                $(importmapdata.SELECTORS.munberObject).text(importmapdata.GLOBAL.dataGeojson.length);
            }
            /* $(importmapdata.SELECTORS.modelPropertiesImport).modal("show");*/
            $(importmapdata.SELECTORS.modelPropertiesImport).addClass('active');
            $(`${importmapdata.SELECTORS.modelPropertiesImport} .title-li-tab .li-tab`).first().trigger('click');

            //importmapdata.CloseContentImportGeoJson(); // đóng content import geojson
        }
        else {
            /*$(importmapdata.SELECTORS.modelPropertiesImport).modal("hide");*/
            $(importmapdata.SELECTORS.modelPropertiesImport).removeClass('active');
        }
    },
    //set properties and setting a obj
    setClonePropertiesAndSetting: function () {
        importmapdata.GLOBAL.listMainObjetAndProperties = [];
        var object = {};
        object.id = mapdata.uuidv4;
        object.idDirectory = mapdata.GLOBAL.idDirectory;
        object.nameObject = "Object Demo";
        object.listProperties = [];
        object.geometry = {};
        object.object3D = null;
        object.isCheckImplementProperties = false;
        object.tags = null;
        $.each(importmapdata.GLOBAL.lstProperties, function (i, obj) {
            var objectparam = {
                nameProperties: obj.NameProperties,
                codeProperties: obj.CodeProperties,
                typeProperties: obj.TypeProperties,
                isShow: obj.IsShow,
                isView: obj.IsView,
                defalutValue: $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + obj.CodeProperties).val(),
                orderProperties: obj.OrderProperties,
                typeSystem: obj.TypeSystem
            };
            object.listProperties.push(objectparam);
            if (obj.CodeProperties.toLowerCase() === "name") {
                object.nameObject = $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + obj.CodeProperties).val();
            }
            if (obj.TypeProperties == "link") {
                objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectLinkCamera);
            }
            if (obj.TypeProperties == "image") {
                objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectImage);
            }
            if (obj.TypeProperties == "list") {
                let array = null;
                objectparam.defalutValue = $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + obj.CodeProperties).val();
            }

            if (obj.TypeProperties == "checkbox") {
                let arraychecked = $(importmapdata.SELECTORS.modelPropertiesImport + " " + ".text-modal-" + obj.CodeProperties + ":checked").map(function () {
                    return this.value;
                }).get();

                objectparam.defalutValue = arraychecked.toString();
            }
            if (obj.TypeProperties == "radiobutton") {
                let stringchecked = $(importmapdata.SELECTORS.modelPropertiesImport + " " + ".text-modal-" + obj.CodeProperties + ":checked").val();
                objectparam.defalutValue = stringchecked;
            }

            if (obj.TypeProperties == "bool") {
                //obj.DefalutValue = $("#text-modal-" + obj.CodeProperties).prop("checked");
                objectparam.defalutValue = $("#text-modal-" + obj.CodeProperties + importmapdata.SELECTORS.modelPropertiesImport.replace(".", "").replace("#", "")).prop("checked").toString();
            }
        });

        object.properties = Config_Main_Object.getValueProperties(importmapdata.GLOBAL.modelPropertiesImport);

        $.each(importmapdata.GLOBAL.dataGeojson, function (i, obj) {
            let objectTemp = JSON.parse(JSON.stringify(object));
            let properties = JSON.parse(JSON.stringify(obj.properties));
            objectTemp.geometry = obj.geometry;
            objectTemp.tags = properties;
            importmapdata.GLOBAL.listMainObjetAndProperties.push(objectTemp);
        });
    },
    /*--properties and setting list obj--*/
    savePropertiesOnList: function () {
        let count = $(importmapdata.SELECTORS.munberObjectCurrent).text();
        //lưu data current
        let obj = importmapdata.GLOBAL.listMainObjetAndProperties.find(x => x.id === count);
        let dataCurrent;
        let index = Number(count) - 1;
        if (typeof obj !== "undefined" && obj !== null) {
            dataCurrent = importmapdata.setPropertiesAndSettingListObject(importmapdata.GLOBAL.dataGeojson[index].geometry, count);
            obj = dataCurrent;
        } else {
            dataCurrent = importmapdata.setPropertiesAndSettingListObject(importmapdata.GLOBAL.dataGeojson[index].geometry, count);
            dataCurrent.tags = importmapdata.GLOBAL.dataGeojson[index].properties;
            importmapdata.GLOBAL.listMainObjetAndProperties.push(dataCurrent);
        }
    },
    setPropertiesAndSettingListObject: function (datageojson, index) {
        var object = {};
        object.id = index;
        object.idDirectory = mapdata.GLOBAL.idDirectory;
        object.nameObject = "";
        object.listProperties = [];
        object.geometry = {};
        object.object3D = null;
        object.isCheckImplementProperties = false;
        object.tags = null;
        $.each(importmapdata.GLOBAL.lstProperties, function (i, obj) {
            var objectparam = {
                nameProperties: obj.NameProperties,
                codeProperties: obj.CodeProperties,
                typeProperties: obj.TypeProperties,
                isShow: obj.IsShow,
                isView: obj.IsView,
                defalutValue: $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + obj.CodeProperties).val(),
                orderProperties: obj.OrderProperties,
                typeSystem: obj.TypeSystem
            };
            if (obj.CodeProperties.toLowerCase() === "name") {
                object.nameObject = $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + obj.CodeProperties).val();
            }

            if (obj.CodeProperties.toLowerCase() === "name") {
                object.nameObject = $(importmapdata.SELECTORS.modelPropertiesImport + ' ' + '#text-modal-' + obj.CodeProperties).val();
            }
            if (obj.TypeProperties == "list") {
                let array = null;
                objectparam.defalutValue = $(importmapdata.SELECTORS.modelPropertiesImport + ' ' + '#text-modal-' + obj.CodeProperties).val();
            }

            if (obj.TypeProperties == "checkbox") {
                let arraychecked = $(importmapdata.SELECTORS.modelPropertiesImport + ' ' + ".text-modal-" + obj.CodeProperties + ":checked").map(function () {
                    return this.value;
                }).get();

                objectparam.defalutValue = arraychecked.toString();
            }
            if (obj.TypeProperties == "radiobutton") {
                let stringchecked = $(importmapdata.SELECTORS.modelPropertiesImport + ' ' + ".text-modal-" + obj.CodeProperties + ":checked").val();
                objectparam.defalutValue = stringchecked;
            }

            if (obj.TypeProperties == "bool") {
                //obj.DefalutValue = $("#text-modal-" + obj.CodeProperties).prop("checked");
                objectparam.defalutValue = $(importmapdata.SELECTORS.modelPropertiesImport + ' ' + "#text-modal-" + obj.CodeProperties + importmapdata.SELECTORS.modelPropertiesImport.replace('.', '')).prop("checked").toString();
            }

            object.listProperties.push(objectparam);
        });
        object.properties = Config_Main_Object.getValueProperties(importmapdata.GLOBAL.modelPropertiesImport);
        //let objectTemp = JSON.parse(JSON.stringify(datageojson));
        object.geometry = datageojson;
        return object;
        //importmapdata.GLOBAL.listMainObjetAndProperties.push(object);
    },
    setMenuNextBack: function () {
        let count = Number($(importmapdata.SELECTORS.munberObjectCurrent).text());
        //hide show menu
        $(importmapdata.SELECTORS.commonBackNext).removeAttr("disabled");
        if (count === 1) {
            $(importmapdata.SELECTORS.backObject).attr("disabled", true);
        }
        if (count === importmapdata.GLOBAL.dataGeojson.length) {
            $(importmapdata.SELECTORS.nextObject).attr("disabled", true);
        }
    },
    setCountNextBack: function (check) {
        let count = Number($(importmapdata.SELECTORS.munberObjectCurrent).text());
        if (check) {//back
            $(importmapdata.SELECTORS.munberObjectCurrent).text(count - 1);
        } else { //next
            $(importmapdata.SELECTORS.munberObjectCurrent).text(count + 1);
        }
    },
    showDataProperties: function (id) {
        if (importmapdata.GLOBAL.listMainObjetAndProperties !== null) {
            let obj = importmapdata.GLOBAL.listMainObjetAndProperties.find(x => x.id == id);
            if (typeof obj !== "undefined" && obj !== null) {
                importmapdata.setPropertiesOnFrom(obj.listProperties);
            } else {
                $(importmapdata.SELECTORS.modalLayer).val("");
                $(importmapdata.SELECTORS.modalLayer + '.div-checkbox-radio').find('input').prop('checked', false);
                $(importmapdata.SELECTORS.modalLayer + "[type='checkbox']").prop('checked', false);
                $(importmapdata.SELECTORS.modelPropertiesImport + " select").prop("selectedIndex", 0).trigger('change');
                $(importmapdata.SELECTORS.modalLayer).parents(".form-group.form-group-modal").find("iframe").contents().find('.wysihtml5-editor').html("");
                $('textarea[type="text"]').each(function () {
                    $(this).trumbowyg('html', "");
                });
                $(importmapdata.SELECTORS.modelPropertiesImport + " .detail-property-body").scrollTop(10);
            }
        }
    },
    /*--End properties and setting list obj--*/
    //save list object import 
    saveListObject: function (object) {
        importmapdata.showLoading(true);
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: true,
            url: importmapdata.CONSTS.URL_SAVELISTOBJECT,
            data: JSON.stringify(object),
            success: function (data) {
                importmapdata.showLoading(false);
                //console.log(data);
                if (data.code == "ok") {
                    mapdata.setOrRestBtnInsert(false);
                    abp.notify.success(l("ManagementLayer:CreateDataSuccesfully"));

                    // update geojson
                    let geojson = mapdata.getDataGeojsonByProperties(data.result.listobject);
                    mapdata.showDataGeojsonPush(JSON.stringify(geojson));

                    // cập nhật số lượng đối tượng
                    if (mapdata.GLOBAL.features != null && mapdata.GLOBAL.features != undefined) {
                        totalMain = mapdata.GLOBAL.features.length.toString();
                        $('.totalMainObject').html(`(${mapdata.GLOBAL.features.length})`);
                    }

                    importmapdata.showOrHideModelImport(false);
                    importmapdata.GLOBAL.dataGeojson = [];
                    importmapdata.GLOBAL.listMainObjetAndProperties = [];

                    menuLeft.resizeMenuLeft();
                    mapdata.ResetInsertMainOjbect();
                    $('.model-geojson').removeClass('active');
                } else {
                    abp.notify.error(l("ManagementLayer:UpdateDataFailed"));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
                importmapdata.showLoading(false);
            }
        });
    },
    //generate html properties form input
    getPropertiesDirectory: function () {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: importmapdata.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=' + mapdata.GLOBAL.idDirectory,
            data: {},
            success: function (data) {
                if (data.code == "ok" && data.result.listProperties != undefined) {
                    var result = [];
                    $(importmapdata.SELECTORS.modalBodyFolderLayerImport).html('');
                    $.each(data.result.listProperties, function (i, obj) {
                        if (obj.typeSystem !== 1) {
                            var element = {
                                NameProperties: obj.nameProperties,
                                CodeProperties: obj.codeProperties,
                                TypeProperties: obj.typeProperties,
                                IsShow: obj.isShow,
                                IsIndexing: obj.isIndexing,
                                IsRequest: obj.isRequest,
                                IsView: obj.isView,
                                IsHistory: obj.isHistory,
                                IsInheritance: obj.isInheritance,
                                IsAsync: obj.isAsync,
                                IsShowExploit: obj.isShowExploit,
                                DefalutValue: obj.defalutValue,
                                ShortDescription: obj.shortDescription,
                                TypeSystem: obj.typeSystem,
                                OrderProperties: obj.orderProperties
                            };
                            result.push(element);
                            if (element.TypeProperties != "geojson" && element.TypeProperties != "image"
                                && element.TypeProperties != "link" && element.TypeProperties != "maptile") {
                                var text = mapdata.selectInputViewInforObj(element, "model-properties-import");
                                $(importmapdata.SELECTORS.modalBodyFolderLayerImport).append(text);
                                if (element.TypeProperties == "date") {
                                    $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).datetimepicker({
                                        locale: 'vi',
                                        format: 'DD/MM/YYYY',
                                        useCurrent: false,
                                        defaultDate: moment(),
                                    }).on('dp.change', function (e) {
                                        $(e.currentTarget).parents(".form-group-modal").find(".label-error").remove();
                                        $(e.currentTarget).parents(".form-group-modal").removeClass("has-error");
                                        if ($(this).val() == "") {
                                            $(this).parent().find('.placeholder').removeClass('focus');
                                        }
                                    }).on('dp.hide', function () {
                                        if ($(this).val() == "") {
                                            $(this).parent().find('.placeholder').removeClass('focus');
                                        }
                                    }).on('dp.show', function () {
                                        $(this).parent().find('.placeholder').addClass('focus');
                                    });
                                } else if (element.TypeProperties == "text" || element.TypeProperties == "stringlarge") {

                                    setTimeout(function () {
                                        $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).trumbowyg({
                                            svgPath: '/libs/bootstrap-trumbowyg/icons.svg',
                                            btns: [
                                                ['viewHTML'],
                                                ['undo', 'redo'], // Only supported in Blink browsers
                                                ['formatting'],
                                                ['strong', 'em', 'del'],
                                                ['superscript', 'subscript'],
                                                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                                                ['unorderedList', 'orderedList'],
                                                ['horizontalRule'],
                                                ['removeformat'],
                                                ['fullscreen']
                                            ]
                                        }).on('tbwchange ', function (e) {
                                            //$(importmapdata.SELECTORS.modelPropertiesImport+" "+ '#text-modal-' + element.CodeProperties).trigger('change');
                                            if ($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).val() != "") {
                                                $(this).parents('.form-group-modal').find('.label-error').remove();
                                                $(this).parents('.form-group-modal').removeClass('has-error');
                                                //    $($(importmapdata.SELECTORS.modelPropertiesImport+" "+ '#text-modal-' + element.CodeProperties).parent().next()).addClass("textarea");
                                                //    let height = $('#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                                //    $($(importmapdata.SELECTORS.modelPropertiesImport+" "+ '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 68}px)`);
                                            }
                                        }).on('tbwfocus', function () {
                                            $($(this).parent().next()).addClass("textarea");
                                            let height = $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                            $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 68}px)`);
                                            $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().parent()).addClass('div-textarea-parent-focus');
                                        }).on('tbwblur', function () {
                                            if ($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).val() != "") {
                                                $($(this).parent().next()).addClass("textarea");
                                                let height = $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                                $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 68}px)`);
                                            } else {
                                                $($(this).parent().next()).removeClass("textarea");
                                                $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);

                                            }
                                            $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().parent()).removeClass('div-textarea-parent-focus');
                                        }).on('tbwresize', function () {
                                            let height = $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                            $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1.0) translateY(10px)`);
                                        });

                                        let height = $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                        //$($('#text-modal-' + element.CodeProperties).parent().next()).css('top', (height + 20) + 'px');
                                        $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().next()).attr('style', `top: ${(height + 20)}px !important`);
                                        $($(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1.0) translateY(10px)`);
                                        /*$($('#text-modal-' + element.CodeProperties).parent().next()).css('display', 'none');*/
                                    }, 200);
                                }
                                else if (element.TypeProperties == "list") {
                                    $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).select2()
                                        .on("select2:opening", function () {
                                            $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                        })
                                        .on("select2:closing", function () {
                                            var value = $(this).val();
                                            if (value != "" && value != null) {
                                                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                            }
                                            else {
                                                $(this).parent().find('.placeholder').attr('style', "");
                                            }
                                        })
                                        .on('select2:select', function () {
                                            var value = $(this).val();
                                            if (value != "" && value != null) {
                                                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                                $(this).trigger('change');
                                            }
                                        });

                                    $(importmapdata.SELECTORS.modelPropertiesImport + " " + '#text-modal-' + element.CodeProperties).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                }
                            }
                        }
                    });
                    importmapdata.GLOBAL.lstProperties = result;
                    importmapdata.showDefaultPropertiesCreateObject();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    setPropertiesOnFrom: function (data) {
        $(importmapdata.SELECTORS.modalLayer).val("");
        $(importmapdata.SELECTORS.modalLayer + '.div-checkbox-radio').find('input').prop('checked', false);
        $(importmapdata.SELECTORS.modalLayer + "[type='checkbox']").prop('checked', false);
        $(importmapdata.SELECTORS.modelPropertiesImport + " select").prop("selectedIndex", 0).trigger('change');
        $(importmapdata.SELECTORS.modalLayer).parents(".form-group.form-group-modal").find("iframe").contents().find('.wysihtml5-editor').html("");
        $('textarea[type="text"]').each(function () {
            $(this).trumbowyg('html', "");
        });

        $(importmapdata.SELECTORS.modelPropertiesImport + " .detail-property-body").scrollTop(10);

        $.each(data, function (i, obj) {

            let lstvalue;
            switch (obj.typeProperties) {
                case "list":
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);
                        if (lstvalue != undefined && $.isArray(lstvalue)) {
                            let value = lstvalue.find(x => x.checked);
                            if (value != null && value != undefined) {
                                $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val(value.code).trigger("select2:select");
                            }
                        }
                        else {
                            $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue).trigger("select2:select");
                        }
                    }
                    catch (e) {
                        if (obj.defalutValue != "" && obj.defalutValue != "null") {
                            $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue).trigger("select2:select");
                        }
                        else {
                            $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val($(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties + " option:first").val()).trigger("select2:select");
                        }

                        if ($(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val() == "" || $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val() == null || $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val() == "null") {
                            $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val($(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties + " option:first").val());
                        }
                    }
                    break;
                case "radiobutton":
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);
                        if (lstvalue != undefined && $.isArray(lstvalue)) {
                            let value = lstvalue.find(x => x.checked);
                            if (value != null && value != undefined) {
                                $(importmapdata.SELECTORS.modelPropertiesImport + ' .text-modal-' + obj.codeProperties + '[value = "' + value.code + '"]').prop("checked", true);
                            }
                        }
                        else {
                            $(importmapdata.SELECTORS.modelPropertiesImport + ' .text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                        }
                    }
                    catch (e) {
                        $(importmapdata.SELECTORS.modelPropertiesImport + ' .text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                    }
                    break;
                case "checkbox":
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);

                        if (lstvalue.length > 0 && $.isArray(lstvalue)) {
                            $.each(lstvalue, function (i, item) {
                                if (item.checked) {
                                    $(importmapdata.SELECTORS.modelPropertiesImport + ' .text-modal-' + obj.codeProperties + '[value = "' + item.code + '"]').prop("checked", true);
                                }
                            });
                        }
                        else {
                            var lstChecked = obj.defalutValue.split(",");
                            $.each(lstChecked, function (i, item) {
                                $(importmapdata.SELECTORS.modelPropertiesImport + ' .text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                            });
                        }
                    }
                    catch (e) {
                        if (obj.defalutValue != "" && obj.defalutValue != null) {
                            var lstChecked = obj.defalutValue.split(",");
                            $.each(lstChecked, function (i, item) {
                                $(importmapdata.SELECTORS.modelPropertiesImport + ' .text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                            });
                        }
                        //$('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                    }
                    break;
                case "bool":
                    if (obj.defalutValue != "false") {
                        var exp = importmapdata.SELECTORS.modelPropertiesImport.split(' ');

                        $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties + exp[0].replace('.', '')).prop('checked', true);
                    }
                    break;
                default:
                    if (obj.typeProperties != "text") {
                        $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue);
                    } else {
                        $(importmapdata.SELECTORS.modelPropertiesImport + ' #text-modal-' + obj.codeProperties).trumbowyg('html', obj.defalutValue);
                    }
                    break;
            }

        });
    },
    showDefaultPropertiesCreateObject: function () {
        ///show info modal create Object
        $(importmapdata.SELECTORS.propertyStrokeText).val(mapdata.GLOBAL.propertiesSetting.Stroke);
        $(importmapdata.SELECTORS.divStrokeImport).css('background-color', mapdata.GLOBAL.propertiesSetting.Stroke);

        $(importmapdata.SELECTORS.propertyStrokeWidth).val(mapdata.GLOBAL.propertiesSetting.StrokeWidth);

        const calcLeftPositionStress = value => 100 / (100 - 0) * (value - 0);
        let strokeOpacity = mapdata.GLOBAL.propertiesSetting.StrokeOpacity * 100;
        $(importmapdata.SELECTORS.propertyStrokeOpacity).val(strokeOpacity);

        $(importmapdata.SELECTORS.thumbMaxStrokeImport).css('left', calcLeftPositionStress(strokeOpacity) + '%');
        $(importmapdata.SELECTORS.maxStrokeSImport).html(strokeOpacity + '.00%');
        $(importmapdata.SELECTORS.lineStrokeImport).css({
            'right': (100 - calcLeftPositionStress(strokeOpacity)) + '%'
        });

        $(importmapdata.SELECTORS.propertyFillText).val(mapdata.GLOBAL.propertiesSetting.Fill);
        $(importmapdata.SELECTORS.divFillImport).css('background-color', mapdata.GLOBAL.propertiesSetting.Fill);

        let fillOpacity = mapdata.GLOBAL.propertiesSetting.FillOpacity * 100;
        $(importmapdata.SELECTORS.propertyFillOpacity).val(fillOpacity);
        $(importmapdata.SELECTORS.thumbMaxFillImport).css('left', calcLeftPositionStress(fillOpacity) + '%');
        $(importmapdata.SELECTORS.maxFillSImport).html(fillOpacity + '.00%');
        $(importmapdata.SELECTORS.lineFillImport).css({
            'right': (100 - calcLeftPositionStress(fillOpacity)) + '%'
        });
    },
    //check form
    checkformmodal: function () {
        let check = true;
        let hasFocus = false;
        importmapdata.clearLabelError();

        $(importmapdata.SELECTORS.modalLayer).each(function () {
            var type = $(this).attr("data-type");
            var status = $(this).attr("data-required").toLowerCase();
            if (status == "true") {
                switch (type) {
                    case "radio":
                    case "checkbox":
                        let inputName = $(this).attr('data-name');
                        var checkBoxRadio = false;
                        $(this).find('input').each(function (i, e) {
                            if ($(this).is(":checked")) {
                                checkBoxRadio = true;
                                return;
                            }
                        });

                        if (!checkBoxRadio) {
                            $(importmapdata.SELECTORS.modelPropertiesImport + " .title-li-tab .nav-link").eq(0).click();

                            check = checkBoxRadio;
                            insertError($(this), "other");
                            if (!hasFocus) {
                                hasFocus = true;
                                $(importmapdata.SELECTORS.modelPropertiesImport + " .detail-property-body").animate({
                                    scrollTop: $(importmapdata.SELECTORS.modelPropertiesImport + " .detail-property-body").scrollTop() - $(importmapdata.SELECTORS.modelPropertiesImport + " .detail-property-body").offset().top + $(this).offset().top
                                }, 50);
                            }

                            mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            return;
                        }
                        break;
                    default:
                        var typeconvert = mapdata.convertcheckTypemodal(type);
                        if (typeconvert != "") {
                            let inputTen = $(this).val();
                            let inputName = $(this).attr('data-name');

                            if (!validateText(inputTen, typeconvert, 0, 0)) {

                                $(importmapdata.SELECTORS.modelPropertiesImport + " .title-li-tab .nav-link").eq(0).click();

                                if ((type == "text" || type == "stringlarge") && inputTen.trim() != "") {
                                    return true;
                                }
                                insertError($(this), "other");
                                if (!hasFocus) {
                                    if (type == "text" || type == "stringlarge") {
                                        $(this).parent().find('.trumbowyg-editor').focus();
                                    }
                                    else {
                                        //console.log($(this).attr("id"));
                                        $(this).focus();
                                    }
                                    hasFocus = true;
                                }
                                check = false;
                                mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));

                                return;
                            }
                        }
                        break;
                }

            }
        });
        //mapdata.eventmodal();
        return check;
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group-modal").append("<label class='label-error' style='color:red'>" + text + "</label>");
        $(element).parents(".form-group-modal").addClass("has-error");
    },
    clearLabelError: function () {
        $(importmapdata.SELECTORS.hasError).find(".label-error").remove();
        $(importmapdata.SELECTORS.hasError).removeClass("has-error");
        $('.form-group.input-textarea').find(".label-error").remove();
        $('.form-group.input-textarea').find(".has-error").removeClass("has-error");
    },
    showLoading: function (isCheck) {
        if (isCheck) {
            $(importmapdata.SELECTORS.loading).show();
        }
        else {
            $(importmapdata.SELECTORS.loading).hide();
        }
    },
    getListObjectPaging: function (array, index, size) {
        index = Math.abs(parseInt(index));
        index = index > 0 ? index - 1 : index;
        size = parseInt(size);
        size = size < 1 ? 1 : size;

        // filter
        return [...(array.filter((value, n) => {
            return (n >= (index * size)) && (n < ((index + 1) * size))
        }))]
    },
    //-----End Import string wkt/geojson-----

    //---------------------------------------
    //---map properties layer geojson--------
    checkMenuGeojson: function () {
        if (importmapdata.GLOBAL.dataGeojson !== null && importmapdata.GLOBAL.dataGeojson.length > 0) {
            switch (mapdata.GLOBAL.selectmenu) {
                case "newinput":
                    importmapdata.showOrHideModelImport(true);
                    break;
                case "newinputpropertiesmap":
                    importmapdata.showOrHideMapProperties(true);
                    importmapdata.ceaterFormMapProperties();
                    break;
                case "newinputpropertiesnew":
                    importmapdata.showOrHideNewProperties(true);
                    importmapdata.createFormNewProperties();
                    break;
            }
        }
    },
    //creater html properties map geojson
    ceaterFormMapProperties: function () {

        if (importmapdata.GLOBAL.lstProperties === null) {
            importmapdata.getPropertiesDirectory();
        }
        if (importmapdata.GLOBAL.lstProperties !== null) {
            let html = "";
            $.each(importmapdata.GLOBAL.lstProperties, function (i, obj) {
                if (importmapdata.CONSTS.NOTPROPERTIES.indexOf(obj.TypeProperties) < 0) {
                    let opttion = importmapdata.getProperties(obj.CodeProperties);
                    let request = obj.IsRequest == true ? "required" : "";
                    //html += `<div class="col-md-12">
                    //            <div class="form-group map-layer-geojson" data-id="${obj.CodeProperties}" data-isrequest="${obj.IsRequest}">
                    //                <label class="control-label ${request}" >${obj.NameProperties} (${obj.CodeProperties})</label>
                    //                <select class="form-control">
                    //                    <option value=''>--${l('ManagementLayer:SelectProperty')}--</option>
                    //                    ${opttion}
                    //                </select>
                    //            </div>
                    //        </div>`;

                    html += `<div class="w-100">
                                <div class="form-group" style="margin-bottom: 0; width: 100%;">
                                    <div class="w-100 position-relative">
                                        <div class="form-group form-group-modal map-layer-geojson" data-id="${obj.CodeProperties}" data-isrequest="${obj.IsRequest}">
                                            <select class="form-control modal-layer" data-id="${obj.CodeProperties}" data-name="${obj.NameProperties}" data-required="${obj.IsRequest}" required="">
                                                <option value=''></option>   
                                                ${opttion}
                                            </select>
                                            <span class="placeholder">${obj.NameProperties}<span class="${request}"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>`
                }
            });
            $(importmapdata.SELECTORS.contentMapProperties).children().remove();
            $(importmapdata.SELECTORS.contentMapProperties).append(html);

            $(importmapdata.SELECTORS.modalLayer).select2().on("select2:opening", function () {
                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
            })
                .on("select2:closing", function () {
                    var value = $(this).val();
                    if (value != "" && value != null) {
                        $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                    }
                    else {
                        $(this).parent().find('.placeholder').attr('style', "");
                    }
                })
                .on('select2:select', function () {
                    var value = $(this).val();
                    if (value != "" && value != null) {
                        $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                        $(this).trigger('change');
                    }
                });

            $(importmapdata.SELECTORS.contentMapProperties + " select").val("").trigger("change");
        }
    },
    //get properties geojson
    getProperties: function (code) {
        let html = "";
        if (importmapdata.GLOBAL.dataGeojson !== null && importmapdata.GLOBAL.dataGeojson.length > 0) {
            let listproper = importmapdata.GLOBAL.dataGeojson[0].properties;
            let list = listproper !== null ? Object.keys(listproper) : null;
            $.each(list, function (i, obj) {
                if (code.toLowerCase() == obj.toLowerCase()) {
                    html += "<option value='" + obj + "' selected>" + obj + "</option>";
                } else {
                    html += "<option value='" + obj + "'>" + obj + "</option>";
                }
            });
        } else if (importmapdata.GLOBAL.listTitleExcel !== null && importmapdata.GLOBAL.listTitleExcel.length > 0) {
            $.each(importmapdata.GLOBAL.listTitleExcel, function (i, obj) {
                if (code.toLowerCase() == obj.toLowerCase()) {
                    html += "<option value='" + obj + "' selected>" + obj + "</option>";
                } else {
                    html += "<option value='" + obj + "'>" + obj + "</option>";
                }
            });
        }
        return html;
    },
    showOrHideMapProperties: function (check) {
        if (check) {
            //$(importmapdata.SELECTORS.modelMapProperties).modal("show");
            $(importmapdata.SELECTORS.modelMapProperties).addClass('active');

        } else {
            $(importmapdata.SELECTORS.modelMapProperties).removeClass('active');
        }
    },
    //setup data geojson
    setupDataMapLayerGeojson: function () {
        if (importmapdata.GLOBAL.dataGeojson !== null && importmapdata.GLOBAL.dataGeojson.length > 0) {
            let listobject = [];
            //importmapdata.GLOBAL.listMainObjetAndProperties = [];
            var object = {};
            object.id = mapdata.uuidv4;
            object.idDirectory = mapdata.GLOBAL.idDirectory;
            object.nameObject = "Object Demo";
            object.listProperties = [];
            object.geometry = {};
            object.object3D = null;
            object.isCheckImplementProperties = false;
            object.tags = null;
            let listProperties = importmapdata.getPropertiesMap();
            object.properties = {
                image2D: null,
                stroke: mapdata.GLOBAL.propertiesSetting.Stroke,
                strokeWidth: mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                strokeOpacity: mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                styleStroke: null,
                fill: mapdata.GLOBAL.propertiesSetting.Fill,
                fillOpacity: mapdata.GLOBAL.propertiesSetting.FillOpacity
            };
            $.each(importmapdata.GLOBAL.dataGeojson, function (i, obj) {
                let propertiesmap = JSON.parse(JSON.stringify(listProperties));
                let objectTemp = JSON.parse(JSON.stringify(object));
                let properties = JSON.parse(JSON.stringify(obj.properties));
                objectTemp.geometry = obj.geometry;
                objectTemp.tags = properties;
                let name = importmapdata.setPropertiesMap(propertiesmap, obj);
                objectTemp.listProperties = propertiesmap;
                objectTemp.nameObject = name;
                listobject.push(objectTemp);
                if ((i + 1) % importmapdata.CONSTS.LIMITSENDAJAX === 0) {
                    importmapdata.sendDataMapProperties(JSON.parse(JSON.stringify(listobject)));
                    listobject = [];
                }
            });
            importmapdata.sendDataMapProperties(JSON.parse(JSON.stringify(listobject)));
            importmapdata.GLOBAL.dataGeojson = null;
        } else {
            importmapdata.setupDataMapLayerExcel();
        }
    },
    getPropertiesMap: function () {
        importmapdata.GLOBAL.listForMapProperties = [];
        let listProperties = [];
        $.each(importmapdata.GLOBAL.lstProperties, function (i, obj) {
            let check = importmapdata.GLOBAL.listMapLayerAndGeojson[obj.CodeProperties.toLowerCase()];
            if (typeof check !== "undefined" && check !== "" && check.length > 0) {
                importmapdata.GLOBAL.listForMapProperties.push(i);
            }
            var objectparam = {
                nameProperties: obj.NameProperties,
                codeProperties: obj.CodeProperties,
                typeProperties: obj.TypeProperties,
                isShow: obj.IsShow,
                isView: obj.IsView,
                defalutValue: "",
                orderProperties: obj.OrderProperties,
                typeSystem: obj.TypeSystem
            };
            listProperties.push(objectparam);
        });
        return listProperties;
    },
    setPropertiesMap: function (listProperties, feature) {
        let name = "";
        $.each(importmapdata.GLOBAL.listForMapProperties, function (i, obj) {
            let objProperties = listProperties[obj];
            let check = importmapdata.GLOBAL.listMapLayerAndGeojson[objProperties.codeProperties.toLowerCase()];
            if (typeof check !== "undefined" && check !== "" && check.length > 0) {
                objProperties.defalutValue = "" + feature.properties[check];
                if (objProperties.codeProperties.toLowerCase() === "name") {
                    name = objProperties.defalutValue;
                }
            }
        });
        return name;
    },
    sendDataMapProperties: function (datageojson) {
        console.log(datageojson)
        if (importmapdata.GLOBAL.countMapSend.SendMap === 0) {
            importmapdata.showLoading(true);
        }
        importmapdata.GLOBAL.countMapSend.SendMap += 1;
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: true,
            url: importmapdata.CONSTS.URL_SAVELISTOBJECT,
            data: JSON.stringify(datageojson),
            success: function (data) {
                //console.log(data);
                importmapdata.GLOBAL.countMapSend.Reponse += 1;
                if (data.code == "ok") {
                    importmapdata.GLOBAL.countMapSend.ReponseSucces += 1;

                    // cập nhật geojson
                    let geojson = mapdata.getDataGeojsonByProperties(data.result.listobject);
                    mapdata.showDataGeojsonPush(JSON.stringify(geojson));

                    // cập nhật số lượng đối tượng
                    if (mapdata.GLOBAL.features != null && mapdata.GLOBAL.features != undefined) {
                        totalMain = mapdata.GLOBAL.features.length.toString();
                        $('.totalMainObject').html(`(${mapdata.GLOBAL.features.length})`);
                    }

                    importmapdata.GLOBAL.listErrorReponse.concat(data.result.listerror);

                }
                if (importmapdata.GLOBAL.countMapSend.SendMap === importmapdata.GLOBAL.countMapSend.Reponse && importmapdata.GLOBAL.countMapSend.Reponse === importmapdata.GLOBAL.countMapSend.ReponseSucces) {

                    abp.notify.success(l("ManagementLayer:CreateDataSuccesfully"));
                    importmapdata.showOrHideExcelProperties(false);
                    importmapdata.showOrHideMapProperties(false);
                    importmapdata.showOrHideNewProperties(false);
                    importmapdata.showLoading(false);
                    mapdata.setOrRestBtnInsert(false);
                    menuLeft.resizeMenuLeft();
                    importmapdata.GLOBAL.countMapSend.SendMap = 0
                    importmapdata.GLOBAL.countMapSend.ReponseSucces = 0;
                    importmapdata.GLOBAL.countMapSend.Reponse = 0;
                    importmapdata.CloseContentImportGeoJson();
                    mapdata.ResetInsertMainOjbect();
                } else if (importmapdata.GLOBAL.countMapSend.SendMap === importmapdata.GLOBAL.countMapSend.Reponse) {
                    let str = l("ManagementLayer:UpdateObjectFailed{0}", importmapdata.GLOBAL.listErrorReponse.length);
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: str,
                        icon: "warning",
                        button: l("ManagementLayer:Close"),
                    })

                    abp.notify.warn(str);
                    importmapdata.showOrHideExcelProperties(false);
                    importmapdata.showOrHideMapProperties(false);
                    importmapdata.showLoading(false);
                    mapdata.setOrRestBtnInsert(false);
                    importmapdata.GLOBAL.countMapSend.SendMap = 0;
                    importmapdata.GLOBAL.countMapSend.ReponseSucces = 0;
                    importmapdata.GLOBAL.countMapSend.Reponse = 0;
                    importmapdata.CloseContentImportGeoJson();
                    mapdata.ResetInsertMainOjbect();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
                importmapdata.showLoading(false);
            }
        });
    },
    //---End map properties layer geojson-----

    //----------------------------------------
    //---New properties with geojson----------
    showOrHideNewProperties: function (check) {
        if (check) {
            $(importmapdata.SELECTORS.modelVewProperties).addClass('active');
        } else {
            $(importmapdata.SELECTORS.modelVewProperties).removeClass('active');
        }
    },
    //create from new properties
    createFormNewProperties: function () {
        let listproperties = importmapdata.GLOBAL.dataGeojson[0].properties;
        let list = listproperties !== null ? Object.keys(listproperties) : null;
        let html = "";
        $.each(list, function (i, obj) {
            //html += `<div class="col-md-6">
            //            <div class="form-group">
            //                <label><input type="checkbox" style="margin-right: 5px;" value="`+ obj + `" checked> ` + obj + `</label>
            //            </div>
            //         </div>`;


            html += `<div class="col-md-6">
                        <div class="form-group">
                            <div class="custom-checkbox custom-control">
                                <input type="checkbox" data-val="true" id="${obj}" name="${obj}" value="${obj}" class="custom-control-input">
                                <label class="custom-control-label" for="${obj}">
                                   ${obj}
                                </label>
                            </div>
                        </div>
                     </div>`;
        });
        $(importmapdata.SELECTORS.contentNewProperties).children().remove();
        $(importmapdata.SELECTORS.contentNewProperties).append(html);
    },
    //create layer properties auto code
    createLayerPropertiesAuto: function () {
        importmapdata.GLOBAL.listMapLayerAndGeojson = {};
        if (importmapdata.GLOBAL.lstProperties === null) {
            importmapdata.getPropertiesDirectory();
        }
        if (importmapdata.GLOBAL.lstProperties !== null) {
            $(importmapdata.SELECTORS.inputNewProperties).each(function () {
                if ($(this).prop('checked')) {
                    let valueProperties = $(this).val().toLowerCase();
                    importmapdata.GLOBAL.listCheckNewProperties.push(valueProperties);
                    let propertiesDirectory = JSON.parse(JSON.stringify(importmapdata.GLOBAL.lstProperties[importmapdata.GLOBAL.lstProperties.length - 1]));
                    propertiesDirectory.CodeProperties = valueProperties;
                    propertiesDirectory.NameProperties = valueProperties;
                    propertiesDirectory.TypeProperties = "string";
                    propertiesDirectory.IsAsync = false;
                    propertiesDirectory.IsHistory = false;
                    propertiesDirectory.IsIndexing = false;
                    propertiesDirectory.IsInheritance = false;
                    propertiesDirectory.IsShowExploit = false;
                    propertiesDirectory.IsRequest = false;
                    propertiesDirectory.IsShow = true;
                    propertiesDirectory.OrderProperties = propertiesDirectory.OrderProperties + 1;
                    propertiesDirectory.ShortDescription = "";
                    propertiesDirectory.TypeSystem = 3;
                    propertiesDirectory.DefalutValue = "";
                    importmapdata.GLOBAL.listPropertiesNew.push(propertiesDirectory);
                }
            });
            var object = {
                IdDirectory: mapdata.GLOBAL.idDirectory,
                NameTypeDirectory: "",
                CodeTypeDirectory: "",
                VerssionTypeDirectory: 0,
                IdImplement: "",
                ListProperties: importmapdata.GLOBAL.listPropertiesNew,
                GroundMaptile: "",
                Location: { lat: 0, lng: 0 },
                Zoom: 0
            };
            importmapdata.updateDataLayerProperties(object);
        }
    },
    //send data layer properties
    updateDataLayerProperties: function (object) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: importmapdata.CONSTS.URL_URLSAVEPROPERTIESLAYER,
            data: JSON.stringify(object),
            success: function (data) {
                if (data.code == "ok") {
                    let result = [];
                    $.each(data.result.listProperties, function (i, obj) {
                        if (obj.typeSystem !== 1) {
                            var element = {
                                NameProperties: obj.nameProperties,
                                CodeProperties: obj.codeProperties,
                                TypeProperties: obj.typeProperties,
                                IsShow: obj.isShow,
                                IsIndexing: obj.isIndexing,
                                IsRequest: obj.isRequest,
                                IsView: obj.isView,
                                IsHistory: obj.isHistory,
                                IsInheritance: obj.isInheritance,
                                IsAsync: obj.isAsync,
                                IsShowExploit: obj.isShowExploit,
                                DefalutValue: obj.defalutValue,
                                ShortDescription: obj.shortDescription,
                                TypeSystem: obj.typeSystem,
                                OrderProperties: obj.orderProperties
                            };
                            result.push(element);
                        }
                    });
                    importmapdata.GLOBAL.lstProperties = result;
                } else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:UpdateDataFailed"),
                        icon: "warning",
                        button: l("ManagementLayer:Close"),
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
                importmapdata.showLoading(false);
            }
        });
    },
    //set properties new geojson
    setPropertiesNew: function (listProperties, feature) {
        let name = "";
        $(importmapdata.SELECTORS.inputNewProperties).each(function () {
            if ($(this).prop('checked')) {
                let value = $(this).val();
                let obj = listProperties.find(x => x.codeProperties.toLowerCase() == value.toLowerCase());
                if (obj !== null) {
                    obj.defalutValue = "" + feature.properties[value];
                    if (obj.codeProperties.toLowerCase() === "name") {
                        name = obj.defalutValue;
                    }
                }
            }
        });
        return name;
    },
    //send data ajax new geojson
    setupDataNewProperties: function () {
        let listobject = [];
        var object = {};
        object.id = mapdata.uuidv4;
        object.idDirectory = mapdata.GLOBAL.idDirectory;
        object.nameObject = "Object Demo";
        object.listProperties = [];
        object.geometry = {};
        object.object3D = null;
        object.isCheckImplementProperties = false;
        object.tags = null;
        let listProperties = importmapdata.getPropertiesMap();
        object.properties = {
            image2D: null,
            stroke: mapdata.GLOBAL.propertiesSetting.Stroke,
            strokeWidth: mapdata.GLOBAL.propertiesSetting.StrokeWidth,
            strokeOpacity: mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
            styleStroke: null,
            fill: mapdata.GLOBAL.propertiesSetting.Fill,
            fillOpacity: mapdata.GLOBAL.propertiesSetting.FillOpacity
        };

        $.each(importmapdata.GLOBAL.dataGeojson, function (i, obj) {
            let propertiesmap = JSON.parse(JSON.stringify(listProperties));
            let objectTemp = JSON.parse(JSON.stringify(object));
            let properties = JSON.parse(JSON.stringify(obj.properties));
            objectTemp.geometry = obj.geometry;
            objectTemp.tags = properties;
            let name = importmapdata.setPropertiesNew(propertiesmap, obj);
            objectTemp.listProperties = propertiesmap;
            objectTemp.nameObject = (typeof name !== "undefined" && name != "") ? name : l("ManagementLayer:Undefined");
            listobject.push(objectTemp);
            if ((i + 1) % importmapdata.CONSTS.LIMITSENDAJAX === 0) {
                importmapdata.sendDataMapProperties(JSON.parse(JSON.stringify(listobject)));
                listobject = [];
            }
        });
        importmapdata.sendDataMapProperties(JSON.parse(JSON.stringify(listobject)));
    },
    fitBoundGeometry: function (lstdata) {
        if (lstdata != undefined && lstdata.length > 0) {
            let latLngBounds = new map4d.LatLngBounds();
            let paddingOptions = {
                top: 10,
                bottom: 50,
                left: 50,
                right: 50
            };
            $.each(lstdata, function (iq, data) {
                if (data.geometry.type == "Point") {
                    latLngBounds.extend(data.geometry.coordinates);
                } else {
                    if (data.geometry.type == "Polygon" || data.geometry.type == "MultiLineString") {
                        for (var i = 0; i < data.geometry.coordinates[0].length; i++) {
                            latLngBounds.extend(data.geometry.coordinates[0][i]);
                        }
                    } else {
                        if (data.geometry.type == "MultiPolygon") {
                            for (var im = 0; im < data.geometry.coordinates[0].length; im++) {
                                for (var ip = 0; ip < data.geometry.coordinates[0][im].length; ip++) {
                                    latLngBounds.extend(data.geometry.coordinates[0][im][ip]);
                                }
                            }
                        } else {
                            for (var iz = 0; iz < data.geometry.coordinates.length; iz++) {
                                latLngBounds.extend(data.geometry.coordinates[iz]);
                            }
                        }
                    }
                }
            });
            map.fitBounds(latLngBounds, paddingOptions);
        }
    },
    CloseContentImportGeoJson: function () {
        $(importmapdata.SELECTORS.error_content_fileGeojson_input).addClass('hidden');
        $(importmapdata.SELECTORS.content_file_geojson_upload).addClass('hidden');
        $(importmapdata.SELECTORS.fileGeojson).val(null).trigger("change");
        $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);
        $(importmapdata.SELECTORS.geojsonWkt).val("");
        $(importmapdata.SELECTORS.content_geojson).removeClass('active');

        //---------------------------------------------
    },
    CloseContentInputGeoJson: function () {
        mapdata.setOrRestBtnInsert(false);
        mapdata.clearAllDraw();
        modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;

        $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);
        $(importmapdata.SELECTORS.btn_next_excel).prop('disabled', true);
        $('.detail-property-new').removeClass('active');
    },
    //---End New properties with geojson------
    ListCheckGeojsonPattern: function (data) {
        let check = false;
        $.each(data, function (i, obj) {
            check = importmapdata.CheckGeojsonPattern(obj.geometry);
            if (check) return false;
        });
        return check;
    },
    CheckGeojsonPattern: function (geometry) {
        let type = geometry.type.toLowerCase();
        let check = false;
        switch (type) {
            case "polygon":
                $.each(geometry.coordinates, function (i, obj) {
                    check = importmapdata.FindIndexLatLngEmpty(obj);
                    if (check) {
                        return false;
                    }
                });
                break;
            case "multipolygon":
                $.each(geometry.coordinates, function (i, obj) {
                    $.each(obj, function (i, obj1) {
                        check = importmapdata.FindIndexLatLngEmpty(obj1);
                        if (check) {
                            return false;
                        }
                    });
                });
                break;
            case "point":
                check = geometry.coordinates.length <= 1 ? true : false;
                break;
            case "multipoint":
                check = importmapdata.FindIndexLatLngEmpty(geometry.coordinates);
                break;
            case "line":
            case "linestring":
                check = importmapdata.FindIndexLatLngEmpty(geometry.coordinates);
                break;
            case "multiline":
                $.each(geometry.coordinates, function (i, obj) {
                    check = importmapdata.FindIndexLatLngEmpty(obj);
                    if (check) {
                        return false;
                    }
                });
                break;
            case "multilinestring":
                $.each(geometry.coordinates, function (i, obj) {
                    $.each(obj, function (i, obj1) {
                        check = importmapdata.FindIndexLatLngEmpty(obj1);
                        if (check) {
                            return false;
                        }
                    });
                });
                break;
            default:
        }
        return check;
    },
    FindIndexLatLngEmpty: function (array) {
        let index = 0;
        index = array.findIndex(x => x.length <= 1);
        return index <= -1 ? false : true;
    },

    //---------execl--------------------
    //creater html properties map excel
    createrFormExcelProperties: function () {
        //if (importmapdata.GLOBAL.lstProperties === null) {
        //    importmapdata.getPropertiesDirectory();
        //}
        //if (importmapdata.GLOBAL.lstProperties !== null) {
        //    let html = "";
        //    $.each(importmapdata.GLOBAL.lstProperties, function (i, obj) {
        //        if (importmapdata.CONSTS.NOTPROPERTIES.indexOf(obj.TypeProperties) < 0) {
        //            let opttion = importmapdata.getPropertiesExcel(obj.CodeProperties);
        //            let request = obj.IsRequest == true ? "required" : "";
        //            html += `<div class="col-md-6">
        //                        <div class="form-group map-layer-excel" data-id="${obj.CodeProperties}" data-isrequest="${obj.IsRequest}">
        //                            <label class="control-label ${request}" >${obj.NameProperties} (${obj.CodeProperties})</label>
        //                            <select class="form-control">
        //                                <option value=''>--chọn thuộc tính--</option>
        //                                ${opttion}
        //                            </select>
        //                        </div>
        //                    </div>`;
        //        }
        //    });
        //    $(importmapdata.SELECTORS.contentExcelProperties).children().remove();
        //    $(importmapdata.SELECTORS.contentExcelProperties).css('display', 'block');
        //    $(importmapdata.SELECTORS.contentExcelProperties).append(html);
        //}

        if (importmapdata.GLOBAL.lstProperties === null) {
            importmapdata.getPropertiesDirectory();
        }
        if (importmapdata.GLOBAL.lstProperties !== null) {
            let html = "";
            $.each(importmapdata.GLOBAL.lstProperties, function (i, obj) {
                if (importmapdata.CONSTS.NOTPROPERTIES.indexOf(obj.TypeProperties) < 0) {
                    let opttion = importmapdata.getPropertiesExcel(obj.CodeProperties);
                    let request = obj.IsRequest == true ? "required" : "";

                    html += `<div class="w-100">
                                <div class="form-group" style="margin-bottom: 0; width: 100%;">
                                    <div class="w-100 position-relative">
                                        <div class="form-group form-group-modal map-layer-geojson" data-id="${obj.CodeProperties}" data-isrequest="${obj.IsRequest}">
                                            <select class="form-control modal-layer" data-id="${obj.CodeProperties}" data-name="${obj.NameProperties}" data-required="${obj.IsRequest}" required="">
                                                <option value=''></option>   
                                                ${opttion}
                                            </select>
                                            <span class="placeholder">${obj.NameProperties}<span class="${request}"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>`
                }
            });
            $(importmapdata.SELECTORS.contentMapProperties).children().remove();
            $(importmapdata.SELECTORS.contentMapProperties).append(html);

            $(importmapdata.SELECTORS.modalLayer).select2().on("select2:opening", function () {
                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
            })
                .on("select2:closing", function () {
                    var value = $(this).val();
                    if (value != "" && value != null) {
                        $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                    }
                    else {
                        $(this).parent().find('.placeholder').attr('style', "");
                    }
                })
                .on('select2:select', function () {
                    var value = $(this).val();
                    if (value != "" && value != null) {
                        $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                        $(this).trigger('change');
                    }
                });

            $(importmapdata.SELECTORS.contentMapProperties + " select").val("").trigger("change");
        }
    },
    //get properties excel
    getPropertiesExcel: function (code) {
        let html = "";
        if (importmapdata.GLOBAL.listDataExcel.length > 0) {
            let listproper = importmapdata.GLOBAL.listTitleExcel;
            //let list = listproper !== null ? Object.keys(listproper) : null;
            $.each(listproper, function (i, obj) {
                if (importmapdata.CONSTS.NOTPROPERTIES.toLowerCase().indexOf(obj.toLowerCase()) < 0) {
                    if (code.toLowerCase() == obj.toLowerCase()) {
                        html += "<option value='" + obj + "' selected>" + obj + "</option>";
                    } else {
                        html += "<option value='" + obj + "'>" + obj + "</option>";
                    }
                }
            });
        }
        return html;
    },
    getPropertiesMapExcel: function () {
        importmapdata.GLOBAL.listForMapExcelProperties = [];
        let listProperties = [];
        $.each(importmapdata.GLOBAL.lstProperties, function (i, obj) {
            let check = importmapdata.GLOBAL.listMapLayerAndGeojson[obj.CodeProperties.toLowerCase()];
            if (typeof check !== "undefined" && check !== "" && check.length > 0) {
                importmapdata.GLOBAL.listForMapExcelProperties.push(i);
            }
            var objectparam = {
                nameProperties: obj.NameProperties,
                codeProperties: obj.CodeProperties,
                typeProperties: obj.TypeProperties,
                isShow: obj.IsShow,
                isView: obj.IsView,
                defalutValue: "",
                orderProperties: obj.OrderProperties,
                typeSystem: obj.TypeSystem
            };
            listProperties.push(objectparam);
        });
        return listProperties;
    },
    setPropertiesMapExcel: function (listProperties, feature) {
        let name = "";
        $.each(importmapdata.GLOBAL.listForMapExcelProperties, function (i, obj) {
            let objProperties = listProperties[obj];
            let check = importmapdata.GLOBAL.listMapLayerAndGeojson[objProperties.codeProperties.toLowerCase()];
            if (typeof check !== "undefined" && check !== "" && check.length > 0) {
                objProperties.defalutValue = (feature[check] == undefined || typeof (feature[check]) == "undefined") ? "" : feature[check];
                if (objProperties.typeProperties == "list" || objProperties.typeProperties == "checkbox" || objProperties.typeProperties == "radiobutton") {
                    var objs = mapdata.GLOBAL.lstPropertiesDirectory.find(x => x.codeProperties == objProperties.codeProperties);
                    if (objs != null && objs != undefined) {
                        try {
                            var lst = JSON.parse(objs.defalutValue);
                            var objlst = lst.find(x => x.name.toLowerCase() == objProperties.defalutValue.toLowerCase()
                                || x.code.toLowerCase() == objProperties.defalutValue.toLowerCase());
                            if (objlst != null && objlst != undefined) {
                                objProperties.defalutValue = objlst.code;
                            }
                        } catch (ex) {

                        }
                    }
                }
                if (objProperties.codeProperties.toLowerCase() === "name") {
                    name = objProperties.defalutValue;
                }
            }
        });
        return name;
    },
    //setup data geojson
    setupDataMapLayerExcel: function () {
        let listobject = [];
        //importmapdata.GLOBAL.listMainObjetAndProperties = [];
        var object = {};
        object.id = mapdata.uuidv4;
        object.idDirectory = mapdata.GLOBAL.idDirectory;
        object.nameObject = "Object Demo";
        object.listProperties = [];
        object.geometry = {};
        object.object3D = null;
        object.isCheckImplementProperties = false;
        object.tags = null;
        let listProperties = importmapdata.getPropertiesMapExcel();
        object.properties = {
            image2D: null,
            stroke: mapdata.GLOBAL.propertiesSetting.Stroke,
            strokeWidth: mapdata.GLOBAL.propertiesSetting.StrokeWidth,
            strokeOpacity: mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
            styleStroke: null,
            fill: mapdata.GLOBAL.propertiesSetting.Fill,
            fillOpacity: mapdata.GLOBAL.propertiesSetting.FillOpacity
        };
        $.each(importmapdata.GLOBAL.dataExcelGeojson, function (i, obj) {
            let propertiesmap = JSON.parse(JSON.stringify(listProperties));
            let objectTemp = JSON.parse(JSON.stringify(object));
            let properties = JSON.parse(JSON.stringify(obj.properties));
            objectTemp.geometry = obj.geometry;
            objectTemp.tags = properties;
            let propertiesExcel = importmapdata.GLOBAL.listDataExcel[i];
            let name = importmapdata.setPropertiesMapExcel(propertiesmap, propertiesExcel);
            objectTemp.listProperties = propertiesmap;
            objectTemp.nameObject = name;
            listobject.push(objectTemp);
            if ((i + 1) % importmapdata.CONSTS.LIMITSENDAJAX === 0) {
                importmapdata.sendDataMapProperties(JSON.parse(JSON.stringify(listobject)));
                listobject = [];
            }
        });
        importmapdata.sendDataMapProperties(JSON.parse(JSON.stringify(listobject)));
    },
    convertExceltoGeojson: function (data,column) {
        importmapdata.GLOBAL.dataExcelGeojson = [];
        let listobj = [];
        //let data = values.split("\n");
        let dataexcel = [];
        let listError = [];
        $.each(data, function (i, obj) {
            try {
                let geojson = JSON.parse(obj[column]);
                if (!importmapdata.CheckGeojsonPattern(geojson)) {
                    let feature = {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": geojson.type,
                            "coordinates": geojson.coordinates
                        }
                    }
                    listobj.push(feature);
                    dataexcel.push(obj);
                } else {
                    let cell = importmapdata.getRowColumnExcel(i, column);
                    listError.push(cell);
                }
                
                //console.log(geojson);
            }
            catch (err) {
                let cell = importmapdata.getRowColumnExcel(i, column);
                listError.push(cell);
                //swal({
                //    title: "Thông báo",
                //    text: "Dữ liệu coordinates không hợp lệ.",
                //    icon: "error",
                //    buttons: 'Đóng',
                //});
                //return false;
            }
        });
        importmapdata.GLOBAL.dataExcelGeojson = listobj;
        importmapdata.GLOBAL.listDataExcel = dataexcel;
        importmapdata.GLOBAL.listErrorColumnRow = listError;
        if (importmapdata.GLOBAL.dataExcelGeojson.length > 0 && importmapdata.GLOBAL.listDataExcel.length > 0 && listError <=0) {
            return true;
        } else {
            return false;
        }

    },
    getDataExcel: function (data) {
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //get the name of First Sheet.
        var Sheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[Sheet]);
        var excelRowstemp = excelRows.filter(x => x.COLLECTION !== "" && x.COLLECTION !== undefined && x.COLLECTION !== null);
        if (excelRowstemp.length > 0) {
            var cell = XLSX.utils.cell_set_number_format(workbook.Sheets[Sheet]);
            var arrayColumn = this.buildColumnsArray(cell['!ref']);
            this.getTitleColumn(cell, arrayColumn);
            //importmapdata.GLOBAL.listTitleExcel = Object.keys(excelRows[0]);
            importmapdata.GLOBAL.listDataExcel = excelRows;
            let status = importmapdata.convertExceltoGeojson(importmapdata.GLOBAL.listDataExcel, "COLLECTION");
            if (status) {
                $(importmapdata.SELECTORS.btn_next_excel).removeAttr("disabled");
                //importmapdata.showOrHideModelExcel(true);
                //importmapdata.showOrHideMapProperties(true);
                //importmapdata.ceaterFormMapProperties();

                //importmapdata.createrFormExcelProperties();
            } else {
                $(importmapdata.SELECTORS.btn_next_excel).prop('disabled', true);
                if (importmapdata.GLOBAL.listErrorColumnRow.length > 0) {
                    let error = importmapdata.GLOBAL.listErrorColumnRow.toString().replaceAll(",", ", ");
                    var htmlError = `<div class="text-danger" style="font-style:italic;">${l("ManagementLayer:FileNotCorrrectFormat")} - Lỗi dữ liệu những ô sau: ${error}</div>`;
                    $(importmapdata.SELECTORS.error_content_execl_input).html(htmlError);
                    $(importmapdata.SELECTORS.error_content_execl_input).removeClass('hidden');
                }
                importmapdata.GLOBAL.listTitleExcel = [];
                importmapdata.GLOBAL.listDataExcel = [];
            }
        } else {
            $(importmapdata.SELECTORS.btn_next_excel).prop('disabled', true);
            var htmlError = `<div class="text-danger" style="font-style:italic;">${l("ManagementLayer:Warring:Geojson")}</div>`;
            $(importmapdata.SELECTORS.error_content_execl_input).html(htmlError);
            $(importmapdata.SELECTORS.error_content_execl_input).removeClass('hidden');
        }
        abp.ui.clearBusy(importmapdata.SELECTORS.model_excel);
    },
    alphaToNum: function (alpha) {
        var i = 0,
            num = 0,
            len = alpha.length;
        for (; i < len; i++) {
            num = num * 26 + alpha.charCodeAt(i) - 0x40;
        }
        return num - 1;
    },
    numToAlpha: function (num) {
        var alpha = '';
        for (; num >= 0; num = parseInt(num / 26, 10) - 1) {
            alpha = String.fromCharCode(num % 26 + 0x41) + alpha;
        }
        return alpha;
    },
    buildColumnsArray: function (range) {
        var i,
            res = [],
            rangeNum = range.split(':').map(function (val) {
                return importmapdata.alphaToNum(val.replace(/[0-9]/g, ''));
            }),
            start = rangeNum[0],
            end = rangeNum[1] + 1;
        for (i = start; i < end; i++) {
            res.push(importmapdata.numToAlpha(i));
        }
        return res;
    },
    getTitleColumn: function (array, listFirstCode) {
        importmapdata.GLOBAL.listColumnRow = [];
        for (var j = 1; j < 10; j++) {
            $.each(listFirstCode, function (i, obj) {
                if (array[obj + j] != undefined) {
                    var text = array[obj + j].v;
                    if (text != null && text != undefined && typeof (text) != "undefined") {
                        importmapdata.GLOBAL.listTitleExcel.push(text);
                        importmapdata.GLOBAL.listColumnRow.push({ title: text, cloumn: obj, row: j });
                    }
                }
            });
            if (importmapdata.GLOBAL.listTitleExcel.length > 1) {
                return false;
            }
        }

    },
    showOrHideExcelProperties: function (check) {
        if (check) {
            $(importmapdata.SELECTORS.model_excel).addClass('active');

        } else {
            $(importmapdata.SELECTORS.model_excel).removeClass('active');
            importmapdata.clearExcelData();
        }
    },
    clearExcelData: function () {
        importmapdata.GLOBAL.listTitleExcel = [];
        importmapdata.GLOBAL.listDataExcel = [];
        $(importmapdata.SELECTORS.error_content_execl_input).addClass('hidden');
        $(importmapdata.SELECTORS.content_file_excel_upload).addClass('hidden');
    },
    CheckUploadGeojsonInputFormBeforeExitsAction: function () {
        var check = true;
        if ($(importmapdata.SELECTORS.geojsonWkt).val() != "" || importmapdata.GLOBAL.fileArray.length > 0) {
            check = false;
        }

        return check;
    },
    getRowColumnExcel: function (i,title) {
        let ColumnRow = importmapdata.GLOBAL.listColumnRow.find(x => x.title == title);
        if (typeof ColumnRow !== "undefined" && ColumnRow !== null) {
            let row = i + ColumnRow.row + 1;
            return ColumnRow.cloumn + row;
        }
        return "";
    },
}