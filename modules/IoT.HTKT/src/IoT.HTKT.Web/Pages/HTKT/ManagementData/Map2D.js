﻿var l = abp.localization.getResource('HTKT');
var informap2d = {
    GLOBAL: {
        IsValidGeometry: true
    },
    CONSTS: {
    },
    SELECTORS: {
        infor2D: ".step-2D #nav-2d-infor tbody",
        infor2Dtable: ".step-2D #nav-2d-infor table",
        infor2DTableWarning: ".step-2D #nav-2d-infor table .warning-infor",
        tabInfor2D: ".step-2D #nav-2d-infor",
        inforGeojson: ".step-2D #nav-2d-geojson .textarea-geojson",
        tableGeojson: ".step-2D #nav-2d-table tbody",
        btnSaveMap2D: ".step-2D .btn-save-map2d",
        btnResetMap2D: ".step-2D .btn-reset-map2d",
        btnDeleteMap2D: ".step-2D .btn-deleted-map2d",
        latLocation: ".input-location-table.lat-location",
        lngLocation: ".input-location-table.lng-location",
        li_tab_2d: ".step-2D .nav-2d-geojson .nav-link",
        content_button: ".step-2D .tool-modal"
    },
    init: function () {
        informap2d.setEvent();
    },
    setEvent: function () {
        //$(informap2d.SELECTORS.tabInfor2D).on("click", function () {
        //    informap2d.setInforData2D();
        //});
        $(informap2d.SELECTORS.inforGeojson).on("keyup", function () {
            if (importmapdata.checkJson($(this).val())) {
                informap2d.updateGeojsonMap2D();
                let geojson = JSON.parse($(this).val());
                informap2d.setTableData2DByData(geojson.geometry);
            }
        });
        $(informap2d.SELECTORS.btnSaveMap2D).on("click", function () {
            //var data = mapdata.convertDataToGeojson();
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:DoYouWantToSaveChangedObject"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    if (importmapdata.checkJson($(informap2d.SELECTORS.inforGeojson).val()) && informap2d.GLOBAL.IsValidGeometry && informap2d.CheckIsValidLatLng()) {
                        toastr.remove();
                        var feature = JSON.parse($(informap2d.SELECTORS.inforGeojson).val());
                        if (!importmapdata.CheckGeojsonPattern(feature.geometry)) {
                            if (mapdata.GLOBAL.mainObjectSelected !== null) {
                                let jsonObject = JSON.parse(JSON.stringify(mapdata.GLOBAL.mainObjectSelected));
                                jsonObject.geometry = feature.geometry;
                                jsonObject.propertiesGeojson = jsonObject.properties;
                                mapdata.UpdateGeometryObject(jsonObject);
                            }
                        } else abp.notify.warn(l("ManagementLayer:Warring:Geojson"));
                        
                    }
                    else {
                        abp.notify.error(l("ManagementLayer:UpdateDataFailed"));
                    }
                }
            });
        });
        $(informap2d.SELECTORS.btnResetMap2D).on("click", function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:DoYouRestoreTotheOriginalState"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    informap2d.setGeojsonData2D();
                    informap2d.setTableData2D();
                    informap2d.updateGeojsonMap2D();
                }
            });
        });
        $(informap2d.SELECTORS.btnDeleteMap2D).on("click", function () {
            swal({
                title: "",
                text: l("ManagementLayer:DoYouWantDeleteThisObject"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Cancel"),
                    l("ManagementLayer:Delete")
                ],
                dangerMode: false,
            }).then((willDelete) => {
                if (willDelete) {
                    let id = mapdata.GLOBAL.mainObjectSelected.id;
                    $.ajax({
                        type: "GET",
                        contentType: "application/json-patch+json",
                        async: false,
                        url: mapdata.CONSTS.URL_AJAXDELETEDDATA + '?id=' + id,
                        data: {
                        },
                        success: function (data) {
                            if (data.code == "ok") {
                                swal({
                                    title: l("ManagementLayer:Notification"),
                                    text: l("ManagementLayer:DeleteInformationSuccessfully"),
                                    icon: "success",
                                    button: l("ManagementLayer:Close"),
                                }).then((value) => {
                                    let object = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.mainObjectSelected.id);
                                    map.data.remove(object);
                                    mapdata.hideInfoData();
                                });
                            } else {
                                swal({
                                    title: l("ManagementLayer:Notification"),
                                    text: l("ManagementLayer:DeleteInformationFailed"),
                                    icon: "error",
                                    button: l("ManagementLayer:Close"),
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
                            console.error(jqXHR + ":" + errorThrown);
                        }
                    });
                }
            });
        });

        $(informap2d.SELECTORS.li_tab_2d).on("click", function () {
            var type = $(this).attr('data-type');
            switch (type) {
                case "info":
                    $(informap2d.SELECTORS.content_button).addClass('hidden');
                    break;
                default:
                    $(informap2d.SELECTORS.content_button).removeClass('hidden');
                    break;
            }
        });
    },
    setInfor2D: function () {
        informap2d.setInforData2D();
        informap2d.setGeojsonData2D();
        informap2d.setTableData2D();
    },
    setInforData2D: function () {
        let infor = mapdata.GLOBAL.mainObjectSelected != null ? mapdata.GLOBAL.mainObjectSelected.tags : null;
        $(informap2d.SELECTORS.infor2DTableWarning).remove();
        $(informap2d.SELECTORS.infor2D).children().remove();
        if (infor !== null && infor.length > 0) {
            let htmlinfor = "";
            for (const property in infor) {
                let val = infor[property] !== null ? infor[property] : "";
                htmlinfor += `<tr><td>${property}</td><td>${val}</td>`;
            }
            $(informap2d.SELECTORS.infor2D).append(htmlinfor);
        } else {
            $(informap2d.SELECTORS.infor2Dtable).append("<span class='warning-infor' style='color: #f44336;'>" + l('ManagementLayer:CurrentlyThisObjectHasNoInformation') + "</span>");
        }
    },
    setGeojsonData2D: function () {
        if (mapdata.GLOBAL.mainObjectSelected !== null) {
            let infor = mapdata.GLOBAL.mainObjectSelected.geometry;
            let properties = mapdata.GLOBAL.mainObjectSelected.properties;
            if (infor !== null) {
                let defaultProperties = {
                    "stroke": (properties != null && properties != undefined) ? properties.stroke : mapdata.GLOBAL.propertiesSetting.Stroke,
                    "stroke-width": (properties != null && properties != undefined) ? properties.strokeWidth : mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                    "stroke-opacity": (properties != null && properties != undefined) ? properties.strokeOpacity : mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                    "fill": (properties != null && properties != undefined) ? properties.fill : mapdata.GLOBAL.propertiesSetting.Fill,
                    "fill-opacity": (properties != null && properties != undefined) ? properties.fillOpacity : mapdata.GLOBAL.propertiesSetting.FillOpacity
                };
                let inforgeojson = {
                    "type": "Feature",
                    "properties": defaultProperties,
                    "geometry": infor
                };
                var str = JSON.stringify(inforgeojson, undefined, 4);
                $(informap2d.SELECTORS.inforGeojson).val(str);
            }
        }
    },
    setTableData2D: function () {
        let geojson = mapdata.GLOBAL.mainObjectSelected.geometry;
        if (geojson !== null) {
            let html = informap2d.getCoordinates(geojson);
            $(informap2d.SELECTORS.tableGeojson).children().remove();
            $(informap2d.SELECTORS.tableGeojson).append(html);
            informap2d.setEventTable();
        }
    },
    setTableData2DByData: function (geojson) {
        if (geojson !== null) {
            let html = informap2d.getCoordinates(geojson);
            $(informap2d.SELECTORS.tableGeojson).children().remove();
            $(informap2d.SELECTORS.tableGeojson).append(html);
            informap2d.setEventTable();
        }
    },
    getCoordinates: function (geojson) {
        var coordinatehtml = "";
        let coordinates = geojson.coordinates;
        var dem = 0;
        switch (geojson.type.toLowerCase()) {
            case "polygon":
                for (var i = 0; i < coordinates.length; i++) {
                    if (coordinates.length > 1) {
                        coordinatehtml += "<tr><th>polygon " + (i + 1) + "</th></tr>"
                    }
                    for (var j = 0; j < coordinates[i].length; j++) {
                        coordinatehtml += ` <tr>
                                                <th>${j + 1}</th>
                                                <td><input class="input-location-table lng-location" data-index="${j}" value="${coordinates[i][j][0]}"/></td>
                                                <td><input class="input-location-table lat-location" data-index="${j}" value="${coordinates[i][j][1]}"/></td>
                                            </tr>`;
                    }
                }
                break;
            case "multipolygon":
                for (var i = 0; i < coordinates.length; i++) {
                    for (var k = 0; k < coordinates[i].length; k++) {
                        for (var j = 0; j < coordinates[i][k].length; j++) {
                            coordinatehtml += ` <tr>
                                            <th>${dem + 1}</th>
                                            <td><input class="input-location-table lng-location" data-group="${i}" data-group-child="${k}" data-index="${j}" value="${coordinates[i][k][j][0]}"/></td>
                                            <td><input class="input-location-table lat-location" data-group="${i}" data-group-child="${k}" data-index="${j}" value="${coordinates[i][k][j][1]}"/></td>
                                        </tr>`;
                            dem++;
                        }
                    }
                }
                break;
            case "point":
                coordinatehtml += ` <tr>
                                        <th>1</th>
                                        <td><input class="input-location-table lng-location" data-index="0" value="${coordinates[0]}"/></td>
                                        <td><input class="input-location-table lat-location" data-index="0" value="${coordinates[1]}"/></td>
                                    </tr>`;
                break;
            case "multipoint":
            case "linestring":
            case "polyline":
                for (var j = 0; j < coordinates.length; j++) {
                    coordinatehtml += ` <tr>
                                            <th>${j + 1}</th>
                                            <td><input class="input-location-table lng-location" data-index="${j}" value="${coordinates[j][0]}"/></td>
                                            <td><input class="input-location-table lat-location" data-index="${j}" value="${coordinates[j][1]}"/></td>
                                        </tr>`;
                }
                break;
            case "multipolyline":
            case "multilinestring":
                for (var i = 0; i < coordinates.length; i++) {
                    for (var j = 0; j < coordinates[i].length; j++) {
                        coordinatehtml += ` <tr>
                                            <th>${dem + 1}</th>
                                            <td><input class="input-location-table lng-location" data-group="${i}" data-index="${j}" value="${coordinates[i][j][0]}"/></td>
                                            <td><input class="input-location-table lat-location" data-group="${i}" data-index="${j}" value="${coordinates[i][j][1]}"/></td>
                                        </tr>`;
                        dem++;
                    }
                }
                break;
        }
        return coordinatehtml;
    },
    updateGeojsonMap2D: function () {
        try {
            if (mapdata.GLOBAL.selectObject2D != null && mapdata.GLOBAL.selectObject2D != undefined) {
                map.data.remove(mapdata.GLOBAL.selectObject2D.feature);
            }

            var feature = JSON.parse($(informap2d.SELECTORS.inforGeojson).val());
            feature.id = mapdata.GLOBAL.mainObjectSelected.id;
            let geojson = JSON.stringify({
                "type": "FeatureCollection",
                "features": [feature]
            });

            var re = map.data.addGeoJson(geojson);
            mapdata.setChangeGeojsonAndHighlight(re);
            informap2d.GLOBAL.IsValidGeometry = true;
        }
        catch (err) {
            informap2d.GLOBAL.IsValidGeometry = false;
        }
        //mapdata.GLOBAL.selectObject2D.feature
    },
    reloadGeoJsonMap2d: function (id) {
        var indexObj = mapdata.GLOBAL.features.findIndex(x => x._id === id);
        if (indexObj != -1) {
            map.data.remove(mapdata.GLOBAL.features[indexObj]);
        }

        if (mapdata.GLOBAL.selectObject2D != null && mapdata.GLOBAL.selectObject2D != undefined) {
            var f = map.data.add(mapdata.GLOBAL.selectObject2D.feature);
            mapdata.GLOBAL.features[indexObj] = mapdata.GLOBAL.selectObject2D.feature;
        }
    },
    getGeojsonFromTableMap2D: function (lat, lng, index) {
        var feature = JSON.parse($(informap2d.SELECTORS.inforGeojson).val());
        switch (feature.geometry.type.toLowerCase()) {
            case "polygon":
                let count = 0, countstart = 0;
                for (var i = 0; i < feature.geometry.coordinates.length; i++) {
                    //if (coordinates.length > 1) {
                    //    coordinatehtml += "<tr><th>polygon " + (i + 1) + "</th></tr>"
                    //}
                    //for (var j = 0; j < coordinates[i].length; j++) {
                    //    coordinatehtml += ` <tr>
                    //                            <th>${j + 1}</th>
                    //                            <td><input class="input-location-table lng-location" data-index="${j}" value="${coordinates[i][j][0]}"/></td>
                    //                            <td><input class="input-location-table lat-location" data-index="${j}" value="${coordinates[i][j][1]}"/></td>
                    //                        </tr>`;
                    //}
                    count += feature.geometry.coordinates[i].length;
                    if (index < (count - 1) && index !== 0 && index !== (count - 1)) {
                        if (lat !== null) {
                            feature.geometry.coordinates[i][index - countstart][1] = Number(lat);
                        }
                        if (lng !== null) {
                            feature.geometry.coordinates[i][index - countstart][0] = Number(lng);
                        }
                    }
                    if (index === 0 || index === (count - 1)) {
                        if (lat !== null) {
                            feature.geometry.coordinates[i][0][1] = Number(lat);
                            feature.geometry.coordinates[i][count - 1][1] = Number(lat);
                        }
                        if (lng !== null) {
                            feature.geometry.coordinates[i][0][0] = Number(lng);
                            feature.geometry.coordinates[i][count - 1][0] = Number(lng);
                        }
                    }
                    countstart += feature.geometry.coordinates[i].length;
                }
                break;
            case "multipolygon":
                break;
            case "point":
                if (lat !== null) {
                    feature.geometry.coordinates[1] = Number(lat);
                }
                if (lng !== null) {
                    feature.geometry.coordinates[0] = Number(lng);
                }
                break;
            case "multipoint":
                break;
            case "linestring":
            case "polyline":
                if (lat !== null) {
                    feature.geometry.coordinates[index][1] = Number(lat);
                }
                if (lng !== null) {
                    feature.geometry.coordinates[index][0] = Number(lng);
                }
                break;
            case "multipolyline":
            case "multilinestring":
                break;
        }
        var str = JSON.stringify(feature, undefined, 4);
        $(informap2d.SELECTORS.inforGeojson).val(str);
        informap2d.updateGeojsonMap2D();
        if (feature.geometry.type.toLowerCase() === "polygon") {
            informap2d.setTableData2DByData(feature.geometry);
        }
    },
    getGeojsonFromTableMap2DNew: function (lat, lng, index, group, groupchild) {
        if (importmapdata.checkJson($(informap2d.SELECTORS.inforGeojson).val())) {
            var feature = JSON.parse($(informap2d.SELECTORS.inforGeojson).val());
            switch (feature.geometry.type.toLowerCase()) {
                case "polygon":
                    let count = 0, countstart = 0;
                    for (var i = 0; i < feature.geometry.coordinates.length; i++) {
                        //if (coordinates.length > 1) {
                        //    coordinatehtml += "<tr><th>polygon " + (i + 1) + "</th></tr>"
                        //}
                        //for (var j = 0; j < coordinates[i].length; j++) {
                        //    coordinatehtml += ` <tr>
                        //                            <th>${j + 1}</th>
                        //                            <td><input class="input-location-table lng-location" data-index="${j}" value="${coordinates[i][j][0]}"/></td>
                        //                            <td><input class="input-location-table lat-location" data-index="${j}" value="${coordinates[i][j][1]}"/></td>
                        //                        </tr>`;
                        //}
                        count += feature.geometry.coordinates[i].length;
                        if (index < (count - 1) && index !== 0 && index !== (count - 1)) {
                            if (lat !== null) {
                                feature.geometry.coordinates[i][index - countstart][1] = Number(lat);
                            }
                            if (lng !== null) {
                                feature.geometry.coordinates[i][index - countstart][0] = Number(lng);
                            }
                        }
                        if (index === 0 || index === (count - 1)) {
                            if (lat !== null) {
                                feature.geometry.coordinates[i][0][1] = Number(lat);
                                feature.geometry.coordinates[i][count - 1][1] = Number(lat);
                            }
                            if (lng !== null) {
                                feature.geometry.coordinates[i][0][0] = Number(lng);
                                feature.geometry.coordinates[i][count - 1][0] = Number(lng);
                            }
                        }
                        countstart += feature.geometry.coordinates[i].length;
                    }
                    break;
                case "multipolygon":
                    //let count = 0, countstart = 0, dem = 0, index2 = 0, total = 0, i = 0;
                    //for (var im = 0; im < feature.geometry.coordinates.length; im++) {
                    //    dem = im;
                    //    let total2 =  total + (feature.geometry.coordinates[im].length - 1);
                    //    if (index <= total2) {
                    //        break;
                    //    }
                    //}

                    //index2 = index;
                    //index = index - total;
                    //i = dem;
                    //    count += feature.geometry.coordinates[i].length;
                    //    if (index < (count - 1) && index !== 0 && index !== (count - 1)) {
                    //        if (lat !== null) {
                    //            feature.geometry.coordinates[i][index - countstart][1] = Number(lat);
                    //        }
                    //        if (lng !== null) {
                    //            feature.geometry.coordinates[i][index - countstart][0] = Number(lng);
                    //        }
                    //    }
                    //    if (index === 0 || index === (count - 1)) {
                    //        if (lat !== null) {
                    //            feature.geometry.coordinates[i][0][1] = Number(lat);
                    //            feature.geometry.coordinates[i][count - 1][1] = Number(lat);
                    //        }
                    //        if (lng !== null) {
                    //            feature.geometry.coordinates[i][0][0] = Number(lng);
                    //            feature.geometry.coordinates[i][count - 1][0] = Number(lng);
                    //        }
                    //    }
                    //    countstart += feature.geometry.coordinates[i].length;
                    let count2 = 0;
                    count2 += feature.geometry.coordinates[group][groupchild].length;
                    if (index < (count2 - 1) && index !== 0 && index !== (count2 - 1)) {
                        if (lat !== null) {
                            feature.geometry.coordinates[group][groupchild][index][1] = Number(lat);
                        }
                        if (lng !== null) {
                            feature.geometry.coordinates[group][groupchild][index][0] = Number(lng);
                        }
                    }
                    if (index === 0 || index === (count2 - 1)) {
                        if (lat !== null) {
                            feature.geometry.coordinates[group][groupchild][0][1] = Number(lat);
                            feature.geometry.coordinates[group][groupchild][count2 - 1][1] = Number(lat);
                        }
                        if (lng !== null) {
                            feature.geometry.coordinates[group][groupchild][0][0] = Number(lng);
                            feature.geometry.coordinates[group][groupchild][count2 - 1][0] = Number(lng);
                        }
                    }

                    break;
                case "point":
                    if (lat !== null) {
                        feature.geometry.coordinates[1] = Number(lat);
                    }
                    if (lng !== null) {
                        feature.geometry.coordinates[0] = Number(lng);
                    }
                    break;
                case "multipoint":
                    if (lat !== null) {
                        feature.geometry.coordinates[index][1] = Number(lat);
                    }
                    if (lng !== null) {
                        feature.geometry.coordinates[index][0] = Number(lng);
                    }
                    break;
                case "linestring":
                case "polyline":
                    if (lat !== null) {
                        feature.geometry.coordinates[index][1] = Number(lat);
                    }
                    if (lng !== null) {
                        feature.geometry.coordinates[index][0] = Number(lng);
                    }
                    break;
                case "multipolyline":
                case "multilinestring":
                    if (lat !== null) {
                        feature.geometry.coordinates[group][index][1] = Number(lat);
                    }
                    if (lng !== null) {
                        feature.geometry.coordinates[group][index][0] = Number(lng);
                    }
                    break;
            }
            var str = JSON.stringify(feature, undefined, 4).replace("null", "");
            $(informap2d.SELECTORS.inforGeojson).val(str);
            informap2d.updateGeojsonMap2D();
            //if (feature.geometry.type.toLowerCase() === "polygon" || feature.geometry.type.toLowerCase() === "multipolygon") {
            //    informap2d.setTableData2DByData(feature.geometry);
            //}
        }
    },
    setEventTable: function () {
        $(informap2d.SELECTORS.latLocation).on("keyup", function () {
            //let lat = $(this).val();
            //let index = $(this).data("index");
            //informap2d.getGeojsonFromTableMap2D(lat, null, index);
            let lat = $(this).val();
            var isNumber = parseFloat(lat);
            if (!isNaN(isNumber)) {
                let index = $(this).data("index");
                let group = $(this).data("group");
                let groupchild = $(this).data("group-child");
                informap2d.getGeojsonFromTableMap2DNew(isNumber, null, index, group == undefined ? 0 : group, groupchild == undefined ? 0 : groupchild);
            }
        });
        $(informap2d.SELECTORS.lngLocation).on("keyup", function () {
            //let lng = $(this).val();
            //let index = $(this).data("index");
            //informap2d.getGeojsonFromTableMap2D(null, lng, index);
            let lng = $(this).val();
            var isNumber = parseFloat(lng);
            if (!isNaN(isNumber)) {
                let index = $(this).data("index");
                let group = $(this).data("group");
                let groupchild = $(this).data("group-child");
                informap2d.getGeojsonFromTableMap2DNew(null, isNumber, index, group == undefined ? 0 : group, groupchild == undefined ? 0 : groupchild);
            }
        });
    },
    CheckIsValidLatLng: function () {
        var check = true;
        $(informap2d.SELECTORS.lngLocation).each(function () {
            if ($(this).val() == "") {
                check = false;
                return false;
            }
            else {
                var isNumber = parseFloat($(this).val());
                if (isNaN(isNumber)) {
                    check = false;
                    return false;
                }
            }
        });

        $(informap2d.SELECTORS.latLocation).each(function () {
            if ($(this).val() == "") {
                check = false;
                return false;
            }
            else {
                var isNumber = parseFloat($(this).val());
                if (isNaN(isNumber)) {
                    check = false;
                    return false;
                }
            }
        });

        return check;
    }
}