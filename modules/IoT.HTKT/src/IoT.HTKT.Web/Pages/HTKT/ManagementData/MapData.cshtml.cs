using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoT.HTKT.Layer;
using IoT.HTKT.LayerPermission;
using IoT.HTKT.ManagementData;
using IoT.HTKT.Permissions;
using IoT.PermissionApp.ManagerIdentity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Identity;
using Volo.Abp.Settings;

namespace IoT.HTKT.Web.Pages.HTKT.ManagementData
{
    [Authorize(HTKTPermissions.ManagementData.Default)]
    public class MapDataModel : AbpPageModel
    {
        //public ISettingProvider settingProvider { get; }
        //protected MapDataModel(ISettingProvider settingProvider)
        //{
        //    SettingProvider = settingProvider;
        //}
        //private IIdentityUserAppService _userAppService { get; }
        //private IIdentityRoleAppService _roleAppService { get; }
        private readonly ILayerUserPermissionHTKTService _layerUserPermissionService;
        private readonly IDirectoryService _directoryService;
        private readonly IManagerIdentityService _managerIdentityService;

        private readonly IMainObjectService _mainObjectService;
        private readonly IConfiguration _configuration;

        [BindProperty]
        public string UserId { get; set; }
        [BindProperty]
        public List<string> RoleId { get; set; } = new List<string>();
        [BindProperty]
        public List<PermissionByDirectory> ListLayer { get; set; } = new List<PermissionByDirectory>();
        [BindProperty]
        public Boolean IsAdmin { get; set; } = false;
        public double TotalCountById { get; set; }

        public double TotalMain { get; set; }

        public MapDataModel( ILayerUserPermissionHTKTService layerUserPermissionService, IManagerIdentityService managerIdentityService,
                        IMainObjectService mainObjectService, IConfiguration configuration, IDirectoryService directoryService)
        {
            //_userAppService = userAppService;
            _layerUserPermissionService = layerUserPermissionService;
            //_roleAppService = roleAppService;
            _mainObjectService = mainObjectService;
            _configuration = configuration;
            _directoryService = directoryService;
            _managerIdentityService = managerIdentityService;
        }

        public virtual async Task<IActionResult> OnGet(string id)
        {
            ViewData["id"] = id;
            UserId = CurrentUser.Id.ToString();
            var guild = Guid.Parse(UserId);
            //var rolelst = await _userAppService.GetRolesAsync(guild);
            var rolelst = await _managerIdentityService.GetRolesAsync(guild);
            RoleId = rolelst.Items.Select(x => x.Id.ToString()).ToList();
            foreach (var iz in RoleId)
            {
                //var check = await _roleAppService.GetAsync(Guid.Parse(iz));
                var check = await _managerIdentityService.GetByIdAsync(Guid.Parse(iz));
                if (check != null && check.IsStatic)
                {
                    IsAdmin = true;
                }
            }

            var lstpermission = await _layerUserPermissionService.GetListOfUserOrRole(UserId, RoleId);
            var checkpermission = lstpermission.Where(x => x.IdDirectory == id).ToList();
            if (checkpermission.Count > 0)
            {
                var obj = new PermissionByDirectory
                {
                    IdDirectory = id,
                    //ListPermisson = checkpermission.ListPermisson.Select(x => x.KeyName).ToList()
                    ListPermisson = new List<string>()
                };
                foreach (var z in checkpermission)
                {
                    obj.ListPermisson.AddRange(z.ListPermisson.Select(x => x.KeyName).ToList());
                }
                obj.ListPermisson = obj.ListPermisson.Distinct().ToList();
                ListLayer.Add(obj);
            }
            else
            {
                var obj = new PermissionByDirectory
                {
                    IdDirectory = id,
                    ListPermisson = new List<string>()
                };
                ListLayer.Add(obj);
            }
            TotalCountById = await _mainObjectService.GetCountByIdDriectory(id);
            TotalMain = TotalCountById;
            TotalCountById /= Convert.ToInt32(_configuration.GetSection("SettingApp:MaxRequest").Value);
            return await Task.FromResult<IActionResult>(Page());
        }
    }
}
