mn_selected = "mn_map_data";

var mapdata = {
    GLOBAL: {
        selectmenu: "",
        selectbtn: "",
        markerDraw: null,
        listMarker: [],
        listDistance: {
            listMarkerDistance: [],
            listPolylineDistance: [],
            listMerterDistance: [],
            path: [],
        }, // vẽ đoạn thẳng
        listArea: {
            listMarkerArea: [],
            listMerterArea: [],
            polygonArea: null,
            PolylineArea: null,
        }, // vẽ đa giác
        listRectangle: {
            listMarkerRectangle: [],
            listPolygonRectangle: [],
            rectangle: null,
        }, // vẽ hình chữ nhật
        listMultiPoint: {
            MultiPoint: [],
        }, // vẽ nhiều điểm
        lstPropertiesDirectory: [],
        listMultiLine: [], // vẽ nhiều đoạn thằng
        listMultiPolygon: [], // vẽ nhiều đa giác
        polylineTemp: null,
        markerMeter: null,
        listDistanceTem: [],
        propertiesSetting: {
            MinZoom: 0,
            MaxZoom: 22,
            Image2D: "",
            Stroke: "#8b5b5b",
            StrokeWidth: 2,
            StrokeOpacity: 1,
            StyleStroke: "",
            Fill: "#8b5b5b",
            FillOpacity: 0.3,
        },
        statusEdit: false,
        statusdone: false,
        //menuSelected: '',
        mainObjectSelected: null,
        idDirectory: indexdata.GLOBAL.idDirectory,
        nameDirectory: '',
        idMainObject: '',
        listEdit: {
            listMarker: [],
            listDistance: [],
            listArea: [],
            listRectangle: [],
        },
        features: null,
        typeGeometrye: '',
        listHighlight: [],
        selectObject2D: null,
        list: [],
        tabselected: "",
        listObjectImageEdit: [],
        listObjectLinkCameraEdit: [],
        listObjectFileEdit: [],
        listObject3D: [],
        statusChosenMap: false,
        // OverLayMap
        OverLay: null,
        OrderBaseMap: -1,
        listObjectMapInRadius: [],
        listObjectByDirectory: [],
        featuresGeojson: null,
        geometryExample: "{\"type\":\"Polygon\",\"coordinates\":[[[106.6248035430908,10.857362422239218],[106.62334442138672,10.855413111418091],[106.62478208541869,10.854538551619454],[106.62527561187744,10.85399063333299],[106.62586569786072,10.853242512511828],[106.62645578384398,10.852230965888225],[106.62711024284363,10.851177267848106],[106.62811875343323,10.850882231731207],[106.62907361984252,10.850955990787728],[106.63011431694031,10.851451229695892],[106.63107991218567,10.851893782919555],[106.63166999816893,10.852230965888225],[106.6317880153656,10.853063384713368],[106.63203477859496,10.854981100271235],[106.63208842277527,10.85564492201989],[106.63205623626709,10.856182300450465],[106.63175582885742,10.857541547460361],[106.63047909736633,10.857562621008723],[106.62874102592468,10.857552084234728],[106.62659525871275,10.85745725325203],[106.6248035430908,10.857362422239218]]]}",
        listHighlightPoint: [],
        lstGeoJsonDefault: [],
        ArrayTree: [],
        ///treeView/
        DirectoryForcus: "",
        ListPropertiesFocus: [],
        //data quan huyen
        District: [],
        WardDistrict: [],
        cordinatesOfPolygon: [],
    },
    CONSTS: {
        URL_AJAXGETDEFAULTDATA: '/api/HTKT/PropertiesDirectory/get-directoryById',
        URL_AJAXSAVE: '/api/HTKT/ManagementData/create-object',
        URL_AJAXUPDATESAVE: '/api/HTKT/ManagementData/update-object',
        URL_AJAXGETDATA: '/api/HTKT/ManagementData/get-object',
        URL_AJAXDELETEDDATA: '/api/HTKT/ManagementData/deleted-object',
        URL_AJAXDEDITDATA: '/api/HTKT/ManagementData/get-object-by-IdDirectory',
        URL_AJAXMENULEFT: "/api/HTKT/ManagementData/get-number-object-by-IdDirectory",
        URL_AJAX_ARRAY_LAYER_BASE_MAP: "/api/htkt/layerBaseMap/get-list",
        URL_AJAXGETOBJECTBYGEOMETRY: '/api/htkt/map4d/get-object-in-geometry',
        URL_UPDATE_PROPERTIES_MAIN_OBJECT: "/api/HTKT/ManagementData/update-properties-main-object", // api cập nhật thuộc tính thông tin cơ bản của đối tượng (ko cập nhật các trường link, file, image)
        URL_GETOBJECTPAGINGBYDIRECTORY: "/api/HTKT/ManagementData/get-object-paging-by-IdDirectory",
        URL_GETLIST_PARENT_DIRECTORY: "/api/htkt/directory/get-list-parent-directory-name", // get toàn bộ tên parent của layer
        GET_LOCATION_CENTER: "/api/htkt/location-setting/get-location-center",
        URL_GETLISTDIRECTORY: "/api/khaithac/khaiThacDirectory/get-list-layer-has-setting",
        ///treeView/
        URL_GET_PROPERTIES_DIRECTORY: "/api/app/khai-thac-properties-directory",
        //data quan huyen
        URL_GET_DATA_DISTRICT_AND_WARDDISTRICT: "/get-country-by-level-or-code",
        URL_GET_GEOJSON_DISTRICT_AND_WARDDISTRICT: "/get-geojson-country-by-code",
    },
    SELECTORS: {
        newcomondraw: "li .newcomondraw",
        endgeojson: ".endgeojson",
        deletegeojson: ".deletegeojson",
        modelWktGeojson: ".model-wkt-geojson",
        cancelgeojson: ".cancelgeojson",
        editgeojson: ".editgeojson",
        newgeojsonbutton: ".newgeojson",
        uploadMultiFile: ".upload-File-Multi-Object",
        //newgeojsonbutton: "#btn-new-action",
        divinputsearch: ".div_inputsearch",
        modalbody: "#modalBodyFolderLayer",
        inputExtendTitle: "#text-extend-title",
        inputExtendDescription: "#text-extend-description",
        btnSave: "#btn-save",
        hasError: ".form-group.has-error",
        inforData: "#InforData",
        editForm: "#EditForm",
        modelGeojson: ".model-geojson",
        btnDeleted: ".btn-deleted",
        btnExportfile: ".btn-exportfile",
        btnEdited: ".btn-edited",
        //modalinfo//
        minZoom: '#rangeMin',
        maxZoom: '#rangeMax',
        strokeColor: '#favcolorText',
        strokeWidth: '#text-width',
        strokeOpacity: '#rangeMax-stress',
        fillColor: '#favcolorText-color',
        fillOpacity: '#rangeMax-color',
        paraSelected: "#para-selected",
        selectParadigmId: '#selectParadigm',
        btnUploadDocument: "#btn-upload-document",
        inputUploadDocument: "#input-upload-document",
        btnSaveProperties: ".btn-save-properties",
        //import data:
        cancelImport: ".model-geojson .btn-cancel",
        //layer
        modalLayerProperties: ".model-properties-setting .modal-layer",

        //layerbasemap
        content_tile_map: ".change-tile-map",
        //new import
        modelGeojsonPropertiesMap: ".model-geojson-properties-map",
        modelGeojsonPropertiesNew: ".model-geojson-properties-new",
        modelGeojsonTile: ".model-geojson .modal-title",
        modelGeojsonTileString: ".model-geojson .tile-string",
        newcomondrawmodal: '.newcomondrawmodal',
        modalButtonCreate: '#model-properties-buttonCreate',
        btnNewAction: "#btn-new-action",
        modalCreateNew: "#modalCreateNew",
        upload_button: "#upload-multi",
        btn_save_step_info: "#btn-save-step-info",
        input_step_info: "#InforData .step-info-content #text-modal-",
        input_checkbox_radio_step_info: "#InforData .step-info-content .text-modal-",
        input_layer: ".modal-layer",
        modalUploadMulti: "#modalUploadMulti",
        menuMap: ".menuMap",
        close_draw: ".close-draw",
        extend_tab: "#InforData .step-extend li[class='nav-item col-md-4']",
        menu_left_sidebar_info: ".li-modal-data-right",
        //treeView//
        titleItem: ".title-item",
        closeTreeView: ".toggle-lst-Objec-property2",
        leftTree: ".left-tree2",
        buttonClose: ".button-close",
        selectDistrict: "#text-modal-District",
        selectWardDistrict: "#text-modal-WardDistrict"
    },
    init: function () {
        var url = new URL(window.location.href);
        mapdata.GLOBAL.idDirectory = url.searchParams.get("id");
        //mapdata.GLOBAL.nameDirectory = url.searchParams.get("name");
        mapdata.setDefaultColor();
        mapdata.setEvent();
        mapdata.eventClickMap();
        mapdata.getEditObject();
        /*        mapdata.modalPropertiesCreateObject();*/
        mapdata.setListObjectModel();
        mapdata.getListObjectInGeometry();
        modalCreateFolderLayer.init();
        mapdata.GetListParentDirectory();
        mapdata.LocationMapDefault(mapdata.GLOBAL.idDirectory);
        mapdata.GetLayerBaseMap();
        Activity.SelectdDanhSachLoaiHoatDong();

        //mapdata.setDefaultLayerBaseMap();
        mapdata.modalInfo();
        mapdata.GetListLayer();
        mapdata.eventTreeView();
        mapdata.GetDataOfDistrictAndWardDistrict("3");
    },
    setEvent: function () {
        $("#hideTree").click(function () {
            let svgtext = '';
            if (mapdata.GLOBAL.selectbtn !== "") {
                return;
            }
            if ($(this).hasClass("disabled")) {
                return;
            }
            $(".div-content").toggleClass("active");
            $(".div-content").toggleClass("active-rotate");

            if ($('.div-content').hasClass('active')) {
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <path class="a" style="fill: none;" d="M24,0H0V24H24Z" transform="translate(6)" />
                                <path class="b" style="fill: var(--primary)" d="M26.172,11H4v2H26.172l-5.364,5.364,1.414,1.414L30,12,22.222,4.222,20.808,5.636Z" transform="translate(-4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 18)" />
                            </svg>`;
                $(this).attr("data-original-title", l("ManagementLayer:OpenPanel"));
            }
            else {
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <g transform="translate(1988 32)">
                                    <g transform="translate(-1988 -32)">
                                        <path style="fill: none;" class="a" d="M0,0H24V24H0Z"></path>
                                        <path style="fill: var(--primary)" class="b" d="M7.828,11H30v2H7.828l5.364,5.364-1.414,1.414L4,12l7.778-7.778,1.414,1.414Z"></path>
                                    </g>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -28)"></rect>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -14)"></rect>
                                </g>
                            </svg>`;
                $(this).attr("data-original-title", l("ManagementLayer:ClosePanel"));
            }

            $(this).html(svgtext);
        });

        $(mapdata.SELECTORS.menuMap).on('click', '.focus', function () {
            //mapdata.CancelAction();
            var type = $(this).attr('data-type');
            if (type != "4") {
                $("#hideTree").html(`<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <g transform="translate(1988 32)">
                                    <g transform="translate(-1988 -32)">
                                        <path style="fill: none;" class="a" d="M0,0H24V24H0Z"></path>
                                        <path style="fill: var(--primary)" class="b" d="M7.828,11H30v2H7.828l5.364,5.364-1.414,1.414L4,12l7.778-7.778,1.414,1.414Z"></path>
                                    </g>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -28)"></rect>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -14)"></rect>
                                </g>
                            </svg>`);
            }

            switch (type) {
                case "1":
                    $(this).html(`<svg id="Group_2277" data-name="Group 2277" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_2628" data-name="Path 2628" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_2629" data-name="Path 2629" d="M16,2l5,5V21.008a.993.993,0,0,1-.993.992H3.993A1,1,0,0,1,3,21.008V2.992A.993.993,0,0,1,3.993,2Zm-5,9H8v2h3v3h2V13h3V11H13V8H11Z" fill="var(--primary)"/>
                                    </svg>`);
                    break;
                case "2":
                    $(this).html(`<svg id="Component_181_1" data-name="Component 181 – 1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_6372" data-name="Path 6372" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_6373" data-name="Path 6373" d="M16.757,3,9.291,10.466,9.3,14.713l4.238-.007L21,7.243V20a1,1,0,0,1-1,1H4a1,1,0,0,1-1-1V4A1,1,0,0,1,4,3Zm3.728-.9L21.9,3.516l-9.192,9.192-1.412,0,0-1.417Z" fill="var(--primary)"/>
                                    </svg>`);
                    break;
                case "3":
                    $(this).html(`<svg id="Group_2627" data-name="Group 2627" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_6398" data-name="Path 6398" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_6399" data-name="Path 6399" d="M20,7V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H2V5H22V7Zm-9,3v7h2V10ZM7,2H17V4H7Z" fill="var(--primary)"/>
                                    </svg>`);
                    break;
                case "5":
                    $(this).html(`<svg id="Group_2169" data-name="Group 2169" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_2641" data-name="Path 2641" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_2642" data-name="Path 2642" d="M16,2l5,5V21.008a.993.993,0,0,1-.993.992H3.993A1,1,0,0,1,3,21.008V2.992A.993.993,0,0,1,3.993,2ZM13,12h3L12,8,8,12h3v4h2Z" fill="var(--primary)"/>
                                    </svg>`);
                    break;
                default:
            }

            $('.focus').removeClass('isForcus');
            $(this).addClass('isForcus');

            $('.focus').prop('disabled', true);
            $(this).prop('disabled', false);

            $('.focus').addClass('disabled');
            $('.notFocus').addClass('disabled');
            $(this).removeClass('disabled');
            $(mapdata.SELECTORS.inforData).find('.toggle-detail-property2').trigger('click');// đóng tab xem thông tin
            $('[data-toggle="tooltip"]').tooltip("hide");
        });

        $(mapdata.SELECTORS.close_draw).on("click", function () {
            model3D.cancelMode3D();
            mapdata.CancelAction();
        });

        $(mapdata.SELECTORS.newgeojsonbutton).on('click', function () {
            if (mapdata.GLOBAL.selectbtn !== "") {
                return;
            }
            //$(mapdata.SELECTORS.modalButtonCreate).modal('show');
            mapdata.GLOBAL.selectbtn = "insert";
            if ($(mapdata.SELECTORS.modalCreateNew).hasClass("activeView")) {
                //$(mapdata.SELECTORS.modalCreateNew).removeClass("activeView");
                //$(this).removeClass("active");
                //$(".div-content").removeClass("active");
            } else {
                $(".left-tree").css("opacity", "0");
                $(mapdata.SELECTORS.modalCreateNew).addClass("activeView");
                $(this).addClass("active");
                $(".div-content").addClass("active");
            }
        });

        $(mapdata.SELECTORS.uploadMultiFile).on('click', function () {
            if (mapdata.GLOBAL.selectbtn !== "") {
                return;
            } else {
                mapdata.GLOBAL.selectbtn = "upload";
            }

            UploadFileMultiObject.setEmptyInput();

            if ($(mapdata.SELECTORS.modalCreateNew).hasClass("activeView")) {
                $(mapdata.SELECTORS.modalCreateNew).removeClass("activeView");
                $(this).removeClass("active");
                $(".div-content").removeClass("active");

                $(mapdata.SELECTORS.modalUploadMulti).addClass("activeView")
                $(this).addClass("active");
                $(".div-content").addClass("active");


            }
            else if ($(mapdata.SELECTORS.modalUploadMulti).hasClass("activeView")) {

            }
            else {
                $(UploadFileMultiObject.SELECTORS.inputListDocument).html('');
                var $optionSelectFile = '';
                $.each(modalCreateFolderLayer.GLOBAL.SelectedFilesFolder, function (iFile, FileObject) {
                    $optionSelectFile += `<option value="${FileObject.codeProperties}">${FileObject.nameProperties}</option>`;
                });
                $(UploadFileMultiObject.SELECTORS.inputListDocument).html($optionSelectFile);

                $(".left-tree").css("opacity", "0");
                $(mapdata.SELECTORS.modalUploadMulti).addClass("activeView")
                $(this).addClass("active");
                $(".div-content").addClass("active");
            }
        });

        $(mapdata.SELECTORS.newcomondraw).on("click", function () {
            mapdata.clearAllDraw();
            mapdata.GLOBAL.selectmenu = $(this).attr("data-code");
            mapdata.GLOBAL.typeGeometrye = $(this).attr("data-code");
            mapdata.setOrRestBtnInsert(true);
            mapdata.hideInforProperties();
            mapdata.clearHighlight();
            mapdata.GLOBAL.listObjectImageEdit = [];
            mapdata.GLOBAL.listObjectLinkCameraEdit = [];
            modalCreateFolderLayer.GLOBAL.listObjectImage = [];
            modalCreateFolderLayer.GLOBAL.listObjectLinkCamera = [];
            modalCreateFolderLayer.GLOBAL.listImageInput = [];
            modalCreateFolderLayer.GLOBALfilecount = 0;
            modalCreateFolderLayer.GLOBALlistIdImageTemporary = [];
        });
        $(mapdata.SELECTORS.newcomondrawmodal).on("click", function () {
            mapdata.clearAllDraw();
            mapdata.GLOBAL.selectmenu = $(this).attr("data-code");
            mapdata.GLOBAL.typeGeometrye = $(this).attr("data-code");

            // clear data thêm mới bằng hình họctrên bản đồ
            mapdata.setOrRestBtnInsert(true);
            mapdata.hideInforProperties();
            mapdata.clearHighlight();
            mapdata.GLOBAL.listObjectImageEdit = [];
            mapdata.GLOBAL.listObjectLinkCameraEdit = [];
            modalCreateFolderLayer.GLOBAL.listObjectImage = [];
            modalCreateFolderLayer.GLOBAL.listObjectLinkCamera = [];
            modalCreateFolderLayer.GLOBAL.listImageInput = [];
            modalCreateFolderLayer.GLOBALfilecount = 0;
            modalCreateFolderLayer.GLOBALlistIdImageTemporary = [];
            //---------------------------------------------------------------
            // clear data thêm mới bằng geojson
            $(importmapdata.SELECTORS.error_content_fileGeojson_input).addClass('hidden');
            $(importmapdata.SELECTORS.content_file_geojson_upload).addClass('hidden');
            $(importmapdata.SELECTORS.fileGeojson).val(null).trigger("change");
            $(importmapdata.SELECTORS.btnConvertGeojson).prop('disabled', true);
            $(importmapdata.SELECTORS.geojsonWkt).val("");
            $(importmapdata.SELECTORS.content_geojson).removeClass('active')
            //-----------------------------------------------------------

            $(mapdata.SELECTORS.modalButtonCreate).modal('hide');
            $(this).addClass('button-active');
            $("#hideTree").addClass('disabled');

            $(mapdata.SELECTORS.newcomondrawmodal).attr('disabled', 'disabled');

            $(mapdata.SELECTORS.newgeojsonbutton).addClass('disabled');
            $('.detail-property-new').removeClass('active');
            switch (mapdata.GLOBAL.selectmenu) {
                case "newinput":
                    $(importmapdata.SELECTORS.content_geojson).addClass('active');
                    $(mapdata.SELECTORS.modelGeojsonTile).text(l('InputInformationObject'));
                    //$(mapdata.SELECTORS.modelGeojsonTileString).text(l('ManagementLayer:WKT/GeojsonString'));
                    break;
                case "newinputpropertiesmap":
                    $(importmapdata.SELECTORS.content_geojson).addClass('active');
                    $(mapdata.SELECTORS.modelGeojsonTile).text(l('InputInformationObject'));
                    //$(mapdata.SELECTORS.modelGeojsonTileString).text(l('ManagementLayer:GeojsonDataString'));
                    break;
                case "newinputpropertiesnew":
                    $(importmapdata.SELECTORS.content_geojson).addClass('active');
                    $(mapdata.SELECTORS.modelGeojsonTile).text(l('InputInformationObject'));
                    //$(mapdata.SELECTORS.modelGeojsonTileString).text(l('ManagementLayer:GeojsonDataString'));
                    break;
                case "import-excel":
                    $(importmapdata.SELECTORS.model_excel).addClass('active');
                    break;
                default:
                    $('#myModal2').addClass('active');
                    $(mapdata.SELECTORS.endgeojson).trigger('click');
                    break;
            }
        });

        $(mapdata.SELECTORS.endgeojson).on("click", function () {
            parentModel = "#myModal2";
            if (mapdata.GLOBAL.selectbtn === "edit") {
                swal({
                    title: l('ManagementLayer:Notification'),
                    text: l('ManagementLayer:AreYouSaveObject'),
                    icon: "warning",
                    buttons: [
                        l('ManagementLayer:Close'),
                        l('ManagementLayer:Agree')
                    ],
                }).then((value) => {
                    if (value) {
                        var data = mapdata.convertDataToGeojson();
                        if (mapdata.GLOBAL.mainObjectSelected !== null) {
                            let jsonObject = JSON.parse(JSON.stringify(mapdata.GLOBAL.mainObjectSelected));
                            jsonObject.geometry = data.features[0].geometry;
                            jsonObject.propertiesGeojson = jsonObject.properties;
                            mapdata.UpdateGeometryObject(jsonObject);
                        }
                        mapdata.GLOBAL.statusdone = true;
                        mapdata.setOrRestBtnEidt(false);
                        mapdata.GLOBAL.selectmenu = "";
                    }
                });
            } else {
                modalCreateFolderLayer.GLOBAL.listObjectFile = [];
                // reset properties
                mapdata.GLOBAL.lstPropertiesDirectory = [];
                mapdata.getPropertiesDirectory(); // show properties input
                modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = true;
                $('#myModal2').addClass('active');
            }
        });
        $(mapdata.SELECTORS.deletegeojson).on("click", function () {
            if (mapdata.GLOBAL.selectbtn !== "") {
                return;
            }
            if ($(this).hasClass("disabled")) {
                return;
            }
            $(".div-content").removeClass("active active-rotate");
            //if (mapdata.GLOBAL.idMainObject !== null && mapdata.GLOBAL.idMainObject.trim() !== "" && mapdata.GLOBAL.idMainObject.trim().length > 0) {
            //    mapdata.clearAllDraw();
            //    mapdata.GLOBAL.statusdone = false;
            //} else {
            //    swal({
            //        title: l("ManagementLayer:Notification"),
            //        text: l("ManagementLayer:PleaseSelectObjectToDelete"),
            //        icon: "warning",
            //        button: l("ManagementLayer:Close"),
            //    });
            //}
            mapdata.GLOBAL.selectbtn = "delete";
        });
        $(mapdata.SELECTORS.cancelgeojson).on("click", function () {
            if (mapdata.GLOBAL.selectbtn === "insert") {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("DoYouWantToAbortAddObject"),
                    icon: "warning",
                    buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                    dangerMode: false,
                }).then((val) => {
                    if (val) {
                        mapdata.setOrRestBtnInsert(false);
                        mapdata.clearAllDraw();
                        modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                        mapdata.GLOBAL.selectmenu = "";
                        $(modalCreateFolderLayer.SELECTORS.modal).removeClass('active');
                        $(mapdata.SELECTORS.newcomondrawmodal).removeClass('button-active');
                        $(mapdata.SELECTORS.newcomondrawmodal).attr('disabled', false);
                        //$("#hideTree").removeClass('disabled');
                        $(mapdata.SELECTORS.newgeojsonbutton).removeClass('disabled');
                    }
                });
            } else if (mapdata.GLOBAL.selectbtn === "edit") {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:DoYouCancel-ObjectGoBackOriginal"),
                    icon: "warning",
                    buttons: [l("ManagementLayer:Close"), l("ManagementLayer:Agree")],
                    dangerMode: false,
                }).then((val) => {
                    if (val) {
                        mapdata.setOrRestBtnEidt(false);
                        map.data.add(mapdata.GLOBAL.selectObject2D.feature);
                        mapdata.clearSelectDrawHighlight();
                        modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                    }
                });
            }
            mapdata.GLOBAL.statusdone = false;
        });
        $(mapdata.SELECTORS.editgeojson).on("click", function () {
            if (mapdata.GLOBAL.selectbtn !== "") {
                return;
            }
            if ($(this).hasClass("disabled")) {
                return;
            }
            $(".div-content").removeClass("active active-rotate");
            //if (mapdata.GLOBAL.idMainObject !== null && mapdata.GLOBAL.idMainObject.trim() !== "" && mapdata.GLOBAL.idMainObject.trim().length > 0) {
            //    mapdata.setOrRestBtnEidt(true);
            //    if (mapdata.GLOBAL.selectObject2D !== null) {
            //        mapdata.convertGeojsonToData(mapdata.GLOBAL.selectObject2D.feature);
            //    } else {
            //        var feature = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.mainObjectSelected.id);
            //        mapdata.convertGeojsonToData(feature);
            //    }
            //    mapdata.hideInforProperties();
            //} else {
            //    swal({
            //        title: l("ManagementLayer:Notification"),
            //        text: l("ManagementLayer:PleaseSelectObjectToEdit"),
            //        icon: "warning",
            //        button: l("ManagementLayer:Close"),
            //    });


            //}
            mapdata.GLOBAL.selectbtn = "edit";
        });
        $(mapdata.SELECTORS.btnExportfile).on("click", function () {
            setTimeout(function () {
                var demo = 'id=' + mapdata.GLOBAL.idMainObject;
                var a = '/api/HTKT/ManagementData/export-object?' + demo;
                window.location.href = a;
            }, 100);
        });
        $(mapdata.SELECTORS.btnSave).on("click", function () {
            //if (mapdata.checkformmodal() && mapdata.GLOBAL.idDirectory.trim() != "")
            toastr.remove();
            if (Info_Properties_Main_Object.checkFormInput('#myModal2 .detail-property-body') && mapdata.GLOBAL.idDirectory.trim() != "" && Info_Properties_Main_Object.checkValidFile()) {
                var object = {};
                object.id = mapdata.GLOBAL.idMainObject;
                object.idDirectory = mapdata.GLOBAL.idDirectory;
                object.nameObject = "Object Demo";
                object.listProperties = [];
                object.geometry = {};
                object.object3D = null;
                object.District = "";
                object.WardDistrict = "";
                if (mapdata.GLOBAL.typeGeometrye != "newPolygonByCountry") {
                    data = mapdata.convertDataToGeojson();
                }
                else {
                    data = mapdata.RenderJsonIntoFeature();
                }
                if (data !== null && data.features !== null && data.features.length > 0) {
                    object.geometry = data.features[0].geometry;
                } else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementData:warningDrawObject"),
                        icon: "warning",
                        button: l("ManagementLayer:Close"),
                    });
                    return false;
                }
                var getButtonIsActive = $(".button-active").attr("data-code");
                // hiển thị thông tin đối tượng
                $.each(mapdata.GLOBAL.lstProperties, function (i, obj) {
                    var objectparam = {
                        nameProperties: obj.NameProperties,
                        codeProperties: obj.CodeProperties,
                        typeProperties: obj.TypeProperties,
                        isShow: obj.IsShow,
                        isView: obj.IsView,
                        defalutValue: $('#myModal2 #text-modal-' + obj.CodeProperties).val(),
                        orderProperties: obj.OrderProperties,
                        typeSystem: obj.TypeSystem
                    };
                    if (obj.TypeProperties == "link") {

                        objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectLinkCamera);
                    }
                    if (obj.TypeProperties == "image") {
                        objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectImage);
                    }

                    if (obj.TypeProperties == "file") {
                        objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectImage);
                    }
                    object.listProperties.push(objectparam);
                    if (obj.CodeProperties.toLowerCase() === "name") {
                        object.nameObject = $('#myModal2 #text-modal-' + obj.CodeProperties).val();
                    }
                    if (obj.TypeProperties == "list") {
                        let array = null;
                        objectparam.defalutValue = $('#myModal2 #text-modal-' + obj.CodeProperties).val();
                    }

                    if (obj.TypeProperties == "checkbox") {
                        let arraychecked = $("#myModal2 .text-modal-" + obj.CodeProperties + ":checked").map(function () {
                            return this.value;
                        }).get();

                        objectparam.defalutValue = arraychecked.toString();
                    }
                    if (obj.TypeProperties == "radiobutton") {
                        let stringchecked = $("#myModal2 .text-modal-" + obj.CodeProperties + ":checked").val();
                        objectparam.defalutValue = stringchecked;
                    }

                    if (obj.TypeProperties == "bool") {
                        //obj.DefalutValue = $("#text-modal-" + obj.CodeProperties).prop("checked");
                        objectparam.defalutValue = $("#myModal2 #text-modal-" + obj.CodeProperties + "myModal2").prop("checked").toString();
                    }
                });


                let urlAjax = '';
                if (object.id == "") {
                    urlAjax = mapdata.CONSTS.URL_AJAXSAVE;
                } else {
                    urlAjax = mapdata.CONSTS.URL_AJAXUPDATESAVE;
                    object.geometry = mapdata.GLOBAL.mainObjectSelected.geometry;
                }
                object.propertiesGeojson = Config_Main_Object.getValueProperties();
                if (getButtonIsActive == "newPolygonByCountry") {
                    object.District = ($('#myModal2 #text-modal-District').val());
                    object.WardDistrict = ($("#myModal2 #text-modal-WardDistrict").val());
                }
                else if (getButtonIsActive == "newMultipolygonByCountry") {
                    object.District = $('#myModal2 #text-modal-District').val().toString();
                    object.WardDistrict = $('#myModal2 #text-modal-WardDistrict').val().toString();
                }
                object.planToDo = $('#myModal2 #text-modal-PlanToDo').val();

                //console.log(object);
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json-patch+json",
                    async: false,
                    url: urlAjax,
                    data: JSON.stringify(object),
                    success: function (data) {
                        if (data.code == "ok") {
                            var message = l("ManagementLayer:UpdateDataSuccesfully")
                            if (object.id == "") {
                                message = l("ManagementLayer:CreateDataSuccesfully")
                            }
                            abp.notify.success(message);

                            mapdata.ResetInsertMainOjbect();

                            let array = [];
                            $.each(modalCreateFolderLayer.GLOBAL.listObjectFile, function (i, obj) {
                                let item = {
                                    id: obj.id,
                                    idMainObject: data.result.id,
                                    nameDocument: obj.name,
                                    iconDocument: '',
                                    urlDocument: obj.url,
                                    nameFolder: obj.idDirectoryProperties,
                                    orderDocument: obj.order
                                };
                                array.push(item);
                            });
                            if (object.id === "" || object.id === null) {
                                let geojson = mapdata.getDataGeojsonByProperties([data.result]);
                                mapdata.showDataGeojson(JSON.stringify(geojson));
                            }
                            map.enable3dMode(false);
                            //mapdata.hideInfoData();
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                //contentType: false,
                                //processData: false,
                                contentType: "application/json-patch+json",
                                async: false,
                                url: '/api/HTKT/DocumentObject/add-file-document',
                                data: JSON.stringify(array),
                                success: function (data) {
                                    //if (data.code == "ok") {
                                    //    //console.log('ok');
                                    //} else {
                                    //    console.log('error');
                                    //}
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.error(jqXHR + ":" + errorThrown);
                                    ViewMap.showLoading(false);
                                }
                            });
                            ;
                        }
                        else {
                            var message = l("ManagementLayer:UpdateDataFailed")
                            if (object.id == "") {
                                message = l("ManagementLayer:CreateDataFailed")
                            }
                            swal({
                                title: l("ManagementLayer:Notification"),
                                text: l("ManagementLayer:UpdateDataFailed"),
                                icon: "warning",
                                button: l("ManagementLayer:Close"),
                            }).then((value) => {
                                //location.href = "/HTKT/Layer";
                            });
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR + ":" + errorThrown);
                        ViewMap.showLoading(false);
                    }
                });
                // cập nhật lại số lượng object
                totalMain = parseInt(totalMain) + 1;
                totalCountByIdDirectory = parseFloat(totalMain / parseInt(maxValueRequest));
                $('.totalMainObject').html(`(${totalMain})`);

                mapdata.GLOBAL.statusdone = true;
                mapdata.setOrRestBtnInsert(false);
                //$('#myModal2').modal("hide");
                $('#myModal2').removeClass('active');
                mapdata.GLOBAL.selectmenu = "";
                mapdata.GLOBAL.selectbtn = "insert";
                //mapdata.clearDataGeojson();
                mapdata.getEditObject(mapdata.GLOBAL.idDirectory);
                menuLeft.resizeMenuLeft();
                //menuLeft.setDataLayer();

            }
        });
        $(mapdata.SELECTORS.btnDeleted).on("click", function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:DoYouWantDeleteThisObject"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Cancel"),
                    l("ManagementLayer:Remove")
                ],
                dangerMode: false,
            }).then((willDelete) => {
                if (willDelete) {
                    let id = mapdata.GLOBAL.idMainObject;
                    $.ajax({
                        type: "GET",
                        contentType: "application/json-patch+json",
                        async: false,
                        url: mapdata.CONSTS.URL_AJAXDELETEDDATA + '?id=' + id,
                        data: {
                        },
                        success: function (data) {
                            if (data.code == "ok") {
                                swal({
                                    title: "Thông báo!",
                                    text: "Xóa thông tin thành công!",
                                    icon: "success",
                                    button: "Đóng!",
                                }).then((value) => {
                                    mapdata.updateMainObject();
                                    //$(mapdata.SELECTORS.inforData).find('.toggle-detail-property').trigger('click');
                                    mapdata.hideInfoData();
                                    menuLeft.resizeMenuLeft();
                                });

                            } else {
                                swal({
                                    title: l("ManagementLayer:Notification"),
                                    text: l("ManagementLayer:DeleteInformationFailed"),
                                    icon: "error",
                                    button: l("ManagementLayer:Close"),
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR + ":" + errorThrown);
                        }
                    });
                }
            });
        });
        $(mapdata.SELECTORS.cancelImport).on("click", function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:DoYouWantToCancel"),
                icon: "warning",
                buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                dangerMode: false,
            }).then((val) => {
                if (val) {
                    $(mapdata.SELECTORS.modelGeojson).modal('hide');
                    mapdata.setOrRestBtnInsert(false);
                    mapdata.clearAllDraw();
                }
            });
            //$(mapdata.SELECTORS.cancelgeojson).trigger("click");
        });
        /*--End event application--*/

        $(document).on("keyup change", mapdata.SELECTORS.input_layer, function () {
            if ($(this).val() != "") {
                $(this).parents(".form-group-modal").find(".label-error").remove();
                $(this).parents(".form-group-modal").removeClass("has-error");
            }
        });

        $(document).on("keyup change", mapdata.SELECTORS.input_layer + " input[type='checkbox'], input[type='radio']", function () {

            $(this).parents(".form-group-modal").find(".label-error").remove();
            $(this).parents(".form-group-modal").removeClass("has-error");

        });

        $(mapdata.SELECTORS.inforData).find('.toggle-detail-property2').on('click', function () {

            $('#InforData').removeClass('active');
            if (!$(mapdata.SELECTORS.inforData).hasClass('detail-property-collapse')) {
                $(mapdata.SELECTORS.inforData).toggleClass('detail-property-collapse');
            }
            let check = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
            if (check !== null && typeof check !== "undefined" && check.object !== null) {
                check.object.setSelected(false);
            }
            mapdata.GLOBAL.idMainObject = '';
            mapdata.GLOBAL.mainObjectSelected = null;
            model3D.hideModel3D();
            model3D.resetAll();
            //$('.li-modal-data-right[data-li="step-info"]').first().trigger('click');
            $('#InforData .li-modal-data-right').removeClass("active");
            $('#InforData .li-modal-data-right[data-li="step-info"]').addClass("active");
            $('.section-info').css('display', 'none');
            $('.step-info').css('display', 'block');
            mapdata.clearSelectDrawHighlight();
            mapdata.showAllObject3D();
            $(menuLeft.SELECTORS.liLeft).removeClass("menu-open");
            documentObject.GLOBAL.lstDocument = [];
            mapdata.clearHighlight();
            mapdata.clearHighlightPoint();
            map.setSelectedBuildings([]);
            mapdata.clearHighlightPoint();
            //model3D.clearMode3D();
        });


        $(mapdata.SELECTORS.btnEdited).on('click', function () {
            mapdata.getPropertiesDirectory();
            let lstproperties = mapdata.GLOBAL.mainObjectSelected.listProperties;
            // hiển thị value input đói tượng
            $.each(lstproperties, function (i, obj) {
                $('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                let lstvalue;
                if (obj.typeProperties == "list") {
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);
                        if (lstvalue != undefined && $.isArray(lstvalue)) {
                            let value = lstvalue.find(x => x.checked);
                            if (value != null && value != undefined) {
                                $('#text-modal-' + obj.codeProperties).val(value.code);
                            }
                        }
                        else {
                            $('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                        }
                    }
                    catch (e) {
                        $('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                    }
                }
                if (obj.typeProperties == "radiobutton") {
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);
                        if (lstvalue != undefined && $.isArray(lstvalue)) {
                            let value = lstvalue.find(x => x.checked);
                            if (value != null && value != undefined) {
                                $('.text-modal-' + obj.codeProperties + '[value = "' + value.code + '"]').prop("checked", true);
                            }
                        }
                        else {
                            $('.text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                        }
                    }
                    catch (e) {
                        $('.text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                    }
                }

                if (obj.typeProperties == "checkbox") {
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);

                        if (lstvalue.length > 0 && $.isArray(lstvalue)) {
                            $.each(lstvalue, function (i, item) {
                                if (item.checked) {
                                    $('.text-modal-' + obj.codeProperties + '[value = "' + item.code + '"]').prop("checked", true);
                                }
                            });
                        }
                        else {
                            var lstChecked = obj.defalutValue.split(",");
                            $.each(lstChecked, function (i, item) {
                                $('.text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                            });
                        }
                    }
                    catch (e) {
                        if (obj.defalutValue != "" && obj.defalutValue != null) {
                            var lstChecked = obj.defalutValue.split(",");
                            $.each(lstChecked, function (i, item) {
                                $('.text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                            });
                        }
                        //$('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                    }

                    //let lstvalue = JSON.parse(obj.defalutValue);
                    //let value = lstvalue.filter(x => x.checked);
                    //if (value.length > 0) {
                    //    $.each(value, function (i, item) {
                    //        $('.text-modal-' + obj.codeProperties + '[value = "' + item.code + '"]').prop("checked", true);
                    //    });
                    //}
                }
            });

            //show tab 2//
            $(mapdata.SELECTORS.propertyStrokeText).val(mapdata.GLOBAL.mainObjectSelected.properties.stroke);
            $(mapdata.SELECTORS.propertyStrokeColor).val(mapdata.GLOBAL.mainObjectSelected.properties.stroke);
            $(mapdata.SELECTORS.propertyDivStroke).css('background-color', mapdata.GLOBAL.mainObjectSelected.properties.stroke);

            $(mapdata.SELECTORS.propertyStrokeWidth).val(mapdata.GLOBAL.mainObjectSelected.properties.strokeWidth);

            const calcLeftPositionStress = value => 100 / (100 - 0) * (value - 0);
            let strokeOpacity = mapdata.GLOBAL.mainObjectSelected.properties.strokeOpacity * 100;
            $(mapdata.SELECTORS.propertyStrokeOpacity).val(strokeOpacity);

            $('#thumbMax-stroke').css('left', calcLeftPositionStress(strokeOpacity) + '%');
            $('#max-stroke').html(strokeOpacity + '.00%');
            $('#line-stroke').css({
                'right': (100 - calcLeftPositionStress(strokeOpacity)) + '%'
            });

            $(mapdata.SELECTORS.propertyFillText).val(mapdata.GLOBAL.mainObjectSelected.properties.fill);
            $(mapdata.SELECTORS.propertyFillColor).val(mapdata.GLOBAL.mainObjectSelected.properties.fill);
            $(mapdata.SELECTORS.propertyDivFill).css('background-color', mapdata.GLOBAL.mainObjectSelected.properties.fill);

            let fillOpacity = mapdata.GLOBAL.mainObjectSelected.properties.fillOpacity * 100;
            $(mapdata.SELECTORS.propertyFillOpacity).val(fillOpacity);
            $('#thumbMax-fill').css('left', calcLeftPositionStress(fillOpacity) + '%');
            $('#max-fill').html(fillOpacity + '.00%');
            $('#line-fill').css({
                'right': (100 - calcLeftPositionStress(fillOpacity)) + '%'
            });
            modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = true;
        });

        $('.li-tab').on('click', function () {
            $('.li-tab').removeClass('active');
            $(this).addClass('active');
            let value = $(this).attr('data-id');
            $('.div-child-body').css('display', 'none');
            $('#' + value).css('display', 'block');
        });

        $('#checkbox-inheritance').on('click', function () {
            let status = $(this).is(":checked");
            //if (status) mapdata.showDefaultPropertiesCreateObject();
        });

        $(mapdata.SELECTORS.btn_save_step_info).on('click', function () {
            toastr.remove();
            if (Info_Properties_Main_Object.checkFormInput(mapdata.SELECTORS.inforData + " .step-info-content")) {
                var object = {
                    id: mapdata.GLOBAL.idMainObject !== "" ? mapdata.GLOBAL.idMainObject : (mapdata.GLOBAL.mainObjectSelected !== null) ? mapdata.GLOBAL.mainObjectSelected.id : "",
                    nameObject: $(mapdata.SELECTORS.inforData + " #text-modal-Name").val(),
                    listProperties: []
                };

                var properties = Info_Properties_Main_Object.getValueFormPropeties(mapdata.SELECTORS.inforData);

                object.listProperties = properties.listProperties;
                //loading
                abp.ui.setBusy(mapdata.SELECTORS.inforData);
                // cập nhật link, image
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json-patch+json",
                    async: true,
                    url: mapdata.CONSTS.URL_UPDATE_PROPERTIES_MAIN_OBJECT,
                    data: JSON.stringify(object),
                    success: function (data) {
                        abp.ui.clearBusy(mapdata.SELECTORS.inforData);
                        if (data.code == "ok") {

                            $("a[menuid='" + object.id + "']").html('<span>' + object.nameObject + '</span>'); // thay đổi tên bên menu left

                            abp.notify.success(l("ManagementLayer:UpdateDataSuccesfully"));

                        }
                        else {
                            //swal({
                            //    title: l("ManagementLayer:Notification"),
                            //    text: l("ManagementLayer:UpdateDataFailed"),
                            //    icon: "warning",
                            //    button: l("ManagementLayer:Close"),
                            //})
                            abp.notify.error(l("ManagementLayer:UpdateDataFailed"));
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR + ":" + errorThrown);
                        abp.ui.clearBusy(mapdata.SELECTORS.inforData);
                    }
                });

            }
        });

        // District event
    },
    eventClickMap: function () {
        /*--event map--*/
        let eventMouseMove = map.addListener("mouseMove", (args) => {
            switch (mapdata.GLOBAL.selectmenu) {
                case "newline":
                    if (mapdata.GLOBAL.listDistance.listMarkerDistance.length > 0) {
                        let path = [];
                        let listMarker = mapdata.GLOBAL.listDistance.listMarkerDistance;
                        let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                        let mousePoint = [args.location.lng, args.location.lat];
                        path.push(endPoint);
                        path.push(mousePoint);
                        mapdata.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                        mapdata.ShowMeterDraw(endPoint, mousePoint, true);
                    }
                    break;
                case "newpolygon":
                    if (mapdata.GLOBAL.listArea.listMarkerArea.length > 0) {
                        let path = [];
                        let listMarker = mapdata.GLOBAL.listArea.listMarkerArea;
                        let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                        let mousePoint = [args.location.lng, args.location.lat];
                        path.push(endPoint);
                        path.push(mousePoint);
                        mapdata.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                        mapdata.ShowMeterDraw(endPoint, mousePoint, true);
                    }
                    break;
                case "newrectangle":
                    if (mapdata.GLOBAL.listRectangle.listMarkerRectangle.length == 1) {
                        let path = [];
                        let listMarker = mapdata.GLOBAL.listRectangle.listMarkerRectangle;
                        let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                        let mousePoint = [args.location.lng, args.location.lat];
                        path.push(endPoint);
                        path.push(mousePoint);
                        let iLatLng = [];
                        let postion = listMarker[0].getPosition();
                        mapdata.drawRectangle(postion, args.location);
                    }
                    break;
                case "newMultiline":
                    if (mapdata.GLOBAL.listDistance.listMarkerDistance.length > 0) {
                        let path = [];
                        let listMarker = mapdata.GLOBAL.listDistance.listMarkerDistance;
                        let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                        let mousePoint = [args.location.lng, args.location.lat];
                        path.push(endPoint);
                        path.push(mousePoint);
                        mapdata.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                        mapdata.ShowMeterDraw(endPoint, mousePoint, true);
                    }
                    break;
                case "newMultipolygon":
                    if (mapdata.GLOBAL.listArea.listMarkerArea.length > 0) {
                        let path = [];
                        let listMarker = mapdata.GLOBAL.listArea.listMarkerArea;
                        let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                        let mousePoint = [args.location.lng, args.location.lat];
                        path.push(endPoint);
                        path.push(mousePoint);
                        mapdata.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                        mapdata.ShowMeterDraw(endPoint, mousePoint, true);
                    }
                    break;
                default: break;
            }
        });

        let eventClickMap = map.addListener("click", (args) => {
            switch (mapdata.GLOBAL.selectbtn) {
                case "edit":
                    break;
                case "delete":
                    break;
                case "insert":
                    switch (mapdata.GLOBAL.selectmenu) {
                        case "newpoint":
                            mapdata.setMarkerDraw(args.location.lat, args.location.lng);
                            mapdata.GLOBAL.selectmenu = "";
                            break;
                        case "newline":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 1);
                            if (mapdata.GLOBAL.listDistance.listMarkerDistance.length > 1) {
                                let iLatLng = [];
                                $.each(mapdata.GLOBAL.listDistance.listMarkerDistance, function () {
                                    let latLng = { lat: this.getPosition().lat, lng: this.getPosition().lng };
                                    iLatLng.push(latLng);
                                });
                                mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, true);
                            }
                            break;
                        case "newpolygon":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 2);
                            if (mapdata.GLOBAL.listArea.listMarkerArea.length > 1) {
                                let iLatLng = [];
                                $.each(mapdata.GLOBAL.listArea.listMarkerArea, function () {
                                    let latLng = { lat: this.getPosition().lat, lng: this.getPosition().lng };
                                    iLatLng.push(latLng);
                                });
                                iLatLng.push(iLatLng[0]);
                                mapdata.createPolygonArea([iLatLng]);

                            }
                            break;
                        case "newrectangle":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 3);
                            break;
                        case "newMultipoint":
                            mapdata.setMultiMarkerDraw(args.location.lat, args.location.lng);
                            break;
                        case "newMultiline":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 1);
                            if (mapdata.GLOBAL.listDistance.listMarkerDistance.length > 1) {
                                let iLatLng = [];
                                $.each(mapdata.GLOBAL.listDistance.listMarkerDistance, function () {
                                    let latLng = { lat: this.getPosition().lat, lng: this.getPosition().lng };
                                    iLatLng.push(latLng);
                                });
                                //mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, true);
                            }
                            break;
                        case "newMultipolygon":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 2);
                            if (mapdata.GLOBAL.listArea.listMarkerArea.length > 1) {
                                let iLatLng = [];
                                $.each(mapdata.GLOBAL.listArea.listMarkerArea, function () {
                                    let latLng = { lat: this.getPosition().lat, lng: this.getPosition().lng };
                                    iLatLng.push(latLng);
                                });
                                iLatLng.push(iLatLng[0]);
                                mapdata.createPolygonArea([iLatLng]);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }, { map: true });

        let eventDoubleClickMap = map.addListener("dblClick", (args) => {
            let iLatLng = [];
            switch (mapdata.GLOBAL.selectbtn) {
                case "edit":
                    break;
                case "insert":
                    switch (mapdata.GLOBAL.selectmenu) {
                        case "newline":
                            let listMarker = mapdata.GLOBAL.listDistance.listMarkerDistance;
                            iLatLng = [];
                            $.each(listMarker, function (i, obj) {
                                let latLng = [obj.getPosition().lng, obj.getPosition().lat];
                                iLatLng.push(latLng);
                            });
                            mapdata.GLOBAL.listDistance.path = iLatLng;
                            mapdata.drawPolylineDbclick(mapdata.GLOBAL.listDistance.path);
                            mapdata.GLOBAL.selectmenu = "";
                            break;
                        case "newpolygon":
                            mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                            mapdata.GLOBAL.markerMeter.setMap(null);
                            mapdata.GLOBAL.markerMeter = null;
                            mapdata.GLOBAL.selectmenu = "";
                            break;
                        case "newrectangle":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 3);
                            mapdata.setDragPoligonMarker(mapdata.GLOBAL.listRectangle.listMarkerRectangle);
                            mapdata.GLOBAL.selectmenu = "";
                            break;
                        case "newMultiline":
                            let listMarkerMulti = mapdata.GLOBAL.listDistance.listMarkerDistance;
                            iLatLng = [];
                            $.each(listMarkerMulti, function (i, obj) {
                                let latLng = [obj.getPosition().lng, obj.getPosition().lat];
                                iLatLng.push(latLng);
                            });
                            mapdata.GLOBAL.listDistance.path = iLatLng;
                            //mapdata.drawPolylineDbclick(mapdata.GLOBAL.listDistance.path);
                            mapdata.drawMultiPolylineDbclick(mapdata.GLOBAL.listDistance.path, mapdata.uuidv4());
                            break;
                        case "newMultipolygon":
                            mapdata.drawMultiPolygonDbclick(mapdata.uuidv4())
                            break;
                        default: break;
                    }
                    break;
                case "delete":
                    break;
                default:
                    break;
            }

        }, { marker: true, polygon: true });

        let eventClickDraw = map.addListener("click", (args) => {
            let iLatLng = [];
            switch (mapdata.GLOBAL.selectbtn) {
                case "edit":
                    break;
                case "delete":
                    break;
                case "insert":
                    switch (mapdata.GLOBAL.selectmenu) {
                        case "newline":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 1);
                            let listMarker = mapdata.GLOBAL.listDistance.listMarkerDistance;
                            $.each(listMarker, function (i, obj) {
                                let latLng = { lat: obj.getPosition().lat, lng: obj.getPosition().lng };
                                iLatLng.push(latLng);

                            });
                            if (iLatLng.length > 1) {
                                let startPoint = [iLatLng[iLatLng.length - 2].lng, iLatLng[iLatLng.length - 2].lat];
                                let endPoint = [iLatLng[iLatLng.length - 1].lng, iLatLng[iLatLng.length - 1].lat];
                                mapdata.ShowMeterDraw(startPoint, endPoint, false);
                            }
                            mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, true);
                            break;
                        case "newpolygon":
                            var oldlenght = mapdata.GLOBAL.listArea.listMarkerArea.length;
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 2);
                            if (mapdata.GLOBAL.listArea.listMarkerArea.length == 2) {
                                let listarea = mapdata.GLOBAL.listArea.listMarkerArea;
                                $.each(listarea, function () {
                                    let item = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                                    iLatLng.push(item);
                                });

                                if (oldlenght != mapdata.GLOBAL.listArea.listMarkerArea.length) {
                                    //drawing polyline
                                    mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, false);
                                }
                            }
                            else if (mapdata.GLOBAL.listArea.listMarkerArea.length > 2) {
                                let listarea = mapdata.GLOBAL.listArea.listMarkerArea;
                                $.each(listarea, function () {
                                    let latLng = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                                    iLatLng.push(latLng);
                                });
                                //drawing polygon
                                iLatLng.push(iLatLng[0]);
                                mapdata.createPolygonArea([iLatLng]);
                                if (mapdata.GLOBAL.listArea.PolylineArea != null) {
                                    mapdata.GLOBAL.listArea.PolylineArea.setMap(null);
                                    mapdata.GLOBAL.listArea.PolylineArea = null;
                                }
                            }
                            break;
                        case "newMultiline":
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 1);
                            let listMarkerMulti = mapdata.GLOBAL.listDistance.listMarkerDistance;
                            $.each(listMarkerMulti, function (i, obj) {
                                let latLng = { lat: obj.getPosition().lat, lng: obj.getPosition().lng };
                                iLatLng.push(latLng);
                            });
                            if (iLatLng.length > 1) {
                                let startPoint = [iLatLng[iLatLng.length - 2].lng, iLatLng[iLatLng.length - 2].lat];
                                let endPoint = [iLatLng[iLatLng.length - 1].lng, iLatLng[iLatLng.length - 1].lat];
                                mapdata.ShowMeterDraw(startPoint, endPoint, false);
                            }
                            mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, true);
                            break;
                        case "newMultipolygon":
                            var oldLenght = mapdata.GLOBAL.listArea.listMarkerArea.length;
                            mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 2);
                            if (mapdata.GLOBAL.listArea.listMarkerArea.length == 2) {
                                let listarea = mapdata.GLOBAL.listArea.listMarkerArea;
                                $.each(listarea, function () {
                                    let item = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                                    iLatLng.push(item);
                                });

                                if (oldLenght != mapdata.GLOBAL.listArea.listMarkerArea.length) {
                                    //drawing polyline
                                    mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, false);
                                }
                            }
                            else if (mapdata.GLOBAL.listArea.listMarkerArea.length > 2) {
                                let listarea = mapdata.GLOBAL.listArea.listMarkerArea;
                                $.each(listarea, function () {
                                    let latLng = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                                    iLatLng.push(latLng);
                                });
                                //drawing polygon
                                iLatLng.push(iLatLng[0]);
                                mapdata.createPolygonArea([iLatLng]);
                                if (mapdata.GLOBAL.listArea.PolylineArea != null) {
                                    mapdata.GLOBAL.listArea.PolylineArea.setMap(null);
                                    mapdata.GLOBAL.listArea.PolylineArea = null;
                                }
                            }
                            break;
                        default: break;
                    }
                    break;
                default:
                    break;
            }
        }, { polyline: true, polygon: true, marker: true });

        let eventdragDraw = map.addListener("drag", (args) => {
            if (mapdata.GLOBAL.statusEdit) {
                let iLatLng = [];
                //line
                if (mapdata.GLOBAL.listDistance.listMarkerDistance != null && mapdata.GLOBAL.listDistance.listMarkerDistance.length > 0) {
                    var markerchange = mapdata.GLOBAL.listDistance.listMarkerDistance.find(x => x.id == args.marker.id);
                    if (typeof markerchange !== "undefined") {
                        iLatLng = []
                        let position = args.marker.getPosition();
                        markerchange.setPosition(position);

                        let listMarker = mapdata.GLOBAL.listDistance.listMarkerDistance;

                        $.each(listMarker, function (i, obj) {
                            let latLng = [obj.getPosition().lng, obj.getPosition().lat];
                            iLatLng.push(latLng);
                        });
                        mapdata.GLOBAL.listDistance.path = iLatLng;
                        mapdata.drawPolylineDbclick(mapdata.GLOBAL.listDistance.path);
                    }
                }
                //polygon
                if (mapdata.GLOBAL.listArea.listMarkerArea != null && mapdata.GLOBAL.listArea.listMarkerArea.length > 0) {
                    if (!model3D.GLOBAL.editMode3D.status || (model3D.GLOBAL.editMode3D.status && model3D.CheckPointInPolygonOrMulti(args.location.lat, args.location.lng))) {
                        let markerchangeMulti = mapdata.GLOBAL.listArea.listMarkerArea.find(x => x.id == args.marker.id);
                        let indexMarker = mapdata.GLOBAL.listArea.listMarkerArea.findIndex(x => x.id == args.marker.id);
                        var pathsP = mapdata.GLOBAL.listArea.polygonArea.getPaths();
                        let position = args.marker.getPosition();
                        pathsP = mapdata.setLatLngPolygon(pathsP, position, indexMarker);
                        mapdata.GLOBAL.listArea.polygonArea.setPaths(pathsP);
                    }

                    //iLatLng = []
                    //let listarea = mapdata.GLOBAL.listArea.listMarkerArea;
                    //$.each(listarea, function () {
                    //    let latLng = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                    //    iLatLng.push(latLng);
                    //});
                    ////drawing polygon
                    //iLatLng.push(iLatLng[0]);
                    //mapdata.createPolygonArea([iLatLng]);
                }
                //rectangle
                if (mapdata.GLOBAL.listRectangle.listMarkerRectangle !== null && mapdata.GLOBAL.listRectangle.listMarkerRectangle.length > 0) {
                    mapdata.drawRectangle(mapdata.GLOBAL.listRectangle.listMarkerRectangle[0].getPosition(), mapdata.GLOBAL.listRectangle.listMarkerRectangle[1].getPosition());
                }
                //multi line
                if (mapdata.GLOBAL.listMultiLine !== null && mapdata.GLOBAL.listMultiLine.length > 0) {
                    let idGuid = args.marker.userData;
                    let objData = mapdata.GLOBAL.listMultiLine.find(x => x.id == idGuid);
                    if (objData !== null && typeof objData !== "undefined") {
                        let markerchangeMulti = objData.polyline.listMarkerDistance.find(x => x.id == args.marker.id);
                        if (typeof markerchangeMulti !== "undefined") {
                            iLatLng = []
                            //console.log("args.marker.position = ", args.marker.getPosition());
                            //console.log("markerchangeMulti.position = ", markerchangeMulti.getPosition());
                            // markerchangeMulti.position = args.marker.position;
                            let position = args.marker.getPosition();
                            markerchangeMulti.setPosition(position);
                            let listMarker = objData.polyline.listMarkerDistance;

                            $.each(listMarker, function (i, obj) {
                                let latLng = [obj.getPosition().lng, obj.getPosition().lat];
                                iLatLng.push(latLng);
                            });
                            objData.polyline.path = iLatLng;
                            mapdata.drawMultiPolylineEdit(objData.polyline.path, idGuid, objData.polyline);
                        }
                    }
                }
                //multi polygon
                if (mapdata.GLOBAL.listMultiPolygon !== null && mapdata.GLOBAL.listMultiPolygon.length > 0) {
                    let idPolygon = args.marker.userData;
                    let objData = mapdata.GLOBAL.listMultiPolygon.find(x => x.id == idPolygon);
                    if (objData !== null && typeof objData !== "undefined") {
                        let markerchangeMulti = objData.polygon.listMarkerArea.find(x => x.id == args.marker.id);
                        let indexMarker = objData.polygon.listMarkerArea.findIndex(x => x.id == args.marker.id);
                        var paths = objData.polygon.polygonArea.getPaths();
                        let position = args.marker.getPosition();
                        paths = mapdata.setLatLngPolygon(paths, position, indexMarker);
                        //paths[indexMarker] = { lat: position.lat, lng: position.lng };
                        objData.polygon.polygonArea.setPaths(paths);
                    }
                }
            }
        }, { marker: true, polygon: true, polyline: true });

        let eventClickData = map.data.addListener("click", (args) => {
            let idClick = args.feature.getId();
            let check = null;
            if (mapdataTreeView.GLOBAL.lstFeature.length > 0) {
                for (var i = 0; i < mapdataTreeView.GLOBAL.lstFeature.length; i++) {
                    let result = mapdataTreeView.GLOBAL.lstFeature[i].find(x => x._id == idClick);
                    if (result != null && result != undefined) {
                        check = result;
                    }
                }
            }
            if (check == null || check == undefined) {
                switch (mapdata.GLOBAL.selectbtn) {
                    case "edit":
                        mapdata.GLOBAL.selectObject2D = args;
                        editform.OpenFormEdit(args.feature._id);
                        documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                        var obj3d = mapdata.GLOBAL.listObject3D.find(x => x.id == args.feature._id); // object 3d

                        if (typeof obj3d !== "undefined" && obj3d !== null && obj3d.object !== null) { // hightlight 3d
                            mapdata.visibleHightlight(false);
                            obj3d.object.setSelected(true);
                        }
                        break;
                    case "delete":
                        //console.log("delete", args.feature._id);
                        //highlight
                        mapdata.highlightObject(args);
                        mapdata.clearHighlightPoint(); //clear highlight khi chọn đối tượng khác
                        if (args.feature._geometry._coordinate !== undefined) {
                            mapdata.highlightObjectPoint(args.feature._geometry._coordinate.lat, args.feature._geometry._coordinate.lng);
                        }
                        if (args.feature._geometry._coordinates !== undefined) {
                            mapdata.highlightObjectMutiplePoint(args.feature._geometry._coordinates);
                        }

                        mapdata.GLOBAL.selectObject2D = args;

                        var idDelete = args.feature._id;
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:DoYouWantDeleteThisObject"),
                            icon: "warning",
                            buttons: [
                                l("ManagementLayer:Cancel"),
                                l("ManagementLayer:Remove")
                            ],
                            dangerMode: false,
                        }).then((willDelete) => {
                            if (willDelete) {
                                mapdata.deleteMainObject(idDelete);
                            }
                            else {
                                if (mapdata.GLOBAL.selectObject2D != null) {
                                    map.data.add(mapdata.GLOBAL.selectObject2D.feature);
                                }
                            }
                            mapdata.clearHighlight();
                            mapdata.clearHighlightPoint();
                        });

                        var feature = mapdata.GLOBAL.features.find(x => x._id == idDelete);
                        if (feature != undefined) {
                            map.data.remove(feature);
                        }

                        break;
                    case "insert":
                        break;
                    default:
                        if (model3D.GLOBAL.editMode3D.status) break; //check if edit mode 3d
                        mapdata.GLOBAL.selectObject2D = args;
                        modalCreateFolderLayer.resetTableData();
                        mapdata.visibleHightlight(false);
                        if (!model3D.GLOBAL.checkdraw) {

                            // ẩn 3d
                            let objold = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
                            if (typeof objold !== "undefined" && objold !== null && objold.object !== null) {
                                objold.object.setSelected(false);
                            }

                            //if (mapdata.GLOBAL.mainObjectSelected == null) {
                            if (mapdata.GLOBAL.selectbtn === "") {

                                model3D.GLOBAL.geojsonDefault3D = "";
                                model3D.GLOBAL.geojson3D = {
                                    type: "Polygon",
                                    coordinates: []
                                };
                                model3D.GLOBAL.objectGeojson3D = null;
                                mapdata.highlightObject(args);
                                mapdata.GLOBAL.idMainObject = args.feature.getId();
                                //map.data.remove(args.feature);

                                $('.ul-left li').removeClass("menu-open");
                                $($('a[menuid="' + mapdata.GLOBAL.idMainObject + '"]')[0]).parent().addClass("menu-open");
                                $('#InforData').addClass('active')
                                mapdata.getMainObject("", mapdata.SELECTORS.inforData);
                                mapdata.getDefaultObject("", mapdata.SELECTORS.inforData);
                                Info_Properties_Main_Object.setValueObject(menuLeft.SELECTORS.content_step_info_data);

                                model3D.setData();
                                mapdata.showAllObject3D();
                                //mapdata.hideObject3DById(mapdata.GLOBAL.idMainObject);
                                var obj3d = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject); // object 3d
                                if (obj3d != undefined && obj3d != null && obj3d.object !== null) { // hightlight 3d
                                    //mapdata.visibleHightlight(false);
                                    //if (obj3d.object.getMap() === null) obj3d.object.setMap(map);
                                    model3D.GLOBAL.building = obj3d.object;
                                    model3D.GLOBAL.building.setSelected(true);
                                    //model3D.GLOBAL.building.setDraggable(true);
                                }

                                informap2d.setInfor2D();
                                documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                                modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                                mapdata.clearHighlightPoint();

                                // hightlight point
                                if (args.feature._geometry._coordinate !== undefined) {
                                    mapdata.highlightObjectPoint(args.feature._geometry._coordinate.lat, args.feature._geometry._coordinate.lng);
                                }

                                // hightlight mutiple point
                                if (args.feature._geometry._coordinates !== undefined) {
                                    mapdata.highlightObjectMutiplePoint(args.feature._geometry._coordinates);
                                }

                            } else if (mapdata.GLOBAL.selectbtn === "edit") {
                                if (mapdata.GLOBAL.selectObject2D === null) {
                                    mapdata.convertGeojsonToData(args.feature);
                                }
                            }
                            Activity.GetDanhSachHoatDong(mapdata.GLOBAL.idMainObject, mapdata.GLOBAL.idDirectory);
                            //}
                            $(`li[menuid='${mapdata.GLOBAL.idMainObject}']`).addClass('menu-open');
                            $('#InforData .li-modal-data-right').eq(3).trigger('click');

                        }

                }
            }
        });

        let event = map.addListener("modeChanged",
            (args) => {
                let objectmainid = mapdata.GLOBAL.idMainObject;
                model3D.GLOBAL.idObject2D = "";
                if (args.is3DMode) {
                    mapdata.clearDataGeojson();

                    //let array = mapdata.GLOBAL.listObjectByDirectory.filter(x => x.id == objectmainid);
                    //let geojson = mapdata.getDataGeojsonByProperties(array);
                    //model3D.showDataGeojson3D(JSON.stringify(geojson));

                    mapdata.GLOBAL.idMainObject = objectmainid;
                    model3D.GLOBAL.idObject2D = objectmainid;
                    mapdata.visibleHightlight(false);
                    mapdata.visibleHighlightPoint(false); // ẩn point highlight
                    mapdata.visibleHightlight(true); // ẩn line hightlight

                    for (var i = 0; i < mapdataTreeView.GLOBAL.listMarker.length; i++) // ẩn marker
                    {
                        var lstMarker = mapdataTreeView.GLOBAL.listMarker[i].lstMarker;
                        for (var j = 0; j < lstMarker.length; j++) {
                            lstMarker[j].setVisible(false)
                        }
                    }
                    for (var i = 0; i < mapdataTreeView.GLOBAL.listObject3D.length; i++) // hiển thị building
                    {
                        if (mapdataTreeView.GLOBAL.listObject3D[i].object != null) {
                            mapdataTreeView.GLOBAL.listObject3D[i].object.setVisible(true)
                        }
                    }
                    //$(mapdataTreeView.SELECTORS.drop_action_draw).addClass('hidden');
                    map.data.clear(); // ẩn geojson
                } else {
                    var camera = map.getCamera();
                    camera.setTilt(0);
                    camera.setBearing(0);
                    map.moveCamera(camera);

                    mapdata.clearDataGeojson();
                    let array = mapdata.GLOBAL.listObjectByDirectory;
                    let geojson = mapdata.getDataGeojsonByProperties(array);
                    mapdata.showDataGeojson(JSON.stringify(geojson));
                    mapdata.GLOBAL.idMainObject = objectmainid;
                    mapdata.visibleHightlight(true);
                    mapdata.visibleHighlightPoint(true); // hiện point highlight
                    mapdata.visibleHightlight(true); // hiện line hightlight

                    //TreeView//
                    for (var i = 0; i < mapdataTreeView.GLOBAL.listMarker.length; i++) // hiển thị marker
                    {
                        var lstMarker = mapdataTreeView.GLOBAL.listMarker[i].lstMarker;
                        for (var j = 0; j < lstMarker.length; j++) {
                            lstMarker[j].setVisible(true)
                        }
                    }

                    mapdataTreeView.GLOBAL.listgeojson.forEach((geojson) => // hiển thị geojson
                    {
                        mapdataTreeView.showDataGeojson2(geojson.geojson, geojson.id);
                    });

                    for (var i = 0; i < mapdataTreeView.GLOBAL.listObject3D.length; i++) //ẩn building
                    {
                        if (mapdataTreeView.GLOBAL.listObject3D[i].object != null) {
                            mapdataTreeView.GLOBAL.listObject3D[i].object.setVisible(false)
                        }
                    }

                    //$(mapdataTreeView.SELECTORS.drop_action_draw).removeClass('hidden');
                }

                switch (mapdata.GLOBAL.selectbtn) {
                    case "edit":
                        // ẩn/hiển thị trạng thái vẽ 2d
                        if (args.is3DMode) {
                            mapdata.HideOrShowDrawMainObject(false);
                        } else {
                            mapdata.HideOrShowDrawMainObject(true);

                            // nếu là 2d thì ẩn geojson của mainobject hiện tại
                            if (mapdata.GLOBAL.selectObject2D != null) {
                                map.data.remove(mapdata.GLOBAL.selectObject2D.feature);
                            }
                        }
                        break;
                    case "delete":
                        break;
                    case "insert":
                        break;
                    default:
                        break;
                }

            });

        let eventbuilding = map.addListener("click",
            (args) => {
                let value = args.building.userData;
                if (value != undefined) {
                    if (value.id != undefined && value.id != null) {
                        let check = mapdataTreeView.GLOBAL.listObject3D.find(x => x.id == value.id);
                        if (check != null && check != undefined) {
                            return;
                        }
                    }
                    if (model3D.GLOBAL.editMode3D.status) return false;
                    map.setSelectedBuildings([]);
                    var obj = mapdata.GLOBAL.features.find(x => x._id === value);
                    let objold = value !== "" ? mapdata.GLOBAL.listObject3D.find(x => x.id == value) : mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
                    if (objold !== null && typeof objold !== "undefined" && objold.object !== null) {
                        objold.object.setSelected(false);
                    }

                    if ((obj != null && obj != undefined) || objold != null) {
                        mapdata.GLOBAL.idMainObject = value;
                        switch (mapdata.GLOBAL.selectbtn) {
                            case "delete":
                                // hightlight da giac
                                var feature = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.idMainObject);
                                mapdata.highlightObject({ feature: feature });

                                // higlight 2d
                                mapdata.clearHighlightPoint();
                                if (obj._geometry._coordinate !== undefined) {
                                    mapdata.highlightObjectPoint(obj._geometry._coordinate.lat, obj._geometry._coordinate.lng);
                                }

                                if (obj._geometry._coordinates !== undefined) {
                                    mapdata.highlightObjectMutiplePoint(obj._geometry._coordinates);
                                }

                                args.building.setSelected(true);

                                if (feature != null && feature != undefined) {
                                    mapdata.GLOBAL.selectObject2D = {
                                        feature: feature,
                                        location: { lat: args.location.lat, lng: args.location.lng }
                                    };
                                }

                                var idDelete = mapdata.GLOBAL.idMainObject;
                                swal({
                                    title: l("ManagementLayer:Notification"),
                                    text: l("ManagementLayer:DoYouWantDeleteThisObject"),
                                    icon: "warning",
                                    buttons: [
                                        l("ManagementLayer:Cancel"),
                                        l("ManagementLayer:Remove")
                                    ],
                                    dangerMode: false,
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        mapdata.deleteMainObject(idDelete);
                                    }
                                    else {
                                        if (!map.is3dMode()) {
                                            if (mapdata.GLOBAL.selectObject2D != null) {
                                                map.data.add(mapdata.GLOBAL.selectObject2D.feature);
                                            }
                                        }
                                        args.building.setSelected(false);
                                    }
                                    mapdata.clearHighlight();
                                    mapdata.clearHighlightPoint();
                                });

                                var feature = mapdata.GLOBAL.features.find(x => x._id == idDelete);
                                if (feature != undefined) {
                                    map.data.remove(feature);
                                }

                                break;
                            case "edit":
                                editform.OpenFormEdit(mapdata.GLOBAL.idMainObject);
                                documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);

                                mapdata.visibleHightlight(false);
                                args.building.setSelected(true);
                                break;
                            default:
                                mapdata.visibleHightlight(false);
                                $('.ul-left li').removeClass("menu-open");
                                $($('a[menuid="' + mapdata.GLOBAL.idMainObject + '"]')[0]).parent().addClass("menu-open");
                                $('#InforData').addClass('active');
                                mapdata.getMainObject("", mapdata.SELECTORS.inforData);
                                mapdata.getDefaultObject("", mapdata.SELECTORS.inforData);
                                Info_Properties_Main_Object.setValueObject(menuLeft.SELECTORS.content_step_info_data);
                                if (mapdata.GLOBAL.mainObjectSelected !== null) mapdata.highlightObjectData(mapdata.GLOBAL.mainObjectSelected);
                                model3D.setData();
                                mapdata.showAllObject3D();
                                model3D.GLOBAL.idObject2D = mapdata.GLOBAL.idMainObject;
                                //mapdata.hideObject3DById(mapdata.GLOBAL.idMainObject);
                                var obj3d = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject); // object 3d
                                if (obj3d != undefined && obj3d != null) { // hightlight 3d
                                    //mapdata.visibleHightlight(false);
                                    if (obj3d.object.getMap() === null) obj3d.object.setMap(map);
                                    model3D.GLOBAL.building = obj3d.object;
                                    model3D.GLOBAL.building.setSelected(true);
                                    //model3D.GLOBAL.building.setDraggable(true);
                                }
                                informap2d.setInfor2D();
                                model3D.setInfo3D();
                                //model3D.GLOBAL.coordinatesDefault3D = JSON.parse(JSON.stringify(model3D.GLOBAL.geojson3D));
                                documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                                modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                                mapdata.clearHighlightPoint();

                                // hightlight da giac
                                var feature = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.idMainObject);
                                mapdata.highlightObject({ feature: feature });

                                // higlight 2d
                                mapdata.clearHighlightPoint();
                                if (obj !== null && typeof obj !== "undefined" && obj._geometry._coordinate !== undefined) {
                                    mapdata.highlightObjectPoint(args.building.properties.location.lat, args.building.properties.location.lng);
                                }

                                if (obj !== null && typeof obj !== "undefined" && obj._geometry._coordinates !== undefined) {
                                    mapdata.highlightObjectMutiplePoint(obj._geometry._coordinates);
                                }
                                Activity.GetDanhSachHoatDong(mapdata.GLOBAL.idMainObject, mapdata.GLOBAL.idDirectory);
                                args.building.setSelected(true);

                                $(`li[menuid='${mapdata.GLOBAL.idMainObject}']`).addClass('menu-open');
                                $('#InforData .li-modal-data-right').eq(3).trigger('click');

                                //}
                                break;
                        }
                    }
                }

            }, { building: true });

        let eventmapbuilding = map.addListener("click",
            (args) => {
                if (model3D.GLOBAL.editMode3D.status) return false;
                let id = args.building.id;
                let objchosen = mapdata.GLOBAL.listObjectByDirectory.find(x => x.object3D != null && x.object3D.type == "para-choosenMap" && x.object3D.id == id);
                let objold = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
                if (objchosen != null && objchosen != undefined) {
                    if (objold !== null && typeof objold !== "undefined" && objold.object !== null) {
                        objold.object.setSelected(false);
                    }
                    let value = objchosen.id;
                    $('.ul-left li').removeClass("menu-open");
                    $($('a[menuid="' + mapdata.GLOBAL.idMainObject + '"]')[0]).parent().addClass("menu-open");
                    $('#InforData').addClass('active');

                    mapdata.GLOBAL.idMainObject = value;
                    mapdata.getMainObject("", mapdata.SELECTORS.inforData);
                    mapdata.getDefaultObject("", mapdata.SELECTORS.inforData);
                    Info_Properties_Main_Object.setValueObject(menuLeft.SELECTORS.content_step_info_data);
                    if (mapdata.GLOBAL.mainObjectSelected !== null) mapdata.highlightObjectData(mapdata.GLOBAL.mainObjectSelected);
                    model3D.setData();
                    mapdata.showAllObject3D();
                    //mapdata.hideObject3DById(mapdata.GLOBAL.idMainObject);
                    informap2d.setInfor2D();
                    model3D.setInfo3D();
                    documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                    //$(menuLeft.SELECTORS.liLeft).removeClass("menu-open");
                    //$($('a[menuid="' + mapdata.GLOBAL.idMainObject + '"]')[0]).parent().addClass("menu-open");

                    map.setSelectedBuildings([id]);
                    //args.building.setSelected(true);
                    Activity.GetDanhSachHoatDong(mapdata.GLOBAL.idMainObject, mapdata.GLOBAL.idDirectory);

                    $(`li[menuid='${mapdata.GLOBAL.idMainObject}']`).addClass('menu-open');
                    $('#InforData .li-modal-data-right').eq(3).trigger('click');
                }
            }, { mapbuilding: true });

        /*--end event map--*/
    },
    CancelCreate: function () {
        $(mapdata.SELECTORS.newgeojsonbutton).removeClass("active");
        $(mapdata.SELECTORS.modalCreateNew).removeClass("activeView");
        $(".div-content").removeClass("active active-rotate");
    },

    CancelUpload: function () {
        $(mapdata.SELECTORS.upload_button).removeClass("active");
        $(mapdata.SELECTORS.modalUploadMulti).removeClass("activeView");
        $(".div-content").removeClass("active active-rotate");
    },

    CancelAction: function () {

        if (/*(mapdata.GLOBAL.selectbtn == "edit" || mapdata.GLOBAL.selectbtn == "delete") &&*/ mapdata.GLOBAL.selectObject2D != null) {
            //map.data.add(mapdata.GLOBAL.selectObject2D.feature);
            if (!map.is3dMode()) {
                informap2d.reloadGeoJsonMap2d(mapdata.GLOBAL.selectObject2D.feature._id);
            }
        }
        mapdata.GLOBAL.selectbtn = "";
        mapdata.GLOBAL.selectmenu = "";
        mapdata.CancelCreate();
        mapdata.CancelUpload();
        mapdata.ResetIcon(); // reset icon thanh menu
        //mapdata.ResetInsertMainOjbect(); // reset trạng thái thêm mới
        modalCreateFolderLayer.resetTableData(); // reset data mở rộng

        mapdata.ResetInsertMainOjbect();
        $(".left-tree").css("opacity", "1"); // mở menu left

        //----------reset thanh menu ngang--------------------
        mapdata.GLOBAL.selectbtn = "";
        $('.focus').removeClass('isForcus');
        $('.focus').prop('disabled', false); // 
        $('.focus').removeClass('disabled');

        $('.notFocus').removeClass('isForcus');
        $('.notFocus').prop('disabled', false); // 
        $('.notFocus').removeClass('disabled');
        $(mapdata.SELECTORS.inforData).removeClass('active'); // ẩn content infodata
        $('#myModal2').removeClass('active'); // ẩn content insert bằng hình học
        $('.treeview').removeClass('menu-open'); // bỏ forcus menu left
        $('#EditForm').removeClass('active');

        $('.model-geojson').removeClass('active'); // ẩn content nhập dữ liệu geojson
        $('#model-properties-import').removeClass('active') // ẩn content nhập dữ liệu geojson bằng wkt/geojson
        $('.model-map-properties-geojson').removeClass('active') // ẩn content nhập dữ liệu geojson bằng thuộc tính có sẵn
        $('.model-new-properties-geojson').removeClass('active') // ẩn content nhập dữ liệu geojson bằng thuộc tính mới
        importmapdata.showOrHideExcelProperties(false);
        //importmapdata.ResetFormGeoJson(); // reset form import file geojson

        // ---------------remove focus một đối tượng-----------------------
        mapdata.GLOBAL.idMainObject = '';
        mapdata.GLOBAL.mainObjectSelected = null;
        model3D.hideModel3D();
        model3D.resetAll();
        mapdata.clearSelectDrawHighlight();
        mapdata.showAllObject3D();
        documentObject.GLOBAL.lstDocument = [];
        mapdata.clearHighlight();
        mapdata.clearHighlightPoint();
        map.setSelectedBuildings([]);

        UploadFileMultiObject.GLOBAL.fileArray = [];

        model3D.GLOBAL.checkdraw = false;// trạng thái vẽ
        model3D.RemoveBulding3D();
        model3D.resetDraw();

        mapdata.clearLabelError();

        DSObject.GLOBAL.table.ajax.reload(null, true);          // reset table danh sách QRCode về trang đầu tiên

        $("#hideTree").html(`<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <g transform="translate(1988 32)">
                                    <g transform="translate(-1988 -32)">
                                        <path style="fill: none;" class="a" d="M0,0H24V24H0Z"></path>
                                        <path style="fill: var(--primary)" class="b" d="M7.828,11H30v2H7.828l5.364,5.364-1.414,1.414L4,12l7.778-7.778,1.414,1.414Z"></path>
                                    </g>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -28)"></rect>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -14)"></rect>
                                </g>
                            </svg>`);

        //mapdata.clearDataGeojson();
        //mapdata.showDataGeojson(JSON.stringify(mapdata.GLOBAL.lstGeoJsonDefault));

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
    },
    modalInfo: function () {

        $(mapdata.SELECTORS.paraSelected).select2({
            language: {
                noResults: function () {
                    return l("ManagementLayer:NoResult");
                }
            }
        });

        $(mapdata.SELECTORS.selectParadigmId).select2({
            language: {
                noResults: function () {
                    return l("ManagementLayer:NoResult");
                }
            }
        });

        $(model3D.SELECTORS.selectObjectChosenMap).select2({
            language: {
                noResults: function () {
                    return l("ManagementLayer:NoResult");
                }
            }
        });

        // thông tin dữ liệu
        $(mapdata.SELECTORS.btnSaveProperties).on('click', function () {
            toastr.remove();
            var object = JSON.parse(JSON.stringify(mapdata.GLOBAL.mainObjectSelected));

            var imageDefault = "";
            if (object.properties != null) {
                imageDefault = object.properties.image2D;
            }
            //object.properties.image2D = "";
            if ($(Config_Main_Object.SELECTORS.content_info + " " + Config_Main_Object.SELECTORS.choosen_image).val() != "") {
                if (object.properties.image2D != "" && object.properties.image2D != null) {
                    var deleteImage = mapdata.DeleteFileImage(object.properties.image2D);
                }
            }

            object.properties = Config_Main_Object.getValueProperties();

            if (object.properties.image2D == null) {
                object.properties.image2D = imageDefault;
                mapdata.GLOBAL.mainObjectSelected.properties = object.properties;
            }

            object.propertiesGeojson = object.properties;

            object.propertiesGeojson.image2D = object.properties.image2D;
            object.isCheckImplementProperties = $(Config_Main_Object.SELECTORS.content_info + " " + Config_Main_Object.SELECTORS.checkbox_implement).is(":checked");
            abp.ui.setBusy(mapdata.SELECTORS.inforData);
            $.ajax({
                type: "POST",
                dataType: 'json',
                //contentType: false,
                //processData: false,
                contentType: "application/json-patch+json",
                async: false,
                url: mapdata.CONSTS.URL_AJAXUPDATESAVE,
                data: JSON.stringify(object),
                success: function (data) {
                    abp.ui.clearBusy(mapdata.SELECTORS.inforData);
                    if (data.code == "ok") {
                        abp.notify.success(l("ManagementLayer:UpdateDataSuccesfully"))
                        mapdata.getEditObject("");
                        mapdata.GLOBAL.mainObjectSelected = data.result;
                        mapdata.GLOBAL.idMainObject = mapdata.GLOBAL.mainObjectSelected.id;
                        $(Config_Main_Object.SELECTORS.choosen_image).val('');
                        mapdata.GLOBAL.mainObjectSelected.object3D != null && model3D.resetMode3DAfterSave(mapdata.GLOBAL.mainObjectSelected.object3D, mapdata.GLOBAL.mainObjectSelected.id);
                    }
                    else {
                        abp.notify.error(l("ManagementLayer:UpdateDataFailed"))
                            .then((value) => {
                                //location.href = "/HTKT/Layer";
                            });
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR + ":" + errorThrown);
                    abp.ui.clearBusy(mapdata.SELECTORS.inforData);
                }
            });
        });

        if ($(mapdata.SELECTORS.selectParadigmId)[0].length == 1) {
            $.each(mapdata.GLOBAL.list, function (i, item) {
                $(mapdata.SELECTORS.selectParadigmId).append($('<option>', {
                    value: item.id,
                    text: item.name
                }));
            });
        }

        if ($(model3D.SELECTORS.selectObjectChosenMap)[0].length == 1) {
            $.each(mapdata.GLOBAL.listObjectMapInRadius, function (i, item) {
                $(model3D.SELECTORS.selectObjectChosenMap).append($('<option>', {
                    value: item.id,
                    text: item.name
                }));
            });
        }
    },
    updateMainObject: function () {
        var array = null;
        if (mapdata.GLOBAL.mainObjectSelected != null) {
            let object = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.mainObjectSelected.id);
            map.data.remove(object);
            mapdata.GLOBAL.features = mapdata.GLOBAL.features.filter(x => x._id !== mapdata.GLOBAL.mainObjectSelected.id);
            array = JSON.parse(JSON.stringify(mapdata.GLOBAL.mainObjectSelected));
            array.properties.stroke = $(mapdata.SELECTORS.strokeColor).val();
            array.properties.strokeWidth = parseFloat($('#text-width').val());
            array.properties.strokeOpacity = $(mapdata.SELECTORS.strokeOpacity).val() / 100;
            array.properties.fill = $(mapdata.SELECTORS.fillColor).val();
            array.properties.fillOpacity = $(mapdata.SELECTORS.fillOpacity).val() / 100;
        }
        return array;
    },

    // --- get thuoc tinh layer cho them moi form---- 
    getPropertiesDirectory: function (id) {
        id = mapdata.GLOBAL.idDirectory;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=' + id,
            data: {
            },
            success: function (data) {
                if (data.code == "ok" && data.result.listProperties != undefined) {
                    var result = [];
                    if (mapdata.GLOBAL.lstPropertiesDirectory.length == 0) {
                        mapdata.GLOBAL.lstPropertiesDirectory = data.result.listProperties;
                    }
                    $(mapdata.SELECTORS.modalbody).html('');
                    $(mapdata.SELECTORS.modelPropertiesImport).html('');
                    modalCreateFolderLayer.GLOBAL.listImageInput = [];
                    modalCreateFolderLayer.GLOBAL.filecount = 0;
                    modalCreateFolderLayer.GLOBAL.listIdImageTemporary = [];
                    //$(modalCreateFolderLayer.SELECTORS.inputListDocument).html('');
                    //$(UploadFileMultiObject.SELECTORS.inputListDocument).html('');

                    let indexExtendActive = -1;
                    var lst = mapdata.GLOBAL.lstPropertiesDirectory.filter(x => x.typeSystem != 1);
                    $('#myModal2 .title-li-tab .class-extend').css('display', 'none'); // ẩn tab mở rộng
                    var getButtonIsActive = $(".button-active").attr("data-code");
                    if (getButtonIsActive == "newPolygonByCountry" || getButtonIsActive == "newMultipolygonByCountry") {
                        mapdata.RenderHTMLForNewPolyGon('District', getButtonIsActive, "Create");
                        mapdata.RenderHTMLForNewPolyGonOfWardDistrict('', getButtonIsActive, "Create")
                    }
                    //$.each(data.result.listProperties, function (i, obj)
                    $.each(lst, function (i, obj) {
                        if (obj.typeSystem !== 1) {
                            var element = {
                                NameProperties: obj.nameProperties,
                                CodeProperties: obj.codeProperties,
                                TypeProperties: obj.typeProperties,
                                IsShow: obj.isShow,
                                IsIndexing: obj.isIndexing,
                                IsRequest: obj.isRequest,
                                IsView: obj.isView,
                                IsHistory: obj.isHistory,
                                IsInheritance: obj.isInheritance,
                                IsAsync: obj.isAsync,
                                IsShowExploit: obj.isShowExploit,
                                DefalutValue: obj.defalutValue,
                                ShortDescription: obj.shortDescription,
                                TypeSystem: obj.typeSystem,
                                OrderProperties: obj.orderProperties
                            };
                            result.push(element);

                            if (element.TypeProperties == "image") {
                                mapdata.GLOBAL.listObjectImageEdit = modalCreateFolderLayer.GLOBAL.listObjectImage;
                                $($('.model-properties-object .item-FilePlace')[0]).css('display', 'flex');
                                indexExtendActive = 0;
                                $('#myModal2 .title-li-tab .class-extend').css('display', 'block'); // mở tab mở rộng
                            }
                            if (element.TypeProperties == "link") {
                                mapdata.GLOBAL.listObjectLinkCameraEdit = modalCreateFolderLayer.GLOBAL.listObjectLinkCamera;
                                $($('.model-properties-object .item-FilePlace')[1]).css('display', 'flex');

                                $('#myModal2 .title-li-tab .class-extend').css('display', 'block'); // mở tab mở rộng

                                indexExtendActive = 1;
                            }
                            if (element.TypeProperties == "file") {
                                mapdata.GLOBAL.listObjectFileEdit = modalCreateFolderLayer.GLOBAL.listObjectFile;
                                for (var i = 0; i < mapdata.GLOBAL.listObjectFileEdit.length; i++) {
                                    if (mapdata.GLOBAL.listObjectFileEdit[i].idDirectoryProperties == obj.codeProperties) {
                                        mapdata.GLOBAL.listObjectFileEdit[i].nameProperties = obj.nameProperties
                                    }
                                }

                                $('#myModal2 .title-li-tab .class-extend').css('display', 'block'); // mở tab mở rộng
                                $($('.model-properties-object .item-FilePlace')[2]).css('display', 'flex');
                                indexExtendActive = 2;
                            }

                            if (element.TypeProperties != "geojson" && element.TypeProperties != "image"
                                && element.TypeProperties != "link" && element.TypeProperties != "maptile") {
                                text = mapdata.selectInputViewInforObj(element, "myModal2");
                                $(mapdata.SELECTORS.modalbody).append(text);
                                if (element.TypeProperties == "date") {
                                    $('#myModal2 #text-modal-' + element.CodeProperties).datetimepicker({
                                        locale: 'vi',
                                        format: 'DD/MM/YYYY',
                                        useCurrent: false,
                                        defaultDate: moment(),
                                    }).on('dp.change', function (e) {
                                        $(e.currentTarget).parents(".form-group-modal").find(".label-error").remove();
                                        $(e.currentTarget).parents(".form-group-modal").removeClass("has-error");
                                        if ($(this).val() == "") {
                                            $(this).parent().find('.placeholder').removeClass('focus');
                                        }
                                    }).on('dp.hide', function () {
                                        if ($(this).val() == "") {
                                            $(this).parent().find('.placeholder').removeClass('focus');
                                        }
                                    }).on('dp.show', function () {
                                        $(this).parent().find('.placeholder').addClass('focus');
                                    });
                                } else if (element.TypeProperties == "text" || element.TypeProperties == "stringlarge") {

                                    setTimeout(function () {
                                        $('#myModal2 #text-modal-' + element.CodeProperties).trumbowyg({
                                            svgPath: '/libs/bootstrap-trumbowyg/icons.svg',
                                            btns: [
                                                ['viewHTML'],
                                                ['undo', 'redo'], // Only supported in Blink browsers
                                                ['formatting'],
                                                ['strong', 'em', 'del'],
                                                ['superscript', 'subscript'],
                                                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                                                ['unorderedList', 'orderedList'],
                                                ['horizontalRule'],
                                                ['removeformat'],
                                                ['fullscreen']
                                            ]
                                        }).on('tbwchange ', function (e) {
                                            //$('#myModal2 #text-modal-' + element.CodeProperties).trigger('change');
                                            if ($('#myModal2 #text-modal-' + element.CodeProperties).val() != "") {
                                                $(this).parents('.form-group-modal').find('.label-error').remove();
                                                $(this).parents('.form-group-modal').removeClass('has-error');
                                                //    $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).addClass("textarea");
                                                //    let height = $('#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                                //    $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 68}px)`);
                                            }
                                        }).on('tbwfocus', function () {
                                            $($(this).parent().next()).addClass("textarea");
                                            let height = $('#myModal2 #text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                            $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 68}px)`);
                                            $($('#myModal2 #text-modal-' + element.CodeProperties).parent().parent()).addClass('div-textarea-parent-focus');
                                        }).on('tbwblur', function () {
                                            if ($('#myModal2 #text-modal-' + element.CodeProperties).val() != "") {
                                                $($(this).parent().next()).addClass("textarea");
                                                let height = $('#myModal2 #text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                                $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 68}px)`);
                                            } else {
                                                $($(this).parent().next()).removeClass("textarea");
                                                $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);

                                            }
                                            $($('#myModal2 #text-modal-' + element.CodeProperties).parent().parent()).removeClass('div-textarea-parent-focus');
                                        }).on('tbwresize', function () {
                                            let height = $('#myModal2 #text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                            $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1.0) translateY(10px)`);
                                        });

                                        let height = $('#myModal2 #text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                        //$($('#text-modal-' + element.CodeProperties).parent().next()).css('top', (height + 20) + 'px');
                                        $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).attr('style', `top: ${(height + 20)}px !important`);
                                        $($('#myModal2 #text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1.0) translateY(10px)`);
                                        /*$($('#text-modal-' + element.CodeProperties).parent().next()).css('display', 'none');*/
                                    }, 200);
                                }
                                else if (element.TypeProperties == "list") {
                                    $('#myModal2 #text-modal-' + element.CodeProperties).select2()
                                        .on("select2:opening", function () {
                                            $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                        })
                                        .on("select2:close", function () {
                                            var value = $(this).val();
                                            if (value != "" && value != null) {
                                                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                            }
                                            else {
                                                $(this).parent().find('.placeholder').attr('style', "");
                                            }
                                        })
                                        .on('select2:select', function () {
                                            var value = $(this).val();
                                            if (value != "" && value != null) {
                                                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                                $(this).trigger('change');
                                            }
                                        });
                                    $('#myModal2 #text-modal-' + element.CodeProperties).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                }
                            }
                        }
                    });

                    // default tab trong tab mở rộng
                    $('.model-properties-object .item-FilePlace').each(function () {
                        if ($(this).css('display') != "none") {
                            $(this).trigger('click');

                            return false;
                        }
                    });

                    mapdata.GLOBAL.lstProperties = result;
                    mapdata.GLOBAL.lstPropertiesDirectory = data.result.listProperties;
                    Config_Main_Object.showDefaultProperties(Config_Main_Object.SELECTORS.content_create); // set value thuộc tính cấu hình

                    $('#myModal2').addClass('active');
                    $('#myModal2 .class-info').first().trigger('click');
                    mapdata.GLOBAL.idDirectory = data.result.idDirectory;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    setDefaultColor: function () {
        var urls = window.location.origin + '/api/HTKT/PropertiesSetting/getByIdDictionary';
        $.ajax({
            type: "GET",
            url: urls,
            data: {
                id: mapdata.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json",
            success: function (data) {
                if (data.code == "ok") {
                    if (data.result != null) {
                        mapdata.GLOBAL.propertiesSetting.MinZoom = data.result.minZoom;
                        mapdata.GLOBAL.propertiesSetting.MaxZoom = data.result.maxZoom;
                        mapdata.GLOBAL.propertiesSetting.Stroke = data.result.stroke;
                        mapdata.GLOBAL.propertiesSetting.StrokeWidth = data.result.strokeWidth;
                        mapdata.GLOBAL.propertiesSetting.StrokeOpacity = data.result.strokeOpacity;
                        mapdata.GLOBAL.propertiesSetting.StyleStroke = data.result.styleStroke;
                        mapdata.GLOBAL.propertiesSetting.Fill = data.result.fill;
                        mapdata.GLOBAL.propertiesSetting.FillOpacity = data.result.fillOpacity;
                        mapdata.GLOBAL.propertiesSetting.Image2D = data.result.image2D;
                    }
                }
            },
        });
    },
    setValueObject: function (content) {
        let lstproperties = mapdata.GLOBAL.mainObjectSelected.listProperties;
        // hiển thị value input đói tượng
        $.each(lstproperties, function (i, obj) {
            $(content + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue);
            let lstvalue;
            if (obj.typeProperties == "list") {
                try {
                    lstvalue = JSON.parse(obj.defalutValue);
                    if (lstvalue != undefined && $.isArray(lstvalue)) {
                        let value = lstvalue.find(x => x.checked);
                        if (value != null && value != undefined) {
                            $(content + ' #text-modal-' + obj.codeProperties).val(value.code);
                        }
                    }
                    else {
                        $(content + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue);
                    }
                }
                catch (e) {
                    $(content + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue);
                }
            }
            if (obj.typeProperties == "radiobutton") {
                try {
                    lstvalue = JSON.parse(obj.defalutValue);
                    if (lstvalue != undefined && $.isArray(lstvalue)) {
                        let value = lstvalue.find(x => x.checked);
                        if (value != null && value != undefined) {
                            $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + value.code + '"]').prop("checked", true);
                        }
                    }
                    else {
                        $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                    }
                }
                catch (e) {
                    $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                }
            }

            if (obj.typeProperties == "checkbox") {
                try {
                    lstvalue = JSON.parse(obj.defalutValue);

                    if (lstvalue.length > 0 && $.isArray(lstvalue)) {
                        $.each(lstvalue, function (i, item) {
                            if (item.checked) {
                                $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + item.code + '"]').prop("checked", true);
                            }
                        });
                    }
                    else {
                        var lstChecked = obj.defalutValue.split(",");
                        $.each(lstChecked, function (i, item) {
                            $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                        });
                    }
                }
                catch (e) {
                    if (obj.defalutValue != "" && obj.defalutValue != null) {
                        var lstChecked = obj.defalutValue.split(",");
                        $.each(lstChecked, function (i, item) {
                            $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                        });
                    }
                    //$('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                }

                //let lstvalue = JSON.parse(obj.defalutValue);
                //let value = lstvalue.filter(x => x.checked);
                //if (value.length > 0) {
                //    $.each(value, function (i, item) {
                //        $('.text-modal-' + obj.codeProperties + '[value = "' + item.code + '"]').prop("checked", true);
                //    });
                //}
            }

            if (obj.typeProperties == "link") {
                modalCreateFolderLayer.GLOBAL.listObjectLinkCamera = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                mapdata.GLOBAL.listObjectLinkCameraEdit = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                //$('li[data-li="step-extend"]').css('display', 'block');
                return;
            }
            if (obj.typeProperties == "image") {
                modalCreateFolderLayer.GLOBAL.listObjectImage = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                mapdata.GLOBAL.listObjectImageEdit = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                //$('li[data-li="step-extend"]').css('display', 'block');
                return;
            }
            if (obj.typeProperties == "file") {
                //$(modalCreateFolderLayer.SELECTORS.inputListDocument).html('');
                //let option = `<option value="${obj.codeProperties}">${obj.nameProperties}</option>`;
                //$(modalCreateFolderLayer.SELECTORS.inputListDocument).append(option);
                statusfile = true;
                return;
            }
        });
    },
    //tính toán data object bao nhiêu page gọi api
    getEditObject: function (id, page = 1) {
        id = mapdata.GLOBAL.idDirectory;
        //importmapdata.showLoading(true);
        abp.ui.setBusy("body");
        let count = Math.ceil(totalCountByIdDirectory);
        mapdata.GLOBAL.listObjectByDirectory = [];
        //mapdata.GLOBAL.lstGeoJsonDefault = [];
        mapdata.GLOBAL.features = [];
        if (map.is3dMode()) {
            mapdata.clearDataGeojson();
        }
        mapdata.clearAllDraw();
        map.data.clear();
        for (var i = 0; i < count; i++) {
            mapdata.loadDataObjectMap(id, i + 1);
        }
        setTimeout(function () {
            abp.ui.clearBusy("body");
            //importmapdata.showLoading(false);
        }, (1000 * count));
    },
    //load data object theo page
    loadDataObjectMap: function (id, page) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: true,
            //url: document.location.origin + mapdata.CONSTS.URL_AJAXDEDITDATA + '?id=' + id,
            url: this.CONSTS.URL_GETOBJECTPAGINGBYDIRECTORY + "?id=" + id + "&page=" + page,
            //data: {},
            success: function (data) {
                if (data.code == "ok") {
                    data.result = JSON.parse(data.result);
                    if (data.result != null && data.result.length > 0) {

                        mapdata.GLOBAL.listObjectByDirectory = mapdata.GLOBAL.listObjectByDirectory.concat(data.result);

                        var geojson = mapdata.getDataGeojsonByProperties(data.result);
                        //mapdata.GLOBAL.lstGeoJsonDefault.push(geojson);
                        mapdata.showDataGeojsonPush(JSON.stringify(geojson));

                        if (mapdata.GLOBAL.selectObject2D != null) // nếu đối tượng đang được selected thì gán lại geojson của đối tượng đó
                        {
                            var editGeojson = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.selectObject2D.feature._id);
                            if (editGeojson != undefined) {
                                mapdata.GLOBAL.selectObject2D.feature = editGeojson;
                            }
                        }

                        $.each(mapdata.GLOBAL.listObject3D, function (i, obj) {
                            obj.object.setMap(null);
                        });
                        mapdata.GLOBAL.listObject3D = [];
                        $.each(data.result, function (i, obj) {
                            if (obj.object3D != null && obj.object3D.type != "para-choosenMap") {
                                let object3d = model3D.newMap4dBuilding(obj.object3D);
                                object3d.setUserData(obj.id);
                                object3d.setMap(map);
                                mapdata.GLOBAL.listObject3D.push({ id: obj.id, object: object3d });
                            }
                        });

                        if (map.is3dMode()) {
                            map.data.clear();
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    //---------- lấy thông tin main object --------------------------------------------------------
    getMainObject: function (id, content) {
        id = mapdata.GLOBAL.idMainObject;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_AJAXGETDATA + '?id=' + id,
            /*url: mapdata.CONSTS.URL_AJAXGETDATA + '?id=' + id,*/
            data: {
            },
            success: function (data) {
                if (data.code == "ok") {
                    mapdata.GLOBAL.mainObjectSelected = data.result; // lưu thông tin của mai object hiện tại
                    Info_Properties_Main_Object.setDefaultValue("", content);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

        model3D.GLOBAL.fileObject3d.objurl = '';
        model3D.GLOBAL.fileObject3d.texture3D = '';
        model3D.GLOBAL.urlObject3d.objurl = '';
        model3D.GLOBAL.urlObject3d.texture3D = '';
    },

    //----------lấy thuộc tính default--------------------------------------------------
    getDefaultObject: function (id, content) {
        id = mapdata.GLOBAL.idDirectory;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=' + id,
            data: {
            },
            success: function (data) {
                if (data.code == "ok" && data.result.listProperties != undefined) {
                    Info_Properties_Main_Object.getDefaultProperties(data.result, content);

                    mapdata.GLOBAL.lstPropertiesDirectory = data.result.listProperties;
                    mapdata.GLOBAL.idDirectory = data.result.idDirectory;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },

    selectInput: function (type) {
        var requried = (type.IsRequest) ? "required" : "";
        var requriednew = (type.IsRequest) ? `<span class="required"></span>` : "";
        //if (type.CodeProperties == 'Title') requried = "required";
        var text = `<div class="form-group" style="margin-bottom: 0;">
                        <div class="w-100 position-relative">
                                <div class="form-group form-group-modal ${type.TypeProperties == "text" ? "input-textarea" : ""}">`;
        //<input type="text" id="text-name" class="form-control form-style" placeholder="Tên kiểu" />
        if ((type.TypeProperties != "list" && type.TypeProperties != "checkbox" && type.TypeProperties != "radiobutton")
            && (type.DefalutValue === null || type.DefalutValue === undefined || typeof (type.DefalutValue) === "undefined")) {
            type.DefalutValue = "";
        }
        switch (type.TypeProperties) {
            case 'bool':
                if (type.DefalutValue || type.DefalutValue == "true") {
                    text += `<input autocomplete="off" type="checkbox" data-type="bool" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" class="modal-layer form-style" checked="true">`;
                } else {
                    text += `<input autocomplete="off" type="checkbox" data-type="bool" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" class="modal-layer form-style">`;
                }
                break;
            case 'text':
                if (type.CodeProperties !== "Description") {
                    text += `<div class="div-textarea">`;
                    text += `<textarea autocomplete="off" type="text" required data-type="text" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                    text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                    text += `</div>`;
                } else {
                    text += `<div class="div-textarea">`;
                    text += `<textarea autocomplete="off" type="text" required data-type="text" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                    text += `<span class="placeholder">${l("Description")} ${requriednew}</span>`;
                    text += `</div>`;
                }
                break;
            case 'float':
                text += `<input autocomplete="off" type="number" step="any" required data-type="float" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'int':
                text += `<input autocomplete="off" type="number" step="any" equired data-type="int" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'list':
                var lst = JSON.parse(type.DefalutValue);
                text += `<select id="text-modal-${type.CodeProperties}" class="form-control modal-layer" data-type="list" data-required=${type.IsRequest} required>`;
                $.each(lst, function (i, obj) {
                    text += `<option ${obj.checked ? "selected" : ""} value="${obj.code}">${obj.name}</option>`;
                });
                text += ` </select>`;
                //text += `<input type="text" data-type="list" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'date':
                text += `<input autocomplete="off" type="text" required data-type="date" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'string':
                if (type.CodeProperties == "Title") {
                    text += `<input autocomplete="off" type="text" required data-type="string" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                    text += `<span class="placeholder">${l("Title")} ${requriednew}</span>`;
                } else
                    if (type.CodeProperties == "Name") {
                        text += `<input autocomplete="off" type="text" required data-type="string" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                        text += `<span class="placeholder">${l("Name")} ${requriednew}</span>`;
                    } else {
                        text += `<input autocomplete="off" type="text" required data-type="string" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                        text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                    }
                break;
            case 'stringlarge':
                text += `<textarea autocomplete="off" type="text" required data-type="stringlarge" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'geojson':
                text += `<textarea autocomplete="off" type="text" required data-type="geojson" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'checkbox':
                text += `<div class="modal-layer div-checkbox-radio" data-type="checkbox" data-required=${type.IsRequest}>`;
                var lst2 = JSON.parse(type.DefalutValue);
                $.each(lst2, function (i, obj) {
                    //text += `<label class="col-sm-12">
                    //            <input autocomplete="off" type="checkbox" name="text-modal-checkbox" data-name="${obj.name} " class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                    //         </label>`;
                    text += `<div class="col-sm-12 input-containter-50">
                                <div class="custom-checkbox custom-control">
                                    <input type="checkbox" data-val="true" id="text-modal-${type.CodeProperties}${i}" data-name="${obj.name}" name="${type.CodeProperties}" value="${obj.code}" class="text-modal-${type.CodeProperties} custom-control-input" ${obj.checked ? "checked='checked'" : ""}>
                                    <label class="custom-control-label" for="text-modal-${type.CodeProperties}${i}">${obj.name}</label>
                                </div>
                            </div>`;
                });
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                text += `</div>`;
                break;
            case 'radiobutton':
                text += `<div class="modal-layer div-checkbox-radio"  data-type="radio" data-required=${type.IsRequest}>`;
                var lst3 = JSON.parse(type.DefalutValue);
                $.each(lst3, function (i, obj) {
                    //text += `<label class="col-sm-12">
                    //            <input autocomplete="off" type="radio" name="text-modal-radio-${type.CodeProperties}" data-name="${obj.name} " class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                    //         </label>`;
                    text += `<div class="col-sm-12 input-containter-50">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="${type.CodeProperties}" id="text-modal-radio-${type.CodeProperties}${i}" data-name="${obj.name}" value="${obj.code}" class="custom-control-input text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} >
                                    <label class="custom-control-label" for="text-modal-radio-${type.CodeProperties}${i}">${obj.name} </label>
                                </div>
                            </div>`;
                });
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                text += `</div>`;
                break;
            default:
                return text = '';
            //break;
        }

        text += `                            </div>
                            </div>
                    </div>`;
        return text;
    },
    selectInputViewInforObj: function (type, content) {
        var requried = (type.IsRequest) ? "required" : "";
        var requriednew = (type.IsRequest) ? `<span class="required"></span>` : "";
        //if (type.CodeProperties == 'Title') requried = "required";
        var text = `<div class="form-group" style="margin-bottom: 0; width: 100%;">
                        <div class="w-100 position-relative">
                                <div class="form-group form-group-modal ${type.TypeProperties == "text" ? "input-textarea" : ""}">`;
        //<input type="text" id="text-name" class="form-control form-style" placeholder="Tên kiểu" />
        if ((type.TypeProperties != "list" && type.TypeProperties != "checkbox" && type.TypeProperties != "radiobutton")
            && (type.DefalutValue === null || type.DefalutValue === undefined || typeof (type.DefalutValue) === "undefined")) {
            type.DefalutValue = "";
        }
        switch (type.TypeProperties) {
            case 'bool':
                if (type.DefalutValue || type.DefalutValue == "true") {
                    //text += `<input autocomplete="off" type="checkbox" data-type="bool" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" class="modal-layer form-style" checked="true">`;

                    text += `<div class="custom-checkbox custom-control">
                                <input type="checkbox" data-type="bool" data-name="${type.NameProperties}" data-required="${type.IsRequest}" id="text-modal-${type.CodeProperties}${content}" name="${type.CodeProperties}$" class="text-modal-${type.CodeProperties} custom-control-input modal-layer form-style" checked>
                                <label class="custom-control-label" for="text-modal-${type.CodeProperties}${content}">${type.NameProperties}</label>
                            </div>`;
                } else {

                    text += `<div class="custom-checkbox custom-control">
                                <input type="checkbox" data-type="bool" data-name="${type.NameProperties}" data-required="${type.IsRequest}" id="text-modal-${type.CodeProperties}${content}" name="${type.CodeProperties}$" class="text-modal-${type.CodeProperties} custom-control-input modal-layer form-style">
                                <label class="custom-control-label" for="text-modal-${type.CodeProperties}${content}">${type.NameProperties}</label>
                            </div>`;
                }
                break;
            case 'text':
                if (type.CodeProperties !== "Description") {
                    text += `<div class="div-textarea">`;
                    text += `<textarea autocomplete="off" type="text" data-name="${type.NameProperties}" required data-type="text" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                    text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                    text += `</div>`;
                } else {
                    text += `<div class="div-textarea">`;
                    text += `<textarea autocomplete="off" type="text" data-name="${l("Description")}" required data-type="text" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                    text += `<span class="placeholder">${l("Description")} ${requriednew}</span>`;
                    text += `</div>`;
                }
                break;
            case 'float':
                text += `<input autocomplete="off" type="number" step="any" data-name="${type.NameProperties}" required data-type="float" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span autocomplete="off" class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'int':
                text += `<input autocomplete="off" type="number" data-name="${type.NameProperties}" step="any" required data-type="int" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'list':
                var lst = JSON.parse(type.DefalutValue);
                text += `<select id="text-modal-${type.CodeProperties}" data-name="${type.NameProperties}" class="form-control modal-layer" data-type="list" data-required=${type.IsRequest} required>`;
                //text += `<option value=""></option>`;
                $.each(lst, function (i, obj) {
                    text += `<option ${obj.checked ? "selected" : ""} value="${obj.code}">${obj.name}</option>`;
                });
                text += ` </select>`;
                //text += `<input type="text" data-type="list" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'date':
                text += `<input autocomplete="off" type="text" required data-name="${type.NameProperties}" data-type="date" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'string':
                if (type.CodeProperties == "Title") {
                    text += `<input autocomplete="off" type="text" required data-name="${l("Title")}" data-type="string" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                    text += `<span class="placeholder">${l("Title")} ${requriednew}</span>`;
                } else
                    if (type.CodeProperties == "Name") {
                        text += `<input autocomplete="off" type="text" data-name="${l("Name")}" required data-type="string" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                        text += `<span class="placeholder">${l("Name")} ${requriednew}</span>`;
                    } else {
                        text += `<input autocomplete="off" type="text" data-name="${type.NameProperties}" required data-type="string" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control modal-layer form-style" placeholder="" />`;
                        text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                    }
                break;
            case 'stringlarge':
                text += `<div class="div-textarea">`;
                text += `<textarea autocomplete="off" type="text" data-name="${type.NameProperties}" required data-type="stringlarge" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                text += `</div>`;
                break;
            case 'geojson':
                text += `<textarea autocomplete="off" type="text" data-name="${type.NameProperties}" required data-type="geojson" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" ></textarea>`;
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                break;
            case 'checkbox':
                text += `<div class="modal-layer div-checkbox-radio" data-name="${type.NameProperties}" data-type="checkbox" data-required=${type.IsRequest}>`;
                var lst2 = JSON.parse(type.DefalutValue);
                $.each(lst2, function (i, obj) {
                    //text += `<label class="col-sm-12">
                    //            <input autocomplete="off" type="checkbox" name="text-modal-checkbox" data-name="${obj.name} " class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                    //         </label>`;

                    text += `<div class="col-sm-12 input-containter-50">
                                <div class="custom-checkbox custom-control">
                                    <input type="checkbox" data-val="true" id="text-modal-${type.CodeProperties}${content}${i}" data-name="${obj.name}" name="${type.CodeProperties}${content}" value="${obj.code}" class="text-modal-${type.CodeProperties} custom-control-input" ${obj.checked ? "checked='checked'" : ""}>
                                    <label class="custom-control-label" for="text-modal-${type.CodeProperties}${content}${i}">${obj.name}</label>
                                </div>
                            </div>`;
                });
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                text += `</div>`;
                break;
            case 'radiobutton':
                text += `<div class="modal-layer div-checkbox-radio" data-name="${type.NameProperties}" data-type="radio" data-required=${type.IsRequest}>`;
                var lst3 = JSON.parse(type.DefalutValue);
                $.each(lst3, function (i, obj) {
                    //text += `<label class="col-sm-12">
                    //            <input autocomplete="off" type="radio" name="text-modal-radio-${type.CodeProperties}" data-name="${obj.name} " class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                    //         </label>`;

                    text += `<div class="col-sm-12 input-containter-50">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="${type.CodeProperties}${content}" id="text-modal-radio-${type.CodeProperties}${content}${i}" data-name="${obj.name}" value="${obj.code}" class="custom-control-input text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} >
                                    <label class="custom-control-label" for="text-modal-radio-${type.CodeProperties}${content}${i}">${obj.name} </label>
                                </div>
                            </div>`;
                });
                text += `<span class="placeholder">${type.NameProperties} ${requriednew}</span>`;
                text += `</div>`;
                break;
            default:
                return text = '';
            //break;
        }

        text += `                </div>
                            </div>
                    </div>`;
        return text;
    },
    checkformmodal: function () {
        mapdata.clearLabelError();
        let check = true;

        let hasFocus = false;

        $(mapdata.SELECTORS.modalLayerProperties).each(function () {
            var type = $(this).attr("data-type");
            var status = $(this).attr("data-required").toLowerCase();
            if (status == "true") {
                switch (type) {
                    case "list":
                    case "radio":
                    case "checkbox":
                        check = false;
                        let inputName = $(this).attr('data-name');
                        $(this).find('input').each(function (i, e) {
                            if ($(this).is(":checked")) {
                                check = true;
                                return;
                            }
                        });

                        if (!check) {
                            insertError($(this), "other");
                            if (!hasFocus) {
                                hasFocus = true;
                                $("#myModal2 .detail-property-body").animate({
                                    scrollTop: $("#myModal2 .detail-property-body").scrollTop() - $("#myModal2 .detail-property-body").offset().top + $(this).offset().top
                                }, 50);
                            }

                            mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            return;
                        }
                        break;
                    default:
                        var typeconvert = mapdata.convertcheckTypemodal(type);
                        if (typeconvert != "") {
                            let inputTen = $(this).val();
                            let inputName = $(this).attr('data-name');
                            if (!validateText(inputTen, typeconvert, 0, 0)) {
                                if (type == "text" && inputTen.trim() != "") {
                                    return true;
                                }
                                insertError($(this), "other");
                                if (!hasFocus) {
                                    //console.log($(this).attr("id"));
                                    $(this).focus();
                                    hasFocus = true;
                                }
                                check = false;
                                mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            }
                        }
                        break;
                }
            }
        });

        let checkHtml = mapdata.CheckedHtmlEntities(mapdata.SELECTORS.modalbody);
        if (check) {
            check = checkHtml;
        }
        mapdata.eventmodal();
        return check;
    },
    checkformStepInfo: function (content_form) {
        mapdata.clearLabelError();
        let check = true;
        let checkHtml = mapdata.CheckedHtmlEntities(content_form);
        let hasFocus = false;

        $(content_form + " .modal-layer").each(function () {
            var type = $(this).attr("data-type");
            var status = $(this).attr("data-required").toLowerCase();
            if (status == "true") {
                switch (type) {
                    case "list":
                    case "radio":
                    case "checkbox":
                        var checkBoxRadio = false;
                        let inputName = $(this).attr('data-name');
                        $(this).find('input').each(function (i, e) {
                            if ($(this).is(":checked")) {
                                checkBoxRadio = true;
                                return;
                            }
                        });

                        if (!checkBoxRadio) {
                            $(content_form + " .li-modal-data-right").eq(3).click();
                            check = checkBoxRadio;
                            insertError($(this), "other");
                            if (!hasFocus) {
                                hasFocus = true;
                                $(content_form).animate({
                                    scrollTop: $(content_form).scrollTop() - $(content_form).offset().top + $(this).offset().top
                                }, 50);
                            }

                            mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            return;
                        }
                        break;
                    default:
                        var typeconvert = mapdata.convertcheckTypemodal(type);
                        if (typeconvert != "") {
                            let inputTen = $(this).val();
                            let inputName = $(this).attr('data-name');
                            if (!validateText(inputTen, typeconvert, 0, 0)) {
                                $(content_form + " .li-modal-data-right").eq(3).click();
                                if ((type == "text" || type == "stringlarge") && inputTen.trim() != "") {
                                    return true;
                                }
                                insertError($(this), "other");
                                if (!hasFocus) {
                                    if (type == "text" || type == "stringlarge") {
                                        $(this).parent().find('.trumbowyg-editor').focus();
                                    }
                                    else {
                                        //console.log($(this).attr("id"));
                                        $(this).focus();
                                    }
                                    hasFocus = true;
                                }
                                check = false;
                                mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));

                                return;
                            }
                        }
                        break;
                }

            }
        });

        return check;
    },
    // check specil HTMLstring
    CheckedHtmlEntities: function (form) {
        var check = true;
        $(`${form} input[type=text]`).each(function () {
            var value = $(this).val();
            let inputName = $(this).attr('data-name');
            var status = $(this).attr("data-required");
            if (status != undefined) {
                status = status.toLowerCase()
            }

            if ($(this).val() != ""/* && status == "true"*/) {
                let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;
                if (re.test(value)) {
                    $(this).parents(".form-group-modal").find('.label-error').remove();
                    insertError($(this), "other");
                    mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), inputName));
                    check = false;
                }
            }
        });
        return check;
    },
    convertcheckTypemodal: function (type) {
        var typeconvert = '';
        switch (type) {
            case 'bool':

                break;
            case 'text':
                typeconvert = "text";
                break;
            case 'float':
                typeconvert = "float";
                break;
            case 'int':
                typeconvert = "number";
                break;
            case 'list':
                typeconvert = "list";
                break;
            case 'date':
                typeconvert = "text";
                break;
            case 'string':
                typeconvert = "text";
                break;
            case 'stringlarge':
                typeconvert = "text";
                break;
            case 'geojson':

                break;
            default:
                return '';
        }
        return typeconvert;
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group-modal").append("<label class='label-error' style='color:red'>" + text + "</label>");
        $(element).parents(".form-group-modal").addClass("has-error");
    },
    clearLabelError: function () {
        $(".label-error").remove();
        $(mapdata.SELECTORS.hasError).find(".label-error").remove();
        $(mapdata.SELECTORS.hasError).removeClass("has-error");
    },
    eventmodal: function () {
        $(document).on("keyup change", mapdata.SELECTORS.modalLayerProperties, function () {
            $(this).parents(".form-group-modal").find(".label-error").remove();
            $(this).parents(".form-group-modal").removeClass("has-error");
        });
    },
    showHideViewProperty: function (isCheck) {
        switch (mapdata.GLOBAL.selectbtn) {
            case "edit":
                if (isCheck) {//show
                    $(mapdata.SELECTORS.editForm).removeClass('detail-property-collapse');
                    $(mapdata.SELECTORS.editForm).addClass('active');
                }
                else {//hide
                    $(mapdata.SELECTORS.editForm).addClass('detail-property-collapse');
                    $(mapdata.SELECTORS.editForm).removeClass('active');
                }
                break;
            default:
                if (isCheck) {//show
                    $(mapdata.SELECTORS.inforData).removeClass('detail-property-collapse');
                }
                else {//hide
                    $(mapdata.SELECTORS.inforData).addClass('detail-property-collapse');
                }
        }
        //if (isCheck) {//show
        //    $(mapdata.SELECTORS.inforData).removeClass('detail-property-collapse');
        //}
        //else {//hide
        //    $(mapdata.SELECTORS.inforData).addClass('detail-property-collapse');
        //}
    },

    /*---draw point,line,polygon,rectangle---*/
    setMarkerDraw: function (lat, lng) {
        if (mapdata.GLOBAL.listMarker !== null && mapdata.GLOBAL.listMarker.length > 0) {
            mapdata.GLOBAL.listMarker[0].setMap(null);
        }
        let markerDraw = new map4d.Marker({
            position: { lat: lat, lng: lng },
            icon: new map4d.Icon(35, 35, "/common/location-1.png"),
            anchor: [0.5, 1],
            draggable: true,
            zIndex: 100
            //title: name
        })
        markerDraw.setUserData(mapdata.GLOBAL.idMainObject);
        markerDraw.setMap(map);
        mapdata.GLOBAL.listMarker.push(markerDraw);
        //mapdata.GLOBAL.markerDraw = markerDraw;
    },
    setMultiMarkerDraw: function (lat, lng) {
        //if (mapdata.GLOBAL.listMarker !== null && mapdata.GLOBAL.listMarker.length > 0) {
        //    mapdata.GLOBAL.listMarker[0].setMap(null);
        //}
        let markerDraw = new map4d.Marker({
            position: { lat: lat, lng: lng },
            icon: new map4d.Icon(35, 35, "/common/location-1.png"),
            anchor: [0.5, 1],
            draggable: true,
            zIndex: 100
            //title: name
        })
        markerDraw.setUserData(mapdata.GLOBAL.idMainObject);
        markerDraw.setMap(map);
        mapdata.GLOBAL.listMultiPoint.MultiPoint.push(markerDraw);
        //mapdata.GLOBAL.markerDraw = markerDraw;
    },
    //Tạo marker đối tượng
    createMarkerDrawLoDat: function (lat, lng, check) {
        var isDraw = true;
        if (mapdata.GLOBAL.listArea != null && mapdata.GLOBAL.listArea.listMarkerArea.length > 0) {
            var locationLast = mapdata.GLOBAL.listArea.listMarkerArea[mapdata.GLOBAL.listArea.listMarkerArea.length - 1].getPosition();
            if (locationLast.lat == lat && locationLast.lng == lng) {
                isDraw = false;
            }
        }

        if (isDraw) {
            let markerDraw = new map4d.Marker({
                position: { lat: lat, lng: lng },
                icon: new map4d.Icon(8, 8, "/common/yellow-point.png"),
                anchor: [0.5, 0.5],
                zIndex: 101
                //title: name
            })
            markerDraw.setMap(map);
            if (check === 2) {
                mapdata.GLOBAL.listArea.listMarkerArea.push(markerDraw);
            } else if (check === 1) {
                mapdata.GLOBAL.listDistance.listMarkerDistance.push(markerDraw);
            } else {
                mapdata.GLOBAL.listRectangle.listMarkerRectangle.push(markerDraw);
            }
        }
    },
    //Tạo polyline đối tượng
    createPolylineLoDat: function (path, strokeWidth, strokeOpacity, check) {
        if (mapdata.GLOBAL.polylineTemp != null) {
            mapdata.GLOBAL.polylineTemp.setMap(null);
        }
        //tạo đối tượng polyline từ PolylineOptions
        var polylineDistance = new map4d.Polyline({
            path: path, visible: true, strokeColor: "#FF8264", strokeWidth: strokeWidth, strokeOpacity: strokeOpacity,
            closed: false, zIndex: 100
        })
        //thêm polyline vào map
        polylineDistance.setMap(map);
        if (check) {
            mapdata.GLOBAL.listDistance.listPolylineDistance.push(polylineDistance)
        } else {
            mapdata.GLOBAL.listArea.PolylineArea = polylineDistance;
        }

    },
    //Tạo polygon
    createPolygonArea: function (data) {
        if (mapdata.GLOBAL.listArea.polygonArea != null) {
            mapdata.GLOBAL.listArea.polygonArea.setMap(null);
        }
        if (mapdata.GLOBAL.polylineTemp != null) {
            mapdata.GLOBAL.polylineTemp.setMap(null);
        }

        let polygonOption = map4d.PolygonOptions = {
            paths: data, fillOpacity: 0.5, zIndex: 100
        }

        mapdata.GLOBAL.listArea.polygonArea = new map4d.Polygon(polygonOption)

        //thêm object vào map
        mapdata.GLOBAL.listArea.polygonArea.setMap(map);
    },
    //Tạo Rectangle
    createRectangle: function (data) {
        if (mapdata.GLOBAL.listRectangle.rectangle != null) {
            mapdata.GLOBAL.listRectangle.rectangle.setMap(null);
        }

        let polygonOption = map4d.PolygonOptions = {
            paths: [data], fillOpacity: 0.5, zIndex: 100
        }

        mapdata.GLOBAL.listRectangle.rectangle = new map4d.Polygon(polygonOption)

        //thêm object vào map
        mapdata.GLOBAL.listRectangle.rectangle.setMap(map);
    },
    //Tạo polyline
    createPolylineByMouseMoveLoDat: function (path, strokeWidth, strokeOpacity) {
        if (mapdata.GLOBAL.polylineTemp != null) {
            mapdata.GLOBAL.polylineTemp.setMap(null);
        }
        //tạo đối tượng polyline từ PolylineOptions
        mapdata.GLOBAL.polylineTemp = new map4d.Polyline({
            path: path, visible: true, strokeColor: "#FF8264", strokeWidth: strokeWidth, strokeOpacity: strokeOpacity,
            closed: false, zIndex: 100
        })
        //thêm polyline vào map
        mapdata.GLOBAL.polylineTemp.setMap(map)
    },
    // Ẩn hiện đối tượng đang ở trạng thái vẽ (marker, line, polygon,..)
    HideOrShowDrawMainObject: function (type) {
        var isMap = map;
        if (!type) {
            isMap = null;
        }

        // point
        if (mapdata.GLOBAL.listMarker.length > 0) {
            $.each(mapdata.GLOBAL.listMarker, function (index, obj) {
                obj.setMap(isMap);
            });
        }

        // mutiple point
        if (mapdata.GLOBAL.listMultiPoint != null && mapdata.GLOBAL.listMultiPoint.MultiPoint.length > 0) {
            $.each(mapdata.GLOBAL.listMultiPoint.MultiPoint, function (index, obj) {
                obj.setMap(isMap);
            });
        }

        // polyline
        if (mapdata.GLOBAL.listDistance != null) {
            // array marker line
            if (mapdata.GLOBAL.listDistance.listMarkerDistance.length > 0) {
                $.each(mapdata.GLOBAL.listDistance.listMarkerDistance, function (index, obj) {
                    obj.setMap(isMap);
                });
            }

            // polyline
            if (mapdata.GLOBAL.listDistance.listPolylineDistance.length > 0) {
                $.each(mapdata.GLOBAL.listDistance.listPolylineDistance, function (index, obj) {
                    obj.setMap(isMap);
                });
            }
        }

        // mutiple polyline
        if (mapdata.GLOBAL.listMultiLine.length > 0) {
            $.each(mapdata.GLOBAL.listMultiLine, function (index, obj) {
                if (obj.polyline != null && obj.polyline != undefined) {
                    // marker line
                    if ((obj.polyline.listMarkerDistance != null && obj.polyline.listMarkerDistance != undefined) && obj.polyline.listMarkerDistance.length > 0) {
                        $.each(obj.polyline.listMarkerDistance, function (indexMarker, objMarker) {
                            objMarker.setMap(isMap);
                        });
                    }

                    // polyline
                    if ((obj.polyline.listPolylineDistance != null && obj.polyline.listPolylineDistance != undefined) && obj.polyline.listPolylineDistance.length > 0) {
                        $.each(obj.polyline.listPolylineDistance, function (indexPolyline, objPolyline) {
                            objPolyline.setMap(isMap);
                        });
                    }
                }
            });
        }

        // polygon
        if (mapdata.GLOBAL.listArea != null && mapdata.GLOBAL.listArea != undefined) {
            // array marker polygon
            if (mapdata.GLOBAL.listArea.listMarkerArea.length > 0) {
                $.each(mapdata.GLOBAL.listArea.listMarkerArea, function (index, obj) {
                    obj.setMap(isMap);
                });
            }

            // polygon
            if (mapdata.GLOBAL.listArea.polygonArea != null && mapdata.GLOBAL.listArea.polygonArea != undefined) {
                mapdata.GLOBAL.listArea.polygonArea.setMap(isMap);
            }
        }

        // mutilple polygon
        if (mapdata.GLOBAL.listMultiPolygon.length > 0) {
            $.each(mapdata.GLOBAL.listMultiPolygon, function (index, obj) {
                if (obj.polygon != null && obj.polygon != undefined) {
                    // marker
                    if ((obj.polygon.listMarkerArea != null && obj.polygon.listMarkerArea != undefined) && obj.polygon.listMarkerArea.length > 0) {
                        $.each(obj.polygon.listMarkerArea, function (indexMarker, objMarker) {
                            objMarker.setMap(isMap);
                        })
                    }

                    // polygon
                    if (obj.polygon.polygonArea != null && obj.polygon.polygonArea != undefined) {
                        obj.polygon.polygonArea.setMap(isMap);
                    }
                }
            });
        }
    },
    ShowMeterDraw: function (endPoint, mousePoint, check) {
        let lat = (endPoint[1] + mousePoint[1]) / 2;
        let lng = (endPoint[0] + mousePoint[0]) / 2;
        let measure = new map4d.Measure([endPoint, mousePoint,]);
        let length = (Math.round(measure.length * 100) / 100).toString();
        if (check) {
            if (mapdata.GLOBAL.markerMeter != null) mapdata.GLOBAL.markerMeter.setMap(null);
            mapdata.GLOBAL.markerMeter = new map4d.Marker({
                position: { lat: lat, lng: lng },
                anchor: [0.5, 1],
                visible: true,
                label: new map4d.MarkerLabel({ text: length + " m", color: "000000", fontSize: 12 }),
                icon: new map4d.Icon(32, 32, ""),
            })
            mapdata.GLOBAL.markerMeter.setMap(map);
        } else {
            mapdata.GLOBAL.listDistance.listMerterDistance.push(mapdata.GLOBAL.markerMeter);
            if (mapdata.GLOBAL.markerMeter != null) mapdata.GLOBAL.markerMeter.setMap(null);
        }
    },
    splitStringDistance: function (str) {
        var list = str.split(' ');
        if (list.length > 0) {
            return Number(list[0]);
        }
        return 0;
    },

    //draw polyline dbclick
    drawPolylineDbclick: function (path) {
        mapdata.clearPolylineDbclick();
        let polyline = new map4d.Polyline({
            id: 123412,
            path: path,
            visible: true,
            strokeColor: mapdata.GLOBAL.propertiesSetting.Stroke,
            strokeOpacity: mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
            strokeWidth: mapdata.GLOBAL.propertiesSetting.StrokeWidth,
            closed: false
        })
        //thêm polyline vào map    
        polyline.setMap(map)
        mapdata.GLOBAL.listDistance.listPolylineDistance.push(polyline);
        mapdata.setDragPoligonMarker(mapdata.GLOBAL.listDistance.listMarkerDistance);
    },

    clearPolylineDbclick: function () {
        if (mapdata.GLOBAL.listDistance.listPolylineDistance !== null && mapdata.GLOBAL.listDistance.listPolylineDistance.length > 0) {
            $.each(mapdata.GLOBAL.listDistance.listPolylineDistance, function (i, obj) {
                obj.setMap(null);
            });
            mapdata.GLOBAL.listDistance.listPolylineDistance = [];
        }
    },
    setDragPoligonMarker: function (data) {
        mapdata.GLOBAL.statusEdit = true;
        $.each(data, function (i, obj) {
            obj.setDraggable(true);
        });
    },
    clearAllDraw: function () {
        let listDistance = mapdata.GLOBAL.listDistance;
        $.each(listDistance.listMarkerDistance, function (i, obj) {
            obj.setMap(null);
            if (typeof listDistance.listMerterDistance[i] != undefined && listDistance.listMerterDistance[i] != null) {
                listDistance.listMerterDistance[i].setMap(null);
            }
            if (typeof listDistance.listPolylineDistance[i] != undefined && listDistance.listPolylineDistance[i] != null) {
                listDistance.listPolylineDistance[i].setMap(null);
            }
        });
        //mapdata.GLOBAL.listDistanceTem = [];
        mapdata.GLOBAL.listDistance = {
            listMarkerDistance: [],
            listPolylineDistance: [],
            listMerterDistance: [],
        }
        let listArea = mapdata.GLOBAL.listArea;
        var datareset = listArea.listMarkerArea.length > listArea.listMerterArea.length ? listArea.listMarkerArea.length : listArea.listMerterArea.length;
        for (var i = 0; i < datareset; i++) {
            if (typeof listArea.listMarkerArea[i] != undefined && listArea.listMarkerArea[i] != null) {
                listArea.listMarkerArea[i].setMap(null);
            }
            if (typeof listArea.listMerterArea[i] != undefined && listArea.listMerterArea[i] != null) {
                listArea.listMerterArea[i].setMap(null);
            }
        }
        if (mapdata.GLOBAL.listArea.polygonArea != null) {
            mapdata.GLOBAL.listArea.polygonArea.setMap(null);
        }
        if (mapdata.GLOBAL.listArea.PolylineArea != null) {
            mapdata.GLOBAL.listArea.PolylineArea.setMap(null);
        }

        mapdata.GLOBAL.listArea = {
            listMarkerArea: [],
            listMerterArea: [],
            polygonArea: null,
            PolylineArea: null,
        }
        //isStartArea = false;
        //mapdata.setShowHideArea(false);
        if (mapdata.GLOBAL.polylineTemp != null) {
            mapdata.GLOBAL.polylineTemp.setMap(null);
            mapdata.GLOBAL.polylineTemp = null;
        }

        //reset rectangle
        let listrectangle = mapdata.GLOBAL.listRectangle;
        if (mapdata.GLOBAL.listRectangle.rectangle != null) {
            mapdata.GLOBAL.listRectangle.rectangle.setMap(null);
        }
        $.each(listrectangle.listMarkerRectangle, function (i, obj) {
            obj.setMap(null);
        });
        listrectangle.listMarkerRectangle = [];
        mapdata.GLOBAL.listRectangle.listPolygonRectangle = [];

        //reset point
        let listMarker = mapdata.GLOBAL.listMarker;
        $.each(listMarker, function (i, obj) {
            obj.setMap(null);
        });
        listMarker = [];
        mapdata.GLOBAL.listMarker = [];
        if (mapdata.GLOBAL.selectmenu === "newpoint") {
            mapdata.GLOBAL.selectmenu = "";
        }
        //reset multi point
        let listMultiPoint = mapdata.GLOBAL.listMultiPoint.MultiPoint;
        $.each(listMultiPoint, function (i, obj) {
            obj.setMap(null);
        });
        listMultiPoint = [];
        mapdata.GLOBAL.listMultiPoint.MultiPoint = [];
        if (mapdata.GLOBAL.selectmenu === "newMultipoint") {
            mapdata.GLOBAL.selectmenu = "";
        }
        //reset multi polyline
        if (mapdata.GLOBAL.selectmenu === "newMultiline" || mapdata.GLOBAL.listMultiLine.length > 0) {
            $.each(mapdata.GLOBAL.listMultiLine, function (i, objParent) {
                let listDistance = objParent.polyline;
                $.each(listDistance.listMarkerDistance, function (i, obj) {
                    obj.setMap(null);
                    if (typeof listDistance.listMerterDistance[i] != undefined && listDistance.listMerterDistance[i] != null) {
                        listDistance.listMerterDistance[i].setMap(null);
                    }
                    if (typeof listDistance.listPolylineDistance[i] != undefined && listDistance.listPolylineDistance[i] != null) {
                        listDistance.listPolylineDistance[i].setMap(null);
                    }
                });
            });
            mapdata.GLOBAL.listMultiLine = [];
            mapdata.GLOBAL.selectmenu = "";
        }
        //reset multi polygon
        if (mapdata.GLOBAL.selectmenu === "newMultipolygon" || mapdata.GLOBAL.listMultiPolygon.length > 0) {
            $.each(mapdata.GLOBAL.listMultiPolygon, function (i, objparent) {
                let listArea = objparent.polygon;
                var datareset = listArea.listMarkerArea.length > listArea.listMerterArea.length ? listArea.listMarkerArea.length : listArea.listMerterArea.length;
                for (var i = 0; i < datareset; i++) {
                    if (typeof listArea.listMarkerArea[i] != undefined && listArea.listMarkerArea[i] != null) {
                        listArea.listMarkerArea[i].setMap(null);
                    }
                    if (typeof listArea.listMerterArea[i] != undefined && listArea.listMerterArea[i] != null) {
                        listArea.listMerterArea[i].setMap(null);
                    }
                }
                if (listArea.polygonArea != null) {
                    listArea.polygonArea.setMap(null);
                }
                if (listArea.PolylineArea != null) {
                    listArea.PolylineArea.setMap(null);
                }
            });
            mapdata.GLOBAL.listMultiPolygon = [];
            mapdata.GLOBAL.selectmenu === "";
        }
        mapdata.GLOBAL.idMainObject = '';
    },
    drawRectangle: function (postion1, postion2) {
        let iLatLng = [];
        iLatLng.push({ lat: postion1.lat, lng: postion1.lng });
        iLatLng.push({ lat: postion1.lat, lng: postion2.lng });
        iLatLng.push({ lat: postion2.lat, lng: postion2.lng });
        iLatLng.push({ lat: postion2.lat, lng: postion1.lng });
        iLatLng.push(iLatLng[0]);
        mapdata.createRectangle(iLatLng);
    },
    checkJson: function (data) {
        var check;
        try {
            JSON.parse(data);
            return true;
        } catch (e) {
            return false;
        }
    },
    drawWKTGeojson: function (data) {
        if (typeof data !== "undefined") {
            let typeDraw = data.type.toLowerCase();
            let coordinates = data.coordinates;
            let iLatLng = []
            switch (typeDraw) {
                case "point":
                    mapdata.setMarkerDraw(coordinates[1], coordinates[0]);
                    break;
                case "linestring":
                    iLatLng = [];
                    $.each(coordinates, function (i, obj) {
                        let latLng = { lat: obj[1], lng: obj[0] };
                        mapdata.createMarkerDrawLoDat(obj[1], obj[0], 1)
                        iLatLng.push(latLng);
                    });
                    mapdata.createPolylineLoDat(iLatLng, 3.0, 1.0, true);
                    mapdata.setDragPoligonMarker(mapdata.GLOBAL.listDistance.listMarkerDistance);
                    break;
                case "polygon":
                    iLatLng = [];
                    let TLatLng = [];
                    for (var i = 0; i < coordinates.length; i++) {
                        iLatLng = [];
                        for (var j = 0; j < coordinates[i].length; j++) {
                            let latLng = { lat: coordinates[i][j][1], lng: coordinates[i][j][0] };
                            if (j < coordinates[i].length - 1) {
                                mapdata.createMarkerDrawLoDat(coordinates[i][j][1], coordinates[i][j][0], 2)
                            }
                            iLatLng.push(latLng);
                        }
                        TLatLng.push(iLatLng);
                    }
                    mapdata.createPolygonArea(TLatLng);
                    mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                    break;
                default: break;
            }
        }
    },
    //draw multi line
    drawMultiPolylineDbclick: function (path, id) {
        mapdata.clearPolylineDbclick();
        let polyline = new map4d.Polyline({
            path: path,
            visible: true,
            strokeColor: mapdata.GLOBAL.propertiesSetting.Stroke,
            strokeOpacity: mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
            strokeWidth: mapdata.GLOBAL.propertiesSetting.StrokeWidth,
            closed: false
        })
        //thêm polyline vào map    
        polyline.setMap(map);
        polyline.setUserData(id);
        mapdata.GLOBAL.listDistance.listPolylineDistance.push(polyline);
        mapdata.setDragMultiPoligonMarker(mapdata.GLOBAL.listDistance.listMarkerDistance, id);
        let line = {
            id: id,
            polyline: mapdata.GLOBAL.listDistance
        }
        mapdata.GLOBAL.listMultiLine.push(line);
        mapdata.GLOBAL.listDistance = {
            listMarkerDistance: [],
            listPolylineDistance: [],
            listMerterDistance: [],
            path: []
        }
    },
    drawMultiPolylineEdit: function (path, id, data) {
        if (data.listPolylineDistance !== null && data.listPolylineDistance.length > 0) {
            $.each(data.listPolylineDistance, function (i, obj) {
                obj.setMap(null);
            });
            data.listPolylineDistance = [];
        }

        let polyline = new map4d.Polyline({
            path: path,
            visible: true,
            strokeColor: mapdata.GLOBAL.propertiesSetting.Stroke,
            strokeOpacity: mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
            strokeWidth: mapdata.GLOBAL.propertiesSetting.StrokeWidth,
            closed: false
        })
        //console.log(polyline);
        //thêm polyline vào map    
        polyline.setMap(map);
        polyline.setUserData(id);
        data.listPolylineDistance.push(polyline);
        mapdata.setDragMultiPoligonMarker(data.listMarkerDistance, id);
        let objData = mapdata.GLOBAL.listMultiLine.find(x => x.id == id);
        if (objData !== null && typeof objData !== "undefined") {
            objData.polyline = data;
        }
        //let line = {
        //    id: id,
        //    polyline: data.listDistance
        //}
        //mapdata.GLOBAL.listMultiLine.push(line);
        mapdata.GLOBAL.listDistance = {
            listMarkerDistance: [],
            listPolylineDistance: [],
            listMerterDistance: [],
            path: []
        }
    },
    setDragMultiPoligonMarker: function (data, id) {
        mapdata.GLOBAL.statusEdit = true;
        $.each(data, function (i, obj) {
            obj.setUserData(id);
            obj.setDraggable(true);
        });
    },
    //End draw multi line
    //draw multi polygon
    drawMultiPolygonDbclick: function () {
        let id = mapdata.uuidv4();
        mapdata.GLOBAL.listArea.polygonArea.setUserData(id);
        mapdata.setDragMultiPolygonMarker(mapdata.GLOBAL.listArea.listMarkerArea, id);
        let MultiPolygon = {
            id: id,
            polygon: mapdata.GLOBAL.listArea
        }
        mapdata.GLOBAL.listMultiPolygon.push(MultiPolygon);
        mapdata.GLOBAL.listArea = {
            listMarkerArea: [],
            listMerterArea: [],
            polygonArea: null,
            PolylineArea: null,
        }
    },
    setDragMultiPolygonMarker: function (data, id) {
        mapdata.GLOBAL.statusEdit = true;
        $.each(data, function (i, obj) {
            obj.setUserData(id);
            obj.setDraggable(true);
        });
    },
    setLatLngPolygon: function (paths, location, index) {
        //console.log(paths[i]);
        let count = 0;
        for (var i = 0; i < paths.length; i++) {
            let check = count == 0 ? count + (paths[i].length - 2) : count + (paths[i].length - 1);
            if (check >= index) {
                let indexcurrent = count == 0 ? index - count : index - count - 1;
                if (indexcurrent == 0) {
                    paths[i][indexcurrent] = { lat: location.lat, lng: location.lng };
                    paths[i][paths[i].length - 1] = { lat: location.lat, lng: location.lng };
                } else {
                    paths[i][indexcurrent] = { lat: location.lat, lng: location.lng };
                }
            }
            count += (paths[i].length - 2);
        }
        return paths;
    },
    convertLatLngPolygon: function (obj) {
        coordinatesParent = [];
        for (var i = 0; i < obj.length; i++) {
            let coodinateschild = [];
            $.each(obj[i], function (i, objLatLng) {
                coodinateschild.push([objLatLng.lng, objLatLng.lat]);
            });
            coordinatesParent.push(coodinateschild);
        }
        return coordinatesParent;
    },
    //end draw multi polygon

    /*---End draw point,line,polygon,rectangle---*/
    /*---Show and edit point,line,polygon---*/
    showDataGeojson: function (geojsonString) {
        mapdata.GLOBAL.features = map.data.addGeoJson(geojsonString);
        if (map.is3dMode()) {
            mapdata.clearDataGeojson();
        }

        if (mapdata.GLOBAL.selectbtn != "insert" && mapdata.GLOBAL.selectbtn != "edit") // nếu đang ở trạng thái thêm mới và edit thì k remove vẽ
        {
            mapdata.clearAllDraw();
        }
    },
    showDataGeojsonPush: function (geojsonString) {
        try {
            var feature = map.data.addGeoJson(geojsonString);
            if (mapdata.GLOBAL.features.length > 0) mapdata.GLOBAL.features = mapdata.GLOBAL.features.concat(feature);
            else mapdata.GLOBAL.features = feature;
            if (map.is3dMode()) {
                mapdata.clearDataGeojson();
                mapdata.clearAllDraw();
            }
        }
        catch (err) { console.log(err) }
        //mapdata.clearAllDraw();
    },
    udpateShowDataGeojson: function (geojsonString) {
        var feature = map.data.addGeoJson(geojsonString);
        var geojson = JSON.parse(geojsonString);
        if (feature.length > 0) {
            var check = mapdata.GLOBAL.features.findIndex(x => x._id === feature[0].getId());
            if (check >= 0) {
                mapdata.GLOBAL.features.splice(check, 1);
            }

            $.each(feature, function (i, obj) {
                mapdata.GLOBAL.features.push(obj);
            });

            mapdata.clearAllDraw();
        }

        return feature;
    },
    showMoreDataGeojson: function (geojsonString) {
        let array = map.data.addGeoJson(geojsonString);
        mapdata.GLOBAL.features = mapdata.GLOBAL.features.concat(array);
        mapdata.clearAllDraw();
        mapdata.GLOBAL.idMainObject = mapdata.GLOBAL.mainObjectSelected != null ? mapdata.GLOBAL.mainObjectSelected.id : "";
    },
    clearDataGeojson: function () {
        map.data.clear();
    },
    removeDataFeatures: function (feature) {
        map.data.remove(feature);
    },
    getDataGeojsonByProperties: function (listMainObject) {
        let features = [];
        let properties = {};
        $.each(listMainObject, function (i, obj) {
            if (obj.hasOwnProperty("properties") == false) {
                properties = Config_Main_Object.getValueProperties();
            }
            else {
                properties = obj.properties == null ? mapdata.GLOBAL.propertiesSetting : obj.properties;
            }
            let defaultProperties = {
                "stroke": properties.stroke,
                "stroke-width": properties.strokeWidth,
                "stroke-opacity": properties.strokeOpacity,
                "fill": properties.fill,
                "fill-opacity": properties.fillOpacity
            };
            let feature = {};
            feature.type = "Feature";
            feature.properties = defaultProperties;
            feature.geometry = obj.geometry;
            feature.id = obj.id;
            features.push(feature);
        });
        let geojson = {
            "type": "FeatureCollection",
            "features": features
        };
        return geojson;
    },
    getDataGeojson: function (listMainObject) {
        let features = [];
        let defaultProperties = {
            "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
            "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
            "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
            "fill": mapdata.GLOBAL.propertiesSetting.Fill,
            "fill-opacity": mapdata.GLOBAL.propertiesSetting.FillOpacity
        };
        $.each(listMainObject, function (i, obj) {
            let feature = {};
            feature.type = "Feature";
            feature.properties = defaultProperties;
            feature.geometry = obj.geometry;
            feature.id = obj.id;
            features.push(feature);
        });
        let geojson = {
            "type": "FeatureCollection",
            "features": features
        };
        return geojson;
    },
    getDataGeojsonByOne: function (MainObject) {
        let features = [];
        let defaultProperties = {
            "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
            "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
            "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
            "fill": mapdata.GLOBAL.propertiesSetting.Fill,
            "fill-opacity": mapdata.GLOBAL.propertiesSetting.FillOpacity
        };
        let feature = {};
        feature.type = "Feature";
        feature.properties = defaultProperties;
        feature.geometry = MainObject.geometry;
        feature.id = MainObject.id;
        features.push(feature);
        let geojson = {
            "type": "FeatureCollection",
            "features": features
        };
        return geojson;
    },
    convertDataToGeojson: function () {
        let data, dataPath, coordinates, feature, features = [];
        let geojson = { "type": "FeatureCollection", "features": [] };
        //point
        if (mapdata.GLOBAL.listMarker.length > 0) {
            $.each(mapdata.GLOBAL.listMarker, function (i, obj) {
                feature = {};
                coordinates = [obj.getPosition().lng, obj.getPosition().lat];
                feature = {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Point",
                        "coordinates": coordinates
                    }
                };
                features.push(feature);
            });
            geojson.features = features;
        }
        //line
        if (mapdata.GLOBAL.listDistance.listPolylineDistance.length > 0) {
            data = mapdata.GLOBAL.listDistance.listPolylineDistance;
            for (var i = 0; i < data.length; i++) {
                feature = {};
                dataPath = data[i].getPath();
                coordinates = [];
                for (var j = 0; j < dataPath.length; j++) {
                    coordinates.push([dataPath[j].lng, dataPath[j].lat]);
                }
                feature = {
                    "type": "Feature",
                    "properties": {
                        "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
                        "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                        "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity
                    },
                    "geometry": {
                        "type": "LineString",
                        "coordinates": coordinates
                    }
                };
                features.push(feature);
            }
            geojson.features = features;
        }
        //polygon
        if (mapdata.GLOBAL.listArea.polygonArea !== null) {
            coordinates = [];
            $.each(mapdata.GLOBAL.listArea.polygonArea.getPaths(), function (i, obj) {
                let coordinateChild = [];
                $.each(obj, function (i, obj1) {
                    coordinateChild.push([obj1.lng, obj1.lat]);
                });
                coordinates.push(coordinateChild);
            });
            feature = {
                "type": "Feature",
                "properties": {
                    "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
                    "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                    "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                    "fill": mapdata.GLOBAL.propertiesSetting.Fill,
                    "fill-opacity": mapdata.GLOBAL.propertiesSetting.FillOpacity
                },
                "geometry": {
                    "type": "Polygon",
                    "coordinates": coordinates
                }
            };
            features.push(feature);
            geojson.features = features;
        }
        if (mapdata.GLOBAL.listRectangle.listMarkerRectangle.length > 0) {
            coordinates = [];
            $.each(mapdata.GLOBAL.listRectangle.rectangle.getPaths(), function (i, obj) {
                let coordinateChild = [];
                $.each(obj, function (i, obj1) {
                    coordinateChild.push([obj1.lng, obj1.lat]);
                });
                coordinates.push(coordinateChild);
            });
            feature = {
                "type": "Feature",
                "properties": {
                    "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
                    "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                    "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                    "fill": mapdata.GLOBAL.propertiesSetting.Fill,
                    "fill-opacity": mapdata.GLOBAL.propertiesSetting.FillOpacity
                },
                "geometry": {
                    "type": "Polygon",
                    "coordinates": coordinates
                }
            };
            features.push(feature);
            geojson.features = features;
        }

        //multiPoint
        if (mapdata.GLOBAL.listMultiPoint !== null && mapdata.GLOBAL.listMultiPoint.MultiPoint.length > 0) {
            coordinates = [];
            $.each(mapdata.GLOBAL.listMultiPoint.MultiPoint, function (i, obj) {
                coordinates.push([obj.getPosition().lng, obj.getPosition().lat]);
            });
            feature = {
                "type": "Feature",
                "properties": {
                    "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
                    "image2D": mapdata.GLOBAL.propertiesSetting.Image2D,
                },
                "geometry": {
                    "type": "MultiPoint",
                    "coordinates": coordinates
                }
            };
            features.push(feature);
            geojson.features = features;
        }
        //MultiLineString or Multipolyline
        if (mapdata.GLOBAL.listMultiLine !== null && mapdata.GLOBAL.listMultiLine.length > 0) {
            coordinatesParent = [];

            $.each(mapdata.GLOBAL.listMultiLine, function (i, objParent) {
                let data = objParent.polyline.listPolylineDistance[0];
                let latlng = [];
                dataPath = data.getPath();
                for (var j = 0; j < dataPath.length; j++) {
                    latlng.push([dataPath[j].lng, dataPath[j].lat]);
                }
                coordinatesParent.push(latlng);
            });
            //properties = mapdata.GLOBAL.listMultiLine[0];
            feature = {
                "type": "Feature",
                "properties": {
                    "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
                    "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                    "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                    "fill": mapdata.GLOBAL.propertiesSetting.Fill,
                    "fill-opacity": mapdata.GLOBAL.propertiesSetting.FillOpacity
                },
                "geometry": {
                    "type": "MultiLineString",
                    "coordinates": coordinatesParent
                }
            };
            features.push(feature);
            geojson.features = features;
        }
        //MultiPolygon
        if (mapdata.GLOBAL.listMultiPolygon !== null && mapdata.GLOBAL.listMultiPolygon.length > 0) {
            coordinatesMultiPolygon = [];
            $.each(mapdata.GLOBAL.listMultiPolygon, function (i, obj) {
                let polygon = mapdata.convertLatLngPolygon(obj.polygon.polygonArea.paths);
                coordinatesMultiPolygon.push(polygon);
            });
            feature = {
                "type": "Feature",
                "properties": {
                    "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
                    "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                    "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                    "fill": mapdata.GLOBAL.propertiesSetting.Fill,
                    "fill-opacity": mapdata.GLOBAL.propertiesSetting.FillOpacity
                },
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": coordinatesMultiPolygon
                }
            };
            features.push(feature);
            geojson.features = features;
        }
        return geojson;
    },
    convertGeojsonToData: function (datafeature) {
        let type = datafeature.getGeometry().getType().toLowerCase();
        let coordinates;//= data.coordinates;
        let iLatLng = []
        switch (type) {
            case "point":
                coordinates = datafeature.getGeometry()._coordinate;
                mapdata.setMarkerDraw(coordinates.lat, coordinates.lng);
                break;
            case "multipoint":
                var listObj = datafeature.getGeometry().getArray();
                $.each(listObj, function (i, obj) {
                    mapdata.setMultiMarkerDraw(obj.lat, obj.lng);
                })
                break;
            case "linestring":
            case "polyline":
                coordinates = [];
                datafeature.getGeometry().forEachLatLng(function (latlng) {
                    let latLng = { lat: latlng.lat, lng: latlng.lng };
                    mapdata.createMarkerDrawLoDat(latlng.lat, latlng.lng, 1)
                    coordinates.push(latLng);
                });
                mapdata.createPolylineLoDat(coordinates, 3.0, 1.0, true);
                mapdata.setDragPoligonMarker(mapdata.GLOBAL.listDistance.listMarkerDistance);
                break;
            case "polygon":
                coordinates = [];
                $.each(datafeature.getGeometry().getArray(), function (i, obj) {
                    let array = [];
                    $.each(obj.elements, function (j, obj1) {
                        let latLng = { lat: obj1.lat, lng: obj1.lng };
                        if (j < obj.elements.length - 1) {
                            mapdata.createMarkerDrawLoDat(obj1.lat, obj1.lng, 2)
                        }
                        array.push(latLng);
                    });
                    coordinates.push(array);
                });
                mapdata.createPolygonArea(coordinates);
                mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                break;
            case "multilinestring":
                $.each(datafeature.getGeometry()._elements, function (i, obj) {
                    line = [];
                    obj.forEachLatLng(function (a) {
                        let latLng = { lat: a.lat, lng: a.lng };
                        mapdata.createMarkerDrawLoDat(latLng.lat, latLng.lng, 1);
                        line.push(latLng)
                    });
                    mapdata.drawMultiPolylineDbclick(line, mapdata.uuidv4());
                    mapdata.setDragPoligonMarker(mapdata.GLOBAL.listDistance.listMarkerDistance);
                });
                break;
            case "multipolygon":
                $.each(datafeature.getGeometry()._elements, function (i, obj) {
                    coordinates = [];
                    $.each(obj.getArray(), function (i, objchild) {
                        let array = [];
                        $.each(objchild.elements, function (j, obj1) {
                            let latLng = { lat: obj1.lat, lng: obj1.lng };
                            if (j < objchild.elements.length - 1) {
                                mapdata.createMarkerDrawLoDat(obj1.lat, obj1.lng, 2)
                            }
                            array.push(latLng);
                        });
                        coordinates.push(array);
                    });
                    mapdata.createPolygonArea(coordinates);
                    mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                    mapdata.drawMultiPolygonDbclick();
                });
                break;
            default: break;
        }
        map.data.remove(datafeature);
        mapdata.clearHighlight();
    },
    /*---End Show and edit point,line,polygon---*/

    /*---button insert,eidt---*/
    setOrRestBtnInsert: function (check) {
        if (check) {
            //$(mapdata.SELECTORS.editgeojson).css('display', 'none');
            //$(mapdata.SELECTORS.deletegeojson).css('display', 'none');
            //$(mapdata.SELECTORS.endgeojson).css('display', 'block');
            //$(mapdata.SELECTORS.cancelgeojson).css('display', 'block');
            $(mapdata.SELECTORS.newgeojsonbutton).attr('disabled', true);
            //$(mapdata.SELECTORS.divinputsearch).css('width', 'calc(100% - 330px)');
            mapdata.GLOBAL.selectbtn = "insert";
        } else {
            //$(mapdata.SELECTORS.endgeojson).css('display', 'none');
            //$(mapdata.SELECTORS.cancelgeojson).css('display', 'none');
            //$(mapdata.SELECTORS.editgeojson).css('display', 'block');
            //$(mapdata.SELECTORS.deletegeojson).css('display', 'block');
            $(mapdata.SELECTORS.newgeojsonbutton).attr('disabled', false);
            //$(mapdata.SELECTORS.divinputsearch).css('width', 'calc(100% - 300px)');
            mapdata.GLOBAL.selectbtn = "";
        }
    },
    setOrRestBtnEidt: function (check) {
        if (check) {
            $(mapdata.SELECTORS.editgeojson).attr('disabled', true);
            $(mapdata.SELECTORS.deletegeojson).css('display', 'none');
            //$(mapdata.SELECTORS.endgeojson).css('display', 'block');
            //$(mapdata.SELECTORS.cancelgeojson).css('display', 'block');
            $(mapdata.SELECTORS.newgeojsonbutton).css('display', 'none');
            mapdata.GLOBAL.selectbtn = "edit";
        } else {
            $(mapdata.SELECTORS.editgeojson).attr('disabled', false);
            $(mapdata.SELECTORS.deletegeojson).css('display', 'block');
            //$(mapdata.SELECTORS.endgeojson).css('display', 'none');
            //$(mapdata.SELECTORS.cancelgeojson).css('display', 'none');
            $(mapdata.SELECTORS.newgeojsonbutton).css('display', 'block');
            mapdata.GLOBAL.selectbtn = "";
        }
    },
    setListObjectModel: function () {
        $.ajax({
            type: "GET",
            url: '/api/HTKT/ObjectModel/get-list',
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok" && data.result != null && data.result.length > 0) {
                    mapdata.GLOBAL.list = [];
                    $.each(data.result, function (i, obj) {
                        var object = {
                            "id": obj.idObject,
                            "name": obj.name,
                            "type": obj.type,
                            "objName": obj.objName,
                            "objUrl": obj.objUrl,
                            "objData": obj.objData,
                            "textureName": obj.textureName,
                            "textureUrl": obj.textureUrl,
                            "textureData": obj.textureData,
                            "color": obj.color,
                            "thumbnailName": obj.thumbnailName,
                            "thumbnailUrl": obj.thumbnailUrl,
                            "coordinates": obj.coordinates,
                            "height": obj.height,
                            "placeTypes": obj.placeTypes,
                            "placeTypeInfo": obj.placeTypeInfo
                        };
                        mapdata.GLOBAL.list.push(object);
                    });
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    /*---end button insert,eidt---*/

    //highligt Line, MultiLine, Polygon, MultiPolygon
    highlightObject: function (args) {
        mapdata.clearHighlight();
        let type = args.feature.getGeometry().getType().toLowerCase();
        let line = [];
        if (type === "multipolygon" || type === "polygon" || type === "linestring") {
            args.feature.getGeometry().forEachLatLng(function (a) {
                if (line.length > 0 && line[0][0] === a.lng && line[0][1] === a.lat) {
                    line.push([a.lng, a.lat]);
                    mapdata.setHighlight(line);
                    line = [];
                } else {
                    line.push([a.lng, a.lat]);
                }
            });
            if (line.length > 0) mapdata.setHighlight(line);
        } else {
            $.each(args.feature.getGeometry()._elements, function (i, obj) {
                obj.forEachLatLng(function (a) {
                    line.push([a.lng, a.lat]);
                });
                mapdata.setHighlight(line);
                line = [];
            });
        }
    },
    highlightObjectMenuLeft: function (args) {
        mapdata.clearHighlight();
        try {
            let type = args.getGeometry().getType().toLowerCase();
            let line = [];
            menuLeft.GLOBAL.boundLatLng = [];
            if (type === "multipolygon" || type === "polygon" || type === "linestring") {
                args.getGeometry().forEachLatLng(function (a) {
                    if (line.length > 0 && line[0][0] === a.lng && line[0][1] === a.lat) {
                        line.push([a.lng, a.lat]);
                        mapdata.setHighlight(line);
                        line = [];
                    } else {
                        line.push([a.lng, a.lat]);
                    }
                    menuLeft.GLOBAL.boundLatLng.push([a.lng, a.lat]);
                    menuLeft.GLOBAL.lat = a.lat;
                    menuLeft.GLOBAL.lng = a.lng;
                });
                if (line.length > 0) mapdata.setHighlight(line);
            }
            else if (type == "point") {
                menuLeft.GLOBAL.lat = args.getGeometry()._coordinate.lat;
                menuLeft.GLOBAL.lng = args.getGeometry()._coordinate.lng;
                menuLeft.GLOBAL.boundLatLng.push([menuLeft.GLOBAL.lng, menuLeft.GLOBAL.lat]);

                mapdata.highlightObjectPoint(menuLeft.GLOBAL.lat, menuLeft.GLOBAL.lng);
            }
            else if (type === "multipoint") {
                var listobj = args.getGeometry().getArray();
                $.each(listobj, function (i, obj) {
                    if (i == 0) {
                        menuLeft.GLOBAL.lat = obj.lat;
                        menuLeft.GLOBAL.lng = obj.lng;
                    }
                    menuLeft.GLOBAL.boundLatLng.push([obj.lng, obj.lat]);
                });

                mapdata.highlightObjectMutiplePoint(listobj);
            }
            else {
                $.each(args.getGeometry()._elements, function (i, obj) {
                    obj.forEachLatLng(function (a) {
                        line.push([a.lng, a.lat]);
                        menuLeft.GLOBAL.lat = a.lat;
                        menuLeft.GLOBAL.lng = a.lng;
                        menuLeft.GLOBAL.boundLatLng.push([a.lng, a.lat]);
                    });
                    if (line.length > 0) mapdata.setHighlight(line);
                    line = [];
                });
            }
        } catch (err) { }

        //if (map.is3dMode()) {
        //    var cameramap = map.getCamera();
        //    cameramap.zoom = 18;
        //    cameramap.setTarget({ lat: menuLeft.GLOBAL.lat, lng: menuLeft.GLOBAL.lng });
        //    cameramap.tilt = 20;
        //    map.moveCamera(cameramap);
        //} else {
        //    menuLeft.fitBounds(menuLeft.GLOBAL.boundLatLng);
        //}
    },
    highlightObjectData: function (data) {
        mapdata.clearHighlight();
        let type = data.geometry.type.toLowerCase();
        let line = [];
        if (type === "linestring") {
            mapdata.setHighlight(data.geometry.coordinates);
        }
        if (type === "polygon") {
            mapdata.setHighlight(data.geometry.coordinates[0]);
        }
        if (type === "multipolygon") {
            for (var i = 0; i < data.geometry.coordinates[0].length; i++) {
                mapdata.setHighlight(data.geometry.coordinates[0][i]);
            }
        }
    },
    // vẽ đoạn thẳng, polygon trên bản đồ 
    setHighlight: function (line) {
        let polyline = new map4d.Polyline({
            path: line,
            strokeColor: "#ff0000",
            strokeOpacity: 1.0,
            strokeWidth: 2,
            zIndex: 100
        })
        // Thêm polyline vào bản đồ
        //if (!map.is3dMode()) {
        polyline.setMap(map);
        //}
        mapdata.GLOBAL.listHighlight.push(polyline);
    },
    // xóa đoạn thẳng, polygon trên bản đồ
    clearHighlight: function () {
        if (mapdata.GLOBAL.listHighlight !== null && mapdata.GLOBAL.listHighlight.length > 0) {
            $.each(mapdata.GLOBAL.listHighlight, function () { this.setMap(null) });
            mapdata.GLOBAL.listHighlight = [];
        }
    },
    // ẩn hiện đoạn thẳng, polygon trên bản đồ
    visibleHightlight: function (type) {
        var mapView = map;
        if (!type) {
            mapView = null;
        }

        if (mapdata.GLOBAL.listHighlight !== null && mapdata.GLOBAL.listHighlight.length > 0) {
            $.each(mapdata.GLOBAL.listHighlight, function () { this.setMap(mapView) });
        }
    },
    //Update geometry object
    UpdateGeometryObject: function (object) {
        abp.ui.setBusy(mapdata.SELECTORS.inforData);
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_AJAXUPDATESAVE,
            data: JSON.stringify(object),
            success: function (data) {
                abp.ui.clearBusy(mapdata.SELECTORS.inforData);
                if (data.code == "ok") {
                    abp.notify.success(l("ManagementLayer:UpdateDataSuccesfully"));
                    let objMain = mapdata.GLOBAL.listObjectByDirectory.find(x => x.id == data.result.id)
                    if (objMain !== null) {
                        objMain.geometry = data.result.geometry;
                        if (mapdata.GLOBAL.mainObjectSelected !== null && mapdata.GLOBAL.mainObjectSelected.id == data.result.id) {
                            mapdata.GLOBAL.mainObjectSelected = data.result;
                            mapdata.GLOBAL.idMainObject = data.result.id;
                        }
                        //mapdata.GLOBAL.mainObjectSelected = (mapdata.GLOBAL.mainObjectSelected !== null && mapdata.GLOBAL.mainObjectSelected.id == data.result.id) ? objMain : mapdata.GLOBAL.mainObjectSelected;
                        //mapdata.GLOBAL.idMainObject = mapdata.GLOBAL.mainObjectSelected !== null ? mapdata.GLOBAL.mainObjectSelected.id : "";
                    }
                    let geojson = mapdata.getDataGeojsonByProperties([data.result]);
                    if (mapdata.GLOBAL.selectObject2D != null && mapdata.GLOBAL.selectObject2D != undefined) {
                        map.data.remove(mapdata.GLOBAL.selectObject2D.feature);
                    }

                    var featureUpdate = mapdata.udpateShowDataGeojson(JSON.stringify(geojson));
                    if (featureUpdate.length > 0) {
                        mapdata.GLOBAL.selectObject2D.feature = featureUpdate[0];
                    }
                    let check = mapdata.GLOBAL.listObjectByDirectory.find(x => x.id == data.result.id);
                    if (check != null && check != undefined) {
                        check.geometry = data.result.geometry;
                    }
                } else {
                    abp.notify.error(l("ManagementLayer:UpdateDataFailed"));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
                abp.ui.clearBusy(mapdata.SELECTORS.inforData);
            }
        });
    },
    //hide infor properties
    hideInforProperties: function () {
        if ($(mapdata.SELECTORS.inforData).hasClass("detail-property-collapse") === false) {
            $(mapdata.SELECTORS.inforData).toggleClass('detail-property-collapse');
        }
    },
    //clear highlight and clear draw and set null select
    clearSelectDrawHighlight: function () {
        mapdata.clearHighlight();
        mapdata.clearAllDraw();
        mapdata.GLOBAL.mainObjectSelected = null;
        mapdata.GLOBAL.idMainObject = '';
        mapdata.GLOBAL.selectObject2D = null;
    },
    hideInfoData: function () {
        !$(mapdata.SELECTORS.inforData).hasClass('detail-property-collapse') && $(mapdata.SELECTORS.inforData).toggleClass('detail-property-collapse');
        mapdata.GLOBAL.idMainObject = '';
        mapdata.GLOBAL.mainObjectSelected = null;
        model3D.hideModel3D();
        model3D.resetAll();
        $('#InforData .li-modal-data-right').eq(3).trigger('click');
        mapdata.clearSelectDrawHighlight();
    },
    //new Guid Id
    uuidv4: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    hideAllObject3D: function () {
        $.each(mapdata.GLOBAL.listObject3D, function (i, obj) {
            obj.object.setMap(null);
        });
    },
    hideObject3DById: function (id) {
        $.each(mapdata.GLOBAL.listObject3D, function (i, obj) {
            if (obj.id == id) {
                obj.object.setMap(null);
            } else {
                obj.object.setMap(map);
            }
        });
    },
    showAllObject3D: function () {
        $.each(mapdata.GLOBAL.listObject3D, function (i, obj) {
            if (obj.object !== null) {
                if (obj.object.getMap() === null) obj.object.setMap(map);
                obj.object.setSelected(false);
            }
        });
    },
    // danh sách biểu đồ nền
    GetLayerBaseMap: function () {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_AJAX_ARRAY_LAYER_BASE_MAP,
            data: {
            },
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    var html = `<div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <div class="item-map item-active data-order="-1" title="${l("ManagementLayer:Default")}"
                                           style="display: block; background-repeat: no-repeat; background-position: center center; background-size: 100%; background-image: url(${location.origin}/images/logomap4d_2.png)"
                                            data-url="" data-image="${location.origin}/images/logomap4d_2.png" >
                                           <label>${l("ManagementLayer:Default")}</label>
                                       </div>
                                    </div>
                                `;

                    for (var i = 0; i < data.items.length; i++) {

                        html += `
                            <div class="col-md-3 col-xs-3">
                                <div class="item-map" data-order="${data.items[i].order}" title="${data.items[i].nameBasMap}"
                                    style="display: block; background-repeat: no-repeat; background-position: center center; background-size: 100%;  background-image: url(${data.items[i].image});"
                                    data-url="${data.items[i].link}"  data-image="${data.items[i].image}">
                                    <label>${mapdata.htmlEntities(data.items[i].nameBasMap)}</label>
                                </div>
                            </div>`;
                    }

                    html += `</div>`;

                    $('#body-modal-list-layer-base-map').html(html);

                    mapdata.EventLayerBaseMap();
                }
                else {
                    console.log(res);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    EventLayerBaseMap: function () {
        $(".change-tile-map").on('click', function () {
            $('#modal-list-layer-base-map').modal('show')
        })

        $(".item-map").click(function () {
            var title = $(this).attr('title');
            var image = $(this).attr('data-image');
            $(".item-map").removeClass('item-active');
            $(this).addClass('item-active');
            $('.parent-tile-map label').text(title);
            $(mapdata.SELECTORS.content_tile_map).attr('data-original-title', title)
            //$('.parent-tile-map').css('background-image', `url(${image})`)

            var order = parseInt($(this).attr('data-order'));

            if (order != mapdata.GLOBAL.OrderBaseMap) {
                mapdata.GLOBAL.OrderBaseMap = order;

                var urlTile = $(this).attr("data-url");

                if (urlTile != "") {
                    var exp = "";
                    var host = "";

                    var lst = urlTile.split('${z}/${x}/${y}');

                    var type = "";
                    if (urlTile.includes('{z}/{x}/{y}')) {
                        lst = urlTile.split('{z}/{x}/{y}');
                        type = '{z}/{x}/{y}';
                    }
                    else if (urlTile.includes('${z}/${x}/${y}')) {
                        lst = urlTile.split('${z}/${x}/${y}');
                        type = '${z}/${x}/${y}';
                    }
                    else if (urlTile.includes('x={x}&y={y}&z={z}')) {
                        lst = urlTile.split('x={x}&y={y}&z={z}');
                        type = 'x={x}&y={y}&z={z}';
                    }
                    else if (urlTile.includes('x=${x}&y=${y}&z=${z}')) {
                        lst = urlTile.split('x=${x}&y=${y}&z=${z}');
                        type = 'x=${x}&y=${y}&z=${z}';

                    }

                    if (lst.length > 1) {
                        host = lst[0];
                        exp = lst[1];
                    }
                    else {
                        host = lst[0];
                    }

                    if (lst != 0) {
                        let options = {
                            getUrl: function (x, y, z, is3dMode) {
                                if (type == "{z}/{x}/{y}" || type == '${z}/${x}/${y}') {
                                    return host + `${z}/${x}/${y}` + exp;
                                }
                                else {
                                    return host + `x=${x}&y=${y}&z=${z}` + exp;
                                }
                                //return urlTile + `/${z}/${x}/${y}.${exp.trim()}`
                                //return `https://maptiles.p.rapidapi.com/en/map/v1/{z}/{x}/{y}.png`;
                            },
                            visible: true,
                            zIndex: 1
                        }

                        if (mapdata.GLOBAL.OverLay != null) {
                            mapdata.GLOBAL.OverLay.setMap(null);
                        }

                        mapdata.GLOBAL.OverLay = new map4d.TileOverlay(options)

                        mapdata.GLOBAL.OverLay.setMap(map);
                    }
                }
                else {
                    if (mapdata.GLOBAL.OverLay != null) {
                        mapdata.GLOBAL.OverLay.setMap(null);
                    }
                }

            }

        });
    },
    LocationMapDefault: function (id) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: true,
            url: mapdata.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=' + id,
            data: {
            },
            success: function (data) {
                if (data.code == "ok") {
                    //if (data.result != null && typeof data.result == "string")
                    if (data.result != null) {
                        if (data.result.location != null) {
                            if (data.result.location.lat > 0 && data.result.location.lng > 0) {
                                var cameraPosition = {
                                    target: { lat: parseFloat(data.result.location.lat), lng: parseFloat(data.result.location.lng) },
                                    tilt: 0,
                                    bearing: 0,
                                    zoom: (data.result.zoom != null ? (data.result.zoom == 0 ? 15 : data.result.zoom) : 15)
                                }

                                map.moveCamera(cameraPosition, null);
                            }
                            else {
                                mapdata.GetCurrentLocation();
                            }
                        }

                        if (data.result.groundMaptile != "" && data.result.groundMaptile != null && data.result.optionDirectory === 1) {
                            var tags = data.result.tags;
                            var objectStringArray = [];
                            if (tags != null) {
                                objectStringArray = (new Function("return " + tags["bounds"] + ";")());
                            }
                            else {
                                objectStringArray = (new Function("return " + bounds + ";")());
                            }

                            if (objectStringArray.length > 0) {
                                var urlTile = data.result.groundMaptile;

                                var Arrayexp = urlTile.trim().split('.');
                                exp = Arrayexp[Arrayexp.length - 1];

                                if (exp == "" || exp != "png" && exp != "jpg") {
                                    exp = "png";
                                }

                                urlTile = urlTile.trim().split('/$')[0];
                                urlTile = urlTile.trim().split('/{')[0];

                                let overlayOptions = {
                                    getUrl: function (x, y, z, is3dMode) {
                                        return urlTile + `/${z}/${x}/${y}.${exp.trim()}`
                                    },
                                    bounds: objectStringArray,
                                    override: false
                                }
                                overlay = new map4d.GroundOverlay(overlayOptions)
                                overlay.setMap(map);
                            }
                        }

                        // appened option select files
                        $(modalCreateFolderLayer.SELECTORS.inputListDocument).html('');
                        $(UploadFileMultiObject.SELECTORS.inputListDocument).html('');

                        var lst = data.result.listProperties.filter(x => x.typeProperties == "file");

                        // kiểm tra nếu ko có thuộc tính file thì ẩn menu upload files
                        $(mapdata.SELECTORS.uploadMultiFile).attr('style', 'display:inline-flex !important');
                        $('#right-line-upload-mutiple-file').show();
                        if (lst.length == 0) {
                            $(mapdata.SELECTORS.uploadMultiFile).attr('style', 'display:none !important');
                            $('#right-line-upload-mutiple-file').hide();
                        }

                        var $optionSelectFile = '';
                        $.each(lst, function (iFile, FileObject) {
                            $optionSelectFile += `<option value="${FileObject.codeProperties}">${FileObject.nameProperties}</option>`;
                        });

                        $(modalCreateFolderLayer.SELECTORS.inputListDocument).html($optionSelectFile);
                        $(UploadFileMultiObject.SELECTORS.inputListDocument).html($optionSelectFile);

                        mapdata.GLOBAL.lstPropertiesDirectory = data.result.listProperties;
                        menuLeft.SelectedSearchProperties();

                        modalCreateFolderLayer.GLOBAL.SelectedFilesFolder = lst; //  files array default
                        $.each(lst, function (i, obj) {
                            let option = `<option value="${obj.codeProperties}">${obj.nameProperties}</option>`;
                            $(UploadFileMultiObject.SELECTORS.inputListDocument).append(option);
                        });
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    GetCurrentLocation: function () {
        $.ajax({
            type: "GET",
            url: mapdata.CONSTS.GET_LOCATION_CENTER,
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok") {
                    var cameraPosition = map.getCamera();
                    cameraPosition.target = {
                        lat: parseFloat(data.result.latitude),
                        lng: parseFloat(data.result.longitude)
                    }
                    map.moveCamera(cameraPosition, null);
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    UploadFileImage: function (element) {
        var obj = {
            check: false,
            url: ""
        };

        var formData = new FormData();
        formData.append('file', $(element)[0].files[0]);

        $.ajax({
            url: '/api/htkt/file/image',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            async: false,
            success: function (res) {
                if (res.code == "ok") {
                    obj.check = true;
                    obj.url = res.result.url;
                }
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });

        return obj;
    },
    DeleteFileImage: function (url) {
        var urlEnCode = encodeURIComponent(url);
        var check = false;
        $.ajax({
            url: '/api/htkt/file/deleteimage',
            type: 'POST',
            data: JSON.stringify({ url: urlEnCode }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (res) {
                if (res.code == "ok") {
                    check = res.result;
                }
                else {
                    console.log(res);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + ": " + errorThrown);
            }
        });
        return check;
    },
    getListObjectInGeometry: function () {
        $.ajax({
            type: "GET",
            url: mapdata.CONSTS.URL_AJAXGETOBJECTBYGEOMETRY,
            data: {},
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (datajson) {
                var data = JSON.parse(datajson);
                if (data.code == "ok" && data.result != null && data.result.length > 0) {
                    mapdata.GLOBAL.listObjectMapInRadius = [];
                    var array = [];
                    data.result.sort(function (a, b) {
                        var textA = a.name.trim().slice(0, 1).toUpperCase();
                        var textB = b.name.trim().slice(0, 1).toUpperCase();
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    });
                    $.each(data.result, function (i, obj) {
                        var object = {
                            "id": obj.id,
                            "name": obj.name,
                            "type": obj.type,
                            "address": obj.address,
                            "location": obj.location,
                            "scale": obj.scale,
                            "bearing": obj.bearing,
                            "elevation": obj.elevation,
                            "heightScale": obj.heightScale
                        };
                        mapdata.GLOBAL.listObjectMapInRadius.push(object);
                        array.push(obj.id);
                    });
                    map.setVisibleBuildings(array);
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },

    //set value properties infor input update
    setDataValueInputUpdate: function () {
        let lstproperties = mapdata.GLOBAL.mainObjectSelected.listProperties;
        $.each(lstproperties, function (i, obj) {
            $('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
            let lstvalue;
            if (obj.typeProperties == "list") {
                try {
                    lstvalue = JSON.parse(obj.defalutValue);
                    if (lstvalue != undefined) {
                        let value = lstvalue.find(x => x.checked);
                        if (value != null && value != undefined) {
                            $('#text-modal-' + obj.codeProperties).val(value.code);
                        }
                    }
                }
                catch (e) {
                    $('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                }
            }
            if (obj.typeProperties == "radiobutton") {
                try {
                    lstvalue = JSON.parse(obj.defalutValue);
                    if (lstvalue != undefined) {
                        let value = lstvalue.find(x => x.checked);
                        if (value != null && value != undefined) {
                            $('.text-modal-' + obj.codeProperties + '[value = "' + value.code + '"]').prop("checked", true);
                        }
                    }
                }
                catch (e) {
                    $('.text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                }
            }

            if (obj.typeProperties == "checkbox") {
                try {
                    lstvalue = JSON.parse(obj.defalutValue);

                    if (lstvalue.length > 0) {
                        $.each(lstvalue, function (i, item) {
                            if (item.checked) {
                                $('.text-modal-' + obj.codeProperties + '[value = "' + item.code + '"]').prop("checked", true);
                            }
                        });
                    }
                }
                catch (e) {
                    if (obj.defalutValue != "" && obj.defalutValue != null) {
                        var lstChecked = obj.defalutValue.split(",");
                        $.each(lstChecked, function (i, item) {
                            $('.text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                        });
                    }
                }
            }
        });
    },
    htmlEntities: function (str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    },
    setDefaultLayerBaseMap: function () {
        let options = {
            getUrl: function (x, y, z, is3dMode) {
                return `https://api-extiler.map4d.vn/map/tile/607814ad343b585096aa3d1e/${z}/${x}/${y}.png`;
            },
            visible: true,
            zIndex: 1
        }

        if (mapdata.GLOBAL.OverLay != null) {
            mapdata.GLOBAL.OverLay.setMap(null);
        }

        mapdata.GLOBAL.OverLay = new map4d.TileOverlay(options)

        mapdata.GLOBAL.OverLay.setMap(map);
    },

    //set features change geojson and highlight
    setChangeGeojsonAndHighlight: function (feature) {
        for (var i = 0; i < feature.length; i++) {
            var check = mapdata.GLOBAL.features.findIndex(x => x._id === feature[i].getId());
            if (check >= 0) {
                mapdata.GLOBAL.features.splice(check, 1);
                mapdata.GLOBAL.features.push(feature[i]);
                mapdata.highlightObjectMenuLeft(feature[i]);
            }
        }
    },
    ShowButton: function () {
        if (isAdmin == "False") {
            if (lstLayerPermission.length > 0) {
                if (lstLayerPermission[0].listPermisson.includes("PermissionLayer.CREATE")) {
                    $(mapdata.SELECTORS.newgeojsonbutton).css('display', 'block');
                }
                if (lstLayerPermission[0].listPermisson.includes("PermissionLayer.EDIT")) {
                    $(mapdata.SELECTORS.editgeojson).css('display', 'block');
                    $(mapdata.SELECTORS.btnEdited).css('display', 'block');

                    $(informap2d.SELECTORS.btnSaveMap2D).css('display', 'block');

                    $(model3D.SELECTORS.btnSave3D).css('display', 'block');

                    $(mapdata.SELECTORS.btnSaveProperties).css('display', 'block');
                }
                if (lstLayerPermission[0].listPermisson.includes("PermissionLayer.DELETE")) {
                    $(mapdata.SELECTORS.deletegeojson).css('display', 'block');
                    $(mapdata.SELECTORS.btnDeleted).css('display', 'block');

                    $(informap2d.SELECTORS.btnDeleteMap2D).css('display', 'block');

                    $(documentObject.SELECTORS.btnDeleted).css('display', 'block');
                }
            }
        } else {
            $(mapdata.SELECTORS.newgeojsonbutton).css('display', 'block');

            $(mapdata.SELECTORS.editgeojson).css('display', 'block');
            $(mapdata.SELECTORS.btnEdited).css('display', 'block');

            $(informap2d.SELECTORS.btnSaveMap2D).css('display', 'block');

            $(model3D.SELECTORS.btnSave3D).css('display', 'block');

            $(mapdata.SELECTORS.btnSaveProperties).css('display', 'block');

            $(mapdata.SELECTORS.deletegeojson).css('display', 'block');
            $(mapdata.SELECTORS.btnDeleted).css('display', 'block');

            $(informap2d.SELECTORS.btnDeleteMap2D).css('display', 'block');

            $(documentObject.SELECTORS.btnDeleted).css('display', 'block');
        }
    },
    //highligt point and multi point
    highlightObjectPoint: function (lat, lng) {
        mapdata.clearHighlightPoint();
        //var icon = args.getIcon();
        //args.setIcon(new map4d.Icon(38, 38, icon.url));
        //args.setZIndex(4);
        let markerDraw = new map4d.Marker({
            position: { lat: lat, lng: lng },
            icon: new map4d.Icon(35, 35, "/common/location-1.png"),
            anchor: [0.5, 1.0],
            draggable: false,
            windowAnchor: { x: 0.5, y: 0.4 }
            //title: name
        });

        mapdata.GLOBAL.listHighlightPoint.push(markerDraw);
        if (!map.is3dMode()) {
            markerDraw.setMap(map);
        }
        else {
            markerDraw.setMap(null);
        }
    },
    highlightObjectMutiplePoint: function (array, point) {
        mapdata.clearHighlightPoint();
        //var icon = args.getIcon();
        //args.setIcon(new map4d.Icon(38, 38, icon.url));
        //args.setZIndex(4);
        for (var i = 0; i < array.length; i++) {
            let markerDraw = new map4d.Marker({
                position: { lat: array[i].lat, lng: array[i].lng },
                icon: new map4d.Icon(35, 35, "/common/location-1.png"),
                anchor: [0.5, 1.0],
                zIndex: 99,
                draggable: false,
                windowAnchor: { x: 0.5, y: 0.4 }
                //title: name
            })
            //var html = `
            //        <div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
            //            <div class="gm-style-iw-a" style="position: absolute;top: 40px;">
            //                <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
            //                    <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px;">
            //                        <img src= "${location.origin}/images/marker-yellow.png" style="width: 35px;" />
            //                    </div>
            //                </div>
            //            </div>
            //        </div>`;

            //markerDraw.setInfoWindow(html);
            //markerDraw.showInfoWindow();
            mapdata.GLOBAL.listHighlightPoint.push(markerDraw);
            if (!map.is3dMode()) {
                markerDraw.setMap(map);
            }
            else {
                markerDraw.setMap(null);
            }
        }

    },
    clearHighlightPoint: function () {
        if (mapdata.GLOBAL.listHighlightPoint !== null && mapdata.GLOBAL.listHighlightPoint.length > 0) {
            $.each(mapdata.GLOBAL.listHighlightPoint, function (i, obj) {
                obj.setMap(null);
            });
            mapdata.GLOBAL.listHighlightPoint = [];
        }
    },
    visibleHighlightPoint: function (type) {
        var mapView = map;
        if (!type) {
            mapView = null;
        }
        if (mapdata.GLOBAL.listHighlightPoint !== null && mapdata.GLOBAL.listHighlightPoint.length > 0) {
            $.each(mapdata.GLOBAL.listHighlightPoint, function (i, obj) {
                obj.setMap(mapView);
            });
        }
    },
    ResetInsertMainOjbect: function () {
        $('#InforData .li-modal-data-right').removeClass("active");
        $(modalCreateFolderLayer.SELECTORS.modal).removeClass('active');

        //mapdata.setOrRestBtnInsert(false);

        $(mapdata.SELECTORS.newcomondrawmodal).removeClass('button-active');
        $(mapdata.SELECTORS.newcomondrawmodal).attr('disabled', false);

        $(mapdata.SELECTORS.newgeojsonbutton).removeClass('disabled');
    },
    ResetEditMainObject: function () {
        if (mapdata.GLOBAL.selectbtn == "edit" && mapdata.GLOBAL.selectObject2D != null) {
            map.data.add(mapdata.GLOBAL.selectObject2D.feature); // add lại geojson 2d edit
        }
        if (map.is3dMode()) // nếu là trạng thái 3d thì ẩn clear geojson 2d
        {
            map.data.clear();
        }
        mapdata.GLOBAL.idMainObject = '';
        mapdata.GLOBAL.mainObjectSelected = null;
        model3D.hideModel3D();
        model3D.resetAll();
        mapdata.clearSelectDrawHighlight();
        mapdata.showAllObject3D();
        documentObject.GLOBAL.lstDocument = [];
        mapdata.clearHighlight();
        mapdata.clearHighlightPoint();
        map.setSelectedBuildings([]);
    },
    deleteMainObject: function (idDelete) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_AJAXDELETEDDATA + '?id=' + idDelete,
            data: {
            },
            success: function (data) {
                if (data.code == "ok") {
                    abp.notify.success(l("ManagementLayer:DeleteInformationSuccessfully"));
                    mapdata.updateMainObject();

                    mapdata.hideInfoData();
                    menuLeft.resizeMenuLeft();

                    var indexFeatures = mapdata.GLOBAL.features.findIndex(x => x._id === idDelete); // xóa 2d
                    if (indexFeatures != -1) {
                        map.data.remove(mapdata.GLOBAL.features[indexFeatures]);
                        mapdata.GLOBAL.features.splice(indexFeatures, 1);
                    }

                    var indexDirectory = mapdata.GLOBAL.listObjectByDirectory.findIndex(x => x.id == idDelete);
                    if (indexDirectory != -1) {
                        mapdata.GLOBAL.listObjectByDirectory.splice(indexDirectory, 1);
                    }

                    var index3d = mapdata.GLOBAL.listObject3D.findIndex(x => x.id == idDelete) // xóa 3d
                    if (index3d != -1) {
                        if (mapdata.GLOBAL.listObject3D[index3d].object != null && mapdata.GLOBAL.listObject3D[index3d].object != undefined) {
                            mapdata.GLOBAL.listObject3D[index3d].object.setMap(null);
                            mapdata.GLOBAL.listObject3D.splice(index3d, 1);
                        }
                    }

                    mapdata.GLOBAL.selectObject2D = null;

                    $("li[menuid='" + idDelete + "']").remove();
                    totalMain = parseInt(totalMain) - 1; //cập nhật lại số lượng
                    $('.totalMainObject').html(`(${totalMain})`);


                } else {
                    ;
                    abp.notify.error(l("ManagementLayer:DeleteInformationFailed"));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },

    // get list parent directory
    GetListParentDirectory: function () {
        $.ajax({
            type: "GET",
            url: mapdata.CONSTS.URL_GETLIST_PARENT_DIRECTORY + "?id=" + mapdata.GLOBAL.idDirectory,
            data: {},
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok") {
                    if (data.result.length > 0) {
                        $('.breadcrumb').html('');
                        var layerOption = `<li class="breadcrumb-item"><a href="/"><i class="fa fa-home"></i> </a></li>
                                            <li class="breadcrumb-item active"><a style="color:#000;" href="/HTKT/ManagementLayer">${l('Menu:ManagementLayer')}<a></li>`;
                        layerOption += `<i class="fa fa-chevron-right" style="padding-left:0.5rem;padding-right:0.5rem"></i>`;

                        for (var i = 0; i < data.result.length; i++) {
                            if (data.result[i].level != 0) {
                                var url = `${location.origin}/HTKT/ManagementData/MapData?id=${data.result[i].id}`;
                                if (data.result[i].type == "folder") {
                                    url = `${location.origin}/HTKT/ManagementLayer?id=${data.result[i].id}`;
                                }

                                layerOption += `<li><a style="${i != data.result.length - 1 ? `color:#000;` : ""}" href="${url}">${data.result[i].name} ${i == data.result.length - 1 ? `<span class="totalMainObject">(${totalMain})</span>` : ""}<a></li>`;

                                if (i != data.result.length - 1) {
                                    layerOption += `<i class="fa fa-chevron-right" style="padding-left:0.5rem;padding-right:0.5rem"></i>`;
                                }
                            }
                        }

                        $('.breadcrumb').html(layerOption);
                    }
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },

    ResetIcon: function () {
        $(mapdata.SELECTORS.menuMap + " .focus").each(function () {
            var type = $(this).attr("data-type");
            switch (type) {
                case "1":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <defs>
                                        </defs>
                                        <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                        <path class="b" style="fill: rgba(0,0,0,0.56);" d="M15,4H5V20H19V8H15ZM3,2.992A1,1,0,0,1,4,2H16l5,5V20.993A1,1,0,0,1,20.007,22H3.993A1,1,0,0,1,3,21.008ZM11,11V8h2v3h3v2H13v3H11V13H8V11Z" />
                                    </svg>`);
                    break;
                case "2":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <defs>
                                        </defs>
                                        <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                        <path class="b" style="fill: rgba(0,0,0,0.56);" d="M16.757,3l-2,2H5V19H19V9.243l2-2V20a1,1,0,0,1-1,1H4a1,1,0,0,1-1-1V4A1,1,0,0,1,4,3Zm3.728-.9L21.9,3.516l-9.192,9.192-1.412,0,0-1.417Z" />
                                    </svg>`);
                    break;
                case "3":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <defs>
                                        </defs>
                                        <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                        <path class="b" style="fill: rgba(0,0,0,0.56);" d="M20,7V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H2V5H22V7ZM6,7V20H18V7ZM7,2H17V4H7Zm4,8h2v7H11Z" />
                                    </svg>`);
                    break;
                case "5":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <defs>
                                        </defs>
                                        <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                        <path class="b" style="fill: rgba(0,0,0,0.56);" d="M15,4H5V20H19V8H15ZM3,2.992A1,1,0,0,1,4,2H16l5,5V20.993A1,1,0,0,1,20.007,22H3.993A1,1,0,0,1,3,21.008ZM13,12v4H11V12H8l4-4,4,4Z" />
                                    </svg>`);
                    break;
                default:
            }
        })
    },
    eventTreeView: function () {
        $(".tree-o-i").on('click', '.item-li', function () {
            var id = $(this).attr('data-id');
            var isFolder = $(this).attr('data-type');

            if ($(`#checkmark-${id}`).hasClass('check')) {
                mapdata.clearHighLightTreeview($(this).prev().find('.checkmark'));
                mapdata.setHighLightTreeview($(this).prev().find('.checkmark'));

                if (mapdata.GLOBAL.DirectoryForcus != id) {
                    if (!$('#lst-Object').hasClass('open')) {
                        $('#lst-Object').addClass('open')
                    }

                    mapdata.GLOBAL.DirectoryForcus = id;
                    mapdata.GLOBAL.ListPropertiesFocus = [];
                }
            }

            if (!$(this).next().hasClass('open')) {
                $(this).next().addClass('open');
                $(this).next().slideToggle("slow");

                var element = $(this).find('.icon-menu-tree');
                if (element.length > 0) {
                    element.addClass('open')
                }
            }
            else {
                $(this).next().slideToggle("slow");
                $(this).next().removeClass('open');
                var element = $(this).find('.icon-menu-tree');
                if (element.length > 0) {
                    element.removeClass('open')
                }
            }
            if (isFolder != "true") {
                mapdata.GetPropertiesDirectory(id);
            }
        });

        $(document).on('click', mapdata.SELECTORS.closeTreeView, function () {
            //if ($(mapdata.SELECTORS.leftTree).hasClass('open')) {
            //    $(mapdata.SELECTORS.leftTree).removeClass('open');
            //    //$('#title-treelayer').attr('title', 'Mở');
            //} else {
            //    $(mapdata.SELECTORS.leftTree).addClass('open');
            //    //$('#title-treelayer').attr('title', 'Đóng');
            //}
            $(this).css('display', 'none');
            $(mapdata.SELECTORS.buttonClose).css('display', 'block');
            $(mapdata.SELECTORS.leftTree).addClass('open');
        });

        $(document).on('click', mapdata.SELECTORS.buttonClose, function () {
            $(this).css('display', 'none');
            $(mapdata.SELECTORS.closeTreeView).css('display', 'flex');
            $(mapdata.SELECTORS.leftTree).removeClass('open');
        });
    },
    GetPropertiesDirectory: function (id) {
        var result = null;
        $.ajax({
            type: "GET",
            url: mapdata.CONSTS.URL_GET_PROPERTIES_DIRECTORY + "/" + id + "/async-by-id",
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                result = data;
                if (mapdata.GLOBAL.ListPropertiesFocus.length == 0) {
                    mapdata.GLOBAL.ListPropertiesFocus = data.listProperties;
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });

        return result;
    },
    //set highlight treeview
    setHighLightTreeview: function (el) {
        $(mapdata.SELECTORS.titleItem).removeClass("active-treeview");
        if ($(el).parent().parent().find(".title-item").length > 0) {
            $(el).parent().parent().find(".title-item").eq(0).addClass("active-treeview");
        }
    },
    clearHighLightTreeview: function (el) {
        $(el).parent().parent().find(".title-item").removeClass("active-treeview");
    },
    GetListLayer: function () {
        $.ajax({
            type: "GET",
            url: mapdata.CONSTS.URL_GETLISTDIRECTORY,
            data: {
            },
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.result.length > 0) {
                    mapdata.GLOBAL.ArrayTree = data.result;

                    var parent = data.result.find(x => x.level == "0");
                    var treedynamic = mapdata.FillDynaTree(parent);
                    //var arrayTree = [];
                    //arrayTree.push(treedynamic);
                    //TreeViewLayer.LoadDynaTree(arrayTree);

                    //console.log(treedynamic);
                    var html = mapdata.DataTreeGen(treedynamic, 1);

                    $(`.tree-o-i`).html(html);

                    $(`.item-li`).each(function () {
                        if ($(this).find('.title-item').text() == "") {
                            $(this).remove();
                        }

                        var isFolder = $(this).attr('data-type');
                        var id = $(this).attr('data-id');

                        if (isFolder != "true") {
                            if (id == mapdata.GLOBAL.idDirectory) {

                                var checkbox = `<label class="container-checkbox">
                                              <input type="checkbox" disabled>
                                              <span class="checkmark disabled" id="checkmark-${id}" data-id="${id}" style="border-radius: 2px;"></span>
                                            </label>`;

                                $(this).parent().prepend(checkbox);
                            } else {
                                var checkbox = `<label class="container-checkbox">
                                              <input type="checkbox">
                                              <span class="checkmark" id="checkmark-${id}" data-id="${id}" style="border-radius: 2px;"></span>
                                            </label>`;

                                $(this).parent().prepend(checkbox);
                            }
                        }
                    });

                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    //dynatree
    FillDynaTree: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            icon: null,
            parentId: data.parentId
        };

        var parent = mapdata.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    if (data.tags != null) {
                        obj.icon = data.tags["icon"];
                    }
                }
            }

            obj.key = parent.id;

            var lstChil = mapdata.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = mapdata.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    // autoGen
    DataTreeGen: function (data, stt) {
        //$('.title').html(data.title);

        var tree = data.children;

        var html = ``;

        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;
            if (tree[i].icon == "" || tree[i].icon == null) {
                icon = `<i class="fa ${tree[i].isFolder ? "fa-folder-open" : "fa-file"}" style="margin-top: 2px;"></i>`;
            }

            if (tree[i].children.length > 0) {
                html += `<li id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-id="${tree[i].key}" data-parent="${tree[i].parentId}" data-stt="${stt}" data-chil="${tree[i].children.length}" data-search="${encodeURIComponent(name.toLowerCase())}">
                            <a href="javascript:;" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                <span class="pull-left-container" style="margin-top: 0px;">
                                    <i class="fa fa-angle-right pull-left icon-menu-tree icon-menu-tree-${tree[i].key}"></i>
                                </span>
                                <span class="title-item" style= "padding-left:${tree[i].isFolder ? "1.5rem;" : "2.8rem;"}">${icon} ${name} (${tree[i].children.length})</span>

                                <ul class="tree-ul" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:none;padding-left: 20px;">`;

                html += mapdata.DataTreeGen(tree[i], stt + 1);

                html += `
                                </ul>
                            </a>
                        </li>`;
            }
            else {
                html += `<li id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-id="${tree[i].key}" data-parent="${tree[i].parentId}" data-chil="${tree[i].children.length}" data-search="${encodeURIComponent(name.toLowerCase())}">
                                <a href="javascript:;" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                <span class="title-item" style= "padding-left:${tree[i].isFolder ? "1.5rem;" : "2.8rem;"}">${icon} ${name}</span></a>    
                           </li>`;
            }
        }

        return html;
    },
    //Render HTML of distric and ward distric for New polygon
    RenderHTMLForNewPolyGon: function (name, type, modal) {
        var modalIsDisplay = '';
        var text = ``;
        var placeHolder = '';
        var check = type == "newMultipolygonByCountry" ? true : false;
        if (name == "District") {
            placeHolder = l('District');
        }
        else if (name == "WardDistrict") {
            placeHolder = l('WardDistrict');
        }
        var option = mapdata.GLOBAL.District;
        text += `<div class="form-group" style="margin-bottom: 0; width: 100%;">
                                    <div class="w-100 position-relative">
                                        <div class="form-group form-group-modal">`;
        text += `<select id="text-modal-${name}" data-name="${name}" class="form-control modal-layer select-${name}" data-type="list" data-required=true required ${check ? "multiple = 'multiple''" : ""}>`;
        text += `<option></option>`;
        for (var i = 0; i < option.length; i++) {
            text += `<option value="${option[i].code}">${option[i].name}</option>`;
        }
        text += ` </select>`;
        //text += `<span class="placeholder">${placeHolder} <span class="required"></span></span>`;
        text += `</div>
                                    </div>
                                        </div>`;
        if (modal == "Create") {
            $(mapdata.SELECTORS.modalbody).append(text);
            modalIsDisplay = '#myModal2';
        }
        else {
            $(mapdata.SELECTORS.inforData + " " + Info_Properties_Main_Object.SELECTORS.infor_basic).append(text);
            modalIsDisplay = '#InforData';
        }
        $(modalIsDisplay + ' #text-modal-' + name).select2({
            placeholder: "Chọn Quận/Huyện",
            allowClear: true
        })
            .on("select2:opening", function () {
                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
            })
            .on("select2:close", function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                }
                else {
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            })
            .on('select2:select', function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                    if (mapdata.GLOBAL.mainObjectSelected != null) {
                        $(this).trigger('change');
                    }
                    //mapdata.RenderHTMLForNewPolyGonOfWardDistrict("4", value);
                    map.data.clear();
                    mapdata.RenderHTMLForNewPolyGonOfWardDistrict(value, type, modal)
                    mapdata.GetGeoJsonOfDistrictOrWardDistrict(value);
                    mapdata.RenderOldPolygon(mapdata.GLOBAL.listObjectByDirectory);
                }
            })
            .on('select2:unselect', function () {
                var value = $(this).val();
                var optionSelect = '<option value=""></option>';
                $(mapdata.SELECTORS.selectWardDistrict).html(optionSelect);
                map.data.clear();
                mapdata.RenderOldPolygon(mapdata.GLOBAL.listObjectByDirectory);
            });
        $(modalIsDisplay + ' #text-modal-' + name).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
    },
    //html phuong xa
    RenderHTMLForNewPolyGonOfWardDistrict: function (code, type, modal) {
        var modalIsDisplay = '';
        var text = ``;
        var check = type == "newMultipolygonByCountry" ? true : false;
        var name = "WardDistrict";
        var placeHolder = l("WardDistrict");
        var option = []
        var optionSelect = '<option value=""></option>';
        if (code !== "") {
            if (check == true) {
                for (var i = 0; i < code.length; i++) {
                    option = option.concat(mapdata.GetDataOfDistrictAndWardDistrict("4", code[i]));
                }
            }
            else {
                option = mapdata.GetDataOfDistrictAndWardDistrict("4", code);
            }
        }
        text += `<div class="form-group" style="margin-bottom: 0; width: 100%;">
                                    <div class="w-100 position-relative">
                                        <div class="form-group form-group-modal">`;
        text += `<select id="text-modal-${name}" data-name="${name}" class="form-control modal-layer select-${name}" data-type="list" data-required=false ${check ? "multiple = 'multiple''" : ""}>`;
        if (option.length > 0) {
            for (var i = 0; i < option.length; i++) {
                optionSelect += `<option value="${option[i].code}">${option[i].name}</option>`;
            }
            if (modal == "Create") {
                modalIsDisplay = '#myModal2 ';
            } else {
                modalIsDisplay = '#InforData ';
            }
            //text += `<span class="placeholder">${placeHolder} <span class="required"></span></span>`;
            if (check) {
                $(modalIsDisplay + mapdata.SELECTORS.selectWardDistrict).append(optionSelect);
            }
            else {
                $(modalIsDisplay + mapdata.SELECTORS.selectWardDistrict).html(optionSelect);
            }

        }
        else {
            text += optionSelect;
            text += ` </select>`;
            //text += `<span class="placeholder">${placeHolder} <span class="required"></span></span>`;
            text += `</div>
                                    </div>
                                        </div>`;
            if (modal == "Create") {
                $(mapdata.SELECTORS.modalbody).append(text);
                modalIsDisplay = '#myModal2';
            }
            else {
                $(mapdata.SELECTORS.inforData + " " + Info_Properties_Main_Object.SELECTORS.infor_basic).append(text);
                modalIsDisplay = '#InforData';
            }
        }
        $(modalIsDisplay + ' #text-modal-' + name).select2({
            placeholder: "Chọn Phường/Xã",
            allowClear: false
        })
            .on("select2:opening", function () {
                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
            })
            .on("select2:close", function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                }
                else {
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            })
            .on('select2:select', function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                    if (mapdata.GLOBAL.mainObjectSelected != null) {
                        $(this).trigger('change');
                    }
                    map.data.clear();
                    mapdata.GetGeoJsonOfDistrictOrWardDistrict(value);
                    mapdata.RenderOldPolygon(mapdata.GLOBAL.listObjectByDirectory);
                    //$(this).trigger('change');
                }
            })
            .on('select2:unselect', function () {
                map.data.clear();
                mapdata.GetGeoJsonOfDistrictOrWardDistrict($(mapdata.SELECTORS.selectDistrict).val());
                mapdata.RenderOldPolygon(mapdata.GLOBAL.listObjectByDirectory);
            });
        $(modalIsDisplay + ' #text-modal-' + name).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
    },
    GetGeoJsonOfDistrictOrWardDistrict: function (code) {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_GET_GEOJSON_DISTRICT_AND_WARDDISTRICT,
            data: {
                code: code
            },
            success: function (data) {
                if (data.code == "ok") {
                    var arrayResult = [];
                    arrayResult.push(data.result);
                    console.log(data.result);
                    mapdata.GLOBAL.cordinatesOfPolygon = data.result.geometry.coordinates;
                    var geojson = mapdata.getDataGeojsonByProperties(arrayResult);
                    //mapdata.GLOBAL.lstGeoJsonDefault.push(geojson);
                    mapdata.showDataGeojsonPush(JSON.stringify(geojson));
                }
            }
        });

    },
    // get data for district and warddistrict
    GetDataOfDistrictAndWardDistrict: function (level, code = null) {
        var response = [];
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_GET_DATA_DISTRICT_AND_WARDDISTRICT,
            data: {
                level: level,
                code: code == null ? AppSetting.CountryCode : code
            },
            success: function (data) {
                if (data.code == "ok") {
                    if (level == "3") {
                        mapdata.GLOBAL.District = data.result;
                    }
                    response = data.result;
                }
            }
        });
        return response;
    },
    //render again old Polygon on database
    RenderOldPolygon: function (lstObject) {
        var geojson = mapdata.getDataGeojsonByProperties(lstObject);
        //mapdata.GLOBAL.lstGeoJsonDefault.push(geojson);
        mapdata.showDataGeojsonPush(JSON.stringify(geojson));
    },
    //saving geojson 
    RenderJsonIntoFeature: function () {
        let features = [];
        let geojson = { "type": "FeatureCollection", "features": [] };
        let feature = {
            "type": "Feature",
            "properties": {
                "stroke": mapdata.GLOBAL.propertiesSetting.Stroke,
                "stroke-width": mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                "stroke-opacity": mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                "fill": mapdata.GLOBAL.propertiesSetting.Fill,
                "fill-opacity": mapdata.GLOBAL.propertiesSetting.FillOpacity
            },
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": mapdata.GLOBAL.cordinatesOfPolygon
            }
        };
        features.push(feature);
        geojson.features = features;
        return geojson;
    }
}

    //RenderHTMLForComboBox: function (name, option) {
    //    debugger;
    //    var text = ``;
    //    var placeHolder = name;
    //    var id = "PlanToDo"
    //    var check = false;
    //    text += `<div class="form-group" style="margin-bottom: 0; width: 100%;">
    //                                <div class="w-100 position-relative">
    //                                    <div class="form-group form-group-modal">`;
    //    text += `<select id="text-modal-${id}" data-name="${name}" class="form-control modal-layer" data-type="list" data-required=true required ${check ? "multiple = 'multiple''" : ""}>`;
    //    for (var i = 0; i < option.length; i++) {
    //        text += `<option value="${i}">${option[i]}</option>`;
    //    }
    //    text += ` </select>`;
    //    text += `<span class="placeholder">${placeHolder} <span class="required"></span></span>`;
    //    text += `</div>
    //                                </div>
    //                                    </div>`;
    //    $(mapdata.SELECTORS.modalbody).append(text);
    //    $('#myModal2 #text-modal-' + name).select2()
    //        .on("select2:opening", function () {
    //            $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
    //        })
    //        .on("select2:close", function () {
    //            var value = $(this).val();
    //            if (value != "" && value != null) {
    //                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
    //            }
    //            else {
    //                $(this).parent().find('.placeholder').attr('style', "");
    //            }
    //        })
    //        .on('select2:select', function () {
    //            var value = $(this).val();
    //            if (value != "" && value != null) {
    //                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
    //                $(this).trigger('change');
    //            }
    //        });
    //    $('#myModal2 #text-modal-' + name).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
    //}
