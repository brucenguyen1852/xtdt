﻿
var mapdataTreeView = {
    GLOBAL: {
        listObjectDirectory: [],
        lstArrayQTSC: [],
        listDirectoryMainObject: [],
        listMarker: [], // danh sách marker trên bản đồ
        listObject3D: [], // danh sách building trên bản đồ
        listgeojson: [], // danh sách những object dạng line, mutipleline, point,... trên bản đồ
        image2dDirectory: '',
        lstFeature: [],
        lstFeatureGroup: [],
        listGroundOvelay: [],
        propertiesSetting: {
            MinZoom: 0,
            MaxZoom: 22,
            Image2D: "",
            Stroke: "#8b5b5b",
            StrokeWidth: 2,
            StrokeOpacity: 1,
            StyleStroke: "",
            Fill: "#8b5b5b",
            FillOpacity: 0.3,
        },
        defaultConfig: {
            zoom: 0,
            lat: 0,
            lng: 0
        },
    },
    CONSTS: {
        URL_AJAXDEDITDATA: '/api/HTKT/ManagementData/get-object-by-IdDirectory',
        URL_AJAXGETDEFAULTDATA: '/api/HTKT/PropertiesDirectory/get-directoryById',
    },
    SELECTORS: {
        search_tree: ".search-tree",
        drop_action_draw: ".drop-action-draw",
    },
    init: function () {
        mapdataTreeView.setEvent();
    },
    setEvent: function () {
        $(".tree-o-i").on('click', '.checkmark', function () {
            let id = $(this).attr('data-id');

            var dem = $('.checkmark.check').length;
            if (!$(this).hasClass('disabled')) {
                if (!$(this).hasClass('check')) {
                    if (dem >= 6) {
                        swal({
                            title: "Thông báo",
                            text: "Không được bật quá 06 lớp dữ liệu nền",
                            icon: "warning",
                            button: "Đóng",
                        });
                    }
                    else {
                        mapdata.GLOBAL.DirectoryForcus = id;
                        mapdata.GLOBAL.ListPropertiesFocus = [];
                        $(this).addClass('check');
                        mapdataTreeView.GLOBAL.image2dDirectory = '';
                        let obj = mapdata.GLOBAL.ArrayTree.find(x => x.id == id);
                        if (obj != null && typeof (obj) != "undefined") {
                            if (obj.image2D != "" && obj.image2D != null && typeof (obj.image2D) != "undefined") {
                                let array = obj.image2D.split('.');
                                if (array[array.length - 1].toLocaleLowerCase() == "png" || array[array.length - 1].toLocaleLowerCase() == "jpg"
                                    || array[array.length - 1].toLocaleLowerCase() == "jpeg") {
                                    mapdataTreeView.GLOBAL.image2dDirectory = obj.image2D;
                                }
                            }
                        }
                        mapdataTreeView.showObjectOfLayer(id);
                        setTimeout(function () {
                            mapdataTreeView.LocationMapDefault(id);
                        }, 100);

                        if (!$('#lst-Object').hasClass('open')) {
                            $('#lst-Object').addClass('open')
                        }

                        mapdata.setHighLightTreeview(this);

                        mapdata.GetPropertiesDirectory(id);
                    }
                }
                else {
                    // xoa id qtsc
                    $('.spinner').css('display', 'block');
                    var arrayObjQTSC = mapdataTreeView.GLOBAL.listObject3D.filter(x => x.idLayer == id && x.idObject !== "");
                    if (arrayObjQTSC != undefined) {
                        var arrayIdQTSC = arrayObjQTSC.map(x => x.idObject);
                        var difference = [];

                        jQuery.grep(arrayIdQTSC, function (el) {
                            if (jQuery.inArray(el, mapdataTreeView.GLOBAL.lstArrayQTSC) == -1) difference.push(el);
                        });

                        mapdataTreeView.GLOBAL.lstArrayQTSC = difference;
                    }
                    //exploit.GLOBAL.lstArrayQTSC listObject3D

                    $(this).removeClass('check');
                    //TreeViewLayer.GLOBAL.ArrayTree.push(id);

                    setTimeout(function () {
                        mapdataTreeView.HideObjectOfLayer2(id);
                        mapdataTreeView.HideLocationMapDefault(id);
                    }, 100);

                    mapdata.GLOBAL.DirectoryForcus = "";

                    mapdata.clearHighLightTreeview(this);

                    setTimeout(function () {
                        $('.spinner').css('display', 'none');
                    }, 500);
                }
            }
        });

        $(mapdataTreeView.SELECTORS.search_tree).on('keyup', delay(function () {
            $(".tree-o-i .treeview-not-result").remove();
            $('.tree-o-i .treeview').hide();
            var value = $(this).val().toLowerCase().trim();
            if (value != "") {
                $('.tree-o-i .treeview').each(function () {
                    var id = $(this).attr('data-id');
                    var name = decodeURIComponent($(this).attr('data-search'));
                    var chilNumber = parseInt($(this).attr('data-chil'));
                    var parent = $(this).attr('data-parent');
                    var stt = $(this).attr('data-stt');
                    if (xoa_dau(name.trim().replace(/\s\s+/g, ' ').toLowerCase()).includes(xoa_dau(value.trim().replace(/\s\s+/g, ' ').toLowerCase()))) {
                        $(this).show();
                        if (chilNumber > 0) {
                            $('#tree-item-' + id).find('.icon-menu-tree-' + id).addClass('open'); // mở open icon
                            $('#tree-ul-' + id).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + id).slideDown();
                            $('#tree-ul-' + id + " li").show();
                        }

                        if (parent != "null") {
                            $('#tree-item-' + parent).find('.icon-menu-tree-' + parent).addClass('open'); // mở open icon
                            $('#tree-ul-' + parent).slideDown();

                            search_tree('#tree-item-' + parent, value);
                        }
                    }
                })

                var countNotResult = $('.tree-o-i .treeview').filter(function () {
                    return $(this).css('display') == 'none'
                }).length;

                if ($('.tree-o-i .treeview').length == countNotResult) {
                    var li = `<div class="treeview-not-result"><span>Không tìm thấy kết quả tìm kiếm</span></div>`;
                    $(".tree-o-i").append(li);
                }

            } else {
                mapdataTreeView.ReloadTree();
                $(".map-side-form .scroll-content").scrollTop(10);
            }
        }, 500));

        //$(".tree-o-i").on('click', '.item-li', function () {
        //    if ($(this).parent().find(">:first-child").find('.checkmark').length > 0) {
        //        $(this).parent().find(">:first-child").find('.checkmark').trigger('click');
        //    }
        //});
    },
    showObjectOfLayer: function (id) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: true,
            beforeSend: function () {
                $('.spinner').css('display', 'block');
            },
            url: document.location.origin + mapdataTreeView.CONSTS.URL_AJAXDEDITDATA + '?id=' + id,
            data: {
            },
            success: function (data) {
                if (data.code == "ok") {
                    var mod = map.is3dMode();

                    if (data.result != null && data.result.length > 0) {
                        if (mapdataTreeView.GLOBAL.listObjectDirectory.length > 0) {
                            var arrayAll = mapdataTreeView.GLOBAL.listObjectDirectory.concat(data.result);
                            mapdataTreeView.GLOBAL.listObjectDirectory = arrayAll;
                        }
                        else {
                            mapdataTreeView.GLOBAL.listObjectDirectory = data.result;
                        }
                        let directory = {
                            id: id,
                            listMainObject: []
                        };
                        let object = JSON.parse(JSON.stringify(data.result.filter(x => x.geometry.type != "Point" && x.geometry.type != "MultiPoint")));
                        var geojson = mapdataTreeView.getDataGeojsonByProperties(object, directory.listMainObject);
                        mapdataTreeView.showDataGeojson2(JSON.stringify(geojson), id);
                        var lstmarker = JSON.parse(JSON.stringify(data.result.filter(x => x.geometry.type == "Point" || x.geometry.type == "MultiPoint")));
                        mapdataTreeView.showListMarkerObject(lstmarker, id, directory.listMainObject);
                        mapdataTreeView.GLOBAL.listDirectoryMainObject.push(directory);
                        let object3d = data.result.filter(x => x.object3D != null);
                        $.each(object3d, function (i, obj) {

                            if (obj.object3D.type != "para-choosenMap") {
                                let object3d = mapdataTreeView.newMap4dBuilding(obj.object3D);
                                object3d.setMap(map);
                                object3d.setUserData(obj);
                                mapdataTreeView.GLOBAL.listObject3D.push({ id: obj.id, idLayer: id, object: object3d, idObject: '' });
                            } else {
                                if (obj.object3D.id != "") {
                                    mapdataTreeView.GLOBAL.lstArrayQTSC.push(obj.object3D.id);
                                }
                                mapdataTreeView.GLOBAL.listObject3D.push({ id: obj.id, idLayer: id, object: null, idObject: obj.object3D.id });
                            }
                        });
                        //mapdataTreeView.showObjectMap();
                    }
                }
            },
            complete: function () {
                $('.spinner').css('display', 'none');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
                console.log(messageErorr);
            }
        });
    },
    LocationMapDefault: function (id) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: true,
            url: mapdataTreeView.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=' + id,
            data: {
            },
            beforeSend: function () {
                $('.spinner').css('display', 'block');
            },
            success: function (data) {
                if (data.code == "ok") {
                    var mod = map.is3dMode();
                    if (data.result != null) {
                        if (data.result.location != null) {
                            if (data.result.location.lat > 0 && data.result.location.lng > 0) {
                                var cameraPosition = {
                                    target: { lat: parseFloat(data.result.location.lat), lng: parseFloat(data.result.location.lng) },
                                    tilt: 0,
                                    bearing: 0,
                                    zoom: (data.result.zoom != null ? (data.result.zoom == 0 ? 15 : data.result.zoom) : 15)
                                };

                                //map.moveCamera(cameraPosition, null);
                            } else {
                                var cameraPosition2 = {
                                    target: { lat: parseFloat(mapdataTreeView.GLOBAL.defaultConfig.lat), lng: parseFloat(mapdataTreeView.GLOBAL.defaultConfig.lng) },
                                    tilt: 0,
                                    bearing: 0,
                                    zoom: (mapdataTreeView.GLOBAL.defaultConfig.zoom != null ? (mapdataTreeView.GLOBAL.defaultConfig.zoom == 0 ? 15 : mapdataTreeView.GLOBAL.defaultConfig.zoom) : 15)
                                };

                                //map.moveCamera(cameraPosition2, null);
                            }
                        }

                        if (mod) {
                            map.enable3dMode(true);
                        }

                        if (data.result.groundMaptile != "" && data.result.groundMaptile != null) {
                            var tags = data.result.tags;
                            var objectStringArray = [];
                            if (tags != null) {
                                objectStringArray = (new Function("return " + tags["bounds"] + ";")());
                            }
                            else {
                                objectStringArray = (new Function("return " + JSON.stringify(bounds) + ";")());
                            }

                            var urlTile = data.result.groundMaptile;

                            var Arrayexp = urlTile.trim().split('.');
                            exp = Arrayexp[Arrayexp.length - 1];

                            if (exp == "" || exp != "png" && exp != "jpg") {
                                exp = "png";
                            }

                            urlTile = urlTile.trim().split('/$')[0];
                            urlTile = urlTile.trim().split('/{')[0];

                            let overlayOptions = {
                                getUrl: function (x, y, z, is3dMode) {
                                    return urlTile + `/${z}/${x}/${y}.${exp.trim()}`;
                                },
                                bounds: objectStringArray,
                                override: false
                            };
                            overlay = new map4d.GroundOverlay(overlayOptions);

                            overlay.setMap(map);

                            let object = {
                                id: id,
                                groundOvelay: overlay
                            };
                            mapdataTreeView.GLOBAL.listGroundOvelay.push(object);
                        }
                    }
                }
            },
            complete: function () {
                $('.spinner').css('display', 'none')
            },
            error: function (jqXHR, textStatus, errorThrown) {
                let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
                console.log(messageErorr);
            }
        });
    },
    getDataGeojsonByProperties: function (listMainObject, listOfDirectory) {
        let features = [];
        $.each(listMainObject, function (i, obj) {
            let properties = obj.properties == null ? mapdataTreeView.GLOBAL.propertiesSetting : obj.properties;
            let defaultProperties = {
                "stroke": properties.stroke,
                "stroke-width": properties.strokeWidth,
                "stroke-opacity": properties.strokeOpacity,
                "fill": properties.fill,
                "fill-opacity": properties.fillOpacity
            };
            let feature = {};
            feature.type = "Feature";
            feature.properties = defaultProperties;
            feature.geometry = obj.geometry;
            feature.id = obj.id;
            feature.view = true;
            features.push(feature);
            listOfDirectory.push(obj.id);
        });
        let geojson = {
            "type": "FeatureCollection",
            "features": features
        };
        return geojson;
    },
    showDataGeojson2: function (geojsonString, id) {
        var mod = map.is3dMode();
        var arr = [];
        if (!mod) {
            let features = map.data.addGeoJson(geojsonString);
            mapdataTreeView.GLOBAL.lstFeature.push(features);
            arr.push(features);
        }

        let object = {
            id: id,
            geojson: geojsonString
        };
        let check = mapdataTreeView.GLOBAL.listgeojson.find(x => x.id == id);
        if (check == null || check == undefined) {
            mapdataTreeView.GLOBAL.listgeojson.push(object);
            //console.log(mapdataTreeView.GLOBAL.listgeojson);
        }
        //console.log(mapdataTreeView.GLOBAL.listgeojson);
        let object2 = {
            id: id,
            feature: arr
        };
        let check2 = mapdataTreeView.GLOBAL.lstFeatureGroup.find(x => x.id == id);
        if (check2 == null || check2 == undefined) {
            mapdataTreeView.GLOBAL.lstFeatureGroup.push(object2);
        }
    },
    showListMarkerObject: function (lst, id, listOfDirectory) {
        let object = {
            id: id,
            lstMarker: []
        };
        var mod = map.is3dMode();
        //var html = ``;
        $.each(lst, function (i, obj) {
            let imageicon = (obj.properties.image2D != "" && obj.properties.image2D != null) ? obj.properties.image2D
                : (mapdataTreeView.GLOBAL.image2dDirectory != "" && mapdataTreeView.GLOBAL.image2dDirectory != null)
                    ? mapdataTreeView.GLOBAL.image2dDirectory : "/common/point.png";
            if (obj.geometry.type == "Point") {
                let markerDraw = new map4d.Marker({
                    position: { lat: obj.geometry.coordinates[1], lng: obj.geometry.coordinates[0] },
                    icon: new map4d.Icon(26, 26, imageicon),
                    anchor: [0.5, 1],
                    windowAnchor: { x: 0.5, y: 0.4 }
                    ///draggable: true
                    //title: name
                });
                markerDraw.setUserData(obj.id);
                markerDraw.setMap(map);

                if (mod) {
                    markerDraw.setVisible(false);
                }

                listOfDirectory.push(obj.id);
                object.lstMarker.push(markerDraw);

                var html = `
                    <div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
                        <div class="gm-style-iw-a" style="position: absolute;top: 40px;">
                            <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
                                <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px;">
                                    <img src= "${imageicon}" style="width: 25px;" />
                                </div>
                            </div>
                        </div>
                    </div>`;

                markerDraw.setInfoWindow(html);

            } else {
                $.each(obj.geometry.coordinates, function (iz, item) {
                    let markerDraw = new map4d.Marker({
                        position: { lat: item[1], lng: item[0] },
                        icon: new map4d.Icon(26, 26, imageicon),
                        anchor: [0.5, 1],
                        windowAnchor: { x: 0.5, y: 0.4 }
                        //draggable: true
                        //title: name
                    });
                    markerDraw.setUserData(obj.id);
                    markerDraw.setMap(map);
                    if (mod) {
                        markerDraw.setVisible(false);
                    }
                    object.lstMarker.push(markerDraw);

                    var html = `
                    <div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
                        <div class="gm-style-iw-a" style="position: absolute;top: 40px;">
                            <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
                                <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px;">
                                    <img src= "${imageicon}" style="width: 25px;" />
                                </div>
                            </div>
                        </div>
                    </div>`;

                    markerDraw.setInfoWindow(html);

                    let check = listOfDirectory.find(x => x == obj.id);
                    check == undefined && listOfDirectory.push(obj.id);
                });
            }

        });
        mapdataTreeView.GLOBAL.listMarker.push(object);
    },
    showObjectMap: function () {
        let objectcount = mapdataTreeView.GLOBAL.listObject3D.filter(x => x.object == null);
        if (objectcount.length > 0) {
            let array = [];
            $.each(objectcount, function (i, obj) {
                array.push(obj.idObject);
            });

            map.setVisibleBuildings(array);
        }

        //mapdataTreeView.EnableGeoMetryExample();

    },
    EnableGeoMetryExample: function () {
        var arrayDefault = mapdataTreeView.getListObjectInGeometry();
        var arrayAll = arrayDefault.concat(mapdataTreeView.GLOBAL.lstArrayQTSC);
        var dem = $('.check').length;

        if (!$(mapdataTreeView.SELECTORS.btn_show_building_out).hasClass('open')) {
            if (dem == 0) {
                map.setBuildingsEnabled(false);
            }
            else {
                if (mapdataTreeView.GLOBAL.lstArrayQTSC.length == 0) {
                    map.setBuildingsEnabled(false);
                }
                else {
                    map.setBuildingsEnabled(true);
                    //map.setBuildingsEnabled(false); // trạng thái hiển thị building của bản đồ
                    map.setVisibleBuildings(mapdataTreeView.GLOBAL.lstArrayQTSC); // hiển thị building QTSC
                    map.setHiddenBuildings(arrayDefault); // ẩn building QTSC
                }
            }
        }
        else {
            map.setBuildingsEnabled(true);
            //map.setBuildingsEnabled(true); // trạng thái hiển thị building của bản đồ
            map.setVisibleBuildings(arrayAll); // hiển thị building QTSC
            map.setHiddenBuildings([]); // ẩn bulding QTSC

        }
    },
    HideObjectOfLayer2: function (id) {
        setTimeout(function () {
            let checklayer = mapdataTreeView.GLOBAL.listgeojson.find(x => x.id == id);
            if (typeof checklayer !== "undefined" && checklayer !== null && checklayer !== "") {
                mapdataTreeView.GLOBAL.listgeojson.splice(mapdataTreeView.GLOBAL.listgeojson.findIndex(function (i) {
                    return i.id === id;
                }), 1);
                //let checkFeature = mapdataTreeView.GLOBAL.lstFeatureGroup.find(x => x.id == id);
                //if (checkFeature != null && checkFeature != undefined) {
                //    checkFeature.feature[0].forEach((feature) => {
                //        map.data.remove(feature)
                //    })
                //}
                mapdataTreeView.GLOBAL.lstFeatureGroup.splice(mapdataTreeView.GLOBAL.lstFeatureGroup.findIndex(function (i) {
                    return i.id === id;
                }), 1);
                map.data.clear();
                mapdataTreeView.GLOBAL.listgeojson.forEach((geojson) => {
                    mapdataTreeView.showDataGeojson2(geojson.geojson, geojson.id);
                });

                //Show all object of layer main after when use map.data.clear() //
                var geojson = mapdata.getDataGeojsonByProperties(mapdata.GLOBAL.listObjectByDirectory);
                mapdata.showDataGeojson(JSON.stringify(geojson));
                ///
            }
            let layerMarker = mapdataTreeView.GLOBAL.listMarker.find(x => x.id == id);
            if (layerMarker != null && layerMarker != undefined) {
                layerMarker.lstMarker.forEach((marker) => {
                    marker.setMap(null);
                });
                mapdataTreeView.GLOBAL.listMarker.splice(mapdataTreeView.GLOBAL.listMarker.findIndex(function (i) {
                    return i.id === id;
                }), 1);
            }
            let layerObject = mapdataTreeView.GLOBAL.listObject3D.filter(x => x.idLayer == id);
            if (layerObject.length > 0) {
                layerObject.forEach((marker) => {
                    marker.object != null && marker.object.setMap(null);
                    mapdataTreeView.GLOBAL.listObject3D.splice(mapdataTreeView.GLOBAL.listObject3D.findIndex(function (i) {
                        return i.id === marker.id;
                    }), 1);
                });
            }
            mapdataTreeView.showObjectMap();

            let lst = mapdataTreeView.GLOBAL.listDirectoryMainObject.filter(x => x.id == id);
            if (lst != null && lst != undefined) {
                mapdataTreeView.GLOBAL.listDirectoryMainObject.splice(mapdataTreeView.GLOBAL.listDirectoryMainObject.findIndex(function (i) {
                    return i.id === id;
                }), 1);
            }

        }, 10);
    },
    HideLocationMapDefault: function (id) {
        let object = mapdataTreeView.GLOBAL.listGroundOvelay.find(x => x.id == id);
        if (object != null && object != undefined) {
            object.groundOvelay.setMap(null);
            mapdataTreeView.GLOBAL.listGroundOvelay.splice(mapdataTreeView.GLOBAL.listGroundOvelay.findIndex(function (i) {
                return i.id === id;
            }), 1);
        }
    },
    newMap4dBuilding: function (data) {
        var building = null;
        if (data.coordinates != null && data.coordinates.length > 0) {
            building = new map4d.Building({
                name: data.name,
                id: data.id,
                scale: data.scale, // Tỉ lệ vẽ của đối tượng
                bearing: data.bearing, // Góc quay của đối tượng
                elevation: data.elevation, // Độ cao so với mặt nước biển
                position: data.location,
                coordinates: data.coordinates,
                height: data.height
            });
        } else {
            //tạo đối tượng model từ ObjectOption
            building = new map4d.Building({
                name: data.name,
                id: data.id,
                scale: data.scale, // Tỉ lệ vẽ của đối tượng
                bearing: data.bearing, // Góc quay của đối tượng
                elevation: data.elevation, // Độ cao so với mặt nước biển
                position: data.location,
                model: data.objectUrl,
                texture: data.texture
            });
        }
        return building;
    },
    ReloadTree: function () {
        var parent = mapdata.GLOBAL.ArrayTree.find(x => x.level == "0");
        var treedynamic = mapdata.FillDynaTree(parent);

        var html = mapdata.DataTreeGen(treedynamic, 1);

        $(`.tree-o-i`).html('');
        $(`.tree-o-i`).html(html);

        $(`.item-li`).each(function () {
            if ($(this).find('.title-item').text() == "") {
                $(this).remove();
            }
            var isFolder = $(this).attr('data-type');
            var id = $(this).attr('data-id');

            if (isFolder != "true") {
                if (id == mapdata.GLOBAL.idDirectory) {

                    var checkbox = `<label class="container-checkbox">
                                              <input type="checkbox" disabled>
                                              <span class="checkmark disabled" id="checkmark-${id}" data-id="${id}" style="border-radius: 2px;"></span>
                                            </label>`;

                    $(this).parent().prepend(checkbox);
                } else {
                    var checkbox = `<label class="container-checkbox">
                                              <input type="checkbox">
                                              <span class="checkmark" id="checkmark-${id}" data-id="${id}" style="border-radius: 2px;"></span>
                                            </label>`;

                    $(this).parent().prepend(checkbox);
                }
            }
        });
    },
}

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    //str = str.replace(/\s/g, '');
    return str;
}

function search_tree(element, value) {
    if ($(element).css("display") == "none") {
        $(element).show();

        var id = $(element).attr('data-id');
        var name = $(element).attr('data-search');
        var parent = $(element).attr('data-parent');

        $('#tree-ul-' + id).slideDown();

        if (name != undefined) {
            //if (name.toLowerCase().includes(value))
            {
                if (parent != "null") {
                    // hiển thị ul của cây
                    $('#tree-ul-' + parent).addClass('open'); // mở nhánh con

                    search_tree('#tree-item-' + parent, value);
                }
            }
        }
    }
}