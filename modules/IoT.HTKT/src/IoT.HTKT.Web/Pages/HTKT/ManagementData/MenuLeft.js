var l = abp.localization.getResource('HTKT');
var menuLeft = {
    GLOBAL: {
        page: 0,
        inCallback: false,
        isReachedScrollEnd: false,
        lst: [],
        lstTemporary: [],
        lat: 0,
        lng: 0,
        boundLatLng: [],
        lstNew: []
    },
    CONSTS: {

    },
    SELECTORS: {
        name: '.name-menu-left',
        itemMenu: '.MenuLeftId',
        treeview_item: ".treeview",
        divContent: '.detail-property-content2',
        ulLeft: '.ul-left',
        liLeft: '.ul-left li',
        spinner: "div#spinner-menu-left",
        textSearch: "#search-Menu-Left",
        headerTopLayer: ".navbar-static-top .header-layer",
        selected_properties: "#select-search-properties",
        selected_properties_table: "#selected-search-table",
        content_step_info_data: "#InforData .step-info"
    },
    int: function () {
        menuLeft.setUpEvent();
        menuLeft.setDataLayer();
        menuLeft.Select2Search();
    },
    setUpEvent: function () {
        //menuLeft.GLOBAL.lst = [];
        let status = menuLeft.appendMenuLeft();
        //$(menuLeft.SELECTORS.name).append(mapdata.GLOBAL.nameDirectory + `(` + menuLeft.GLOBAL.lst.length + ` đối tượng)`);
        $(menuLeft.SELECTORS.divContent).scroll(menuLeft.scrollHandler);
        window.onresize = menuLeft.resizeMenuLeft;

        $(document).on('click', menuLeft.SELECTORS.treeview_item, function () {
            let value = $(this).attr('menuid');
            if (value != undefined) {
                switch (mapdata.GLOBAL.selectbtn) {
                    case "edit":
                        mapdata.GLOBAL.idMainObject = $(this).attr('menuid');
                        editform.OpenFormEdit($(this).attr('menuid'));
                        documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                        break;
                    case "delete":
                        //console.log("delete", $(this).attr('menuid'));
                        //highlight
                        var obj = mapdata.GLOBAL.features.find(x => x._id === $(this).attr('menuid'));
                        if (!map.is3dMode()) {
                            mapdata.highlightObjectMenuLeft(obj);
                        }
                        //highlight menu left
                        $($('a[menuid="' + $(this).attr('menuid') + '"]')[0]).parent().addClass("menu-open");

                        mapdata.clearHighlightPoint(); //clear highlight khi chọn đối tượng khác
                        if (obj != undefined) {
                            if (obj._geometry._coordinate !== undefined) {
                                mapdata.highlightObjectPoint(menuLeft.GLOBAL.lat, menuLeft.GLOBAL.lng);
                            }

                            if (obj._geometry._coordinates !== undefined) {
                                mapdata.highlightObjectMutiplePoint(obj._geometry._coordinates);
                            }
                        }

                        var idDelete = $(this).attr('menuid');
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:DoYouWantDeleteThisObject"),
                            icon: "warning",
                            buttons: [
                                l("ManagementLayer:Cancel"),
                                l("ManagementLayer:Remove")
                            ],
                            dangerMode: false,
                        }).then((willDelete) => {
                            if (willDelete) {
                                mapdata.deleteMainObject(idDelete);
                            }
                            $($('a[menuid="' + idDelete + '"]')[0]).parent().removeClass("menu-open");
                            mapdata.clearHighlight();
                            mapdata.clearHighlightPoint();
                        });
                        break;
                    default:
                        // tìm kiếm feature
                        mapdata.GLOBAL.selectObject2D = null;

                        var obj = mapdata.GLOBAL.features.find(x => x._id === value);
                        if (obj != null && obj != undefined) {
                            mapdata.GLOBAL.selectObject2D = {
                                feature: obj,
                                location: { lat: menuLeft.GLOBAL.lat, lng: menuLeft.GLOBAL.lng }
                            };
                        }

                        modalCreateFolderLayer.resetTableData();
                        mapdata.visibleHightlight(false);
                        if (!model3D.GLOBAL.checkdraw) {
                            // ẩn 3d
                            let objold = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
                            if (objold != undefined && objold != null) {
                                objold.object.setSelected(false);
                            }
                            map.setSelectedBuildings([]);

                            //if (mapdata.GLOBAL.mainObjectSelected == null) {
                            if (mapdata.GLOBAL.selectbtn === "") // kiểm tra trạng thái action
                            {
                                model3D.GLOBAL.geojsonDefault3D = "";
                                model3D.GLOBAL.geojson3D = {
                                    type: "Polygon",
                                    coordinates: []
                                };
                                model3D.GLOBAL.objectGeojson3D = null;

                                if (mapdata.GLOBAL.selectObject2D != null) // hightlight đối tượng dạng polygon
                                {
                                    mapdata.highlightObject(mapdata.GLOBAL.selectObject2D);
                                }

                                mapdata.GLOBAL.idMainObject = value; // set id đối tượng hiện tại
                                model3D.GLOBAL.idObject2D = value; // set id2d (check vùng vẽ 3d)

                                // bôi đen menu
                                $('.ul-left li').removeClass("menu-open");
                                $($('a[menuid="' + mapdata.GLOBAL.idMainObject + '"]')[0]).parent().addClass("menu-open");

                                $('#InforData').addClass('active'); // mở content thông tin

                                mapdata.getMainObject("", mapdata.SELECTORS.inforData); // thấy thông tin đối tượng
                                mapdata.getDefaultObject("", mapdata.SELECTORS.inforData); // lấy thuộc tính đối tượng

                                Info_Properties_Main_Object.setValueObject(menuLeft.SELECTORS.content_step_info_data); // set value đối tượng vào form input thông tin
                                // set value cho thuộc tính 3d
                                model3D.setData();
                                mapdata.showAllObject3D();
                                model3D.setInfo3D();

                                // kiểm tra nếu đối tượng có 3d thì higlight
                                var obj3d = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject); // object 3d
                                if (obj3d != undefined && obj3d != null) { // hightlight 3d
                                    obj3d.object.setSelected(true);
                                    model3D.GLOBAL.building = obj3d.object;
                                }

                                // set thông tin 2d
                                informap2d.setInfor2D();

                                // get tài liệu của đối tượng
                                documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                                modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;

                                mapdata.clearHighlightPoint();
                                // hightlight point
                                if (mapdata.GLOBAL.selectObject2D != null) {
                                    if (mapdata.GLOBAL.selectObject2D.feature._geometry._coordinate !== undefined) {
                                        mapdata.highlightObjectPoint(mapdata.GLOBAL.selectObject2D.feature._geometry._coordinate.lat, mapdata.GLOBAL.selectObject2D.feature._geometry._coordinate.lng);
                                    }

                                    // hightlight mutiple point
                                    if (mapdata.GLOBAL.selectObject2D.feature._geometry._coordinates !== undefined) {
                                        mapdata.highlightObjectMutiplePoint(mapdata.GLOBAL.selectObject2D.feature._geometry._coordinates);
                                    }
                                }
                            }

                            Activity.GetDanhSachHoatDong(mapdata.GLOBAL.idMainObject, mapdata.GLOBAL.idDirectory);
                            //}
                            $(`li[menuid='${mapdata.GLOBAL.idMainObject}']`).addClass('menu-open');
                            $('#InforData .li-modal-data-right').eq(3).trigger('click');
                        }
                }
            }
        });

        $(menuLeft.SELECTORS.textSearch).on('keyup', delay(function () {
            $(mapdata.SELECTORS.inforData).find('.toggle-detail-property2').trigger('click');
            let value = $(this).val().trim();

            menuLeft.resizeMenuLeft();
        }, 500));

        $(menuLeft.SELECTORS.selected_properties).on("change", function () {
            var pageSize = Math.ceil($(menuLeft.SELECTORS.divContent).height() / 44);
            var numberOfRecordToskip = menuLeft.GLOBAL.page * pageSize;
            menuLeft.getEditObject($(menuLeft.SELECTORS.textSearch).val(), numberOfRecordToskip, pageSize);
            menuLeft.resizeMenuLeft();
        })
    },
    resizeMenuLeft: function () {
        $(menuLeft.SELECTORS.ulLeft).html('');
        menuLeft.GLOBAL.page = 0;
        let status = menuLeft.appendMenuLeft();
    },
    scrollHandler: function () {

        var h1 = $(menuLeft.SELECTORS.divContent).scrollTop()
        var h2 = $(menuLeft.SELECTORS.ulLeft).height() - $(menuLeft.SELECTORS.divContent).height() - 20;;
        //console.log("H1: " + h1);
        //console.log("H2: " + h2);
        if (menuLeft.GLOBAL.isReachedScrollEnd == false &&
            (h1 >= h2)) {
            menuLeft.loadMenuLeft();
        }
    },
    loadMenuLeft: function () {
        if (menuLeft.GLOBAL.page > -1 && !menuLeft.GLOBAL.inCallback) {
            menuLeft.GLOBAL.inCallback = true;
            menuLeft.GLOBAL.page++;
            $(menuLeft.SELECTORS.spinner).show();
            setTimeout(function () {
                let status = menuLeft.appendMenuLeft();
                if (!status) {
                    menuLeft.GLOBAL.page = -1;
                }
                menuLeft.GLOBAL.inCallback = false;
                $(menuLeft.SELECTORS.spinner).hide();
            }, 500);

        }
    },
    appendMenuLeft: function () {
        $('.treeview-not-result').remove();
        let status = false;
        var pageSize = Math.ceil($(menuLeft.SELECTORS.divContent).height() / 44);
        var numberOfRecordToskip = menuLeft.GLOBAL.page * pageSize;
        menuLeft.getEditObject($(menuLeft.SELECTORS.textSearch).val(), numberOfRecordToskip, pageSize);
        var data = menuLeft.GLOBAL.lstNew;
        if (data.length != 0) {
            $.each(data, function (i, obj) {
                var string = `<li class="treeview ${mapdata.GLOBAL.idMainObject == obj.id ? "menu-open" : ""}" menuid="${obj.id}">
                                        <a class="MenuLeftId" menuid="${obj.id}" style="white-space:normal;">
                                            <span> ${obj.nameObject != null && obj.nameObject.trim() != "" ? obj.nameObject : l('ManagementLayer:Undefined')} </span>
                                        </a>
                                    </li>`;
                $(menuLeft.SELECTORS.ulLeft).append(string);
            });
            status = true;
        }
        else {
            if ($('.treeview').length == 0) {
                var string = `<div class="treeview-not-result"><span class="item-li">${l("ManagementLayer:NoSearchResult")}</span></div>`;
                if ($(menuLeft.SELECTORS.textSearch).val() == "") {
                    string = `<div class="treeview-not-result"><span class="item-li">${l("ManagementLayer:NoData")}</span></div>`;
                }
            }
            $(menuLeft.SELECTORS.ulLeft).append(string);
        }
        return status;
    },
    fitBounds: function (data) {
        let latLngBounds = new map4d.LatLngBounds();
        let paddingOptions = {
            top: 10,
            bottom: 50,
            left: 50,
            right: 50
        };

        for (var i = 0; i < data.length; i++) {
            latLngBounds.extend(data[i]);
        }
        map.fitBounds(latLngBounds, paddingOptions);
    },
    getEditObject: function (search, skipnumber, countnumber) {
        let id = mapdata.GLOBAL.idDirectory;
        menuLeft.GLOBAL.lstNew = [];
        var code = $(menuLeft.SELECTORS.selected_properties).val();
        var type = $(menuLeft.SELECTORS.selected_properties + " option:selected").attr("data-type") != undefined ? $(menuLeft.SELECTORS.selected_properties + " option:selected").attr("data-type") : "";
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: mapdata.CONSTS.URL_AJAXMENULEFT + '?id=' + id + "&skipnumber=" + skipnumber + "&countnumber="
                + countnumber + "&search=" + search + "&code=" + code + "&type=" + type,
            data: {},
            success: function (data) {
                if (data.code == "ok") {
                    if (data.result != null && data.result.length > 0) {
                        /*menuLeft.GLOBAL.lst = JSON.parse(JSON.stringify(menuLeft.GLOBAL.lst.concat(data.result)));*/
                        /*menuLeft.GLOBAL.lstTemporary = JSON.parse(JSON.stringify(menuLeft.GLOBAL.lst));*/
                        menuLeft.GLOBAL.lstNew = data.result;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    setDataLayer: function () {
        var name = localStorage.getItem("namelayer");
        var treeview = localStorage.getItem("treeviewlayer");
        $(menuLeft.SELECTORS.name).text("");
        $(menuLeft.SELECTORS.name).text(name + ` (` + (mapdata.GLOBAL.listObjectByDirectory.length) + ` ${l('ManagementLayer:Object')})`);
        $(menuLeft.SELECTORS.headerTopLayer).append(treeview);
    },
    Select2Search: function () {
        $(menuLeft.SELECTORS.selected_properties).select2({
            placeholder: `Chọn chỉ mục`,
            allowClear: true,
            width: '100%'
        });

        $(menuLeft.SELECTORS.selected_properties_table).select2({
            placeholder: `Chọn chỉ mục`,
            allowClear: true,
            width: '100%'
        });
    },
    SelectedSearchProperties: function () {
        var lst = mapdata.GLOBAL.lstPropertiesDirectory.filter(x => (x.typeSystem == 3 || x.typeSystem == 2) && (x.typeProperties == "text" || x.typeProperties == "float"
            || x.typeProperties == "int" || x.typeProperties == "string" || x.typeProperties == "stringlarge") && x.isIndexing == true); // chỉ cho hiển thị chỉ mục là : chuỗi và số

        var html = '<option value= ""></option>';
        $(menuLeft.SELECTORS.selected_properties).html(html);
        $(menuLeft.SELECTORS.selected_properties_table).html(html);
        for (var i = 0; i < lst.length; i++) {
            html += `<option value= "${lst[i].codeProperties}" data-type = "${lst[i].typeProperties}">${lst[i].nameProperties}</option>`;
        }

        $(menuLeft.SELECTORS.selected_properties).html(html);
        $(menuLeft.SELECTORS.selected_properties_table).html(html);

    }
};

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}
