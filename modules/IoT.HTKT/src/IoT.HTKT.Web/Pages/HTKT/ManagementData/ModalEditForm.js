﻿mn_selected = "mn_map_data";
var l = abp.localization.getResource('HTKT');
var editform = {
    GLOBAL: {

    },
    CONSTS: {

    },
    SELECTORS: {
        editForm: "#EditForm",
        btnAddNewFile: "#upload-file-btn",
        btnAddLink: "#add-link-btn",
        step_extend: "#EditForm .step-extend",
        li_step_extend: "#EditForm .step-extend .nav-link",
        table: ".table-edit-object tbody",
        tableHead: ".table-edit-object thead",
        btnSaveAll: ".btn-edit-all",
        extend_tab: "#EditForm li[class='nav-item col-md-4']",
        checkbox: "#EditForm #checkbox-config",
        btn_close_form: ".close-edit-form"
    },
    init: function () {
        editform.setEvent();
    },

    setEvent: function () {
        //event button đóng form
        $(editform.SELECTORS.editForm).find('.toggle-detail-property2').on('click', function () {
            if (mapdata.GLOBAL.selectbtn == "edit" && mapdata.GLOBAL.selectObject2D != null) {
                if (!map.is3dMode()) {
                    map.data.add(mapdata.GLOBAL.selectObject2D.feature);
                }
            }

            mapdata.showHideViewProperty(false);
            $(mapdata.SELECTORS.editForm).removeClass('active');
            let check = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
            if (check != null && check != undefined) {
                check.object.setSelected(false);
            }
            mapdata.GLOBAL.idMainObject = '';
            mapdata.GLOBAL.mainObjectSelected = null;
            model3D.hideModel3D();
            model3D.resetAll();
            //$('.li-modal-data-right[data-li="step-info"]').first().trigger('click');
            $('.li-modal-data-right').removeClass("active");
            $('.li-modal-data-right[data-li="step-info"]').addClass("active");
            $('.section-info').css('display', 'none');
            $('.step-info').css('display', 'block');
            mapdata.clearSelectDrawHighlight();
            mapdata.showAllObject3D();
            $(menuLeft.SELECTORS.liLeft).removeClass("menu-open");
            documentObject.GLOBAL.lstDocument = [];
            mapdata.clearHighlight();
            mapdata.clearHighlightPoint();
            map.setSelectedBuildings([]);
            mapdata.clearHighlightPoint();

        });

        //event button đóng form
        $(editform.SELECTORS.btn_close_form).on('click', function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("AreYouExitAction"),
                icon: "warning",
                buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                dangerMode: false,
            }).then((val) => {
                if (val) {
                    if (mapdata.GLOBAL.selectbtn == "edit" && mapdata.GLOBAL.selectObject2D != null) {
                        map.data.add(mapdata.GLOBAL.selectObject2D.feature);
                    }

                    mapdata.showHideViewProperty(false);
                    $(mapdata.SELECTORS.editForm).removeClass('active');
                    let check = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
                    if (check != null && check != undefined) {
                        check.object.setSelected(false);
                    }
                    mapdata.GLOBAL.idMainObject = '';
                    mapdata.GLOBAL.mainObjectSelected = null;
                    model3D.hideModel3D();
                    model3D.resetAll();
                    //$('.li-modal-data-right[data-li="step-info"]').first().trigger('click');
                    $('.li-modal-data-right').removeClass("active");
                    $('.li-modal-data-right[data-li="step-info"]').addClass("active");
                    $('.section-info').css('display', 'none');
                    $('.step-info').css('display', 'block');
                    mapdata.clearSelectDrawHighlight();
                    mapdata.showAllObject3D();
                    $(menuLeft.SELECTORS.liLeft).removeClass("menu-open");
                    documentObject.GLOBAL.lstDocument = [];
                    mapdata.clearHighlight();
                    mapdata.clearHighlightPoint();
                    map.setSelectedBuildings([]);
                    mapdata.clearHighlightPoint();
                }
            });

        });

        //event update table
        $(editform.SELECTORS.li_step_extend).on("click", function () {
            var type = $(this).attr("data-type");
            modalCreateFolderLayer.updateTable2(type, editform.SELECTORS.step_extend);
        });

        //đổi tab
        //$('#EditForm .li-modal-data-right').on('click', function () {
        //    let step = $(this).attr("data-li");

        //    $('.li-modal-data-right').removeClass("active");
        //    $(this).addClass("active");

        //    var obj = mapdata.GLOBAL.features.find(x => x._id === mapdata.GLOBAL.idMainObject);
        //    $('.section-info').css('display', 'none');

        //    switch (step) {
        //        case "step-info":
        //            $('.step-info').css('display', 'block');

        //            break;
        //        case "step-config":
        //            $('.step-config').css('display', 'block');
        //            break;
        //        case "step-3D":
        //            $('.step-3D').css('display', 'block');
        //            model3D.MoveToBuilding3D();
        //            $(model3D.SELECTORS.paraSelected).val($(model3D.SELECTORS.paraSelected + " option:first").val()).trigger("change"); // set lại trạng thái mặc định của step 3d
        //            break;
        //        case "step-extend":
        //            parentModel = editform.SELECTORS.editForm;
        //            //$('.step-extend').css('display', 'block');
        //            $(mapdata.SELECTORS.btnEdited).trigger('click');
        //            $(parentModel + " " + '.model-properties-object .class-info').css('display', 'none');
        //            $(parentModel + " " + '.model-properties-object .class-config').css('display', 'none');

        //            $('#nav-image-extend-edit-tab').trigger('click'); //mặc định content mở rộng là hiên thị tab hình ảnh
        //            //modalCreateFolderLayer.updateTable2("image", editform.SELECTORS.step_extend); // hiển thị tab hình ảnh

        //            modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = true;
        //            $('.step-extend').css('display', 'block');
        //            break;
        //        case "step-2D":
        //            $('.step-2D').css('display', 'block');
        //            map.enable3dMode(false);
        //            break;
        //    }

        //    $(documentObject.SELECTORS.divBorderFile).removeClass("file-active");
        //});

        //show form add file
        $(editform.SELECTORS.step_extend).on("click", editform.SELECTORS.btnAddNewFile, function () {
            modalCreateFolderLayer.clearLabelError();
            $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("show");
        });
        //show form add link
        $(editform.SELECTORS.step_extend).on("click", editform.SELECTORS.btnAddLink, function () {
            modalCreateFolderLayer.clearLabelError();
            $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).modal("show");
        });
        //btn save all
        $(editform.SELECTORS.btnSaveAll).on("click", function () {
            toastr.remove();
            if (Info_Properties_Main_Object.checkFormInput(editform.SELECTORS.editForm + " .step-info-content") && mapdata.GLOBAL.idDirectory.trim() != "" && Info_Properties_Main_Object.checkValidFile()) {
                var object = Info_Properties_Main_Object.getValueFormPropeties(editform.SELECTORS.editForm);
                object.properties = object.propertiesGeojson;
                object.isCheckImplementProperties = $(editform.SELECTORS.editForm + " " + Config_Main_Object.SELECTORS.checkbox_implement).is(":checked");

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json-patch+json",
                    async: false,
                    url: mapdata.CONSTS.URL_AJAXUPDATESAVE,
                    data: JSON.stringify(object),
                    success: function (data) {
                        if (data.code == "ok") {
                            var message = l("ManagementLayer:UpdateDataSuccesfully")
                            abp.notify.success(message);

                            $(editform.SELECTORS.editForm).removeClass('active');
                            let objMain = mapdata.GLOBAL.listObjectByDirectory.find(x => x.id == data.result.id)
                            if (objMain !== null) {
                                objMain = data.result;
                            }
                            let geojson = mapdata.getDataGeojsonByProperties([data.result]);
                            mapdata.udpateShowDataGeojson(JSON.stringify(geojson)); // cập nhật lại geojson trên bản đồ


                            // lưu file vào object main
                            let array = [];
                            $.each(modalCreateFolderLayer.GLOBAL.listObjectFile, function (i, obj) {
                                let item = {
                                    id: obj.id,
                                    idMainObject: data.result.id,
                                    nameDocument: obj.name,
                                    iconDocument: '',
                                    urlDocument: obj.url,
                                    nameFolder: obj.idDirectoryProperties,
                                    orderDocument: obj.order
                                };
                                array.push(item);
                            });

                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                //contentType: false,
                                //processData: false,
                                contentType: "application/json-patch+json",
                                async: false,
                                url: '/api/HTKT/DocumentObject/add-file-document',
                                data: JSON.stringify(array),
                                success: function (data) {
                                    
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.error(jqXHR + ":" + errorThrown);
                                    //ViewMap.showLoading(false);
                                }
                            });

                        }
                        else {
                            var message = l("ManagementLayer:UpdateDataFailed")
                            abp.notify.success(message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR + ":" + errorThrown);
                    }
                });
                ///
                mapdata.GLOBAL.statusdone = true;
                //mapdata.setOrRestBtnInsert(false);
                mapdata.getEditObject(mapdata.GLOBAL.idDirectory);
                menuLeft.resizeMenuLeft();
            }
        });
    },
    EditOnMap: function () {
        if (mapdata.GLOBAL.idMainObject !== null && mapdata.GLOBAL.idMainObject.trim() !== "" && mapdata.GLOBAL.idMainObject.trim().length > 0) {
            //mapdata.setOrRestBtnEidt(true);
            if (mapdata.GLOBAL.selectObject2D !== null) {
                mapdata.convertGeojsonToData(mapdata.GLOBAL.selectObject2D.feature);
            } else {
                var feature = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.mainObjectSelected.id);
                mapdata.convertGeojsonToData(feature);
            }
            mapdata.hideInforProperties();
        } else {

            abp.notify.error("Error");
        }
    },
    OpenFormEdit: function (id) {
        if (mapdata.GLOBAL.mainObjectSelected != null) {
            mapdata.ResetEditMainObject(); // res
        }

        mapdata.GLOBAL.idMainObject = id;
        $(editform.SELECTORS.editForm + " " + '.infor-detail').html('');

        var getFeature = mapdata.GLOBAL.features.find(x => x._id == mapdata.GLOBAL.idMainObject);
        mapdata.GLOBAL.selectObject2D = { feature: getFeature };

        $('.ul-left li').removeClass("menu-open");
        $($('a[menuid="' + mapdata.GLOBAL.idMainObject + '"]')[0]).parent().addClass("menu-open");

        mapdata.getMainObject("", editform.SELECTORS.editForm);
        mapdata.getDefaultObject("", mapdata.SELECTORS.editForm);
        Info_Properties_Main_Object.setValueObject();
        model3D.setData();
        editform.EditOnMap();
        $(editform.SELECTORS.editForm + ' .li-modal-data-right').eq(3).trigger('click');
        mapdata.clearHighlightPoint();

        // kiểm tra hiển thị fix 2d nếu bản đồ đang ở trạng thái 3d
        if (map.is3dMode()) {
            mapdata.HideOrShowDrawMainObject(false);
        } else {
            mapdata.HideOrShowDrawMainObject(true);

            // nếu là 2d thì ẩn geojson của mainobject hiện tại
            map.data.remove(mapdata.GLOBAL.selectObject2D.feature);
        }
    }
}