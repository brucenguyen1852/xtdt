﻿var l = abp.localization.getResource('HTKT');
var model3D = {
    GLOBAL: {
        building: null,
        object3D: null,
        texture3D: null,
        checkdraw: false,
        coor: [],
        polygon: null,
        polyline: null,
        marker: null,
        polygonArray: [],
        polylineArray: [],
        markerArray: [],
        polylineDraw: null,
        geojson3D: {
            type: "Polygon",
            coordinates: []
        },
        objectGeojson3D: null,
        geojsonDefault3D: "",
        coordinatesDefault3D: {
            type: "Polygon",
            coordinates: []
        },
        objectChoosenMap: {
            id: '',
            name: '',
            location: {
                lat: 0,
                lng: 0
            },
            bearing: 0,
            scale: 1,
            height: 1,
            elevation: 0
        },
        idObjectMapTemporary: '',
        fileObject3d: {
            objurl: null,
            texture3D: null
        },
        urlObject3d: {
            objurl: '',
            texture3D: ''
        },
        isResetDraw: false,
        idObject2D: '',
        buildingOld: {
            type: "",
            building: null
        },
        editMode3D: {
            status: false,
            listMarker: [],
            polygon: null
        }
    },
    CONSTS: {
        URL_AJAXDeleteMode3D: '/api/HTKT/ManagementData/delete-mode-3d-by-IdObject',
    },
    SELECTORS: {
        content_3d: ".step-3D",
        lat: '#lat',
        lng: '#lng',
        scale: '#scale',
        bearing: '#bearing',
        elevation: '#elevation',
        selectParadigmId: '#selectParadigm',
        paraSelected: "#para-selected",
        chooseImg: "#choosenFile-img",
        imgUpload: "#imgUpload",
        choosenFileObj: "#choosenFile-obj",
        drawStart: "#drawStart",
        drawEnd: "#drawEnd",
        drawCancel: "#drawCancel",
        drawReturn: "#drawReturn",
        height: "#draw-height",
        btnSave3D: ".btn-save-3D",
        inforGeojson: ".step-3D #nav-3d-geojson .textarea-geojson",
        tableGeojson: ".step-3D #nav-3d-infor tbody",
        radioChoosenMap: 'input[name="optionsChoosenMap"]',
        parameterMap: ".parameter-map",
        selectObjectChosenMap: '#selectChoosenMap',
        choosenMapStart: "#choosenMapStart",
        choosenMapEnd: "#choosenMapEnd",
        choosenMapCancel: "#choosenMapCancel",

        file_upload_content: ".obj-file-content",
        image_upload_content: ".image-file-content",
        content_error_file_obj: ".content-errors-file-obj",
        content_error_image_obj: ".content-errors-image-obj",

        tabJson_Table_3d_draw: ".content-3d-json .nav-3d-geojson .nav-link",
        paraExClass: ".paraExClass",
        choosenFileClass: ".choosenFileClass",
        drawClass: ".drawClass",
        choosenMapClass: ".choosenMapClass",
        mode3dDraw: ".3d-draw",
        selectObject3D: "#SelectObject3D",
        actionFileObj: ".action-file-obj",

        btnCancelModel3D: ".btn-cancel-3D",
        checkboxMode3d: "#checkbox-mode-3d",
        btnEditMode3D: "#drawEidt",
        btnEndMode3D: "#drawEidtEnd",
    },
    init: function () {
        model3D.setEvent();
        //model3D.setData();
        model3D.eventChoosenMap();
    },
    setEvent: function () {
        $(model3D.SELECTORS.selectParadigmId).on('change', function () {
            let value = $(this).val();
            if (value != null && value != "")
            {
                if (model3D.GLOBAL.building != null) {
                    model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                    model3D.resetModelFile();
                }
                let object = mapdata.GLOBAL.list.find(x => x.id == value);
                // Tạo đối tượng Building là một cây cầu từ BuildingOptions với model và texture
                var object2 = {
                    name: object.name,
                    position: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                    objectUrl: object.objUrl,
                    texture: object.textureUrl,
                    scale: $(model3D.SELECTORS.scale).val(),
                    bearing: $(model3D.SELECTORS.bearing).val(),
                    elevation: $(model3D.SELECTORS.elevation).val(),
                    id: "",
                    height: 1,
                    location: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                    coordinates: []
                };
                let building = model3D.newMap4dBuilding(object2);
                building.setSelected(true);
                building.setDraggable(true);
                // Thêm building vào bản đồ
                building.setMap(map);
                // xử lý fix theo 2D
                model3D.GLOBAL.building = building;
            } else {
                if (model3D.GLOBAL.building != null) {
                    model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                }
            }
        });

        $(model3D.SELECTORS.scale).on('change keyup', function () {
            if ($(this).val() < 0) $(model3D.SELECTORS.scale).val(1);
            model3D.GLOBAL.building !== null && model3D.GLOBAL.building.setScale($(this).val());
        });

        $(model3D.SELECTORS.bearing).on('change keyup', function () {
            model3D.GLOBAL.building !== null && model3D.GLOBAL.building.setBearing($(this).val());
        });

        $(model3D.SELECTORS.elevation).on('change keyup', function () {
            if ($(this).val() < 0) $(model3D.SELECTORS.elevation).val(0);
            model3D.GLOBAL.building !== null && model3D.GLOBAL.building.setElevation($(this).val());
        });

        $(model3D.SELECTORS.height).on('change keyup', function () {
            if ($(this).val() < 0) $(model3D.SELECTORS.height).val(1);
            model3D.GLOBAL.building !== null && model3D.GLOBAL.building.setHeight($(this).val());
        });

        $(model3D.SELECTORS.btnSave3D).on('click', function () {
            toastr.remove();
            var object = JSON.parse(JSON.stringify(mapdata.GLOBAL.mainObjectSelected));
            if (model3D.GLOBAL.building !== null && model3D.checkBuildingInPolygon(model3D.GLOBAL.building)) abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
            else {
                if (model3D.GLOBAL.objectChoosenMap.id != "" || model3D.GLOBAL.building != null) {
                    var message = "";
                    if (model3D.GLOBAL.building != null) {
                        if (object.object3D != null && object.object3D != undefined) {
                            message = l("ManagementLayer:Update3DObject");
                        }
                        else {
                            message = l("ManagementLayer:Create3DObject");
                        }

                        if (model3D.GLOBAL.fileObject3d.objurl != "" && model3D.GLOBAL.fileObject3d.objurl != null && model3D.GLOBAL.fileObject3d.objurl != undefined) {
                            model3D.GLOBAL.urlObject3d.objurl = model3D.SendFileDocument(model3D.GLOBAL.fileObject3d.objurl);
                        }
                        else if ((model3D.GLOBAL.building.getCoordinates().length <= 0) && (model3D.GLOBAL.building.properties.modelUrl === null || model3D.GLOBAL.building.properties.modelUrl === "")) {
                            var html = `<div class="text-danger" data-toggle="tooltip" data-placement="bottom">${l("ManagementLayer:NoFileUpload")}</div>`;
                            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_file_obj}`).html(html);
                            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_file_obj}`).removeClass('hidden');
                            return false;
                        }
                        if (model3D.GLOBAL.fileObject3d.texture3D != "" && model3D.GLOBAL.fileObject3d.texture3D != null && model3D.GLOBAL.fileObject3d.texture3D != undefined) {
                            model3D.GLOBAL.urlObject3d.texture3D = model3D.SendFileDocument(model3D.GLOBAL.fileObject3d.texture3D);
                        }
                        else if ((model3D.GLOBAL.building.getCoordinates().length <= 0) && (model3D.GLOBAL.building.properties.textureUrl === null || model3D.GLOBAL.building.properties.textureUrl === "")) {
                            var html = `<div class="text-danger" data-toggle="tooltip" data-placement="bottom">${l("ManagementLayer:NoFileUpload")}</div>`;
                            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_image_obj}`).html(html);
                            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_image_obj}`).removeClass('hidden');
                            return false;
                        }

                        var textureUrl = model3D.GLOBAL.urlObject3d.texture3D != '' ? model3D.GLOBAL.urlObject3d.texture3D : model3D.GLOBAL.building.properties.textureUrl;
                        var objectUrl = model3D.GLOBAL.urlObject3d.objurl != '' ? model3D.GLOBAL.urlObject3d.objurl : model3D.GLOBAL.building.properties.modelUrl;

                        if ($(model3D.SELECTORS.paraSelected).val() != "para-choosenFile") {
                            if (model3D.GLOBAL.building != null) {
                                textureUrl = model3D.GLOBAL.building.properties.textureUrl;
                                objectUrl = model3D.GLOBAL.building.properties.modelUrl;
                            }
                        }

                        model3D.GLOBAL.fileObject3d;
                        object.object3D = {};
                        object.object3D.id = model3D.GLOBAL.building.properties.id;
                        object.object3D.objectUrl = objectUrl;
                        object.object3D.texture = textureUrl;
                        object.object3D.location = model3D.GLOBAL.building.properties.location;
                        object.object3D.name = model3D.GLOBAL.building.properties.name;
                        object.object3D.scale = parseFloat(model3D.GLOBAL.building.properties.scale);
                        object.object3D.bearing = parseFloat(model3D.GLOBAL.building.properties.bearing);
                        object.object3D.elevation = parseFloat(model3D.GLOBAL.building.properties.elevation);
                        object.object3D.height = parseFloat(model3D.GLOBAL.building.properties.height);
                        object.object3D.coordinates = model3D.GLOBAL.building.properties.coordinates;
                        object.object3D.type = $(model3D.SELECTORS.paraSelected).val();
                        //object.object3D.status = true;
                    } else {
                        if (object.object3D != null && object.object3D != undefined) {
                            message = l("ManagementLayer:Update3DObject");
                        }
                        else {
                            message = l("ManagementLayer:Create3DObject");
                        }

                        if (model3D.GLOBAL.objectChoosenMap.id === "" || typeof model3D.GLOBAL.objectChoosenMap.id === "undefined") {
                            return false;
                        }

                        object.object3D = {};
                        object.object3D.id = model3D.GLOBAL.objectChoosenMap.id;
                        object.object3D.objectUrl = '';
                        object.object3D.texture = '';
                        object.object3D.location = model3D.GLOBAL.objectChoosenMap.location;
                        object.object3D.name = model3D.GLOBAL.objectChoosenMap.name;
                        object.object3D.scale = model3D.GLOBAL.objectChoosenMap.scale;
                        object.object3D.bearing = model3D.GLOBAL.objectChoosenMap.bearing;
                        object.object3D.elevation = model3D.GLOBAL.objectChoosenMap.elevation;
                        object.object3D.height = model3D.GLOBAL.objectChoosenMap.height;
                        object.object3D.coordinates = null;
                        object.object3D.type = $(model3D.SELECTORS.paraSelected).val();
                    }
                    abp.ui.setBusy(mapdata.SELECTORS.inforData);
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        contentType: "application/json-patch+json",
                        async: true,
                        url: mapdata.CONSTS.URL_AJAXUPDATESAVE,
                        data: JSON.stringify(object),
                        success: function (data) {
                            abp.ui.clearBusy(mapdata.SELECTORS.inforData);
                            if (data.code == "ok") {
                                //$(mapdata.SELECTORS.inforData).find('.toggle-detail-property2').trigger('click');
                                //mapdata.getEditObject("");

                                model3D.showHideOptionMode(data.result.object3D.type);
                                mapdata.GLOBAL.mainObjectSelected.object3D = data.result.object3D;
                                if (model3D.GLOBAL.building != null) {
                                    model3D.GLOBAL.building.setMap(null);
                                    model3D.GLOBAL.building = null;
                                }
                                if (data.result.object3D.type !== "para-choosenMap") {
                                    model3D.resetDataObject3DAfterSave(data.result.object3D, data.result.id);

                                    model3D.SetInforOptionModel3D(mapdata.GLOBAL.mainObjectSelected.object3D);
                                    let object3dTemp = model3D.newMap4dBuilding(data.result.object3D);
                                    model3D.GLOBAL.building = object3dTemp;
                                    model3D.GLOBAL.building.setMap(map);
                                    model3D.GLOBAL.building.setSelected(true);
                                    model3D.clearOptionMode3D();
                                } else {
                                    model3D.SetInforOptionModel3D(mapdata.GLOBAL.mainObjectSelected.object3D);
                                    let objchosen = mapdata.GLOBAL.listObjectByDirectory.find(x => x.object3D != null && x.object3D.type == "para-choosenMap" && x.id == data.result.id);
                                    if (typeof objchosen !== "undefined" && objchosen !== null) {
                                        objchosen.object3D = data.result.object3D;
                                    }
                                    model3D.clearOptionMode3D();
                                }

                                model3D.GLOBAL.urlObject3d.texture3D = '';
                                model3D.GLOBAL.urlObject3d.objurl = '';
                                abp.notify.success(message)
                            } else {
                                swal({
                                    title: l('ManagementLayer:Notification'),
                                    text: l('ManagementLayer:UpdateDataFailed'),
                                    icon: "warning",
                                    button: l('ManagementLayer:Close'),
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR + ":" + errorThrown);
                            abp.ui.clearBusy(mapdata.SELECTORS.inforData);
                        }
                    });
                } else {
                    if (model3D.GLOBAL.editMode3D.status) abp.notify.warn(l('Mode3D:Waring:EndEidtMode'))
                    else {
                        swal({
                            title: l('ManagementLayer:Notification'),
                            text: l('ManagementLayer:NoDataToSave'),
                            icon: "warning",
                            button: l('ManagementLayer:Close'),
                        })
                    }

                }
            }
        });

        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).on('click', `.action-file-obj`, function () {
            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).html('');
            $(model3D.SELECTORS.choosenFileObj).val(null).trigger("change");
            model3D.GLOBAL.fileObject3d.objurl = null;
            model3D.GLOBAL.object3D = "";
            model3D.setDataBuildingTemp(model3D.GLOBAL.object3D, true);
            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).addClass('hidden');
        });

        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).on('click', `.action-file-obj`, function () {
            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).html('');
            $(model3D.SELECTORS.chooseImg).val(null).trigger("change");
            model3D.GLOBAL.fileObject3d.texture3D = null;
            model3D.GLOBAL.texture3D = "";
            $(model3D.SELECTORS.imgUpload).attr('src', '/common/image-default.svg');

            model3D.setDataBuildingTemp(model3D.GLOBAL.object3D, true);
            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).addClass('hidden');
        })

        $(model3D.SELECTORS.choosenFileObj).change(function () {
            toastr.remove();
            var file = this.files;
            if (file.length > 0) {
                //for (var i = 0; i < file.length; i++) {
                // check size file 
                if (file[0].size <= 2097152) {
                    let fileObj = file[0];
                    var exp = fileObj.name.substring(fileObj.name.lastIndexOf(".") + 1).toLowerCase();
                    if (exp != "obj") {
                        var html = `<div class="text-danger" data-toggle="tooltip" data-placement="bottom">${l("ManagementLayer:FileExitsFormat")}</div>`;
                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_file_obj}`).html(html);
                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_file_obj}`).removeClass('hidden');

                    } else {
                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_file_obj}`).addClass('hidden');
                        model3D.GLOBAL.fileObject3d.objurl = file[0];
                        let reader = new FileReader();
                        reader.readAsText(fileObj);
                        reader.onload = function (e) {

                            // hiển thị tên file
                            var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${fileObj.name}" data-toggle="tooltip" data-placement="bottom">${fileObj.name}</div>
                            <div class="action-file-obj" title="Xóa" data-toggle="tooltip" data-placement="bottom"><img src="/images/close_cricle.svg" /></div>`;
                            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).html(html);
                            $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).removeClass('hidden');

                            model3D.GLOBAL.object3D = reader.result;
                            if (model3D.GLOBAL.texture3D != null && model3D.GLOBAL.object3D != null) {
                                model3D.setDataBuildingTemp(model3D.GLOBAL.object3D);
                                //model3D.createObjectModelByFile($(model3D.SELECTORS.lat).val(), $(model3D.SELECTORS.lng).val(),
                                //    $(model3D.SELECTORS.scale).val(), $(model3D.SELECTORS.bearing).val(), $(model3D.SELECTORS.elevation).val(),
                                //    model3D.GLOBAL.object3D, model3D.GLOBAL.texture3D);
                            }
                        };
                    }
                } else {
                    abp.notify.error(l("Mode3D:Waring:MaxSize"));
                    //swal({
                    //    title: l("ManagementLayer:Notification"),
                    //    text: l("ManagementLayer:FileSizeCannotLarger50mb{0}", file[i].name),
                    //    icon: "warning",
                    //    button: l("ManagementLayer:Close")
                    //});
                }
            }
        });

        $(model3D.SELECTORS.chooseImg).change(function (event) {
            toastr.remove();
            if (this.files.length > 0) {
                var file = this.files[0];
                var url = this.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (file.type.split('/')[0] == "image" && (ext === "gif" || ext === "png" || ext === "jpeg" || ext === "jpg")) {
                    if (file.size <= 2097152) { //2MB
                        // hiển thị tên file
                        var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${file.name}" data-toggle="tooltip" data-placement="bottom">${file.name}</div>
                            <div class="action-file-obj" title="Xóa" data-toggle="tooltip" data-placement="bottom"><img src="/images/close_cricle.svg" /></div>`;
                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).html(html);
                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).removeClass('hidden');

                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_image_obj}`).addClass('hidden');
                        model3D.GLOBAL.texture3D = URL.createObjectURL(this.files[0]);

                        model3D.GLOBAL.fileObject3d.texture3D = file;
                        let reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function (e) {
                            $('#imgUpload').attr('src', e.target.result);
                            //createTruAnten.GLOBAL.texture3d = reader.result;
                            if (model3D.GLOBAL.texture3D != null && model3D.GLOBAL.object3D != null) {
                                model3D.setDataBuildingTemp(model3D.GLOBAL.object3D);
                                //model3D.createObjectModelByFile($(model3D.SELECTORS.lat).val(), $(model3D.SELECTORS.lng).val(),
                                //    $(model3D.SELECTORS.scale).val(), $(model3D.SELECTORS.bearing).val(), $(model3D.SELECTORS.elevation).val(),
                                //    model3D.GLOBAL.object3D, model3D.GLOBAL.texture3D);
                            }
                        };
                    } else {
                        abp.notify.error(l("Mode3D:Waring:MaxSize"));
                    }
                }
                else {
                    var exp = file.name.substring(file.name.lastIndexOf(".") + 1).toLowerCase();
                    if (exp != "obj") {
                        var html = `<div class="text-danger" data-toggle="tooltip" data-placement="bottom">${l("ManagementLayer:FileExitsFormat")}</div>`;
                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_image_obj}`).html(html);
                        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_image_obj}`).removeClass('hidden');

                    }
                }
            }
            //else {
            //    $(model3D.SELECTORS.imgUpload).attr('src', '/common/image-default.svg');
            //}
        });

        $(model3D.SELECTORS.drawStart).on('click', function () {
            //$(this).css('display', 'none');
            //$(model3D.SELECTORS.drawReturn).css('display', 'inline-block');
            //$(model3D.SELECTORS.drawEnd).css('display', 'inline-block');
            //$(model3D.SELECTORS.drawCancel).css('display', 'inline-block');
            //model3D.GLOBAL.checkdraw = true;
            //if (model3D.GLOBAL.building != null) {
            //    model3D.GLOBAL.building.setMap(null);
            //    model3D.GLOBAL.building = null;
            //}
            //model3D.GLOBAL.coor = [];
            //model3D.resetModelFile();
        });

        $(model3D.SELECTORS.drawCancel).on('click', function () {
            toastr.remove();
            if (!model3D.GLOBAL.editMode3D.status) {

                model3D.resetDraw();
                if (model3D.GLOBAL.building != null) {
                    model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                    model3D.GLOBAL.coor = [];
                }
                if (model3D.GLOBAL.geojsonDefault3D != "") {
                    var defaultobject = JSON.parse(model3D.GLOBAL.geojsonDefault3D);
                    let html = model3D.getCoordinates(defaultobject.geometry);
                    $(model3D.SELECTORS.tableGeojson).children().remove();
                    $(model3D.SELECTORS.tableGeojson).append(html);

                    $(model3D.SELECTORS.inforGeojson).val(model3D.GLOBAL.geojsonDefault3D);
                    Info_Properties_Main_Object.setRisizeTextareaJson3d();
                }
                else {
                    $(model3D.SELECTORS.tableGeojson).children().remove();
                    $(model3D.SELECTORS.tableGeojson).append("");

                    $(model3D.SELECTORS.inforGeojson).val(model3D.GLOBAL.geojsonDefault3D);
                    Info_Properties_Main_Object.setRisizeTextareaJson3d();
                }

                model3D.GLOBAL.checkdraw = true;
                $(model3D.SELECTORS.drawCancel).attr('disabled', true);
                $(model3D.SELECTORS.checkboxMode3d).prop('checked', false);
            } else {
                abp.notify.warn(l("Mode3D:Waring:EndEidtMode"));
            }
        });

        $(model3D.SELECTORS.drawEnd).on('click', function () {
            model3D.GLOBAL.checkdraw = false;
            if (!model3D.GLOBAL.isResetDraw) {
                if (model3D.GLOBAL.coor.length > 0) {
                    if (model3D.GLOBAL.coor.length > 2) {
                        let array = [];
                        $.each(model3D.GLOBAL.coor, function (i, item) {
                            array.push([item.lng, item.lat]);
                        });
                        array.push([model3D.GLOBAL.coor[0].lng, model3D.GLOBAL.coor[0].lat]);
                        var polygon = turf.polygon([array]);
                        var centroid = turf.centroid(polygon);
                        $(model3D.SELECTORS.lat).val(centroid.geometry.coordinates[1]);
                        $(model3D.SELECTORS.lng).val(centroid.geometry.coordinates[0]);
                        var object2 = {
                            name: "Tòa nhà",
                            position: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                            objectUrl: null,
                            texture: null,
                            scale: $(model3D.SELECTORS.scale).val(),
                            bearing: $(model3D.SELECTORS.bearing).val(),
                            elevation: $(model3D.SELECTORS.elevation).val(),
                            id: "",
                            height: $(model3D.SELECTORS.height).val(),
                            location: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                            coordinates: model3D.GLOBAL.coor
                        };

                        if (model3D.GLOBAL.building != null) {
                            model3D.GLOBAL.building.setMap(null);
                            model3D.GLOBAL.building = null;
                        }

                        model3D.GLOBAL.building = model3D.newMap4dBuilding(object2);
                        model3D.GLOBAL.building.setDraggable(true);
                        model3D.GLOBAL.building.setSelected(true);
                        model3D.GLOBAL.building.setMap(map);
                        var arraygeojson = [];
                        $.each(model3D.GLOBAL.coor, function (i, obj) {
                            arraygeojson.push([obj.lng, obj.lat]);
                        });
                        arraygeojson.push([model3D.GLOBAL.coor[0].lng, model3D.GLOBAL.coor[0].lat]);
                        model3D.GLOBAL.geojson3D.coordinates = [];
                        model3D.GLOBAL.geojson3D.coordinates.push(arraygeojson);
                        model3D.setGeojsonData3D(model3D.GLOBAL.geojson3D);
                        model3D.setTableData3D(model3D.GLOBAL.geojson3D);
                    }
                    for (i = 0; i < model3D.GLOBAL.polygonArray.length; i++) {
                        model3D.GLOBAL.polygonArray[i].setMap(null);
                    }
                    for (i = 0; i < model3D.GLOBAL.markerArray.length; i++) {
                        model3D.GLOBAL.markerArray[i].setMap(null);
                    }
                    for (i = 0; i < model3D.GLOBAL.polylineArray.length; i++) {
                        model3D.GLOBAL.polylineArray[i].setMap(null);
                    }

                    if (model3D.GLOBAL.polygonArray != null) {
                        model3D.GLOBAL.polygonArray = [];
                    }
                    if (model3D.GLOBAL.polylineArray != null) {
                        model3D.GLOBAL.polylineArray = [];
                    }
                    if (model3D.GLOBAL.markerArray != null) {
                        model3D.GLOBAL.markerArray = [];
                    }
                    if (model3D.GLOBAL.polylineDraw != null) {
                        model3D.GLOBAL.polylineDraw.setMap(null);
                        model3D.GLOBAL.polylineDraw = null;
                    }
                } else {
                    swal({
                        title: l('ManagementLayer:Notification'),
                        text: l('ManagementLayer:CannotFinishWithoutDrawingPoint'),
                        icon: "warning",
                        button: l('ManagementLayer:Close')
                    });
                }

                model3D.GLOBAL.isResetDraw = true;
            }

        });

        $(model3D.SELECTORS.drawReturn).on("click", function () {
            model3D.hideModel3D();
            model3D.resetDraw();
        });

        // hidden show paradigm
        $(model3D.SELECTORS.paraSelected).on('select2:select', function () {
            let value = $(this).val();
            if (model3D.GLOBAL.editMode3D.status) // nếu đang ở trạng thái vẽ 3d thì phải kết thúc trạng thái vẽ
            {
                $(model3D.SELECTORS.btnEndMode3D).click();
            }

            $(model3D.SELECTORS.mode3dDraw).hide();
            //$('.3d-draw').css('display', 'none');
            $(model3D.SELECTORS.parameterMap).show();
            //$(model3D.SELECTORS.parameterMap).css('display', 'block');
            $(model3D.SELECTORS.choosenMapCancel).trigger('click');
            model3D.GLOBAL.checkdraw = false;// trạng thái vẽ
            model3D.RemoveBulding3D();
            model3D.resetDraw();
            model3D.showHideOptionMode(value);
            //switch (value) {
            //    case "para-paraEx":
            //        $(model3D.SELECTORS.paraExClass).show();
            //        $(model3D.SELECTORS.choosenFileClass).hide();
            //        $(model3D.SELECTORS.drawClass).hide();
            //        $(model3D.SELECTORS.choosenMapClass).hide();
            //        //$('.paraExClass').css('display', 'block');
            //        //$('.choosenFileClass').css('display', 'none');
            //        //$('.drawClass').css('display', 'none');
            //        //$('.choosenMapClass').css('display', 'none');
            //        model3D.GLOBAL.building != null && model3D.GLOBAL.building.setMap(map);
            //        break;
            //    case "para-choosenFile":
            //        $(model3D.SELECTORS.paraExClass).hide();
            //        $(model3D.SELECTORS.choosenFileClass).show();
            //        $(model3D.SELECTORS.drawClass).hide();
            //        $(model3D.SELECTORS.choosenMapClass).hide();
            //        //$('.paraExClass').css('display', 'none');
            //        //$('.choosenFileClass').css('display', 'block');
            //        //$('.drawClass').css('display', 'none');
            //        //$('.choosenMapClass').css('display', 'none');
            //        $(model3D.SELECTORS.height).val(10);
            //        break;
            //    case "para-draw":
            //        model3D.GLOBAL.checkdraw = true; // vẽ 3d
            //        $(model3D.SELECTORS.paraExClass).hide();
            //        $(model3D.SELECTORS.choosenFileClass).hide();
            //        $(model3D.SELECTORS.drawClass).show();
            //        $(model3D.SELECTORS.choosenMapClass).hide();
            //        //$('.paraExClass').css('display', 'none');
            //        //$('.choosenFileClass').css('display', 'none');
            //        //$('.drawClass').css('display', 'block');
            //        //$('.choosenMapClass').css('display', 'none');
            //        model3D.showObjectExample3D();
            //        $(model3D.SELECTORS.mode3dDraw).show();
            //        //$('.3d-draw').css('display', 'block');
            //        $(model3D.SELECTORS.height).val(100);
            //        setTimeout(function () {
            //            Info_Properties_Main_Object.setRisizeTextareaJson3d();
            //        }, 300)
            //        break;
            //    case "para-choosenMap":
            //        $(model3D.SELECTORS.paraExClass).hide();
            //        $(model3D.SELECTORS.choosenFileClass).hide();
            //        $(model3D.SELECTORS.drawClass).hide();
            //        $(model3D.SELECTORS.choosenMapClass).show();
            //        //$('.paraExClass').css('display', 'none');
            //        //$('.choosenFileClass').css('display', 'none');
            //        //$('.drawClass').css('display', 'none');
            //        //$('.choosenMapClass').css('display', 'block');
            //        $(model3D.SELECTORS.parameterMap).hide();
            //        //$(model3D.SELECTORS.parameterMap).css('display', 'none');
            //        $(model3D.SELECTORS.selectObject3D).prop('checked', true);
            //        $(model3D.SELECTORS.selectObjectChosenMap).attr('disabled', false);
            //        break;
            //    default:
            //        model3D.GLOBAL.checkdraw = false;// trạng thái vẽ
            //        model3D.RemoveBulding3D();
            //        model3D.resetDraw();
            //        $(model3D.SELECTORS.paraExClass).hide();
            //        $(model3D.SELECTORS.choosenFileClass).hide();
            //        $(model3D.SELECTORS.drawClass).hide();
            //        $(model3D.SELECTORS.choosenMapClass).hide();
            //        $(model3D.SELECTORS.parameterMap).hide();
            //        break;
            //}
        });

        $(model3D.SELECTORS.tabJson_Table_3d_draw).on('click', function () {
            setTimeout(function () { Info_Properties_Main_Object.setRisizeTextareaJson3d(); }, 300)

        })


        map.data.addListener("click", (args) => {
            if (model3D.GLOBAL.checkdraw) {
                // reset vẽ lại
                if (model3D.GLOBAL.isResetDraw) {
                    if (model3D.GLOBAL.building != null) {
                        model3D.GLOBAL.building.setMap(null);
                        model3D.GLOBAL.building = null;
                        model3D.GLOBAL.coor = [];
                    }

                    model3D.GLOBAL.isResetDraw = false;
                }

                //$(model3D.SELECTORS.lat).val(args.location.lat);
                //$(model3D.SELECTORS.lng).val(args.location.lng);
                if (model3D.GLOBAL.polygon != null) {
                    model3D.GLOBAL.polygon.setMap(null)
                }
                if (model3D.GLOBAL.marker != null) {
                    model3D.GLOBAL.marker.setMap(null)
                }
                var lng = args.location.lng;
                var lat = args.location.lat;
                if (model3D.GLOBAL.coor.length == 0) {
                    model3D.GLOBAL.marker = new map4d.Marker({
                        position: { lat: lat, lng: lng },
                        icon: new map4d.Icon(8, 8, "/common/yellow-point.png"),
                        anchor: [0.5, 1],
                        zIndex: 100
                    })
                    model3D.GLOBAL.marker.setMap(map);
                    model3D.GLOBAL.coor.push({ lng, lat });
                    model3D.GLOBAL.markerArray.push(model3D.GLOBAL.marker);
                }
            }
        });

        let clickStartDraw = map.addListener("click", (args) => {
            if (model3D.GLOBAL.checkdraw && model3D.CheckPointInPolygonOrMulti(args.location.lat, args.location.lng)) {
                // reset vẽ lại
                if (model3D.GLOBAL.isResetDraw) {
                    if (model3D.GLOBAL.building != null) {
                        model3D.GLOBAL.building.setMap(null);
                        model3D.GLOBAL.building = null;
                        model3D.GLOBAL.coor = [];
                    }
                    model3D.GLOBAL.isResetDraw = false;
                }

                //$(model3D.SELECTORS.lat).val(args.location.lat);
                //$(model3D.SELECTORS.lng).val(args.location.lng);
                if (model3D.GLOBAL.polygon != null) {
                    model3D.GLOBAL.polygon.setMap(null)
                }
                if (model3D.GLOBAL.marker != null) {
                    model3D.GLOBAL.marker.setMap(null)
                }
                var lng = args.location.lng;
                var lat = args.location.lat;
                if (model3D.GLOBAL.coor.length == 0) {
                    model3D.GLOBAL.marker = new map4d.Marker({
                        position: { lat: lat, lng: lng },
                        icon: new map4d.Icon(8, 8, "/common/yellow-point.png"),
                        anchor: [0.5, 1],
                        zIndex: 100
                    })
                    model3D.GLOBAL.marker.setMap(map);
                    model3D.GLOBAL.coor.push({ lng, lat });
                    model3D.GLOBAL.markerArray.push(model3D.GLOBAL.marker);
                }
            }
            else if (model3D.GLOBAL.checkdraw) {
                abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
            }
        }, { map: true, mapbuilding: true, location: true });

        let clickDrawPolygon = map.addListener("click", (args) => {
            if (model3D.GLOBAL.checkdraw && model3D.CheckPointInPolygonOrMulti(args.location.lat, args.location.lng)) {
                // reset vẽ lại
                if (model3D.GLOBAL.isResetDraw) {
                    if (model3D.GLOBAL.building != null) {
                        model3D.GLOBAL.building.setMap(null);
                        model3D.GLOBAL.building = null;
                        model3D.GLOBAL.coor = [];
                    }
                    model3D.GLOBAL.isResetDraw = false;
                }

                var lng = args.location.lng;
                var lat = args.location.lat;
                var check = model3D.GLOBAL.coor.find(x => x.lat == lat && x.lng == lng);
                if (check == null || check == undefined) {
                    model3D.GLOBAL.marker = new map4d.Marker({
                        position: { lat, lng },
                        icon: new map4d.Icon(8, 8, "/common/yellow-point.png"),
                        anchor: [0.5, 1],
                        zIndex: 100
                    });
                    model3D.GLOBAL.marker.setMap(map);
                    model3D.GLOBAL.coor.push({ lng, lat });
                    if (model3D.GLOBAL.coor.length > 2) {
                        let polygonOption = map.PolygonOptions = {
                            paths: [
                                model3D.GLOBAL.coor.concat(model3D.GLOBAL.coor[0])
                            ],
                            fillColor: "#FBF2EF",
                            fillOpacity: 1.0,
                            strokeColor: "#8A0808",
                            zIndex: 100
                            //draggable: true
                        };
                        model3D.GLOBAL.polygon = new map4d.Polygon(polygonOption);
                        model3D.GLOBAL.polygon.setMap(map);
                        for (i = 0; i < model3D.GLOBAL.polygonArray.length; i++) {
                            model3D.GLOBAL.polygonArray[i].setMap(null);
                        }
                        model3D.GLOBAL.polygonArray.push(model3D.GLOBAL.polygon);
                    }
                    model3D.GLOBAL.markerArray.push(model3D.GLOBAL.marker);
                    model3D.GLOBAL.polyline = new map4d.Polyline({
                        path: model3D.GLOBAL.coor,
                        strokeColor: "#ff0000",
                        strokeOpacity: 1.0,
                        strokeWidth: 1,
                        zIndex: 100
                    })
                    model3D.GLOBAL.polyline.setMap(map);
                    model3D.GLOBAL.polylineArray.push(model3D.GLOBAL.polyline);
                    if (model3D.GLOBAL.polylineDraw != null) {
                        model3D.GLOBAL.polylineDraw.setMap(null);
                        model3D.GLOBAL.polylineDraw = null;
                    }
                }
            }
            else if (model3D.GLOBAL.checkdraw) {
                abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
            }
        }, { polyline: true });

        let dblclickDrawPolygon = map.addListener("dblClick", (args) => {
            if (model3D.GLOBAL.checkdraw) {
                if (model3D.GLOBAL.coor.length > 2) {
                    $(model3D.SELECTORS.drawEnd).trigger('click');
                    model3D.GLOBAL.isResetDraw = true;
                    model3D.GLOBAL.checkdraw = false;
                    $(model3D.SELECTORS.drawCancel).attr('disabled', false);
                }
            }
        }, { marker: true });

        let hoverDrawPolygon = map.addListener("mouseMove", (args) => {
            if (!model3D.GLOBAL.isResetDraw) {
                if (model3D.GLOBAL.checkdraw) {
                    if (model3D.GLOBAL.coor.length > 0) {
                        var array = [];
                        var lng = model3D.GLOBAL.coor[model3D.GLOBAL.coor.length - 1].lng;
                        var lat = model3D.GLOBAL.coor[model3D.GLOBAL.coor.length - 1].lat;
                        array.push({ lng, lat });
                        lat = args.location.lat;
                        lng = args.location.lng;
                        array.push({ lng, lat });
                        if (model3D.GLOBAL.polylineDraw != null) {
                            model3D.GLOBAL.polylineDraw.setMap(null);
                            model3D.GLOBAL.polylineDraw = null;
                        }
                        model3D.GLOBAL.polylineDraw = new map4d.Polyline({
                            path: array,
                            strokeColor: "#ff0000",
                            strokeOpacity: 1.0,
                            strokeWidth: 1,
                            zIndex: 100
                        })
                        model3D.GLOBAL.polylineDraw.setMap(map);
                    }
                }
            }
        }, { map: true });
        //Chỉnh sửa mode 3d
        let EditDrawPolygon = map.addListener("click", (args) => {
            toastr.remove();
            if (model3D.GLOBAL.editMode3D.status && model3D.CheckPointInPolygonOrMulti(args.location.lat, args.location.lng)) {
                mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 2);
                mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                model3D.setPolygonEidtMode3D(args.location.lat, args.location.lng);
            } else if (model3D.GLOBAL.editMode3D.status) {
                abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
            }
        }, { location: true, mapbuilding: true, polyline: true, polygon: true }); //building: true,

        let EditDrawDataPolygon = map.data.addListener("click", (args) => {
            toastr.remove();
            if (model3D.GLOBAL.editMode3D.status && model3D.CheckPointInPolygonOrMulti(args.location.lat, args.location.lng)) {
                mapdata.createMarkerDrawLoDat(args.location.lat, args.location.lng, 2);
                mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                model3D.setPolygonEidtMode3D(args.location.lat, args.location.lng);
            } else if (model3D.GLOBAL.editMode3D.status) {
                abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
            }
        });

        let event = map.addListener("modeChanged", (args) => {
            if (!args.is3DMode) {
                model3D.GLOBAL.editMode3D.status = false;
                model3D.GLOBAL.checkdraw = false;
                informap2d.setInfor2D();
                if (Info_Properties_Main_Object.GLOBAL.step !=="step-2D") {
                    $('#InforData .li-modal-data-right').eq(3).trigger('click');
                }
                
            }
        });

        let eventdragDrawEnd = map.addListener("dragEnd", (args) => {
            toastr.remove();
            if (typeof args.marker !== "undefined" && model3D.GLOBAL.editMode3D.status && !model3D.CheckPointInPolygonOrMulti(args.location.lat, args.location.lng)) {
                abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
            }
            if (typeof args.building !== "undefined" && args.building !== null) {
                if (!model3D.CheckPointInPolygonOrMulti(args.location.lat, args.location.lng)) abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
                else if (model3D.checkBuildingInPolygon(args.building)) abp.notify.warn(l('Mode3D:Waring:OutPolygon'));
            }
        }, { marker: true, building: true });

        $(document).on('change', model3D.SELECTORS.inforGeojson, function () {
            var text = $(this).val();
            text.trim() != "" && model3D.getGeojsonData3D($(this).val());
        });

        $(document).on('click', '.btn-reset-geojson3D', function () {
            $(model3D.SELECTORS.inforGeojson).val(model3D.GLOBAL.geojsonDefault3D);
            Info_Properties_Main_Object.setRisizeTextareaJson3d();

            $(model3D.SELECTORS.inforGeojson).trigger('change');
            if (model3D.GLOBAL.geojsonDefault3D == "") {
                $(model3D.SELECTORS.tableGeojson).children().remove();
                if (model3D.GLOBAL.building != null) {
                    model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                }
            } else {
                let object = JSON.parse(model3D.GLOBAL.geojsonDefault3D);
                model3D.GLOBAL.geojson3D = JSON.parse(JSON.stringify(object.geometry));
            }
            //model3D.GLOBAL.geojson3D = JSON.parse(JSON.stringify(model3D.GLOBAL.coordinatesDefault3D)); 
            //model3D.setTableData3D();
        });

        $(model3D.SELECTORS.radioChoosenMap).on('change', function () {

            let value = $(this).val();
            if (value == "SelectObject") {
                mapdata.GLOBAL.statusChosenMap = false;
                $(model3D.SELECTORS.choosenMapCancel).click();
                //$(model3D.SELECTORS.radioChoosenMap + '[value="MapObject"]').attr('disabled', true);
                $(model3D.SELECTORS.selectObjectChosenMap).attr('disabled', false);
                $(model3D.SELECTORS.choosenMapStart).attr('disabled', true);
            } else if (value == "MapObject") {
                mapdata.GLOBAL.statusChosenMap = true;
                $(model3D.SELECTORS.selectObjectChosenMap).val("").select2();
                map.setSelectedBuildings([]);
                $(model3D.SELECTORS.selectObjectChosenMap).attr('disabled', true);
                $(model3D.SELECTORS.choosenMapStart).attr('disabled', false);
            }
        });

        $(model3D.SELECTORS.choosenMapStart).on('click', function () {
            $(model3D.SELECTORS.radioChoosenMap + '[value="SelectObject"]').attr('disabled', true);
            $(this).css('display', 'none');
            $(model3D.SELECTORS.choosenMapEnd).css('display', 'block');
            $(model3D.SELECTORS.choosenMapCancel).css('display', 'block');
            mapdata.GLOBAL.statusChosenMap = true;
        });

        $(model3D.SELECTORS.choosenMapEnd).on('click', function () {
            $(model3D.SELECTORS.radioChoosenMap + '[value="SelectObject"]').attr('disabled', false);
            $(model3D.SELECTORS.choosenMapStart).css('display', 'block');
            $(this).css('display', 'none');
            $(model3D.SELECTORS.choosenMapCancel).css('display', 'none');
            mapdata.GLOBAL.statusChosenMap = false;
            if (model3D.GLOBAL.idObjectMapTemporary != "") {
                model3D.GLOBAL.objectChoosenMap.id = model3D.GLOBAL.idObjectMapTemporary;
                if (model3D.GLOBAL.building != null) {
                    model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                }
            }
        });

        $(model3D.SELECTORS.choosenMapCancel).on('click', function () {
            $(model3D.SELECTORS.radioChoosenMap + '[value="SelectObject"]').attr('disabled', false);
            $(model3D.SELECTORS.choosenMapStart).css('display', 'block');
            $(this).css('display', 'none');
            $(model3D.SELECTORS.choosenMapEnd).css('display', 'none');
            mapdata.GLOBAL.statusChosenMap = false;
            model3D.GLOBAL.objectChoosenMap.id = '';
            map.setSelectedBuildings([]);
            model3D.GLOBAL.idObjectMapTemporary = '';
        });

        $(model3D.SELECTORS.selectObjectChosenMap).on('change', function () {
            let id = $(this).val();

            if (model3D.GLOBAL.building != null) {
                model3D.GLOBAL.building.setMap(null);
                model3D.GLOBAL.building = null;
            }

            map.setSelectedBuildings([id]);
            model3D.GLOBAL.objectChoosenMap.id = id;
            //let object = mapdata.GLOBAL.listObjectMapInRadius.find(x => x.id == id);
            //if (object != null && object != undefined) {
            //    let camera = map.getCamera();
            //    camera.setTarget(object.location);
            //    camera.setZoom(20);
            //    map.moveCamera(camera);
            //}
        });

        //nut hủy 3d
        $(model3D.SELECTORS.btnCancelModel3D).on("click", function () {
            swal({
                title: l("Layer:Notification"),
                text: "Bạn có chắc chắn muốn xóa đối tượng 3D không?",
                icon: "warning",
                buttons: [
                    l("Layer:Cancel"),
                    l("Layer:Agree")
                ],
                dangerMode: true,
            }).then(function (isConfirm) {
                if (isConfirm) {
                    if (model3D.GLOBAL.building !== null && typeof model3D.GLOBAL.building !=="undefined") {
                        model3D.GLOBAL.building.setMap(null);
                        model3D.GLOBAL.building = null;
                    }
                    //send update xóa mode 3d
                    let id = (mapdata.GLOBAL.mainObjectSelected != null) ? mapdata.GLOBAL.mainObjectSelected.id : "";
                    if (typeof id != "undefined" && id.length > 0) model3D.DeleteMode3DObject(id);
                }
            })
        });
        //check box tự động vẽ 3d
        $(model3D.SELECTORS.checkboxMode3d).on("change", function () {
            toastr.remove();
            if (model3D.GLOBAL.checkdraw && (model3D.GLOBAL.polygonArray.length > 0 || model3D.GLOBAL.markerArray.length > 0)) {
                $(this).prop('checked', false);
                abp.notify.warn(l("Mode3D:Waring:EndDrawMode"));
                return false;
            }
            if (!model3D.GLOBAL.editMode3D.status) {
                if ($(this).is(':checked')) {
                    model3D.showObjectExample3D(true);
                    let geo = model3D.getGeojsonByBuilding();
                    model3D.setGeojsonData3D(geo);
                    model3D.setTableData3D(geo);
                    $(model3D.SELECTORS.drawCancel).attr('disabled', false);
                } else {
                    model3D.GLOBAL.building != null && model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                    $(model3D.SELECTORS.inforGeojson).val("");
                    $(model3D.SELECTORS.tableGeojson).children().remove();
                    $(model3D.SELECTORS.drawCancel).attr('disabled', true);
                    model3D.GLOBAL.checkdraw = true;
                }
            } else if (model3D.GLOBAL.editMode3D.status) {
                $(this).prop('checked', false);
                abp.notify.warn(l("Mode3D:Waring:EndEidtMode"));
            }
        });
        //edit mode 3d
        $(model3D.SELECTORS.btnEditMode3D).on("click", function () {
            if (model3D.GLOBAL.building !== null) {
                let path = model3D.GLOBAL.building.getCoordinates();
                model3D.editMode3Ddraw(path, "polygon");
                $(model3D.SELECTORS.btnEditMode3D).hide();
                $(model3D.SELECTORS.btnEndMode3D).show();
                model3D.GLOBAL.editMode3D.status = true;
                model3D.GLOBAL.checkdraw = false;
                $(model3D.SELECTORS.checkboxMode3d).prop('checked', false);
            }
        });
        $(model3D.SELECTORS.btnEndMode3D).on("click", function () {
            model3D.endEidtMode3D();
            //let path = mapdata.GLOBAL.listArea.polygonArea.getPaths();
            //model3D.showMode3DEndEdit(path[0]);
            //$(model3D.SELECTORS.btnEditMode3D).show();
            //$(model3D.SELECTORS.btnEndMode3D).hide();
            //let geo = model3D.getGeojsonByBuilding();
            //model3D.setGeojsonData3D(geo);
            //model3D.setTableData3D(geo);
            //model3D.GLOBAL.editMode3D.status = false;
            //model3D.GLOBAL.checkdraw = false;
            //$(model3D.SELECTORS.checkboxMode3d).prop('checked', false);

        });
    },
    createObjectModelByFile: function (lat, lng, scaleNumber, bearingNumber, elevationNumber, objData, textureData) {
        let position;
        if (model3D.GLOBAL.building != null) {
            position = model3D.GLOBAL.building.getPosition();
            model3D.GLOBAL.building.setMap(null);
            model3D.GLOBAL.building = null;
            model3D.resetModelExample();
        }
        const blob = new Blob([objData]);
        var objectURL = URL.createObjectURL(blob);
        var object2 = {
            name: "Tòa nhà",
            position: (typeof position !== "undefined" && position !== null) ? position : { lat: lat, lng: lng },
            objectUrl: objectURL,
            texture: textureData,
            scale: scaleNumber,
            bearing: bearingNumber,
            elevation: elevationNumber,
            id: "",
            height: 1,
            location: (typeof position !== "undefined" && position !== null) ? position : { lat: lat, lng: lng },
            coordinates: []
        };
        //tạo đối tượng model từ ObjectOption
        model3D.GLOBAL.building = model3D.newMap4dBuilding(object2);
        model3D.GLOBAL.building.setSelected(true);
        model3D.GLOBAL.building.setDraggable(true);
        //setTimeout(function () {
        model3D.GLOBAL.building.setMap(map);
        //}, 200);
    },
    setData: function () {
        if (mapdata.GLOBAL.mainObjectSelected != null) {
            //if (mapdata.GLOBAL.mainObjectSelected.geometry.type == "Polygon") {
            //    let array = [];
            //    $.each(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[0], function (i, item) {
            //        array.push([item[0], item[1]]);
            //    });
            //    var polygon = turf.polygon([array]);
            //    var centroid = turf.centroid(polygon);
            //    $(model3D.SELECTORS.lat).val(centroid.geometry.coordinates[1]);
            //    $(model3D.SELECTORS.lng).val(centroid.geometry.coordinates[0]);
            //}

            if (mapdata.GLOBAL.mainObjectSelected.object3D != null) {
                //model3D.GLOBAL.building != null && model3D.GLOBAL.building.setMap(null);
                //model3D.GLOBAL.building = model3D.newMap4dBuilding(mapdata.GLOBAL.mainObjectSelected.object3D);
                //model3D.GLOBAL.building.setSelected(true);
                //model3D.GLOBAL.building.setMap(map);

                $(model3D.SELECTORS.scale).val(mapdata.GLOBAL.mainObjectSelected.object3D.scale);
                $(model3D.SELECTORS.height).val(mapdata.GLOBAL.mainObjectSelected.object3D.height);
                $(model3D.SELECTORS.elevation).val(mapdata.GLOBAL.mainObjectSelected.object3D.elevation);
                $(model3D.SELECTORS.bearing).val(mapdata.GLOBAL.mainObjectSelected.object3D.bearing);
            }
        }
    },
    hideModel3D: function () {
        if (model3D.GLOBAL.building != null) {
            model3D.GLOBAL.building.setMap(null);
        }
    },
    resetModelExample: function () {
        $(model3D.SELECTORS.selectParadigmId).prop('selectedIndex', 0);
        $('#select').trigger('change');
    },
    resetModelFile: function () {
        const dt = new DataTransfer();
        $(model3D.SELECTORS.choosenFileObj)[0].files = dt.files;
        $(model3D.SELECTORS.chooseImg)[0].files = dt.files;
        $(model3D.SELECTORS.imgUpload).attr("src", "/common/image-default.svg");
        model3D.GLOBAL.fileObject3d.imageurl = null;
        model3D.GLOBAL.fileObject3d.texture3D = null;
    },
    resetDraw: function () {
        for (i = 0; i < model3D.GLOBAL.polygonArray.length; i++) {
            model3D.GLOBAL.polygonArray[i].setMap(null);
        }
        for (i = 0; i < model3D.GLOBAL.markerArray.length; i++) {
            model3D.GLOBAL.markerArray[i].setMap(null);
        }
        for (i = 0; i < model3D.GLOBAL.polylineArray.length; i++) {
            model3D.GLOBAL.polylineArray[i].setMap(null);
        }

        if (model3D.GLOBAL.polygonArray != null) {
            model3D.GLOBAL.polygonArray = [];
        }
        if (model3D.GLOBAL.polylineArray != null) {
            model3D.GLOBAL.polylineArray = [];
        }
        if (model3D.GLOBAL.markerArray != null) {
            model3D.GLOBAL.markerArray = [];
        }
        if (model3D.GLOBAL.polylineDraw != null) {
            model3D.GLOBAL.polylineDraw.setMap(null);
            model3D.GLOBAL.polylineDraw = null;
        }
        model3D.GLOBAL.coor = [];
    },
    resetAll: function () {
        var content = Info_Properties_Main_Object.getContent();

        $(content + " " + model3D.SELECTORS.selectParadigmId).prop('selectedIndex', 0);
        $(content + " " + '#select').trigger('change');
        $(content + " " + model3D.SELECTORS.paraSelected).val('para-paraEx');
        $(content + " " + model3D.SELECTORS.paraSelected).trigger('change');
        $(content + " " + model3D.SELECTORS.lat).val('');
        $(content + " " + model3D.SELECTORS.lng).val('');
        $(content + " " + model3D.SELECTORS.lat).val('1');
        $(content + " " + model3D.SELECTORS.lng).val('0');
        $(content + " " + model3D.SELECTORS.lat).val('1');
        model3D.hideModel3D();
        model3D.resetDraw();
        model3D.resetModelFile();
        mapdata.GLOBAL.statusChosenMap = false;
        $(content + " " + model3D.SELECTORS.tableGeojson).children().remove();
        $(content + " " + model3D.SELECTORS.inforGeojson).val('');
        Info_Properties_Main_Object.setRisizeTextareaJson3d();

        model3D.GLOBAL.fileObject3d.imageurl = null;
        model3D.GLOBAL.fileObject3d.texture3D = null;

        model3D.GLOBAL.fileObject3d.objurl = '';
        model3D.GLOBAL.fileObject3d.texture3D = '';
        model3D.GLOBAL.urlObject3d.objurl = '';
        model3D.GLOBAL.urlObject3d.texture3D = '';
    },
    showObjectExample3D: function (isCheck = false) {
        if (mapdata.GLOBAL.mainObjectSelected.geometry.type == "Polygon") {
            let coor = [];
            $.each(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[0], function (i, obj) {
                let check = coor.find(x => x.lat === obj[1] && x.lng === obj[0]);
                if (check == undefined || check == null) {
                    coor.push({ lng: obj[0], lat: obj[1] });
                }
            });
            if (mapdata.GLOBAL.mainObjectSelected.object3D == null || (mapdata.GLOBAL.mainObjectSelected.object3D.coordinates.length == 0
                && (mapdata.GLOBAL.mainObjectSelected.object3D.objectUrl == "" || mapdata.GLOBAL.mainObjectSelected.object3D.objectUrl == null)
                && (mapdata.GLOBAL.mainObjectSelected.object3D.texture == "" || mapdata.GLOBAL.mainObjectSelected.object3D.texture == null)) || isCheck) {
                var object2 = {
                    name: "Tòa nhà",
                    //position: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                    objectUrl: null,
                    texture: null,
                    scale: $(model3D.SELECTORS.scale).val(),
                    bearing: $(model3D.SELECTORS.bearing).val(),
                    elevation: $(model3D.SELECTORS.elevation).val(),
                    id: "",
                    height: $(model3D.SELECTORS.height).val(),
                    //location: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                    coordinates: coor
                };
                model3D.GLOBAL.building != null && model3D.GLOBAL.building.setMap(null);
                model3D.GLOBAL.building = null;
                model3D.GLOBAL.building = model3D.newMap4dBuilding(object2);
                model3D.GLOBAL.building.setSelected(true);
                model3D.GLOBAL.building.setMap(map);
                model3D.GLOBAL.building.setDraggable(true);
                model3D.resetModelFile();
            }
        }
    },
    newMap4dBuilding: function (data) {
        var building = null;
        if (data.coordinates != null && data.coordinates.length > 0) {
            building = new map4d.Building({
                name: data.name,
                id: data.id,
                scale: data.scale, // Tỉ lệ vẽ của đối tượng
                bearing: data.bearing, // Góc quay của đối tượng
                elevation: data.elevation, // Độ cao so với mặt nước biển
                position: data.location,
                coordinates: data.coordinates,
                height: data.height
            });
        } else {
            //tạo đối tượng model từ ObjectOption
            building = new map4d.Building({
                name: data.name,
                id: data.id,
                scale: data.scale, // Tỉ lệ vẽ của đối tượng
                bearing: data.bearing, // Góc quay của đối tượng
                elevation: data.elevation, // Độ cao so với mặt nước biển
                position: data.location,
                model: data.objectUrl,
                texture: data.texture
            });
        }
        return building;
    },
    setGeojsonData3D: function (infor) {
        //let infor = model3D.GLOBAL.geojson3D;
        if (infor !== null) {
            let inforgeojson = {
                "type": "Feature",
                "properties": {},
                "geometry": infor
            };
            var str = JSON.stringify(inforgeojson, undefined, 4);
            $(model3D.SELECTORS.inforGeojson).val(str);
            Info_Properties_Main_Object.setRisizeTextareaJson3d();

            model3D.GLOBAL.objectGeojson3D = inforgeojson;
        }
    },
    setTableData3D: function (geojson) {
        //let geojson = model3D.GLOBAL.geojson3D;
        if (geojson !== null) {
            let html = model3D.getCoordinates(geojson);
            $(model3D.SELECTORS.tableGeojson).children().remove();
            $(model3D.SELECTORS.tableGeojson).append(html);
        }
    },
    getGeojsonData3D: function (str) {
        let object = JSON.parse(str);
        if (object != undefined && object.geometry != undefined) {
            if (object.geometry.type != undefined && object.geometry.type == "Polygon") {
                if (object.geometry.coordinates != undefined && object.geometry.coordinates.length > 0) {
                    if (object.geometry.coordinates[0].length >= 4) {
                        if (object.geometry.coordinates[0][0][0] == object.geometry.coordinates[0][object.geometry.coordinates[0].length - 1][0]
                            && object.geometry.coordinates[0][0][1] == object.geometry.coordinates[0][object.geometry.coordinates[0].length - 1][1]) {
                            model3D.GLOBAL.objectGeojson3D = JSON.parse(JSON.stringify(object));
                            let coor = [];
                            $.each(model3D.GLOBAL.objectGeojson3D.geometry.coordinates[0], function (i, obj) {
                                let check = coor.find(x => x.lat === obj[1] && x.lng === obj[0]);
                                if (check == undefined || check == null) {
                                    coor.push({ lng: obj[0], lat: obj[1] });
                                }
                            });
                            var polygon = turf.polygon(model3D.GLOBAL.objectGeojson3D.geometry.coordinates);
                            var centroid = turf.centroid(polygon);
                            $(model3D.SELECTORS.lat).val(centroid.geometry.coordinates[1]);
                            $(model3D.SELECTORS.lng).val(centroid.geometry.coordinates[0]);
                            var object2 = {
                                name: "Tòa nhà",
                                position: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                                objectUrl: null,
                                texture: null,
                                scale: $(model3D.SELECTORS.scale).val(),
                                bearing: $(model3D.SELECTORS.bearing).val(),
                                elevation: $(model3D.SELECTORS.elevation).val(),
                                id: "",
                                height: $(model3D.SELECTORS.height).val(),
                                location: { lat: $(model3D.SELECTORS.lat).val(), lng: $(model3D.SELECTORS.lng).val() },
                                coordinates: coor
                            };

                            model3D.GLOBAL.building != null && model3D.GLOBAL.building.setMap(null);
                            model3D.GLOBAL.building = null;
                            model3D.GLOBAL.building = model3D.newMap4dBuilding(object2);
                            model3D.GLOBAL.building.setSelected(true);
                            model3D.GLOBAL.building.setMap(map);
                            model3D.GLOBAL.geojson3D.coordinates = model3D.GLOBAL.objectGeojson3D.geometry.coordinates;
                            model3D.setTableData3D();
                        }
                    }
                }
            }
        }
    },
    setInfo3D: function () {
        if (model3D.GLOBAL.building != null) {
            if (model3D.GLOBAL.building.id != null) {
                var arraygeojson = [];
                $.each(model3D.GLOBAL.building.properties.coordinates, function (i, obj) {
                    arraygeojson.push([obj.lng, obj.lat]);
                });
                if (arraygeojson.length > 0) {
                    arraygeojson.push([model3D.GLOBAL.building.properties.coordinates[0].lng, model3D.GLOBAL.building.properties.coordinates[0].lat]);
                    model3D.GLOBAL.geojson3D.coordinates.push(arraygeojson);
                    let infor = model3D.GLOBAL.geojson3D;
                    if (infor !== null) {
                        let inforgeojson = {
                            "type": "Feature",
                            "properties": {},
                            "geometry": infor
                        };
                        var str = JSON.stringify(inforgeojson, undefined, 4);
                        model3D.GLOBAL.geojsonDefault3D = str;
                    }
                    model3D.setGeojsonData3D(infor);
                    model3D.setTableData3D(infor);
                    var polygon = turf.polygon([arraygeojson]);
                    var centroid = turf.centroid(polygon);
                    $(model3D.SELECTORS.lat).val(centroid.geometry.coordinates[1]);
                    $(model3D.SELECTORS.lng).val(centroid.geometry.coordinates[0]);
                } else {
                    $(model3D.SELECTORS.lat).val(model3D.GLOBAL.building.properties.location.lat);
                    $(model3D.SELECTORS.lng).val(model3D.GLOBAL.building.properties.location.lng);
                }
            } else {
                model3D.GLOBAL.building = null;
            }
        }
    },
    eventChoosenMap: function () {
        map.addListener("click", (args) => {
            if (mapdata.GLOBAL.statusChosenMap) {
                if (model3D.GLOBAL.building != null) {
                    model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                }
                //console.log(args);
                let id = args.building.id;
                map.setSelectedBuildings([id]);
                model3D.GLOBAL.idObjectMapTemporary = id;
                model3D.GLOBAL.objectChoosenMap.id = id;
            }
        }, { mapbuilding: true });
    },
    SendFileDocument: function (file) {
        var formData = new FormData();
        formData.append('file', file);
        let url = '';
        $.ajax({
            url: "/api/HTKT/file/document",
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            async: false,
            success: function (data) {
                if (data.code == "ok") {
                    var re = data.result;
                    url = re.url;
                }
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });
        return url;
    },
    DeleteFileDocument: function (url) {
        var urlEnCode = encodeURIComponent(url);
        var check = false;
        $.ajax({
            url: "/api/HTKT/file/deletedocument",
            type: 'POST',
            data: JSON.stringify({ url: urlEnCode }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.code == "ok") {
                    check = data.result;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + ": " + errorThrown);
            }
        });
        return check;
    },
    MoveToBuilding3D: function () {
        let array = [];
        switch (mapdata.GLOBAL.mainObjectSelected.geometry.type.toLowerCase()) {
            case "polygon":
                array = [];
                $.each(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[0], function (i, item) {
                    array.push([item[0], item[1]]);
                });
                var polygon = turf.polygon([array]);
                var centroid = turf.centroid(polygon);
                map.enable3dMode(true);
                $(model3D.SELECTORS.lat).val(centroid.geometry.coordinates[1]);
                $(model3D.SELECTORS.lng).val(centroid.geometry.coordinates[0]);
                break;
            case "multipolygon":
                array = [];
                for (var im = 0; im < mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[0].length; im++) {
                    $.each(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[0][im], function (i, item) {
                        array.push([item[0], item[1]]);
                    });
                }
                var polygon2 = turf.polygon([array]);
                var centroid2 = turf.centroid(polygon2);
                map.enable3dMode(true);
                $(model3D.SELECTORS.lat).val(centroid2.geometry.coordinates[1]);
                $(model3D.SELECTORS.lng).val(centroid2.geometry.coordinates[0]);
                break;
            case "point":
                map.enable3dMode(true);
                $(model3D.SELECTORS.lat).val(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[1]);
                $(model3D.SELECTORS.lng).val(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[0]);
                break;
            case "multipoint":
                array = [];
                for (var im = 0; im < mapdata.GLOBAL.mainObjectSelected.geometry.coordinates.length; im++) {
                    var item = mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[im];
                    array.push([item[0], item[1]]);
                }

                var polygon = turf.points(array);
                var centroid = turf.centroid(polygon);

                map.enable3dMode(true);
                $(model3D.SELECTORS.lat).val(centroid.geometry.coordinates[1]);
                $(model3D.SELECTORS.lng).val(centroid.geometry.coordinates[0]);
                break;
            case "linestring":
                array = [];
                $.each(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates, function (i, item) {
                    array.push([item[0], item[1]]);
                });
                var features = turf.points(array);
                var centroid4 = turf.center(features);
                map.enable3dMode(true);
                $(model3D.SELECTORS.lat).val(centroid4.geometry.coordinates[1]);
                $(model3D.SELECTORS.lng).val(centroid4.geometry.coordinates[0]);
            case "multilinestring":
                array = [];
                for (var im2 = 0; im2 < mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[0].length; im2++) {
                    $.each(mapdata.GLOBAL.mainObjectSelected.geometry.coordinates[im2], function (i, item) {
                        array.push([item[0], item[1]]);
                    });
                }
                var features2 = turf.points(array);
                var centroid5 = turf.center(features2);
                map.enable3dMode(true);
                $(model3D.SELECTORS.lat).val(centroid5.geometry.coordinates[1]);
                $(model3D.SELECTORS.lng).val(centroid5.geometry.coordinates[0]);
        }
    },
    RemoveBulding3D: function () {
        $(model3D.SELECTORS.choosenMapCancel).click();
        model3D.GLOBAL.checkdraw = false; // vẽ 3d
        if (model3D.GLOBAL.building != null) {
            model3D.GLOBAL.building.setMap(null);
            model3D.GLOBAL.building = null;
        }
        model3D.GLOBAL.coor = [];
        model3D.resetModelFile();

        $(model3D.SELECTORS.inforGeojson).val(model3D.GLOBAL.geojsonDefault3D);
        auto_risze_textarea($(model3D.SELECTORS.inforGeojson));
    },
    //cập nhật lại thông tin dữ liệu đã vẽ
    SetInforOptionModel3D: function (dataobject) {

        let type = dataobject !== null ? dataobject.type : "";
        this.showHideOptionMode(type);
        $(model3D.SELECTORS.mode3dDraw).hide();
        if (dataobject !== null) {
            $(this.SELECTORS.scale).val(dataobject.scale);
            $(this.SELECTORS.bearing).val(dataobject.bearing);
            $(this.SELECTORS.elevation).val(dataobject.elevation);
        }
        $(this.SELECTORS.paraSelected).val(type).select2();
        $(this.SELECTORS.btnCancelModel3D).removeAttr("disabled");
        model3D.GLOBAL.buildingOld = {
            type: type,
            building: model3D.GLOBAL.building
        }
        switch (type) {
            case "para-paraEx": //mô hình mẫu
                let obj = mapdata.GLOBAL.list.find(x => x.name == dataobject.name);
                $(this.SELECTORS.selectParadigmId).val(obj.id).select2();
                //$('#selectParadigm').val('59f069228e07bc16c40956cb').select2();
                //model3D.GLOBAL.building = obj;
                //$(this.SELECTORS.selectParadigmId).trigger('change');
                $(model3D.SELECTORS.parameterMap).show();
                $(model3D.SELECTORS.btnCancelModel3D).attr('disabled', false);
                break;
            case "para-choosenFile": // chọn file
                var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${dataobject.objectUrl}" data-toggle="tooltip" data-placement="bottom">${dataobject.objectUrl}</div>`;
                $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).html(html);
                $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).removeClass('hidden');

                let texture = dataobject.texture !== null && dataobject.texture.length > 1 ? dataobject.texture : "/common/image-default.svg";
                var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                            <div class="name-file-obj" title="${dataobject.texture}" data-toggle="tooltip" data-placement="bottom">${dataobject.texture}</div>`;
                $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).html(html);
                $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).removeClass('hidden');
                $(model3D.SELECTORS.btnCancelModel3D).attr('disabled', false);
                $(model3D.SELECTORS.parameterMap).show();
                $(model3D.SELECTORS.imgUpload).attr("src", texture);
                break;
            case "para-draw": // vẽ
                $(model3D.SELECTORS.drawCancel).attr('disabled', false);
                $(model3D.SELECTORS.btnCancelModel3D).attr('disabled', false);
                $(model3D.SELECTORS.parameterMap).show();
                $(model3D.SELECTORS.mode3dDraw).show();
                let geo = model3D.getGeojsonByBuilding();
                model3D.setGeojsonData3D(geo);
                model3D.setTableData3D(geo);
                model3D.GLOBAL.checkdraw = false;
                break;
            case "para-choosenMap": // chọn trên map
                $(model3D.SELECTORS.btnCancelModel3D).attr('disabled', false);
                if (dataobject !== null && typeof dataobject.id !== "undefined" && dataobject.id !== null && dataobject.id !== "") {
                    $(model3D.SELECTORS.selectObjectChosenMap).val(dataobject.id).select2();
                }else
                    $(model3D.SELECTORS.selectObjectChosenMap).val("").select2();
                break;
            default://xóa hết dữ liệu
                $(this.SELECTORS.paraSelected).val("para-choosen");
                $(this.SELECTORS.paraSelected).trigger('change');
                $(this.SELECTORS.selectParadigmId).val("");
                $(this.SELECTORS.selectParadigmId).trigger('change');
                $(this.SELECTORS.actionFileObj).trigger('click');
                $(this.SELECTORS.height).val(10);
                $(this.SELECTORS.selectObject3D).prop('checked', true);
                $(this.SELECTORS.scale).val(1);
                $(this.SELECTORS.bearing).val(0);
                $(this.SELECTORS.elevation).val(0);
                $(this.SELECTORS.tableGeojson).children().remove();
                $(this.SELECTORS.inforGeojson).val("");
                $(this.SELECTORS.btnCancelModel3D).attr("disabled", "disabled");
                $(model3D.SELECTORS.selectObjectChosenMap).val("").select2();
                break;
        }
    },
    showHideOptionMode: function (value) {
        this.ClearSelectMode3D();
        switch (value) {
            case "para-paraEx":
                $(model3D.SELECTORS.paraExClass).show();
                $(model3D.SELECTORS.choosenFileClass).hide();
                $(model3D.SELECTORS.drawClass).hide();
                $(model3D.SELECTORS.choosenMapClass).hide();
                break;
            case "para-choosenFile":
                $(model3D.SELECTORS.paraExClass).hide();
                $(model3D.SELECTORS.choosenFileClass).show();
                $(model3D.SELECTORS.drawClass).hide();
                $(model3D.SELECTORS.choosenMapClass).hide();
                $(model3D.SELECTORS.height).val(20);
                break;
            case "para-draw":
                model3D.GLOBAL.checkdraw = true; // vẽ 3d
                $(model3D.SELECTORS.paraExClass).hide();
                $(model3D.SELECTORS.choosenFileClass).hide();
                $(model3D.SELECTORS.drawClass).show();
                $(model3D.SELECTORS.choosenMapClass).hide();
                $(model3D.SELECTORS.mode3dDraw).show();
                $(model3D.SELECTORS.height).val(20);

                setTimeout(function () {
                    Info_Properties_Main_Object.setRisizeTextareaJson3d();
                }, 300)
                break;
            case "para-choosenMap":
                $(model3D.SELECTORS.paraExClass).hide();
                $(model3D.SELECTORS.choosenFileClass).hide();
                $(model3D.SELECTORS.drawClass).hide();
                $(model3D.SELECTORS.choosenMapClass).show();
                $(model3D.SELECTORS.parameterMap).hide();
                $(model3D.SELECTORS.selectObject3D).prop('checked', true);
                $(model3D.SELECTORS.selectObjectChosenMap).attr('disabled', false);
                break;
            default:
                model3D.GLOBAL.checkdraw = false;// trạng thái vẽ
                model3D.RemoveBulding3D();
                model3D.resetDraw();
                $(model3D.SELECTORS.paraExClass).hide();
                $(model3D.SELECTORS.choosenFileClass).hide();
                $(model3D.SELECTORS.drawClass).hide();
                $(model3D.SELECTORS.choosenMapClass).hide();
                $(model3D.SELECTORS.parameterMap).hide();
                break;
        }
    },
    //delete 3D trên database
    DeleteMode3DObject: function (id) {
        var idCurrent = id;
        $.ajax({
            type: "POST",
            dataType: 'json',
            processData: false,
            contentType: "application/json",
            async: false,
            url: model3D.CONSTS.URL_AJAXDeleteMode3D,
            data: JSON.stringify(id),
            success: function (data) {
                if (data.code === "ok") {
                    let obj = mapdata.GLOBAL.listObject3D.find(x => x.id == idCurrent);
                    if (typeof obj !== "undefined" && obj !== null) {
                        obj.object.setMap(null);
                        let index = mapdata.GLOBAL.listObject3D.findIndex(x => x.id == idCurrent);
                        mapdata.GLOBAL.listObject3D.splice(index, 1);
                        abp.notify.success("Đã xóa thành công");
                        if ($(model3D.SELECTORS.paraSelected).val() === "para-draw") {
                            $(model3D.SELECTORS.drawCancel).trigger("click");
                        }
                    } else {
                        let objchosen = mapdata.GLOBAL.listObjectByDirectory.find(x => x.object3D != null && x.object3D.type == "para-choosenMap" && x.id == idCurrent);
                        if (typeof objchosen !== "undefined" && objchosen !== null) {
                            $(model3D.SELECTORS.selectObjectChosenMap).val("").select2();
                            map.setSelectedBuildings([]);
                            objchosen.object3D = null;
                            mapdata.GLOBAL.statusChosenMap = false;
                            abp.notify.success("Đã xóa thành công");
                        }
                    }
                    model3D.GLOBAL.buildingOld = {
                        type: "",
                        building: null
                    };
                    model3D.ClearSelectMode3D();
                    //$(model3D.SELECTORS.btnCancelModel3D).attr('disabled', false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
                //ViewMap.showLoading(false);
            }
        });
    },
    //clear select mode
    ClearSelectMode3D: function () {
        //$(this.SELECTORS.paraSelected).val("para-choosen");
        //$(this.SELECTORS.paraSelected).trigger('change');
        $(this.SELECTORS.selectParadigmId).val("").select2();
        //$(this.SELECTORS.selectParadigmId).trigger('change');
        $(this.SELECTORS.actionFileObj).trigger('click');
        $(this.SELECTORS.height).val(10);
        $(this.SELECTORS.selectObject3D).prop('checked', true);
        $(this.SELECTORS.scale).val(1);
        $(this.SELECTORS.bearing).val(0);
        $(this.SELECTORS.elevation).val(0);
        $(this.SELECTORS.tableGeojson).children().remove();
        $(this.SELECTORS.inforGeojson).val("");
        $(this.SELECTORS.checkboxMode3d).prop('checked', false);
        $(this.SELECTORS.btnCancelModel3D).attr("disabled", "disabled");
        $(this.SELECTORS.drawCancel).attr("disabled", "disabled");
        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).html("");
        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.file_upload_content}`).addClass('hidden');
        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).html("");
        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.image_upload_content}`).addClass('hidden');
        $(this.SELECTORS.imgUpload).attr("src", "/common/image-default.svg");
        this.GLOBAL.object3D = "";
        this.GLOBAL.texture3D = "";
        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_image_obj}`).addClass('hidden');
        $(`${model3D.SELECTORS.content_3d} ${model3D.SELECTORS.content_error_file_obj}`).addClass('hidden');

        $(model3D.SELECTORS.selectObjectChosenMap).val('').select2();

    },
    clearOptionMode3D: function () {
        this.GLOBAL.checkdraw = false;
        this.GLOBAL.editMode3D.status = false;
    },
    endEidtMode3D: function () {
        if (typeof mapdata.GLOBAL.listArea.polygonArea !== "undefined" && mapdata.GLOBAL.listArea.polygonArea !== null) {
            let path = mapdata.GLOBAL.listArea.polygonArea.getPaths();
            model3D.showMode3DEndEdit(path[0]);
        }
        $(model3D.SELECTORS.btnEditMode3D).show();
        $(model3D.SELECTORS.btnEndMode3D).hide();
        let geo = model3D.getGeojsonByBuilding();
        model3D.setGeojsonData3D(geo);
        model3D.setTableData3D(geo);
        model3D.GLOBAL.editMode3D.status = false;
        model3D.GLOBAL.checkdraw = false;
        $(model3D.SELECTORS.checkboxMode3d).prop('checked', false);
    },
    cancelMode3D: function () {
        model3D.endEidtMode3D();
        model3D.ClearSelectMode3D();
    },
    setDataBuildingTemp: function (object3D, check = false) {
        if (object3D !== null && typeof object3D !== "undefined") {
            let texture3D = "";
            if (model3D.GLOBAL.building != null) {
                object3D = typeof object3D !== "undefined" && object3D !== null ? object3D : model3D.GLOBAL.building.properties.modelUrl;
                texture3D = typeof model3D.GLOBAL.texture3D !== "undefined" && model3D.GLOBAL.texture3D !== null && model3D.GLOBAL.texture3D !== "" ? model3D.GLOBAL.texture3D : (!check ? model3D.GLOBAL.building.properties.textureUrl : "");
            }
            model3D.createObjectModelByFile($(model3D.SELECTORS.lat).val(), $(model3D.SELECTORS.lng).val(),
                $(model3D.SELECTORS.scale).val(), $(model3D.SELECTORS.bearing).val(), $(model3D.SELECTORS.elevation).val(),
                object3D, texture3D);
        } else {
            if (model3D.GLOBAL.building != null) {
                model3D.GLOBAL.building.setMap(null);
                model3D.GLOBAL.building = null;
                model3D.resetModelExample();
            }
        }
    },
    //reset sau khi lưu object 3D
    resetDataObject3DAfterSave: function (object3D, id) {
        let obj = mapdata.GLOBAL.listObject3D.find(x => x.id == id);

        let object3dTemp = model3D.newMap4dBuilding(object3D);
        object3dTemp.setUserData(id);
        object3dTemp.setMap(null);
        if (typeof obj === "undefined") {
            mapdata.GLOBAL.listObject3D.push({ id: id, object: object3dTemp });
        } else {
            obj.object = null;
            obj.object = object3dTemp;
        }

        //if (obj != undefined && obj != null && obj.object !== null) { // hightlight 3d
        ////    //mapdata.visibleHightlight(false);
        ////    //if (obj3d.object.getMap() === null) obj3d.object.setMap(map);
        //    model3D.GLOBAL.building = obj.object;
        ////    model3D.GLOBAL.building.setMap(map);
        //    model3D.GLOBAL.building.setSelected(true);
        ////    //model3D.GLOBAL.building.setDraggable(true);
        //}

    },
    /*---end button insert,eidt---*/

    //#region -------------waring and stop draw-------
    CheckPointInPolygonOrMulti: function (lat, lng) {
        var check = false;
        let idobject = model3D.GLOBAL.idObject2D !== "" ? model3D.GLOBAL.idObject2D : mapdata.GLOBAL.mainObjectSelected.id;
        var obj = mapdata.GLOBAL.listObjectByDirectory.filter(x => x.id == idobject);
        let latNew = model3D.getLatLngConvert(lat);  //Math.floor(lat * 10000000) / 10000000;
        let lngNew = model3D.getLatLngConvert(lng); //Math.floor(lng * 10000000) / 10000000;
        var pt = turf.point([lngNew, latNew]);
        //var pt = turf.point([lng, lat]);
        var shape;
        if (obj !== null && obj.length > 0 && (obj[0].geometry.type.toLowerCase() === "polygon" || obj[0].geometry.type.toLowerCase() === "multipolygon")) {
            if (obj[0].geometry.type.toLowerCase() === "polygon") {
                let coordinates = model3D.getConvertCoordinate(obj[0].geometry);
                shape = turf.polygon(coordinates);
                //shape = turf.polygon(obj[0].geometry.coordinates);
            } else {
                let coordinates = model3D.getConvertCoordinate(obj[0].geometry);
                shape = turf.multiPolygon(coordinates);
                //shape = turf.multiPolygon(obj[0].geometry.coordinates);
            }
            check = turf.booleanPointInPolygon(pt, shape);
        }
        if (obj[0].geometry.type.toLowerCase() === "point" || obj[0].geometry.type.toLowerCase() === "linestring" || obj[0].geometry.type.toLowerCase() === "multipoint" || obj[0].geometry.type.toLowerCase() === "multilinestring"){
            return true;
        }
        return check;
    },
    showDataGeojson3D: function (geojsonString) {
        mapdata.GLOBAL.features = map.data.addGeoJson(geojsonString);
        mapdata.clearAllDraw();
    },
    //#endregion

    //#region -------------Edit draw mode 3D ---------
    editMode3Ddraw: function (path, type) {
        switch (type) {
            case "polygon":
                coordinates = [];
                $.each(path, function (i, obj) {
                    if (i === 0 || (obj.lat !== path[0].lat && obj.lng !== path[0].lng)) {
                        mapdata.createMarkerDrawLoDat(obj.lat, obj.lng, 2)
                        let latLng = { lat: obj.lat, lng: obj.lng };
                        coordinates.push(latLng);
                    }
                });
                coordinates.push(coordinates[0])
                mapdata.createPolygonArea([coordinates]);
                mapdata.setDragPoligonMarker(mapdata.GLOBAL.listArea.listMarkerArea);
                if (model3D.GLOBAL.building != null) {
                    model3D.GLOBAL.building.setMap(null);
                    model3D.GLOBAL.building = null;
                }
                break;
        }
    },
    showMode3DEndEdit: function (path) {
        var object2 = {
            name: "Tòa nhà",
            objectUrl: null,
            texture: null,
            scale: $(model3D.SELECTORS.scale).val(),
            bearing: $(model3D.SELECTORS.bearing).val(),
            elevation: $(model3D.SELECTORS.elevation).val(),
            id: "",
            height: $(model3D.SELECTORS.height).val(),
            coordinates: path
        };
        model3D.GLOBAL.building != null && model3D.GLOBAL.building.setMap(null);
        model3D.GLOBAL.building = null;
        model3D.GLOBAL.building = model3D.newMap4dBuilding(object2);
        model3D.GLOBAL.building.setDraggable(true);
        model3D.GLOBAL.building.setSelected(true);
        model3D.GLOBAL.building.setMap(map);
        model3D.resetModelFile();

        mapdata.clearAllDraw();

    },
    getGeojsonByBuilding: function () {
        let array = [];
        if (model3D.GLOBAL.building !== null && typeof model3D.GLOBAL.building !== "undefined") {
            $.each(model3D.GLOBAL.building.getCoordinates(), function (i, obj) {
                array.push([obj.lng, obj.lat]);
            });
            let geo = {
                type: "Polygon",
                coordinates: [array]
            }
            return geo;
        }
        return null;
    },
    setPolygonEidtMode3D: function (lat, lng) {
        let path = JSON.parse(JSON.stringify(mapdata.GLOBAL.listArea.polygonArea.paths[0]));
        let edit = { distance: -1, count: -1 };
        let pt = turf.point([lng, lat]);
        for (var i = 0; i < path.length - 1; i++) {
            let p1 = [path[i].lng, path[i].lat];
            let p2 = [path[i + 1].lng, path[i + 1].lat];
            var line = turf.lineString([p1, p2]);
            distance = turf.pointToLineDistance(pt, line, { units: 'miles' });
            if (edit.distance == -1 || (edit.distance !== -1 && edit.distance > distance)) {
                edit.distance = distance;
                edit.count = i;
            }
        }
        path.splice(edit.count + 1, 0, { lat: lat, lng: lng });
        console.log(path);
        mapdata.clearAllDraw();
        model3D.editMode3Ddraw(path, "polygon");
        //mapdata.GLOBAL.listArea.polygonArea.setPaths([path]);
    },
    checkBuildingInPolygon: function (building) {
        let list = []; //building.getCoordinates();
        list = building.getCoverCoordinates();//getCoverCoordinates();
        if (list == null || list.length <= 0) {
            building.getBoundsCoordinates((a) => { list = a; });
        }
        let check = false;
        $.each(list, function (i, obj) {
            if (!model3D.CheckPointInPolygonOrMulti(obj.lat, obj.lng)) {
                check = true;
                return false;
            }
        });
        return check;
    },
    getCoordinates: function (geojson) {
        var coordinatehtml = "";
        let coordinates = geojson.coordinates;
        var dem = 0;
        switch (geojson.type.toLowerCase()) {
            case "polygon":
                for (var i = 0; i < coordinates.length; i++) {
                    if (coordinates.length > 1) {
                        coordinatehtml += "<tr><th>polygon " + (i + 1) + "</th></tr>"
                    }
                    for (var j = 0; j < coordinates[i].length; j++) {
                        coordinatehtml += ` <tr>
                                                <th>${j + 1}</th>
                                                <td><input class="input-location-table lng-location" data-index="${j}" value="${coordinates[i][j][0]}" readonly/></td>
                                                <td><input class="input-location-table lat-location" data-index="${j}" value="${coordinates[i][j][1]}" readonly/></td>
                                            </tr>`;
                    }
                }
                break;
            case "multipolygon":
                for (var i = 0; i < coordinates.length; i++) {
                    for (var k = 0; k < coordinates[i].length; k++) {
                        for (var j = 0; j < coordinates[i][k].length; j++) {
                            coordinatehtml += ` <tr>
                                            <th>${dem + 1}</th>
                                            <td><input class="input-location-table lng-location" data-group="${i}" data-group-child="${k}" readonly data-index="${j}" value="${coordinates[i][k][j][0]}"/></td>
                                            <td><input class="input-location-table lat-location" data-group="${i}" data-group-child="${k}" readonly data-index="${j}" value="${coordinates[i][k][j][1]}"/></td>
                                        </tr>`;
                            dem++;
                        }
                    }
                }
                break;
            case "point":
                coordinatehtml += ` <tr>
                                        <th>1</th>
                                        <td><input class="input-location-table lng-location" data-index="0" value="${coordinates[0]}" readonly/></td>
                                        <td><input class="input-location-table lat-location" data-index="0" value="${coordinates[1]}" readonly/></td>
                                    </tr>`;
                break;
            case "multipoint":
            case "linestring":
            case "polyline":
                for (var j = 0; j < coordinates.length; j++) {
                    coordinatehtml += ` <tr>
                                            <th>${j + 1}</th>
                                            <td><input class="input-location-table lng-location" data-index="${j}" value="${coordinates[j][0]}" readonly/></td>
                                            <td><input class="input-location-table lat-location" data-index="${j}" value="${coordinates[j][1]}" readonly/></td>
                                        </tr>`;
                }
                break;
            case "multipolyline":
            case "multilinestring":
                for (var i = 0; i < coordinates.length; i++) {
                    for (var j = 0; j < coordinates[i].length; j++) {
                        coordinatehtml += ` <tr>
                                            <th>${dem + 1}</th>
                                            <td><input class="input-location-table lng-location" data-group="${i}" data-index="${j}" value="${coordinates[i][j][0]}" readonly/></td>
                                            <td><input class="input-location-table lat-location" data-group="${i}" data-index="${j}" value="${coordinates[i][j][1]}" readonly/></td>
                                        </tr>`;
                        dem++;
                    }
                }
                break;
        }
        return coordinatehtml;
    },
    getLatLngConvert: function (num) {
        var a = num * 1000000;
        var a1 = Math.floor(num * 1000000);
        if ((a - a1) > 0.3) {
            if ((a - a1) >= 0.999) return Math.round(a) / 1000000;
            else return Math.floor(num * 1000000) / 1000000;
        } else
            return Math.floor(num * 1000000) / 1000000;
    },
    resetMode3DAfterSave: function (object3D, id) {
        if (model3D.GLOBAL.building != null) {
            model3D.GLOBAL.building.setMap(null);
            model3D.GLOBAL.building = null;
        }
        model3D.resetDataObject3DAfterSave(object3D, id);
        let object3dTemp = model3D.newMap4dBuilding(object3D);
        model3D.GLOBAL.building = object3dTemp;
        model3D.GLOBAL.building.setMap(map);
        model3D.GLOBAL.building.setSelected(true);
        model3D.clearOptionMode3D();
        model3D.GLOBAL.idObject2D = id;
    },
    getConvertCoordinate: function (geojson) {
        let coordinates = JSON.parse(JSON.stringify(geojson.coordinates));
        switch (geojson.type.toLowerCase()) {
            case "polygon":
                for (var i = 0; i < coordinates.length; i++) {
                    for (var j = 0; j < coordinates[i].length; j++) {
                        coordinates[i][j][0] = Math.floor(coordinates[i][j][0] * 1000000) / 1000000;
                        coordinates[i][j][1] = Math.floor(coordinates[i][j][1] * 1000000) / 1000000;
                    }
                }
                break;
            case "multipolygon":
                for (var i = 0; i < coordinates.length; i++) {
                    for (var k = 0; k < coordinates[i].length; k++) {
                        for (var j = 0; j < coordinates[i][k].length; j++) {
                            coordinates[i][k][j][0] = Math.floor(coordinates[i][k][j][0] * 1000000) / 1000000;
                            coordinates[i][k][j][1] = Math.floor(coordinates[i][k][j][1] * 1000000) / 1000000;
                            //coordinatehtml += ` <tr>
                            //                <th>${dem + 1}</th>
                            //                <td><input class="input-location-table lng-location" data-group="${i}" data-group-child="${k}" readonly data-index="${j}" value="${coordinates[i][k][j][0]}"/></td>
                            //                <td><input class="input-location-table lat-location" data-group="${i}" data-group-child="${k}" readonly data-index="${j}" value="${coordinates[i][k][j][1]}"/></td>
                            //            </tr>`;
                            //dem++;
                        }
                    }
                }
                break;
        }
        return coordinates;
    }
    //#endregion 
};