﻿var l = abp.localization.getResource('HTKT');
var UploadFileMultiObject = {
    GLOBAL: {
        arrayObjectId: [],
        urlUploadFile: '',
        fileArray: [],
    },
    CONSTS: {

    },
    SELECTORS: {
        modal: '#modalUploadMulti',
        btnShow: '.upload-File-Multi-Object',
        objectIdSelected: "#selected-object-id",
        listObject: "#dtc-list-selected",
        hasError: ".form-group.has-error",
        labelChooseFile: "#UrlFileMultiObject",
        //input//
        inputNameFile: "input[name='NameFileMultiObject']",
        inputLinkFile: "input[name='UrlFileMultiObject']",
        inputListDocument: "select[name='ListDocumentMultiObject']",
        btnSaveFile: ".btn-multi-file-save",
        modalUploadMulti: "#modalUploadMulti"
    },
    init: function () {
        UploadFileMultiObject.setEvent();
        UploadFileMultiObject.setSelect2();
    },
    setEvent: function () {
        // keyup name file
        $(UploadFileMultiObject.SELECTORS.inputNameFile).on('keyup change', function () {
            if ($(this).val() != "") {
                $(this).parent().find('.lable-error').remove();
            }
        })

        // remove object da chon
        $(UploadFileMultiObject.SELECTORS.listObject).on('click', '.remove', function () {
            var id = $(this).attr('data-id');
            UploadFileMultiObject.GLOBAL.arrayObjectId = jQuery.grep(UploadFileMultiObject.GLOBAL.arrayObjectId, function (value) {
                return value != id;
            });

            $(UploadFileMultiObject.SELECTORS.arrayObjectId).val(JSON.stringify(UploadFileMultiObject.GLOBAL.arrayObjectId));

            $(this).parent().remove();

            UploadFileMultiObject.CustomSearchSelected();

            if (UploadFileMultiObject.GLOBAL.arrayObjectId.length == 0) {
                $(UploadFileMultiObject.SELECTORS.arrayObjectId).val("");
            }
            else {
                $(UploadFileMultiObject.SELECTORS.arrayObjectId).val(JSON.stringify(UploadFileMultiObject.GLOBAL.arrayObjectId));
            }

            $(UploadFileMultiObject.SELECTORS.objectIdSelected).val(JSON.stringify(UploadFileMultiObject.GLOBAL.arrayObjectId));
        });

        //luu file
        $(UploadFileMultiObject.SELECTORS.modalUploadMulti).on("click", UploadFileMultiObject.SELECTORS.btnSaveFile, function () {
            var elementForm = UploadFileMultiObject.SELECTORS.modalUploadMulti;

            if (UploadFileMultiObject.checkFile(elementForm)) {
                var file = UploadFileMultiObject.GLOBAL.fileArray;         //$(`${elementForm} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`)[0].files;
                UploadFileMultiObject.SendFileDocument(file[0]);
                if (UploadFileMultiObject.GLOBAL.urlUploadFile != "") {
                    var array = [];
                    $.each(UploadFileMultiObject.GLOBAL.arrayObjectId, function (i, obj) {
                        var item = {
                            id: mapdata.uuidv4(),
                            idMainObject: obj,
                            nameDocument: $(`${elementForm} ${UploadFileMultiObject.SELECTORS.inputNameFile}`).val().trim(),
                            iconDocument: '',
                            urlDocument: UploadFileMultiObject.GLOBAL.urlUploadFile,
                            nameFolder: $(`${elementForm} ${UploadFileMultiObject.SELECTORS.inputListDocument}`).val().trim(),
                            orderDocument: 0
                        };
                        array.push(item);
                    });
                    $('.spinner').css('display', 'block');
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        contentType: "application/json-patch+json",
                        async: false,
                        url: '/api/HTKT/DocumentObject/add-file-document',
                        data: JSON.stringify(array),
                        success: function (data) {
                            if (data.code == "ok") {
                                abp.notify.success(l('ManagementLayer:AddFileSuccessfully'));

                                mapdata.CancelAction();
                            } else {
                                abp.notify.error(l('ManagementLayer:AddFileFailed'));
                            }
                            $('.spinner').css('display', 'none');
                            $(UploadFileMultiObject.SELECTORS.modal).modal('hide');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR + ":" + errorThrown);
                            ViewMap.showLoading(false);
                        }
                    });
                }
            } else {
                //console.log('Not ok');
            }
        });

        $(document).on('hidden.bs.modal', UploadFileMultiObject.SELECTORS.modal, function () {
            UploadFileMultiObject.setEmptyInput();
        });

        $(UploadFileMultiObject.SELECTORS.modalUploadMulti).on("change", UploadFileMultiObject.SELECTORS.labelChooseFile, function () {
            $(".form-button-upload").find(".lable-error").remove();
            $(".form-button-upload").removeClass("has-error");
            $(this).parent('.form-group-modal').removeClass("has-error");
            $(".form-file-toggle").show();
            if ($(UploadFileMultiObject.SELECTORS.inputLinkFile)[0].files.length) {
                UploadFileMultiObject.GLOBAL.fileArray = $(UploadFileMultiObject.SELECTORS.inputLinkFile)[0].files;
                $("#upload-file-name").html($(UploadFileMultiObject.SELECTORS.inputLinkFile)[0].files[0].name);
            }
        });

        $(UploadFileMultiObject.SELECTORS.modalUploadMulti).on("click", "#remove-file-upload", function () {
            $(UploadFileMultiObject.SELECTORS.inputLinkFile).val(""); //set file rỗng
            UploadFileMultiObject.GLOBAL.fileArray = [];
            //$("#upload-file-name").html(""); //remove file name
            $(".form-file-toggle").hide(); //ẩn form file
        });
    },

    //file
    checkFile: function (modalForm) {
        UploadFileMultiObject.clearLabelError();
        let check = true;
        let inputNameFile = $(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputNameFile}`).val();
        if (!validateText(inputNameFile, "text", 0, 0)) {
            insertError($(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputNameFile}`), "other");
            check = false;
            UploadFileMultiObject.showLabelError(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputNameFile}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:DocumentName")));
        }
        let inputLinkFile = $(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputListDocument}`).val();
        if (!validateText(inputLinkFile, "text", 0, 0)) {
            insertError($(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputListDocument}`), "other");
            check = false;
            UploadFileMultiObject.showLabelError(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputListDocument}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:Folder")));
        }

        if ($(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`)[0] == undefined) {
            insertError($(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`), "other");
            check = false;
            UploadFileMultiObject.showLabelError(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:Document")));
        } else {
            let inputDocumentFile = UploadFileMultiObject.GLOBAL.fileArray.length;           //$(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`)[0].files.length
            if (inputDocumentFile == 0) {
                insertError($(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`), "other");
                check = false;
                UploadFileMultiObject.showLabelError(`${modalForm} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:Document")));
            }
        }

        if (UploadFileMultiObject.GLOBAL.arrayObjectId.length == 0) {
            insertError($(`${modalForm} ${UploadFileMultiObject.SELECTORS.objectIdSelected}`), "other");
            check = false;
            UploadFileMultiObject.showLabelError(`${modalForm} ${UploadFileMultiObject.SELECTORS.objectIdSelected}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:ListObject")));
        }
        return check;
    },
    SendFileDocument: function (file) {
        var formData = new FormData();
        formData.append('file', file);
        modalCreateFolderLayer.GLOBAL.urlUploadFile = '';
        $.ajax({
            url: "/api/htkt/file/document",
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            async: false,
            success: function (data) {
                if (data.code == "ok") {
                    var re = data.result;
                    UploadFileMultiObject.GLOBAL.urlUploadFile = re.url;
                }
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });

    },
    setEmptyInput: function () {
        $(`${UploadFileMultiObject.SELECTORS.modal} ${UploadFileMultiObject.SELECTORS.inputNameFile}`).val("");
        $(`${UploadFileMultiObject.SELECTORS.modal} ${UploadFileMultiObject.SELECTORS.inputIdFile}`).val("");
        $(`${UploadFileMultiObject.SELECTORS.modal} ${UploadFileMultiObject.SELECTORS.inputListDocument}`).val(
            $(`${UploadFileMultiObject.SELECTORS.modal} ${UploadFileMultiObject.SELECTORS.inputListDocument} option:first`).val());
        const dt = new DataTransfer();
        $(`${UploadFileMultiObject.SELECTORS.modal} ${UploadFileMultiObject.SELECTORS.inputLinkFile}`)[0].files = dt.files;
        $(`${UploadFileMultiObject.SELECTORS.modal} ${UploadFileMultiObject.SELECTORS.inputListDocument}`).removeAttr("disabled");
        UploadFileMultiObject.clearLabelError();
        $.each(UploadFileMultiObject.GLOBAL.arrayObjectId, function (i, obj) {
            $('#tru_selected_' + obj + ' span').trigger('click');
        });
        UploadFileMultiObject.GLOBAL.arrayObjectId = [];

        $(".form-file-toggle").hide();
    },
    showLabelError: function (element, text) {
        //$(element).parents(".form-group").children("lable").remove();
        $(element).parents(".form-group-modal").append("<lable class='lable-error' style='color:red'>" + text + "</lable>");
    },
    showLabelErrorFile: function (element, text) {
        //$(element).parents(".form-group").children("lable").remove();
        $(element).parents(".form-button-upload").append("<lable class='lable-error' style='color:red'>" + text + "</lable>");
    },
    clearLabelError: function () {
        $(".lable-error").remove();
        $(UploadFileMultiObject.SELECTORS.hasError).find(".lable-error").remove();
        $(UploadFileMultiObject.SELECTORS.hasError).removeClass("has-error");
    },
    setSelect2: function () {
        let branch_all = [];

        function formatResult(state) {
            if (!state.id) {
                var btn = $('<div class="text-right"></div>');
                return btn;
            }
            console.log(state)
            var isSelected = UploadFileMultiObject.GLOBAL.arrayObjectId.indexOf(state.id);
            if (state.id == "") {
                return "";
            }
            branch_all.push(state.id);
            var id = 'state' + state.id;
            var checkbox = $('<div class="checkbox"><input id="' + id + '" class="check-tru" type="checkbox" ' + (isSelected > -1 ? 'checked' : '') + '><label for="checkbox1">' + state.text + '</label></div>', { id: id });
            var check = $('#' + id);
            if (check.length == 0) {
                return checkbox;
            }
        }

        function formatState(state) {

            setTimeout(function () {
                UploadFileMultiObject.CustomSearchSelected();
            }, 10)
            return '';
        }
        let optionSelect2 = {
            templateResult: formatResult,
            templateSelection: formatState,
            closeOnSelect: false,
            width: '100%',
            ajax: {
                url: mapdata.CONSTS.URL_AJAXMENULEFT,
                async: true,
                data: function (params) {
                    var query = {
                        id: mapdata.GLOBAL.idDirectory,
                        skipnumber: params.page ? (params.page - 1) * 10 : 0,
                        countnumber: 10,
                        search: params.term
                    };

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    if (data.code == "ok" || data.result.length != 0) {
                        let results = data.result.map((item, index) => {
                            return {
                                id: item.id,
                                text: `${item.nameObject}`,
                            }
                        });

                        var more = true;

                        if (data.result.length == 0) {
                            more = false;
                        }
                        //if (params.page != undefined) {
                        //    more = ((params.page * 10) < data.result.length ? true : false);
                        //}

                        return {
                            results: results,
                            pagination: {
                                more: more
                            }
                        };
                    }
                },
                cache: true
            }
        };

        let $select2 = $(UploadFileMultiObject.SELECTORS.objectIdSelected).select2(optionSelect2);

        UploadFileMultiObject.CustomSearchSelected();

        $select2.on("select2:select", function (event) {
            UploadFileMultiObject.AddElementListTru(event.params.data.id, event.params.data.text);
            UploadFileMultiObject.GLOBAL.arrayObjectId.push(event.params.data.id);

            UploadFileMultiObject.CustomSearchSelected();

            if (UploadFileMultiObject.GLOBAL.arrayObjectId.length == 0) {
                $(UploadFileMultiObject.SELECTORS.arrayObjectId).val("");
            }
            else {
                $(UploadFileMultiObject.SELECTORS.arrayObjectId).val(JSON.stringify(UploadFileMultiObject.GLOBAL.arrayObjectId));
                $(`${UploadFileMultiObject.SELECTORS.modalUploadMulti} ${UploadFileMultiObject.SELECTORS.objectIdSelected}`).parent(".form-group-modal").removeClass('has-error');
                $(`${UploadFileMultiObject.SELECTORS.modalUploadMulti} ${UploadFileMultiObject.SELECTORS.objectIdSelected}`).parent(".form-group-modal").find('.lable-error').remove();;
            }

            $("#state" + event.params.data.id).prop('checked', true);
        });

        $select2.on("select2:unselecting", function (event) {
            var $pr = $('#' + event.params.args.data._resultId).parent();

            UploadFileMultiObject.GLOBAL.arrayObjectId = jQuery.grep(UploadFileMultiObject.GLOBAL.arrayObjectId, function (value) {
                return value != event.params.args.data.id;
            });

            // xoa recored list
            UploadFileMultiObject.RemoveElementListTru(event.params.args.data.id);

            if (UploadFileMultiObject.GLOBAL.arrayObjectId.length == 0) {
                $(UploadFileMultiObject.SELECTORS.arrayObjectId).val("");
            }
            else {
                $(UploadFileMultiObject.SELECTORS.arrayObjectId).val(JSON.stringify(UploadFileMultiObject.GLOBAL.arrayObjectId));
            }

            scrollTop = $pr.prop('scrollTop');

            $("#state" + event.params.args.data.id).prop('checked', false);

        });

        $select2.on("select2:unselect", function (event) {
            UploadFileMultiObject.CustomSearchSelected();
        });

        $(document).on('change', '.check-tru', function () {
            if ($(this).prop('checked')) {
                $(this).prop("checked", false);
            } else {
                $(this).prop("checked", true);
            }

            UploadFileMultiObject.CustomSearchSelected();
        });


        // select folder
        $(UploadFileMultiObject.SELECTORS.inputListDocument).select2()
            .on("select2:opening", function () {
                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
            })
            .on("select2:closing", function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                }
                else {
                    $(this).parent().find('.placeholder').attr('style', "");
                }
            })
            .on('select2:select', function () {
                var value = $(this).val();
                if (value != "" && value != null) {
                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                    $(this).trigger('change');
                }
            });

        $(UploadFileMultiObject.SELECTORS.inputListDocument).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
    },
    CustomSearchSelected: function () {
        $('.select2-selection--multiple .select2-selection__rendered li:not(.select2-search--inline)').remove();
        $('.counter').remove();
        var counter = UploadFileMultiObject.GLOBAL.arrayObjectId.length;
        //$('.select2-selection--multiple .select2-selection__rendered').after('<div style="border-top: 1px solid #ccc; line-height: 28px; padding: 5px;" class="counter">' + l('ManagementLayer:NumberObjectSelected') + ' ' + counter + '</div>');
        $('#objectCount').html(counter);
    },
    AddElementListTru: function (id, text) {
        var html = `   <div id="tru_selected_${id}" class="dtc-item-selected" data-id="${id}">
                                            <label class="name">${text}</label>
                                            <span data-id="${id}" class="remove">x</span>
                                        </div>`;

        $(UploadFileMultiObject.SELECTORS.listObject).append(html);
    },
    RemoveElementListTru: function (id) {
        $(".dtc-item-selected").each((index, element) => {
            var idtru = $(element).attr("data-id");
            if (idtru == id) {
                $(element).remove();
                return;
            }
        });
    },
    setSelecedFile: function () {
        id = mapdata.GLOBAL.idDirectory;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: true,
            url: mapdata.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=' + id,
            data: {
            },
            success: function (data) {
                if (data.code == "ok" && data.result.listProperties != undefined) {
                    var lstFile = data.result.listProperties.filter(x => x.typeProperties == "file");
                    var option = '';
                    $(UploadFileMultiObject.SELECTORS.inputListDocument).html(option);
                    for (var i = 0; i < lstFile.length; i++) {
                        option += `<option value="${lstFile[i].codeProperties}">${lstFile[i].nameProperties}</option>`;
                    }

                    $(UploadFileMultiObject.SELECTORS.inputListDocument).html(option);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    CloseUploadMultiple: function () {

    }
};