﻿var l = abp.localization.getResource('HTKT');
var Activity = {
    GLOBAL: {
        ArrayActivityDetail: null,
        ArrayDirectoryActivity: [],
        ArrayProperties: [],
        IdActivityDetailFocus: "",
        IdActivityDirectoryInfo: "",
        checkModalActivity: false,
        listObjectFile: []
    },
    CONSTS: {
        URL_DANH_SACH_HOAT_DONG: "/api/htkt/detailActivity/get-list-detail-activity",
        URL_DANH_SACH_LOAI_HOAT_DONG: "/api/htkt/directoryactivity/get-list-new",
        URL_THEM_MOI_HOAT_DONG: '/api/htkt/detailActivity/create-activity-detail',
        URL_CAP_NHAT_HOAT_DONG: '/api/HTKT/detailActivity/update-activity-detail',
        URL_XOA_HOAT_DONG: '/api/HTKT/detailActivity/delete-activity-detail',
        URL_IMPORT_HOAT_DONG: '/api/htkt/detailActivity/importdata',
        URL_EXPORT_HOAT_DONG: '/api/htkt/detailActivity/exportdata'
    },
    SELECTORS: {
        table: ".table-activity",
        btn_activity_info: "#btn-activity-info",
        modal: "#myModalActivity",
        content_modal: ".div-hoat-dong-body",

        selected_loai_hoat_dong: "#seleted-loai-hoat-dong",
        id_loai_hoat_dong: "#id-loai-hoat-dong",
        body_thong_tin: "#body-thong-tin-hoat-dong",
        body_mo_rong: "#body-mo-rong-hoat-dong",
        btn_create_hoat_dong: "#btn-create-activity-detail",
        btn_save: "#btn-save-activity-detail",

        input_activity: '.property-activity-input',
        input_name_activity: "text-modal-NameDirectoryActivity",
        input_date_activity: "text-modal-DateActivity",

        //***infor**//
        Modal_Infor: "#myModalInforActivityDetail",
        btn_them_moi_detail_activity_info: ".them-moi-detail-activity-info",
        btn_cap_nhat_detail_activity_info: ".cap-nhat-detail-activity-info",
        btn_xoa_detail_activity_info: ".xoa-detail-activity-info",
        btn_import_detail_activity_info: ".import-detail-activity-info",
        btn_export_detail_acitivity: ".export-detail-activity-info",
        input_import_file_detail_activity_info: "#import-file",

        btnEditFile: ".btn-edit-file-activity",
        btnDeleteFile: ".btn-deleteTable-file-activity",
    },
    init: function () {
        this.setEvent();
        //this.GetDanhSachHoatDong();
    },
    setEvent: function () {
        // chi tiết hoạt động
        $(Activity.SELECTORS.btn_activity_info).on('click', function () {
            if (Activity.GLOBAL.ArrayActivityDetail.length > 0) {
                $(Activity.SELECTORS.Modal_Infor).modal('show');
                Activity.AddTableDanhSachHoatDong();
            }
        });

        // chuyển tab
        $(Activity.SELECTORS.modal).on('click', '.li-tab', function () {
            var id = $(this).attr('data-id');
            $(Activity.SELECTORS.content_modal).addClass('hidden');
            $(`#${id}`).removeClass('hidden');

        })

        ////*********_thêm_mới_***************////
        //thêm mới hoạt động
        $(Activity.SELECTORS.btn_create_hoat_dong).on('click', function () {
            parentModel = $(Activity.SELECTORS.modal); // chuyển modal
            $(Activity.SELECTORS.modal).modal('show');
            $(Activity.SELECTORS.btn_save).prop('disabled', true);
            if (Activity.GLOBAL.IdActivityDirectoryInfo == "") {
                $(Activity.SELECTORS.selected_loai_hoat_dong).prop('disabled', false);
            }

            $(Activity.SELECTORS.content_modal).addClass('hidden'); // ẩn nội dung

            $(`${Activity.SELECTORS.modal} .modal-body ul`).addClass('hidden'); // ẩn nội dung
            $(`${Activity.SELECTORS.modal} .li-tab`).removeClass('active'); // reset tab
            Activity.SelectdDanhSachLoaiHoatDong();
            Activity.GLOBAL.checkModalActivity = true;
        });

        // thay đổi loại hoạt động
        $(Activity.SELECTORS.selected_loai_hoat_dong).on('select2:select', function () {
            var id = $(this).val();
            if (id != "") {
                $(Activity.SELECTORS.id_loai_hoat_dong).val(id);

                $(Activity.SELECTORS.btn_save).prop('disabled', false);
                $(`${Activity.SELECTORS.modal} .modal-body ul`).removeClass('hidden'); // hiển thị tab content 
            }
            else {
                $(Activity.SELECTORS.btn_save).prop('disabled', true);
                $(`${Activity.SELECTORS.modal} .modal-body ul`).addClass('hidden'); // an tab content 
            }

            $(Activity.SELECTORS.body_thong_tin).html("");

            var directoryActivity = Activity.GLOBAL.ArrayDirectoryActivity.find(x => x.id == id);
            if (directoryActivity != undefined) {
                Activity.GLOBAL.IdActivityDirectoryInfo = id;
                var result = [];
                $(`${Activity.SELECTORS.modal} .modal-body .nav-tabs .nav-item`).children().removeClass('active');
                $(`${Activity.SELECTORS.modal} .modal-body .nav-tabs .nav-item`).eq(0).children().addClass('active');
                $(Activity.SELECTORS.body_thong_tin).removeClass('hidden'); // hiện thị nội dung nhập thông tin
                $(Activity.SELECTORS.body_mo_rong).addClass('hidden');
                if (directoryActivity.listProperties == null) {
                    directoryActivity.listProperties = [];
                }
                var checkExits = directoryActivity.listProperties.filter(x => x.typeProperties == "image" || x.typeProperties == "link" || x.typeProperties == "file");

                if (checkExits.length == 0) {
                    $(`${Activity.SELECTORS.modal} .modal-body .nav-item`).eq(1).addClass('hidden');
                }
                else {
                    $(`${Activity.SELECTORS.modal} .modal-body .nav-item`).eq(1).removeClass('hidden');
                }

                var lstProperties = directoryActivity.listProperties;
                var activityDetailCurrent = null;
                if (Activity.GLOBAL.IdActivityDetailFocus != "") // kiểm tra nếu là trạng thái edit thì gán value
                {
                    activityDetailCurrent = Activity.GLOBAL.ArrayActivityDetail.find(x => x.id == Activity.GLOBAL.IdActivityDetailFocus);
                }

                var textDefault = `<div class="row">
                                        <label class="control-label col-md-3 col-xs-12 required">${l("ManagementLayer:OperationName")}</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="row form-group-modal">
                                                <input type="text" data-type="string" data-name="${l("ManagementLayer:OperationName")}" data-required= "${true}" id="${Activity.SELECTORS.input_name_activity}" value="${activityDetailCurrent != null ? activityDetailCurrent.nameDirectoryActivity : ""}" class="form-control property-activity-input form-style" placeholder="" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label class="control-label col-md-3 col-xs-12 required">${l("ManagementLayer:ActivityDate")}</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="row form-group-modal">
                                                <input type="text" data-type="date" data-name="${l("ManagementLayer:ActivityDate")}" data-required="${true}" id="${Activity.SELECTORS.input_date_activity}" value="${activityDetailCurrent != null ? moment(activityDetailCurrent.dateActivity).format('DD/MM/YYYY') : ""}" class="form-control property-activity-input form-style" placeholder="" />
                                            </div>
                                        </div>
                                    </div>`;
                $(Activity.SELECTORS.body_thong_tin).append(textDefault);

                $(`${Activity.SELECTORS.modal} #text-modal-DateActivity`).datetimepicker({
                    locale: 'vi',
                    format: 'DD/MM/YYYY',
                    useCurrent: false,
                    defaultDate: moment(),
                }).on('dp.change', function (e) {
                    $(e.currentTarget).parents(".form-group-modal").find(".label-error").remove();
                    $(e.currentTarget).parents(".form-group-modal").removeClass("has-error");
                });

                let statusimage = false;
                $(modalCreateFolderLayer.SELECTORS.inputListDocument).html('');
                //if (activityDetailCurrent != null) {
                //    lstProperties = activityDetailCurrent.listProperties;
                //}
                $(`${Activity.SELECTORS.modal} .modal-body .class-extend`).hide();
                $(`${Activity.SELECTORS.modal} ${Activity.SELECTORS.body_mo_rong} .item-FilePlace`).hide();
                $.each(lstProperties, function (i, obj) {
                    if (obj.typeSystem !== 1) {
                        if (activityDetailCurrent != null) {
                            var currentProperties = activityDetailCurrent.listProperties.find(x => x.codeProperties == obj.codeProperties);
                            if (typeof currentProperties !== "undefined" && currentProperties !== null) {
                                // nếu thuộc tính là dạng danh sách
                                if (currentProperties.typeProperties == "checkbox" || currentProperties.typeProperties == "list" || currentProperties.typeProperties == "radiobutton") {
                                    var valueCurrent = ""; // dùng để get value so sánh với json danh sách
                                    try {
                                        var PropertiCurent = JSON.parse(currentProperties.defalutValue); // chuyển json string về type list
                                        if (PropertiCurent != undefined && $.isArray(PropertiCurent)) // kiểm tra null và biến chuyển về có phải là array hay ko? nếu đúng thì kiểm tra value 
                                        {
                                            if (currentProperties.typeProperties == "checkbox") {
                                                var valuePropertiCurentCB = PropertiCurent.filter(x => x.checked == true); // lay value hien tai
                                                if (valuePropertiCurentCB != undefined && valuePropertiCurentCB.length > 0) {
                                                    valueCurrent = valuePropertiCurentCB.map(x => x.code);
                                                }
                                            }
                                            else {
                                                var valuePropertiCurent = PropertiCurent.find(x => x.checked == true); // lay value hien tai
                                                if (valuePropertiCurent != undefined) {
                                                    valueCurrent = valuePropertiCurent.code;
                                                }
                                            }
                                        }
                                        else {
                                            valueCurrent = currentProperties.defalutValue; // gắn trực tiếp value
                                        }
                                    }
                                    catch (e) {
                                        valueCurrent = currentProperties.defalutValue; // gắn trực tiếp value
                                    }

                                    if (valueCurrent != "") {
                                        // set value vao danh sach hien thi
                                        var lstItem = JSON.parse(obj.defalutValue); // danh sách phần tử của thuộc tính của loại hoạt động
                                        for (var k = 0; k < lstItem.length; k++) {
                                            if (currentProperties.typeProperties != "checkbox") // nếu không phải là checkbox thì kiểm tra value đã checked
                                            {
                                                lstItem[k].checked = false;
                                                if (lstItem[k].code == valueCurrent) {
                                                    lstItem[k].checked = true;
                                                }
                                            }
                                            else // nếu là checkbox thì cắt chuỗi và kiểm tra value đã checked
                                            {
                                                var lstValuecheckbox = valueCurrent.split(",");

                                                $.each(lstValuecheckbox, function (p, item) {
                                                    if (lstItem[k].code == item) {
                                                        lstItem[k].checked = true;
                                                    }
                                                })
                                            }
                                        }

                                        obj.defalutValue = JSON.stringify(lstItem); // chuyển về định dạng json để gen ra element trên màn hình
                                    }
                                }
                                else {
                                    obj.defalutValue = currentProperties.defalutValue;
                                }
                            }
                        }

                        var element = {
                            NameProperties: obj.nameProperties,
                            CodeProperties: obj.codeProperties,
                            TypeProperties: obj.typeProperties,
                            IsShow: obj.isShow,
                            IsIndexing: obj.isIndexing,
                            IsRequest: obj.isRequest,
                            IsView: obj.isView,
                            IsHistory: obj.isHistory,
                            IsAsync: obj.isAsync,
                            IsShowExploit: obj.isShowExploit,
                            DefalutValue: obj.defalutValue,
                            ShortDescription: obj.shortDescription,
                            TypeSystem: obj.typeSystem,
                            OrderProperties: obj.orderProperties
                        };

                        result.push(element);

                        if (element.TypeProperties == "image") {
                            if (element.DefalutValue != "") {
                                ModalInput.GLOBAL.listObjectImage = JSON.parse(element.DefalutValue);
                            }
                            $(`${Activity.SELECTORS.modal} .modal-body .class-extend`).show();
                            $(`${Activity.SELECTORS.modal} ${Activity.SELECTORS.body_mo_rong} .item-FilePlace`).eq(0).show();
                        }
                        if (element.TypeProperties == "link") {
                            if (element.DefalutValue != "") {
                                ModalInput.GLOBAL.listObjectLinkCamera = JSON.parse(element.DefalutValue);
                            }
                            $(`${Activity.SELECTORS.modal} .modal-body .class-extend`).show();
                            $(`${Activity.SELECTORS.modal} ${Activity.SELECTORS.body_mo_rong} .item-FilePlace`).eq(1).show();
                        }
                        if (element.TypeProperties == "file") {

                            if (Activity.GLOBAL.listObjectFile == null) {
                                Activity.GLOBAL.listObjectFile = [];
                            }

                            for (var i = 0; i < Activity.GLOBAL.listObjectFile.length; i++) {
                                if (Activity.GLOBAL.listObjectFile[i].idDirectoryProperties == obj.codeProperties) {
                                    Activity.GLOBAL.listObjectFile[i].nameProperties = obj.nameProperties
                                }
                            }

                            $(`${Activity.SELECTORS.modal} .modal-body .class-extend`).show();
                            $(`${Activity.SELECTORS.modal} ${Activity.SELECTORS.body_mo_rong} .item-FilePlace`).eq(2).show();

                            let option = `<option value="${element.CodeProperties}">${element.NameProperties}</option>`;
                            $(modalCreateFolderLayer.SELECTORS.inputListDocument).append(option);
                        }

                        if (element.TypeProperties != "geojson" && element.TypeProperties != "image"
                            && element.TypeProperties != "link" && element.TypeProperties != "maptile") {
                            var text = Activity.selectInput(element);
                            $(Activity.SELECTORS.body_thong_tin).append(text);
                            if (element.TypeProperties == "date") {
                                $(`${Activity.SELECTORS.modal} #text-modal-` + element.CodeProperties).datetimepicker({
                                    locale: 'vi',
                                    format: 'DD/MM/YYYY',
                                    useCurrent: false,
                                    defaultDate: moment(),
                                }).on('dp.change', function (e) {
                                    $(e.currentTarget).parents(".form-group-modal").find(".label-error").remove();
                                    $(e.currentTarget).parents(".form-group-modal").removeClass("has-error");
                                });
                            } else if (element.TypeProperties == "text" || element.TypeProperties == "stringlarge") {
                                setTimeout(function () {
                                    $(`${Activity.SELECTORS.modal} #text-modal-` + element.CodeProperties).trumbowyg({
                                        svgPath: '/libs/bootstrap-trumbowyg/icons.svg',
                                        btns: [
                                            ['viewHTML'],
                                            ['undo', 'redo'], // Only supported in Blink browsers
                                            ['formatting'],
                                            ['strong', 'em', 'del'],
                                            ['superscript', 'subscript'],
                                            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                                            ['unorderedList', 'orderedList'],
                                            ['horizontalRule'],
                                            ['removeformat'],
                                            ['fullscreen']
                                        ]
                                    }).on('tbwchange ', function () {
                                        $(`${Activity.SELECTORS.modal} #text-modal-` + element.CodeProperties).trigger('change');
                                    });
                                }, 200);
                            }
                            else if (element.TypeProperties == "list") {
                                $(`${Activity.SELECTORS.modal} #text-modal-` + element.CodeProperties).select2();
                            }
                        }
                    }
                });

                Activity.GLOBAL.ArrayProperties = result;

                $(`${Activity.SELECTORS.modal} ${Activity.SELECTORS.body_mo_rong} .item-FilePlace`).each(function () {
                    if ($(this).css('display') != "none") {
                        $(this).click();
                        return false;
                    }
                })
            }
        });

        // lưu hoạt động
        $(Activity.SELECTORS.btn_save).on('click', function () {
            if (Activity.CheckValidateForm() && mapdata.GLOBAL.idDirectory.trim() != "" && Activity.checkValidFile()) {
                var object = {};
                object.id = Activity.GLOBAL.IdActivityDetailFocus;
                object.idDirectoryActivity = $(Activity.SELECTORS.id_loai_hoat_dong).val(); // id loại hoạt động
                object.idMainObject = mapdata.GLOBAL.idMainObject; // id mainobject
                object.idDirectory = mapdata.GLOBAL.idDirectory; // id lớp dữ liệu
                object.nameDirectoryActivity = $(`#${Activity.SELECTORS.input_name_activity}`).val(); // tên hoạt động
                object.dateActivity = $(`#${Activity.SELECTORS.input_date_activity}`).val(); // ngày hoạt động

                object.listProperties = []; // danh sách thuộc tính

                $.each(Activity.GLOBAL.ArrayProperties, function (i, obj) {
                    var objectparam = {
                        nameProperties: obj.NameProperties,
                        codeProperties: obj.CodeProperties,
                        typeProperties: obj.TypeProperties,
                        isShow: obj.IsShow,
                        isView: obj.IsView,
                        defalutValue: $(`${Activity.SELECTORS.modal} #text-modal-` + obj.CodeProperties).val(),
                        orderProperties: obj.OrderProperties,
                        typeSystem: obj.TypeSystem
                    };

                    if (obj.TypeProperties == "link") {
                        objectparam.defalutValue = JSON.stringify(ModalInput.GLOBAL.listObjectLinkCamera);
                    }
                    if (obj.TypeProperties == "image") {
                        objectparam.defalutValue = JSON.stringify(ModalInput.GLOBAL.listObjectImage);
                    }
                    object.listProperties.push(objectparam);
                    if (obj.CodeProperties.toLowerCase() === "name") {
                        object.nameObject = $(`${Activity.SELECTORS.modal} #text-modal-` + obj.CodeProperties).val();
                    }

                    if (obj.TypeProperties == "list") {
                        let array = null;
                        objectparam.defalutValue = $('#text-modal-' + obj.CodeProperties).val();
                        //let array = null;
                        //if (obj.DefalutValue != null && obj.DefalutValue != "") {
                        //    array = JSON.parse(obj.DefalutValue);
                        //    let value = $('#text-modal-' + obj.CodeProperties).val();
                        //    $.each(array, function (i, obj) {
                        //        if (obj.code == value) {
                        //            obj.checked = true;
                        //        } else {
                        //            obj.checked = false;
                        //        }
                        //    });
                        //}
                        //objectparam.defalutValue = JSON.stringify(array);
                    }
                    if (obj.TypeProperties == "checkbox") {
                        let arraychecked = $(".text-modal-" + obj.CodeProperties + ":checked").map(function () {
                            return this.value;
                        }).get();

                        objectparam.defalutValue = arraychecked.toString();

                        //let arraychecked = $(".text-modal-" + obj.CodeProperties + ":checked").map(function () {
                        //    return this.value;
                        //}).get();
                        //let array = null;
                        //if (obj.DefalutValue != null && obj.DefalutValue != "") {
                        //    array = JSON.parse(obj.DefalutValue);
                        //    $.each(array, function (i, obj) {
                        //        if (arraychecked.includes(obj.code)) {
                        //            obj.checked = true;
                        //        } else {
                        //            obj.checked = false;
                        //        }
                        //    });
                        //}
                        //objectparam.defalutValue = JSON.stringify(array);
                    }
                    if (obj.TypeProperties == "radiobutton") {
                        let stringchecked = $(".text-modal-" + obj.CodeProperties + ":checked").val();
                        objectparam.defalutValue = stringchecked;
                        //let stringchecked = $(".text-modal-" + obj.CodeProperties + ":checked").val();
                        //let array = null;
                        //if (obj.DefalutValue != null && obj.DefalutValue != "") {
                        //    array = JSON.parse(obj.DefalutValue);
                        //    $.each(array, function (i, obj) {
                        //        if (obj.code == stringchecked) {
                        //            obj.checked = true;
                        //        } else {
                        //            obj.checked = false;
                        //        }
                        //    });
                        //}
                        //objectparam.defalutValue = JSON.stringify(array);
                    }
                    if (obj.TypeProperties == "bool") {
                        let value = $(`#text-modal-${obj.CodeProperties}-activity`).prop("checked");
                        objectparam.defalutValue = "" + value;
                    }
                });

                let urlAjax = '';
                var message = "";
                if (object.id == "") {
                    urlAjax = Activity.CONSTS.URL_THEM_MOI_HOAT_DONG;
                    message = l("ManagementLayer:CreateActivitySuccesfully");
                } else {
                    urlAjax = Activity.CONSTS.URL_CAP_NHAT_HOAT_DONG;
                    message = l("ManagementLayer:UpdateActivitySuccesfully");
                }

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    //contentType: false,
                    //processData: false,
                    contentType: "application/json-patch+json",
                    async: false,
                    url: urlAjax,
                    data: JSON.stringify(object),
                    success: function (data) {
                        if (data.code == "ok")
                        {
                            abp.notify.success(message);
                            Activity.HideFormActivityDetail();
                            Activity.GetDanhSachHoatDong(data.result.idMainObject, data.result.idDirectory);

                            setTimeout(function () {
                                Activity.AddTableDanhSachHoatDong();
                            }, 500)
                            let array = [];
                            $.each(Activity.GLOBAL.listObjectFile, function (i, obj) {
                                let item = {
                                    id: obj.id,
                                    idMainObject: data.result.idMainObject,
                                    idActivity: data.result.id,
                                    nameDocument: obj.name,
                                    iconDocument: '',
                                    urlDocument: obj.url,
                                    nameFolder: obj.idDirectoryProperties,
                                    orderDocument: obj.order
                                };
                                array.push(item);
                            });

                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                //contentType: false,
                                //processData: false,
                                contentType: "application/json-patch+json",
                                async: false,
                                url: '/api/HTKT/DocumentActivity/add-file-document',
                                data: JSON.stringify(array),
                                success: function (data) {
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.error(jqXHR + ":" + errorThrown);
                                    ViewMap.showLoading(false);
                                }
                            });
                            //});
                        } else {
                            swal({
                                title: l("ManagementLayer:Notification"),
                                text: l("ManagementLayer:UpdateDataFailed{0}", data.message),
                                icon: "warning",
                                button: l("ManagementLayer:Close"),
                            }).then((value) => {
                                //location.href = "/HTKT/Layer";
                            });
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR + ":" + errorThrown);
                        ViewMap.showLoading(false);
                    }
                });

                ///

                Activity.GLOBAL.IdActivityDirectoryInfo = "";
                //mapdata.GLOBAL.statusdone = true;
                //mapdata.setOrRestBtnInsert(false);
                //$('#myModal2').modal("hide");
                //mapdata.GLOBAL.selectmenu = "";
            }
        });

        //*******end********////

        //*************info************//
        // thêm mới hoạt động theo loại hoạt động
        $(Activity.SELECTORS.Modal_Infor).on('click', Activity.SELECTORS.btn_them_moi_detail_activity_info, function () {
            var id = $(this).attr('data-id');

            Activity.GLOBAL.IdActivityDirectoryInfo = id;
            $(Activity.SELECTORS.id_loai_hoat_dong).val(id);

            $(Activity.SELECTORS.Modal_Infor).modal('hide');

            $(Activity.SELECTORS.selected_loai_hoat_dong).prop('disabled', true);

            $(Activity.SELECTORS.btn_create_hoat_dong).trigger('click');

            //setTimeout(function ()
            //{
            //    $(Activity.SELECTORS.selected_loai_hoat_dong).val(id).trigger('change');
            //},200)
        })

        $(Activity.SELECTORS.Modal_Infor).on('click', Activity.SELECTORS.btn_cap_nhat_detail_activity_info, function () {
            $(Activity.SELECTORS.selected_loai_hoat_dong).prop('disabled', true);
            var id = $(this).attr('data-id');
            var directoryActivityId = $(this).attr('data-directoryActivity');
            Activity.GetFileByActivityId(id);

            $(Activity.SELECTORS.id_loai_hoat_dong).val(id);

            Activity.GLOBAL.IdActivityDirectoryInfo = directoryActivityId;
            Activity.GLOBAL.IdActivityDetailFocus = id;

            $(Activity.SELECTORS.btn_create_hoat_dong).trigger('click');
            Activity.GLOBAL.checkModalActivity = true;
        });

        $(Activity.SELECTORS.Modal_Infor).on('click', Activity.SELECTORS.btn_xoa_detail_activity_info, function () {
            var id = $(this).attr('data-id');
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:AreYouDeleteActivity"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        contentType: "application/json-patch+json",
                        async: true,
                        url: Activity.CONSTS.URL_XOA_HOAT_DONG + "?id=" + id,
                        success: function (data) {
                            if (data.code == "ok") {
                                swal({
                                    title: l("ManagementLayer:Notification"),
                                    text: l("ManagementLayer:DeleteOperationSussessfully"),
                                    icon: "success",
                                    button: l("ManagementLayer:Close")
                                });

                                Activity.GetDanhSachHoatDong(mapdata.GLOBAL.idMainObject, mapdata.GLOBAL.idDirectory);
                                setTimeout(function () {
                                    Activity.AddTableDanhSachHoatDong();
                                }, 500)
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR + ":" + errorThrown);
                        }
                    });
                }
            })
        });

        $(Activity.SELECTORS.Modal_Infor).on('click', Activity.SELECTORS.btn_import_detail_activity_info, function () {
            var id = $(this).attr('data-id');
            $(Activity.SELECTORS.input_import_file_detail_activity_info).attr('data-id', id);
            $(Activity.SELECTORS.input_import_file_detail_activity_info).trigger("click");
        });

        $(Activity.SELECTORS.Modal_Infor).on('change', Activity.SELECTORS.input_import_file_detail_activity_info, function () {
            var id = $(this).attr('data-id');

            if (this.files.length > 0) {
                var file = this.files[0];
                var ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();

                if (ext == "xlsx" || ext == "xls" || ext == "csv") {
                    var fd = new FormData();
                    fd.append('Files', file);
                    fd.append('DirectoryActivityId', id);
                    fd.append('IdMainObject', mapdata.GLOBAL.idMainObject);
                    fd.append('IdDirectory', mapdata.GLOBAL.idDirectory);

                    $.ajax({
                        type: 'POST',
                        data: fd,
                        //contentType: 'application/json',
                        contentType: false,
                        processData: false,
                        async: true,
                        url: Activity.CONSTS.URL_IMPORT_HOAT_DONG,
                        success: function (data) {
                            if (data.code == "ok") {
                                swal({
                                    title: l("ManagementLayer:Notification"),
                                    text: data.message,
                                    icon: "success",
                                    button: l("ManagementLayer:Close")
                                });

                                Activity.GetDanhSachHoatDong(mapdata.GLOBAL.idMainObject, mapdata.GLOBAL.idDirectory);
                                setTimeout(function () {
                                    Activity.AddTableDanhSachHoatDong();
                                }, 500)

                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR + ":" + errorThrown);
                        }
                    });
                }
                else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:FileNotCorrrectFormat"),
                        icon: "warning",
                        button: l("ManagementLayer:Close")
                    });

                    $(this).val(null);
                }
            }

        });

        //*******end********////

        // export hoạt động
        $(Activity.SELECTORS.Modal_Infor).on('click', Activity.SELECTORS.btn_export_detail_acitivity, function () {
            var id = $(this).attr('data-id');

            $.ajax({
                type: "GET",
                dataType: 'json',
                contentType: "application/json-patch+json",
                async: true,
                url: Activity.CONSTS.URL_EXPORT_HOAT_DONG,
                data: {
                    idMainObject: mapdata.GLOBAL.idMainObject,
                    idDirectory: mapdata.GLOBAL.idDirectory,
                    idDirectoryActivity: id
                },
                success: function (data) {
                    if (data.code == "ok") {

                        var local = window.location.origin;

                        var url = `${local}/HoatDong/Danh_sach_hoat_dong.xlsx`;
                        Activity.Download(url, "Danh_sach_hoat_dong.xlsx");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR + ":" + errorThrown);
                }
            });
        })

        $(Activity.SELECTORS.Modal_Infor).on('shown.bs.modal', function () {
            $(Activity.SELECTORS.body_thong_tin).html('');
            //$(Activity.SELECTORS.body_mo_rong).html('');
            Activity.ResetTabExented();
        });

        $(Activity.SELECTORS.modal).on('hidden.bs.modal', function () {
            Activity.GLOBAL.IdActivityDirectoryInfo = "";
            Activity.GLOBAL.IdActivityDetailFocus = "";
            Activity.HideFormActivityDetail();
            Activity.GLOBAL.checkModalActivity = false;
        });

        //$(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.btnAddNewFile}`).on('click', function () {
        //    Activity.GLOBAL.checkModalActivity = true;
        //});

        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).on("click", modalCreateFolderLayer.SELECTORS.btnSaveFile, function () {
            var elementForm = modalCreateFolderLayer.SELECTORS.modalFileUploadInfor;

            if (Activity.GLOBAL.checkModalActivity && modalCreateFolderLayer.checkFile(elementForm)) {
                $('.label-error').remove();
                let thutu = Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim());
                let iddirectoryproperties = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim();
                if (Activity.GLOBAL.listObjectFile == null) {
                    Activity.GLOBAL.listObjectFile = [];
                }

                var checkthutu = Activity.GLOBAL.listObjectFile.find(x => x.order == thutu && x.idDirectoryProperties == iddirectoryproperties);

                let id = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputIdFile}`).val();
                if (typeof id !== "undefined" && id.length > 0 && ((typeof checkthutu !== "undefined" && checkthutu.id == id) || typeof checkthutu == "undefined")) {
                    let status = false;
                    if ($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files.length > 0) {
                        let file = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files;
                        modalCreateFolderLayer.SendFileDocument(file[0]);
                        status = true;
                    }
                    var objTemp = Activity.GLOBAL.listObjectFile.find(x => x.id == id);
                    //modalCreateFolderLayer.DeleteFileDocument(objTemp.url);
                    objTemp.name = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val().trim();
                    objTemp.idDirectoryProperties = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim();
                    objTemp.order = Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim());
                    if (status) {
                        modalCreateFolderLayer.DeleteFileDocument(objTemp.url);
                        objTemp.url = modalCreateFolderLayer.GLOBAL.urlUploadFile;
                    }
                    ModalInput.updateTable("FileUpload");
                    $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("hide");
                } else if ((typeof id == "undefined" || id.length <= 0) && typeof checkthutu == "undefined") {
                    var count = modalCreateFolderLayer.GLOBAL.listObjectFile.length + 1;
                    var checkOrder = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim();
                    let file = modalCreateFolderLayer.GLOBAL.fileArray;                      // $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files;
                    modalCreateFolderLayer.SendFileDocument(file[0]);

                    if (modalCreateFolderLayer.GLOBAL.urlUploadFile != "") {
                        var item = {
                            //stt: count + 1,
                            id: mapdata.uuidv4(),
                            name: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val().trim(),
                            idDirectoryProperties: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim(),
                            url: modalCreateFolderLayer.GLOBAL.urlUploadFile,
                            order: Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim()),
                        };

                        var directoryActivity = Activity.GLOBAL.ArrayDirectoryActivity.find(x => x.id == Activity.GLOBAL.IdActivityDirectoryInfo);

                        if (directoryActivity != undefined) {
                            var checkExitsNameFile = directoryActivity.listProperties.find(x => x.typeSystem != 1 && x.codeProperties == item.idDirectoryProperties);
                            if (checkExitsNameFile != undefined) {
                                item.nameProperties = checkExitsNameFile.nameProperties;
                            }
                        }
                        //var checkExitsNameFile = mapdata.GLOBAL.lstPropertiesDirectory.find(x => x.typeSystem != 1 && x.codeProperties == item.idDirectoryProperties);
                        //if (checkExitsNameFile != undefined) {
                        //    item.nameProperties = checkExitsNameFile.nameProperties;
                        //}

                        Activity.GLOBAL.listObjectFile.push(item);
                        ModalInput.updateTable("FileUpload");
                        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("hide");
                    }
                } else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:No.AlreadyExists"),
                        icon: "error",
                        button: l("ManagementLayer:Close"),
                    }).then((value) => {
                    });
                }
            }
        });

        $(`${Activity.SELECTORS.modal}`).on("click", Activity.SELECTORS.btnEditFile, function () {
            modalCreateFolderLayer.clearLabelError();
            //Activity.GLOBAL.checkModalActivity = true;
            let id = $(this).attr("edit-for");
            let obj = Activity.GLOBAL.listObjectFile.find(x => x.id == id);
            modalCreateFolderLayer.setDataEidtFile(obj);
            $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("show");
        });

        $(`${Activity.SELECTORS.modal}`).on("click", Activity.SELECTORS.btnDeleteFile, function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:AreYouWantToDeleteThisFile"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    var id = $(this).attr('delete-for');
                    let obj = Activity.GLOBAL.listObjectFile.find(x => x.id == id);
                    modalCreateFolderLayer.DeleteFileDocument(obj.url);
                    Activity.GLOBAL.listObjectFile = JSON.parse(JSON.stringify(Activity.GLOBAL.listObjectFile.filter(x => x.id != id)));
                    ModalInput.updateTable("FileUpload");
                    //Activity.DeletedFileById(id);
                }
            });

        });
    },
    updateTable: function (checktext) {
        checktext = typeof checktext !== "undefined" ? checktext : $(modalCreateFolderLayer.SELECTORS.itemPlaceActive).attr("data-value");
        var listcolumn = [];
        if (checktext == "image") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnImage;
            if (modalCreateFolderLayer.GLOBAL.listObjectImage !== null && modalCreateFolderLayer.GLOBAL.listObjectImage.length > 0) {
                let test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectImage.length; i++) {
                    test += `<tr>
                                                                <th scope="row">${i + 1}</th>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectImage[i].name}</td>
                                                                <td><img src="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].url}" style="width: 100px;" class="image-infor"></td>
                                                                <td class="url">${modalCreateFolderLayer.GLOBAL.listObjectImage[i].url}</td>
                                                                <td><a href="javascript:;" title="${l('ManagementLayer:Delete')}" class="delete btn btn-danger btn-xs btn-deleteTable-image" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
                modalCreateFolderLayer.eventSildeImage();
            } else {
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="5">${l('No data')}</th>
                                                                        </tr>`);
            }
        }
        if (checktext == "LinkCamera") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnLinkCamera;
            if (modalCreateFolderLayer.GLOBAL.listObjectLinkCamera !== null && modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length > 0) {
                let test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length; i++) {
                    test += `<tr>
                                                                <th scope="row">${i + 1}</th>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].name}</td>
                                                                <td class="url">${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].url}</td>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].order}</td>
                                                                <td><a href="javascript:;" title="${l('ManagementLayer:Edit')}" class="btn btn-info btn-xs btn-edit-link" edit-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l('ManagementLayer:Delete')}" class="delete btn btn-danger btn-xs btn-deleteTable-link" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="5">${l('ManagementLayer:NoData')}</th>
                                                                        </tr>`);
            }
        }
        if (checktext == "FileUpload") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnFile;
            if (Activity.GLOBAL.listObjectFile !== null && Activity.GLOBAL.listObjectFile.length > 0) {
                let test = '';
                for (let i = 0; i < Activity.GLOBAL.listObjectFile.length; i++) {
                    test += `<tr>
                                                                <th scope="row">${i + 1}</th>
                                                                <td>${Activity.GLOBAL.listObjectFile[i].name}</td>
                                                                <td>${Activity.GLOBAL.listObjectFile[i].nameProperties}</td>
                                                                <td class="url">${Activity.GLOBAL.listObjectFile[i].url}</td>
                                                                <td>${Activity.GLOBAL.listObjectFile[i].order}</td>
                                                                <td><a href="javascript:;" title="${l('ManagementLayer:Edit')}" class="btn btn-info btn-xs btn-edit-file-activity" edit-for="${Activity.GLOBAL.listObjectFile[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l('ManagementLayer:Delete')}" class="delete btn btn-danger btn-xs btn-deleteTable-file-activity" delete-for="${Activity.GLOBAL.listObjectFile[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="6">{l('ManagementLayer:NotData')}/th>
                                                                        </tr>`);
            }
        }
        var text = `<tr>`;
        for (var im = 0; im < listcolumn.length; im++) {
            text += `<th scope="col" width="${listcolumn[im].width}%">${listcolumn[im].name}</th>`;
        }
        text += `</tr>`;
        $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableThead}`).html('');
        $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableThead}`).append(text);
    },
    Download: function (url, filename) {
        fetch(url)
            .then(resp => resp.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.href = url;
                // the filename you want
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                window.URL.revokeObjectURL(url);
            })
            .catch(() => alert('oh no!'));
    },
    eventmodal: function () {
        $(Activity.SELECTORS.body_thong_tin).on("keyup change", Activity.SELECTORS.input_activity, function () {
            $(this).parents(".form-group-modal").find(".label-error").remove();
            $(this).parents(".form-group-modal").removeClass("has-error");
        });

        $('iframe').contents().find('.wysihtml5-editor').on('keyup', function () {
            $(this).parents(".form-group-modal").find(".label-error").remove();
            $(this).parents(".form-group-modal").removeClass("has-error");
        });
    },
    GetDanhSachHoatDong: function (idMainObject, idDirectory) {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: true,
            url: Activity.CONSTS.URL_DANH_SACH_HOAT_DONG,
            data: {
                idMainObject: idMainObject,
                idDirectory: idDirectory
            },
            success: function (data) {
                if (data.code == "ok")
                {
                    Activity.GLOBAL.ArrayActivityDetail = [];
                    if (data.result.items.length == 0) {

                        $(`${Activity.SELECTORS.table} tbody`).html(` <tr>
                            <td colspan="3">${l('ManagementLayer:NoData')}</td>
                            </tr>`);

                    } else {
                        Activity.GLOBAL.ArrayActivityDetail = data.result.items;
                        var lstActive = data.result.items;
                        ArrayActivityDetail = data.result.items;
                        var html = ``;
                        for (var i = 0; i < lstActive.length; i++) {
                            html += `<tr>
                                    <td class="columms-10" >${i + 1}</td>
                                    <td class="columms-40" style="text-align: left">${lstActive[i].nameDirectoryActivity}</td>
                                    <td>${moment(lstActive[i].dateActivity).format('DD/MM/YYYY')}</td>
                                </tr>`;
                        }

                        $(`${Activity.SELECTORS.table} tbody`).html(html);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    SelectdDanhSachLoaiHoatDong: function () {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: true,
            url: Activity.CONSTS.URL_DANH_SACH_LOAI_HOAT_DONG,
            data: {
                idDirectory: mapdata.GLOBAL.idDirectory
            },
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;

                    Activity.GLOBAL.ArrayDirectoryActivity = data;
                    var html = `<option value="">${l('ManagementLayer:SelectActivityType')}</option>`;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].type == "file") {
                            html += `<option value="${data[i].id}">${data[i].name}</option>`;
                        }
                    }

                    $(Activity.SELECTORS.selected_loai_hoat_dong).html(html);

                    if (Activity.GLOBAL.IdActivityDirectoryInfo != "") {
                        $(Activity.SELECTORS.selected_loai_hoat_dong).val(Activity.GLOBAL.IdActivityDirectoryInfo).trigger("select2:select");
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

        $(Activity.SELECTORS.selected_loai_hoat_dong).select2({
            //language: Host + "/libs/select2/js/i18n/vi.js",
            placeholder: l('ManagementLayer:SelectActivityType'),
            allowClear: true,
            width: '100%'
        });
    },
    // gen bảng của từng loại hoạt động
    AddTableDanhSachHoatDong: function () {
        var group_to_values = Activity.GLOBAL.ArrayActivityDetail.reduce(function (obj, item) {
            obj[item.nameTypeDirectoryActivity] = obj[item.nameTypeDirectoryActivity] || [];
            obj[item.nameTypeDirectoryActivity].push(item);
            return obj;
        }, {});

        var groups = Object.keys(group_to_values).map(function (key) {
            return { group: key, id: group_to_values[key][0].idDirectoryActivity, data: group_to_values[key] };
        });

        var html = `<input type="file" class="hidden" id="import-file" accept=".xlsx, .xls" />`;
        $('#danh-sach-chi-tiet-hoat-dong').html(html);

        //ListProperties
        for (var i = 0; i < groups.length; i++) {
            html += `<div style="margin: 8px 0px; display:flex;justify-content:space-between;">
                        <h4>${i + 1}. ${groups[i].group}</h4>
                        <div>
                            <button class="export-detail-activity-info btn btn-xs btn-success" data-id="${groups[i].id}" type="button">${l('ManagementLayer:Export')}</button>
                            <button class="import-detail-activity-info btn btn-xs btn-success" data-id="${groups[i].id}" type="button">${l('ManagementLayer:ImportFile')}</button>
                            <button class="them-moi-detail-activity-info btn-xs btn btn-primary" data-id="${groups[i].id}" title="${l('ManagementLayer:AddNew')}" type="button"><i class="fa fa-plus"></i></button>   
                        </div>
                        </div>`;
            var th = `<th class="first-col">${l('ManagementLayer:No.')}</th>
                    <th class="second-col">${l('ManagementLayer:OperationName')}</th>
                    <th class="second-col">${l('ManagementLayer:Date')}</th>`;
            var td = ``;
            var exitsTh = []; // danh sách thuộc tính hiển thị
            var exitsCode = []; // chỉ hiển thị những thuộc tính theo properties của layer

            var directoryActivity = Activity.GLOBAL.ArrayDirectoryActivity.find(x => x.id == groups[i].id);
            if (directoryActivity != undefined) {
                directoryActivity.listProperties = directoryActivity.listProperties.filter(x => x.typeSystem != 1
                    && x.typeProperties != "image" && x.typeProperties != "file" && x.typeProperties != "link");

                for (var j = 0; j < directoryActivity.listProperties.length; j++) {
                    if (jQuery.inArray(directoryActivity.listProperties[j].nameProperties, exitsTh) == -1) // kiểm tra thuộc tính đã có
                    {
                        if (directoryActivity.listProperties[j].typeProperties != "maptile" && directoryActivity.listProperties[j].typeProperties != "geojson") {
                            if (directoryActivity.listProperties[j].isShow) // kiểm tra hiển thị thuộc tính
                            {
                                th += `<th class="item-col">${directoryActivity.listProperties[j].nameProperties}</th>`;
                                exitsTh.push(directoryActivity.listProperties[j].nameProperties);
                                exitsCode.push(directoryActivity.listProperties[j].codeProperties);
                            }
                        }
                    }
                }

                var lstDetailActivity = groups[i].data;
                for (var j = 0; j < lstDetailActivity.length; j++) {
                    td += `<tr><td class="first-col" style="text-align: left;">${k = j + 1}</td>
                          <td class="second-col" style="text-align: left;">${lstDetailActivity[j].nameDirectoryActivity}</td>
                          <td class="second-col">${moment(lstDetailActivity[j].dateActivity).format('DD/MM/YYYY')}</td>`;

                    for (var k = 0; k < directoryActivity.listProperties.length; k++) {
                        var checkExits = lstDetailActivity[j].listProperties.find(x => x.codeProperties == directoryActivity.listProperties[k].codeProperties); // kiểm tra code của thuộc tính
                        var value = "";
                        var danhSachValue;
                        if (checkExits != undefined) {
                            checkExits.typeProperties = directoryActivity.listProperties[k].typeProperties; // set lại kiểu thuộc tính
                            if (checkExits.typeProperties != "maptile" && checkExits.typeProperties != "geojson") //k show thuộc tính bản đồ và maptile
                            {
                                if (checkExits.typeProperties == "checkbox" || checkExits.typeProperties == "list" || checkExits.typeProperties == "radiobutton") // nếu là thuộc tính danh sách thì lấy những value đã dc check
                                {
                                    try {
                                        var lstItems = JSON.parse(checkExits.defalutValue);

                                        if (lstItems != undefined && $.isArray(lstItems)) {
                                            if (checkExits.typeProperties != "checkbox") {
                                                var valueItem = lstItems.find(x => x.checked == true);

                                                if (valueItem != null) {
                                                    value = valueItem.name;
                                                }
                                            }
                                            else {
                                                var string = "";
                                                $.each(lstvalue, function (i, item) {
                                                    if (item.checked) {
                                                        string += "," + item.name;
                                                    }
                                                });

                                                value = string;
                                            }
                                        }
                                        else {
                                            danhSachValue = JSON.parse(directoryActivity.listProperties[k].defalutValue);

                                            if (checkExits.typeProperties != "checkbox") {
                                                var valueItem = danhSachValue.find(x => x.code == checkExits.defalutValue);

                                                if (valueItem != undefined) {
                                                    value = valueItem.name;
                                                }
                                            }
                                            else {
                                                var lstCheck = [];
                                                if (checkExits.defalutValue != "" && checkExits.defalutValue != null) {
                                                    lstCheck = checkExits.defalutValue.split(",");
                                                }

                                                var arrayCheckboxObject = danhSachValue.filter(x => lstCheck.indexOf(x.code) > -1)
                                                if (arrayCheckboxObject.length > 0) {
                                                    value = arrayCheckboxObject.map(x => x.name).toString();
                                                }
                                            }

                                        }
                                    }
                                    catch (e) {
                                        danhSachValue = JSON.parse(directoryActivity.listProperties[k].defalutValue);
                                        if (checkExits.typeProperties != "checkbox") {
                                            var valueItem = danhSachValue.find(x => x.code == checkExits.defalutValue);

                                            if (valueItem != undefined) {
                                                value = valueItem.name;
                                            }
                                        }
                                        else {
                                            var lstCheck = [];
                                            if (checkExits.defalutValue != "" && checkExits.defalutValue != null) {
                                                lstCheck = checkExits.defalutValue.split(",");
                                            }

                                            var arrayCheckboxObject = danhSachValue.filter(x => lstCheck.indexOf(x.code) > -1)
                                            if (arrayCheckboxObject.length > 0) {
                                                value = arrayCheckboxObject.map(x => x.name).toString();
                                            }
                                        }

                                        //value = checkExits.defalutValue;
                                    }
                                }
                                else {
                                    if (checkExits.typeProperties == "bool") {
                                        if (checkExits.defalutValue == "true" || checkExits.defalutValue == true || checkExits.defalutValue == "on") {
                                            value = "Có";
                                        } else {
                                            value = "Không";
                                        }
                                    }
                                    else {
                                        value = checkExits.defalutValue;
                                    }
                                }

                                if (exitsCode.includes(directoryActivity.listProperties[k].codeProperties)) // kiểm tra thuộc tính của đối tượng có nằm trong thuộc tính của layer hay ko
                                {
                                    if (checkExits.typeProperties == "date") {
                                        td += `<td class="item-col">${value}</td>`;
                                    } else {
                                        td += `<td class="${checkExits.typeProperties == "text" || checkExits.typeProperties == "stringlarge" ? "item-text-large" : "item-col"}" style="text-align: left;">${value}</td>`;
                                    }
                                }
                            }
                            else {
                                if (exitsCode.includes(directoryActivity.listProperties[k].codeProperties)) // kiểm tra thuộc tính của đối tượng có nằm trong thuộc tính của layer hay ko
                                {
                                    td += `<td></td>`;
                                }
                            }
                        }
                        else {
                            td += `<td></td>`;
                        }
                    }

                    td += `<td class="second-col">
                            <button class="cap-nhat-detail-activity-info btn btn-xs btn-success" title="${l('ManagementLayer:Update')}" data-directoryActivity="${groups[i].id}" data-id="${lstDetailActivity[j].id}" type="button"><i class="fa fa-edit"></i></button>
                            <button class="xoa-detail-activity-info btn btn-xs btn-danger" title="${l('ManagementLayer:Delete')}" data-id="${lstDetailActivity[j].id}" type="button"><i class="fa fa-trash"></i></button>
                       </td></tr>`;
                }

            }

            th += `<th class="second-col" style="width: 100px;">${l('ManagementLayer:Action')} </th>`;

            var trHead = `<tr>${th}</tr>`;
            var thead = `<thead>${trHead}</thead>`;
            //var trBody = `<tr>${td}</tr>`;
            var tbody = `<tbody>${td}</tbody>`;
            html += `<table class="table table-striped table-bordered">
                            ${thead}
                            ${tbody}
                        </table>`;
        }

        $('.danh-sach-chi-tiet-hoat-dong').html(html);
    },
    // check input form hợp lệ
    CheckValidateForm: function () {
        mapdata.clearLabelError();
        var content_form = `${Activity.SELECTORS.modal} .modal-body`;
        let hasFocus = false;
        let check = true;
        $(`${content_form} ${Activity.SELECTORS.input_activity}`).each(function () {
            var type = $(this).attr("data-type");
            var status = $(this).attr("data-required").toLowerCase();
            if (status == "true") {
                switch (type) {
                    case "radio":
                    case "checkbox":
                        var checkBoxRadio = false;
                        let inputName = $(this).attr('data-name');
                        $(this).find('input').each(function (i, e) {
                            if ($(this).is(":checked")) {
                                checkBoxRadio = true;
                                return;
                            }
                        });

                        if (!checkBoxRadio) {
                            $(content_form + " .nav-tabs .nav-item .nav-link").eq(0).click();
                            check = checkBoxRadio;
                            insertError($(this), "other");
                            if (!hasFocus) {
                                hasFocus = true;
                                $(content_form).animate({
                                    scrollTop: $(content_form).scrollTop() - $(content_form).offset().top + $(this).offset().top
                                }, 50);
                            }

                            mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            return;
                        }
                        break;
                    default:
                        var typeconvert = mapdata.convertcheckTypemodal(type);
                        if (typeconvert != "") {
                            let inputTen = $(this).val();
                            let inputName = $(this).attr('data-name');
                            if (!validateText(inputTen, typeconvert, 0, 0)) {
                                $(content_form + " .nav-tabs .nav-item .nav-link").eq(0).click();

                                if ((type == "text" || type == "stringlarge") && inputTen.trim() != "") {
                                    return true;
                                }
                                insertError($(this), "other");

                                if (!hasFocus) {
                                    if (type == "text" || type == "stringlarge") {
                                        $(this).parent().find('.trumbowyg-editor').focus();
                                    }
                                    else {
                                        //console.log($(this).attr("id"));
                                        $(this).focus();
                                    }
                                    hasFocus = true;
                                }

                                check = false;
                                mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            }
                        }
                        break;
                }
            }
        });

        let checkHtml = mapdata.CheckedHtmlEntities(Activity.SELECTORS.modal);
        if (check) {
            check = checkHtml;
        }
        Activity.eventmodal();
        return check;
    },
    CheckedHtmlEntities: function (form) {
        var check = true;
        $(`${form} input[type=text]`).each(function () {
            var value = $(this).val();
            var inputName = $(this).attr('data-name');
            var status = $(this).attr("data-required");
            if (status != undefined) {
                status = status.toLowerCase()
            }

            if ($(this).val() != "") {
                let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;
                if (re.test(value)) {
                    $(this).parents(".form-group-modal").find('.label-error').remove();
                    insertError($(this), "other");
                    mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), inputName));
                    check = false;
                }
            }
        });
        return check;
    },
    checkValidFile: function () {
        var content = `${Activity.SELECTORS.modal} .modal-body`;

        var isCheckedFile = true;
        var message = [];
        var lstExtendProperties = Activity.GLOBAL.ArrayProperties.filter(x => (x.TypeProperties == "link" || x.TypeProperties == "image" || x.TypeProperties == "file") && x.IsRequest);
        lstExtendProperties = [...new Map(lstExtendProperties.map(item =>
            [item["TypeProperties"], item])).values()];

        if (lstExtendProperties.length > 0) {
            $.each(lstExtendProperties, function (i, obj) {

                if (obj.TypeProperties == "link") {
                    if (ModalInput.GLOBAL.listObjectLinkCamera.length == 0) {
                        isCheckedFile = false;
                        message.push(l("link"));
                    }
                }
                if (obj.TypeProperties == "image") {
                    if (ModalInput.GLOBAL.listObjectImage.length == 0) {
                        isCheckedFile = false;
                        message.push(l("image"));
                    }
                }
                if (obj.TypeProperties == "file") {
                    if (Activity.GLOBAL.listObjectFile.length == 0) {
                        isCheckedFile = false;
                        message.push(l("file"));
                    }
                }
            });

            if (!isCheckedFile) {
                $(content + " .nav-tabs .nav-item .nav-link").eq(1).click();

                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("Dữ liệu mở rộng (" + message.toString() + ") không được bỏ trống"),
                    icon: "warning",
                    button: l("ManagementLayer:Close"),
                })
            }
        }

        return isCheckedFile;
    },
    selectInput: function (type) {
        var requried = (type.IsRequest) ? "required" : "";
        //if (type.CodeProperties == 'Title') requried = "required";
        var text = `<div class="row">
                        <label class="control-label col-md-3 col-xs-12 ${requried}">${type.NameProperties}</label>
                        <div class="col-md-9 col-xs-12">
                                <div class="row form-group-modal">`;
        //<input type="text" id="text-name" class="form-control form-style" placeholder="Tên kiểu" />
        switch (type.TypeProperties) {
            case 'bool':
                var check = true;
                if (typeof type.DefalutValue == "string") {
                    if (type.DefalutValue != "true") {
                        check = false
                    }
                }

                if (check) {
                    //text += `<input type="checkbox" data-type="bool" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" class="property-activity-input form-style" checked="true">`;
                    text += `<div class="custom-checkbox custom-control">
                                <input type="checkbox" data-type="bool" data-name="${type.NameProperties}" data-required="${type.IsRequest}" id="text-modal-${type.CodeProperties}-activity" name="${type.CodeProperties}$" class="property-activity-input custom-control-input modal-layer form-style" checked>
                                <label class="custom-control-label" for="text-modal-${type.CodeProperties}-activity">${type.NameProperties}</label>
                            </div>`;
                } else {
                    //text += `<input type="checkbox" data-type="bool" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" class="property-activity-input form-style">`;
                    text += `<div class="custom-checkbox custom-control">
                                <input type="checkbox" data-type="bool" data-name="${type.NameProperties}" data-required="${type.IsRequest}" id="text-modal-${type.CodeProperties}-activity" name="${type.CodeProperties}$" class="property-activity-input custom-control-input modal-layer form-style">
                                <label class="custom-control-label" for="text-modal-${type.CodeProperties}-activity"></label>
                            </div>`;
                }
                break;
            case 'text':
                text += `<textarea type="text" data-type="text" data-name="${type.NameProperties}" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" >${type.DefalutValue}</textarea>`;
                break;
            case 'float':
                text += `<input type="number" autocomplete="off" data-type="float" data-name="${type.NameProperties}" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'int':
                text += `<input type="number"  autocomplete="off" data-type="int" data-name="${type.NameProperties}" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'list':
                var lst = JSON.parse(type.DefalutValue);
                text += `<select id="text-modal-${type.CodeProperties}" data-name="${type.NameProperties}" data-required=${type.IsRequest}" class="form-control">`;
                $.each(lst, function (i, obj) {
                    text += `<option ${obj.checked ? "selected" : ""} value="${obj.code}">${obj.name}</option>`;
                });
                text += ` </select>`;
                //text += `<input type="text" data-type="list" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                break;
            case 'date':
                text += `<input type="text" autocomplete="off" data-type="date"data-name="${type.NameProperties}" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'string':
                text += `<input type="text" autocomplete="off"  data-type="string" data-name="${type.NameProperties}" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'stringlarge':
                text += `<textarea type="text" data-type="stringlarge" data-name="${type.NameProperties}" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" >${type.DefalutValue}</textarea>`;
                break;
            case 'geojson':
                text += `<textarea type="text" data-type="geojson" data-name="${type.NameProperties}" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" >${type.DefalutValue}</textarea>`;
                break;
            case 'checkbox':
                var lst2 = JSON.parse(type.DefalutValue);
                text += `<div class="property-activity-input div-checkbox-radio" data-name="${type.NameProperties}" data-type="checkbox" data-required=${type.IsRequest}>`;
                $.each(lst2, function (i, obj) {
                    //text += `<label class="col-sm-6">
                    //            <input type="checkbox" name="text-modal-checkbox" data-name="${obj.name} " class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                    //         </label>`;

                    text += `<div class="col-sm-6" style="padding-left:0;">
                                <div class="custom-checkbox custom-control">
                                    <input type="checkbox" data-val="true" id="text-modal-${type.CodeProperties}${i}" data-name="${obj.name}" name="${type.CodeProperties}" value="${obj.code}" class="text-modal-${type.CodeProperties} custom-control-input" ${obj.checked ? "checked='checked'" : ""}>
                                    <label class="custom-control-label" for="text-modal-${type.CodeProperties}${i}">${obj.name}</label>
                                </div>
                            </div>`;
                });
                text += `</div>`
                break;
            case 'radiobutton':
                var lst3 = JSON.parse(type.DefalutValue);
                text += `<div class="property-activity-input div-checkbox-radio" data-name="${type.NameProperties}" data-type="checkbox" data-required=${type.IsRequest}>`;
                $.each(lst3, function (i, obj) {
                    //text += `<label class="col-sm-6">
                    //            <input type="radio" name="text-modal-radio-${type.CodeProperties}" data-name="${obj.name}" class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                    //         </label>`;

                    text += `<div class="col-sm-6" style="padding-left:0;">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="${type.CodeProperties}" id="text-modal-radio-${type.CodeProperties}${i}" data-name="${obj.name}" value="${obj.code}" class="custom-control-input text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} >
                                    <label class="custom-control-label" for="text-modal-radio-${type.CodeProperties}${i}">${obj.name} </label>
                                </div>
                            </div>`;
                });
                text += `</div>`
                break;
            default:
                return text = '';
            //break;
        }
        text += `                            </div>
                        </div>
                    </div>`;
        return text;
    },
    // reset data hoạt động sau khi thêm mới
    HideFormActivityDetail: function () {
        // reset data activity modal
        ModalInput.GLOBAL.listImageInput = [];
        ModalInput.GLOBAL.filecount = 0;
        ModalInput.GLOBAL.listIdImageTemporary = [];
        ModalInput.GLOBAL.listObjectImage = [];
        ModalInput.GLOBAL.listObjectLinkCamera = [];
        ModalInput.GLOBAL.listObjectFile = [];

        $(Activity.SELECTORS.modal).modal('hide');

    },
    DeletedFileById: function (id) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + "/api/HTKT/DocumentActivity/delete-file?Id=" + id,
            data: {
            },
            success: function (data) {
                if (data.code == "ok") {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:DeleteDataSuccessfully"),
                        icon: "success",
                        button: l("ManagementLayer:Close"),
                    }).then((value) => {

                    });
                } else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:DeleteDataFailed"),
                        icon: "warning",
                        button: l("ManagementLayer:Close"),
                    });
                }
            }
        });
    },
    GetFileByActivityId: function (id) {

        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + "/api/HTKT/DocumentActivity/get-file-by-DetailActivityId?Id=" + id,
            data: {
            },
            success: function (data) {
                Activity.GLOBAL.listObjectFile = [];
                if (data.code == "ok" && data.result.length > 0) {
                    $.each(data.result, function (i, obj) {
                        var item = {
                            id: obj.id,
                            name: obj.nameDocument,
                            idDirectoryProperties: obj.nameFolder,
                            url: obj.urlDocument,
                            order: obj.orderDocument
                        };
                        Activity.GLOBAL.listObjectFile.push(item);
                    });
                }
            }
        });
    },
    ResetTabExented: function () {
        ModalInput.GLOBAL.listImageInput = [];
        ModalInput.GLOBAL.filecount = 0;
        ModalInput.GLOBAL.listIdImageTemporary = []
        ModalInput.GLOBAL.listObjectImage = [];
        ModalInput.GLOBAL.listObjectLinkCamera = [];
        ModalInput.GLOBAL.listObjectFile = [];
    }
};

var ModalInput = {
    GLOBAL: {
        // image
        listImageInput: [],
        filecount: 0,
        listIdImageTemporary: [],
        listObjectImage: [],
        listObjectLinkCamera: [],
        listObjectFile: [],
    },
    CONSTS: {
    },
    SELECTORS: {
        fileUploadFile: "#myModalActivity #body-mo-rong-hoat-dong .file-upload",
        itemFilePlace: ".item-FilePlace",
        searchNewfile: ".search-newfile",
        btnAddNewFile: ".search-newfile button",
        searchTenTable: ".search-newfile>input",

        myModalLinkCamera: "#myModalLinkCamera",
        modalFileUploadInfor: "#myModalFileUpload",
        // image
        fileUploadInput: ".file-upload-input",
        imageUploadWrap: '.image-upload-wrap',
        deleteBtn: '.remove-image-modal',
        fileUploadContent: '.file-upload-content',
        fileUploadBtn: '.file-upload-btn',
        btnSaveImage: '#save-image',
        btnDeleteImage: ".btn-deleteTable-image",
        itemPlaceActive: ".item-FilePlace.activefile",
        modelViewImage: "#myModalViewImageInfor",
        carouselIndicatorsLiViewImage: "#myModalViewImageInfor .carousel-indicators",
        carouselIndicatorsViewImage: "#myModalViewImageInfor .carousel-inner",

        //link
        btnSaveLink: ".btn-link-save",
        inputIdLink: "input[name='IdLink']",
        inputNameLink: "input[name='NameLink']",
        inputLinkLink: "input[name='UrlLink']",
        inputOrderLink: "input[name='OrderLink']",
        btnEditLink: ".btn-edit-link",
        btnDeleteLink: ".btn-deleteTable-link",

        tableTbody: "#myModalActivity .table-infor-object tbody",
        tableThead: "#myModalActivity .table-infor-object thead",
        table: ".table-infor-object",
    },
    init: function () {
        this.setEvent();
    },
    setEvent: function () {
        // tab li
        $(Activity.SELECTORS.modal).on('click', ModalInput.SELECTORS.itemFilePlace, function () {
            var id = $(this).attr("data-value");
            $(ModalInput.SELECTORS.itemFilePlace).removeClass("activefile");
            $(this).addClass("activefile");
            $(ModalInput.SELECTORS.fileUploadFile).css("display", "none");
            if (id == "image") {
                $(ModalInput.SELECTORS.fileUploadFile).css("display", "block");
                $(ModalInput.SELECTORS.searchNewfile).hide();
            } else {
                $(ModalInput.SELECTORS.searchNewfile).show();
            }
            ModalInput.resetAll();
            ModalInput.updateTable(id);
            //Activity.updateTable();
        });

        // image
        $(Activity.SELECTORS.modal).on("change", ModalInput.SELECTORS.fileUploadInput, function () {
            $(this).parents('.tab-pane').find('.lable-error').remove();
            var indexCounter = 0;
            var exitsFormat = true;

            for (var i = 0; i < this.files.length; i++) {
                indexCounter++;
                var exp = this.files[i].name.substring(this.files[i].name.lastIndexOf(".") + 1).toLowerCase();
                if (exp == "png" || exp == "jpg" || exp == "jpeg") {
                    if (this.files[i].size <= sizeFiles.sizeImgNormal) // check size
                    {
                        let uid = mapdata.uuidv4();
                        ModalInput.readURL2(this.files[i], uid);
                        ModalInput.GLOBAL.listImageInput.push({ id: uid, file: this.files[i] });
                    }
                    else {
                        exitsFormat = false;
                    }
                } else {
                    exitsFormat = false;
                }
            };

            if (!exitsFormat) {
                if (indexCounter > 1) {
                    $(this).parents('.tab-pane').append(`<label class="lable-error text-danger mt-2">${l("ManagementLayer:MutipleFileExitsFormat")}</label>`);
                }
                else {
                    $(this).parents('.tab-pane').append(`<label class="lable-error text-danger mt-2">${l("ManagementLayer:FileExitsFormat")}</label>`);
                }

            }

            $(ModalInput.SELECTORS.imageUploadWrap).removeClass('image-dropping');
            $(this).val(null);
        });

        $(Activity.SELECTORS.modal).on('click', ModalInput.SELECTORS.fileUploadBtn, function () {
            $(this).parents('.tab-pane').find('.lable-error').remove();
            if (ModalInput.GLOBAL.filecount > 0) {
                var lstfile = ModalInput.GLOBAL.listImageInput;
                for (var i = 0; i < lstfile.length; i++) {
                    if (lstfile[i].file.type.toLowerCase() == "image/gif" || lstfile[i].file.type.toLowerCase() == "image/jpeg"
                        || lstfile[i].file.type.toLowerCase() == "image/png" || lstfile[i].file.type.toLowerCase() == "image/jpg") {
                        ModalInput.GLOBAL.nameImage = lstfile[i].file.name;
                        ModalInput.SendFileImage(lstfile[i], ModalInput.UpdateInforFileList);
                    }
                }
                ModalInput.deleteAllImageTemporary();
                ModalInput.updateTable("image");
                const dt = new DataTransfer();
                $(ModalInput.SELECTORS.fileUploadInput)[0].files = dt.files;

                //swal({
                //    title: l("ManagementLayer:Notification"),
                //    text: l("ManagementLayer:AreYouSaveImage"),
                //    icon: "warning",
                //    buttons: [
                //        l("ManagementLayer:Close"),
                //        l("ManagementLayer:Agree")
                //    ],
                //}).then((value) => {
                //    if (value) {

                //    }
                //})
            } else {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:NoImageSeleted"),
                    icon: "warning",
                    button: l("ManagementLayer:Close"),
                }).then((value) => {
                });
            }

        });

        // xóa hình input
        $(`${ModalInput.SELECTORS.fileUploadFile}`).on("click", ModalInput.SELECTORS.deleteBtn, function () {
            $(this).parents('.tab-pane').find('.lable-error').remove();

            var id = $(this).attr('data-id');
            var elementbuttondelete = document.getElementById(id);
            //elementbuttondelete.parentElement.parentElement.parentNode.removeChild(elementbuttondelete.parentElement.parentElement);
            document.getElementById(id).remove();
            var elementbuttondelete = document.getElementById(id);
            if (elementbuttondelete != null) {
                document.getElementById(id).remove();
            }
            ModalInput.GLOBAL.filecount--;
            if (ModalInput.GLOBAL.filecount == 0) $(ModalInput.SELECTORS.fileUploadBtn).hide();
            var fileinput = ModalInput.GLOBAL.listImageInput;
            var filedelete = fileinput.find(x => x.id == id);
            const dt = new DataTransfer();
            for (var i = 0; i < fileinput.length; i++) {
                if (fileinput[i].id != id) {
                    dt.items.add(fileinput[i].file)
                }
            }
            var lstinput = [];
            $(`${Activity.SELECTORS.modal} ${ModalInput.SELECTORS.fileUploadInput}`)[0].files = dt.files;
            for (var i = 0; i < fileinput.length; i++) {
                if (fileinput[i].file.name != filedelete.file.name) {
                    lstinput.push(fileinput[i])
                }
            }
            //ModalInput.GLOBAL.listImageInput = lstinput;
            ModalInput.GLOBAL.listImageInput = ModalInput.GLOBAL.listImageInput.filter(x => x.id != id);
            ModalInput.GLOBAL.listIdImageTemporary = JSON.parse(JSON.stringify(ModalInput.GLOBAL.listIdImageTemporary.filter(x => x != id)));
        });

        // xóa hình trong table
        $(`${Activity.SELECTORS.modal} ${ModalInput.SELECTORS.table}`).on("click", ModalInput.SELECTORS.btnDeleteImage, function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:AreYouDeleteImage"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    var data = $(this).attr('delete-for');
                    var obj = ModalInput.GLOBAL.listObjectImage.find(x => x.id == data);
                    if (obj !== null) {
                        //let url = encodeURIComponent(obj.url)
                        let check = ModalInput.DeleteFileImage(obj.url);
                        //if (check) {
                        ModalInput.GLOBAL.listObjectImage = JSON.parse(JSON.stringify(ModalInput.GLOBAL.listObjectImage.filter(x => x.id != data)));
                        ModalInput.updateTable("image");
                        //}
                    }
                }
            })
        });

        ///***************link*************//
        $(Activity.SELECTORS.modal).on("click", ModalInput.SELECTORS.btnAddNewFile, function () {
            modalCreateFolderLayer.clearLabelError();
            let check = $(ModalInput.SELECTORS.itemPlaceActive).attr("data-value");
            switch (check.toLowerCase()) {
                case "linkcamera":
                    ModalInput.reSetFormLink();
                    $(ModalInput.SELECTORS.myModalLinkCamera).modal("show");
                    break;
                case "fileupload":
                    $(ModalInput.SELECTORS.modalFileUploadInfor).modal("show");
                    break;
                default:
                    break;
            }
        });


        $(ModalInput.SELECTORS.myModalLinkCamera).on('hidden.bs.modal', function () {
            ModalInput.reSetFormLink();
        });

        $(ModalInput.SELECTORS.myModalLinkCamera).on("click", ModalInput.SELECTORS.btnSaveLink, function () {
            if ($(Activity.SELECTORS.modal).hasClass('show')) {
                var elementForm = ModalInput.SELECTORS.myModalLinkCamera;

                if (modalCreateFolderLayer.checkLink(elementForm)) {
                    let thutu = Number($(`${elementForm} ${ModalInput.SELECTORS.inputOrderLink}`).val().trim());

                    if (ModalInput.GLOBAL.listObjectLinkCamera == null) {
                        ModalInput.GLOBAL.listObjectLinkCamera = [];
                    }

                    var checkthutu = ModalInput.GLOBAL.listObjectLinkCamera.find(x => x.order == thutu);

                    let id = $(`${elementForm} ${ModalInput.SELECTORS.inputIdLink}`).val();
                    if (typeof id !== "undefined" && id.length > 0 && ((typeof checkthutu !== "undefined" && checkthutu.id == id) || typeof checkthutu == "undefined")) {
                        var objTemp = ModalInput.GLOBAL.listObjectLinkCamera.find(x => x.id == id);
                        objTemp.name = $(`${elementForm} ${ModalInput.SELECTORS.inputNameLink}`).val().trim();
                        objTemp.url = $(`${elementForm} ${ModalInput.SELECTORS.inputLinkLink}`).val().trim();
                        objTemp.order = Number($(`${elementForm} ${ModalInput.SELECTORS.inputOrderLink}`).val().trim());
                        ModalInput.updateTable("LinkCamera");
                        $(ModalInput.SELECTORS.myModalLinkCamera).modal("hide");
                    } else if ((typeof id == "undefined" || id.length <= 0) && typeof checkthutu == "undefined") {
                        var count = ModalInput.GLOBAL.listObjectLinkCamera.length;
                        var item = {
                            //stt: count + 1,
                            id: mapdata.uuidv4(),
                            name: $(`${elementForm} ${ModalInput.SELECTORS.inputNameLink}`).val().trim(),
                            url: $(`${elementForm} ${ModalInput.SELECTORS.inputLinkLink}`).val().trim(),
                            order: Number($(`${elementForm} ${ModalInput.SELECTORS.inputOrderLink}`).val().trim()),
                        };
                        ModalInput.GLOBAL.listObjectLinkCamera.push(item);
                        //$('#myModalVideo').hide();
                        ModalInput.updateTable("LinkCamera");
                        $(ModalInput.SELECTORS.myModalLinkCamera).modal("hide");
                    } else {
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:No.AlreadyExists"),
                            icon: "error",
                            button: l("ManagementLayer:Close"),
                        }).then((value) => {
                        });
                    }
                }
            }
        })


        $(`${Activity.SELECTORS.modal} ${ModalInput.SELECTORS.table}`).on("click", ModalInput.SELECTORS.btnEditLink, function () {
            modalCreateFolderLayer.clearLabelError();

            let id = $(this).attr("edit-for");

            let obj = ModalInput.GLOBAL.listObjectLinkCamera.find(x => x.id == id);
            ModalInput.setDataEidtLink(obj);
            $(ModalInput.SELECTORS.myModalLinkCamera).modal("show");
        });

        $(`${Activity.SELECTORS.modal} ${ModalInput.SELECTORS.table}`).on("click", ModalInput.SELECTORS.btnDeleteLink, function () {
            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:AreYouDeleteLink"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    var data = $(this).attr('delete-for');
                    ModalInput.GLOBAL.listObjectLinkCamera = JSON.parse(JSON.stringify(ModalInput.GLOBAL.listObjectLinkCamera.filter(x => x.id != data)));
                    ModalInput.updateTable("LinkCamera");
                }
            })
        });
    },
    //image
    readURL2: function (input, id) {
        var typeinput = input.name.split('.').pop().toLowerCase();
        if (typeinput == "jpg" || typeinput == "png" || typeinput == "jpeg" || typeinput == "gif") {
            if (input) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(ModalInput.SELECTORS.fileUploadBtn).css('display', 'inline-block');
                    var text = `<div style="width: 120px;height:100px; position: relative;" id="${id}">
                                            <img class="file-upload-image" src="${e.target.result}" alt="your image" title=${input.name}>
                                            <div class="image-title-wrap" style="position: absolute;top: 5px;right: 5px;margin: 0px;padding: 0px;">
                                                <button type="button" data-id=${id} title="Xóa ảnh ${input.name}" class="btn remove-image remove-image-modal"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                                            </div>
                                        </div>`
                    ModalInput.GLOBAL.listIdImageTemporary.push(id);
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.fileUploadContent}`).append(text);

                    ModalInput.GLOBAL.filecount++;
                };
                reader.readAsDataURL(input);
            }
        }
    },
    SendFileImage: function (file, callback) {
        var formData = new FormData();
        formData.append('file', file.file);
        $.ajax({
            url: "/api/htkt/file/image",
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            async: false,
            success: callback,
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });
    },
    deleteAllImageTemporary: function () {
        for (var i = 0; i < ModalInput.GLOBAL.listIdImageTemporary.length; i++) {
            var id = ModalInput.GLOBAL.listIdImageTemporary[i];
            //var elementbuttondelete = document.getElementById(id);
            //elementbuttondelete.parentElement.parentElement.parentNode.removeChild(elementbuttondelete.parentElement.parentElement);
            document.getElementById(id).remove();
        }
        ModalInput.GLOBAL.filecount = 0;
        ModalInput.GLOBAL.listIdImageTemporary = [];
        ModalInput.GLOBAL.listImageInput = [];
        $(ModalInput.SELECTORS.fileUploadBtn).hide();
    },
    updateTable: function (checktext) {
        checktext = typeof checktext !== "undefined" ? checktext : $(ModalInput.SELECTORS.itemPlaceActive).attr("data-value");
        var listcolumn = [];
        if (checktext == "image") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnImage;
            if (ModalInput.GLOBAL.listObjectImage !== null && ModalInput.GLOBAL.listObjectImage.length > 0) {
                var test = '';
                for (var i = 0; i < ModalInput.GLOBAL.listObjectImage.length; i++) {
                    test += `<tr>
                                                                <td>${ModalInput.GLOBAL.listObjectImage[i].name}</td>
                                                                <td><img src="${ModalInput.GLOBAL.listObjectImage[i].url}" style="width: 100px;" class="image-infor"></td>
                                                                <td><a href="javascript:;" title="Xóa" class="delete btn btn-danger btn-xs btn-deleteTable-image" delete-for="${ModalInput.GLOBAL.listObjectImage[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                }
                $(ModalInput.SELECTORS.tableTbody).children().remove();
                $(ModalInput.SELECTORS.tableTbody).append(test);
                ModalInput.eventSildeImage();
            } else {
                $(ModalInput.SELECTORS.tableTbody).html('');
                $(ModalInput.SELECTORS.tableTbody).append(`<tr>
                                                                            <th scope="row" colspan="5">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }
        if (checktext == "LinkCamera") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnLinkCamera;
            if (ModalInput.GLOBAL.listObjectLinkCamera !== null && ModalInput.GLOBAL.listObjectLinkCamera.length > 0) {
                var testCamera = '';

                for (var i = 0; i < ModalInput.GLOBAL.listObjectLinkCamera.length; i++) {

                    testCamera += `<tr>
                                                                <td>${ModalInput.GLOBAL.listObjectLinkCamera[i].name}</td>
                                                                <td class="url">${ModalInput.GLOBAL.listObjectLinkCamera[i].url}</td>
                                                                <td>${ModalInput.GLOBAL.listObjectLinkCamera[i].order}</td>
                                                                <td><a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn btn-info btn-xs btn-edit-link" edit-for="${ModalInput.GLOBAL.listObjectLinkCamera[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn btn-danger btn-xs btn-deleteTable-link" delete-for="${ModalInput.GLOBAL.listObjectLinkCamera[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                }

                $(ModalInput.SELECTORS.tableTbody).html('');
                $(ModalInput.SELECTORS.tableTbody).append(testCamera);
            } else {
                $(ModalInput.SELECTORS.tableTbody).html('');
                $(ModalInput.SELECTORS.tableTbody).append(`<tr>
                                                                            <th scope="row" colspan="5">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }
        //if (checktext == "FileUpload") {
        //    listcolumn = modalCreateFolderLayer.GLOBAL.listColumnFile;
        //    if (ModalInput.GLOBAL.listObjectFile !== null && ModalInput.GLOBAL.listObjectFile.length > 0) {
        //        var test2 = '';
        //        for (var iz = 0; iz < ModalInput.GLOBAL.listObjectFile.length; iz++) {
        //            test2 += `<tr>
        //                                                        <th scope="row">${iz + 1}</th>
        //                                                        <td>${ModalInput.GLOBAL.listObjectFile[iz].name}</td>
        //                                                        <td class="url">${ModalInput.GLOBAL.listObjectFile[iz].url}</td>
        //                                                        <td>${ModalInput.GLOBAL.listObjectFile[iz].order}</td>
        //                                                        <td><a href="javascript:;" title="Chỉnh sửa" class="btn btn-info btn-xs btn-edit-link" edit-for="${ModalInput.GLOBAL.listObjectLinkCamera[iz].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
        //                                                            <a href="javascript:;" title="Xóa" class="delete btn btn-danger btn-xs btn-deleteTable-link" delete-for="${ModalInput.GLOBAL.listObjectLinkCamera[iz].id}"><i class="fa fa-trash-o"></i></a></td>
        //                                                    </tr>`;
        //        }

        //        $(ModalInput.SELECTORS.tableTbody).html('');
        //        $(ModalInput.SELECTORS.tableTbody).append(test2);
        //    } else {
        //        $(ModalInput.SELECTORS.tableTbody).html('');
        //        $(ModalInput.SELECTORS.tableTbody).append(`<tr>
        //                                                                    <th scope="row" colspan="6">Không có dữ liệu</th>
        //                                                                </tr>`);
        //    }
        //}
        if (checktext == "FileUpload") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnFile;
            if (Activity.GLOBAL.listObjectFile !== null && Activity.GLOBAL.listObjectFile.length > 0) {
                let test = '';
                for (let i = 0; i < Activity.GLOBAL.listObjectFile.length; i++) {
                    test += `<tr>
                                                                <td>${Activity.GLOBAL.listObjectFile[i].name}</td>
                                                                <td>${Activity.GLOBAL.listObjectFile[i].nameProperties}</td>
                                                                <td class="url">${Activity.GLOBAL.listObjectFile[i].url}</td>
                                                                <td><a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn btn-info btn-xs btn-edit-file-activity" edit-for="${Activity.GLOBAL.listObjectFile[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn btn-danger btn-xs btn-deleteTable-file-activity" delete-for="${Activity.GLOBAL.listObjectFile[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${Activity.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="6">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }
        var text = `<tr>`;
        for (var i = 0; i < listcolumn.length; i++) {
            text += `<th scope="col" width="${listcolumn[i].width}%">${listcolumn[i].name}</th>`;
        }
        text += `</tr>`;
        $(ModalInput.SELECTORS.tableThead).html('');
        $(ModalInput.SELECTORS.tableThead).append(text);
    },
    UpdateInforFileList: function (data) {
        if (data.code == "ok") {
            var re = data.result;
            var count = ModalInput.GLOBAL.listObjectImage.length;
            var ten = (ModalInput.GLOBAL.nameImage !== "") ? ModalInput.GLOBAL.nameImage : re.name
            var item = {
                id: re.id,
                order: count + 1,
                name: ten,
                url: re.url,
            };
            ModalInput.GLOBAL.listObjectImage.push(item);
            ModalInput.updateTable("image");
        }
    },
    eventSildeImage: function () {
        ModalInput.addImageSilde();
        $(ModalInput.SELECTORS.imageInfor).on("click", function () {
            $(ModalInput.SELECTORS.modelViewImage).modal("show");
        });
    },
    addImageSilde: function () {
        let html1 = "";
        $.each(ModalInput.GLOBAL.listObjectImage, function (i, obj) {
            let active = (i == 0) ? "active" : "";
            html1 += '<div class="item ' + active + '"><img src = "' + obj.url + '" data-holder-rendered="true"></div>';
        });
        $(ModalInput.SELECTORS.carouselIndicatorsViewImage).children().remove();
        $(ModalInput.SELECTORS.carouselIndicatorsViewImage).append(html1);
    },
    DeleteFileImage: function (url) {
        var urlEnCode = encodeURIComponent(url);
        var check = false;
        $.ajax({
            url: "/api/htkt/file/deleteimage",
            type: 'POST',
            data: JSON.stringify({ url: urlEnCode }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (res) {
                if (res.code == "ok") {
                    check = res.result;
                }
                else {
                    console.log(res);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + ": " + errorThrown);
            }
        });
        return check;
    },
    resetAll: function () {
        $(ModalInput.SELECTORS.searchTenTable).val('');
        ModalInput.deleteAllImageTemporary();
    },
    //**link*/
    setDataEidtLink: function (obj) {
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputIdLink}`).val(obj.id);
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputNameLink}`).val(obj.name);
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputLinkLink}`).val(obj.url);
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputOrderLink}`).val(obj.order);;
    },
    reSetFormLink: function () {
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputIdLink}`).val("");
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputNameLink}`).val("");
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputLinkLink}`).val("");
        $(`#myModalLinkCamera ${ModalInput.SELECTORS.inputOrderLink}`).val("");;
    }
}
/*
* Page loaded
*/
$(document).ready(function () {
    Activity.init();
    ModalInput.init();
});
