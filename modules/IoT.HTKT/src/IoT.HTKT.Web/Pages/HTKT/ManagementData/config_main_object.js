﻿var Config_Main_Object = {
    GLOBAL: {
    },
    CONSTS: {
    },
    SELECTORS: {

        content_create: "#myModal2",
        content_info: "#InforData",
        content_edit: "#EditForm",
        modelPropertiesImport: ".model-properties-import",

        strokeColor: ".favcolorText-edit",
        strokeWidth: ".text-width",
        strokeOpacity: '.rangeMax-stress',
        fillColor: '.favcolorText-color',
        fillOpacity: '.rangeMax-color',

        //stepconfig variable
        propertyStrokeText: ".stroke-text-main-object", // elemnt input màu thuộc tính đường
        propertyStrokeColor: ".strokeColor-main-object", // element bảng màu thuộc tính đường
        propertyDivStroke: ".div-stroke-main-object", // element ô màu thuộc tính đường
        checkbox_inheritance: ".checkbox-inheritance", // checkbox kế thừa thuộc tính layer

        propertyStrokeWidth: ".text-width-main-object", // element input độ rộng thuộc tính đường
        propertyStrokeOpacity: ".rangeMax-stress-main-object", // element độ trong suốt thuộc tính đường

        thumbMax_stroke: ".thumbMax-stroke-main-object",
        maxStroke: ".max-stroke-main-object",
        lineStroke: ".line-stroke-main-object",

        propertyFillText: ".fillText-color-main-object",
        propertyFillColor: ".fillColor-color-main-object",
        propertyDivFill: ".div-fill-main-object",
        propertyFillOpacity: ".rangeMax-color-main-object",


        thumbMax_fill: ".thumbMax-fill-main-object",
        maxFill: ".max-fill-main-object",
        lineFill: ".line-fill-main-object",

        // image
        choosen_image: ".choosen-icon-2d-main-object",
        image_config: ".img-icon-2d-main-object",

        checkbox_implement: ".checkbox-inheritance"
    },
    init: function () {
        this.setEvent();
    },
    setEvent: function () {
        const calcLeftPositionStress = value => 100 / (100 - 0) * (value - 0);

        //------------------------------sự kiện click vào input text màu của thuộc tính đường-------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyStrokeText).on('click', function () {
            var content = Config_Main_Object.getContent();

            $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeColor).trigger('click');
        });

        //-----------------------sự kiện click ra ngoài input text màu của thuộc tính đường----------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyStrokeText).on('focusout', function () {
            var content = Config_Main_Object.getContent();

            if ($(this).val().indexOf("#") >= 0) {
                $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeText).val($(this).val());
                $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeColor).val($(this).val());
                $(content + " " + Config_Main_Object.SELECTORS.propertyDivStroke).css('background-color', $(this).val());
                $(content + " " + Config_Main_Object.SELECTORS.checkbox_inheritance).prop('checked', false);
            }
        });

        //------------------sự kiện click vào ô màu của thuộc tính đường--------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyDivStroke).on('click', function () {
            var content = Config_Main_Object.getContent();
            $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeColor).trigger('click');
        });

        //------------------sự kiện chọn trong bảng màu của thuộc tính đường---------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyStrokeColor).on('input', function () {
            var content = Config_Main_Object.getContent();
            $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeText).val($(this).val());
            $(content + " " + Config_Main_Object.SELECTORS.propertyDivStroke).css('background-color', $(this).val());
            $(content + " " + Config_Main_Object.SELECTORS.checkbox_inheritance).prop('checked', false);
        });

        //--------------------sự kiện thay đổi độ rộng của thuộc tính đường-----------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyStrokeWidth).on('change', function () {
            var content = Config_Main_Object.getContent();
            $(content + " " + Config_Main_Object.SELECTORS.checkbox_inheritance).prop('checked', false);
        });

        //--------------------sự kiện thay đổi độ trong suốt của thuộc tính đường-------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyStrokeOpacity).on('input', function (e) {
            var content = Config_Main_Object.getContent();
            const newValue = parseInt(e.target.value);
            $(content + " " + Config_Main_Object.SELECTORS.thumbMax_stroke).css('left', parseInt(calcLeftPositionStress(newValue)) + '%');
            $(content + " " + Config_Main_Object.SELECTORS.maxStroke).html(newValue + '.00%');
            $(content + " " + Config_Main_Object.SELECTORS.lineStroke).css({
                'right': (100 - parseInt(calcLeftPositionStress(newValue))) + '%'
            });

            $(content + " " + Config_Main_Object.SELECTORS.checkbox_inheritance).prop('checked', false);
        });

        //-------------------sự kiện click vào input text màu sắc của thuộc tính tô màu------------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyFillText).on('click', function () {
            var content = Config_Main_Object.getContent();
            $(content + " " + Config_Main_Object.SELECTORS.propertyFillColor).trigger('click');
        });

        //------------------sự kiện click ra ngoài input text màu sắc của thuộc tính tô màu----------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyFillText).on('focusout', function () {
            if ($(this).val().indexOf("#") >= 0) {
                var content = Config_Main_Object.getContent();
                $(content + " " + Config_Main_Object.SELECTORS.propertyFillText).val($(this).val());
                $(content + " " + Config_Main_Object.SELECTORS.propertyFillColor).val($(this).val());
                $(content + " " + Config_Main_Object.SELECTORS.propertyDivFill).css('background-color', $(this).val());
                $(content + " " + Config_Main_Object.SELECTORS.checkbox_inheritance).prop('checked', false);
            }
        });

        //------------------sự kiên click vào ô màu của thuộc tính tô màu------------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyDivFill).on('click', function () {
            var content = Config_Main_Object.getContent();
            $(content + " " + Config_Main_Object.SELECTORS.propertyFillColor).trigger('click');
        });

        //------------------sự kiện chọn màu trong bảng màu của thuộc tính tô màu-------------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyFillColor).on('input', function () {
            var content = Config_Main_Object.getContent();
            $(content + " " + Config_Main_Object.SELECTORS.propertyFillText).val($(this).val());
            $(content + " " + Config_Main_Object.SELECTORS.propertyDivFill).css('background-color', $(this).val());
            $(content + " " + Config_Main_Object.SELECTORS.checkbox_inheritance).prop('checked', false);
        });

        //------------------sự kiện thay đổi độ trong suốt của thuộc tính tô màu--------------------------------------------------------------
        $(Config_Main_Object.SELECTORS.propertyFillOpacity).on('input', function (e) {
            const newValue = parseInt(e.target.value);
            var content = Config_Main_Object.getContent();

            $(content + " " + Config_Main_Object.SELECTORS.thumbMax_fill).css('left', parseInt(calcLeftPositionStress(newValue)) + '%');
            $(content + " " + Config_Main_Object.SELECTORS.maxFill).html(newValue + '.00%');
            $(content + " " + Config_Main_Object.SELECTORS.lineFill).css({
                'right': (100 - parseInt(calcLeftPositionStress(newValue))) + '%'
            });
            $(content + " " + Config_Main_Object.SELECTORS.checkbox_inheritance).prop('checked', false);
        });

        //----------------su kien chon hinh------------------------------------------------

        $(Config_Main_Object.SELECTORS.choosen_image).on('change', function () {
            var url = this.value;
            var content = Config_Main_Object.getContent();
            $(this).parent().find('.lable-error').remove();

            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (this.files[0].size <= sizeFiles.sizeIcon) {
                if (this.files && this.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(content + " " + Config_Main_Object.SELECTORS.image_config).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
                else {
                    $(content + " " + Config_Main_Object.SELECTORS.image_config).attr('src', '/images/anhMacDinh.png');
                    $(this).parent().append(`<label class="col-md-12 lable-error text-danger mt-2">${jQuery.validator.format(l("Layer:ValidFormat"), l('Icon'))}</label>`);
                }
            }
            else {
                $(this).parent().append(`<label class="col-md-12 lable-error text-danger mt-2">${l('ManagementLayer:ValidSizeFileIcon')}</label>`);
            }
        });

        // sự kiện check kế thừa
        $(Config_Main_Object.SELECTORS.checkbox_implement).on('change', function () {
            var content = Config_Main_Object.getContent();
            if ($(this).is(":checked")) {
                Config_Main_Object.showDefaultProperties();
            }
        });
    },

    //-------------------lấy value thuộc tính cấu hình---------------------------------------------------------
    getValueProperties: function () {
        var content = Config_Main_Object.getContent();
        var obj = {
            image2D: null,
            stroke: $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeText).val(),
            strokeWidth: $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeWidth).val(),
            strokeOpacity: $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeOpacity).val() / 100,
            styleStroke: null,
            fill: $(content + " " + Config_Main_Object.SELECTORS.propertyFillText).val(),
            fillOpacity: $(content + " " + Config_Main_Object.SELECTORS.propertyFillOpacity).val() / 100
        }

        if (typeof $(content + " " + Config_Main_Object.SELECTORS.choosen_image).val() !== "undefined" && $(content + " " + Config_Main_Object.SELECTORS.choosen_image).val() != "") {

            var upload = mapdata.UploadFileImage(content + " " + Config_Main_Object.SELECTORS.choosen_image);

            if (upload.check) {
                obj.image2D = upload.url;
            }
        }


        return obj;
    },

    setValueProperties: function (data, content) {
        if (content == "" || content == undefined) {
            content = Config_Main_Object.getContent();
        }
        ///show info modal create Object
        $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeText).val(data.stroke); // set input text màu mặc định
        $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeColor).val(data.stroke); // set value màu mặc định trong bảng màu
        $(content + " " + Config_Main_Object.SELECTORS.propertyDivStroke).css('background-color', data.stroke); // hiển thị màu mặc định

        $(Config_Main_Object.SELECTORS.propertyStrokeWidth).val(data.strokeWidth); // set độ rộng mặc định

        const calcLeftPositionStress = value => 100 / (100 - 0) * (value - 0);

        let strokeOpacity = data.strokeOpacity * 100;

        //------------set value input đột trong suốt mặc định---------------------------------------------------------
        $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeOpacity).val(strokeOpacity);
        $(content + " " + Config_Main_Object.SELECTORS.thumbMax_stroke).css('left', parseInt(calcLeftPositionStress(strokeOpacity)) + '%');
        $(content + " " + Config_Main_Object.SELECTORS.maxStroke).html(strokeOpacity + '.00%');
        $(content + " " + Config_Main_Object.SELECTORS.lineStroke).css({
            'right': (100 - parseInt(calcLeftPositionStress(strokeOpacity))) + '%'
        });

        //--------------thuộc tính tô màu-------------------------------------------------------
        $(content + " " + Config_Main_Object.SELECTORS.propertyFillText).val(data.fill);
        $(content + " " + Config_Main_Object.SELECTORS.propertyFillColor).val(data.fill);
        $(content + " " + Config_Main_Object.SELECTORS.propertyDivFill).css('background-color', data.fill);

        let fillOpacity = data.fillOpacity * 100;
        $(content + " " + Config_Main_Object.SELECTORS.propertyFillOpacity).val(fillOpacity);
        $(content + " " + Config_Main_Object.SELECTORS.thumbMax_fill).css('left', parseInt(calcLeftPositionStress(fillOpacity)) + '%');
        $(content + " " + Config_Main_Object.SELECTORS.maxFill).html(fillOpacity + '.00%');
        $(content + " " + Config_Main_Object.SELECTORS.lineFill).css({
            'right': (100 - parseInt(calcLeftPositionStress(fillOpacity))) + '%'
        });

    },
    //-------------------------hiển thị mặc định thuộc tính cấu hình---------------------------------------------
    showDefaultProperties: function (content) {
        if (content == "" || content == undefined) {
            content = Config_Main_Object.getContent();
        }
        ///show info modal create Object
        $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeText).val(mapdata.GLOBAL.propertiesSetting.Stroke); // set input text màu mặc định
        $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeColor).val(mapdata.GLOBAL.propertiesSetting.Stroke); // set value màu mặc định trong bảng màu
        $(content + " " + Config_Main_Object.SELECTORS.propertyDivStroke).css('background-color', mapdata.GLOBAL.propertiesSetting.Stroke); // hiển thị màu mặc định

        $(Config_Main_Object.SELECTORS.propertyStrokeWidth).val(mapdata.GLOBAL.propertiesSetting.StrokeWidth); // set độ rộng mặc định

        const calcLeftPositionStress = value => 100 / (100 - 0) * (value - 0);

        let strokeOpacity = mapdata.GLOBAL.propertiesSetting.StrokeOpacity * 100;

        //------------set value input đột trong suốt mặc định---------------------------------------------------------
        $(content + " " + Config_Main_Object.SELECTORS.propertyStrokeOpacity).val(strokeOpacity);
        $(content + " " + Config_Main_Object.SELECTORS.thumbMax_stroke).css('left', parseInt(calcLeftPositionStress(strokeOpacity)) + '%');
        $(content + " " + Config_Main_Object.SELECTORS.maxStroke).html(strokeOpacity + '.00%');
        $(content + " " + Config_Main_Object.SELECTORS.lineStroke).css({
            'right': (100 - parseInt(calcLeftPositionStress(strokeOpacity))) + '%'
        });

        //--------------thuộc tính tô màu-------------------------------------------------------
        $(content + " " + Config_Main_Object.SELECTORS.propertyFillText).val(mapdata.GLOBAL.propertiesSetting.Fill);
        $(content + " " + Config_Main_Object.SELECTORS.propertyFillColor).val(mapdata.GLOBAL.propertiesSetting.Fill);
        $(content + " " + Config_Main_Object.SELECTORS.propertyDivFill).css('background-color', mapdata.GLOBAL.propertiesSetting.Fill);

        let fillOpacity = mapdata.GLOBAL.propertiesSetting.FillOpacity * 100;
        $(content + " " + Config_Main_Object.SELECTORS.propertyFillOpacity).val(fillOpacity);
        $(content + " " + Config_Main_Object.SELECTORS.thumbMax_fill).css('left', parseInt(calcLeftPositionStress(fillOpacity)) + '%');
        $(content + " " + Config_Main_Object.SELECTORS.maxFill).html(fillOpacity + '.00%');
        $(content + " " + Config_Main_Object.SELECTORS.lineFill).css({
            'right': (100 - parseInt(calcLeftPositionStress(fillOpacity))) + '%'
        });


    },

    //--------lấy content-----------------------------------------------------
    getContent: function () {
        if ($(Config_Main_Object.SELECTORS.content_create).hasClass('active')) {
            return Config_Main_Object.SELECTORS.content_create;
        }

        if ($(Config_Main_Object.SELECTORS.content_info).hasClass('active')) {
            return Config_Main_Object.SELECTORS.content_info;
        }

        if ($(Config_Main_Object.SELECTORS.content_edit).hasClass('active')) {
            return Config_Main_Object.SELECTORS.content_edit;
        }

        if ($(Config_Main_Object.SELECTORS.modelPropertiesImport).hasClass('active')) {
            return Config_Main_Object.SELECTORS.modelPropertiesImport;
        }

        return "";
    },
    //--------Check kế thừa-----------------------------------------------------
    checkImplementData: function (data) {
        if (data.fill !== mapdata.GLOBAL.propertiesSetting.Fill || data.fillOpacity !== mapdata.GLOBAL.propertiesSetting.FillOpacity
            || data.stroke !== mapdata.GLOBAL.propertiesSetting.Stroke || data.strokeOpacity !== mapdata.GLOBAL.propertiesSetting.StrokeOpacity
            || data.strokeWidth !== mapdata.GLOBAL.propertiesSetting.StrokeWidth) {
            return false;
        } else {
            return true;
        }
    }
}