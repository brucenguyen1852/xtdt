﻿var Info_Properties_Main_Object = {
    GLOBAL: {
        step: ''
    },
    CONSTS: {
    },
    SELECTORS: {
        info_modal: "#InforData",
        create_modal: "#myModal2",
        edit_form: "#EditForm",

        menu_left_sidebar_info: ".li-modal-data-right", // menu trái (tab2d, 3d, config, info,...)
        content_tab_section: ".section-info", // content(2d, 3d, config, info,...)

        title_info: ".Title-info",
        content_info: ".Content-info",
        info_detail: ".infor-detail",
        infor_basic: ".infor-basic",

        checkbox_config: ".checkbox-inheritance",
        image_icon_config: ".img-icon-2d-main-object",

        li_step_extend: 'li[data-li="step-extend"]',
        li_tab_step_extend: ".form-step-extend ul li",
        wardDistrict: ".select-WardDistrict",
        district: ".select-District",
    },
    init: function () {
        this.stepEvent();
    },
    stepEvent: function () {
        // tab menu bên trái content info data, editfom
        $(Info_Properties_Main_Object.SELECTORS.menu_left_sidebar_info).on('click', function () {

            var content = Info_Properties_Main_Object.getContent(); // lấy content hiện tại

            let step = $(this).attr("data-li");
            Info_Properties_Main_Object.GLOBAL.step = step;
            $(content + " " + Info_Properties_Main_Object.SELECTORS.menu_left_sidebar_info).removeClass("active");
            $(this).addClass("active");

            var obj = mapdata.GLOBAL.features.find(x => x._id === mapdata.GLOBAL.idMainObject);
            $(content + " " + Info_Properties_Main_Object.SELECTORS.content_tab_section).css('display', 'none'); // ẩn toàn bộ tab

            //model3D.RemoveBulding3D(); // xóa 3d hiện đang vẽ
            model3D.resetDraw();

            switch (step) {
                case "step-info":
                    Info_Properties_Main_Object.ResetIcon();
                    $('.step-info').css('display', 'block');
                    $(this).find("a").html(`<svg id="Group_2087" data-name="Group 2087" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path id="Path_44" data-name="Path 44" d="M0,0H24V24H0Z" fill="none"/>
                                                <path id="Path_45" data-name="Path 45" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22ZM11,11v6h2V11Zm0-4V9h2V7Z" fill="var(--primary)"/>
                                            </svg>`);
                    break;
                case "step-config":
                    Info_Properties_Main_Object.ResetIcon();
                    $('.step-config').css('display', 'block');
                    $(this).find("a").html(`<svg id="Component_183_1" data-name="Component 183 – 1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path id="Path_6394" data-name="Path 6394" d="M0,0H24V24H0Z" fill="none"/>
                                                <path id="Path_6395" data-name="Path 6395" d="M5.334,4.545A9.99,9.99,0,0,1,8.876,2.5a4,4,0,0,0,6.248,0,9.99,9.99,0,0,1,3.542,2.048,4,4,0,0,0,3.125,5.409,10.043,10.043,0,0,1,0,4.09,4,4,0,0,0-3.125,5.41A9.99,9.99,0,0,1,15.124,21.5a4,4,0,0,0-6.248,0,9.99,9.99,0,0,1-3.542-2.047,4,4,0,0,0-3.125-5.409,10.043,10.043,0,0,1,0-4.091A4,4,0,0,0,5.334,4.546ZM13.5,14.6a3,3,0,1,0-4.1-1.1,3,3,0,0,0,4.1,1.1Z" fill="var(--primary)"/>
                                            </svg>`);
                    break;
                case "step-3D":
                    Info_Properties_Main_Object.ResetIcon();
                    $('.step-3D').css('display', 'block');
                    //model3D.resetAll();
                    model3D.SetInforOptionModel3D(mapdata.GLOBAL.mainObjectSelected.object3D);
                    model3D.MoveToBuilding3D();
                    //$(model3D.SELECTORS.paraSelected).val($(model3D.SELECTORS.paraSelected + " option:first").val()).trigger("change"); // set lại trạng thái mặc định của step 3d
                    break;
                case "step-extend":
                    Info_Properties_Main_Object.ResetIcon();
                    $(mapdata.SELECTORS.btnEdited).trigger('click');

                    $(content + " " + '.step-extend .nav-item').each(function () {
                        if ($(this).css('display') != "none") {
                            $(this).find('.nav-link').click();
                            return false;
                        }
                    })

                    modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = true;

                    $('.step-extend').css('display', 'block');
                    $(this).find("a").html(`<svg id="Component_184_1" data-name="Component 184 – 1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 18">
                                                <path id="Path_456" data-name="Path 456" d="M21,3a1,1,0,0,1,1,1v7H20V5H4V19h6v2H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3Zm0,10a1,1,0,0,1,1,1v6a1,1,0,0,1-1,1H13a1,1,0,0,1-1-1V14a1,1,0,0,1,1-1ZM11.5,7,9.457,9.043l2.25,2.25-1.414,1.414-2.25-2.25L6,12.5V7Z" transform="translate(-2 -3)" fill="var(--primary)"/>
                                            </svg>`);
                    break;
                case "step-2D":
                    Info_Properties_Main_Object.ResetIcon();
                    $('.step-2D').css('display', 'block');
                    map.enable3dMode(false);
                    break;
                case "step-action":
                    Info_Properties_Main_Object.ResetIcon();
                    $('.step-action').css('display', 'block');
                    $(this).find("a").html(`<svg id="Group_2849" data-name="Group 2849" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                              <path id="Path_6517" data-name="Path 6517" d="M0,0H24V24H0Z" fill="none"/>
                                              <path id="Path_6518" data-name="Path 6518" d="M16.626,3.132,9.29,10.466,9.3,14.713l4.238-.007,7.331-7.332a9.991,9.991,0,1,1-4.241-4.242ZM20.486,2.1,21.9,3.515l-9.192,9.192-1.412,0,0-1.417L20.485,2.1Z" fill="var(--primary)"/>
                                            </svg>`);
                    break;
                default:
            }

            //if (content != Info_Properties_Main_Object.SELECTORS.edit_form) {
            //    if (obj != null && obj != undefined) {
            //        if (!map.is3dMode()) {
            //            mapdata.highlightObjectMenuLeft(obj);
            //        }
            //    }

            //    if (obj != null && obj != undefined) {
            //        if (!map.is3dMode()) {
            //            mapdata.highlightObjectMenuLeft(obj);
            //        }
            //    }
            //}
            $(documentObject.SELECTORS.divBorderFile).removeClass("file-active");
        });
    },
    // set value thuộc tính của main object (2d, 3d, cấu hình,...)
    setDefaultValue: function (data, content) {
        if (content == "" || content == undefined) {
            content = Info_Properties_Main_Object.getContent();
        }

        $(content + " " + Info_Properties_Main_Object.SELECTORS.title_info).html(''); // reset title
        $(content + " " + Info_Properties_Main_Object.SELECTORS.content_info).html(''); // reset conteninfo
        $(content + " " + Info_Properties_Main_Object.SELECTORS.info_detail).html(''); // reset info detail

        modalCreateFolderLayer.resetTableData(); // reset data phần mở rộng
        // set value thuộc tính cấu hình
        mapdata.showHideViewProperty(true);
        if (mapdata.GLOBAL.mainObjectSelected.properties != null) {
            Config_Main_Object.setValueProperties(mapdata.GLOBAL.mainObjectSelected.properties, content); // set value thuộc tính cấu hình
        }
        else {
            Config_Main_Object.showDefaultProperties(content); // set value thuộc tính defaul cấu hình
            mapdata.GLOBAL.mainObjectSelected.properties = {
                minZoom: mapdata.GLOBAL.propertiesSetting.MinZoom,
                maxZoom: mapdata.GLOBAL.propertiesSetting.MaxZoom,
                image2D: mapdata.GLOBAL.propertiesSetting.Image2D,
                stroke: mapdata.GLOBAL.propertiesSetting.Stroke,
                strokeWidth: mapdata.GLOBAL.propertiesSetting.StrokeWidth,
                strokeOpacity: mapdata.GLOBAL.propertiesSetting.StrokeOpacity,
                styleStroke: mapdata.GLOBAL.propertiesSetting.StyleStroke,
                fill: mapdata.GLOBAL.propertiesSetting.Fill,
                fillOpacity: mapdata.GLOBAL.propertiesSetting.FillOpacity,
            };
        }

        if (mapdata.GLOBAL.mainObjectSelected.isCheckImplementProperties && Config_Main_Object.checkImplementData(mapdata.GLOBAL.mainObjectSelected.properties)) {
            $(content + " " + Info_Properties_Main_Object.SELECTORS.checkbox_config).prop("checked", true);
            Config_Main_Object.showDefaultProperties();
        } else {
            $(content + " " + Info_Properties_Main_Object.SELECTORS.checkbox_config).prop("checked", false);
        }

        if (mapdata.GLOBAL.mainObjectSelected.properties != null && mapdata.GLOBAL.mainObjectSelected.properties != undefined) {
            var urlImage = mapdata.GLOBAL.mainObjectSelected.properties.image2D;
            if (urlImage == "" || urlImage == null) {
                urlImage = location.origin + "/images/anhMacDinh.png";
            }
        }
        else {
            urlImage = location.origin + "/images/anhMacDinh.png";
        }

        $(content + " " + Info_Properties_Main_Object.SELECTORS.image_icon_config).attr('src', urlImage);

        $('li[data-li="step-extend"]').css('display', 'none'); // ẩn tab mở rộng

        mapdata.GLOBAL.mainObjectSelected.propertiesGeojson = mapdata.GLOBAL.mainObjectSelected.properties;
        if (mapdata.GLOBAL.mainObjectSelected.object3D != null) {
            var objectexample = mapdata.GLOBAL.list.find(x => x.objUrl == mapdata.GLOBAL.mainObjectSelected.object3D.objectUrl && x.textureUrl == mapdata.GLOBAL.mainObjectSelected.object3D.texture);

            //var oldBuilding = model3D.GLOBAL.building; // building info trước đó

            $(mapdata.SELECTORS.selectParadigmId).val('').trigger('change'); // reset 3d

            //if (oldBuilding != null) // set lại building trước đó lên bản đồ
            //{
            //    oldBuilding.setMap(map);
            //}
            if (objectexample != undefined) {
                var object3dInfo = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.mainObjectSelected.id);
                if (object3dInfo != undefined && object3dInfo.object != null) // set building info hiện tại lên bản đồ
                {
                    model3D.GLOBAL.building = object3dInfo.object;
                    model3D.GLOBAL.building.setMap(map);
                }
            }
            if (mapdata.GLOBAL.mainObjectSelected.object3D.type == "para-choosenMap") {
                $(mapdata.SELECTORS.paraSelected).val(mapdata.GLOBAL.mainObjectSelected.object3D.type).trigger('change');
                $(model3D.SELECTORS.radioChoosenMap + '[value="SelectObject"]').trigger('click');
                $(model3D.SELECTORS.selectObjectChosenMap).val(mapdata.GLOBAL.mainObjectSelected.object3D.id).trigger('change');
            }
        } else {
            $(mapdata.SELECTORS.selectParadigmId).val('').trigger('change');
        }
    },

    //---------------hiển thị những thuộc tính input------------------------------------
    getDefaultProperties: function (data, content) {
        Info_Properties_Main_Object.resetContent();
        if (content == "" || content == undefined) {
            content = Info_Properties_Main_Object.getContent();
        }
        var result = [];
        if (mapdata.GLOBAL.lstPropertiesDirectory.length == 0) {
            mapdata.GLOBAL.lstPropertiesDirectory = data.result.listProperties;
        }

        $(content + " " + Info_Properties_Main_Object.SELECTORS.info_detail).html('');
        $(content + " " + Info_Properties_Main_Object.SELECTORS.infor_basic).html('');

        var lstBasic = mapdata.GLOBAL.lstPropertiesDirectory.filter(x => x.typeSystem == 2); // danh sách thuộc tính cơ bản
        var checkProperty = mapdata.GLOBAL.mainObjectSelected.district != null && mapdata.GLOBAL.mainObjectSelected.district != "" ? true : false;
        var typeValue = typeof (mapdata.GLOBAL.mainObjectSelected.district) == "string" ? "newPolygonByCountry" : "newMultipolygonByCountry";
        if (checkProperty) {
            mapdata.RenderHTMLForNewPolyGon('District', typeValue, "Edit");

            mapdata.RenderHTMLForNewPolyGonOfWardDistrict('', typeValue, "Edit");
            
        }

        $.each(lstBasic, function (i, obj) {
            var element = {
                NameProperties: obj.nameProperties,
                CodeProperties: obj.codeProperties,
                TypeProperties: obj.typeProperties,
                IsShow: obj.isShow,
                IsIndexing: obj.isIndexing,
                IsRequest: obj.isRequest,
                IsView: obj.isView,
                IsHistory: obj.isHistory,
                IsInheritance: obj.isInheritance,
                IsAsync: obj.isAsync,
                IsShowExploit: obj.isShowExploit,
                DefalutValue: obj.defalutValue,
                ShortDescription: obj.shortDescription,
                TypeSystem: obj.typeSystem,
                OrderProperties: obj.orderProperties
            };
            result.push(element);

            if (element.TypeProperties != "geojson" && element.TypeProperties != "image"
                && element.TypeProperties != "link" && element.TypeProperties != "maptile") {
                var text = mapdata.selectInputViewInforObj(element, content.split('#')[1]);
                $(content + " " + Info_Properties_Main_Object.SELECTORS.infor_basic).append(text);

                if (element.TypeProperties == "date") {
                    $(content + " " + '#text-modal-' + element.CodeProperties).datetimepicker({
                        locale: 'vi',
                        format: 'DD/MM/YYYY',
                        useCurrent: false,
                        //defaultDate: moment(),
                    }).on('dp.change', function (e) {
                        $(e.currentTarget).parents(".form-group-modal").find(".label-error").remove();
                        $(e.currentTarget).parents(".form-group-modal").removeClass("has-error");
                    });
                } else if (element.TypeProperties == "text" || element.TypeProperties == "stringlarge") {
                    setTimeout(function () {
                        $(content + " " + '#text-modal-' + element.CodeProperties).trumbowyg({
                            svgPath: '/libs/bootstrap-trumbowyg/icons.svg',
                            btns: [
                                ['viewHTML'],
                                ['undo', 'redo'], // Only supported in Blink browsers
                                ['formatting'],
                                ['strong', 'em', 'del'],
                                ['superscript', 'subscript'],
                                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                                ['unorderedList', 'orderedList'],
                                ['horizontalRule'],
                                ['removeformat'],
                                ['fullscreen']
                            ]
                        }).on('tbwchange ', function (e) {
                            $(content + " " + '#text-modal-' + element.CodeProperties).trigger('change');
                            if ($(content + " " + '#text-modal-' + element.CodeProperties).val() != "") {
                                $(this).parents('.form-group-modal').removeClass('has-error');
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).addClass("textarea");
                                let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 78}px)`);
                            }
                        }).on('tbwfocus', function () {
                            $($(this).parent().next()).addClass("textarea");
                            let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                            $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 78}px)`);
                            $($(content + " " + '#text-modal-' + element.CodeProperties).parent().parent()).addClass('div-textarea-parent-focus');
                        }).on('tbwblur', function () {
                            if ($(content + " " + '#text-modal-' + element.CodeProperties).val() != "") {
                                $($(this).parent().next()).addClass("textarea");
                                let height = $('#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 78}px)`);
                            } else {
                                $($(this).parent().next()).removeClass("textarea");
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);
                            }
                            $($(content + " " + '#text-modal-' + element.CodeProperties).parent().parent()).removeClass('div-textarea-parent-focus');
                        }).on('tbwresize', function () {
                            let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                            $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);
                        });

                        let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                        //$($('#text-modal-' + element.CodeProperties).parent().next()).css('top', (height + 20) + 'px');
                        $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).attr('style', `top: ${(height + 20)}px !important`);
                        $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);

                        $(content + " " + '#text-modal-' + element.CodeProperties).trigger('tbwchange');
                    }, 200);
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------------------------

        var lstInfo = mapdata.GLOBAL.lstPropertiesDirectory.filter(x => x.typeSystem != 1 && x.typeSystem != 2); // danh sách thuộc tính layer chi tiết
        $(mapdata.SELECTORS.extend_tab).css("display", "none"); // ẩn toàn bộ tab mở rộng

        $(content + " " + Info_Properties_Main_Object.SELECTORS.li_step_extend).css('display', 'none');
        $(content + " " + Info_Properties_Main_Object.SELECTORS.li_tab_step_extend).css('display', 'none');
        $.each(lstInfo, function (i, obj) {
            if (obj.typeSystem !== 1) {
                var element = {
                    NameProperties: obj.nameProperties,
                    CodeProperties: obj.codeProperties,
                    TypeProperties: obj.typeProperties,
                    IsShow: obj.isShow,
                    IsIndexing: obj.isIndexing,
                    IsRequest: obj.isRequest,
                    IsView: obj.isView,
                    IsHistory: obj.isHistory,
                    IsInheritance: obj.isInheritance,
                    IsAsync: obj.isAsync,
                    IsShowExploit: obj.isShowExploit,
                    DefalutValue: obj.defalutValue,
                    ShortDescription: obj.shortDescription,
                    TypeSystem: obj.typeSystem,
                    OrderProperties: obj.orderProperties
                };
                result.push(element);

                if (obj.typeProperties == "link") {
                    $(content + " " + Info_Properties_Main_Object.SELECTORS.li_step_extend).css('display', 'block'); // hiển thị tab mở rộng
                    $(content + " " + Info_Properties_Main_Object.SELECTORS.li_tab_step_extend).eq(1).show(); // hiển thị tab link trong content mở rộng
                    return;
                }
                if (obj.typeProperties == "image") {
                    $(content + " " + Info_Properties_Main_Object.SELECTORS.li_step_extend).css('display', 'block'); // hiển thị tab mở rộng
                    $(content + " " + Info_Properties_Main_Object.SELECTORS.li_tab_step_extend).eq(0).show(); // hiển thị tab hình ảnh trong content mở rộng
                    return;
                }
                if (obj.typeProperties == "file") {
                    $(content + " " + Info_Properties_Main_Object.SELECTORS.li_step_extend).css('display', 'block'); // hiển thị tab mở rộng
                    $(content + " " + Info_Properties_Main_Object.SELECTORS.li_tab_step_extend).eq(2).show(); // hiển thị tab file trong content mở rộng
                    return;
                }

                if (element.TypeProperties != "geojson" && element.TypeProperties != "image"
                    && element.TypeProperties != "link" && element.TypeProperties != "maptile") {
                    var text = mapdata.selectInputViewInforObj(element, content.split('#')[1]);
                    $(content + " " + Info_Properties_Main_Object.SELECTORS.info_detail).append(text);
                    if (element.TypeProperties == "date") {
                        $(content + " " + '#text-modal-' + element.CodeProperties).datetimepicker({
                            locale: 'vi',
                            format: 'DD/MM/YYYY',
                            useCurrent: false,
                            //defaultDate: moment()
                        }).on('dp.change', function (e) {
                            $(e.currentTarget).parents(".form-group-modal").find(".label-error").remove();
                            $(e.currentTarget).parents(".form-group-modal").removeClass("has-error");
                            if ($(this).val() == "") {
                                $(this).parent().find('.placeholder').removeClass('focus');
                            }
                        }).on('dp.hide', function () {
                            if ($(this).val() == "") {
                                $(this).parent().find('.placeholder').removeClass('focus');
                            }
                        }).on('dp.show', function () {
                            $(this).parent().find('.placeholder').addClass('focus');
                        });
                    } else if (element.TypeProperties == "text" || element.TypeProperties == "stringlarge") {
                        setTimeout(function () {
                            $(content + " " + '#text-modal-' + element.CodeProperties).trumbowyg({
                                svgPath: '/libs/bootstrap-trumbowyg/icons.svg',
                                btns: [
                                    ['viewHTML'],
                                    ['undo', 'redo'], // Only supported in Blink browsers
                                    ['formatting'],
                                    ['strong', 'em', 'del'],
                                    ['superscript', 'subscript'],
                                    ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                                    ['unorderedList', 'orderedList'],
                                    ['horizontalRule'],
                                    ['removeformat'],
                                    ['fullscreen']
                                ]
                            }).on('tbwchange ', function (e) {
                                $(content + " " + '#text-modal-' + element.CodeProperties).trigger('change');
                                if ($(content + " " + '#text-modal-' + element.CodeProperties).val() != "") {
                                    $(this).parents('.form-group-modal').removeClass('has-error');
                                    $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).addClass("textarea");
                                    let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                    $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 78}px)`);
                                }
                            }).on('tbwfocus', function () {
                                $($(this).parent().next()).addClass("textarea");
                                let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 78}px)`);
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().parent()).addClass('div-textarea-parent-focus');
                            }).on('tbwblur', function () {
                                if ($(content + " " + '#text-modal-' + element.CodeProperties).val() != "") {
                                    $($(this).parent().next()).addClass("textarea");
                                    let height = $('#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                    $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(0.8) translateY(-${height + 78}px)`);
                                } else {
                                    $($(this).parent().next()).removeClass("textarea");
                                    $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);
                                }
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().parent()).removeClass('div-textarea-parent-focus');
                            }).on('tbwresize', function () {
                                let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                                $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);
                            });

                            let height = $(content + " " + '#text-modal-' + element.CodeProperties).parent().find(".trumbowyg-button-pane").height();
                            //$($('#text-modal-' + element.CodeProperties).parent().next()).css('top', (height + 20) + 'px');
                            $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).attr('style', `top: ${(height + 20)}px !important`);
                            $($(content + " " + '#text-modal-' + element.CodeProperties).parent().next()).css('transform', `scale(1) translateY(10px)`);

                            $(content + " " + '#text-modal-' + element.CodeProperties).trigger('tbwchange');
                        }, 200);
                    }
                    else if (element.TypeProperties == "list") {
                        $(content + " " + '#text-modal-' + element.CodeProperties).select2()
                            .on("select2:opening", function () {
                                $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                            })
                            .on("select2:closing", function () {
                                var value = $(this).val();
                                if (value != "" && value != null) {
                                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                }
                                else {
                                    $(this).parent().find('.placeholder').attr('style', "");
                                }
                            })
                            .on('select2:select', function () {
                                var value = $(this).val();
                                if (value != "" && value != null) {
                                    $(this).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                                    $(this).trigger('change');
                                }
                            });

                        $(content + " " + '#text-modal-' + element.CodeProperties).parent().find('.placeholder').attr('style', " transform: scale(0.8) translateY(-24px);background: #fff;");
                    }
                }
            }
        });

        mapdata.GLOBAL.lstProperties = result;
    },

    //---------------set value cho từng thuộc tính vào form input----------------------------------
    setValueObject: function (content) {
        if (content == "" || content == undefined) {
            content = Info_Properties_Main_Object.getContent();
        }
        
        let lstproperties = mapdata.GLOBAL.mainObjectSelected.listProperties;
        $(Info_Properties_Main_Object.SELECTORS.district).val(mapdata.GLOBAL.mainObjectSelected.district).trigger("select2:select");
        $(Info_Properties_Main_Object.SELECTORS.wardDistrict).val(mapdata.GLOBAL.mainObjectSelected.wardDistrict).trigger("select2:select");
        // hiển thị value input đói tượng
        $.each(lstproperties, function (i, obj) {
            //if (obj.defalutValue != "" && obj.defalutValue != "null") {
            //    $(content + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue);
            //}
            let lstvalue;
            switch (obj.typeProperties) {
                case "list":
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);
                        if (lstvalue != undefined && $.isArray(lstvalue)) {
                            let value = lstvalue.find(x => x.checked);
                            if (value != null && value != undefined) {
                                $(content + ' #text-modal-' + obj.codeProperties).val(value.code).trigger("select2:select");
                            }
                        }
                        else {
                            $(content + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue).trigger("select2:select");
                        }
                    }
                    catch (e) {
                        if (obj.defalutValue != "" && obj.defalutValue != "null") {
                            $(content + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue).trigger("select2:select");
                        }
                        else {
                            $(content + ' #text-modal-' + obj.codeProperties).val($(content + ' #text-modal-' + obj.codeProperties + " option:first").val()).trigger("select2:select");
                        }

                        if ($(content + ' #text-modal-' + obj.codeProperties).val() == "" || $(content + ' #text-modal-' + obj.codeProperties).val() == null || $(content + ' #text-modal-' + obj.codeProperties).val() == "null") {
                            $(content + ' #text-modal-' + obj.codeProperties).val($(content + ' #text-modal-' + obj.codeProperties + " option:first").val());
                        }
                    }
                    break;
                case "radiobutton":
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);
                        if (lstvalue != undefined && $.isArray(lstvalue)) {
                            let value = lstvalue.find(x => x.checked);
                            if (value != null && value != undefined) {
                                $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + value.code + '"]').prop("checked", true);
                            }
                        }
                        else {
                            $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                        }
                    }
                    catch (e) {
                        $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + obj.defalutValue + '"]').prop("checked", true);
                    }
                    break;
                case "checkbox":
                    try {
                        lstvalue = JSON.parse(obj.defalutValue);

                        if (lstvalue.length > 0 && $.isArray(lstvalue)) {
                            $.each(lstvalue, function (i, item) {
                                if (item.checked) {
                                    $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + item.code + '"]').prop("checked", true);
                                }
                            });
                        }
                        else {
                            var lstChecked = obj.defalutValue.split(",");
                            $.each(lstChecked, function (i, item) {
                                $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                            });
                        }
                    }
                    catch (e) {
                        if (obj.defalutValue != "" && obj.defalutValue != null) {
                            var lstChecked = obj.defalutValue.split(",");
                            $.each(lstChecked, function (i, item) {
                                $(content + ' .text-modal-' + obj.codeProperties + '[value = "' + item.trim() + '"]').prop("checked", true);
                            });
                        }
                        //$('#text-modal-' + obj.codeProperties).val(obj.defalutValue);
                    }
                    break;
                case "link":
                    modalCreateFolderLayer.GLOBAL.listObjectLinkCamera = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                    mapdata.GLOBAL.listObjectLinkCameraEdit = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                    break;
                case "image":
                    modalCreateFolderLayer.GLOBAL.listObjectImage = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                    mapdata.GLOBAL.listObjectImageEdit = obj.defalutValue !== "" && obj.defalutValue !== null ? JSON.parse(obj.defalutValue) : [];
                    break;
                case "file":
                    statusfile = true;
                    break
                case "bool":
                    if (obj.defalutValue != "false") {
                        var exp = content.split(' ');

                        $(content + ' #text-modal-' + obj.codeProperties + exp[0].replace('#', '')).prop('checked', true);
                    }
                    break;
                default:
                    $(content + ' #text-modal-' + obj.codeProperties).val(obj.defalutValue)
                    break;
            }
        });
    },

    //----------------get value propeties từ form nhập-----------------------------
    getValueFormPropeties: function (content) {
        if (content == "" || content == undefined) {
            content = Info_Properties_Main_Object.getContent();
        }

        var object = JSON.parse(JSON.stringify(mapdata.GLOBAL.mainObjectSelected));
        var dataGeometry = mapdata.convertDataToGeojson();

        if (dataGeometry !== null) {
            if (dataGeometry.features.length > 0) {
                object.geometry = dataGeometry.features[0].geometry;
            }
        }

        if ($(content + " " + Config_Main_Object.SELECTORS.choosen_image).val() != "") {
            if (object.propertiesGeojson.image2D != "") {
                var deleteImage = mapdata.DeleteFileImage(object.propertiesGeojson.image2D);
            }
        }

        var imageDefault = mapdata.GLOBAL.mainObjectSelected.properties.image2D;

        object.propertiesGeojson = Config_Main_Object.getValueProperties();

        if (object.propertiesGeojson.image2D == null) {
            object.propertiesGeojson.image2D = imageDefault;
        }

        var lstPropertiesValue = [];

        // hiển thị thông tin đối tượng
        $.each(mapdata.GLOBAL.lstProperties, function (i, obj) {
            var objectparam = {
                nameProperties: obj.NameProperties,
                codeProperties: obj.CodeProperties,
                typeProperties: obj.TypeProperties,
                isShow: obj.IsShow,
                isView: obj.IsView,
                defalutValue: $(content + ' ' + '#text-modal-' + obj.CodeProperties).val(),
                orderProperties: obj.OrderProperties,
                typeSystem: obj.TypeSystem
            };

            if (obj.TypeProperties == "link") {
                objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectLinkCamera);
            }
            if (obj.TypeProperties == "image") {
                objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectImage);
            }
            if (obj.TypeProperties == "file") {
                objectparam.defalutValue = "[]";
            }

            lstPropertiesValue.push(objectparam);
            if (obj.CodeProperties.toLowerCase() === "name") {
                object.nameObject = $(content + ' ' + '#text-modal-' + obj.CodeProperties).val();
            }
            if (obj.TypeProperties == "list") {
                let array = null;
                objectparam.defalutValue = $(content + ' ' + '#text-modal-' + obj.CodeProperties).val();
            }

            if (obj.TypeProperties == "checkbox") {
                let arraychecked = $(content + ' ' + ".text-modal-" + obj.CodeProperties + ":checked").map(function () {
                    return this.value;
                }).get();

                objectparam.defalutValue = arraychecked.toString();
            }
            if (obj.TypeProperties == "radiobutton") {
                let stringchecked = $(content + ' ' + ".text-modal-" + obj.CodeProperties + ":checked").val();
                objectparam.defalutValue = stringchecked;
            }

            if (obj.TypeProperties == "bool") {
                //obj.DefalutValue = $("#text-modal-" + obj.CodeProperties).prop("checked");
                objectparam.defalutValue = $(content + ' ' + "#text-modal-" + obj.CodeProperties + content.replace('#', '')).prop("checked").toString();
            }
        });

        object.listProperties = lstPropertiesValue;
        return object;
    },

    //---------------check form propeties form nhập-----------------------
    checkFormInput: function (content) {
        mapdata.clearLabelError();
        let check = true;

        let hasFocus = false;
        var litab = content.split(" "); // tab content

        $(content + " .modal-layer").each(function () {
            var type = $(this).attr("data-type");
            var status = $(this).attr("data-required").toLowerCase();
            if (status == "true") {
                switch (type) {
                    //case "list":
                    case "radio":
                    case "checkbox":
                        let inputName = $(this).attr('data-name');
                        var checkBoxRadio = false;
                        $(this).find('input').each(function (i, e) {
                            if ($(this).is(":checked")) {
                                checkBoxRadio = true;
                                return;
                            }
                        });

                        if (!checkBoxRadio) {
                            $(litab[0] + " .li-modal-data-right").eq(3).click(); // tabinfo, edit

                            $(litab[0] + " .title-li-tab .nav-link").eq(0).click();

                            check = checkBoxRadio;
                            insertError($(this), "other");
                            if (!hasFocus) {
                                hasFocus = true;
                                $(content).animate({
                                    scrollTop: $(content).scrollTop() - $(content).offset().top + $(this).offset().top
                                }, 50);
                            }

                            mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            return;
                        }
                        break;
                    default:
                        if ($(this).attr("multiple") != 'multiple') {
                            var typeconvert = mapdata.convertcheckTypemodal(type);
                            if (typeconvert != "") {
                                let inputTen = $(this).val();
                                let inputName = $(this).attr('data-name');

                                if (!validateText(inputTen, typeconvert, 0, 0)) {
                                    $(litab[0] + " .li-modal-data-right").eq(3).click(); // tabinfo, edit

                                    $(litab[0] + ".title-li-tab .nav-link").eq(0).click();

                                    if ((type == "text" || type == "stringlarge") && inputTen.trim() != "") {
                                        return true;
                                    }
                                    insertError($(this), "other");
                                    if (!hasFocus) {
                                        if (type == "text" || type == "stringlarge") {
                                            $(this).parent().find('.trumbowyg-editor').focus();
                                        }
                                        else {
                                            //console.log($(this).attr("id"));
                                            $(this).focus();
                                        }
                                        hasFocus = true;
                                    }
                                    check = false;
                                    mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));

                                    return;
                                }
                            }
                        }
                        else {
                            if ($(this).val().length == 0) {
                                check = false;
                                let inputName = $(this).attr('data-name');
                                mapdata.showLabelError(this, jQuery.validator.format(l("ManagementLayer:DataIsValid"), inputName));
                            }
                        }
                        break;
                }

            }
        });

        let checkHtml = mapdata.CheckedHtmlEntities(content);
        if (check) {
            check = checkHtml;
        }

        return check;
    },

    //--------lấy content-----------------------------------------------------
    getContent: function () {
        if ($(Info_Properties_Main_Object.SELECTORS.create_modal).hasClass('active')) {
            return Info_Properties_Main_Object.SELECTORS.create_modal;
        }

        if ($(Info_Properties_Main_Object.SELECTORS.info_modal).hasClass('active')) {
            return Info_Properties_Main_Object.SELECTORS.info_modal;
        }

        if ($(Info_Properties_Main_Object.SELECTORS.edit_form).hasClass('active')) {
            return Info_Properties_Main_Object.SELECTORS.edit_form;
        }

        return "";
    },

    resetContent: function () {
        $(Info_Properties_Main_Object.SELECTORS.info_detail).html('');
        $(modalCreateFolderLayer.SELECTORS.fileUploadInput).val(null);

    },
    setRisizeTextareaJson3d: function () {
        var content = Info_Properties_Main_Object.getContent();
        auto_risze_textarea($(content + " " + model3D.SELECTORS.inforGeojson));
    },

    //---------- check required file-----------------------------------------
    checkValidFile: function () {
        var content = Info_Properties_Main_Object.getContent();
        var litab = content.split(" "); // tab content

        var isCheckedFile = true;
        var message = [];
        var lstExtendProperties = mapdata.GLOBAL.lstProperties.filter(x => (x.TypeProperties == "link" || x.TypeProperties == "image" || x.TypeProperties == "file") && x.IsRequest);
        if (lstExtendProperties.length > 0) {
            $.each(lstExtendProperties, function (i, obj) {

                if (obj.TypeProperties == "link") {
                    if (modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length == 0) {
                        isCheckedFile = false;
                        message.push(l("link"));
                    }
                }
                if (obj.TypeProperties == "image") {
                    if (modalCreateFolderLayer.GLOBAL.listObjectImage.length == 0) {
                        isCheckedFile = false;
                        message.push(l("image"));
                    }
                }
                if (obj.TypeProperties == "file") {
                    if (modalCreateFolderLayer.GLOBAL.listObjectFile.length == 0) {
                        isCheckedFile = false;
                        message.push(l("file"));
                    }
                }
            });

            if (!isCheckedFile) {
                $(".nav-link[data-id='modalBodyExtend']").click();
                $(litab[0] + " .li-modal-data-right").eq(4).click(); // tabinfo, edit

                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("Dữ liệu mở rộng (" + message.toString() + ") không được bỏ trống"),
                    icon: "warning",
                    button: l("ManagementLayer:Close"),
                })
            }
        }

        return isCheckedFile;
    },
    ResetIcon: function () {
        $(Info_Properties_Main_Object.SELECTORS.menu_left_sidebar_info + " a").each(function () {
            var step = $(this).parent().attr("data-li");
            switch (step) {
                case "step-info":
                    //$(this).html(`<svg id="Group_2665" data-name="Group 2665" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    //                <path id="Path_6420" data-name="Path 6420" d="M0,0H24V24H0Z" fill="none" />
                    //                <path id="Path_6421" data-name="Path 6421" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-2a8,8,0,1,0-8-8A8,8,0,0,0,12,20ZM11,7h2V9H11Zm0,4h2v6H11Z" fill="rgba(0,0,0,0.87)" />
                    //              </svg>`);
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs></defs><path class="a" d="M0,0H24V24H0Z" /><path class="b" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-2a8,8,0,1,0-8-8A8,8,0,0,0,12,20ZM11,7h2V9H11Zm0,4h2v6H11Z" /></svg>`)
                    break;
                case "step-config":
                    //$(this).html(`<svg id="Group_2083" data-name="Group 2083" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    //                <path id="Path_32" data-name="Path 32" d="M0,0H24V24H0Z" fill="none" />
                    //                <path id="Path_33" data-name="Path 33" d="M2,12a10.036,10.036,0,0,1,.316-2.5A3,3,0,0,0,4.99,4.867a9.99,9.99,0,0,1,4.335-2.5,3,3,0,0,0,5.348,0,9.99,9.99,0,0,1,4.335,2.5A3,3,0,0,0,21.683,9.5a10.075,10.075,0,0,1,0,5.007,3,3,0,0,0-2.675,4.629,9.99,9.99,0,0,1-4.335,2.505,3,3,0,0,0-5.348,0A9.99,9.99,0,0,1,4.99,19.133,3,3,0,0,0,2.315,14.5,10.056,10.056,0,0,1,2,12Zm4.8,3a5,5,0,0,1,.564,3.524,8.007,8.007,0,0,0,1.3.75,5,5,0,0,1,6.67,0,8.007,8.007,0,0,0,1.3-.75,5,5,0,0,1,3.334-5.774,8.126,8.126,0,0,0,0-1.5,5,5,0,0,1-3.335-5.774,7.989,7.989,0,0,0-1.3-.75,5,5,0,0,1-6.669,0,7.99,7.99,0,0,0-1.3.75A5,5,0,0,1,4.034,11.25a8.126,8.126,0,0,0,0,1.5A4.993,4.993,0,0,1,6.805,15ZM12,15a3,3,0,1,1,3-3A3,3,0,0,1,12,15Zm0-2a1,1,0,1,0-1-1A1,1,0,0,0,12,13Z" fill="rgba(0,0,0,0.87)" />
                    //              </svg>`);
                    //$(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs></defs><path class="a" d="M0,0H24V24H0Z" /><path class="b" d="M5.334,4.545A9.99,9.99,0,0,1,8.876,2.5a4,4,0,0,0,6.248,0,9.99,9.99,0,0,1,3.542,2.048,4,4,0,0,0,3.125,5.409,10.043,10.043,0,0,1,0,4.09,4,4,0,0,0-3.125,5.41A9.99,9.99,0,0,1,15.124,21.5a4,4,0,0,0-6.248,0,9.99,9.99,0,0,1-3.542-2.047,4,4,0,0,0-3.125-5.409,10.043,10.043,0,0,1,0-4.091A4,4,0,0,0,5.334,4.546ZM13.5,14.6a3,3,0,1,0-4.1-1.1,3,3,0,0,0,4.1,1.1Z" /></svg>`)
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path id="Path_33" class="b" data-name="Path 33" d="M2,12a10.036,10.036,0,0,1,.316-2.5A3,3,0,0,0,4.99,4.867a9.99,9.99,0,0,1,4.335-2.5,3,3,0,0,0,5.348,0,9.99,9.99,0,0,1,4.335,2.5A3,3,0,0,0,21.683,9.5a10.075,10.075,0,0,1,0,5.007,3,3,0,0,0-2.675,4.629,9.99,9.99,0,0,1-4.335,2.505,3,3,0,0,0-5.348,0A9.99,9.99,0,0,1,4.99,19.133,3,3,0,0,0,2.315,14.5,10.056,10.056,0,0,1,2,12Zm4.8,3a5,5,0,0,1,.564,3.524,8.007,8.007,0,0,0,1.3.75,5,5,0,0,1,6.67,0,8.007,8.007,0,0,0,1.3-.75,5,5,0,0,1,3.334-5.774,8.126,8.126,0,0,0,0-1.5,5,5,0,0,1-3.335-5.774,7.989,7.989,0,0,0-1.3-.75,5,5,0,0,1-6.669,0,7.99,7.99,0,0,0-1.3.75A5,5,0,0,1,4.034,11.25a8.126,8.126,0,0,0,0,1.5A4.993,4.993,0,0,1,6.805,15ZM12,15a3,3,0,1,1,3-3A3,3,0,0,1,12,15Zm0-2a1,1,0,1,0-1-1A1,1,0,0,0,12,13Z" transform="translate(0)" fill="rgba(0,0,0,0.56)"></path>
                        </svg>`)
                    break;
                case "step-extend":
                    //$(this).html(`<svg id="Group_2779" data-name="Group 2779" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    //                <path id="Path_6437" data-name="Path 6437" d="M0,0H24V24H0Z" fill="none" />
                    //                <path id="Path_6438" data-name="Path 6438" d="M21,3a1,1,0,0,1,1,1v7H20V5H4V19h6v2H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3Zm0,10a1,1,0,0,1,1,1v6a1,1,0,0,1-1,1H13a1,1,0,0,1-1-1V14a1,1,0,0,1,1-1Zm-1,2H14v4h6ZM11.5,7,9.457,9.043l2.25,2.25-1.414,1.414-2.25-2.25L6,12.5V7Z" fill="rgba(0,0,0,0.87)" />
                    //              </svg>`);
                    //$(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs></defs><path class="a" d="M0,0H24V24H0Z" /><path class="b" d="M21,3a1,1,0,0,1,1,1v7H20V5H4V19h6v2H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3Zm0,10a1,1,0,0,1,1,1v6a1,1,0,0,1-1,1H13a1,1,0,0,1-1-1V14a1,1,0,0,1,1-1ZM11.5,7,9.457,9.043l2.25,2.25-1.414,1.414-2.25-2.25L6,12.5V7Z" /></svg>`)
                    $(this).html(`<svg id="Group_2779" data-name="Group 2779" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_6437" data-name="Path 6437" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_6438" class="b" data-name="Path 6438" d="M21,3a1,1,0,0,1,1,1v7H20V5H4V19h6v2H3a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3Zm0,10a1,1,0,0,1,1,1v6a1,1,0,0,1-1,1H13a1,1,0,0,1-1-1V14a1,1,0,0,1,1-1Zm-1,2H14v4h6ZM11.5,7,9.457,9.043l2.25,2.25-1.414,1.414-2.25-2.25L6,12.5V7Z" fill="rgba(0,0,0,0.87)"/>
                                    </svg>`);
                    break;
                case "step-action":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <defs>
                            </defs>
                            <path class="a" style="fill: none;" d="M0,0H24V24H0Z"></path>
                            <path class="b" style="fill: rgba(0,0,0,0.87);" d="M16.625,3.133l-1.5,1.5a7.992,7.992,0,1,0,4.242,4.242l1.5-1.5a10,10,0,1,1-4.242-4.242Zm1.739,1.089,1.414,1.414L12,13.414,10.586,12Z"></path>
                        </svg >`);
                    //$(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs></defs><path class="a" d="M0,0H24V24H0Z" /><path class="b" d="M18.328,4.258,10.586,12,12,13.414l7.742-7.742a10,10,0,1,1-1.414-1.414Z" /></svg>`)
                    break;
            }
        })
    }
}