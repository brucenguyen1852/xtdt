﻿var hexDigits = new Array
    ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");

var l = abp.localization.getResource('HTKT');

var DSObject =
{
    /*
        * Global Objects
     */
    GLOBAL:
    {
        table: "",
        textnotification: "",
        isDraw: false,
        objectClick: "",
        fileClick: "",
        changeAction: "",
        MessageActionFile: "",
        isOpenTable: false,
        idDirectory: null,
        sendAjax: null,
        checkLoadInt: false
    },

    /*
        * CONSTANTS  Objects
     */
    CONSTANTS:
    {
    },

    /*
        * SELECTORS OBJECT
     */
    SELECTORS:
    {
        listdata: "#listData",
        btn_open_table: ".open-table",
        btn_close_table: ".close-content-table",
        table_content: ".table-object",
        content_info_object: '.section-info',
        btn_toggle_info_content: ".toggle-detail-property",
        content_info_data: "#InforData",
        tableListData: "#listData tbody",
        btnInById: ".btn-in-by-id",
        btnInListPDF: ".btn-in-list-pdf",
        tableRow: "#listData tbody tr[role='row']",
        modal: "#exampleModal",
        btnCloseModal: ".btn-close-form-qr"
    },

    /*
        * Init function
     */
    init: function () {
        var url = new URL(window.location.href);
        DSObject.GLOBAL.idDirectory = url.searchParams.get("id");
        this.setDatatable();
        this.setUpEvent();
    },
    /*
       * Set up event
    */
    setUpEvent: function () {
        $(DSObject.SELECTORS.tableListData).on("click", DSObject.SELECTORS.btnInById, function (e) {
            e.stopPropagation();
            var btn_disable = this;
            $(btn_disable).prop("disabled", true);
            var id = $(this).data("id");
            var table = $("#listData").DataTable();
            var data = table.rows().data();
            var item = data.filter(x => x.id == id)[0];

            var qrCodeValue = JSON.stringify(DSObject.convertQRCode(item)).toString();
            if (item.qrCodeObject != "" && item.qrCodeObject != null && item.qrCodeObject != "null") {
                qrCodeValue = item.qrCodeObject.toString();
            }

            let obj = {
                Name: item.nameObject,
                QRCode: qrCodeValue
            };
            var arrayData = [];
            arrayData.push(obj);
            var dataPost = JSON.stringify(arrayData);
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: "application/json-patch+json",
                async: true,
                url: document.location.origin + "/api/HTKT/ManagementData/export-to-pdf",
                data: dataPost,
                beforeSend: function () {
                    var spinner = document.getElementsByClassName('spinner');
                    spinner[0].style.display = "block";
                },
                success: function (data) {
                    if (data.code == "ok") {
                        var local = window.location.origin;
                        var url = `${local}/FileQRCode/${data.result}`;
                        DSObject.Download(url, data.result);
                    }
                    else {
                        console.error(data);
                    }
                    $(btn_disable).prop("disabled", false);
                },
                complete: function () {
                    var spinner = document.getElementsByClassName('spinner');
                    spinner[0].style.display = "none";
                },
            });
        })
        // toggle content_table
        $(DSObject.SELECTORS.btn_open_table).on('click', function () {
            if (mapdata.GLOBAL.selectbtn !== "" && mapdata.GLOBAL.selectbtn !== "inPDF") {
                return;
            } else {
                mapdata.GLOBAL.selectbtn = "inPDF";
            }
            if ($(this).hasClass("disabled")) {
                return;
            }
            $('#exampleModal').modal('show');
            DSObject.GLOBAL.isOpenTable = DSObject.GLOBAL.isOpenTable == false ? true : false;
            //if (!$(DSObject.SELECTORS.table_content).hasClass('open')) {
            //    if ($(DSObject.SELECTORS.content_info_data).hasClass(`detail-property-collapse`)) {
            //        //$(DSObject.SELECTORS.table_content).addClass('w-100');
            //    }
            //    else {
            //        //$(DSObject.SELECTORS.table_content).removeClass('w-100');
            //    }

            //    $(DSObject.SELECTORS.table_content).addClass('open');
            //}
            //else {
            //    $(DSObject.SELECTORS.table_content).removeClass('open');
            //}

            //DSObject.GLOBAL.table.ajax.reload();

        });

        // STT table
        DSObject.GLOBAL.table.on('order.dt search.dt', function (data) {
            var pageInfo = DSObject.GLOBAL.table.page.info();
            var num = (pageInfo.length * pageInfo.page) + 1;
            DSObject.GLOBAL.table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = num++;
            });
        });

        $(DSObject.SELECTORS.btnCloseModal).on('click', function ()
        {
            mapdata.CancelAction();
        });

        // toggle sidebar
        $(`.wrapper`).on('click', '.sidebar-toggle', function () {
            setTimeout(function () {
                //DSObject.GLOBAL.table.ajax.reload();
                DSObject.GLOBAL.table.columns.adjust();
            }, 300);
        });

        $("li[data-li='step-document']").click(function () {

            var ObjectMainId = DSObject.GLOBAL.objectClick;

            //$(DSObject.SELECTORS.content_info_object).html($(".step-document").html());
            DSObject.GetFileByObjectMainId(ObjectMainId);
        });

        $("#fileNameValue").change(function () {
            //lay thong tin file
            //let url = $(this).val();
            //let temp = url.split("\");
            //let fileName = temp[temp.length - 1];
            //console.log(fileName);

            $("#uploadFileSubmit").removeAttr("disabled");


            var file = $("#fileNameValue")[0].files;
            $("#file-name").text(file[0].name);
            const mb = 1048576;
            sizeCovert = parseFloat(file[0].size / mb).toFixed(2);


            // Hiệu ứng upload file
            $(".progress-done").animate({
                width: "100%"
            }, 500);



            $("#file-size").html(sizeCovert + " Mb  <i id='delete-file-upload' class='fa fa-trash'></i>");

            //$("#file").

            $(".choosenFile").addClass("hidden");
            $(".fileUploadedArea").removeClass("hidden");
        });
        $(document).on("click", "#delete-file-upload", function () {
            $("#fileNameValue").val("");
            $(".choosenFile").removeClass("hidden");
            $(".fileUploadedArea").addClass("hidden");
        });

        $("#uploadFileSubmit").click(function () {
            var data = {
                IdMainObject: DSObject.GLOBAL.objectClick,
                NameDocument: $("#file-name").text(),
                IconDocument: document.location.origin + "/common/IconMacDinh.png",
                UrlDocument: $("#fileNameValue").val()
            };


            if (DSObject.GLOBAL.changeAction == true) {
                var id = DSObject.GLOBAL.fileClick;
                if (id != "") {
                    DSObject.removeFileByObjId(id);
                }
            }
            DSObject.UploadFile(data);
        });

        $(document).on('click', '.file-item', function () {
            var bg = "";
            if ($(this).css("background-color") != null) {
                bg = rgb2hex($(this).css("background-color"));
            }
            if (bg == "#fff" || bg == "" || bg == "#ffffff") {
                $('.file-item').css("background-color", "#fff");
                DSObject.GLOBAL.fileClick = $(this).attr("item-id");
                $(this).css("background-color", "#f4f4f4");
            } else {
                DSObject.GLOBAL.fileClick = "";
                $(this).css("background-color", "#fff");
            }
        });

        $(document).on('click', '#btn-remove-file', function () {
            if (DSObject.GLOBAL.fileClick != "") {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:AreYouWantToDeleteThisFile"),
                    icon: "warning",
                    buttons: [
                        l("ManagementLayer:Cancel"),
                        l("ManagementLayer:Agree"),
                    ],
                }).then(function (i) {
                    if (i) {
                        DSObject.GLOBAL.MessageActionFile = l("ManagementLayer:FileDeletedSuccessfully");
                        var id = DSObject.GLOBAL.fileClick;
                        if (id != "") {
                            DSObject.removeFileByObjId(id);
                        }
                    }
                });
            } else {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:PleaseSelectFileToDelete"),
                    icon: "warning"
                })
            }
        });

        $(document).on('click', '#btn-upload-document', function () {
            DSObject.GLOBAL.changeAction = false;
            DSObject.GLOBAL.MessageActionFile = l("ManagementLayer:UploadFileSuccessfullly");
        });

        $(document).on('click', '#btn-change-document', function () {
            if (DSObject.GLOBAL.fileClick != "") {
                DSObject.GLOBAL.MessageActionFile = l("ManagementLayer:FileChangeSuccessfully");
                DSObject.GLOBAL.changeAction = true;
                $('#uploadFileModal').modal('show');
            } else {
                alert(l("ManagementLayer:PleaseSelectFileToEdit"));
                $('#uploadFileModal').modal('hide');
            }
        });

        $('#uploadFileModal').on('hidden.bs.modal', function () {
            DSObject.GLOBAL.changeAction = false;
            $('#custom-file-upload').val("");
            $("#file-name").text("");
            $("#file-size").html("");
            $(".choosenFile").removeClass("hidden");
            $(".fileUploadedArea").addClass("hidden");
        });

        $("#timKiem").on('keyup', delay(function () {
            var keyWord = $(this).val();
            keyWord = keyWord.replace(/\s\s+/g, ' ');

            if (DSObject.GLOBAL.sendAjax != null) {
                DSObject.GLOBAL.sendAjax.abort();
            }
            DSObject.FindObjectByKeyword(keyWord);

            DSObject.GLOBAL.table.ajax.reload();
        }, 500));

        $(document).on('click', '#exportPDF', function () {
            var btn_timed_out = this;
            $(btn_timed_out).prop("disabled", true);
            var table = $("#listData").DataTable();
            var data = table.rows().data();
            var arrayData = [];
            data.each(function (item, index) {
                var qrCodeValue = JSON.stringify(DSObject.convertQRCode(item)).toString();
                if (item.qrCodeObject != "" && item.qrCodeObject != null && item.qrCodeObject != "null") {
                    qrCodeValue = item.qrCodeObject.toString();
                }

                let obj = {
                    Name: item.nameObject,
                    QRCode: qrCodeValue
                };
                arrayData.push(obj);
            });
            var dataPost = JSON.stringify(arrayData);

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: "application/json-patch+json",
                async: true,
                url: document.location.origin + "/api/HTKT/ManagementData/export-to-pdf",
                data: dataPost,
                beforeSend: function () {
                    var spinner = document.getElementsByClassName('spinner');
                    spinner[0].style.display = "block";
                },
                success: function (data) {
                    if (data.code == "ok") {
                        var local = window.location.origin;
                        var url = `${local}/FileQRCode/${data.result}`;
                        DSObject.Download(url, data.result);
                    }
                    else {
                        console.log(data);
                    }
                    $(btn_timed_out).prop("disabled", false);
                },
                complete: function () {
                    var spinner = document.getElementsByClassName('spinner');
                    spinner[0].style.display = "none";
                },
            });
        });

        $(DSObject.SELECTORS.modal).on("click", DSObject.SELECTORS.tableRow, function () {
            $(DSObject.SELECTORS.btnCloseModal).trigger("click");
            var idShow = $(this).find(".action " + DSObject.SELECTORS.btnInById).attr("data-id");

            let value = idShow;
            var obj = mapdata.GLOBAL.features.find(x => x._id === value);
            if (obj != null && obj != undefined) {

                mapdata.CancelAction();
                // tìm kiếm feature
                mapdata.GLOBAL.selectObject2D = null;
                var obj = mapdata.GLOBAL.features.find(x => x._id === value);
                if (obj != null && obj != undefined) {
                    mapdata.GLOBAL.selectObject2D = {
                        feature: obj,
                        location: { lat: menuLeft.GLOBAL.lat, lng: menuLeft.GLOBAL.lng }
                    };
                }

                modalCreateFolderLayer.resetTableData();
                mapdata.visibleHightlight(false);

                // ẩn 3d
                let objold = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject);
                if (objold != undefined && objold != null) {
                    objold.object.setSelected(false);
                }


                model3D.GLOBAL.geojsonDefault3D = "";
                model3D.GLOBAL.geojson3D = {
                    type: "Polygon",
                    coordinates: []
                };
                model3D.GLOBAL.objectGeojson3D = null;

                if (mapdata.GLOBAL.selectObject2D != null) // hightlight đối tượng dạng polygon
                {
                    mapdata.highlightObject(mapdata.GLOBAL.selectObject2D);
                }

                mapdata.GLOBAL.idMainObject = value; // set id đối tượng hiện tại

                // bôi đen menu
                $('.ul-left li').removeClass("menu-open");
                $($('a[menuid="' + mapdata.GLOBAL.idMainObject + '"]')[0]).parent().addClass("menu-open");

                $('#InforData').addClass('active'); // mở content thông tin

                mapdata.getMainObject("", mapdata.SELECTORS.inforData); // thấy thông tin đối tượng
                mapdata.getDefaultObject("", mapdata.SELECTORS.inforData); // lấy thuộc tính đối tượng

                Info_Properties_Main_Object.setValueObject(menuLeft.SELECTORS.content_step_info_data); // set value đối tượng vào form input thông tin

                // set value cho thuộc tính 3d
                model3D.setData();
                model3D.setInfo3D();

                // kiểm tra nếu đối tượng có 3d thì higlight
                var obj3d = mapdata.GLOBAL.listObject3D.find(x => x.id == mapdata.GLOBAL.idMainObject); // object 3d
                if (obj3d != undefined && obj3d != null) { // hightlight 3d
                    obj3d.object.setSelected(true);
                }

                // set thông tin 2d
                informap2d.setInfor2D();

                // get tài liệu của đối tượng
                documentObject.GetFileByObjectMainId(mapdata.GLOBAL.idMainObject, documentObject.updateList);
                modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;

                mapdata.clearHighlightPoint();
                // hightlight point
                if (mapdata.GLOBAL.selectObject2D != null) {
                    if (mapdata.GLOBAL.selectObject2D.feature._geometry._coordinate !== undefined) {
                        mapdata.highlightObjectPoint(mapdata.GLOBAL.selectObject2D.feature._geometry._coordinate.lat, mapdata.GLOBAL.selectObject2D.feature._geometry._coordinate.lng);
                    }

                    // hightlight mutiple point
                    if (mapdata.GLOBAL.selectObject2D.feature._geometry._coordinates !== undefined) {
                        mapdata.highlightObjectMutiplePoint(mapdata.GLOBAL.selectObject2D.feature._geometry._coordinates);
                    }
                }


                Activity.GetDanhSachHoatDong(mapdata.GLOBAL.idMainObject, mapdata.GLOBAL.idDirectory);

                $(`li[menuid='${mapdata.GLOBAL.idMainObject}']`).addClass('menu-open');
                $('#InforData .li-modal-data-right').eq(3).trigger('click');

            }
        });
    },
    Download: function (url, filename) {
        fetch(url)
            .then(resp => resp.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.href = url;
                // the filename you want
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                window.URL.revokeObjectURL(url);
            })
            .catch(() => alert('oh no!'));
    },
    AddElementObjectData: function (data) {
        var thongTinCoBan = ``;
        var thongTinChiTiet = ``;
        for (var i = 0; i < data.listProperties.length; i++) {
            if (data.listProperties[i].typeSystem == 2) {
                thongTinCoBan += `<div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-xs-4">
                                        <label>${data.listProperties[i].nameProperties}:</label>
                                    </div>
                                    <div class="col-xs-8">
                                        <span ${(data.listProperties[i].defalutValue != "" ? "" : "style='color:#ccc;'")}>${(data.listProperties[i].defalutValue != "" ? data.listProperties[i].defalutValue : l("ManagementLayer:NoData"))}</span>
                                    </div>
                               </div>
                        </div>
                    </div>`;
            }

            if (data.listProperties[i].typeSystem == 3) {
                thongTinChiTiet += `<div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-xs-4">
                                        <label>${data.listProperties[i].nameProperties}:</label>
                                    </div>
                                    <div class="col-xs-8">
                                        <span ${(data.listProperties[i].defalutValue != "" ? "" : "style='color:#ccc;'")}>${(data.listProperties[i].defalutValue != "" ? data.listProperties[i].defalutValue : l("ManagementLayer:NoData"))}</span>
                                    </div>
                               </div>
                        </div>
                    </div>`;
            }
        }

        var html = `
                    <div class="k-content">
                        <div class="w-100 scroll-content">
                            <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group"> 
                                        <h4 style="font-weight: 700;">${l('ManagementLayer:BasicInformation')}</h4>
                                    </div>
                                </div>
                            </div>
                            ${thongTinCoBan}
                            <hr />
                            <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <h4 style="font-weight: 700;">${l('ManagementLayer:DetailInfo')}</h4>
                                    </div>
                                </div>
                            </div>
                            ${thongTinChiTiet}

                        </div>
                        <div class="k-button-group">
                            <div class="d-flex flex-row justify-content-between w-100">
                                <div><button class="btn btx-xs btn-primary mx-1 edit-object" data-id="${data.id}">${l('ManagementLayer:Edit')}</button></div>
                                <div><button class="btn btx-xs btn-danger mx-1 delete-object" data-id="${data.id}">${l('ManagementLayer:Remove')}</button></div>
                                <div><button class="btn btx-xs btn-success mx-1 export-object" data-id="${data.id}">${l('ManagementLayer:Download')}</button></div>
                            </div>
                        </div>

                    </div>
                    `;

        $(DSObject.SELECTORS.content_info_object).html(html);
    },
    AppendUploadFileForm: function (data) {

        var listFile = '';
        if (data !== null && data.length > 0) {
            $.each(data, function (index, value) {
                listFile += '<div class="row file-item"  item-id="' + value.id + '">';
                listFile += '    <div class="col-md-2 col-xs-6 stt-item">' + (index + 1) + '</div>';
                listFile += '    <div class="col-md-2 col-xs-6 icon-item"><img src="' + value.iconDocument + '"></img></div>';
                listFile += '    <div class="col-md-8 col-xs-12 title-item">' + value.nameDocument + '</div>';
                listFile += '</div>';
            });
        }
        else {
            listFile += '<div class="row file-item" style="text-align:center">';
            listFile += '   <span style="color: #c4cec4;font-weight: bold;">' + l("ManagementLayer:NoFileUpload") + '</span>';
            listFile += '</div>';
        }

        var html = '';
        html += '<div class="box-action">'
        html += '   <button class="btn btn-primary" id="btn-upload-document" data-toggle="modal" data-target="#uploadFileModal">'
        html += '       <i class="fa fa-upload" aria-hidden="true"></i><span style="padding: 5px;">' + l("ManagementLayer:Upload") + '</span>'
        html += '   </button>'
        html += '   <button class="btn btn-primary" id="btn-change-document" data-toggle="modal">'
        html += '       <i class="fa fa-exchange" aria-hidden="true"></i><span style="padding: 5px;">' + l("ManagementLayer:Replace") + '</span>'
        html += '   </button>'
        html += '   <button class="btn btn-primary" id="btn-remove-file">'
        html += '       <i class="fa fa-trash" aria-hidden="true"></i><span style="padding: 5px;">' + l("ManagementLayer:Delete") + '</span>'
        html += '   </button>'
        html += '</div>';

        html += '<div class="row" style="background: rgba(214, 235, 255, 0.95)">';
        html += '    <div class="col-md-2 col-title"> ' + l("ManagementLayer:No.") + '</div>';
        html += '    <div class="col-md-2 col-title"> ' + l("ManagementLayer:Icon") + '</div>';
        html += '    <div class="col-md-8 col-title">' + l("ManagementLayer:Title") + '</div>';
        html += '</div >';

        html += listFile;
        $(DSObject.SELECTORS.content_info_object).html(html);
    },
    GetFileByObjectMainId: function (ObjectMainid) {
        let id = ObjectMainid;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + "/api/HTKT/DocumentObject/get-file-by-objectId?Id=" + id,
            data: {
            },
            success: function (data) {
                if (data.code === "ok") {
                    DSObject.AppendUploadFileForm(data.result);
                }
            }
        });
    },
    UploadFile: function (data) {
        var fdata = new FormData();
        var files = $('#fileNameValue')[0].files;
        if (files.length > 0) {
            fdata.append('Files', files[0]);
            fdata.append('IdMainObject', data.IdMainObject);
            fdata.append('NameDocument', data.NameDocument);
            fdata.append('IconDocument', data.IconDocument);
            fdata.append('UrlDocument', data.UrlDocument);
        }

        $.ajax({
            type: "POST",
            contentType: 'application/json',
            async: false,
            data: fdata,
            contentType: false,
            processData: false,
            url: document.location.origin + "/api/HTKT/DocumentObject/add-file",
            success: function (result) {
                swal({
                    title: "Thông báo",
                    text: DSObject.GLOBAL.MessageActionFile,
                    icon: "success",
                    button: "Đóng",
                });
                //$("#uploadFileModal").addClass("hidden");
                $('#uploadFileModal').modal('hide');
                $('#custom-file-upload').val("");
                $("#file-name").text("");
                $("#file-size").html("");
                $(".choosenFile").removeClass("hidden");
                $(".fileUploadedArea").addClass("hidden");
                DSObject.GetFileByObjectMainId(data.IdMainObject);
            }
        });
    },
    removeFileByObjId: function (id) {
        id = id;
        $.ajax({
            type: "DELETE",
            url: document.location.origin + "/api/HTKT/DocumentObject/delete-file?Id=" + id,
            success: function (data) {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: DSObject.GLOBAL.MessageActionFile,
                    icon: "success",
                    button: l("ManagementLayer:Close"),
                });

                DSObject.GetFileByObjectMainId(DSObject.GLOBAL.objectClick);
            }
        });
    },
    FindObjectByKeyword: function (keyWord) {
        $(".list-result").remove();
        if (keyWord.trim() != "") {
            DSObject.GLOBAL.sendAjax = $.ajax({
                type: "GET",
                contentType: "application/json-patch+json",
                //async: false,
                url: document.location.origin + "/api/app/mainObject/objectByKeyWord?keyWord=" + keyWord.toLowerCase().trim(),
                data: {

                },
                success: function (data) {
                    if (data.length > 0) {
                        var html = '<ul class="list-result">';

                        $.each(data, function () {
                            html += '<li class="item-result">' + this.nameObject + '</li>';
                        });
                        html += '</ul>';

                        //$(html).insertAfter("#timKiem");
                    } else {
                        var html = '<ul class="list-result">';
                        html += '<li class="item-result"> ' + l("ManagementLayer:NoData") + ' </li>';
                        html += '</ul>';

                        //$(html).insertAfter("#timKiem");
                    }
                },
                //timeout: 3000
            });
        }
        if (DSObject.GLOBAL.isOpenTable == true) {
            $(".list-result").addClass("hidden");
        } else {
            $(".list-result").removeClass("hidden");
        }
    },
    /**
      * Set datatable
     */
    setDatatable: function () {
        DSObject.GLOBAL.table = $(DSObject.SELECTORS.listdata).DataTable({
            "lengthMenu":
                [
                    [5, 10, 25, 50, 100, -1],
                    ['5', '10', '25', '50', '100', l("ManagementLayer:All")]
                ],
            "pageLength": 5,
            "iDisplayLength": 5,
            "autoWidth": true,
            "responsive": true,
            "bStateSave": false,
            "stateDuration": -1,
            "proccessing": true,
            "serverSide": true,
            //"scrollY": "calc(100vh - 325px)",
            //"scrollCollapse": true,
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem_MENU_mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trang trước",
                    "sNext": "Trang kế",
                    "sLast": "Cuối"
                },
                "searchPlaceholder": "Tìm kiếm"
            },
            "rowCallback": function (row, data) {

            },
            "initComplete": function (settings, json) {

            },
            "columnDefs": [
                { "searchable": false, "orderable": false, "targets": 0 }
            ],
            "order": [[1, 'asc']],
            "dom": "<'row'<'col-md-12 right-control'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-lg-5'i><'col-sm-12 col-lg-7'p>>",
            "ajax": {
                "url": "/api/HTKT/ManagementData/get-list-datatable",
                "type": 'POST',
                "contentType": "application/x-www-form-urlencoded; charset=utf-8",
                "data": function (d) {
                    d.search.value = xoa_dau($('#timKiem').val().toLowerCase().trim().replace(/\s\s+/g, ' '));
                    d.search.regex = DSObject.GLOBAL.idDirectory;
                    d.code = $(menuLeft.SELECTORS.selected_properties_table).val()
                    d.typeProperties = $(menuLeft.SELECTORS.selected_properties_table + " option:selected").attr("data-type") != undefined ? $(menuLeft.SELECTORS.selected_properties_table + " option:selected").attr("data-type") : "";
                },
                //"dataSrc": "items",
                "dataFilter": function (data) {
                    var json = jQuery.parseJSON(data);
                    if (json.code == "ok") {
                        var rs = json.result;
                        json.recordsTotal = rs.totalCount;
                        json.recordsFiltered = rs.totalCount;
                        json.data = rs.items;

                        return JSON.stringify(json); // return JSON string
                    }
                    else {
                        json.recordsTotal = 0;
                        json.recordsFiltered = 0;
                        json.data = [];

                        return JSON.stringify(json); // return JSON string
                    }

                },
                "complete": function (data) {
                    $(DSObject.SELECTORS.listdata).trigger("order.dt");

                    let recordsTotal = data.responseJSON.totalCount;
                    $(DSObject.SELECTORS.toTalRecord).text(`Quản lý chủ sở hữu: ${recordsTotal} User`)
                }
            },
            "columns": [
                { "data": null, "width": "50px", "className": "text-center", "orderable": false },
                { "data": "nameObject", "name": "nameObject", "className": "", "width": "40%", },
                //{
                //    "data": function (data, type, full) {
                //        var result = '';

                //        return result;

                //    }, "name": "", "className": "", "orderable": false,
                //},
                {
                    "data": null,
                    "render": function (data, type, row) {
                        var qrcode = new QRCode.Encoder();
                        var errorCorrectionLevel = QRCode.ErrorCorrectionLevel['L'];
                        qrcode.setEncodingHint(true).setErrorCorrectionLevel(errorCorrectionLevel);
                        var mode = 'Auto';

                        if (data.qrCodeObject != "" && data.qrCodeObject != null && data.qrCodeObject != "null")
                        {
                            try {
                     
                                var size = mode === 'Auto' ? DSObject.chooseBestModeData(data.qrCodeObject) : new QRCode[mode](data.qrCodeObject);
                                qrcode.write(size).make();

                                return `<img width="85" height="87" style =  "display: block; margin: 10px auto" src="${qrcode.toDataURL(2, 0)}" />`;
                            }
                            catch (err) {
                                var text = JSON.stringify(DSObject.convertQRCode(data));
                                var size = mode === 'Auto' ? DSObject.chooseBestModeData(text) : new QRCode[mode](text);
                                qrcode.write(size).make();
                                return `<img width="85" height="87" style =  "display: block; margin: 10px auto" src="${qrcode.toDataURL(2, 0)}" />`;

                                //return `<img width="85" height="87" style =  "display: block; margin: 10px auto" src="/" />`;
                            }
                        }
                        else {

                            try {
                                var text = JSON.stringify(DSObject.convertQRCode(data));
                                var size = mode === 'Auto' ? DSObject.chooseBestModeData(text) : new QRCode[mode](text);
                                qrcode.write(size).make();
                                return `<img width="85" height="87" style =  "display: block; margin: 10px auto" src="${qrcode.toDataURL(2, 0)}" />`;
                            }
                            catch (err) { return `<img width="85" height="87" style =  "display: block; margin: 10px auto" src="/" />`; }
                        }
                    }, "name": "", "className": "", "orderable": false
                },
                {
                    "data": function (data) { return htmlAction(data) }, "width": "188px", "className": "action text-center", "orderable": false, title: "Hành động"
                },

            ],
        });
        function htmlAction(data) {
            let result = "";
            result += /*`<img data-id="${data.id} class=" btn-in-by-id src="/common/Mask-Group-75.png" width="24" heigth="24"></img>`;*/
                `<button href="javascript:;" title="In pdf" class=" btn-in-by-id" data-id="${data.id}">
                                <img src="/common/Mask-Group-75.png" width="24" heigth="24"></img>
                            </button>`;
            return result;
        }
    },
    convertQRCode: function (data) {
        if (data != undefined) {
            var obj = {
                id: "",
                location: []
            };

            if (data.geometry.type == "Point") {

                obj.id = data.id;
                obj.location = data.geometry.coordinates;
            } else if (data.geometry.type == "MultiPoint") {

                let listPoint = turf.multiPoint(data.geometry.coordinates);
                let centroid = turf.centroid(listPoint);

                obj.id = data.id;
                obj.location = centroid;
            } else if (data.geometry.type == "LineString") {

                let line = turf.lineString(data.geometry.coordinates);
                let centroid = turf.centroid(line);

                obj.id = data.id;
                obj.location = centroid;
            } else if (data.geometry.type == "MultiLineString") {

                let line = turf.multiLineString(data.geometry.coordinates);
                let centroid = turf.centroid(line);

                obj.id = data.id;
                obj.location = centroid;
            } else if (data.geometry.type == "MultiPolygon") {

                let polygon = turf.multiPolygon(data.geometry.coordinates);
                let centroid = turf.centroid(polygon);

                obj.id = data.id;
                obj.location = centroid;
            } else {
                if (data.geometry.coordinates != null && data.geometry.coordinates != undefined && data.geometry.coordinates.length > 0) {
                    let polygon = turf.polygon(data.geometry.coordinates);
                    let centroid = turf.centroid(polygon);
                    obj.location = centroid;
                }
                obj.id = data.id;

            }

            return obj;
        }
    },
    chooseBestModeData: function (data) {
        var TEST_NUMERIC = /^\d+$/;
        var TEST_ALPHANUMERIC = /^[0-9A-Z$%*+-./: ]+$/;

        if (TEST_NUMERIC.test(data)) {
            return new QRCode.QRNumeric(data);
        } else if (TEST_ALPHANUMERIC.test(data)) {
            return new QRCode.QRAlphanumeric(data);
        }

        try {
            return new QRCode.QRKanji(data);
        } catch (error) {
            return new QRCode.QRByte(data);
        }
    },
}

/*
* Page loaded
*/
$(document).ready(function () {
    //DSObject.init();
});

function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    if (rgb != null) {
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }
    else {
        return "";
    }
}

function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    //str = str.replace(/\s/g, '');
    return str;
}