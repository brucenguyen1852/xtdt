var modalCreateFolderLayer = {
    GLOBAL: {
        listImageInput: [],
        filecount: 0,
        listIdImageTemporary: [],
        listObjectImage: [],
        listObjectLinkCamera: [],
        listObjectFile: [],
        nameImage: "",
        indexCarousel: 0,
        fileArray: [],
        listColumnImage: [
            //{
            //    "width": 5,
            //    "name": "STT"
            //},
            {
                "width": 90,
                "name": "Tên"
            },
            {
                "width": 150,
                "name": "Hình ảnh"
            },
            //{
            //    "width": 100,
            //    "name": "Đường dẫn"
            //},
            {
                "width": 88,
                "name": "Thao tác"
            }
        ],
        listColumnLinkCamera: [
            //{
            //    "width": 5,
            //    "name": "STT"
            //},
            {
                "width": 90,
                "name": "Tên"
            },
            {
                "width": 70,
                "name": "Thứ tự"
            },
            {
                "width": 100,
                "name": "Đường dẫn"
            },
            {
                "width": 88,
                "name": "Thao tác"
            }
        ],
        listColumnFile: [
            //{
            //    "width": 5,
            //    "name": "STT"
            //},
            {
                "width": 90,
                "name": "Tên"
            },
            {
                "width": 70,
                "name": "Thứ tự"
            },
            {
                "width": 100,
                "name": "Thư mục"
            },
            //{
            //    "width": 100,
            //    "name": "Đường dẫn"
            //},
            {
                "width": 88,
                "name": "Thao tác"
            }
        ],
        urlUploadFile: '',
        clickCreateFolderLayer: false,
        listObjectFileDelete: [],
        SelectedFilesFolder: []
    },
    SELECTORS: {
        modal: "#myModal2",
        btn_close_modal: "#btn-close-model-properties-object",
        itemFilePlace: ".item-FilePlace",
        fileUploadFile: ".file-upload",
        searchNewfile: ".search-newfile",
        btnAddNewFile: ".search-newfile button",
        myModalLinkCamera: "#myModalLinkCamera",
        tableTbody: ".table-infor-object tbody",
        tableThead: ".table-infor-object thead",
        table: ".table-infor-object",
        searchTenTable: ".search-newfile>input",
        hasError: ".form-group.has-error",


        //image
        fileUploadInput: ".file-upload-input",
        imageUploadWrap: '.image-upload-wrap',
        deleteBtn: '.remove-image-modal',
        fileUploadContent: '.file-upload-content',
        fileUploadBtn: '.file-upload-btn',
        btnSaveImage: '#save-image',
        btnDeleteImage: ".btn-deleteTable-image",
        itemPlaceActive: ".item-FilePlace.activefile",
        modelViewImage: "#myModalViewImageInfor",
        carouselIndicatorsLiViewImage: "#myModalViewImageInfor .carousel-indicators",
        carouselIndicatorsViewImage: "#myModalViewImageInfor .carousel-inner",

        //link
        btnSaveLink: ".btn-link-save",
        btn_close_modal_link: "#close-myModalLinkCamera",
        inputIdLink: "input[name='IdLink']",
        inputNameLink: "input[name='NameLink']",
        inputLinkLink: "input[name='UrlLink']",
        inputOrderLink: "input[name='OrderLink']",
        btnEditLink: ".btn-edit-link",
        btnDeleteLink: ".btn-deleteTable-link",
        //file infor
        modalFileUploadInfor: "#myModalFileUpload",
        imageInfor: ".table-infor-object .image-infor",
        //file
        btn_close_file_modal: ".btn-close-file-upload",
        btnSaveFile: ".btn-file-save",
        inputIdFile: "input[name='IdFile']",
        inputNameFile: "input[name='NameFile']",
        inputLinkFile: "input[name='UrlFile']",
        inputOrderFile: "input[name='OrderFile']",
        inputListDocument: "select[name='ListDocument']",
        labelUrlFile: "label[name='LabelUrlFile']",
        btnEditFile: ".btn-edit-file",
        btnDeleteFile: ".btn-deleteTable-file",
        linkButton: ".link-button",
        fileButton: ".file-button",
        closePopup: ".toggle-detail-property2",
        buttonCreateLink: ".buttonCreateLink",

        form_info: "#InforData",
        form_edit: "#EditForm",


        content_step_extend: "#InforData .step-extend",
        edit_parent: "#EditForm .step-extend",
        contentParent: "#myModal2,#InforData .step-extend,#EditForm .step-extend",

        btn_save_extend: "#InforData .step-extend #save-extend-info",
        content_file_upload_modal_upload_info: "#myModalFileUpload .file-upload-content", // content hiển thị file được chọn trong modal tài liệu
        btn_delete_file_input_modal_upload_info: ".action-file-obj"
    },
    CONST: {
        URL_UPDATE_EXTEND_MAIN_OBJECT: "/api/HTKT/ManagementData/update-extend-main-object",
        URL_UPDATE_DOCUMENT_MAIN_OBJECT: "/api/HTKT/DocumentObject/update-file-document-main-object"
    },
    init: function () {
        modalCreateFolderLayer.setUpEvent();
        modalCreateFolderLayer.EventStepConfig();
    },
    setUpEvent: function () {
        // click input file modal tài liệu
        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).on('click', modalCreateFolderLayer.SELECTORS.buttonCreateLink, function () {
            $(modalCreateFolderLayer.SELECTORS.inputLinkFile).trigger('click');
        });

        // close modal create
        $(modalCreateFolderLayer.SELECTORS.modal).on("click", modalCreateFolderLayer.SELECTORS.closePopup, function () {
            if (mapdata.GLOBAL.selectbtn === "insert") {
                if (!modalCreateFolderLayer.CheckChangeValueInputFormBeforeExitsAction('#myModal2 .detail-property-body')) {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("AreYouExitAction"),
                        icon: "warning",
                        buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                        dangerMode: false,
                    }).then((val) => {
                        if (val) {
                            mapdata.clearAllDraw();
                            modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                            mapdata.ResetInsertMainOjbect();
                            mapdata.GLOBAL.selectmenu = '';
                        }
                    });
                }
                else {
                    mapdata.clearAllDraw();
                    modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                    mapdata.ResetInsertMainOjbect();
                    mapdata.GLOBAL.selectmenu = '';
                }
            }
            else {
                $(mapdata.SELECTORS.cancelgeojson).trigger('click');
            }
        });

        // close modal create
        $(modalCreateFolderLayer.SELECTORS.modal).on("click", modalCreateFolderLayer.SELECTORS.btn_close_modal, function () {

            if (mapdata.GLOBAL.selectbtn === "insert") {
                if (!modalCreateFolderLayer.CheckChangeValueInputFormBeforeExitsAction('#myModal2 .detail-property-body')) {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("AreYouExitAction"),
                        icon: "warning",
                        buttons: [l("ManagementLayer:No"), l("ManagementLayer:Agree")],
                        dangerMode: false,
                    }).then((val) => {
                        if (val) {
                            mapdata.clearAllDraw();
                            modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                            mapdata.ResetInsertMainOjbect();
                        }
                    });
                }
                else {
                    mapdata.clearAllDraw();
                    modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
                    mapdata.ResetInsertMainOjbect();
                }
            }
            else {
                $(mapdata.SELECTORS.cancelgeojson).trigger('click');
            }
        });

        //image,link
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("click", modalCreateFolderLayer.SELECTORS.btnAddNewFile, function () {
            modalCreateFolderLayer.clearLabelError();
            let check = $(this).attr("data-value");
            switch (check.toLowerCase()) {
                case "linkcamera":
                    $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).modal("show");
                    break;
                case "fileupload":
                    $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("show");
                    //modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = true;
                    break;
                default:
                    break;
            }
        });

        $(modalCreateFolderLayer.SELECTORS.modal).on('click', modalCreateFolderLayer.SELECTORS.itemFilePlace, function () {
            var id = $(this).attr("data-value");
            $(modalCreateFolderLayer.SELECTORS.itemFilePlace).removeClass("activefile");
            $(this).addClass("activefile");
            $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.fileUploadFile}`).css("display", "none");
            if (id == "image") {
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.fileUploadFile}`).css("display", "block");
                $(modalCreateFolderLayer.SELECTORS.searchNewfile).hide();
            } else {
                $(modalCreateFolderLayer.SELECTORS.searchNewfile).show();
            }
            modalCreateFolderLayer.resetAll();
            modalCreateFolderLayer.updateTable2(id, modalCreateFolderLayer.SELECTORS.modal);
        });

        $(modalCreateFolderLayer.SELECTORS.modal).on('keyup', modalCreateFolderLayer.SELECTORS.searchTenTable, function () {
            var selected = $(modalCreateFolderLayer.SELECTORS.itemPlaceActive).attr("data-value");
            var lst = [];
            if (selected == "image") {
                lst = modalCreateFolderLayer.GLOBAL.listObjectImage;
            }
            if (selected == "LinkCamera") {
                lst = modalCreateFolderLayer.GLOBAL.listObjectLinkCamera;
            }

            var text = $(this).val().trim();
            var lstsearch = lst.filter(x => x.name.includes(text));

            modalCreateFolderLayer.updateTableSearch(lstsearch);
        });

        //image
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("change", modalCreateFolderLayer.SELECTORS.fileUploadInput, function () {
            $(this).parents('.tab-pane').find('.lable-error').remove();

            var content = modalCreateFolderLayer.GetContentParent();
            var indexCounter = 0;
            var exitsFormat = true;
            for (var i = 0; i < this.files.length; i++) {
                indexCounter++;
                var exp = this.files[i].name.substring(this.files[i].name.lastIndexOf(".") + 1).toLowerCase();
                if (exp == "png" || exp == "jpg" || exp == "jpeg") // check định dạng
                {
                    if (this.files[i].size <= sizeFiles.sizeImgNormal) // check size
                    {
                        let uid = mapdata.uuidv4();
                        modalCreateFolderLayer.readURL2(this.files[i], uid, content, i);
                        modalCreateFolderLayer.GLOBAL.listImageInput.push({ id: uid, file: this.files[i] });
                    }
                    else {
                        exitsFormat = false;
                    }
                }
                else {
                    exitsFormat = false;
                }
            };

            if (!exitsFormat) {
                if (indexCounter > 1) {
                    $(this).parents('.tab-pane').append(`<label class="lable-error text-danger mt-2">${l("ManagementLayer:MutipleFileExitsFormat")}</label>`);
                }
                else {
                    $(this).parents('.tab-pane').append(`<label class="lable-error text-danger mt-2">${l("ManagementLayer:FileExitsFormat")}</label>`);
                }

            }

            $(modalCreateFolderLayer.SELECTORS.imageUploadWrap).removeClass('image-dropping');

            $(this).val(null);

        });

        // xóa hình khi thêm mới hình
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("click", modalCreateFolderLayer.SELECTORS.deleteBtn, function () {

            var content = modalCreateFolderLayer.GetContentParent();
            $(this).parents('.tab-pane').find('.lable-error').remove();

            var id = $(this).attr('data-id');
            var elementbuttondelete = document.getElementById(id);
            //elementbuttondelete.parentElement.parentElement.parentNode.removeChild(elementbuttondelete.parentElement.parentElement);
            document.getElementById(id).remove();
            var elementbuttondelete = document.getElementById(id);
            if (elementbuttondelete != null) {
                document.getElementById(id).remove();
            }
            modalCreateFolderLayer.GLOBAL.filecount--;
            if (modalCreateFolderLayer.GLOBAL.filecount == 0) $(modalCreateFolderLayer.SELECTORS.fileUploadBtn).hide();
            var fileinput = modalCreateFolderLayer.GLOBAL.listImageInput;
            var filedelete = fileinput.find(x => x.id == id);
            const dt = new DataTransfer();
            for (var i = 0; i < fileinput.length; i++) {
                if (fileinput[i].id != id) {
                    dt.items.add(fileinput[i].file)
                }
            }
            var lstinput = [];
            $(modalCreateFolderLayer.SELECTORS.fileUploadInput)[0].files = dt.files;
            for (var i = 0; i < fileinput.length; i++) {
                if (fileinput[i].file.name != filedelete.file.name) {
                    lstinput.push(fileinput[i])
                }
            }

            // modalCreateFolderLayer.GLOBAL.listImageInput = lstinput;
            modalCreateFolderLayer.GLOBAL.listImageInput = modalCreateFolderLayer.GLOBAL.listImageInput.filter(x => x.id != id);
            modalCreateFolderLayer.GLOBAL.listIdImageTemporary = JSON.parse(JSON.stringify(modalCreateFolderLayer.GLOBAL.listIdImageTemporary.filter(x => x != id)));
        });

        // lưu hình
        $(modalCreateFolderLayer.SELECTORS.contentParent).on('click', modalCreateFolderLayer.SELECTORS.btnSaveImage, function () {
            if (!$(Activity.SELECTORS.modal).hasClass('show')) // kiểm tra popup thêm mới hoạt động có được show hay k?
            {
                $(this).parents('.tab-pane').find('.lable-error').remove();
                var content = modalCreateFolderLayer.GetContentParent();

                if (modalCreateFolderLayer.GLOBAL.filecount > 0) {
                    var lstfile = modalCreateFolderLayer.GLOBAL.listImageInput;
                    for (var i = 0; i < lstfile.length; i++) {
                        if (lstfile[i].file.type.toLowerCase() == "image/gif" || lstfile[i].file.type.toLowerCase() == "image/jpeg"
                            || lstfile[i].file.type.toLowerCase() == "image/png" || lstfile[i].file.type.toLowerCase() == "image/jpg") {
                            modalCreateFolderLayer.GLOBAL.nameImage = lstfile[i].file.name;
                            modalCreateFolderLayer.SendFileImage(lstfile[i], content);
                        }
                    }
                    modalCreateFolderLayer.deleteAllImageTemporary();
                    modalCreateFolderLayer.updateTable2("image", content);

                    const dt = new DataTransfer();
                    $(modalCreateFolderLayer.SELECTORS.fileUploadInput)[0].files = dt.files;
                    modalCreateFolderLayer.resetAll();
                    modalCreateFolderLayer.hideBtnSaveImage();
                    //swal({
                    //    title: l("ManagementLayer:Notification"),
                    //    text: l("ManagementLayer:AreYouSaveImage"),
                    //    icon: "warning",
                    //    buttons: [
                    //        l("ManagementLayer:Close"),
                    //        l("ManagementLayer:Agree")
                    //    ],
                    //}).then((value) => {
                    //    if (value) {

                    //    }
                    //})
                } else {
                    swal({
                        title: l("ManagementLayer:Notification"),
                        text: l("ManagementLayer:NotSelectImage"),
                        icon: "warning",
                        button: l("ManagementLayer:Close"),
                    }).then((value) => {
                    });
                }
            }
        });

        // xóa hình ra khỏi bảng
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("click", modalCreateFolderLayer.SELECTORS.btnDeleteImage, function () {
            var content = modalCreateFolderLayer.GetContentParent();

            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:AreYouDeleteImage"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    var data = $(this).attr('delete-for');
                    var obj = modalCreateFolderLayer.GLOBAL.listObjectImage.find(x => x.id == data);
                    if (obj !== null) {
                        //let url = encodeURIComponent(obj.url)
                        let check = modalCreateFolderLayer.DeleteFileImage(obj.url);
                        //if (check) {
                        modalCreateFolderLayer.GLOBAL.listObjectImage = JSON.parse(JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectImage.filter(x => x.id != data)));

                        modalCreateFolderLayer.updateTable2("image", content);
                        //}
                    }
                }
            })
        });

        // close modal link
        $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).on("click", modalCreateFolderLayer.SELECTORS.btn_close_modal_link, function () {
            $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).modal('hide');
            //swal({
            //    title: l("ManagementLayer:Notification"),
            //    text: l("ManagementLayer:AreYouExitAction"),
            //    icon: "warning",
            //    buttons: [
            //        l("ManagementLayer:Close"),
            //        l("ManagementLayer:Agree")
            //    ],
            //}).then((value) => {
            //    if (value) {

            //    }
            //});
        })

        //link
        $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).on("click", modalCreateFolderLayer.SELECTORS.btnSaveLink, function () {
            var btn_time_out = $(this);
            $(this).prop("disabled", true);
            setTimeout(function () {
                $(btn_time_out).prop("disabled", false)
            }, 1000);
            if (!$(Activity.SELECTORS.modal).hasClass('show')) // kiểm tra popup thêm mới hoạt động có được show hay k?
            {
                var content = modalCreateFolderLayer.GetContentParent();

                var elementForm = modalCreateFolderLayer.SELECTORS.myModalLinkCamera;

                if (modalCreateFolderLayer.checkLink(elementForm)) {
                    let thutu = Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderLink}`).val().trim());

                    if (modalCreateFolderLayer.GLOBAL.listObjectLinkCamera == null) {
                        modalCreateFolderLayer.GLOBAL.listObjectLinkCamera = [];
                    }

                    var checkthutu = modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.find(x => x.order == thutu);

                    let id = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputIdLink}`).val();
                    if (typeof id !== "undefined" && id.length > 0 && ((typeof checkthutu !== "undefined" && checkthutu.id == id) || typeof checkthutu == "undefined")) {
                        var objTemp = modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.find(x => x.id == id);
                        objTemp.name = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputNameLink}`).val().trim();
                        objTemp.url = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`).val().trim();
                        var checkOrder = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderLink}`).val().trim();
                        objTemp.order = Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderLink}`).val().trim());

                        modalCreateFolderLayer.updateTable2('LinkCamera', content);

                        $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).modal("hide");
                    } else if ((typeof id == "undefined" || id.length <= 0) && typeof checkthutu == "undefined") {
                        var count = modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length + 1;
                        var checkOrder = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderLink}`).val().trim();
                        var item = {
                            //stt: count + 1,
                            id: mapdata.uuidv4(),
                            name: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputNameLink}`).val().trim(),
                            url: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`).val().trim(),
                            order: (checkOrder !== null && checkOrder !== "") ?
                                Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderLink}`).val().trim())
                                : count,
                        };
                        modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.push(item);
                        //$('#myModalVideo').hide();
                        modalCreateFolderLayer.updateTable2('LinkCamera', content);

                        $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).modal("hide");
                    } else {
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:No.AlreadyExists"),
                            icon: "error",
                            button: l("ManagementLayer:Close"),
                        }).then((value) => {
                        });
                    }
                }
            }

        });

        //***********************************TAILIEU**********************************************
        // đóng modal upload file
        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).on("click", modalCreateFolderLayer.SELECTORS.btn_close_file_modal, function () {
            $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal('hide');
            //swal({
            //    title: l("ManagementLayer:Notification"),
            //    text: l("ManagementLayer:AreYouExitAction"),
            //    icon: "warning",
            //    buttons: [
            //        l("ManagementLayer:Close"),
            //        l("ManagementLayer:Agree")
            //    ],
            //}).then((value) => {
            //    if (value) {

            //    }
            //});
        });

        // lưu file khi thêm tài liệu
        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).on("click", modalCreateFolderLayer.SELECTORS.btnSaveFile, function () {
            var btn_time_out = this;

            setTimeout(function () {
                $(btn_time_out).prop("disabled", false)
            }, 1500);
            if (!$(Activity.SELECTORS.modal).hasClass('show')) // kiểm tra popup thêm mới hoạt động có được show hay k?
            {
                $('.label-error').remove();
                var content = modalCreateFolderLayer.GetContentParent();

                var elementForm = modalCreateFolderLayer.SELECTORS.modalFileUploadInfor;

                if (!Activity.GLOBAL.checkModalActivity && modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer && modalCreateFolderLayer.checkFile(elementForm)) {
                    let thutu = Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim());
                    let iddirectoryproperties = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim();
                    if (modalCreateFolderLayer.GLOBAL.listObjectFile == null) {
                        modalCreateFolderLayer.GLOBAL.listObjectFile = [];
                    }
                    var checkthutu = modalCreateFolderLayer.GLOBAL.listObjectFile.find(x => x.order == thutu && x.idDirectoryProperties == iddirectoryproperties);

                    let id = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputIdFile}`).val();
                    if (typeof id !== "undefined" && id.length > 0 && ((typeof checkthutu !== "undefined" && checkthutu.id == id) || typeof checkthutu == "undefined")) {
                        let status = false;
                        if (modalCreateFolderLayer.GLOBAL.fileArray.length > 0) {           // $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files.length > 0
                            let file = modalCreateFolderLayer.GLOBAL.fileArray;             // $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files;
                            $(btn_time_out).prop("disabled", true);
                            //setTimeout(function () {
                            //    $(btn_time_out).prop("disabled", false)
                            //}, 1000);
                            modalCreateFolderLayer.SendFileDocument(file[0]);
                            status = true;
                        }
                        var objTemp = modalCreateFolderLayer.GLOBAL.listObjectFile.find(x => x.id == id);
                        //modalCreateFolderLayer.DeleteFileDocument(objTemp.url);
                        objTemp.name = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val().trim();
                        objTemp.idDirectoryProperties = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim();
                        objTemp.order = Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim());

                        var checkExitsNameFile = mapdata.GLOBAL.lstPropertiesDirectory.find(x => x.typeSystem != 1 && x.codeProperties == objTemp.idDirectoryProperties);
                        if (checkExitsNameFile != undefined) {
                            objTemp.nameProperties = checkExitsNameFile.nameProperties;
                        }

                        if (status) {
                            modalCreateFolderLayer.DeleteFileDocument(objTemp.url);
                            objTemp.url = modalCreateFolderLayer.GLOBAL.urlUploadFile;
                        }
                        //check fomr update table
                        modalCreateFolderLayer.updateTable2("FileUpload", content);

                        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("hide");
                    } else if ((typeof id == "undefined" || id.length <= 0) && typeof checkthutu == "undefined") {
                        var count = modalCreateFolderLayer.GLOBAL.listObjectFile.length + 1;
                        var checkOrder = $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim();
                        let file = modalCreateFolderLayer.GLOBAL.fileArray;                      // $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files;
                        modalCreateFolderLayer.SendFileDocument(file[0]);

                        if (modalCreateFolderLayer.GLOBAL.urlUploadFile != "") {
                            $(btn_time_out).prop("disabled", true);
                            var item = {
                                //stt: count + 1,
                                id: mapdata.uuidv4(),
                                name: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val().trim(),
                                idDirectoryProperties: $(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val().trim(),
                                url: modalCreateFolderLayer.GLOBAL.urlUploadFile,
                                order: (checkOrder !== null && checkOrder !== "") ?
                                    Number($(`${elementForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val().trim())
                                    : count,
                            };
                            var checkExitsNameFile = mapdata.GLOBAL.lstPropertiesDirectory.find(x => x.typeSystem != 1 && x.codeProperties == item.idDirectoryProperties);
                            if (checkExitsNameFile != undefined) {
                                item.nameProperties = checkExitsNameFile.nameProperties;
                            }
                            //idDirectoryProperties
                            modalCreateFolderLayer.GLOBAL.listObjectFile.push(item);
                            //check fomr update table
                            modalCreateFolderLayer.updateTable2("FileUpload", content);

                            $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("hide");
                        }
                    } else {
                        swal({
                            title: l("ManagementLayer:Notification"),
                            text: l("ManagementLayer:No.AlreadyExists"),
                            icon: "error",
                            button: l("ManagementLayer:Close"),
                        }).then((value) => {
                        });
                    }
                }
            }
        });

        // sửa file upload
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("click", modalCreateFolderLayer.SELECTORS.btnEditFile, function () {
            //if (modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer) {
            modalCreateFolderLayer.clearLabelError();

            let id = $(this).attr("edit-for");
            let obj = modalCreateFolderLayer.GLOBAL.listObjectFile.find(x => x.id == id);
            modalCreateFolderLayer.setDataEidtFile(obj);
            $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).modal("show");
            //}
        });

        // xóa file trong bảng
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("click", modalCreateFolderLayer.SELECTORS.btnDeleteFile, function () {
            var content = modalCreateFolderLayer.GetContentParent();

            if (modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer) {
                swal({
                    title: l("ManagementLayer:Notification"),
                    text: l("ManagementLayer:AreYouWantToDeleteThisFile"),
                    icon: "warning",
                    buttons: [
                        l("ManagementLayer:Close"),
                        l("ManagementLayer:Agree")
                    ],
                }).then((value) => {
                    if (value) {
                        var id = $(this).attr('delete-for');
                        let obj = modalCreateFolderLayer.GLOBAL.listObjectFile.find(x => x.id == id);
                        modalCreateFolderLayer.DeleteFileDocument(obj.url);
                        modalCreateFolderLayer.GLOBAL.listObjectFile = JSON.parse(JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectFile.filter(x => x.id != id)));
                        modalCreateFolderLayer.updateTable2("FileUpload", content);
                        modalCreateFolderLayer.GLOBAL.listObjectFileDelete.push(id);
                        //documentObject.DeletedFileById(id);
                    }
                })
            }
        });

        // chọn file trong modal tài liệu
        $(modalCreateFolderLayer.SELECTORS.inputLinkFile).on('change', function () {
            $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).html('');
            $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).addClass('hidden');

            $(this).parent().find('.label-error').remove();
            var file = this.files;
            if (file.length > 0) {
                if (file[0].size <= sizeFiles.sizeFileDocument) {
                    modalCreateFolderLayer.GLOBAL.fileArray = file;
                    let fileObj = file[0];
                    model3D.GLOBAL.fileObject3d.objurl = file[0];
                    let reader = new FileReader();
                    reader.readAsText(fileObj);
                    reader.onload = function (e) {
                        // hiển thị tên file
                        var html = `<div class="icon-file-obj"><img src="/common/icon-file-obj.svg" /></div>
                                        <div class="name-file-obj" title="${fileObj.name}" data-toggle="tooltip" data-placement="bottom">${fileObj.name}</div>
                                        <div class="action-file-obj" title="Xóa" data-toggle="tooltip" data-placement="bottom"><img src="/images/close_cricle.svg" />
                                    </div>`;

                        $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).html(html);
                        $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).removeClass('hidden');
                    };
                }
                else {
                    modalCreateFolderLayer.showLabelError(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`, l("ManagementLayer:ValidSizeFileDocument"));
                    $(this).val(null);
                }
            }
        });

        // xóa file trong modal tài liệu
        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).on('click', modalCreateFolderLayer.SELECTORS.btn_delete_file_input_modal_upload_info, function () {
            $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).html('');
            $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).addClass('hidden');
            $(modalCreateFolderLayer.SELECTORS.inputLinkFile).val(null).trigger("change");
        })

        //******************LINK********************
        // sửa link trong bảng
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("click", modalCreateFolderLayer.SELECTORS.btnEditLink, function () {
            modalCreateFolderLayer.clearLabelError();

            let id = $(this).attr("edit-for");
            let obj = modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.find(x => x.id == id);
            modalCreateFolderLayer.setDataEidtLink(obj);
            $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).modal("show");
        });

        // xóa link trong bảng
        $(modalCreateFolderLayer.SELECTORS.contentParent).on("click", modalCreateFolderLayer.SELECTORS.btnDeleteLink, function () {
            var content = modalCreateFolderLayer.GetContentParent();

            swal({
                title: l("ManagementLayer:Notification"),
                text: l("ManagementLayer:AreYouDeleteLink"),
                icon: "warning",
                buttons: [
                    l("ManagementLayer:Close"),
                    l("ManagementLayer:Agree")
                ],
            }).then((value) => {
                if (value) {
                    var data = $(this).attr('delete-for');
                    modalCreateFolderLayer.GLOBAL.listObjectLinkCamera = JSON.parse(JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.filter(x => x.id != data)));
                    modalCreateFolderLayer.updateTable2('LinkCamera', content);
                }
            })
        });

        // đóng modal link
        $(modalCreateFolderLayer.SELECTORS.modal).on('hidden.bs.modal', modalCreateFolderLayer.SELECTORS.myModalLinkCamera, function () {
            modalCreateFolderLayer.setEmptyLink();
        });

        // đóng modal tài liệu
        $(document).on('hidden.bs.modal', modalCreateFolderLayer.SELECTORS.modalFileUploadInfor, function () {
            $(this).find('.label-error').remove(); // clear label error
            modalCreateFolderLayer.setEmptyFile();
            //Activity.GLOBAL.checkModalActivity = false;
            //modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
        });

        $(document).on('hidden.bs.modal', modalCreateFolderLayer.SELECTORS.modal, function () {
            modalCreateFolderLayer.GLOBAL.clickCreateFolderLayer = false;
        });

        $(document).on('shown.bs.modal', modalCreateFolderLayer.SELECTORS.modal, function () {
            $(modalCreateFolderLayer.SELECTORS.modal + ' .li-tab[data-id="modalBodyFolderLayer"]').trigger('click');
        });

        $('.left.carousel-control').on('click', function () {
            modalCreateFolderLayer.GLOBAL.indexCarousel--;
            if (modalCreateFolderLayer.GLOBAL.indexCarousel == -1) {
                modalCreateFolderLayer.GLOBAL.indexCarousel = modalCreateFolderLayer.GLOBAL.listObjectImage.length - 1;
            }
            $('.carousel-inner .item').removeClass("active");
            $($('.carousel-inner .item')[modalCreateFolderLayer.GLOBAL.indexCarousel]).addClass("active");
        });

        $('.right.carousel-control').on('click', function () {
            modalCreateFolderLayer.GLOBAL.indexCarousel++;
            if (modalCreateFolderLayer.GLOBAL.indexCarousel == modalCreateFolderLayer.GLOBAL.listObjectImage.length) {
                modalCreateFolderLayer.GLOBAL.indexCarousel = 0;
            }
            $('.carousel-inner .item').removeClass("active");
            $($('.carousel-inner .item')[modalCreateFolderLayer.GLOBAL.indexCarousel]).addClass("active");
        });

        // cập nhật file, image, link cho một đối tượng
        $(modalCreateFolderLayer.SELECTORS.btn_save_extend).on('click', function () {
            if (Info_Properties_Main_Object.checkValidFile()) {
                let array = [];
                $.each(modalCreateFolderLayer.GLOBAL.listObjectFile, function (i, obj) {
                    let item = {
                        id: obj.id,
                        idMainObject: mapdata.GLOBAL.idMainObject,
                        nameDocument: obj.name,
                        iconDocument: '',
                        urlDocument: obj.url,
                        nameFolder: obj.idDirectoryProperties,
                        orderDocument: obj.order
                    };
                    array.push(item);
                });
                if (mapdata.GLOBAL.lstProperties.length > 0) {
                    var lst = mapdata.GLOBAL.lstProperties.filter(x => x.TypeProperties == "image" || x.TypeProperties == "link");

                    var object = {
                        id: mapdata.GLOBAL.idMainObject,
                        listProperties: []
                    };

                    $.each(lst, function (i, obj) {
                        var objectparam = {
                            nameProperties: obj.NameProperties,
                            codeProperties: obj.CodeProperties,
                            typeProperties: obj.TypeProperties,
                            isShow: obj.IsShow,
                            isView: obj.IsView,
                            defalutValue: "",
                            orderProperties: obj.OrderProperties,
                            typeSystem: obj.TypeSystem
                        };

                        if (obj.TypeProperties == "link") {
                            objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectLinkCamera);
                        }
                        if (obj.TypeProperties == "image") {
                            objectparam.defalutValue = JSON.stringify(modalCreateFolderLayer.GLOBAL.listObjectImage);
                        }

                        object.listProperties.push(objectparam);
                    });
                }

                // cập nhật link, image
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json-patch+json",
                    async: true,
                    url: modalCreateFolderLayer.CONST.URL_UPDATE_EXTEND_MAIN_OBJECT,
                    data: JSON.stringify(object),
                    success: function (data) {
                        if (data.code == "ok") {
                            var dtoDocument = {
                                idMainObject: mapdata.GLOBAL.idMainObject,
                                data: array
                            };
                            // cập nhật file
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                contentType: "application/json-patch+json",
                                async: true,
                                url: modalCreateFolderLayer.CONST.URL_UPDATE_DOCUMENT_MAIN_OBJECT,
                                data: JSON.stringify(dtoDocument),
                                success: function (data) {
                                    if (data.code == "ok") {
                                        //swal({
                                        //    title: l("ManagementLayer:Notification"),
                                        //    text: l("ManagementLayer:UpdateDataSuccesfully"),
                                        //    icon: "success",
                                        //    button: l("ManagementLayer:Close"),
                                        //});
                                        abp.notify.success(l("ManagementLayer:UpdateDataSuccesfully"));
                                    }
                                    else {
                                        //swal({
                                        //    title: l("ManagementLayer:Notification"),
                                        //    text: l("ManagementLayer:UpdateDataFailed"),
                                        //    icon: "warning",
                                        //    button: l("ManagementLayer:Close"),
                                        //})
                                        abp.notify.error(l("ManagementLayer:UpdateDataFailed"));
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.error(jqXHR + ":" + errorThrown);
                                    ViewMap.showLoading(false);
                                }
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR + ":" + errorThrown);
                        ViewMap.showLoading(false);
                    }
                });
            }
        });

        // keyup file
        $(modalCreateFolderLayer.SELECTORS.modalFileUploadInfor).on('keyup change', 'input', function () {
            if ($(this).val() != "") {
                $(this).parent().removeClass('has-error');
                $(this).parent().find('.label-error').remove();
            }
        });

        // keyup link
        $(modalCreateFolderLayer.SELECTORS.myModalLinkCamera).on('keyup change', 'input', function () {
            if ($(this).val() != "") {
                $(this).parent().removeClass('has-error');
                $(this).parent().find('.label-error').remove();
            }
        });
    },
    resetAll: function () {
        $(modalCreateFolderLayer.SELECTORS.searchTenTable).val('');
        modalCreateFolderLayer.deleteAllImageTemporary();
    },
    updateTable: function (checktext) {
        checktext = typeof checktext !== "undefined" ? checktext : $(modalCreateFolderLayer.SELECTORS.itemPlaceActive).attr("data-value");
        var listcolumn = [];
        $(modalCreateFolderLayer.SELECTORS.searchNewfile).css('display', 'none');
        if (checktext == "image") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnImage;
            if (modalCreateFolderLayer.GLOBAL.listObjectImage !== null && modalCreateFolderLayer.GLOBAL.listObjectImage.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectImage.length; i++) {
                    test += `<tr>
                                <td>${modalCreateFolderLayer.GLOBAL.listObjectImage[i].name}</td>
                                <td><img src="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].url}" style="width: 100px;" data-index="${i}" class="image-infor"></td>
                                <td><a href="javascript:;" title="Xóa" class="delete btn btn-xs btn-deleteTable-image" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].id}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs></defs><path class="a" style="fill:none;" d="M0,0H24V24H0Z"/><path class="b" style="fill:red;" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z"/></svg>
                                    </a>
                                </td>
                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
                modalCreateFolderLayer.eventSildeImage();
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="3">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }
        if (checktext == "LinkCamera") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnLinkCamera;
            if (modalCreateFolderLayer.GLOBAL.listObjectLinkCamera !== null && modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length; i++) {
                    test += `<tr>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].name}</td>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].order}</td>
                                                                <td class="url">${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].url}</td>
                                                                <td><a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn btn-info btn-xs btn-edit-link" edit-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn btn-xs btn-deleteTable-link" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><style>.a{fill:none;}.b{fill:red;}</style></defs><path class="a" d="M0,0H24V24H0Z"/><path class="b" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z"/></svg>
                                                                    </a>
                                                                </td>
                                                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="4">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
            $(modalCreateFolderLayer.SELECTORS.linkButton).css('display', 'block');
        }
        if (checktext == "FileUpload") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnFile;
            if (modalCreateFolderLayer.GLOBAL.listObjectFile !== null && modalCreateFolderLayer.GLOBAL.listObjectFile.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectFile.length; i++) {
                    test += `<tr>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].name}</td>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].order}</td>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].nameProperties}</td>
                                                                <td><a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn btn-info btn-xs btn-edit-file" edit-for="${modalCreateFolderLayer.GLOBAL.listObjectFile[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn btn-danger btn-xs btn-deleteTable-file" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectFile[i].id}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><style>.a{fill:none;}.b{fill:red;}</style></defs><path class="a" d="M0,0H24V24H0Z"/><path class="b" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z"/></svg>
                                                                    </a>
                                                                </td>
                                                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="4">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
            $(modalCreateFolderLayer.SELECTORS.fileButton).css('display', 'block');
        }
        var text = `<tr>`;
        for (var i = 0; i < listcolumn.length; i++) {
            text += `<th scope="col" width="${listcolumn[i].width}px">${listcolumn[i].name}</th>`;
        }
        text += `</tr>`;
        $(modalCreateFolderLayer.SELECTORS.tableThead).html('');
        $(modalCreateFolderLayer.SELECTORS.tableThead).append(text);
    },
    updateTable2: function (checktext, elementContent) {
        checktext = typeof checktext !== "undefined" ? checktext : $(modalCreateFolderLayer.SELECTORS.itemPlaceActive).attr("data-value");
        var listcolumn = [];
        $(modalCreateFolderLayer.SELECTORS.searchNewfile).css('display', 'none');
        if (checktext == "image") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnImage;
            if (modalCreateFolderLayer.GLOBAL.listObjectImage !== null && modalCreateFolderLayer.GLOBAL.listObjectImage.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectImage.length; i++) {
                    test += `<tr>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectImage[i].name}</td>
                                                                <td><img src="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].url}" style="width: 100px;" data-index="${i}" class="image-infor"></td>
                                                                <td><a href="javascript:;" title="Xóa" class="delete btn-deleteTable-image" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].id}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><style>.a{fill:none;}.b{fill:red;}</style></defs><path class="a" d="M0,0H24V24H0Z"/><path class="b" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z"/></svg>
                                                                    </a>
                                                                </td>
                                                            </tr>`;
                    $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
                modalCreateFolderLayer.eventSildeImage();
            } else {
                $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="3">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }
        if (checktext == "LinkCamera") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnLinkCamera;
            if (modalCreateFolderLayer.GLOBAL.listObjectLinkCamera !== null && modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length; i++) {
                    test += `<tr>
                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].name}</td>
                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].order}</td>
                                <td class="url">${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].url}</td>
                                <td>
                                    <a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn-edit-link" edit-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                            <defs>
                                            </defs>
                                            <path class="a" style="fill:none;" d="M0,0H24V24H0Z" />
                                            <path class="b" style="fill: #00559a;" d="M21,6.757l-2,2V4H10V9H5V20H19V17.243l2-2v5.765a.993.993,0,0,1-.993.992H3.993A1,1,0,0,1,3,20.993V8L9,2H20a1,1,0,0,1,1,.992Zm.778,2.05,1.414,1.415L15.414,18,14,18l0-1.412,7.778-7.778Z" />
                                        </svg>
                                    </a>
                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn-deleteTable-link" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                            <defs>
                                            </defs>
                                            <path class="a" style="fill:none;" d="M0,0H24V24H0Z" />
                                            <path class="b" style="fill:red;" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z" />
                                        </svg>
                                    </a>
                                </td>
                            </tr>`;
                    $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="4">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
            $(modalCreateFolderLayer.SELECTORS.linkButton).css('display', 'block');
        }
        if (checktext == "FileUpload") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnFile;
            if (modalCreateFolderLayer.GLOBAL.listObjectFile !== null && modalCreateFolderLayer.GLOBAL.listObjectFile.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectFile.length; i++) {

                    var nameFolder = modalCreateFolderLayer.GLOBAL.listObjectFile[i].nameProperties;

                    var checkExitsNameFile = mapdata.GLOBAL.lstPropertiesDirectory.find(x => x.typeSystem != 1 && x.codeProperties == modalCreateFolderLayer.GLOBAL.listObjectFile[i].idDirectoryProperties);
                    if (checkExitsNameFile != undefined) {
                        nameFolder = checkExitsNameFile.nameProperties;
                    }

                    test += `<tr>
                                    <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].name}</td>
                                    <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].order}</td>
                                    <td>${nameFolder}</td>
                                    <td>
                                        <a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn-edit-file" edit-for="${modalCreateFolderLayer.GLOBAL.listObjectFile[i].id}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                                <defs>
                                                </defs>
                                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z" />
                                                <path class="b" style="fill: #00559a;" d="M21,6.757l-2,2V4H10V9H5V20H19V17.243l2-2v5.765a.993.993,0,0,1-.993.992H3.993A1,1,0,0,1,3,20.993V8L9,2H20a1,1,0,0,1,1,.992Zm.778,2.05,1.414,1.415L15.414,18,14,18l0-1.412,7.778-7.778Z" />
                                            </svg>
                                        </a>
                                        <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn-deleteTable-file" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectFile[i].id}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                                <defs>
                                                </defs>
                                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z" />
                                                <path class="b" style="fill:red;" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z" />
                                            </svg>
                                        </a>
                                    </td>
                                </tr>`;
                    $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="4">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
            $(modalCreateFolderLayer.SELECTORS.fileButton).css('display', 'block');
        }
        var text = `<tr>`;
        for (var i = 0; i < listcolumn.length; i++) {
            text += `<th scope="col" width="${listcolumn[i].width}px">${listcolumn[i].name}</th>`;
        }
        text += `</tr>`;
        $(modalCreateFolderLayer.SELECTORS.tableThead).html('');
        $(modalCreateFolderLayer.SELECTORS.tableThead).append(text);
    },
    updateTableSearch: function (list) {
        checktext = typeof checktext !== "undefined" ? checktext : $(modalCreateFolderLayer.SELECTORS.itemPlaceActive).attr("data-value");
        var listcolumn = [];
        if (checktext == "image") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnImage;
            if (list.length > 0) {
                var test = '';
                for (var i = 0; i < list.length; i++) {
                    test += `<tr>
                                                                <th scope="row">${list[i].order}</th>
                                                                <td>${list[i].name}</td>
                                                                <td class="url">${list[i].url}</td>
                                                                <td><a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn btn-danger btn-xs btn-deleteTable-image" delete-for="${list[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="4">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }

        if (checktext == "LinkCamera") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnLinkCamera;
            if (list.length > 0) {
                var test = '';
                for (var i = 0; i < list.length; i++) {
                    test += `<tr>
                                                                <th scope="row">${i + 1}</th>
                                                                <td>${list[i].name}</td>
                                                                <td class="url">${list[i].url}</td>
                                                                <td>${list[i].order}</td>
                                                                <td><a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn btn-info btn-xs btn-edit-link" edit-for="${list[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn btn-danger btn-xs btn-deleteTable-link" delete-for="${list[i].id}"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.modal} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="5">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }

        var text = `<tr>`;
        for (var i = 0; i < listcolumn.length; i++) {
            text += `<th scope="col" width="${listcolumn[i].width}%">${listcolumn[i].name}</th>`;
        }
        text += `</tr>`;
        $(modalCreateFolderLayer.SELECTORS.tableThead).html('');
        $(modalCreateFolderLayer.SELECTORS.tableThead).append(text);
    },
    readURL2: function (input, id, elementContent, indexCounter) {
        var typeinput = input.name.split('.').pop().toLowerCase();
        if (typeinput == "jpg" || typeinput == "png" || typeinput == "jpeg" || typeinput == "gif") {
            if (input) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(modalCreateFolderLayer.SELECTORS.fileUploadBtn).css('display', 'inline-block');
                    var text = `<div class="content-image-upload" id="${id}">
                                            <img class="file-upload-image" src="${e.target.result}" alt="your image" title=${input.name}>
                                            <div class="image-title-wrap" style="position: absolute;top: 5px;right: 5px;margin: 0px;padding: 0px;">
                                                <button type="button" data-id=${id} title="Xóa ảnh ${input.name}" class="btn remove-image remove-image-modal" index="${indexCounter}"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                                            </div>
                                        </div>`
                    modalCreateFolderLayer.GLOBAL.listIdImageTemporary.push(id);
                    $(`${elementContent} ${modalCreateFolderLayer.SELECTORS.fileUploadContent}`).append(text);
                    modalCreateFolderLayer.GLOBAL.filecount++;
                };
                reader.readAsDataURL(input);
            }
        }
    },
    deleteAllImageTemporary: function () {
        for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listIdImageTemporary.length; i++) {
            var id = modalCreateFolderLayer.GLOBAL.listIdImageTemporary[i];
            //var elementbuttondelete = document.getElementById(id);
            //elementbuttondelete.parentElement.parentElement.parentNode.removeChild(elementbuttondelete.parentElement.parentElement);
            document.getElementById(id).remove();
            //document.getElementById(id).remove();
        }
        modalCreateFolderLayer.GLOBAL.filecount = 0;
        modalCreateFolderLayer.GLOBAL.listIdImageTemporary = [];
        modalCreateFolderLayer.GLOBAL.listImageInput = [];

    },
    hideBtnSaveImage: function () {
        if (modalCreateFolderLayer.GLOBAL.listIdImageTemporary.length == 0) {
            $(modalCreateFolderLayer.SELECTORS.fileUploadBtn).hide();
        }
    },
    SendFileImage: function (file, content) {
        var formData = new FormData();
        formData.append('file', file.file);
        $.ajax({
            url: "/api/htkt/file/image",
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            async: false,
            success: function (result) {
                modalCreateFolderLayer.UpdateInforFileList(result, content);
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });
        //var object = {
        //    code: "ok",
        //    result: {
        //        id: file.id,
        //        name: file.file.name,
        //        url: file.file.name
        //    }
        //};
        //callback(object);
    },
    SendFileDocument: function (file) {
        var formData = new FormData();
        formData.append('file', file);
        modalCreateFolderLayer.GLOBAL.urlUploadFile = '';
        $(modalCreateFolderLayer.SELECTORS.btnSaveFile).prop('disabled', true);

        $.ajax({
            url: "/api/htkt/file/document",
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            async: false,
            success: function (data) {
                $(modalCreateFolderLayer.SELECTORS.btnSaveFile).prop('disabled', false);
                if (data.code == "ok") {
                    var re = data.result;
                    modalCreateFolderLayer.GLOBAL.urlUploadFile = re.url;
                    $(modalCreateFolderLayer.SELECTORS.btnSaveFile).prop("disabled", false);
                    console.log("2");
                    //abp.notify.success(l("SaveFileDocumentSuccess"));
                }
            },
            error: function (jqXHR) {
            },
            complete: function (jqXHR, status) {
            }
        });
    },
    DeleteFileDocument: function (url) {
        var urlEnCode = encodeURIComponent(url);
        var check = false;
        $.ajax({
            url: "/api/htkt/file/deletedocument",
            type: 'POST',
            data: JSON.stringify({ url: urlEnCode }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (data) {
                if (data.code == "ok") {
                    check = data.result;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + ": " + errorThrown);
            }
        });
        return check;
    },
    UpdateInforFileList: function (data, content) {
        if (data.code == "ok") {
            var re = data.result;
            var count = modalCreateFolderLayer.GLOBAL.listObjectImage.length;
            var ten = (modalCreateFolderLayer.GLOBAL.nameImage !== "") ? modalCreateFolderLayer.GLOBAL.nameImage : re.name
            var item = {
                id: re.id,
                order: count + 1,
                name: ten,
                url: re.url,
            };
            modalCreateFolderLayer.GLOBAL.listObjectImage.push(item);
            modalCreateFolderLayer.updateTable2("image", content);
        }
    },
    DeleteFileImage: function (url) {
        var urlEnCode = encodeURIComponent(url);
        var check = false;
        $.ajax({
            url: "/api/htkt/file/deleteimage",
            type: 'POST',
            data: JSON.stringify({ url: urlEnCode }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (res) {
                if (res.code == "ok") {
                    check = res.result;
                }
                else {
                    console.log(res);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + ": " + errorThrown);
            }
        });
        return check;
    },
    clearLabelError: function () {
        $(modalCreateFolderLayer.SELECTORS.hasError).find(".label-error").remove();
        $(modalCreateFolderLayer.SELECTORS.hasError).removeClass("has-error");
    },
    //link
    checkLink: function (modalForm) {
        modalCreateFolderLayer.clearLabelError();
        let check = true;
        let inputNameLink = $(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputNameLink}`).val();
        if (!validateText(inputNameLink, "text", 0, 0)) {
            insertError($(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputNameLink}`), "other");
            check = false;
            modalCreateFolderLayer.showLabelError(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputNameLink}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:Name")));
        }
        let inputLinkLink = $(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`).val();
        if (!validateText(inputLinkLink, "text", 0, 0)) {
            insertError($(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`), "other");
            check = false;
            modalCreateFolderLayer.showLabelError(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:Path")));
        } else if (!validateText(inputLinkLink, "url", 0, 0)) {
            insertError($(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`), "other");
            check = false;
            modalCreateFolderLayer.showLabelError(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`, jQuery.validator.format(l("ManagementLayer:InvalidDataEntered"), l("ManagementLayer:Path")));
        }

        let checkHtml = mapdata.CheckedHtmlEntities(modalForm);
        if (check) {
            check = checkHtml;
        }

        return check;
    },
    //file
    checkFile: function (modalForm) {
        modalCreateFolderLayer.clearLabelError();
        let check = true;
        let inputNameFile = $(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val();
        if (!validateText(inputNameFile, "text", 0, 0)) {
            insertError($(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`), "other");
            check = false;
            modalCreateFolderLayer.showLabelError(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:DocumentName")));
        }
        let inputLinkFile = $(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val();
        if (!validateText(inputLinkFile, "text", 0, 0)) {
            insertError($(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`), "other");
            check = false;
            modalCreateFolderLayer.showLabelError(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:Folder")));
        }

        //let inputOrderFile = $(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val();
        //if (!validateText(inputOrderFile, "number", 0, 0)) {
        //    insertError($(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`), "other");
        //    check = false;
        //    modalCreateFolderLayer.showLabelError(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`, l("ManagementLayer:DataCannotEmptyAndValid"));
        //}

        let inputDocumentFile = modalCreateFolderLayer.GLOBAL.fileArray.length;                     // $(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files.length;
        if (inputDocumentFile == 0) {
            let value = $('#myModalFileUpload label[name="LabelUrlFile"]').text().trim();
            if (value == "") {
                insertError($(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`), "other");
                check = false;
                modalCreateFolderLayer.showLabelError(`${modalForm} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`, jQuery.validator.format(l("ManagementLayer:DataIsValid"), l("ManagementLayer:Document")));
            }
        }

        let checkHtml = mapdata.CheckedHtmlEntities(modalForm);
        if (check) {
            check = checkHtml;
        }

        return check;

        return check;
    },
    setDataEidtLink: function (obj) {
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputIdLink}`).val(obj.id);
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputNameLink}`).val(obj.name);
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`).val(obj.url);
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputOrderLink}`).val(obj.order);;
    },
    setEmptyLink: function () {
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputNameLink}`).val("");
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputLinkLink}`).val("");
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputOrderLink}`).val(0);
        $(`${modalCreateFolderLayer.SELECTORS.myModalLinkCamera} ${modalCreateFolderLayer.SELECTORS.inputIdLink}`).val("");
    },
    setDataEidtFile: function (obj) {
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputIdFile}`).val(obj.id);
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val(obj.name);
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.labelUrlFile}`).html(obj.url);
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val(obj.order);
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val(obj.idDirectoryProperties);
    },
    setEmptyFile: function () {
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputNameFile}`).val("");
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.labelUrlFile}`).html('');
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputOrderFile}`).val("");
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputIdFile}`).val("");
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).val(
            $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputListDocument} option:first`).val());
        const dt = new DataTransfer();
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputLinkFile}`)[0].files = dt.files;
        modalCreateFolderLayer.GLOBAL.fileArray = [];
        $(`${modalCreateFolderLayer.SELECTORS.modalFileUploadInfor} ${modalCreateFolderLayer.SELECTORS.inputListDocument}`).removeAttr("disabled");
        $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).html('');
        $(modalCreateFolderLayer.SELECTORS.content_file_upload_modal_upload_info).addClass('hidden');
        modalCreateFolderLayer.clearLabelError();
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group").children("label").remove()
        $(element).parents(".form-group").append("<label class='label-error' style='color:red'>" + text + "</label>");
    },
    uuidv4: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    //image
    eventSildeImage: function () {
        modalCreateFolderLayer.addImageSilde();
        $(modalCreateFolderLayer.SELECTORS.imageInfor).on("click", function () {
            modalCreateFolderLayer.GLOBAL.indexCarousel = Number($(this).attr('data-index'));
            $('.carousel-inner .item').removeClass("active");
            $($('.carousel-inner .item')[modalCreateFolderLayer.GLOBAL.indexCarousel]).addClass("active");
            $(modalCreateFolderLayer.SELECTORS.modelViewImage).modal("show");
        });
    },
    addImageSilde: function () {
        let html1 = "";
        //modalCreateFolderLayer.GLOBAL.indexCarousel = 0;
        $.each(modalCreateFolderLayer.GLOBAL.listObjectImage, function (i, obj) {
            let active = (i == 0) ? "active" : "";
            html1 += '<div class="item ' + active + '"><img src = "' + obj.url + '" data-holder-rendered="true"></div>';
        });
        $(modalCreateFolderLayer.SELECTORS.carouselIndicatorsViewImage).children().remove();
        $(modalCreateFolderLayer.SELECTORS.carouselIndicatorsViewImage).append(html1);
    },
    // event step config
    EventStepConfig: function () {
        // step config
        $(modalCreateFolderLayer.SELECTORS.content_step_extend).on('click', `.nav-link`, function () {
            var type = $(this).attr('data-type');
            switch (type) {
                case "image":
                    break;
                case "link":
                    break;
                case "file":
                    break;
                default:
            }
            //modalCreateFolderLayer.resetAll();
            modalCreateFolderLayer.hideBtnSaveImage();
            modalCreateFolderLayer.updateTable2(type, modalCreateFolderLayer.SELECTORS.content_step_extend);

            //var id = $(this).attr("data-value");
            //$(modalCreateFolderLayer.SELECTORS.itemFilePlace).removeClass("activefile");
            //$(this).addClass("activefile");
            //$(modalCreateFolderLayer.SELECTORS.fileUploadFile).css("display", "none");
            //if (id == "image") {
            //    $(modalCreateFolderLayer.SELECTORS.fileUploadFile).css("display", "block");
            //    $(modalCreateFolderLayer.SELECTORS.searchNewfile).hide();
            //} else {
            //    $(modalCreateFolderLayer.SELECTORS.searchNewfile).show();
            //}
            //modalCreateFolderLayer.resetAll();
            //modalCreateFolderLayer.updateTable(id);
        });

        // set select 2 
        $(modalCreateFolderLayer.SELECTORS.inputListDocument).select2();
    },
    UploadTableStepConfig: function (checktext) {

        checktext = typeof checktext !== "undefined" ? checktext : $(modalCreateFolderLayer.SELECTORS.itemPlaceActive).attr("data-value");
        var listcolumn = [];
        $(modalCreateFolderLayer.SELECTORS.searchNewfile).css('display', 'none');
        if (checktext == "image") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnImage;
            if (modalCreateFolderLayer.GLOBAL.listObjectImage !== null && modalCreateFolderLayer.GLOBAL.listObjectImage.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectImage.length; i++) {
                    test += `<tr>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectImage[i].name}</td>
                                                                <td><img src="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].url}" style="width: 100px;" data-index="${i}" class="image-infor"></td>
                                                                <td><a href="javascript:;" title="Xóa" class="delete btn btn-xs btn-deleteTable-image" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectImage[i].id}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><style>.a{fill:none;}.b{fill:red;}</style></defs><path class="a" d="M0,0H24V24H0Z"/><path class="b" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z"/></svg>
                                                                    </a>
                                                                </td>
                                                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
                modalCreateFolderLayer.eventSildeImage();
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="3">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
        }
        if (checktext == "LinkCamera") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnLinkCamera;
            if (modalCreateFolderLayer.GLOBAL.listObjectLinkCamera !== null && modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectLinkCamera.length; i++) {
                    test += `<tr>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].name}</td>
                                                                <td>${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].order}</td>
                                                                <td class="url">${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].url}</td>
                                                                <td><a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn btn-info btn-xs btn-edit-link" edit-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn btn-xs btn-deleteTable-link" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectLinkCamera[i].id}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><style>.a{fill:none;}.b{fill:red;}</style></defs><path class="a" d="M0,0H24V24H0Z"/><path class="b" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z"/></svg>
                                                                    </a>
                                                                </td>
                                                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="4">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
            $(modalCreateFolderLayer.SELECTORS.linkButton).css('display', 'block');
        }
        if (checktext == "FileUpload") {
            listcolumn = modalCreateFolderLayer.GLOBAL.listColumnFile;
            if (modalCreateFolderLayer.GLOBAL.listObjectFile !== null && modalCreateFolderLayer.GLOBAL.listObjectFile.length > 0) {
                var test = '';
                for (var i = 0; i < modalCreateFolderLayer.GLOBAL.listObjectFile.length; i++) {
                    test += `<tr>
                                <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].name}</td>
                                <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].order}</td>
                                <td>${modalCreateFolderLayer.GLOBAL.listObjectFile[i].nameProperties}</td>
                                <td>
                                    <a href="javascript:;" title="${l("ManagementLayer:Edit")}" class="btn-edit-file" edit-for="${modalCreateFolderLayer.GLOBAL.listObjectFile[i].id}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                            <defs>
                                            </defs>
                                            <path class="a" style="fill:none;" d="M0,0H24V24H0Z" />
                                            <path class="b" style="fill: #00559a;" d="M21,6.757l-2,2V4H10V9H5V20H19V17.243l2-2v5.765a.993.993,0,0,1-.993.992H3.993A1,1,0,0,1,3,20.993V8L9,2H20a1,1,0,0,1,1,.992Zm.778,2.05,1.414,1.415L15.414,18,14,18l0-1.412,7.778-7.778Z" />
                                        </svg>
                                    </a>
                                    <a href="javascript:;" title="${l("ManagementLayer:Delete")}" class="delete btn-deleteTable-file" delete-for="${modalCreateFolderLayer.GLOBAL.listObjectFile[i].id}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                            <defs>
                                            </defs>
                                            <path class="a" style="fill:none;" d="M0,0H24V24H0Z" />
                                            <path class="b" style="fill:red;" d="M7,4V2H17V4h5V6H20V21a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V6H2V4ZM6,6V20H18V6ZM9,9h2v8H9Zm4,0h2v8H13Z" />
                                        </svg>
                                    </a>
                                </td>
                            </tr>`;
                    $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                    $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(test);
                }
            } else {
                $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).html('');
                $(`${modalCreateFolderLayer.SELECTORS.content_step_extend} ${modalCreateFolderLayer.SELECTORS.tableTbody}`).append(`<tr>
                                                                            <th scope="row" colspan="4">${l("ManagementLayer:NoData")}</th>
                                                                        </tr>`);
            }
            $(modalCreateFolderLayer.SELECTORS.fileButton).css('display', 'block');
        }
        var text = `<tr>`;
        for (var i = 0; i < listcolumn.length; i++) {
            text += `<th scope="col" width="${listcolumn[i].width}px">${listcolumn[i].name}</th>`;
        }
        text += `</tr>`;
        $(modalCreateFolderLayer.SELECTORS.tableThead).html('');
        $(modalCreateFolderLayer.SELECTORS.tableThead).append(text);
    },
    resetTableData: function () {
        modalCreateFolderLayer.resetAll();
        modalCreateFolderLayer.GLOBAL.listObjectFile = [];
        modalCreateFolderLayer.GLOBAL.listObjectFileDelete = [];
        modalCreateFolderLayer.GLOBAL.listObjectImage = [];
        modalCreateFolderLayer.GLOBAL.listObjectLinkCamera = [];
        $('#nav-image-extend-tab').click(); //reset tab mở rộng về image
    },
    GetContentParent: function () {
        var content = modalCreateFolderLayer.SELECTORS.modal;
        if ($(modalCreateFolderLayer.SELECTORS.modal).hasClass("active")) {
            content = modalCreateFolderLayer.SELECTORS.modal;
        }

        if ($(modalCreateFolderLayer.SELECTORS.form_info).hasClass("active")) {
            content = modalCreateFolderLayer.SELECTORS.content_step_extend;
        }

        if ($(modalCreateFolderLayer.SELECTORS.form_edit).hasClass("active")) {
            content = modalCreateFolderLayer.SELECTORS.edit_parent;
        }

        return content;
    },

    //check change value input form
    CheckChangeValueInputFormBeforeExitsAction: function (content) {
        let check = true;

        $(content + " .modal-layer").each(function () {
            var type = $(this).attr("data-type");

            switch (type) {
                case "radio":
                case "checkbox":
                    $(this).find('input').each(function (i, e) {
                        if ($(this).is(":checked")) {
                            check = false;
                            return;
                        }
                    });
                    break;
                case "list":
                    break;
                case "date":
                    break;
                default:
                    var typeconvert = mapdata.convertcheckTypemodal(type);
                    if (typeconvert != "") {
                        let inputValue = $(this).val();

                        if (inputValue != "") {
                            check = false;
                        }
                    }
                    break;
            }

            if (!check) {
                return;
            }
        });

        return check;
    }
}