﻿mn_selected = "mn_map_data";
var l = abp.localization.getResource('HTKT');
var indexdata = {
    GLOBAL: {
        idDirectory: '',
        lstPropertiesDirectory: [],
        lstIdPropertiesDirectory: [],
        nameDirectory: ''
    },
    CONSTS: {
        URL_AJAXGETDEFAULTDATA: '/api/HTKT/PropertiesDirectory/get-list',
        URL_GETLISTDIRECTORY: "/api/htkt/directory/get-list-layer-has-map",
        URL_GETLISTDIRECTORY_COUNT: '/api/HTKT/PropertiesDirectoryStatistic/get-list-directory-statistic'
    },
    SELECTORS: {
        json: [],      // list dạng tree - parent/children
        item: null,
        listFolder: [], // list dạng mảng,
        listFile: [], // list dạng mảng
        listGroupItem: ".list-group-item",
        btnOpenLayer: "#btnOpenLayer",
        divBorder: ".div-border",
        svgSidebar: ".svg-sidebar",
        sideForm: ".map-side-form"
    },
    init: function () {
        indexdata.setEvent();
        //indexdata.setEventTable();
    },
    setEvent: function () {
        //indexdata.getListPropertiesDirectory();
        //indexdata.getPropertiesDirectory();

        $(document).on('click', indexdata.SELECTORS.divBorder, function () {
            let id = $(this).attr('id');
            let name = $(this).attr('data-name');

            var isShow = true;
            isShow = TreeLayerManagementLayer.CheckPermission(id);
            if (isShow) {
                indexdata.GLOBAL.idDirectory = id;
                indexdata.getTextLayer(indexdata.GLOBAL.idDirectory);
                indexdata.GLOBAL.nameDirectory = name;
                indexdata.updateCountLayer();
                window.location.href = window.location.origin + "/HTKT/ManagementData/MapData?id=" + id;
            }
        });

        $(document).on('click', indexdata.SELECTORS.listGroupItem, function () {
            let id = $(this).attr('id');
            let dataType = $(this).attr('data-type');
            //$(indexdata.SELECTORS.listGroupItem).css('background-color', '#fff');
            if (dataType == "file") {
                indexdata.GLOBAL.idDirectory = id;
                indexdata.GLOBAL.nameDirectory = name;
                //$('#' + id).css('background-color', '#dee2e6');
                //window.location.href = window.location.origin + "/HTKT/ManagementData/MapData?id=" + id;
            }
        });

        $(document).on('dblclick', indexdata.SELECTORS.listGroupItem, function () {
            let dataType = $(this).attr('data-type');
            let id = $(this).attr('id');
            let name = $(this).attr('data-name');
            if (dataType == "file") {
                indexdata.GLOBAL.idDirectory = id;
                indexdata.GLOBAL.nameDirectory = name;
                indexdata.updateCountLayer();
                //indexdata.getTextLayer(indexdata.GLOBAL.idDirectory);
                window.location.href = window.location.origin + "/HTKT/ManagementData/MapData?id=" + indexdata.GLOBAL.idDirectory;
            }
        });

        $(document).on('click', indexdata.SELECTORS.btnOpenLayer, function () {
            if (indexdata.GLOBAL.idDirectory.trim() !== "") {
                indexdata.updateCountLayer();
                //indexdata.getTextLayer(indexdata.GLOBAL.idDirectory);
                window.location.href = window.location.origin + "/HTKT/ManagementData/MapData?id=" + indexdata.GLOBAL.idDirectory;
            }
        });

        $(document).on('click', indexdata.SELECTORS.svgSidebar, function () {
            let svgtext = '';
            if ($(indexdata.SELECTORS.sideForm).hasClass("show")) {
                $(indexdata.SELECTORS.sideForm).removeClass("show");
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <path class="a" style="fill: none;" d="M24,0H0V24H24Z" transform="translate(6)" />
                                <path class="b" style="fill: var(--primary)" d="M26.172,11H4v2H26.172l-5.364,5.364,1.414,1.414L30,12,22.222,4.222,20.808,5.636Z" transform="translate(-4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 4)" />
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 18)" />
                            </svg>`;
                $(".card-title").attr("data-original-title", l("ManagementLayer:OpenPanel"));
            } else {
                $(indexdata.SELECTORS.sideForm).addClass("show");
                svgtext += `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <g transform="translate(1988 32)">
                                    <g transform="translate(-1988 -32)">
                                        <path style="fill: none;" class="a" d="M0,0H24V24H0Z"></path>
                                        <path style="fill: var(--primary)" class="b" d="M7.828,11H30v2H7.828l5.364,5.364-1.414,1.414L4,12l7.778-7.778,1.414,1.414Z"></path>
                                    </g>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -28)"></rect>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -14)"></rect>
                                </g>
                            </svg>`;
                $(".card-title").attr("data-original-title", l("ManagementLayer:ClosePanel"));
            }
            $(this).html('');
            $(this).html(svgtext);
        });
    },
    updateCountLayer: function () {
        var object = {
            idDirectory: indexdata.GLOBAL.idDirectory.trim(),
            count: 0
        };
        $.ajax({
            type: "POST",
            url: "/api/HTKT/PropertiesDirectoryStatistic/create",
            data: JSON.stringify(object),
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //console.log('ok');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    setEventTable: function () {
        $.ajax({
            type: "GET",
            url: indexdata.CONSTS.URL_GETLISTDIRECTORY,
            data: {
            },
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if (res.code == "ok")
                {
                    var data = res.result;
                    var array = [];
                    if (data.length > 0) {
                        indexdata.SELECTORS.listFile = data.filter(x => x.type == "file");
                        indexdata.SELECTORS.listFolder = data;
                        indexdata.SELECTORS.json = indexdata.FormatTree(indexdata.SELECTORS.listFolder);
                        item = indexdata.SELECTORS.json[0];
                        //$('#tree').bstreeview({ data: JSON.stringify(indexdata.SELECTORS.json) });
                        //TreeView.GetInforTreeView();
                        $('#' + item.id).trigger('click');
                        //indexdata.GetDirectoryCount();

                    }
                }
                else {
                    console.log(res)
                }

                $('.spinner').css('display', 'none');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    GetDirectoryCount: function () {
        $.ajax({
            type: "POST",
            url: indexdata.CONSTS.URL_GETLISTDIRECTORY_COUNT,
            data: JSON.stringify(ManagementLayer.GLOBAL.ListIdShow),
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //console.log(data);
                if (data.code == "ok") {
                    var text = '';
                    let order = 1;

                    var lstDataCount = data.result;

                    if (ManagementLayer.GLOBAL.isAdmin.toLowerCase() == "false") {
                        lstDataCount = [];
                        var checkDataPermission = data.result.filter(x => ManagementLayer.GLOBAL.lstIdLayer.some(y => y == x.idDirectory));
                        if (checkDataPermission != undefined && checkDataPermission.length < 10) {
                            var dem = 10 - checkDataPermission.length;

                            $.each(checkDataPermission, function (index, obj) {
                                lstDataCount.push({
                                    id: obj.id,
                                    idDirectory: obj.id,
                                    image2D: obj.image2D,
                                    isDeleted: obj.isDeleted,
                                    level: obj.level,
                                    name: obj.name
                                });
                            });

                            $.each(indexdata.SELECTORS.listFile, function (index, obj) {
                                if (dem == 0) {
                                    return;
                                }

                                if (obj.type.toLowerCase() != "folder") {
                                    var checkDuplicate = lstDataCount.find(x => x.id == obj.id); // kiểm tra danh sach hiển thị đã co hay chưa
                                    if (checkDuplicate == undefined) {
                                        var directory = ManagementLayer.GLOBAL.lstIdLayer.find(x => x == obj.id);
                                        if (directory != undefined) {
                                            lstDataCount.push({
                                                id: obj.id,
                                                idDirectory: obj.id,
                                                image2D: obj.image2D,
                                                isDeleted: obj.isDeleted,
                                                level: obj.level,
                                                name: obj.name
                                            });
                                            dem--;
                                        }
                                    }
                                }
                            });
                        }
                    }

                    $.each(lstDataCount, function (i, obj) {
                        var directory = indexdata.SELECTORS.listFile !== null && indexdata.SELECTORS.listFile.length > 0 ? indexdata.SELECTORS.listFile.find(x => x.id == obj.idDirectory) : obj;
                        if (directory != undefined) {
                            var classname = "";
                            if (order <= 5) {
                                classname += " div-border-bottom"
                            }
                            if (order % 5 !== 0) {
                                classname += " div-border-right"
                            }

                            text += `<div class="div-border ${classname} position-relative" id="${directory.id}" data-name="${directory.name}">
                                        <div class="content-layer">
                                            ${obj.image2D != "" && obj.image2D != null ?
                                    `<img src="${obj.image2D}" class="image-avatar">` :
                                    `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lightning-fill svg-avatar" viewBox="0 0 16 16">
                                                <path d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                            </svg>`}
                                            <div class="name-avatar">
                                                <span style="font-weight: 500;">${directory.name}</span>
                                            </div>
                                        </div>
                                </div>`;
                            $(".file-upload-content").html('');
                            $(".file-upload-content").append(text);
                            order++;
                        }
                    });
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    FormatTree: function (a) {
        var tree = [],
            mappedArr = {},
            arrElem,
            mappedElem;
        for (var i = 0, len = a.length; i < len; i++) {
            arrElem = a[i];
            //let code = a[i].codeTypeDirectory !== null && a[i].codeTypeDirectory.length > 1 ? a[i].codeTypeDirectory : "";
            //arrElem.text = "<div><span>" + a[i].name + "</span><span>" + code +"</span></div>";
            arrElem.text = a[i].name;
            if (arrElem.type == "folder") {
                arrElem.icon = "fa fa-folder"
            }
            if (arrElem.type == "file") {
                arrElem.icon = "fa fa-database"
            }
            arrElem.id = a[i].id;
            mappedArr[arrElem.id] = arrElem;
            mappedArr[arrElem.id]['nodes'] = [];
        }


        for (var id in mappedArr) {
            if (mappedArr.hasOwnProperty(id)) {
                mappedElem = mappedArr[id];
                if (mappedElem.parentId) {
                    if (typeof mappedArr[mappedElem.parentId] !== "undefined") {
                        mappedArr[mappedElem['parentId']]['nodes'].push(mappedElem);
                    }
                }
                else {
                    tree.push(mappedElem);
                }
            }
        }
        return tree;

    },
    showDirectory: function () {
        var file = indexdata.SELECTORS.listFile.sort((a, b) => parseFloat(b.count) - parseFloat(a.count)).slice(0, 10);
        var text = '';
        $.each(file, function (i, obj) {
            //let check = indexdata.GLOBAL.lstPropertiesDirectory.find(x => x.idDirectory == obj.id);
            //if (check != null && check != undefined) {
            text += `<div class="div-border position-relative" id="${obj.id}" data-name="${obj.name}">
                        <div class="content-layer">
                                        ${obj.image2D != "" && obj.image2D != null ?
                    `<img src="${obj.image2D}" class="image-avatar">` :
                    `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lightning-fill svg-avatar" viewBox="0 0 16 16">
                                            <path d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                        </svg>`}
                                        <div class="name-avatar">
                                            <span style="font-weight: 900;">${obj.name}</span>
                                        </div>
                                    </div>
                        </div>`;
            $(".file-upload-content").html('');
            $(".file-upload-content").append(text);
            //}
        });
    },
    getTextLayer: function (id) {
        if (typeof id !== "undefined" && id !== "" && id !== null) {
            var parent = id;
            var text = "", name = "";
            while (parent !== null) {
                var obj = TreeLayerManagementLayer.GLOBAL.ArrayTree.find(x => x.id == parent);
                if (typeof obj !== "undefined" && obj !== null) {
                    text = (text.length <= 0) ? obj.name : (obj.name + ' - ' + text);
                    parent = obj.parentId;
                    if (name.length <= 0) {
                        name = obj.name
                    }
                } else break;
            }
            localStorage.setItem("namelayer", name);
            localStorage.setItem("treeviewlayer", text);
        }
    },
}