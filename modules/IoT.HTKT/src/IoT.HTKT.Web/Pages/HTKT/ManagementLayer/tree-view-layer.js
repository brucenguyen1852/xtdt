﻿var TreeLayerManagementLayer = {
    GLOBAL: {
        ArrayTree: [],
        ArrayNodeCheck: [],
        DirectoryForcus: "",
        idDirectory: '',
        levelMax: 0
    },
    CONSTS: {
        URL_GETLISTDIRECTORY: "/api/htkt/directory/get-list-new-layer",
        URL_GETLISTDIRECTORYHASMAP: "/api/htkt/directory/get-list-layer-has-map",
        //URL_GET_PROPERTIES_DIRECTORY: "/api/app/khaiThacPropertiesDirectory"
    },
    SELECTORS: {
        tree: "#checkTree",
        dyntree_node_text: ".dynatree-title",
        search_tree: ".search-tree",
        nameFolderParent: ".name-folder-parent",
        divParentTree: "#div-parent-tree",
        content_left_sidebar: '.map-side-form .scroll-content',
    },
    init: function () {
        TreeLayerManagementLayer.setEvent();
        TreeLayerManagementLayer.GetListLayer();
        TreeLayerManagementLayer.OpenCurrentData();
    },
    setEvent: function () {
        $(TreeLayerManagementLayer.SELECTORS.divParentTree).on('click', function () {
            var id = $(this).attr('data-id');
            TreeLayerManagementLayer.GLOBAL.idDirectory = id;
            TreeLayerManagementLayer.GLOBAL.DirectoryForcus = id;
            thisId = id;
            parentId = id;
            level = 1;
            $('.item-li').css('background-color', 'transparent');
            $(this).css('background-color', '#ddd');
            //$(TreeView.SELECTORS.addFolder).prop('disabled', false);
        });

        $(TreeLayerManagementLayer.SELECTORS.tree).on('click', TreeLayerManagementLayer.SELECTORS.dyntree_node_text, function () {
            var folder = $(this).parent().attr('data-folder');
            //var id = $(this).attr('data-id');
            if (folder != "true") {
                $(this).parent().children().eq(1).click();
            }
        })

        $(".tree-o-i").on('click', '.item-li', function () {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');
            $(TreeLayerManagementLayer.SELECTORS.divParentTree).css('background-color', 'transparent');
            if (id != null && id != undefined) {
                //if ($(`#checkmark-${id}`).hasClass('check')) {
                if (TreeLayerManagementLayer.GLOBAL.DirectoryForcus != id) {
                    if (!$('#lst-Object').hasClass('open')) {
                        $('#lst-Object').addClass('open')
                    }

                    $('.item-li').removeClass('forcus');
                    $(this).addClass('forcus');
                }
                //}

                if (!$(this).next().hasClass('open')) {
                    $(this).next().addClass('open');
                    $(this).next().slideToggle("slow");

                    var element = $(this).find('.icon-menu-tree');
                    if (element.length > 0) {
                        element.addClass('open')
                    }
                }
                else {
                    $(this).next().slideToggle("slow");
                    $(this).next().removeClass('open');
                    let element = $(this).find('.icon-menu-tree');
                    if (element.length > 0) {
                        element.removeClass('open')
                    }
                }

            }
        });

        $(".tree-o-i").on('dblclick', '.item-li', function () {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');
            var name = $(this).attr('data-name');
            if (id != null && id != undefined) {

                var isShow = true;
                isShow = TreeLayerManagementLayer.CheckPermission(id);

                if (isShow) {
                    if (TreeLayerManagementLayer.GLOBAL.DirectoryForcus != id) {
                        if (!$('#lst-Object').hasClass('open')) {
                            $('#lst-Object').addClass('open')
                        }
                        TreeLayerManagementLayer.GLOBAL.idDirectory = id;
                        TreeLayerManagementLayer.GLOBAL.DirectoryForcus = id;
                        $('.item-li').css('background-color', 'transparent');
                        $(this).css('background-color', '#ddd');
                        if (!type || type == "false") {
                            indexdata.GLOBAL.idDirectory = id;
                            indexdata.GLOBAL.nameDirectory = name;
                            indexdata.updateCountLayer();
                            indexdata.getTextLayer(indexdata.GLOBAL.idDirectory);
                            window.location.href = window.location.origin + "/HTKT/ManagementData/MapData?id=" + indexdata.GLOBAL.idDirectory;
                        } else {

                        }
                    }
                }

            }
        });

        $(".tree-o-i").on('click', '.checkmark', function () {
            let id = $(this).attr('data-id');

            if (!$(this).hasClass('check')) {
                if (TreeLayerManagementLayer.GLOBAL.idDirectory != "") {
                    $('#tree-item-' + TreeLayerManagementLayer.GLOBAL.idDirectory + ' input').prop('checked', false);
                }
                TreeLayerManagementLayer.GLOBAL.DirectoryForcus = id;
                TreeLayerManagementLayer.GLOBAL.idDirectory = id;
                $(this).addClass('check');
                TreeLayerManagementLayer.GetInfoTree();
            }
            else {
                $(this).removeClass('check');
                TreeLayerManagementLayer.GLOBAL.DirectoryForcus = "";
                TreeLayerManagementLayer.GLOBAL.idDirectory = "";
            }
        });

        $(TreeLayerManagementLayer.SELECTORS.search_tree).on('keyup', delay(function () {
            $('.treeview-not-result').remove();

            $('.tree-o-i .treeview').hide();
            var value = $(this).val().toLowerCase().trim();
            if (value != "") {
                $('.tree-o-i .treeview').each(function () {
                    //search_tree(this, value);
                    var id = $(this).attr('data-id');
                    var name = decodeURIComponent($(this).attr('data-search'));
                    var parent = $(this).attr('data-parent');
                    var level = parseInt($(this).attr('data-level'));
                    var folder = $(this).attr('data-type');
                    var chil = parseInt($(this).attr('data-chil'));
                    if (level == 0) {
                        $(this).show();
                    }
                    if (xoa_dau(name.trim().toLowerCase()).includes(xoa_dau(value.trim().toLowerCase()))) {
                        $(this).show();

                        if (chil > 0) {
                            $('#tree-item-' + id).find('.icon-menu-tree-' + id).addClass('open'); // mở open icon
                            $('#tree-ul-' + id).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + id).slideDown();
                            $('#tree-ul-' + id + " li").show();
                        }

                        if (parent != "null") {
                            $('#tree-item-' + parent).find('.icon-menu-tree-' + id).addClass('open'); // mở open icon
                            $('#tree-ul-' + parent).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + parent).slideDown();

                            search_tree('#tree-item-' + parent, value);
                        }
                    }
                });

                var countNotResult = $(" .treeview").filter(function () {
                    return $(this).css('display') == 'none'
                }).length;

                if ($(".treeview").length == countNotResult) {
                    var li = `<div class="treeview-not-result"><span>${l("ManagementLayer:NoSearchResult")}</span></div>`;
                    $('.tree-o-i').append(li);
                }
            }
            else {
                TreeLayerManagementLayer.ReloadTree();
                $(TreeLayerManagementLayer.SELECTORS.treeview).show();
            }
        }, 500));

        function search_tree(element, value) {
            if ($(element).css("display") == "none") {
                $(element).show();
                var id = $(element).attr('data-id');
                var name = decodeURIComponent($(element).attr('data-search'));
                var parent = $(element).attr('data-parent');
                var level = parseInt($(this).attr('data-level'));
                if (level == 0) {
                    $(this).show();
                }
                if (name != undefined) {
                    //if (name.toLowerCase().includes(value))
                    {
                        if (parent != "null") {
                            // hiển thị ul của cây
                            $('#tree-item-' + parent).find('.icon-menu-tree-' + id).addClass('open'); // mở open icon
                            $('#tree-ul-' + parent).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + parent).slideDown();

                            search_tree('#tree-item-' + parent, value);
                        }
                    }
                }
            }
        }

    },
    GetListLayer: function (idfocus) {
        indexdata.GetDirectoryCount();
        $.ajax({
            type: "GET",
            url: TreeLayerManagementLayer.CONSTS.URL_GETLISTDIRECTORYHASMAP,
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if (res.code == "ok")
                {
                    var data = res.result;
                    if (data.length > 0) {
                        //TreeLayerManagementLayer.GLOBAL.ArrayTree = data;
                        if (ManagementLayer.GLOBAL.isAdmin == "True") {
                            TreeLayerManagementLayer.GLOBAL.ArrayTree = data;
                            //indexdata.GetDirectoryCount();
                        } else {
                            TreeLayerManagementLayer.GetListIdLayerPermission(data);
                            TreeLayerManagementLayer.GLOBAL.ArrayTree = data.filter(x => ManagementLayer.GLOBAL.ListIdShow.includes(x.id) || x.level == "0");
                            //indexdata.GetDirectoryCount();
                        }
                        listFolder = TreeLayerManagementLayer.GLOBAL.ArrayTree;
                        indexdata.SELECTORS.listFile = listFolder.filter(x => x.type == "file");
                        indexdata.SELECTORS.listFolder = listFolder;
                        indexdata.SELECTORS.json = indexdata.FormatTree(indexdata.SELECTORS.listFolder);
                        var parent = TreeLayerManagementLayer.GLOBAL.ArrayTree.find(x => x.level == "0");
                        var treedynamic = TreeLayerManagementLayer.FillDynaTree(parent);
                        //var arrayTree = [];
                        //arrayTree.push(treedynamic);
                        //TreeLayerManagementLayer.LoadDynaTree(arrayTree);
                        var count = TreeLayerManagementLayer.GLOBAL.ArrayTree.filter(x => x.level == "1");
                        $(TreeLayerManagementLayer.SELECTORS.nameFolderParent).text(` ${parent.name} (${count.length})`);
                        $(TreeLayerManagementLayer.SELECTORS.divParentTree).attr("data-id", parent.id);
                        TreeLayerManagementLayer.GLOBAL.idDirectory = parent.id;
                        TreeLayerManagementLayer.GLOBAL.DirectoryForcus = parent.id;
                        thisId = parent.id;
                        parentId = parent.id;
                        level = 1;
                        //console.log(treedynamic);
                        var html = TreeLayerManagementLayer.DataTreeGen(treedynamic);

                        $(`.tree-o-i`).html(html);

                        $(`.item-li`).each(function () {
                            if ($(this).find('.title-item').text() == "") {
                                $(this).remove();
                            }

                            var isFolder = $(this).attr('data-type');
                            var id = $(this).attr('data-id');

                            //if (isFolder != "true") {
                            //    var checkbox = `<label class="container-checkbox">                                             
                            //                      <span class="checkmark" id="checkmark-${id}" data-id="${id}"></span>
                            //                    </label>`;

                            //    $(this).parent().prepend(checkbox);
                            //}
                        });
                        if (idfocus != null && idfocus != undefined) {
                            $(TreeLayerManagementLayer.SELECTORS.TreeLayerManagementLayer).animate({
                                scrollTop: $('#item-li-' + idfocus).offset().top - $(TreeLayerManagementLayer.SELECTORS.TreeLayerManagementLayer).offset().top
                            }, 'slow');
                            $('#item-li-' + idfocus).trigger('click');
                        }
                    }
                }
                else {
                    console.log(res)
                }

            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    GetListIdLayerPermission: function (data) {
        var lst = data.filter(x => ManagementLayer.GLOBAL.lstIdLayer.indexOf(x.id) >= 0);
        var lstcheck = [];
        $.each(lst, function (i, obj) {
            let listid = TreeLayerManagementLayer.getListIdParent(obj, data);
            lstcheck = lstcheck.concat(listid);
        });

        ManagementLayer.GLOBAL.ListIdShow = [...new Set(lstcheck)];
    },
    getListIdParent: function (obj, data) {
        let lst = [obj.id];
        if (obj.parentId != "" && obj.parentId != null) {
            let check = data.find(x => x.id == obj.parentId);
            if (check != null && check != undefined) {
                let lstcheck = TreeLayerManagementLayer.getListIdParent(check, data);
                lst = lst.concat(lstcheck);
            }
        }
        return lst;
    },
    //dynatree
    FillDynaTree: function (data) {
        if (TreeLayerManagementLayer.GLOBAL.levelMax < data.level) {
            TreeLayerManagementLayer.GLOBAL.levelMax = data.level;
        }
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            icon: null,
            level: data.level,
            parentId: data.parentId
        };

        var parent = TreeLayerManagementLayer.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "" && data.image2D != "null") {
                    obj.icon = data.image2D;
                }
            }

            obj.key = parent.id;

            var lstChil = TreeLayerManagementLayer.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = TreeLayerManagementLayer.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    // autoGen
    DataTreeGen: function (data) {
        //$('.title').html(data.title);

        var tree = data.children;

        var html = ``;

        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;
            if (tree[i].isFolder) {
                icon = `<svg id="Group_1232" data-name="Group 1232" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                              <path id="Path_388" data-name="Path 388" d="M0,0H24V24H0Z" fill="none"/>
                              <path id="Path_389" data-name="Path 389" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H4V19l2-8H22.5l-2.31,9.243a1,1,0,0,1-.97.757Z" fill="var(--primary)"/>
                            </svg>`;
            }
            else {
                icon = `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                <defs></defs>
                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z"></path>
                                <path class="b " style="fill:var(--primary)" d="M21,9V20.993A1,1,0,0,1,20.007,22H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,4,2H14V8a1,1,0,0,0,1,1Zm0-2H16V2Z"></path>
                            </svg>`;
            }

            if (tree[i].children.length > 0) {
                html += `<li id="tree-item-${tree[i].key}" data-id="${tree[i].key}" data-type="${tree[i].isFolder}" data-level="${tree[i].level}" data-chil="${tree[i].children.length}" class="treeview" style="height: auto;" data-search="${encodeURIComponent(name.toLowerCase())}" data-parent="${tree[i].parentId}">
                            <div href="javascript:;" class="item-li" id="item-li-${tree[i].key}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                 <span class="pull-left-container">
                                    <i class="fa fa-angle-right pull-left icon-menu-tree"></i>
                                </span>
                                <span class="title-item">${icon} <span class="name-layer-item">${name} (${tree[i].children.length})</span></span>
                            </div>

                            <ul class="tree-ul" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:none;">`;

                html += TreeLayerManagementLayer.DataTreeGen(tree[i]);

                html += `
                             </ul>
                        </li>`;
            }
            else {
                html += `<li id="tree-item-${tree[i].key}"  data-id="${tree[i].key}" data-type="${tree[i].isFolder}" data-level="${tree[i].level}" data-chil="${tree[i].children.length}"  class="treeview" style="height: auto;" data-search="${encodeURIComponent(name.toLowerCase())}"  data-parent="${tree[i].parentId}">
                                <div href="javascript:;" class="item-li" id="item-li-${tree[i].key}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                <span class="pull-left-container">

                                </span>
                                <span class="title-item">${icon} <span class="name-layer-item"> ${name}</span></span></a>    
                           </li>`;
            }
        }

        return html;
    },
    GetPropertiesDirectory: function (id) {
        var result = null;
        $.ajax({
            type: "GET",
            url: TreeLayerManagementLayer.CONSTS.URL_GET_PROPERTIES_DIRECTORY + "/" + id + "/asyncById",
            data: {
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                result = data;
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });

        return result;
    },
    GetInfoTree: function () {
        if ($('#InforData').width() == 0) {
            $('#InforData').animate({
                width: 372,
            }, 200);
        }

        //$(TreeView.SELECTORS.listGroupItem).css('background-color', 'white');
        //var idSelected = thisId;
        //TreeView.setHideOrShowFolder(true);
        //TreeLayerManagementLayer.GLOBAL.idDirectory = id;
        $.ajax({
            type: "GET",
            url: '/api/HTKT/PropertiesDirectory/get-directoryById',
            data: {
                id: TreeLayerManagementLayer.GLOBAL.idDirectory
            },
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok") {
                    $('#DataName').val(data.result.nameTypeDirectory);
                    $('#h5NameData').html('Lớp: ' + data.result.nameTypeDirectory);
                    $('#DataCode').val(data.result.codeTypeDirectory);
                    $('#Version').val(data.result.verssionTypeDirectory);
                    if (data.result.location == null) {
                        $('#DataLat').val("");
                        $('#DataLng').val("");
                    }
                    else {
                        $('#DataLat').val(data.result.location.lat);
                        $('#DataLng').val(data.result.location.lng);
                    }

                    if (data.result.zoom != null) {
                        $('#DataZoom').val(data.result.zoom);
                    }
                    else {
                        $('#DataZoom').val("");
                    }
                    $(config.SELECTORS.NameImplement).val(data.result.idImplement);
                    let nameImplement = listFolder.find(x => x.id == data.result.idImplement);
                    nameImplement = typeof nameImplement !== "undefined" && nameImplement !== null ? nameImplement.name + " (" + nameImplement.codeTypeDirectory + ")" : ""
                    $(TreeView.SELECTORS.inputnameImplement).val(nameImplement);
                    $(TreeView.SELECTORS.inputUrlMaptile).val(data.result.groundMaptile);
                    var tags = data.result.tags;

                    if (tags != null) {
                        $(TreeView.SELECTORS.inputBoundMaptile).val(tags["bounds"]);
                    }
                    else {
                        //$(TreeView.SELECTORS.inputBoundMaptile).val(bounds);
                        $(TreeView.SELECTORS.inputBoundMaptile).val("");
                    }

                    var result = [];
                    $.each(data.result.listProperties, function (i, obj) {
                        var element = {
                            NameProperties: obj.nameProperties,
                            CodeProperties: obj.codeProperties,
                            TypeProperties: obj.typeProperties,
                            IsShow: obj.isShow,
                            IsIndexing: obj.isIndexing,
                            IsRequest: obj.isRequest,
                            IsView: obj.isView,
                            IsHistory: obj.isHistory,
                            IsInheritance: obj.isInheritance,
                            IsAsync: obj.isAsync,
                            IsShowExploit: obj.isShowExploit,
                            DefalutValue: obj.defalutValue,
                            ShortDescription: obj.shortDescription,
                            TypeSystem: obj.typeSystem,
                            OrderProperties: obj.orderProperties
                        };
                        result.push(element);
                    });
                    config.GLOBAL.table = result;
                    config.addTbodyTr();
                    config.setEventAffter();
                }
                else {
                    console.log(data)
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
        if ($(config.SELECTORS.collapseOne).hasClass("collapse in")) {
            $(config.SELECTORS.collapseOne).removeClass('in');
            $(config.SELECTORS.iconHeadingOne).removeClass('fa-minus').addClass('fa-plus');
        }
        if (!$(config.SELECTORS.collapseTwo).hasClass("collapse in")) {
            $(config.SELECTORS.collapseTwo).addClass('in');
            $(config.SELECTORS.iconHeadingTwo).removeClass('fa-plus').addClass('fa-minus');
        }
    },
    ReloadTree: function () {
        var parent = TreeLayerManagementLayer.GLOBAL.ArrayTree.find(x => x.level == "0");
        var treedynamic = TreeLayerManagementLayer.FillDynaTree(parent);

        var html = TreeLayerManagementLayer.DataTreeGen(treedynamic);

        $(`.tree-o-i`).html('');
        $(`.tree-o-i`).html(html);
    },
    OpenCurrentData: function () {
        var url = new URL(window.location.href);
        var checkid = url.searchParams.get("id");
        if (checkid !== null) {
            setTimeout(function () {
                $(TreeLayerManagementLayer.SELECTORS.content_left_sidebar).animate({
                    scrollTop: $('#item-li-' + checkid).offset().top - $(TreeLayerManagementLayer.SELECTORS.content_left_sidebar).offset().top
                }, 'slow');

                $('#item-li-' + checkid).click();
                $('#item-li-' + checkid).addClass('active');
            }, 500)

            var parent = $('#item-li-' + checkid).parent().attr('data-parent');

            if (parent != "null") {
                var chil = parseInt($('#item-li-' + parent).attr('data-chil'));
                if (chil > 0) {
                    $('#tree-ul-' + parent).addClass('open');
                    $('#tree-ul-' + parent).slideDown();
                    OpenTree($('#item-li-' + parent));

                }
            }
        };

        function OpenTree(element) {
            var parent = $(element).parent().attr('data-parent');

            if (parent != "null") {
                var chil = parseInt($('#item-li-' + parent).attr('data-chil'));
                if (chil > 0) {
                    $('#tree-ul-' + parent).addClass('open');
                    $('#tree-ul-' + parent).slideDown();
                    OpenTree($('#item-li-' + parent));
                }
            }
        }
    },
    CheckPermission: function (id) {
        var isShow = true;
        if (ManagementLayer.GLOBAL.isAdmin.toLowerCase() == "false") {
            isShow = false
        }

        if (!isShow) {
            var checkExits = ManagementLayer.GLOBAL.lstLayerPermission.find(x => x.idDirectory == id);
            if (checkExits != undefined) {
                isShow = true;

                var perMissionLayer = checkExits.listPermisson.filter(x => x != "PermissionLayer.VIEW");
                if (perMissionLayer == undefined) {
                    isShow = false;
                }
                else {
                    if (perMissionLayer.length == 0) {
                        isShow = false;
                    }
                }


            }
        }

        return isShow;
    }
}


function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    str = str.replace(/\s/g, '');
    return str;
}
