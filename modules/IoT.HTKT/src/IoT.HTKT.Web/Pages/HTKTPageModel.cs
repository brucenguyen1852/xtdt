﻿using IoT.HTKT.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace IoT.HTKT.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class HTKTPageModel : AbpPageModel
    {
        protected HTKTPageModel()
        {
            LocalizationResourceType = typeof(HTKTResource);
            ObjectMapperContext = typeof(HTKTWebModule);
        }
    }
}