﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace IoT.KhaiThac
{
    [DependsOn(
        typeof(KhaiThacDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class KhaiThacApplicationContractsModule : AbpModule
    {

    }
}
