﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.KhaiThac.Layer
{
    public class CreateDirectoryDto
    {
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string ParentId { get; set; }
        public string Search { get; set; }
        public bool TypeDirectory { get; set; }
    }
}
