﻿using IoT.KhaiThac.PropertiesDirectory;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.KhaiThac.Layer
{
    public class DirectoryProperties
    {
        public string Id { get; set; }
        public string IdDirectory { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string ParentId { get; set; }
        public string Search { get; set; }
        public string NameTypeDirectory { get; set; } //Tên loại dữ liệu
        public string CodeTypeDirectory { get; set; } //Mã loại dữ liệu
        public int VerssionTypeDirectory { get; set; } //Phiên bản
        public string IdImplement { get; set; } // kế thừa
        public List<DetailProperties> ListProperties { get; set; } // Bảnh thuộc tính
        public int Count { get; set; }
        public string Image2D { get; set; } // ảnh icon map
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
