﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.KhaiThac.ManagementData
{
    public class CreateUpdateDetailObjectDto
    {
        public string IdMainObject { get; set; }
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
    }
}
