﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.KhaiThac.ManagementData
{
    public interface IKhaiThacDetailObjectService : ICrudAppService< //Defines CRUD methods
            DetailObjectDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateDetailObjectDto> //Used to create/update a PropertiesDirectory
    {
        List<DetailObjectDto> GetPropertiesByArrayMainObject(List<string> array);

        List<DetailObjectDto> SearchPropertiesObjectByArrayDirectoryId(FormSearchPropeties dto);
    }
}
