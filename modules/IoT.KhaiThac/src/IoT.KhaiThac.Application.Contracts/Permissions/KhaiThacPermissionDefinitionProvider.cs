﻿using IoT.KhaiThac.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace IoT.KhaiThac.Permissions
{
    public class KhaiThacPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            //var myGroup = context.AddGroup(KhaiThacPermissions.GroupName, L("Permission:KhaiThac"));

            //var exploitManagementDataPermission = myGroup.AddPermission(KhaiThacPermissions.ExploitView.Default, L("Permission:KhaiThac"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<KhaiThacResource>(name);
        }
    }
}