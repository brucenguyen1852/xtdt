﻿using Volo.Abp.Reflection;

namespace IoT.KhaiThac.Permissions
{
    public class KhaiThacPermissions
    {
        public const string GroupName = "KhaiThac";

        public static class ExploitView
        {
            public const string Default = GroupName + ".Default";
        }

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(KhaiThacPermissions));
        }
    }
}