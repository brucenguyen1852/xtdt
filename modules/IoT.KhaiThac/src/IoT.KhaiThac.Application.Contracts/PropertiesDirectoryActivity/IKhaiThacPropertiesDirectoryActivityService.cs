﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.KhaiThac.PropertiesDirectoryActivity
{
    public interface IKhaiThacPropertiesDirectoryActivityService : ICrudAppService< //Defines CRUD methods
            PropertiesDirectoryActivityDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePropertiesDirectoryActivityDto> //Used to create/update a book
    {
        Task<PropertiesDirectoryActivityDto> GetDirectoryActivityInfo(string id);
    }
}
