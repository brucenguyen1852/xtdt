﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.KhaiThac.PropertiesSetting
{
    public class CreatePropertiesSettingDto
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public int MinZoom { get; set; } //zoom nhỏ nhất
        public int MaxZoom { get; set; }
        public string Image2D { get; set; } // ảnh đại điện
        public string Object3D { get; set; } // json url
        public string Texture3D { get; set; } // json url
        public string Stroke { get; set; }  // màu viền
        public int StrokeWidth { get; set; } // độ rộng của viền
        public string StrokeOpacity { get; set; } // độ mờ của viền 100% / 100
        public string StyleStroke { get; set; } // Kiểu đường
        public string Fill { get; set; } //màu của vùng
        public string FillOpacity { get; set; } //độ mờ của vùng 100% / 100
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
