﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.KhaiThac.PropertiesSetting
{
    public interface IKhaiThacPropertiesSettingService : ICrudAppService< //Defines CRUD methods
             PropertiesSettingDto, //Used to show books
             Guid, //Primary key of the book entity
             PagedAndSortedResultRequestDto, //Used for paging/sorting
             CreatePropertiesSettingDto> //Used to create/update a book
    {
        Task<PropertiesSettingDto> FindPropertyByIdDirection(string id);
        Task<List<PropertiesSettingDto>> GetAllSettingAsync();
    }
}
