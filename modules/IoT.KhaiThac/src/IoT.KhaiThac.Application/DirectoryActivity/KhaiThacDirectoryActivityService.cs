﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.KhaiThac.DirectoryActivity
{
    public class KhaiThacDirectoryActivityService : CrudAppService<
           IoT.KhaiThac.ManageActivity.DirectoryActivity, //The Book entity
           DirectoryActivityDto, //Used to show books
           Guid, //Primary key of the book entity
           PagedAndSortedResultRequestDto, //Used for paging/sorting
           CreateDirectoryActivityDto>, //Used to create/update a book
       IKhaiThacDirectoryActivityService //implement the IBookAppService
    {
        private readonly IRepository<IoT.KhaiThac.ManageActivity.PropertiesDirectoryActivity> _propertiesDirectoryActivities;
        public KhaiThacDirectoryActivityService(IRepository<IoT.KhaiThac.ManageActivity.DirectoryActivity, Guid> repository,
                                       IRepository<IoT.KhaiThac.ManageActivity.PropertiesDirectoryActivity> propertiesDirectoryActivities) : base(repository)
        {
            _propertiesDirectoryActivities = propertiesDirectoryActivities;
        }
    }
}
