﻿using IoT.KhaiThac.Localization;
using Volo.Abp.Application.Services;

namespace IoT.KhaiThac
{
    public abstract class KhaiThacAppService : ApplicationService
    {
        protected KhaiThacAppService()
        {
            LocalizationResource = typeof(KhaiThacResource);
            ObjectMapperContext = typeof(KhaiThacApplicationModule);
        }
    }
}
