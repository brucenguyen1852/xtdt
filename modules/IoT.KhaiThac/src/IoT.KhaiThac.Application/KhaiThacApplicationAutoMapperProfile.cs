﻿using AutoMapper;
using IoT.KhaiThac.DirectoryActivity;
using IoT.KhaiThac.Layer;
using IoT.KhaiThac.PropertiesSetting;

namespace IoT.KhaiThac
{
    public class KhaiThacApplicationAutoMapperProfile : Profile
    {
        public KhaiThacApplicationAutoMapperProfile()
        {
            CreateMap<Directory, DirectoryDto>();
            CreateMap<IoT.KhaiThac.Layer.PropertiesSetting, PropertiesSettingDto>();
            CreateMap<IoT.KhaiThac.ManageActivity.DirectoryActivity, DirectoryActivityDto>();
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}