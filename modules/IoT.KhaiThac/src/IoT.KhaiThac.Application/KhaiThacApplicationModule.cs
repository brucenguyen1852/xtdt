﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace IoT.KhaiThac
{
    [DependsOn(
        typeof(KhaiThacDomainModule),
        typeof(KhaiThacApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class KhaiThacApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAutoMapperObjectMapper<KhaiThacApplicationModule>();
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<KhaiThacApplicationModule>(validate: true);
            });
        }
    }
}
