﻿using IoT.Common;
using IoT.KhaiThac.PropertiesDirectory;
using IoT.KhaiThac.PropertiesSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.KhaiThac.Layer
{
    public class KhaiThacDirectoryService : CrudAppService<
           Directory, //The Book entity
           DirectoryDto, //Used to show books
           Guid, //Primary key of the book entity
           PagedAndSortedResultRequestDto, //Used for paging/sorting
           CreateDirectoryDto>, //Used to create/update a book
       IKhaiThacDirectoryService //implement the IBookAppService
    {
        private readonly IKhaiThacPropertiesDirectoryAppService _propertiesDirectoryAppService;
        private readonly IKhaiThacPropertiesSettingService _propertiesSettingService;
        public KhaiThacDirectoryService(IRepository<Directory, Guid> repository,
                                        IKhaiThacPropertiesDirectoryAppService propertiesDirectoryAppService,
                                        IKhaiThacPropertiesSettingService propertiesSettingService) : base(repository)
        {
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
            _propertiesSettingService = propertiesSettingService;
        }

        public async Task<List<DirectoryProperties>> GetListDirectoryPropertiesHasSetting()
        {
                List<DirectoryProperties> result = new List<DirectoryProperties>();
                var directoryfolder = Repository.Where(x => x.Type == "folder").ToList();
                result = directoryfolder.Clone<List<DirectoryProperties>>();
                var directory = Repository.Where(x => x.Type == "file").ToList();
                var listpropertiesSetting = await _propertiesSettingService.GetAllSettingAsync();
                var re = directory.Where(x => listpropertiesSetting.All(y => y.IdDirectory != x.Id.ToString("D"))).ToList();
                var re1 = re.Clone<List<DirectoryProperties>>();
                result.AddRange(re1);
                re = directory.Where(x => listpropertiesSetting.Any(y => y.IdDirectory == x.Id.ToString("D"))).ToList();
                foreach (var item in re)
                {
                    DirectoryProperties directoryProperties = item.Clone<DirectoryProperties>();
                    //if (item.Type == "file")
                    //{
                    //directoryProperties.Type = "file";
                    //directoryProperties.IdDirectory = item.Id.ToString("D");
                    //var obj = await _propertiesDirectoryAppService.GetAsyncById(item.Id.ToString("D"));
                    //if (obj != null)
                    //{
                    var image = listpropertiesSetting.Find(x => x.IdDirectory == item.Id.ToString("D")); //await _propertiesSettingService.FindPropertyByIdDirection(item.Id.ToString("D"));
                    if (image != null)
                    {
                        directoryProperties.Image2D = image.Image2D;
                        directoryProperties.Tags = image.Tags;
                    }
                    //directoryProperties.CodeTypeDirectory = obj.CodeTypeDirectory;
                    result.Add(directoryProperties);
                    //}
                    //}
                    //else
                    //{
                    //    directoryProperties.Type = "folder";
                    //    result.Add(directoryProperties);
                    //}
                }
                return result.OrderByDescending(x => x.Type).ToList();
        }
    }
}
