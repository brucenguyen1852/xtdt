﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.KhaiThac.ManagementData
{
    public class KhaiThacDetailObjectService :
         CrudAppService<
             DetailObject, //The DetailObject entity
             DetailObjectDto, //Used to show DetailObject
             Guid, //Primary key of the DetailObject entity
             PagedAndSortedResultRequestDto, //Used for paging/sorting
             CreateUpdateDetailObjectDto>, //Used to create/update a DetailObject
         IKhaiThacDetailObjectService //implement the MainObjeIMainObjectServicect
    {
        public KhaiThacDetailObjectService(IRepository<DetailObject, Guid> repository)
           : base(repository)
        {

        }

        public List<DetailObjectDto> GetPropertiesByArrayMainObject(List<string> array)
        {
            var lstResult = new List<DetailObjectDto>();

            if (array == null)
            {
                return lstResult;
            }

            var lstMainObject = new List<DetailObject>();
            foreach (var item in array)
            {
                var mainObjectDetail = this.Repository.FirstOrDefault(x => x.IdMainObject == item);
                if (mainObjectDetail != null)
                {
                    lstMainObject.Add(mainObjectDetail);
                }
            }

            //var lstMainObject = this.Repository.Where(x => array.Contains(x.IdMainObject)).ToList();

            lstResult = lstMainObject.Clone<List<DetailObjectDto>>();

            return lstResult;
        }

        public List<DetailObjectDto> SearchPropertiesObjectByArrayDirectoryId(FormSearchPropeties dto)
        {
            var lstResult = new List<DetailObjectDto>();

            if (dto.ArrayDirectoryId == null)
            {
                return lstResult;
            }
            var blstMainObject = this.Repository.Where(x => dto.ArrayDirectoryId.Contains(x.IdDirectory));

            var lstMainObject = this.Repository.Where(x => dto.ArrayDirectoryId.Contains(x.IdDirectory) && x.ListProperties.Any(y => y.TypeSystem == 2 && y.DefalutValue.ToLower().Contains(dto.Search.ToLower()))).ToList();
            //var arrayTemp = new List<DetailObject>();

            //foreach (var item in lstMainObject)
            //{
            //    var lstProperties = item.ListProperties.FindAll(x => x.TypeSystem == 2 && x.DefalutValue.ToLower().Contains(dto.Search.ToLower()));
            //    if (lstProperties != null && lstProperties.Count() > 0)
            //    {
            //        arrayTemp.Add(item);
            //    }
            //}

            lstResult = lstMainObject.Clone<List<DetailObjectDto>>();

            return lstResult;
        }
    }
}
