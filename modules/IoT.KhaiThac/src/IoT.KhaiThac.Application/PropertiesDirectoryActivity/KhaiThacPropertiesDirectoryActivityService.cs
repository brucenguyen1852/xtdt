﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.KhaiThac.PropertiesDirectoryActivity
{
    public class KhaiThacPropertiesDirectoryActivityService : CrudAppService<
            IoT.KhaiThac.ManageActivity.PropertiesDirectoryActivity, //The Book entity
            PropertiesDirectoryActivityDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePropertiesDirectoryActivityDto>, //Used to create/update a book
        IKhaiThacPropertiesDirectoryActivityService //implement the IBookAppService
    {
        public KhaiThacPropertiesDirectoryActivityService(IRepository<IoT.KhaiThac.ManageActivity.PropertiesDirectoryActivity, Guid> repository) : base(repository)
        {
        }

        public async Task<PropertiesDirectoryActivityDto> GetDirectoryActivityInfo(string id)
        {

                //var guidId = Guid.Parse(id);
                var data = await Repository.GetAsync(x => x.IdDirectoryActivity == id);

                if (data != null)
                {
                    var result = data.Clone<PropertiesDirectoryActivityDto>();

                    return result;
                }

                return null;
        }
    }
}
