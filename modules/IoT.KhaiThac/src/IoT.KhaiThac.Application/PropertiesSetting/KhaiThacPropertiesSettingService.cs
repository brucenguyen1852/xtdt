﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.KhaiThac.PropertiesSetting
{
    public class KhaiThacPropertiesSettingService :
        CrudAppService<
            IoT.KhaiThac.Layer.PropertiesSetting, //The PropertiesDirectory entity
            PropertiesSettingDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the PropertiesDirectory entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePropertiesSettingDto>, //Used to create/update a PropertiesDirectory
        IKhaiThacPropertiesSettingService //implement the IPropertiesDirectoryAppService
    {
        public KhaiThacPropertiesSettingService(IRepository<IoT.KhaiThac.Layer.PropertiesSetting, Guid> repository)
            : base(repository)
        {

        }

        public async Task<PropertiesSettingDto> FindPropertyByIdDirection(string id)
        {
            var findObj = await Repository.FirstOrDefaultAsync(x => x.IdDirectory == id);
            var json2 = JsonConvert.SerializeObject(findObj);
            var result = JsonConvert.DeserializeObject<PropertiesSettingDto>(json2);
            return result;
        }

        public async Task<List<PropertiesSettingDto>> GetAllSettingAsync()
        {
            try
            {
                var list = await Repository.GetListAsync();
                var json2 = JsonConvert.SerializeObject(list);
                var result = JsonConvert.DeserializeObject<List<PropertiesSettingDto>>(json2);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
