﻿using Volo.Abp.Modularity;
using Volo.Abp.Localization;
using IoT.KhaiThac.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Validation;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace IoT.KhaiThac
{
    [DependsOn(
        typeof(AbpValidationModule)
    )]
    public class KhaiThacDomainSharedModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<KhaiThacDomainSharedModule>();
            });

            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Add<KhaiThacResource>("en")
                    .AddBaseTypes(typeof(AbpValidationResource))
                    .AddVirtualJson("/Localization/KhaiThac");
            });

            Configure<AbpExceptionLocalizationOptions>(options =>
            {
                options.MapCodeNamespace("KhaiThac", typeof(KhaiThacResource));
            });
        }
    }
}
