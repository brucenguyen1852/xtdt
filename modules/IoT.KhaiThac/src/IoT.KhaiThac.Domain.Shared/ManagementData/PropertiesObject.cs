﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.KhaiThac.ManagementData
{
    public class PropertiesObject
    {
        public string NameProperties { get; set; } // tên thuộc tính
        public string CodeProperties { get; set; } // mã thuộc tính
        public string TypeProperties { get; set; } // loại thuộc tính
        public bool IsShow { get; set; } // hiển thị
        public bool IsView { get; set; } // chỉ xem
        public string DefalutValue { get; set; } //Giá trị
        public int OrderProperties { get; set; } // thứ tự thuộc tính
        public int TypeSystem { get; set; } // Thuộc tính hệ thống hay thường
    }
}
