﻿namespace IoT.KhaiThac
{
    public static class KhaiThacDbProperties
    {
        public static string DbTablePrefix { get; set; } = "KhaiThac";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "KhaiThac";
    }
}
