﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace IoT.KhaiThac
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(KhaiThacDomainSharedModule)
    )]
    public class KhaiThacDomainModule : AbpModule
    {

    }
}
