﻿using Volo.Abp.Settings;

namespace IoT.KhaiThac.Settings
{
    public class KhaiThacSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from KhaiThacSettings class.
             */
        }
    }
}