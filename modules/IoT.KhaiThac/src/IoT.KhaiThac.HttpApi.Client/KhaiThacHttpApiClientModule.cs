﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace IoT.KhaiThac
{
    [DependsOn(
        typeof(KhaiThacApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class KhaiThacHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "KhaiThac";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(KhaiThacApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
