﻿using IoT.Common;
using IoT.KhaiThac.Layer;
using IoT.KhaiThac.PropertiesDirectory;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.KhaiThac.Directory
{
    [RemoteService]
    [Route("/api/khaithac/khaiThacDirectory")]
    public class DirectoryController : KhaiThacController
    {
        private readonly IKhaiThacDirectoryService _directoryService;
        public DirectoryController(IKhaiThacDirectoryService directoryService)
        {
            _directoryService = directoryService;
        }

        [HttpGet("get-list-layer-has-setting")]
        public async Task<ActionResult> GetListLayerHasSetting()
        {
            try
            {
                var list = await _directoryService.GetListDirectoryPropertiesHasSetting();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, list));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }
    }
}
