﻿using IoT.KhaiThac.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace IoT.KhaiThac
{
    public abstract class KhaiThacController : AbpController
    {
        protected KhaiThacController()
        {
            LocalizationResource = typeof(KhaiThacResource);
        }
    }
}
