﻿using Localization.Resources.AbpUi;
using IoT.KhaiThac.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace IoT.KhaiThac
{
    [DependsOn(
        typeof(KhaiThacApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class KhaiThacHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(KhaiThacHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<KhaiThacResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
