﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Volo.Abp;

namespace IoT.KhaiThac.ManagementData
{
    [RemoteService]
    [Route("/api/khaithac/khaiThacDetailObject")]
    public class DetailObjectController : KhaiThacController
    {
        private IKhaiThacDetailObjectService _khaiThacDetailObjectService;
        public DetailObjectController(IKhaiThacDetailObjectService khaiThacDetailObjectService)
        {
            _khaiThacDetailObjectService = khaiThacDetailObjectService;
        }

        [HttpPost("get-properties-by-array-main-object")]
        public IActionResult GetPropertiesByArrayMainObjectId(GetPropertiesMainObjectModel dto)
        {
            try
            {
                var result = _khaiThacDetailObjectService.GetPropertiesByArrayMainObject(dto.ArrayId);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        [HttpPost("search-properties-by-array-directory")]
        public IActionResult SearchPropertiesObject(FormSearchPropeties dto)
        {
            try
            {
                var result = _khaiThacDetailObjectService.SearchPropertiesObjectByArrayDirectoryId(dto);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }

    public class GetPropertiesMainObjectModel
    {
        public List<string> ArrayId { get; set; }
    }
}
