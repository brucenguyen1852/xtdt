﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.KhaiThac.Map4d
{
    [RemoteService]
    [Route("/api/khaithac/map4d")]
    public class Map4DController : KhaiThacController
    {
        private IConfiguration configuration;
        public Map4DController(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        [HttpGet("get-object-in-geometry")]
        public async Task<ActionResult> GetListObjectInGeometry()
        {
            try
            {
                var key = configuration.GetSection("Map4d:Key").Value;
                var geometry = configuration.GetSection("Map4d:Geometry").Value;
                var url = "https://api-private.map4d.vn/app/object/object-in-geometry?Key=" + key;
                HttpClient client = new HttpClient();
                StringContent httpContent = new StringContent(geometry, System.Text.Encoding.UTF8, "application/json");
                var response = await client.PostAsync(url, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                    //return Ok(true);
                }
                //return Ok(JsonConvert.SerializeObject(new { code = "error", message = "Lỗi hệ thống", result = "null" }));
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Lỗi hệ thống", "null"));
            }
            catch (Exception ex)
            {
                //return Ok(JsonConvert.SerializeObject(new { code = "error", message = ex.Message, result = "null" }));
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
