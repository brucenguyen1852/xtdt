﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.KhaiThac.PropertiesDirectoryActivity
{
    [RemoteService]
    [Route("/api/khaithac/khaiThacPropertiesDirectoryActivity")]
    public class PropertiesDirectoryActivityController : KhaiThacController
    {
        private IKhaiThacPropertiesDirectoryActivityService _khaiThacPropertiesDirectoryActivityService;
        public PropertiesDirectoryActivityController(IKhaiThacPropertiesDirectoryActivityService khaiThacPropertiesDirectoryActivityService)
        {
            _khaiThacPropertiesDirectoryActivityService = khaiThacPropertiesDirectoryActivityService;
        }



        [HttpGet("get-properties-directory-activity")]
        public async Task<ActionResult> GetPropertiesDirectoryActivity(string id)
        {
            try
            {
                var obj = await _khaiThacPropertiesDirectoryActivityService.GetDirectoryActivityInfo(id);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
