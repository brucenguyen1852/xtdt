﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.KhaiThac.MongoDB
{
    [ConnectionStringName(KhaiThacDbProperties.ConnectionStringName)]
    public interface IKhaiThacMongoDbContext : IAbpMongoDbContext
    {
        /* Define mongo collections here. Example:
         * IMongoCollection<Question> Questions { get; }
         */
    }
}
