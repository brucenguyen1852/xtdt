﻿using IoT.KhaiThac.Layer;
using IoT.KhaiThac.ManageActivity;
using IoT.KhaiThac.ManagementData;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.KhaiThac.MongoDB
{
    [ConnectionStringName(KhaiThacDbProperties.ConnectionStringName)]
    public class KhaiThacMongoDbContext : AbpMongoDbContext, IKhaiThacMongoDbContext
    {
        /* Add mongo collections here. Example:
         * public IMongoCollection<Question> Questions => Collection<Question>();
         */
        public IMongoCollection<Directory> Directorys => Collection<Directory>();
        public IMongoCollection<PropertiesDirectory> PropertiesDirectorys => Collection<PropertiesDirectory>();
        public IMongoCollection<PropertiesSetting> PropertiesSettings => Collection<PropertiesSetting>();
        public IMongoCollection<DirectoryActivity> DirectoryActivities => Collection<DirectoryActivity>();
        public IMongoCollection<PropertiesDirectoryActivity> PropertiesDirectoryActivities => Collection<PropertiesDirectoryActivity>();
        public IMongoCollection<DetailObject> DetailObjects => Collection<DetailObject>();
        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigureKhaiThac();
        }
    }
}