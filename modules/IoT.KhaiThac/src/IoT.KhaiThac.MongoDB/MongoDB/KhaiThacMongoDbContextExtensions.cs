﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace IoT.KhaiThac.MongoDB
{
    public static class KhaiThacMongoDbContextExtensions
    {
        public static void ConfigureKhaiThac(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new KhaiThacMongoModelBuilderConfigurationOptions(
                KhaiThacDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}