﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace IoT.KhaiThac.MongoDB
{
    public class KhaiThacMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public KhaiThacMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}