﻿using IoT.HTKT.Permissions;
using IoT.KhaiThac.Localization;
using IoT.KhaiThac.Permissions;
using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace IoT.KhaiThac.Web.Menus
{
    public class KhaiThacMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            //Add main menu items.
            //context.Menu.AddItem(new ApplicationMenuItem(KhaiThacMenus.Prefix, displayName: "KhaiThac", "~/KhaiThac", icon: "fa fa-globe"));
            var l = context.GetLocalizer<KhaiThacResource>();
            if (await context.IsGrantedAsync(HTKTPermissions.Exploit.Default))
            {
                context.Menu.AddItem(new ApplicationMenuItem(KhaiThacMenus.Prefix, l["Menu:KhaiThac"], "~/KhaiThac", icon: "fas fa-truck-loading", order: 2));
            }

            //return Task.CompletedTask;
        }
    }
}