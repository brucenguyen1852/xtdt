﻿var d2r = Math.PI / 180;
var r2d = 180 / Math.PI;
var convertxyz = {
    lon2tile: function (lon, zoom) {
        return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
    },
    lat2tile: function (lat, zoom) {
        return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));
    },
    tile2lon: function (x, z) {
        return x / Math.pow(2, z) * 360 - 180;
    },
    tile2lat: function (y, z) {
        var n = Math.PI - 2 * Math.PI * y / Math.pow(2, z);
        return r2d * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n)));
    },
    tile2boundingBox: function (x, y, zoom) {
        var bb = {};
        bb.north = convertxyz.tile2lat(y, zoom);
        bb.south = convertxyz.tile2lat(y + 1, zoom);
        bb.west = convertxyz.tile2lon(x, zoom);
        bb.east = convertxyz.tile2lon(x + 1, zoom);
        return bb.west + "," + bb.south + "," + bb.east + "," + bb.north;
    },
    tile2boundingBoxNSEW: function (x, y, zoom) {
        var bb = {};
        bb.north = convertxyz.tile2lat(y, zoom);
        bb.south = convertxyz.tile2lat(y + 1, zoom);
        bb.west = convertxyz.tile2lon(x, zoom);
        bb.east = convertxyz.tile2lon(x + 1, zoom);
        return bb.south + "," + bb.west + "," + bb.north + "," + bb.east;
    },
    tile2boundingBoxNSWE: function (x, y, zoom) {
        var bb = {};
        bb.north = convertxyz.tile2lat(y, zoom);
        bb.south = convertxyz.tile2lat(y + 1, zoom);
        bb.west = convertxyz.tile2lon(x, zoom);
        bb.east = convertxyz.tile2lon(x + 1, zoom);
        return bb;
    },
    ConvertWGS84ToVN2000New: function (x, y, zoom) {
        var bbox = convertxyz.tile2boundingBoxNSWE(x, y, zoom);
        var bboxVN20001 = ConvertLocationMap.WGS84toVN2000(bbox.south, bbox.west, 107.75, 3);
        var bboxVN20002 = ConvertLocationMap.WGS84toVN2000(bbox.north, bbox.east, 107.75, 3);
        var bbox = null;
        if (typeof bboxVN20001 !== "undefined" && typeof bboxVN20002 !== "undefined") {
            bbox = bboxVN20001[1] + "," + bboxVN20001[0] + "," + bboxVN20002[1] + "," + bboxVN20002[0];
        }
        return bbox;
    },
    ConvertWGS84ToVN2000: function (x, y, zoom) {
        var bboxSWNE = convertxyz.tile2boundingBoxNSEW(x, y, zoom);
        var bboxVN2000 = convertxyz.GetAPI(bboxSWNE);
        var split = bboxVN2000.split(',');
        if (typeof split !== "undefined" && split.length >= 4) {
            return split[2] + "," + split[3] + "," + split[0] + "," + split[1];//WSEN
        }
        return null;
    },
    GetAPI: function (bbox) {
        var dataRP = null;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: "https://api-private.map4d.vn/app/geojson/viewbox-wgs84-to-vn2000",
            data: {
                viewbox: bbox,
                provinceName: "Quảng Nam",
                Key: "d9f5568c4c512c562cf0cf9f8ff487ec"
            },
            success: function (data) {
                if (data.code === "ok") {
                    dataRP = data.result;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
        return dataRP;
    },
    getTileNumber: function (lat, lon, zoom) {
        var xtile = (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));;
        var ytile = (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));
        var xy = { x: xtile, y: ytile };
        return xy;
    },
    getLonLat: function (xtile, ytile, zoom) {
        var lon_deg = this.tile2lon(xtile, zoom);
        var lat_deg = this.tile2lat(ytile, zoom);
        var latlng = { lng: lon_deg, lat: lat_deg };
        return latlng;
    },
    LonLatToBbox: function (lat, lon, zoom) {
        var width = 100;
        var height = 100;
        var tile_size = 256;

        var xy = this.getTileNumber(lat, lon, zoom);
        var xtile_s = (xy.x * tile_size - width / 2) / tile_size;
        var ytile_s = (xy.y * tile_size - height / 2) / tile_size;
        var xtile_e = (xy.x * tile_size + width / 2) / tile_size;
        var ytile_e = (xy.y * tile_size + height / 2) / tile_size;

        var lnglat1 = this.getLonLat(xtile_s, ytile_s, zoom);
        var lnglat2 = this.getLonLat(xtile_e, ytile_e, zoom);

        var bbox = lnglat1.lng + "," + lnglat1.lat + "," + lnglat2.lng + "," + lnglat2.lat;
        return bbox;
    },
    CheckInBboxMaptile: function (bbox, x, y, zoom) {
        if (bbox.length > 0) {
            if (bbox[0].lat >= -90 && bbox[0].lat <= 90) {
                //WGS84
                var MaxY = this.lat2tile(bbox[0].lat, zoom);
                var MinX = this.lon2tile(bbox[0].lng, zoom);
                var MinY = this.lat2tile(bbox[1].lat, zoom);
                var MaxX = this.lon2tile(bbox[1].lng, zoom);
                if ((MaxY >= y && MinY <= y && MaxX >= x && MinX <= x)) return true;
            } else {
                //VN2000
                return true;
            }
        } else
            return true;
        return false;
    },
};
var ConvertLocationMap = {
    goc2ddmmss: function (goc) {
        var num = Math.trunc(goc);
        var num2 = goc - num;
        var num3 = num2 * 60.0;
        var num4 = Math.trunc(num3);
        var num5 = (num3 - num4) * 60.0;
        return (((num * 10000) + (num4 * 100)) + num5);
    },
    BLH2XYZ: function (B, L, H) {
        B = ConvertLocationMap.Degree_2_Rad(B);
        L = ConvertLocationMap.Degree_2_Rad(L);
        var num = 6378137.0;
        var num2 = 0.00669437999013;
        var num3 = num / Math.sqrt(1.0 - ((num2 * Math.sin(B)) * Math.sin(B)));
        var num4 = ((num3 + H) * Math.cos(B)) * Math.cos(L);
        var num5 = ((num3 + H) * Math.cos(B)) * Math.sin(L);
        var num6 = ((num3 * (1.0 - num2)) + H) * Math.sin(B);
        var array = [num4, num5, num6];
        return array;
    },
    XYZ2BLH: function (X, Y, Z) {
        var num = 6378137.0;
        var num4 = 6356752.3142;
        var num2 = 0.00669437999013;
        var num3 = 0.00673949674228;
        var num7 = Math.atan(Y / X);
        if (num7 < 0.0) {
            num7 += Math.PI;
        }
        var num12 = Math.sqrt((X * X) + (Y * Y));
        var a = Math.atan((Z * num) / (num12 * num4));
        var num10 = Z + ((num3 * num4) * Math.pow(Math.sin(a), 3.0));
        var num11 = num12 - ((num2 * num) * Math.pow(Math.cos(a), 3.0));
        var num6 = Math.atan(num10 / num11);
        var num5 = num / Math.sqrt(1.0 - ((num2 * Math.sin(num6)) * Math.sin(num6)));
        var num8 = (num12 - (num5 * Math.cos(num6))) / Math.cos(num6);
        var array = [num6, num7, num8];
        return array;
    },
    Degree_2_Rad: function (B) {
        var num2 = Math.trunc(B / 10000.0);
        var num3 = B - (num2 * 10000);
        var num4 = Math.trunc(num3 / 100.0);
        var num5 = num3 - (num4 * 100);
        var num6 = num2;
        var num7 = num4;
        var num = (num6 + (num7 / 60.0)) + (num5 / 3600.0);
        return ((num * Math.PI) / 180.0);
    },
    BL2XY: function (lat, lng, KTtruc, zone) {
        KTtruc = ConvertLocationMap.Degree_2_Rad(KTtruc);
        var num = 1.0;
        if (zone == 3.0) {
            num = 0.9999;
        }
        else {
            num = 0.9996;
        }
        //lat *= num;
        //lng *= num;
        var a = 6378137.0;
        var e = 0.00669437999013;
        var d = 0.00673949674228;
        var m0 = a * (1.0 - e);
        var m2 = ((3.0 * e) * m0) / 2.0;
        var m4 = ((5.0 * e) * m2) / 4.0;
        var m6 = ((7.0 * e) * m4) / 6.0;
        var m8 = ((9.0 * e) * m6) / 8.0;
        var a0 = m0 + (m2 / 2) + ((3 * m4) / 8) + ((5 * m6) / 16) + ((35 * m8) / 128);
        var a2 = (m2 / 2) + (m4 / 2) + ((15 * m6) / 32) + ((7 * m8) / 16);
        var a4 = (m4 / 8) + ((3 * m6) / 16) + ((7 * m8) / 32);
        var a6 = (m6 / 32) + (m8 / 16);
        var a8 = m8 / 128;
        var L = lng * (Math.PI / 180); //Degree_2_Rad(lng);
        var l = L - KTtruc; // l=L-L0
        l *= num;
        var B = lat * (Math.PI / 180); //Degree_2_Rad(lat);
        var sinB = Math.sin(B);
        var CosB = Math.cos(B);
        var TanB = Math.tan(B);
        var N = a / Math.sqrt(1.0 - ((e * sinB) * sinB));
        var nu = Math.sqrt(d) * CosB;
        var A2 = (N * sinB * CosB) / 2.0;
        var a11 = N * sinB;
        var A4 = (N * sinB * Math.pow(CosB, 3) * (5 - (Math.pow(TanB, 2) + (9 * Math.pow(nu, 2)) + (9 * Math.pow(nu, 4))))) / 24.0;
        var A6 = (N * sinB * Math.pow(CosB, 5) * (61 - (58 * Math.pow(TanB, 2)) + Math.pow(TanB, 4) + (270 * Math.pow(nu, 2)) - (330 * Math.pow(nu, 2) * Math.pow(TanB, 2)))) / 720.0;
        var A8 = (N * sinB * Math.pow(CosB, 7) * (1385 - (311 * Math.pow(TanB, 2)) + (543 * Math.pow(TanB, 4)) - Math.pow(TanB, 6))) / 40320.0;
        var B1 = N * CosB;
        var B3 = (N * Math.pow(CosB, 3) * (1 - Math.pow(TanB, 2) + Math.pow(nu, 2))) / 6.0;
        var B5 = (N * Math.pow(CosB, 5) * (5 - (18 * Math.pow(TanB, 2)) + Math.pow(TanB, 4) + (14 * Math.pow(nu, 2)) - (58 * Math.pow(nu, 2)) * Math.pow(TanB, 2))) / 120.0;
        var B7 = (N * Math.pow(CosB, 7) * (61 - (479 * Math.pow(TanB, 2)) + (179 * Math.pow(TanB, 4)) - Math.pow(TanB, 6))) / 5040.0;
        var X0 = (a0 * B) - ((a2 / 2) * (Math.sin(2.0 * B))) + ((a4 / 4) * (Math.sin(4.0 * B))) - ((a6 / 6) * Math.sin(6.0 * B)) + ((a8 / 8) * Math.sin(8.0 * B));

        var x = X0 + (A2 * Math.pow(l, 2)) + (A4 * Math.pow(l, 4)) + (A6 * Math.pow(l, 6)) + (A8 * Math.pow(l, 8));
        var y = B1 * l + (B3 * Math.pow(l, 3)) + (B5 * Math.pow(l, 5)) + (B7 * Math.pow(l, 7));
        x = x * num;
        var array = [x, y];
        return array;
    },
    XY2BL: function (X, Y, KTtruc, zone) {
        KTtruc = ConvertLocationMap.Degree_2_Rad(KTtruc);
        var num = 1.0;
        if (zone == 3.0) {
            num = 0.9999;
        }
        else {
            num = 0.9996;
        }
        X /= num;
        Y = (Y - 500000.0) / num;
        var num2 = 6378137.0;//a
        var num8 = 0.00669437999013;//e^2
        var d = 0.00673949674228;
        var num3 = num2 * (1.0 - num8); //a(1-e^2) //m0
        var num4 = ((3.0 * num8) * num3) / 2.0;//m2
        var num5 = ((5.0 * num8) * num4) / 4.0;//m4
        var num6 = ((7.0 * num8) * num5) / 6.0;//m6
        var num7 = ((9.0 * num8) * num6) / 8.0;//m8
        var num10 = (((num3 + (num4 / 2.0)) + ((3.0 * num5) / 8.0)) + ((5.0 * num6) / 16.0)) + ((35.0 * num7) / 128.0);//a0
        var num11 = (((num4 / 2.0) + (num5 / 2.0)) + ((15.0 * num6) / 32.0)) + ((7.0 * num7) / 16.0);//a2
        var num12 = ((num5 / 8.0) + ((3.0 * num6) / 16.0)) + ((7.0 * num7) / 32.0);//a4
        var num13 = (num6 / 32.0) + (num7 / 16.0);//a6
        var num14 = num7 / 128.0;//a8
        var num15 = X / num10;//t
        var num16 = num15 + ((((((num11 * Math.sin(2.0 * num15)) / 2.0) - ((num12 * Math.sin(4.0 * num15)) / 4.0)) + ((num13 * Math.sin(6.0 * num15)) / 6.0)) - ((num14 * Math.sin(8.0 * num15)) / 8.0)) / num10);//t1
        var num17 = num15 + ((((((num11 * Math.sin(2.0 * num16)) / 2.0) - ((num12 * Math.sin(4.0 * num16)) / 4.0)) + ((num13 * Math.sin(6.0 * num16)) / 6.0)) - ((num14 * Math.sin(8.0 * num16)) / 8.0)) / num10);//t2
        while (Math.abs(num17 - num16) > Math.pow(10.0, -14.0)) {
            num16 = num17;
            num17 = num15 + ((((((num11 * Math.sin(2.0 * num16)) / 2.0) - ((num12 * Math.sin(4.0 * num16)) / 4.0)) + ((num13 * Math.sin(6.0 * num16)) / 6.0)) - ((num14 * Math.sin(8.0 * num16)) / 8.0)) / num10);
        }
        var a = num17;//tn == B0
        var num28 = Math.sin(a);
        var num20 = num2 / Math.sqrt(1.0 - (num8 * (num28 * num28))); //num2 / Math.Sqrt(1.0 - ((num8 * num28) * num28));
        var num38 = num20 * num20;
        var num39 = num38 * num38;
        var num40 = num39 * num38;
        var num19 = Math.sqrt(d) * Math.cos(a);//e' cos(B)//N
        var num22 = num19 * num19;
        var num23 = num22 * num22;
        var x = Math.tan(a);
        var num24 = x * x;
        var num25 = Math.pow(x, 4.0);
        var num26 = Math.pow(x, 6.0);
        var num27 = Math.cos(a);
        Math.pow(num27, 3.0);
        Math.pow(num27, 5.0);
        Math.pow(num27, 7.0);
        var num37 = 1.0 + num22;//Vx
        var num29 = (-num37 * x) / (2.0 * num38);
        var num30 = (-num29 * ((((5.0 + (3.0 * num24)) + num22) - ((9.0 * num22) * num24)) - (4.0 * num23))) / (12.0 * num38);
        var num31 = (num29 * (((((61.0 + (90.0 * num24)) + (45.0 * num25)) + (46.0 * num22)) - ((252.0 * num22) * num24)) - ((90.0 * num22) * num25))) / (360.0 * num39);
        var num32 = (-num29 * ((1385.0 + ((3633.0 * num24) * a)) + ((4095.0 * num26) * a))) / (20160.0 * num40);

        var num33 = 1.0 / (num20 * num27);
        var num34 = (-num33 * ((1.0 + (2.0 * num24)) + num22)) / (6.0 * num38);
        var num35 = (-num33 * (((5.0 + (28.0 * num24)) + (6.0 * num22)) + ((8.0 * num22) * num24))) / (120.0 * num39);
        var num36 = (-num33 * (((61.0 + (662.0 * num24)) + (1320.0 * num25)) + (720.0 * num26))) / (5040.0 * num40);
        var num41 = (((a + (num29 * Math.pow(Y, 2.0))) + (num30 * Math.pow(Y, 4.0))) + (num31 * Math.pow(Y, 6.0))) + (num32 * Math.pow(Y, 8.0));
        var num43 = (((num33 * Y) + (num34 * Math.pow(Y, 3.0))) + (num35 * Math.pow(Y, 5.0))) + (num36 * Math.pow(Y, 7.0));
        var num42 = KTtruc + num43;
        var array = [num41, num42];
        return array;
    },
    //------------------------
    WGS84toVN2000: function (lat, lng, ktTruc, zone) {
        var ktt = ktTruc;
        var pdo = Math.trunc(ktt);
        var pphut = Math.trunc((ktt - pdo) * 60);
        var kTtruc = this.XLL0_2(pdo.toString(), pphut.toString());
        //lat lng to lat lng rad B'L'
        var B84 = (lat * Math.PI) / 180;
        var L84 = (lng * Math.PI) / 180;
        //BLH=>XYZ
        var numArraygoc84 = [0, 0];
        numArraygoc84[0] = this.goc2ddmmss((B84 * 180.0) / Math.PI);
        numArraygoc84[1] = this.goc2ddmmss((L84 * 180.0) / Math.PI);
        var numArrayXYZ84 = this.BLH2XYZ(numArraygoc84[0], numArraygoc84[1], 0.0);
        //XYZ84=>XYZ2000
        var numArray20000 = this.WGS84_2_VN2000_second(numArrayXYZ84[0], numArrayXYZ84[1], numArrayXYZ84[2]);
        //XYZ2000=>BLH2000
        var numArrayBLH20001 = this.XYZ2BLH(numArray20000[0], numArray20000[1], numArray20000[2]);
        var B2000 = numArrayBLH20001[0] * (180.0 / Math.PI);
        var L2000 = numArrayBLH20001[1] * (180.0 / Math.PI);
        //BLH2000=>XY2000
        var numArrayXY = this.BL2XY(B2000, L2000, kTtruc, zone);
        var XYVN2000 = [0, 0];
        XYVN2000[0] = numArrayXY[0];
        XYVN2000[1] = numArrayXY[1] + 500000;
        return XYVN2000;
    },
    XLL0_2: function (pdo, pph) {
        var num = Number(pdo);
        var num2 = Number(pph);
        var num3 = (num * 10000) + (num2 * 100);
        return num3;
    },
    WGS84_2_VN2000_second: function (X1, Y1, Z1) {
        var num = 4.84813681109536E-06;
        var num2 = 191.90441429;
        var num3 = 39.30318279;
        var num4 = 111.45032835;
        var num5 = 0.00928836 * num;
        var num6 = -0.01975479 * num;
        var num7 = 0.00427372 * num;
        var num8 = 0.999999747093722;
        var num9 = num2 + (num8 * ((X1 + (num7 * Y1)) - (num6 * Z1)));
        var num10 = num3 + (num8 * (((-num7 * X1) + Y1) + (num5 * Z1)));
        var num11 = num4 + (num8 * (((num6 * X1) - (num5 * Y1)) + Z1));
        return [num9, num10, num11];
    },
    VN2000_2_WGS84_second: function (X1, Y1, Z1) {
        var num = 4.84813681109536E-06;
        var num2 = -191.90441429;
        var num3 = -39.30318279;
        var num4 = -111.45032835;
        var num5 = -0.00928836 * num;
        var num6 = 0.01975479 * num;
        var num7 = -0.00427372 * num;
        var num8 = 1.0000002529062779;
        var num9 = num2 + (num8 * ((X1 + (num7 * Y1)) - (num6 * Z1)));
        var num10 = num3 + (num8 * (((-num7 * X1) + Y1) + (num5 * Z1)));
        var num11 = num4 + (num8 * (((num6 * X1) - (num5 * Y1)) + Z1));
        return [num9, num10, num11];
    },
    VN2000toWGS84: function (x, y, ktTruc, zone) {
        var ktt = ktTruc;
        var pdo = Math.trunc(ktt);
        var pphut = Math.trunc((ktt - pdo) * 60);
        var kTtruc = this.XLL0_2(pdo.toString(), pphut.toString());
        //XY=>BL
        var numArrayBL = this.XY2BL(x, y, kTtruc, zone);
        //BLH2000=>XYZ2000
        var numArraygoc = [0, 0];
        numArraygoc[0] = this.goc2ddmmss((numArrayBL[0] * 180.0) / Math.PI);
        numArraygoc[1] = this.goc2ddmmss((numArrayBL[1] * 180.0) / Math.PI);
        var numArrayXYZ = this.BLH2XYZ(numArraygoc[0], numArraygoc[1], 0.0);
        //XYZ2000=>XYZ84
        var numArray2 = this.VN2000_2_WGS84_second(numArrayXYZ[0], numArrayXYZ[1], numArrayXYZ[2]);
        //XYZ84=>BLH84
        var numArray3 = this.XYZ2BLH(numArray2[0], numArray2[1], numArray2[2]);
        //var latk = numArray3[0] * (180.0 / Math.PI);
        numArray3[0] = this.goc2ddmmss((numArray3[0] * 180.0) / Math.PI);
        numArray3[1] = this.goc2ddmmss((numArray3[1] * 180.0) / Math.PI);
        var LatLng84 = [0, 0];
        LatLng84[0] = (this.Degree_2_Rad(numArray3[0]) * 180.0) / Math.PI;
        LatLng84[1] = (this.Degree_2_Rad(numArray3[1]) * 180.0) / Math.PI;
        return LatLng84;
    },
}
//var ConvertLocationMap = {
//    goc2ddmmss: function (goc) {
//        var num = Math.trunc(goc);
//        var num2 = goc - num;
//        var num3 = num2 * 60.0;
//        var num4 = Math.trunc(num3);
//        var num5 = (num3 - num4) * 60.0;
//        return (((num * 10000) + (num4 * 100)) + num5);
//    },
//    BLH2XYZ: function (B, L, H) {
//        B = ConvertLocationMap.Degree_2_Rad(B);
//        L = ConvertLocationMap.Degree_2_Rad(L);
//        var num = 6378137.0;
//        var num2 = 0.00669437999013;
//        var num3 = num / Math.sqrt(1.0 - ((num2 * Math.sin(B)) * Math.sin(B)));
//        var num4 = ((num3 + H) * Math.cos(B)) * Math.cos(L);
//        var num5 = ((num3 + H) * Math.cos(B)) * Math.sin(L);
//        var num6 = ((num3 * (1.0 - num2)) + H) * Math.sin(B);
//        var array = [num4, num5, num6];
//        return array;
//    },
//    XYZ2BLH: function (X, Y, Z) {
//        var num = 6378137.0;
//        var num4 = 6356752.3142;
//        var num2 = 0.00669437999013;
//        var num3 = 0.00673949674228;
//        var num7 = Math.atan(Y / X);
//        if (num7 < 0.0) {
//            num7 += Math.PI;
//        }
//        var num12 = Math.sqrt((X * X) + (Y * Y));
//        var a = Math.atan((Z * num) / (num12 * num4));
//        var num10 = Z + ((num3 * num4) * Math.pow(Math.sin(a), 3.0));
//        var num11 = num12 - ((num2 * num) * Math.pow(Math.cos(a), 3.0));
//        var num6 = Math.atan(num10 / num11);
//        var num5 = num / Math.sqrt(1.0 - ((num2 * Math.sin(num6)) * Math.sin(num6)));
//        var num8 = (num12 - (num5 * Math.cos(num6))) / Math.cos(num6);
//        var array = [num6, num7, num8];
//        return array;
//    },
//    Degree_2_Rad: function (B) {
//        var num2 = Math.trunc(B / 10000.0);
//        var num3 = B - (num2 * 10000);
//        var num4 = Math.trunc(num3 / 100.0);
//        var num5 = num3 - (num4 * 100);
//        var num6 = num2;
//        var num7 = num4;
//        var num = (num6 + (num7 / 60.0)) + (num5 / 3600.0);
//        return ((num * Math.PI) / 180.0);
//    },
//    BL2XY: function (lat, lng, KTtruc, zone) {
//        KTtruc = ConvertLocationMap.Degree_2_Rad(KTtruc);
//        var num = 1.0;
//        if (zone == 3.0) {
//            num = 0.9999;
//        }
//        else {
//            num = 0.9996;
//        }
//        //lat *= num;
//        //lng *= num;
//        var a = 6378137.0;
//        var e = 0.00669437999013;
//        var d = 0.00673949674228;
//        var m0 = a * (1.0 - e);
//        var m2 = ((3.0 * e) * m0) / 2.0;
//        var m4 = ((5.0 * e) * m2) / 4.0;
//        var m6 = ((7.0 * e) * m4) / 6.0;
//        var m8 = ((9.0 * e) * m6) / 8.0;
//        var a0 = m0 + (m2 / 2) + ((3 * m4) / 8) + ((5 * m6) / 16) + ((35 * m8) / 128);
//        var a2 = (m2 / 2) + (m4 / 2) + ((15 * m6) / 32) + ((7 * m8) / 16);
//        var a4 = (m4 / 8) + ((3 * m6) / 16) + ((7 * m8) / 32);
//        var a6 = (m6 / 32) + (m8 / 16);
//        var a8 = m8 / 128;
//        var L = lng * (Math.PI / 180); //Degree_2_Rad(lng);
//        var l = L - KTtruc; // l=L-L0
//        l *= num;
//        var B = lat * (Math.PI / 180); //Degree_2_Rad(lat);
//        var sinB = Math.sin(B);
//        var CosB = Math.cos(B);
//        var TanB = Math.tan(B);
//        var N = a / Math.sqrt(1.0 - ((e * sinB) * sinB));
//        var nu = Math.sqrt(d) * CosB;
//        var A2 = (N * sinB * CosB) / 2.0;
//        var a11 = N * sinB;
//        var A4 = (N * sinB * Math.pow(CosB, 3) * (5 - (Math.pow(TanB, 2) + (9 * Math.pow(nu, 2)) + (9 * Math.pow(nu, 4))))) / 24.0;
//        var A6 = (N * sinB * Math.pow(CosB, 5) * (61 - (58 * Math.pow(TanB, 2)) + Math.pow(TanB, 4) + (270 * Math.pow(nu, 2)) - (330 * Math.pow(nu, 2) * Math.pow(TanB, 2)))) / 720.0;
//        var A8 = (N * sinB * Math.pow(CosB, 7) * (1385 - (311 * Math.pow(TanB, 2)) + (543 * Math.pow(TanB, 4)) - Math.pow(TanB, 6))) / 40320.0;
//        var B1 = N * CosB;
//        var B3 = (N * Math.pow(CosB, 3) * (1 - Math.pow(TanB, 2) + Math.pow(nu, 2))) / 6.0;
//        var B5 = (N * Math.pow(CosB, 5) * (5 - (18 * Math.pow(TanB, 2)) + Math.pow(TanB, 4) + (14 * Math.pow(nu, 2)) - (58 * Math.pow(nu, 2)) * Math.pow(TanB, 2))) / 120.0;
//        var B7 = (N * Math.pow(CosB, 7) * (61 - (479 * Math.pow(TanB, 2)) + (179 * Math.pow(TanB, 4)) - Math.pow(TanB, 6))) / 5040.0;
//        var X0 = (a0 * B) - ((a2 / 2) * (Math.sin(2.0 * B))) + ((a4 / 4) * (Math.sin(4.0 * B))) - ((a6 / 6) * Math.sin(6.0 * B)) + ((a8 / 8) * Math.sin(8.0 * B));

//        var x = X0 + (A2 * Math.pow(l, 2)) + (A4 * Math.pow(l, 4)) + (A6 * Math.pow(l, 6)) + (A8 * Math.pow(l, 8));
//        var y = B1 * l + (B3 * Math.pow(l, 3)) + (B5 * Math.pow(l, 5)) + (B7 * Math.pow(l, 7));
//        x = x * num;
//        var array = [x, y];
//        return array;
//    },
//    //------------------------
//    WGS84toVN2000: function (lat, lng, ktTruc, zone) {
//        var ktt = ktTruc;
//        var pdo = Math.trunc(ktt);
//        var pphut = Math.trunc((ktt - pdo) * 60);
//        var kTtruc = this.XLL0_2(pdo.toString(), pphut.toString());
//        //lat lng to lat lng rad B'L'
//        var B84 = (lat * Math.PI) / 180;
//        var L84 = (lng * Math.PI) / 180;
//        //BLH=>XYZ
//        var numArraygoc84 = [0, 0];
//        numArraygoc84[0] = this.goc2ddmmss((B84 * 180.0) / Math.PI);
//        numArraygoc84[1] = this.goc2ddmmss((L84 * 180.0) / Math.PI);
//        var numArrayXYZ84 = this.BLH2XYZ(numArraygoc84[0], numArraygoc84[1], 0.0);
//        //XYZ84=>XYZ2000
//        var numArray20000 = this.WGS84_2_VN2000_second(numArrayXYZ84[0], numArrayXYZ84[1], numArrayXYZ84[2]);
//        //XYZ2000=>BLH2000
//        var numArrayBLH20001 = this.XYZ2BLH(numArray20000[0], numArray20000[1], numArray20000[2]);
//        var B2000 = numArrayBLH20001[0] * (180.0 / Math.PI);
//        var L2000 = numArrayBLH20001[1] * (180.0 / Math.PI);
//        //BLH2000=>XY2000
//        var numArrayXY = this.BL2XY(B2000, L2000, kTtruc, zone);
//        var XYVN2000 = [0, 0];
//        XYVN2000[0] = numArrayXY[0];
//        XYVN2000[1] = numArrayXY[1] + 500000;
//        return XYVN2000;
//    },
//    XLL0_2: function (pdo, pph) {
//        var num = Number(pdo);
//        var num2 = Number(pph);
//        var num3 = (num * 10000) + (num2 * 100);
//        return num3;
//    },
//    WGS84_2_VN2000_second: function (X1, Y1, Z1) {
//        var num = 4.84813681109536E-06;
//        var num2 = 191.90441429;
//        var num3 = 39.30318279;
//        var num4 = 111.45032835;
//        var num5 = 0.00928836 * num;
//        var num6 = -0.01975479 * num;
//        var num7 = 0.00427372 * num;
//        var num8 = 0.999999747093722;
//        var num9 = num2 + (num8 * ((X1 + (num7 * Y1)) - (num6 * Z1)));
//        var num10 = num3 + (num8 * (((-num7 * X1) + Y1) + (num5 * Z1)));
//        var num11 = num4 + (num8 * (((num6 * X1) - (num5 * Y1)) + Z1));
//        return [num9, num10, num11];
//    },
//}