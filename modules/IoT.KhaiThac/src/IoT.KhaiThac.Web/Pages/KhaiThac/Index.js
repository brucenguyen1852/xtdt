﻿mn_selected = "mn_exploit";
var l = abp.localization.getResource('KhaiThac');
var polylineTemp = null;
var isAction = false;
var exploit = {
    GLOBAL: {
        listObjectDirectory: [],
        lstArrayQTSC: [],
        listfeatures: [],
        propertiesSetting: {
            MinZoom: 0,
            MaxZoom: 20,
            Image2D: "",
            Stroke: "#8b5b5b",
            StrokeWidth: 2,
            StrokeOpacity: 1,
            StyleStroke: "",
            Fill: "#8b5b5b",
            FillOpacity: 0.3,
        },
        listMarker: [], // danh sách marker trên bản đồ
        listObject3D: [], // danh sách building trên bản đồ
        listgeojson: [], // danh sách những object dạng line, mutipleline, point,... trên bản đồ
        listDirectoryMainObject: [],
        listGroundOvelay: [],
        image2dDirectory: '',
        defaultConfig: {
            zoom: 0,
            lat: 0,
            lng: 0
        },
        ArrayGeoMetryExample: [],
        lstFeature: [],
        // draw on map
        TypeDraw: -1, // trạng thái vẽ
        // vẽ điểm
        PointDraw: null,
        RadiusPoint: null,
        // vẽ đoạn thẳng
        IsResetDraw: false,
        ArrayLine: {
            ListPoint: null,
            ListMeter: null,
            Polyline: null,
            Distacne: 0,
            Acreage: null
        },
        // vẽ polygon
        ArrayPolygon: {
            ListPoint: null,
            Meter: null,
            Polyline: null,
            Polygon: null,
            Acreage: 0,
        },
        ActionToolbar: {
            XemThongTinHover: false,
            MarkerShowInfo: null
        },
        isMobile: false,
        //object hover maptile
        hoverObjectMaptile: null,
        //list Object In Geometry
        listObjectInGeometry: [],

        ArrayFeaturesByDirectory: [],
        ArrayGeometryByDirectory: [],
        centerMap: {
            lat: 0,
            lng: 0
        }
    },
    CONSTS: {
        URL_AJAXDEDITDATA: '/api/HTKT/ManagementData/get-object-by-IdDirectory-KhaiThac',
        URL_AJAXGETDEFAULTDATA: '/api/HTKT/PropertiesDirectory/get-directoryById',
        URL_AJAXGETOBJECTBYGEOMETRY: '/api/khaithac/map4d/get-object-in-geometry',
        URL_AJAXGETOBJECTEXPLOIT: '/api/HTKT/ManagementData/get-object-exploit',
        URL_AJAXGETOBJECTEXPLOITMapTile: '/api/LayerToLayer/LayerGeo/getInfor',
        URL_GETTOTALOBJECTBYID: '/api/HTKT/ManagementData/get-total-count-by-IdDirectory',
        URL_GETOBJECTPAGINGBYDIRECTORY: "/api/HTKT/ManagementData/get-object-paging-by-IdDirectory",
        KTTruc: kTTruc,
        Zone: zone,
        ListSRS: sRSEPSG,
        GET_LOCATION_CENTER: '/api/htkt/location-setting/get-location-center',
    },
    SELECTORS: {
        btn_show_building_out: ".sugest-open-building",
        // draw
        //drop_action_draw: ".drop-action-draw",
        menu_hori: ".menuMap",
        content_infor_map: ".content-show-info-map",
        body_infor_map: ".body-info-map",
        input_radius: "#input_radius",
        input_vung_dem_line: "#input_vung_dem_line",
        close_geo_info: ".geo-info-close-icon",
        close_draw: ".close-draw",
        XemThongTinButton: "#xemThongTin",
        content_lst_object: "#lst-Object",
        header_list_object_span: "#lst-Object .lst-Object-property-header span",
        btn_brand_reference: ".btn-brand-reference",
    },
    init: function () {
        exploit.checkDevice();
        exploit.setEvent();
        exploit.evenMap();
        initTooltip();
        exploit.GetCurrentLocation();
    },
    setEvent: function () {
        $('.toogle-tree-bar').on('click', function () {
            if (!$('.div-content').hasClass('toogle-tree')) {
                $('.div-content').addClass('toogle-tree');

                $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <path class="a" style="fill: none;" d="M24,0H0V24H24Z" transform="translate(6)"></path>
                                <path class="b" style="fill: var(--primary)" d="M26.172,11H4v2H26.172l-5.364,5.364,1.414,1.414L30,12,22.222,4.222,20.808,5.636Z" transform="translate(-4)"></path>
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 4)"></rect>
                                <rect class="c" style="fill: #b2b2b2;" width="13" height="2" transform="translate(0 18)"></rect>
                            </svg>`);
                $(".toogle-tree-bar").attr('data-original-title', l("Main:OpenPanel"));
            }
            else {
                $('.div-content').removeClass('toogle-tree');
                $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="30" height="24" viewBox="0 0 30 24" style="cursor: pointer;">
                                <defs></defs>
                                <g transform="translate(1988 32)">
                                    <g transform="translate(-1988 -32)">
                                        <path style="fill: none;" class="a" d="M0,0H24V24H0Z"></path>
                                        <path style="fill: var(--primary)" class="b" d="M7.828,11H30v2H7.828l5.364,5.364-1.414,1.414L4,12l7.778-7.778,1.414,1.414Z"></path>
                                    </g>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -28)"></rect>
                                    <rect style="fill: #b2b2b2;" class="c" width="13" height="2" transform="translate(-1971 -14)"></rect>
                                </g>
                            </svg>`);
                $(".toogle-tree-bar").attr('data-original-title', l("Main:ClosePanel"));
            }
            if (exploit.GLOBAL.isMobile && $(".lst-Objec-property1").hasClass("open")) {
                $(".lst-Objec-property1").removeClass("open");
            }
        });
        exploit.EnableGeoMetryExample();


        $(exploit.SELECTORS.menu_hori).on('click', '.draw', function () {
            exploit.CancelAction();
            isAction = true;
            var type = $(this).attr('data-type');
            $(exploit.SELECTORS.content_infor_map).removeClass('open');

            $('.draw').removeClass('isForcus');
            $(this).addClass('isForcus');

            $('.draw').prop('disabled', true);
            $(this).prop('disabled', false);

            $('.draw').addClass('disabled');
            $(this).removeClass('disabled');

            $('[data-toggle="tooltip"]').tooltip("hide");

            $(exploit.SELECTORS.content_lst_object).removeClass('open'); // đóng danh sách đối tượng
            switch (type) {
                case "-1":
                    exploit.HideOrShowBuildingMap();
                    break;
                case "0": // chức năng lấy tọa độ
                    exploit.GLOBAL.TypeDraw = 0;
                    $(this).html(`<svg id="Group_2226" data-name="Group 2226" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_2671" data-name="Path 2671" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_2672" data-name="Path 2672" d="M13,22A10,10,0,1,1,23,12,10,10,0,0,1,13,22ZM8,11.5,12,13l1.5,4L17,8Z" transform="translate(-1)" fill="var(--primary)"/>
                                    </svg>`);
                    //var html = `<label>${l("Radius")} (m)</label>
                    //            <input type="number" id="input_radius" value="100"/>
                    //            <div>${l("NotInformation")}</div>`;
                    //$(exploit.SELECTORS.body_infor_map).html(html);
                    //$(exploit.SELECTORS.content_infor_map).addClass('open');
                    break;
                case "1": // chức năng do chiều dài
                    exploit.GLOBAL.TypeDraw = 1;
                    $(this).html(`<svg id="Group_2229" data-name="Group 2229" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path id="Path_2675" data-name="Path 2675" d="M0,0H24V24H0Z" fill="none"/>
                                        <path id="Path_2676" data-name="Path 2676" d="M5,18v2H9V18ZM3,7,7,2l4,5V22H3ZM21,8H19v2h2v2H18v2h3v2H19v2h2v3a1,1,0,0,1-1,1H14a1,1,0,0,1-1-1V5a1,1,0,0,1,1-1h6a1,1,0,0,1,1,1Z" fill="var(--primary)"/>
                                    </svg>`);
                    //var html = `
                    //            <div>${l("Length")}: 0(m)</div>`;
                    //$(exploit.SELECTORS.body_infor_map).html(html);
                    //$(exploit.SELECTORS.content_infor_map).addClass('open');
                    break;
                case "2": // chức năng do diện tích
                    exploit.GLOBAL.TypeDraw = 2;
                    $(this).html(`<svg id="Group_2233" data-name="Group 2233" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_2679" data-name="Path 2679" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_2680" data-name="Path 2680" d="M2,2H7V7H2ZM2,17H7v5H2ZM17,2h5V7H17Zm0,15h5v5H17ZM8,4h8V6H8ZM4,8H6v8H4ZM18,8h2v8H18ZM8,18h8v2H8Z" fill="var(--primary)"/>
                                    </svg>`);
                    break;
                case "3": // chức năng hover xem thông tin
                    exploit.GLOBAL.ActionToolbar.XemThongTinHover = true;
                    $(this).html(`<svg id="Group_2593" data-name="Group 2593" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_6370" data-name="Path 6370" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Path_6371" data-name="Path 6371" d="M14,6h2V8h5a1,1,0,0,1,1,1v7.5L16,13l.036,8.062,2.223-2.15L20.041,22H9a1,1,0,0,1-1-1V16H6V14H8V9A1,1,0,0,1,9,8h5Zm8,11.338V21a1,1,0,0,1-.048.307l-1.96-3.394L22,17.338ZM4,14v2H2V14Zm0-4v2H2V10ZM4,6V8H2V6ZM4,2V4H2V2ZM8,2V4H6V2Zm4,0V4H10V2Zm4,0V4H14V2Z" fill="var(--primary)"/>
                                    </svg>`);
                    break;
                case "4": // mở content tìm kiếm
                    TimKiem.OpenContentTimKiem();
                    $(this).html(`<svg id="Group_2237" data-name="Group 2237" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path id="Path_2683" data-name="Path 2683" d="M0,0H24V24H0Z" fill="none"/>
                                      <path id="Subtraction_2" data-name="Subtraction 2" d="M-6885.39-4764.578h0l-2.733-2.733,1.781-1.687,2.719,2.719-1.765,1.7ZM-6895-4767a9.011,9.011,0,0,1-9-9,9.011,9.011,0,0,1,9-9,9.01,9.01,0,0,1,9,9A9.01,9.01,0,0,1-6895-4767Zm0-15a6.007,6.007,0,0,0-6,6,6.007,6.007,0,0,0,6,6,6.007,6.007,0,0,0,6-6A6.007,6.007,0,0,0-6895-4782Z" transform="translate(6906 4787)" fill="var(--primary)"/>
                                    </svg>`);
                    break;
                case "5": //mở reference
                    ReferenceContent.OpenContentReference();
                    $(this).html(`<svg id="Group_3514" data-name="Group 3514" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                          <path id="Path_6779" data-name="Path 6779" d="M0,0H24V24H0Z" fill="none"/>
                                          <path id="Path_6780" data-name="Path 6780" d="M7,7V3A1,1,0,0,1,8,2H21a1,1,0,0,1,1,1V16a1,1,0,0,1-1,1H17v3.993A1.006,1.006,0,0,1,15.993,22H3.007A1.006,1.006,0,0,1,2,20.993L2,8.007A1.006,1.006,0,0,1,3.01,7ZM9,7h6.993A1.006,1.006,0,0,1,17,8.007V15h3V4H9Z" fill="var(--primary)"/>
                                        </svg>`);
                    break;
                default:
            }
        });

        $(exploit.SELECTORS.close_geo_info).on('click', function () {
            $(exploit.SELECTORS.content_infor_map).removeClass('open');
            $('.draw').removeClass('isForcus');
            exploit.GLOBAL.TypeDraw = -1;
            exploit.RemoveDrawPoint();
            exploit.RemoveLine();
            exploit.RemovePolygon();
        });

        $(exploit.SELECTORS.close_draw).on('click', function () {
            exploit.CancelAction();
            //exploit.CloseDraw();
            //TimKiem.CloseContentTimKiem();
            //$('.toggle-detail-property2').trigger('click'); // đóng content xem thông tin 1 đối tượng
        });

        // event point
        $('#map4d').on('focusin', exploit.SELECTORS.input_radius, function () {
            $(this).data('val', $(this).val());
        });

        $('#map4d').on('change', exploit.SELECTORS.input_radius, function () {
            var prev = $(this).data('val');
            if (exploit.GLOBAL.RadiusPoint != null) {
                var radius = 100;
                if ($(this).val() != "") {
                    radius = $(this).val();
                }

                if (radius < 10) {
                    swal({
                        title: "Thông báo",
                        text: "Bán kính không được nhỏ hơn 10 !",
                        icon: "warning",
                        button: "Đóng",
                    }).then((value) => {
                        radius = parseInt(prev);
                        $(this).val(radius);
                        exploit.GLOBAL.RadiusPoint.setRadius(radius);
                    });
                }
                else {
                    exploit.GLOBAL.RadiusPoint.setRadius(radius);
                }

            }
        });

        $('#map4d').on('focusin', exploit.SELECTORS.input_vung_dem_line, function () {
            $(this).data('val', $(this).val());
        });

        // event line
        $('#map4d').on('change', exploit.SELECTORS.input_vung_dem_line, function () {
            var prev = $(this).data('val');
            var iLatLng = [];
            $.each(exploit.GLOBAL.ArrayLine.ListPoint, function (i, obj) {

                var latlng = [obj.getPosition().lng, obj.getPosition().lat];
                iLatLng.push(latlng);
            });

            // vẽ chiều rộng của đường thẳng
            if (exploit.GLOBAL.ArrayLine.Acreage != null) {
                exploit.GLOBAL.ArrayLine.Acreage.setMap(null);
            }
            var regt = 100;
            if ($(this).val() != "") {
                regt = parseInt($(this).val());
            }

            if (regt < 10) {
                swal({
                    title: "Thông báo",
                    text: "Vùng đệm không được nhỏ hơn 10 !",
                    icon: "warning",
                    button: "Đóng",
                }).then((value) => {
                    regt = parseInt(prev);
                    $(this).val(regt);

                    var polylineDistance = new map4d.Polyline({
                        path: iLatLng, visible: true, strokeColor: "#00559A", strokeWidth: regt, strokeOpacity: 0.4,
                        closed: false,
                        zIndex: 1
                    })
                    //thêm polyline vào map
                    polylineDistance.setMap(map);

                    exploit.GLOBAL.ArrayLine.Acreage = polylineDistance
                });
            }
            else {

                var polylineDistance = new map4d.Polyline({
                    path: iLatLng, visible: true, strokeColor: "#00559A", strokeWidth: regt, strokeOpacity: 0.4,
                    closed: false,
                    zIndex: 1
                })
                //thêm polyline vào map
                polylineDistance.setMap(map);

                exploit.GLOBAL.ArrayLine.Acreage = polylineDistance;
            }
        });

        //hide entry-row + abpToolbar
        //$(".entry-row").css("display", "none");
        //$("#AbpContentToolbarQTSC").hide();
        //$("#AbpContentToolbar").hide();
    },
    evenMap: function () {
        // sự kiện thay đổi map 3d, 2d
        let changeMode = map.addListener("modeChanged",
            (args) => {
                var mod = map.is3dMode();
                if (mod)// chế độ 3d
                {
                    //$('.draw').prop('disabled', true);
                    if (exploit.GLOBAL.TypeDraw != -1) {
                        $(exploit.SELECTORS.close_geo_info).trigger('click');
                    }

                    for (var i = 0; i < exploit.GLOBAL.listMarker.length; i++) // ẩn marker
                    {
                        var lstMarker = exploit.GLOBAL.listMarker[i].lstMarker;
                        for (var j = 0; j < lstMarker.length; j++) {
                            lstMarker[j].setMap(null);
                            lstMarker[j].setVisible(false)
                        }
                    }

                    for (var i = 0; i < ThongTin.GLOBAL.listHighlight.length; i++) // ẩn polyline hight line
                    {
                        ThongTin.GLOBAL.listHighlight[i].setMap(null);
                    }

                    for (var i = 0; i < exploit.GLOBAL.listObject3D.length; i++) // hiển thị building
                    {
                        if (exploit.GLOBAL.listObject3D[i].object != null && exploit.GLOBAL.listObject3D[i].object != undefined) {
                            exploit.GLOBAL.listObject3D[i].object.setVisible(true)
                        }
                    }

                    $(exploit.SELECTORS.drop_action_draw).addClass('hidden');
                    map.data.clear(); // ẩn geojson

                    // đóng hover
                    exploit.hideorShowHoverInfo(false);
                }
                else {
                    map.data.clear(); // ẩn geojson

                    $('.draw').prop('disabled', false);
                    for (var i = 0; i < exploit.GLOBAL.listMarker.length; i++) // hiển thị marker
                    {
                        var lstMarker = exploit.GLOBAL.listMarker[i].lstMarker;
                        for (var j = 0; j < lstMarker.length; j++) {
                            lstMarker[j].setMap(map);
                            lstMarker[j].setVisible(true)
                        }
                    }

                    exploit.GLOBAL.ArrayGeometryByDirectory.forEach((geojson) => // hiển thị geojson
                    {
                        //exploit.showDataGeojson2(geojson.geojson, geojson.id);
                        exploit.showDataGeojsonReset(geojson.geojson, geojson.id);
                    });

                    for (var i = 0; i < ThongTin.GLOBAL.listHighlight.length; i++) {
                        ThongTin.GLOBAL.listHighlight[i].setMap(map);
                    }

                    for (var i = 0; i < exploit.GLOBAL.listObject3D.length; i++) //ẩn building
                    {
                        if (exploit.GLOBAL.listObject3D[i].object != null && exploit.GLOBAL.listObject3D[i].object != undefined) {
                            exploit.GLOBAL.listObject3D[i].object.setVisible(false)
                        }
                    }

                    exploit.resetHighlight2DModeTo3D(); // reset highlight 2d

                    $(exploit.SELECTORS.drop_action_draw).removeClass('hidden');

                    // nếu trạng thái là hành động
                    if (isAction) {
                        // mở hover info
                        exploit.hideorShowHoverInfo(true);
                    }
                    else {
                        exploit.CloseHoverToolBar(); // đóng hover và hiển thị marker 2d lên
                    }
                }
            });

        //***************************************************************************************************************************
        var click3D = map.addListener("click", (args) => {
            if (exploit.GLOBAL.TypeDraw == -1 && TimKiem.GLOBAL.isDrawSearch == -1) {
                if (!exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                    var oldForcus = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
                    if (oldForcus != undefined) {
                        if (oldForcus.object != undefined && oldForcus.object != null) {
                            oldForcus.object.setSelected(false);
                        }
                    }

                    map.setSelectedBuildings([]); // bỏ highlight object map

                    if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
                        exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(null);
                        exploit.GLOBAL.ActionToolbar.MarkerShowInfo = null;
                    }

                    ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                    ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ

                    ThongTin.GLOBAL.idDirectory = "";
                    ThongTin.GLOBAL.idMainObject = "";
                    map.setSelectedBuildings([]); // bỏ highlight object map

                    if (args.building.userData != undefined && args.building.userData != null) {
                        var id = args.building.userData.id;
                        if (id != ThongTin.GLOBAL.idMainObject) {
                            ThongTin.GLOBAL.idMainObject = id;

                            ThongTin.getMainObject(ThongTin.GLOBAL.idMainObject);
                            ThongTin.showHideViewProperty(true);
                            ThongTin.GLOBAL.idDirectory = ThongTin.getIdDirectoryByMainObjectId(ThongTin.GLOBAL.idMainObject);
                        }
                    }
                    args.building.setSelected(true);

                    exploit.resetHighlight2DModeTo3D(); // reset highlight 2d
                }
            }
            else {
                // do diện tích, lấy tọa độ, đo đường thẳng
                exploit.ChoosenTypeDraw(args);
            }
        }, { building: true })

        //***************************************************************************************************************************
        // sự kiện click đối tượng 3d
        var click3DMap = map.addListener("click", (args) => {
            if (exploit.GLOBAL.TypeDraw == -1 && TimKiem.GLOBAL.isDrawSearch == -1) {
                if (!exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                    if (args.building != undefined && args.building != null) {
                        let id = args.building.id;
                        let objchosen = exploit.GLOBAL.listObject3D.find(x => x.idObject == id);
                        let objold = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
                        map.setSelectedBuildings([]); // bỏ highlight object map

                        if (objchosen != null && objchosen != undefined) {
                            if (objold != null && objold != undefined) {
                                if (objold.object != null) {
                                    objold.object.setSelected(false); // bỏ highlight object layer
                                    //map.setSelectedBuildings([objold.object.idObject])
                                }
                            }

                            let value = objchosen.id;
                            ThongTin.GLOBAL.idMainObject = value;
                            ThongTin.getMainObject(ThongTin.GLOBAL.idMainObject);
                            ThongTin.showHideViewProperty(true);
                            ThongTin.GLOBAL.idDirectory = ThongTin.getIdDirectoryByMainObjectId(ThongTin.GLOBAL.idMainObject);

                            //args.building.setSelected(true);
                            map.setSelectedBuildings([args.building.id])

                            exploit.resetHighlight2DModeTo3D(); // reset highlight 2d
                        }
                    }
                }
                //args.building.setSelected(true);
            }
            else {
                // do diện tích, lấy tọa độ, đo đường thẳng
                exploit.ChoosenTypeDraw(args);
            }
        }, { mapbuilding: true });

        //***************************************************************************************************************************
        // sự kiện click bất kỳ trên map
        var clickMap = map.addListener("click", (args) => {
            if (exploit.GLOBAL.TypeDraw != -1 || TimKiem.GLOBAL.isDrawSearch != -1) {
                let iLatLng = [];
                switch (exploit.GLOBAL.TypeDraw) {
                    case 0:
                        exploit.DrawPoint(args.location.lat, args.location.lng);
                        break;
                    case 1:
                        if (exploit.GLOBAL.IsResetDraw) {
                            exploit.RemoveLine();
                            exploit.ShowPolylineInfoMap();
                            exploit.GLOBAL.IsResetDraw = false;
                        }
                        exploit.createPointMutipleLine(args.location.lat, args.location.lng, 1);
                        let listMarkerMulti = exploit.GLOBAL.ArrayLine.ListPoint;

                        $.each(listMarkerMulti, function (i, obj) {
                            let latLng = { lat: obj.getPosition().lat, lng: obj.getPosition().lng };
                            iLatLng.push(latLng);
                        });
                        if (iLatLng.length > 1) {
                            let startPoint = [iLatLng[iLatLng.length - 2].lng, iLatLng[iLatLng.length - 2].lat];
                            let endPoint = [iLatLng[iLatLng.length - 1].lng, iLatLng[iLatLng.length - 1].lat];
                            exploit.ShowMeterDrawLine(startPoint, endPoint, false);
                            exploit.ShowPolylineInfoMap();
                        }
                        exploit.DrawLine();
                        break;
                    case 2:
                        if (exploit.GLOBAL.IsResetDraw) {
                            exploit.RemovePolygon();
                            exploit.ShowPolyGonInfoMap();
                            exploit.GLOBAL.IsResetDraw = false;
                        }

                        exploit.createPointMutiplePolygon(args.location.lat, args.location.lng);
                        if (exploit.GLOBAL.ArrayPolygon.ListPoint.length == 2) {
                            let listarea = exploit.GLOBAL.ArrayPolygon.ListPoint;
                            $.each(listarea, function () {
                                let item = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                                iLatLng.push(item);
                            })

                            exploit.createPolylineLoDat(iLatLng);
                        }
                        else if (exploit.GLOBAL.ArrayPolygon.ListPoint.length > 2) {
                            exploit.DrawPolygon();
                            if (exploit.GLOBAL.ArrayPolygon.Polyline != null) {
                                exploit.GLOBAL.ArrayPolygon.Polyline.setMap(null);
                                exploit.GLOBAL.ArrayPolygon.Polyline = null;
                            }

                            exploit.ShowPolyGonInfoMap();
                        }
                        break;
                    default:
                }
            }
            else {
                if (exploit.GLOBAL.listGroundOvelay.length > 0 && !ReferenceContent.CONSTS.isCheckReference) {
                    ThongTin.GLOBAL.MarkerFocus = null;
                    ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                    ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ
                    exploit.getInforShapeMapTile(args);
                } else {
                    if (ReferenceContent.CONSTS.isCheckReference) {
                        ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                        ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ
                        ReferenceContent.checkClickObjectReferenceOptionMaptile(args);
                    }
                }
            }

            if (exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != undefined && exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
                    exploit.GLOBAL.ActionToolbar.MarkerShowInfo.showInfoWindow();
                }

                // hiển thị lại bảng thông tin hover
                $.each(ThongTin.GLOBAL.listHighlightPoint, function (index, obj)
                {
                    if (obj.properties.infoWindow != "") {
                        obj.showInfoWindow();
                        return false; // breaks
                    }
                })
            }
        });

        // kết thúc vẽ
        //***************************************************************************************************************************
        let eventDoubleClickMapObject = map.addListener("dblClick", (args) => {
            if (exploit.GLOBAL.TypeDraw != -1) {
                exploit.GLOBAL.IsResetDraw = true;
                if (polylineTemp != null) {
                    polylineTemp.setMap(null);
                    polylineTemp = null;
                }
            }
        }, { marker: true, polygon: true, polyline: true })

        let eventDoubleClickMap = map.addListener("dblClick", (args) => {
            if (exploit.GLOBAL.TypeDraw != -1) {
                exploit.GLOBAL.IsResetDraw = true;
                if (polylineTemp != null) {
                    polylineTemp.setMap(null);
                    polylineTemp = null;
                }
            }
        });

        //***************************************************************************************************************************
        // sư kiện di chuyển 1 marker trên bản đồ
        let eventdragDrawStart = map.addListener("dragStart", (args) => {
            if (exploit.GLOBAL.TypeDraw != -1) {
                if (polylineTemp != null) {
                    polylineTemp.setMap(null);
                    polylineTemp = null;
                }
            }
        }, { marker: true });

        // sư kiện di chuyển 1 marker trên bản đồ
        let eventdragDraw = map.addListener("dragEnd", (args) => {
            //line
            if (exploit.GLOBAL.TypeDraw != -1) {
                switch (exploit.GLOBAL.TypeDraw) {
                    case 1:
                        if (exploit.GLOBAL.ArrayLine.ListPoint != null && exploit.GLOBAL.ArrayLine.ListPoint.length > 0) {
                            let markerchangeMulti = exploit.GLOBAL.ArrayLine.ListPoint.find(x => x.id == args.marker.id);
                            if (typeof markerchangeMulti !== "undefined") {
                                iLatLng = [];
                                //console.log("args.marker.position = ", args.marker.getPosition());
                                //console.log("markerchangeMulti.position = ", markerchangeMulti.getPosition());
                                // markerchangeMulti.position = args.marker.position;
                                let position = args.marker.getPosition();
                                markerchangeMulti.setPosition(position);
                                let listMarker = exploit.GLOBAL.ArrayLine.ListPoint;

                                $.each(listMarker, function (i, obj) {
                                    let latLng = [obj.getPosition().lng, obj.getPosition().lat];
                                    iLatLng.push(latLng);
                                });
                                exploit.drawMultiPolylineEdit(iLatLng);
                            }
                        }
                        break;
                    case 2:
                        if (exploit.GLOBAL.ArrayPolygon.ListPoint != null && exploit.GLOBAL.ArrayPolygon.ListPoint.length > 0) {
                            let position = args.marker.getPosition();
                            let indexMarker = exploit.GLOBAL.ArrayPolygon.ListPoint.findIndex(x => x.id == args.marker.id);

                            exploit.GLOBAL.ArrayPolygon.ListPoint[indexMarker].setPosition(position);

                            // neu diem nho hon 2 thi fix poyline
                            if (exploit.GLOBAL.ArrayPolygon.ListPoint.length <= 2) {
                                exploit.EditPoylineInPolygon(exploit.GLOBAL.ArrayPolygon.ListPoint);
                            }
                            else if (exploit.GLOBAL.ArrayPolygon.Polygon != null) {
                                // set lai path cho polygon
                                var pathsP = exploit.GLOBAL.ArrayPolygon.Polygon.getPaths();
                                pathsP = exploit.setLatLngPolygon(pathsP, position, indexMarker);
                                exploit.GLOBAL.ArrayPolygon.Polygon.setPaths(pathsP);

                                var ILatLngDienTich = [];
                                $.each(pathsP[0], function (index, obj) {
                                    ILatLngDienTich.push([obj.lng, obj.lat])
                                })

                                // tính diện tích
                                let measure = new map4d.Measure([])
                                measure.setPath(
                                    ILatLngDienTich
                                );
                                exploit.GLOBAL.ArrayPolygon.Meter = measure;

                                // hiển thị thông tin
                                exploit.ShowPolyGonInfoMap();
                            }
                        }
                        break;
                    default:
                }
            }
        }, { marker: true, polygon: true, polyline: true });

        //***************************************************************************************************************************
        // sự kiện di chuyển chuột trên map
        let eventMouseMove = map.addListener("mouseMove", (args) => {
            // hiển thị line kế tiếp nếu đang vẽ đoạn thẳng hoặc đa giác
            if (exploit.GLOBAL.TypeDraw != -1) {
                if (!exploit.GLOBAL.IsResetDraw) {
                    switch (exploit.GLOBAL.TypeDraw) {
                        case 1:
                            if (exploit.GLOBAL.ArrayLine.ListPoint != null && exploit.GLOBAL.ArrayLine.ListPoint.length > 0) {
                                let path = [];
                                let listMarker = exploit.GLOBAL.ArrayLine.ListPoint;
                                let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                                let mousePoint = [args.location.lng, args.location.lat];
                                path.push(endPoint);
                                path.push(mousePoint);
                                exploit.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                                //mapdata.ShowMeterDraw(endPoint, mousePoint, true);
                            }
                            break;
                        case 2:
                            if (exploit.GLOBAL.ArrayPolygon.ListPoint != null && exploit.GLOBAL.ArrayPolygon.ListPoint.length > 0) {
                                let path = [];
                                let listMarker = exploit.GLOBAL.ArrayPolygon.ListPoint;
                                let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                                let mousePoint = [args.location.lng, args.location.lat];
                                path.push(endPoint);
                                path.push(mousePoint);
                                exploit.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                                //mapdata.ShowMeterDraw(endPoint, mousePoint, true);
                            }
                            break;
                        default: break;
                    }
                }
            }
        });

        //***************************************************************************************************************************
        // sự kiện click line vẽ kế tiếp
        let eventClickDraw = map.addListener("click", (args) => {
            if (exploit.GLOBAL.TypeDraw != -1) {
                let iLatLng = [];
                switch (exploit.GLOBAL.TypeDraw) {
                    case 0:
                        exploit.DrawPoint(args.location.lat, args.location.lng);
                        break;
                    case 1:
                        if (exploit.GLOBAL.IsResetDraw) {
                            exploit.RemoveLine();
                            exploit.ShowPolylineInfoMap();
                            exploit.GLOBAL.IsResetDraw = false;
                        }
                        exploit.createPointMutipleLine(args.location.lat, args.location.lng, 1);
                        let listMarkerMulti = exploit.GLOBAL.ArrayLine.ListPoint;

                        $.each(listMarkerMulti, function (i, obj) {
                            let latLng = { lat: obj.getPosition().lat, lng: obj.getPosition().lng };
                            iLatLng.push(latLng);
                        });
                        if (iLatLng.length > 1) {
                            let startPoint = [iLatLng[iLatLng.length - 2].lng, iLatLng[iLatLng.length - 2].lat];
                            let endPoint = [iLatLng[iLatLng.length - 1].lng, iLatLng[iLatLng.length - 1].lat];
                            exploit.ShowMeterDrawLine(startPoint, endPoint, false);
                            exploit.ShowPolylineInfoMap();
                        }
                        exploit.DrawLine();
                        break;
                    case 2:
                        if (exploit.GLOBAL.IsResetDraw) {
                            exploit.RemovePolygon();
                            exploit.ShowPolyGonInfoMap();
                            exploit.GLOBAL.IsResetDraw = false;
                        }

                        exploit.createPointMutiplePolygon(args.location.lat, args.location.lng);
                        if (exploit.GLOBAL.ArrayPolygon.ListPoint.length == 2) {
                            let listarea = exploit.GLOBAL.ArrayPolygon.ListPoint;
                            $.each(listarea, function () {
                                let item = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                                iLatLng.push(item);
                            })

                            exploit.createPolylineLoDat(iLatLng);
                        }
                        else if (exploit.GLOBAL.ArrayPolygon.ListPoint.length > 2) {
                            exploit.DrawPolygon();
                            if (exploit.GLOBAL.ArrayPolygon.Polyline != null) {
                                exploit.GLOBAL.ArrayPolygon.Polyline.setMap(null);
                                exploit.GLOBAL.ArrayPolygon.Polyline = null;
                            }

                            exploit.ShowPolyGonInfoMap();
                        }
                        break;
                    default:
                }
            }
        }, { polyline: true, polygon: true, marker: true });

        //***************************************************************************************************************************
        // hover một đối tượng 2d trên bản đồ để lấy thông tin
        var hover2d = map.addListener("hover", (args) => {
            //console.log(args);
            var marker = args.marker;
            if (exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                $(".mf-mapgl").css("cursor", "pointer");
                if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
                    exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(null);
                    exploit.GLOBAL.ActionToolbar.MarkerShowInfo = null;
                }

                ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ

                //$('.mf-info-window-container div').html('');
                if (marker != undefined && marker != null) {
                    if (args.marker.userData !== "" || args.marker.userData !== null || args.marker.userData !== undefined) {
                        var userData = args.marker.userData;
                        if (userData != undefined) {
                            var idLayer = userData.idDirectory;
                            var id = userData.id;

                            var objLayer = exploit.GLOBAL.listMarker.find(x => x.id == idLayer);

                            var objPoint = objLayer.lstMarker.filter(x => x.userData.id == id);
                            if (objPoint != undefined) {
                                if (objPoint.length == 1) {

                                    ThongTin.highlightObjectPoint(args.marker);
                                }
                                else {
                                    ThongTin.hightlightObjectMutiplePoint(objPoint);
                                }
                            }

                            args.marker.hideInfoWindow();
                            var html = exploit.getAndShowInfo(id, "point");
                            args.marker.setInfoWindow(html);
                            args.marker.showInfoWindow();

                            //exploit.GLOBAL.ActionToolbar.MarkerShowInfo = args.marker;
                        }
                    }
                }
            }
            else {
                if (marker != undefined && marker != null) {
                    args.marker.hideInfoWindow();
                }
            }
            // map.setSelectedBuildings([]); // bỏ highlight object map
        }, { marker: true, polygon: true, polyline: true });

        let eventhoverMapTile = map.addListener("hover", (args) => {
            $(".mf-mapgl").css("cursor", "default");
            if (exploit.GLOBAL.ActionToolbar.XemThongTinHover && exploit.GLOBAL.listGroundOvelay.length > 0) {
                exploit.GLOBAL.hoverObjectMaptile = null;
                exploit.getInforShapeMapTile(args, true);
                if (exploit.GLOBAL.hoverObjectMaptile !== null) {
                    ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                    ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ

                    ThongTin.highlightObjectGeosjon(exploit.GLOBAL.hoverObjectMaptile);
                    exploit.ShowInforHover(exploit.GLOBAL.hoverObjectMaptile.id, args.location.lat, args.location.lng);
                }
            }
        })

        let eventHoverData = map.data.addListener("hover", (args) => {
            if (exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                $(".mf-mapgl").css("cursor", "pointer");
                if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
                    exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(null);
                    exploit.GLOBAL.ActionToolbar.MarkerShowInfo = null;
                }

                ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ

                var id = args.feature._id;
                ThongTin.highlightObject(args);
                exploit.ShowInforHover(id, args.location.lat, args.location.lng);
            }
        });

        //***************************************************************************************************************************
        // sự kiện click poylgon
        let eventClickHighlight = map.addListener("click", (args) => {
            if (exploit.GLOBAL.TypeDraw == -1 && TimKiem.GLOBAL.isDrawSearch == -1 && !exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                ThongTin.GLOBAL.idDirectory = ThongTin.getIdDirectoryByMainObjectId(ThongTin.GLOBAL.idMainObject);
            }

        }, { polyline: true })
    },
    checkDevice: function () {
        //check mobile device
        if (window.matchMedia("(max-width: 767px)").matches && (!$(".div-content").hasClass("toogle-tree"))) {
            exploit.GLOBAL.isMobile = true;
        }

        if (exploit.GLOBAL.isMobile) {
            $(".div-content").addClass("toogle-tree");
            $(".toogle-tree-bar svg").css("transform", "rotateY(180deg)");
        }
    },
    showObjectOfLayer: function (id) {
        let count = this.getCountLayerById(id);
        $(exploit.SELECTORS.header_list_object_span).html(l('Main:ListObject') + ` (${count})`);
        count = Math.ceil(count / maxRequest);
        $('.spinner').css('display', 'block');
        for (var i = 0; i < count; i++) {
            this.showOjectLayerByPageAndId(id, i + 1);
        }
        setTimeout(function () {
            $('.spinner').css('display', 'none');
        }, (1000 * count));
    },
    HideObjectOfLayer: function (id) {
        let layer = exploit.GLOBAL.listfeatures.find(x => x.id == id);
        if (layer != null && layer != undefined) {
            // setTimeout(function () {
            if (exploit.GLOBAL.listfeatures.length > 1) {
                layer.features.forEach((feature) => {
                    map.data.remove(feature);
                });
                exploit.GLOBAL.listfeatures.splice(exploit.GLOBAL.listfeatures.findIndex(function (i) {
                    return i.id === id;
                }), 1);
            } else {
                map.data.clear();
                exploit.GLOBAL.listfeatures = [];
            }
            //}, 10);
        }
        let layerMarker = exploit.GLOBAL.listMarker.find(x => x.id == id);
        if (layerMarker != null && layerMarker != undefined) {
            //setTimeout(function () {
            layerMarker.lstMarker.forEach((marker) => {
                marker.setMap(null);
            });
            exploit.GLOBAL.listMarker.splice(exploit.GLOBAL.listMarker.findIndex(function (i) {
                return i.id === id;
            }), 1);
            // }, 10);
        }
        let layerObject = exploit.GLOBAL.listObject3D.filter(x => x.idLayer == id);
        if (layerObject.length > 0) {
            //setTimeout(function () {
            layerObject.forEach((marker) => {
                marker.object.setMap(null);
            });
            exploit.GLOBAL.listObject3D.splice(exploit.GLOBAL.listObject3D.findIndex(function (i) {
                return i.id === id;
            }), 1);
            //}, 10);
        }
    },
    HideObjectOfLayer2: function (id) {
        setTimeout(function () {
            let checklayer = exploit.GLOBAL.listgeojson.find(x => x.id == id);
            if (typeof checklayer !== "undefined" && checklayer !== null && checklayer !== "") {
                checklayer = exploit.GLOBAL.listgeojson.filter(x => x.id == id);
                if (checklayer.length > 1) {
                    for (var i = 0; i < checklayer.length; i++) {
                        exploit.GLOBAL.listgeojson.splice(exploit.GLOBAL.listgeojson.findIndex(function (i) {
                            return i.id === id;
                        }), 1);
                    }
                } else {
                    exploit.GLOBAL.listgeojson.splice(exploit.GLOBAL.listgeojson.findIndex(function (i) {
                        return i.id === id;
                    }), 1);
                }

                map.data.clear();
                exploit.GLOBAL.lstFeature = [];
                exploit.GLOBAL.listgeojson.forEach((geojson) => {
                    exploit.showDataGeojsonReset(geojson.geojson, geojson.id);
                });
            }

            checklayer = exploit.GLOBAL.ArrayGeometryByDirectory.find(x => x.id == id);
            if (typeof checklayer !== "undefined" && checklayer !== null && checklayer !== "") {
                checklayer = exploit.GLOBAL.ArrayGeometryByDirectory.filter(x => x.id == id);
                if (checklayer.length > 1) {
                    for (var i = 0; i < checklayer.length; i++) {
                        exploit.GLOBAL.ArrayGeometryByDirectory.splice(exploit.GLOBAL.ArrayGeometryByDirectory.findIndex(function (i) {
                            return i.id === id;
                        }), 1);
                    }
                } else {
                    exploit.GLOBAL.ArrayGeometryByDirectory.splice(exploit.GLOBAL.ArrayGeometryByDirectory.findIndex(function (i) {
                        return i.id === id;
                    }), 1);
                }

                map.data.clear();
                exploit.GLOBAL.lstFeature = [];
                exploit.GLOBAL.ArrayGeometryByDirectory.forEach((geojson) => {
                    exploit.showDataGeojsonReset(geojson.geojson, geojson.id);
                });
            }

            let layerMarker = exploit.GLOBAL.listMarker.find(x => x.id == id);
            if (layerMarker != null && layerMarker != undefined) {
                layerMarker.lstMarker.forEach((marker) => {
                    marker.setMap(null);
                });
                exploit.GLOBAL.listMarker.splice(exploit.GLOBAL.listMarker.findIndex(function (i) {
                    return i.id === id;
                }), 1);
            }
            let layerObject = exploit.GLOBAL.listObject3D.filter(x => x.idLayer == id);
            if (layerObject.length > 0) {
                layerObject.forEach((marker) => {
                    marker.object != null && marker.object.setMap(null);
                    exploit.GLOBAL.listObject3D.splice(exploit.GLOBAL.listObject3D.findIndex(function (i) {
                        return i.id === marker.id;
                    }), 1);
                });
            }
            exploit.showObjectMap();

            let lst = exploit.GLOBAL.listDirectoryMainObject.filter(x => x.id == id);
            if (lst != null && lst != undefined) {
                exploit.GLOBAL.listDirectoryMainObject.splice(exploit.GLOBAL.listDirectoryMainObject.findIndex(function (i) {
                    return i.id === id;
                }), 1);
            }

        }, 10);
    },
    //-------------- hiển thị đối tượng point lên bản đồ-----------------
    showListMarkerObject: function (lst, id, listOfDirectory) {
        let object = {
            id: id,
            lstMarker: []
        };
        var mod = map.is3dMode();

        $.each(lst, function (i, obj) {

            let imageicon = "/common/location-2.png";
            if (obj.properties != undefined && obj.properties != null) {
                imageicon = (obj.properties.image2D != "" && obj.properties.image2D != null) ? obj.properties.image2D
                    : (exploit.GLOBAL.image2dDirectory != "" && exploit.GLOBAL.image2dDirectory != null)
                        ? exploit.GLOBAL.image2dDirectory : "/common/location-2.png";
            }

            if (obj.geometry.type == "Point") {
                let markerDraw = new map4d.Marker({
                    position: { lat: obj.geometry.coordinates[1], lng: obj.geometry.coordinates[0] },
                    icon: new map4d.Icon(21, 22, imageicon),
                    anchor: [0.5, 1],
                    windowAnchor: { x: 0.5, y: 0.4 },
                    zIndex: 99,
                });
                markerDraw.setUserData(obj);

                if (!mod) {
                    markerDraw.setMap(map);
                }
                if (mod || !ViewObjectWithZoom.checkShowObjectWithZoom(id)) {
                    markerDraw.setVisible(false);
                }

                listOfDirectory.push(obj.id);
                object.lstMarker.push(markerDraw);

                //var html = `
                //    <div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
                //        <div class="gm-style-iw-a" style="position: absolute;top: 40px;">
                //            <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
                //                <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px;">
                //                    <img src= "${imageicon}" style="width: 25px;" />
                //                </div>
                //            </div>
                //        </div>
                //    </div>`;

                //markerDraw.setInfoWindow(html);

            } else {
                $.each(obj.geometry.coordinates, function (iz, item) {
                    let markerDraw = new map4d.Marker({
                        position: { lat: item[1], lng: item[0] },
                        icon: new map4d.Icon(21, 22, imageicon),
                        anchor: [0.5, 1],
                        windowAnchor: { x: 0.5, y: 0.4 },
                        zIndex: 99,
                        //snippet: id
                        //draggable: true
                        //title: name
                    });
                    markerDraw.setUserData(obj);
                    if (!mod) {
                        markerDraw.setMap(map);
                    }

                    if (mod) {
                        markerDraw.setVisible(false);
                    }

                    object.lstMarker.push(markerDraw);

                    var html = `
                    <div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
                        <div class="gm-style-iw-a" style="position: absolute;top: 40px;">
                            <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
                                <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px;">
                                    <img src= "${imageicon}" style="width: 25px;" />
                                </div>
                            </div>
                        </div>
                    </div>`;

                    //markerDraw.setInfoWindow(html);

                    let check = listOfDirectory.find(x => x == obj.id);
                    check == undefined && listOfDirectory.push(obj.id);
                });
            }
        });

        this.setListMarkerMainObject(id, object);
    },
    getDataGeojsonByProperties: function (listMainObject, listOfDirectory, id = "") {
        let features = [];
        $.each(listMainObject, function (i, obj) {
            let properties = obj.properties == null ? exploit.GLOBAL.propertiesSetting : obj.properties;
            let defaultProperties = {
                "stroke": properties.stroke,
                "stroke-width": properties.strokeWidth,
                "stroke-opacity": properties.strokeOpacity,
                "fill": properties.fill,
                "fill-opacity": properties.fillOpacity,
                "idDirectory": id
            };
            let feature = {};
            feature.type = "Feature";
            feature.properties = defaultProperties;
            feature.geometry = obj.geometry;
            feature.id = obj.id;
            features.push(feature);
            listOfDirectory.push(obj.id);
        });
        let geojson = {
            "type": "FeatureCollection",
            "features": features
        };
        return geojson;
    },
    showDataGeojson: function (geojsonString, id) {
        let features = map.data.addGeoJson(geojsonString);
        let object = {
            id: id,
            features: features
        };
        exploit.GLOBAL.listfeatures.push(object);
    },

    //-----------hiển thị đối tượng đa giác (line, polyline,..) lên bản đồ ---------
    showDataGeojson2: function (geojsonString, id) {
        var mod = map.is3dMode();

        var arrayFeature = [];

        if (!mod && ViewObjectWithZoom.checkShowObjectWithZoom(id)) { //check object with zoom
            let features = map.data.addGeoJson(geojsonString);
            exploit.GLOBAL.lstFeature.push(features);

            arrayFeature = features;
        }


        // lưu danh sách feature trên bản đồ theo từng layer
        var exitsFeature = exploit.GLOBAL.ArrayFeaturesByDirectory.find(x => x.id == id);
        if (exitsFeature != undefined) {
            exitsFeature.features = arrayFeature;
        }
        else {
            var objFeature = {
                id: id,
                features: arrayFeature
            };
            exploit.GLOBAL.ArrayFeaturesByDirectory.push(objFeature);
        }


        let object = {
            id: id,
            geojson: geojsonString
        };
        //let check = exploit.GLOBAL.listgeojson.find(x => x.id == id);
        //if (check == null || check == undefined) {
        exploit.GLOBAL.listgeojson.push(object);
        //}

        let check = exploit.GLOBAL.ArrayGeometryByDirectory.find(x => x.id == id);
        if (check == null || check == undefined) {
            exploit.GLOBAL.ArrayGeometryByDirectory.push(object);
        }
    },
    showDataGeojsonReset: function (geojsonString, id) {
        var mod = map.is3dMode();
        if (!mod && ViewObjectWithZoom.checkShowObjectWithZoom(id)) {
            let features = map.data.addGeoJson(geojsonString);
            exploit.GLOBAL.lstFeature.push(features);
        }
    },
    setDefaultColor: function (id) {
        var urls = window.location.origin + '/api/HTKT/PropertiesSetting/getByIdDictionary';
        $.ajax({
            type: "GET",
            url: urls,
            data: {
                id: id
            },
            async: false,
            contentType: "application/json",
            success: function (data) {
                if (data.code == "ok") {
                    if (data.result != null) {
                        exploit.GLOBAL.propertiesSetting.MinZoom = data.result.minZoom;
                        exploit.GLOBAL.propertiesSetting.MaxZoom = data.result.maxZoom;
                        exploit.GLOBAL.propertiesSetting.Stroke = data.result.stroke;
                        exploit.GLOBAL.propertiesSetting.StrokeWidth = data.result.strokeWidth;
                        exploit.GLOBAL.propertiesSetting.StrokeOpacity = data.result.strokeOpacity;
                        exploit.GLOBAL.propertiesSetting.StyleStroke = data.result.styleStroke;
                        exploit.GLOBAL.propertiesSetting.Fill = data.result.fill;
                        exploit.GLOBAL.propertiesSetting.FillOpacity = data.result.fillOpacity;
                        exploit.GLOBAL.propertiesSetting.Image2D = data.result.image2D;
                    }
                }
            },
        });
    },
    newMap4dBuilding: function (data) {
        var building = null;
        if (data.coordinates != null && data.coordinates.length > 0) {
            building = new map4d.Building({
                name: data.name,
                id: data.id,
                scale: data.scale, // Tỉ lệ vẽ của đối tượng
                bearing: data.bearing, // Góc quay của đối tượng
                elevation: data.elevation, // Độ cao so với mặt nước biển
                position: data.location,
                coordinates: data.coordinates,
                height: data.height
            });
        } else {
            //tạo đối tượng model từ ObjectOption
            building = new map4d.Building({
                name: data.name,
                id: data.id,
                scale: data.scale, // Tỉ lệ vẽ của đối tượng
                bearing: data.bearing, // Góc quay của đối tượng
                elevation: data.elevation, // Độ cao so với mặt nước biển
                position: data.location,
                model: data.objectUrl,
                texture: data.texture
            });
        }
        return building;
    },
    LocationMapDefault: function (id) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: true,
            url: exploit.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=' + id,
            data: {
            },
            beforeSend: function () {
                $('.spinner').css('display', 'block');
            },
            success: function (data) {
                if (data.code == "ok") {
                    var mod = map.is3dMode();
                    if (data.result != null) {
                        exploit.GLOBAL.listDirectoryMainObject.push({ id: id, listMainObject: [], propertiesDirectory: data.result });
                        //check optionDirectory == 1 opend data
                        if (data.result.optionDirectory === 1) {
                            exploit.showObjectOfLayer(id);
                        }
                        if (data.result.location != null) {
                            if (data.result.location.lat <= 0 && data.result.location.lng <= 0 && exploit.GLOBAL.centerMap.lat != 0 && exploit.GLOBAL.centerMap.lng != 0) {
                                let lat = parseFloat(exploit.GLOBAL.centerMap.lat);
                                let lng = parseFloat(exploit.GLOBAL.centerMap.lng);
                                let zoom = data.result.zoom > 0 ? data.result.zoom : (exploit.GLOBAL.defaultConfig.zoom != null && exploit.GLOBAL.defaultConfig.zoom > 0 ? exploit.GLOBAL.defaultConfig.zoom : 15);

                                var cameraPosition = {
                                    target: { lat: lat, lng: lng },
                                    tilt: 0,
                                    bearing: 0,
                                    zoom: zoom
                                };

                                map.moveCamera(cameraPosition, null);
                            } else {
                                let lat = data.result.location.lat > 0 ? parseFloat(data.result.location.lat) : parseFloat(exploit.GLOBAL.defaultConfig.lat);
                                let lng = data.result.location.lng > 0 ? parseFloat(data.result.location.lng) : parseFloat(exploit.GLOBAL.defaultConfig.lng);
                                let zoom = data.result.zoom > 0 ? data.result.zoom : (exploit.GLOBAL.defaultConfig.zoom != null && exploit.GLOBAL.defaultConfig.zoom > 0 ? exploit.GLOBAL.defaultConfig.zoom : 15);

                                var cameraPosition = {
                                    target: { lat: lat, lng: lng },
                                    tilt: 0,
                                    bearing: 0,
                                    zoom: zoom
                                };

                                map.moveCamera(cameraPosition, null);
                            }
                        }

                        if (mod) {
                            map.enable3dMode(true);
                        }

                        if (data.result.groundMaptile != "" && data.result.groundMaptile != null && data.result.optionDirectory == 1) {
                            var tags = data.result.tags;
                            var objectStringArray = [];
                            if (tags != null) {
                                objectStringArray = (new Function("return " + tags["bounds"] + ";")());
                            }
                            else {
                                objectStringArray = (new Function("return " + JSON.stringify(bounds) + ";")());
                            }

                            var urlTile = exploit.getUrlTile(data);
                            let overlayOptions = {
                                getUrl: function (x, y, z, is3dMode) {
                                    return urlTile + `/${z}/${x}/${y}.${exp.trim()}`;
                                },
                                bounds: objectStringArray,
                                override: false
                            };

                            overlay = new map4d.TileOverlay(overlayOptions);
                            //overlay = new map4d.GroundOverlay(overlayOptions);

                            overlay.setMap(map);

                            let object = {
                                id: id,
                                groundOvelay: overlay,
                                optionDirectory: 1,
                                url: ""
                            };
                            exploit.GLOBAL.listGroundOvelay.push(object);
                        } else if (data.result.groundMaptile != "" && data.result.groundMaptile != null) {
                            if (data.result.groundMaptile.trim().indexOf("SRS=EPSG") >= 0) {
                                //exploit.GLOBAL.isCheckInfor = true;
                                //exploit.GLOBAL.linkTileMap = data.result.groundMaptile.trim();
                                var urlTile = data.result.groundMaptile.trim().split('${');
                                var bbox = (data.result.tags != null && data.result.tags.bounds != null) ? JSON.parse(data.result.tags.bounds.replace(/([a-zA-Z0-9]+?):/g, '"$1":').replace(/'/g, '"')) : [];

                                let optionAnPhu = null;
                                //map tile option 2 / 3 
                                if ((data.result.optionDirectory == 2 || data.result.optionDirectory == 3) && (data.result.groundMaptile.trim().indexOf("SRS=EPSG:4326") >= 0 || data.result.groundMaptile.trim().indexOf("4326") >= 0)) {
                                    optionAnPhu = {
                                        getUrl: function (x, y, zoom, is3dMode) {
                                            let groundOvelay = exploit.GLOBAL.listGroundOvelay.find(x => x.groundOvelay.id == this.id);
                                            if (convertxyz.CheckInBboxMaptile(groundOvelay.bbox, x, y, zoom) && ViewObjectWithZoom.checkShowObjectWithZoom(groundOvelay.id)) {
                                                var bbox = convertxyz.tile2boundingBox(x, y, zoom);
                                                if (bbox !== null) {
                                                    return urlTile[0] + `${bbox}`;
                                                }
                                            }
                                        },
                                        visible: true,
                                        zIndex: 12
                                    }
                                }
                                else if (data.result.optionDirectory == 2 || data.result.optionDirectory == 3) {
                                    optionAnPhu = {
                                        getUrl: function (x, y, zoom, is3dMode) {
                                            let groundOvelay = exploit.GLOBAL.listGroundOvelay.find(x => x.groundOvelay.id == this.id);
                                            if (ViewObjectWithZoom.checkShowObjectWithZoom(groundOvelay.id)) {
                                                var bbox = convertxyz.ConvertWGS84ToVN2000New(x, y, zoom);
                                                if (bbox !== null) {
                                                    return urlTile[0] + `${bbox}`;
                                                }
                                            }
                                        },
                                        visible: true,
                                        zIndex: 12
                                    }
                                }
                                let overlayAP = new map4d.TileOverlay(optionAnPhu);
                                let object = {
                                    id: id,
                                    groundOvelay: overlayAP,
                                    optionDirectory: data.result.optionDirectory,
                                    url: data.result.groundMaptile.trim(),
                                    bbox: bbox
                                };
                                exploit.GLOBAL.listGroundOvelay.push(object);
                                overlayAP.setMap(map);
                            } else {
                                var urlTile = exploit.getUrlTile(data);
                                let overlayOptions = {
                                    getUrl: function (x, y, z, is3dMode) {
                                        return urlTile + `/${z}/${x}/${y}.${exp.trim()}`;
                                    },
                                    visible: true,
                                    zIndex: 1
                                };
                                let overlayAP = new map4d.TileOverlay(overlayOptions);
                                overlayAP.setMap(map);
                                let object = {
                                    id: id,
                                    groundOvelay: overlayAP,
                                    optionDirectory: data.result.optionDirectory,
                                    url: ""
                                };
                                exploit.GLOBAL.listGroundOvelay.push(object);
                            }
                        }
                        // thêm thuộc tính vào dữ liệu tìm kiếm
                        TimKiem.AddPropertiesLayer(data.result.listProperties, id);
                    }
                }
            },
            complete: function () {
                $('.spinner').css('display', 'none')
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    HideLocationMapDefault: function (id) {
        let object = exploit.GLOBAL.listGroundOvelay.find(x => x.id == id);
        if (object != null && object != undefined) {
            object.groundOvelay.setMap(null);
            exploit.GLOBAL.listGroundOvelay.splice(exploit.GLOBAL.listGroundOvelay.findIndex(function (i) {
                return i.id === id;
            }), 1);
        }
    },
    showObjectMap: function () {
        let objectcount = exploit.GLOBAL.listObject3D.filter(x => x.object == null);
        if (objectcount.length > 0) {
            let array = [];
            $.each(objectcount, function (i, obj) {
                array.push(obj.idObject);
            });

            map.setVisibleBuildings(array);
        }

        exploit.EnableGeoMetryExample();

    },
    getListObjectInGeometry: function () {
        var array = [];
        $.ajax({
            type: "GET",
            url: exploit.CONSTS.URL_AJAXGETOBJECTBYGEOMETRY,
            data: {},
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if (res.code == "ok") {
                var data = JSON.parse(res.result);
                if (data.code == "ok" && data.result != null && data.result.length > 0) {
                    //mapdata.GLOBAL.listObjectMapInRadius = [];
                    //var array = [];
                    data.result.sort(function (a, b) {
                        var textA = a.name.trim().slice(0, 1).toUpperCase();
                        var textB = b.name.trim().slice(0, 1).toUpperCase();
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    });
                    $.each(data.result, function (i, obj) {
                        var object = {
                            "id": obj.id,
                            "name": obj.name,
                            "type": obj.type,
                            "address": obj.address,
                            "location": obj.location,
                            "scale": obj.scale,
                            "bearing": obj.bearing,
                            "elevation": obj.elevation,
                            "heightScale": obj.heightScale
                        };

                        if (exploit.GLOBAL.lstArrayQTSC.indexOf(obj.id) == -1) {
                            //mapdata.GLOBAL.listObjectMapInRadius.push(object);
                            array.push(obj.id);
                        }

                        //array.push(object);
                    });
                    exploit.GLOBAL.listObjectInGeometry = array;
                }
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });

        return array;
    },
    EnableGeoMetryExample: function () {
        var arrayDefault = (exploit.GLOBAL.listObjectInGeometry.length > 0) ? exploit.GLOBAL.listObjectInGeometry : exploit.getListObjectInGeometry();
        var arrayAll = arrayDefault.concat(exploit.GLOBAL.lstArrayQTSC);
        var dem = $('.check').length;

        if (!$(exploit.SELECTORS.btn_show_building_out).hasClass('open')) {
            if (dem == 0) {
                map.setBuildingsEnabled(false);
            }
            else {
                if (exploit.GLOBAL.lstArrayQTSC.length == 0) {
                    map.setBuildingsEnabled(false);
                }
                else {
                    map.setBuildingsEnabled(true);
                    //map.setBuildingsEnabled(false); // trạng thái hiển thị building của bản đồ
                    map.setVisibleBuildings(exploit.GLOBAL.lstArrayQTSC); // hiển thị building QTSC
                    map.setHiddenBuildings(arrayDefault); // ẩn building QTSC
                }
            }
        }
        else {
            map.setBuildingsEnabled(true);
            //map.setBuildingsEnabled(true); // trạng thái hiển thị building của bản đồ
            map.setVisibleBuildings(arrayAll); // hiển thị building QTSC
            map.setHiddenBuildings([]); // ẩn bulding QTSC

        }
    },
    // di chuyyển camera tới một vùng
    fitBounds: function (data) {
        let latLngBounds = new map4d.LatLngBounds();
        let paddingOptions = {
            top: 10,
            bottom: 50,
            left: 50,
            right: 50
        };

        for (var i = 0; i < data.length; i++) {
            latLngBounds.extend(data[i]);
        }
        map.fitBounds(latLngBounds, paddingOptions);
    },

    //**************************************
    // Draw Point
    DrawPoint: function (lat, lng) {
        if (exploit.GLOBAL.TypeDraw == 0) {
            exploit.RemoveDrawPoint();

            // vẽ điểm
            exploit.GLOBAL.PointDraw = new map4d.Marker({
                position: { lat: lat, lng: lng },
                icon: new map4d.Icon(26, 26, location.origin + "/common/location-1.png"),
                anchor: [0.5, 1],
                zIndex: 99
            });
            exploit.GLOBAL.PointDraw.setMap(map);

            var radius = 100;
            if ($(exploit.SELECTORS.input_radius).val() != "") {
                radius = $(exploit.SELECTORS.input_radius).val();
            }
            // vẽ bán kính
            exploit.GLOBAL.RadiusPoint = new map4d.Circle({
                center: { lat: lat, lng: lng },
                fillColor: "#28a745",
                fillOpacity: 0.5,
                radius: radius,
                strokeWidth: 1.0,
                strokeColor: "#0000ff"
            });

            /*exploit.GLOBAL.RadiusPoint.setMap(map);*/

            var html = `<div style="display : flex;">
                        <div style="font-weight: bold; margin-right: 4px; margin-bottom:4px;">${l("Longitude")}:</div><div> ${lng}</div>
                            </div>
                        <div style="display : flex;">
                        <div style="font-weight: bold; margin-right: 6px;">${l("Latitude")}:</div><div> ${lat}</div>
                            </div>`;
            $(exploit.SELECTORS.body_infor_map).html(html);
            $(exploit.SELECTORS.content_infor_map).addClass('open');
        }
    },
    RemoveDrawPoint: function () {

        if (exploit.GLOBAL.PointDraw != null) {
            exploit.GLOBAL.PointDraw.setMap(null);
        }

        if (exploit.GLOBAL.RadiusPoint != null) {
            exploit.GLOBAL.RadiusPoint.setMap(null);
        }
    },
    // Draw line
    createPointMutipleLine: function (lat, lng) {
        let markerDraw = new map4d.Marker({
            position: { lat: lat, lng: lng },
            icon: new map4d.Icon(8, 8, location.origin + "/common/EllipseBlue.svg"),
            anchor: [0.5, 0.5],
            draggable: true,
            zIndex: 2
            //title: name
        })
        markerDraw.setMap(map);

        if (exploit.GLOBAL.ArrayLine.ListPoint == null) {
            exploit.GLOBAL.ArrayLine.ListPoint = [];
        }

        exploit.GLOBAL.ArrayLine.ListPoint.push(markerDraw);
    },
    ShowMeterDrawLine: function (endPoint, mousePoint, check) {
        let lat = (endPoint[1] + mousePoint[1]) / 2;
        let lng = (endPoint[0] + mousePoint[0]) / 2;
        let measure = new map4d.Measure([endPoint, mousePoint,]);
        let length = (Math.round(measure.length * 100) / 100).toString();

        if (exploit.GLOBAL.ArrayLine.ListMeter == null) {
            exploit.GLOBAL.ArrayLine.ListMeter = [];
        }
        exploit.GLOBAL.ArrayLine.Distacne += parseFloat(length)
    },
    DrawLine: function () {
        var iLatLng = [];
        $.each(exploit.GLOBAL.ArrayLine.ListPoint, function (i, obj) {

            var latlng = [obj.getPosition().lng, obj.getPosition().lat];
            iLatLng.push(latlng);
        });

        // vẽ đường thẳng
        if (exploit.GLOBAL.ArrayLine.Polyline != null) {
            exploit.GLOBAL.ArrayLine.Polyline.setMap(null);
        }

        var polyline = new map4d.Polyline({
            path: iLatLng, visible: true, strokeColor: "#00559A", strokeWidth: 2.0, strokeOpacity: 2.0,
            closed: false,
            zIndex: 2
        })

        exploit.GLOBAL.ArrayLine.Polyline = polyline;
        polyline.setMap(map);

        // vẽ chiều rộng của đường thẳng
        if (exploit.GLOBAL.ArrayLine.Acreage != null) {
            exploit.GLOBAL.ArrayLine.Acreage.setMap(null);
        }
        var regt = 100;
        if ($(exploit.SELECTORS.input_vung_dem_line).val() != "") {
            regt = parseInt($(exploit.SELECTORS.input_vung_dem_line).val());
        }
        var polylineDistance = new map4d.Polyline({
            path: iLatLng, visible: true, strokeColor: "#00559A", strokeWidth: regt, strokeOpacity: 0.4,
            closed: false,
            zIndex: 2
        })
        //thêm polyline vào map
        polylineDistance.setMap(map);

        exploit.GLOBAL.ArrayLine.Acreage = polylineDistance;
    },
    ShowPolylineInfoMap: function () {
        var regt = 10;
        if ($(exploit.SELECTORS.input_vung_dem_line).val() != "") {
            regt = parseInt($(exploit.SELECTORS.input_vung_dem_line).val());
        }

        if (isNaN(regt)) {
            regt = 10;
        }
        var html = `<div style="display : flex;">
                        <div style="font-weight: bold; margin-right: 6px;">${l("Length")}:</div><div> ${exploit.GLOBAL.ArrayLine.Distacne.toFixed(2)}(m)</div>
                            </div>`;

        $(exploit.SELECTORS.body_infor_map).html(html);
        $(exploit.SELECTORS.content_infor_map).addClass('open');
    },
    drawMultiPolylineEdit: function (path) {
        exploit.GLOBAL.ArrayLine.Distacne = 0;
        if (exploit.GLOBAL.ArrayLine.ListPoint !== null && exploit.GLOBAL.ArrayLine.ListPoint.length > 0) {
            $.each(exploit.GLOBAL.ArrayLine.ListPoint, function (i, obj) {
                obj.setMap(null);
            });
            exploit.GLOBAL.ArrayLine.ListPoint = [];
        }

        if (exploit.GLOBAL.ArrayLine.Polyline !== null) {
            exploit.GLOBAL.ArrayLine.Polyline.setMap(null)
        }

        $.each(path, function (i, obj) {
            exploit.createPointMutipleLine(obj[1], obj[0]);
            if (i < path.length - 1) {
                exploit.ShowMeterDrawLine(path[i], path[i + 1]);
            }
        });

        exploit.DrawLine();

        exploit.ShowPolylineInfoMap();
    },
    RemoveLine: function () {
        // xóa điểm
        if (exploit.GLOBAL.ArrayLine.ListPoint != null) {
            for (var i = 0; i < exploit.GLOBAL.ArrayLine.ListPoint.length; i++) {
                exploit.GLOBAL.ArrayLine.ListPoint[i].setMap(null);
            }
        }

        if (exploit.GLOBAL.ArrayLine.ListMeter != null) {
            for (var i = 0; i < exploit.GLOBAL.ArrayLine.ListMeter.length; i++) {
                exploit.GLOBAL.ArrayLine.ListMeter[i].setMap(null);
            }
        }

        if (exploit.GLOBAL.ArrayLine.Polyline != null) {
            exploit.GLOBAL.ArrayLine.Polyline.setMap(null);
        }

        if (exploit.GLOBAL.ArrayLine.Acreage != null) {
            exploit.GLOBAL.ArrayLine.Acreage.setMap(null);
        }

        if (polylineTemp != null) {
            polylineTemp.setMap(null);
            polylineTemp = null;
        }

        exploit.GLOBAL.ArrayLine.ListPoint = null;
        exploit.GLOBAL.ArrayLine.Acreage = null;
        exploit.GLOBAL.ArrayLine.ListMeter = null;
        exploit.GLOBAL.ArrayLine.Polyline = null;
        exploit.GLOBAL.ArrayLine.Distacne = 0;
    },
    // Draw polygon
    createPointMutiplePolygon: function (lat, lng) {
        var isDraw = true;
        if (exploit.GLOBAL.ArrayPolygon.ListPoint != null && exploit.GLOBAL.ArrayPolygon.ListPoint.length > 0) {
            var locationLast = exploit.GLOBAL.ArrayPolygon.ListPoint[exploit.GLOBAL.ArrayPolygon.ListPoint.length - 1].getPosition();
            if (locationLast.lat == lat && locationLast.lng == lng) {
                isDraw = false;
            }
        }

        if (isDraw) {
            let markerDraw = new map4d.Marker({
                position: { lat: lat, lng: lng },
                icon: new map4d.Icon(8, 8, location.origin + "/common/EllipseBlue.svg"),
                anchor: [0.5, 0.5],
                draggable: true,
                zIndex: 2
                //title: name
            })
            markerDraw.setMap(map);

            if (exploit.GLOBAL.ArrayPolygon.ListPoint == null) {
                exploit.GLOBAL.ArrayPolygon.ListPoint = [];
            }

            exploit.GLOBAL.ArrayPolygon.ListPoint.push(markerDraw);
        }
    },
    createPolylineLoDat: function (path) {
        //tạo đối tượng polyline từ PolylineOptions
        var polylineDistance = new map4d.Polyline({
            path: path, visible: true, strokeColor: "#00559A", strokeWidth: 2.0, strokeOpacity: 2.0,
            closed: false,
            zIndex: 2
        })
        //thêm polyline vào map
        polylineDistance.setMap(map);
        if (exploit.GLOBAL.ArrayPolygon.Polyline != null) {
            exploit.GLOBAL.ArrayPolygon.Polyline.setMap(null);
        }

        exploit.GLOBAL.ArrayPolygon.Polyline = polylineDistance
    },
    DrawPolygon: function () {
        var iLatLng = [];
        var iLatLngDienTich = [];
        $.each(exploit.GLOBAL.ArrayPolygon.ListPoint, function () {
            let latLng = { lng: this.getPosition().lng, lat: this.getPosition().lat };

            iLatLng.push(latLng);

            iLatLngDienTich.push([this.getPosition().lng, this.getPosition().lat]);
        });

        // tính diện tích
        let measure = new map4d.Measure([])
        measure.setPath(
            iLatLngDienTich
        );

        exploit.GLOBAL.ArrayPolygon.Meter = measure;

        //drawing polygon
        iLatLng.push(iLatLng[0]);

        if (exploit.GLOBAL.ArrayPolygon.Polygon != null) {
            exploit.GLOBAL.ArrayPolygon.Polygon.setMap(null);
        }

        let polygonOption = map4d.PolygonOptions = {
            paths: [iLatLng],
            fillOpacity: 0.5,
            fillColor: "#F9F4E4",
            strokeWidth: 2.0,
            strokeColor: "#00559A",
            zIndex: 1
        }

        var polygon = new map4d.Polygon(polygonOption)
        //thêm object vào map
        polygon.setMap(map);

        exploit.GLOBAL.ArrayPolygon.Polygon = polygon;
    },
    setLatLngPolygon: function (paths, location, index) {
        let count = 0;
        for (var i = 0; i < paths.length; i++) {
            let check = count == 0 ? count + (paths[i].length - 2) : count + (paths[i].length - 1);
            if (check >= index) {
                let indexcurrent = count == 0 ? index - count : index - count - 1;
                if (indexcurrent == 0) {
                    paths[i][indexcurrent] = { lat: location.lat, lng: location.lng };
                    paths[i][paths[i].length - 1] = { lat: location.lat, lng: location.lng };
                } else {
                    paths[i][indexcurrent] = { lat: location.lat, lng: location.lng };
                }
            }
            count += (paths[i].length - 2);
        }
        return paths;
    },
    EditPoylineInPolygon: function (path) {
        if (exploit.GLOBAL.ArrayPolygon.ListPoint !== null && exploit.GLOBAL.ArrayPolygon.ListPoint.length > 0) {
            $.each(exploit.GLOBAL.ArrayPolygon.ListPoint, function (i, obj) {
                obj.setMap(null);
            });
            exploit.GLOBAL.ArrayPolygon.ListPoint = [];
        }

        if (exploit.GLOBAL.ArrayPolygon.Polyline !== null) {
            exploit.GLOBAL.ArrayPolygon.Polyline.setMap(null);
        }

        var ILatLng = [];

        $.each(path, function (index, obj) {
            let item = { lng: this.getPosition().lng, lat: this.getPosition().lat };
            exploit.createPointMutiplePolygon(this.getPosition().lat, this.getPosition().lng);
            ILatLng.push(item);
        });

        exploit.createPolylineLoDat(ILatLng);

        exploit.ShowPolyGonInfoMap();
    },
    ShowPolyGonInfoMap: function () {
        var regt = 0;
        if (exploit.GLOBAL.ArrayPolygon.Meter != null) {
            regt = parseFloat(exploit.GLOBAL.ArrayPolygon.Meter.area).toFixed(2);
        }
        var html = `<div style="display : flex;">
                        <span style="font-weight: bold; margin-right: 4px;">${l("Acreage")}:</span><span> ${regt}(㎡)</span>
                            </div>`;
        $(exploit.SELECTORS.body_infor_map).html(html);
        $(exploit.SELECTORS.content_infor_map).addClass('open');
    },
    RemovePolygon: function () {
        // xóa điểm
        if (exploit.GLOBAL.ArrayPolygon.ListPoint != null) {
            for (var i = 0; i < exploit.GLOBAL.ArrayPolygon.ListPoint.length; i++) {
                exploit.GLOBAL.ArrayPolygon.ListPoint[i].setMap(null);
            }
        }

        if (exploit.GLOBAL.ArrayPolygon.Polyline != null) {
            exploit.GLOBAL.ArrayPolygon.Polyline.setMap(null);
        }


        if (exploit.GLOBAL.ArrayPolygon.Polygon != null) {
            exploit.GLOBAL.ArrayPolygon.Polygon.setMap(null);
        }

        exploit.GLOBAL.ArrayPolygon.Acreage = 0;
        exploit.GLOBAL.ArrayPolygon.ListPoint = null;
        exploit.GLOBAL.ArrayPolygon.Meter = null;
        exploit.GLOBAL.ArrayPolygon.Polyline = null;
        exploit.GLOBAL.ArrayPolygon.Polygon = null;
    },
    RenderHTML: function (data, type) {
        html = '';
        var top = 40;
        if (type == "point") {
            top = 0;
        }
        if (data.code === "ok" && data.result !== null && data.result !== "null") {
            var text = '';
            var layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == data.result.idDirectory);
            if (layer != undefined) {

                var lstPropertiesLayer = layer.listProperties.filter(x => x.typeSystem != 1);
                $.each(lstPropertiesLayer, function () {
                    let property = this;

                    var obj = data.result.listProperties.find(x => x.codeProperties == property.codeProperties);
                    if (obj != undefined) {
                        if (obj.typeProperties == "string" && obj.codeProperties == "Name") {
                            text = `<div class="row   p-1">
                                             <span class="col-md-12" style="padding-right:12px; font-weight: bold; padding-left: 12px;">${obj.defalutValue !== null ? obj.defalutValue : ""}</span>
                                        </div>` + text;
                        }

                        if (property.isShowExploit) {
                            switch (obj.typeProperties) {
                                case "image":
                                    ThongTin.GLOBAL.ArrayImageMainObject = [];
                                    var value = '';
                                    if (obj.defalutValue !== null && obj.defalutValue !== '') {
                                        var lstImage = JSON.parse(obj.defalutValue);
                                        if (lstImage != null && $.isArray(lstImage)) {
                                            value = ``;
                                            if (lstImage.length > 0) {
                                                ThongTin.GLOBAL.ArrayImageMainObject = lstImage;

                                                value = `<img src="${lstImage[0].url}" 
                                                                    id="img-icon-2d" height="110" style="width: 40%;border: 5px solid  #EDEDED;" onerror="this.onerror = null; this.src = /images/anhMacDinh.png;">`;
                                            }
                                            text += `<div class="row  p-1">
                                                    <label class="col-md-6 text-bold">${obj.nameProperties}:</label>
                                                    <div class="col-md-6">${value}</div>
                                                 </div>`;
                                        }
                                    }
                                    break;
                                case "link":
                                    var value = '';
                                    if (obj.defalutValue !== null && obj.defalutValue !== '') {
                                        var lstLink = JSON.parse(obj.defalutValue);
                                        if (lstLink != null && $.isArray(lstLink)) {
                                            if (lstLink.length > 0) {
                                                value = `<a  href="${lstLink[0].url}" class="" style="overflow-wrap: anywhere;" target="_blank" title="${l('Main:OpenLink')}">${lstLink[0].url}</a>`;
                                            }

                                            text += `<div class="row   p-1">
                                                    <label class="col-md-6 text-bold">${obj.nameProperties}:</label>
                                                    <div class="col-md-6">${value}</div>
                                                 </div>`;
                                        }
                                    }
                                    break;
                                case "file":
                                    //ThongTin.GLOBAL.lstFileMainObject.push({ name: obj.nameProperties, code: obj.codeProperties });
                                    break;
                                case "text":
                                    text += `<div class="row   p-1">
                                             <label class="col-md-6 text-bold">${obj.nameProperties}:</label>
                                             <span class="col-md-6">${obj.defalutValue != null ? obj.defalutValue : ""}</span>
                                        </div>`;
                                    break;
                                case "stringlarge":
                                    text += `<div class="row   p-1">
                                            <label class="col-md-6 text-bold">${obj.nameProperties}:</label>
                                            <span class="col-md-6">${obj.defalutValue != null ? obj.defalutValue : ""}</span>
                                        </div>`;
                                    break;
                                case "list":
                                case "radiobutton":
                                case "checkbox":
                                    var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
                                    var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách
                                    var valuePropertyView = ""; // value hiển thị
                                    try {
                                        lstValueDefault = JSON.parse(property.defalutValue);
                                    }
                                    catch (e) { }
                                    if (lstValueDefault.length > 0) {
                                        if (obj.defalutValue != "" && obj.defalutValue != undefined && obj.defalutValue != null) {
                                            lstValueObjectMain = obj.defalutValue.split(',');
                                        }
                                        var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng
                                        valuePropertyView = lstMapValue.map(x => x.name).join(', ');
                                    }
                                    else {
                                        valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
                                    }

                                    text += `<div class= "row  p-1">
                                            <label class="col-md-6 text-bold">${obj.nameProperties}:</label>
                                            <span class="col-md-6">${valuePropertyView != null ? valuePropertyView : ""}</span>
                                        </div>`;
                                    break;
                                case "bool":
                                    text += `<div class= "row  p-1">
                                            <label class="col-md-6 text-bold">${obj.nameProperties}:</label>
                                            <span class="col-md-6">${obj.defalutValue != null ? (obj.defalutValue == "true" ? l("Exploit:True") : l("Exploit:False")) : ""}</span>
                                        </div>`;
                                    break;
                                default:
                                    if (obj.typeProperties != "file" && obj.codeProperties !== "Name") {
                                        text += `<div class= "row  p-1">
                                            <label class="col-md-6 text-bold">${obj.nameProperties}:</label>
                                            <span class="col-md-6">${obj.defalutValue != null ? obj.defalutValue : ""}</span>
                                        </div>`;
                                    }
                                    break;
                            }
                        }
                    }
                });
            }

            html = `<div style = "cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;" >
                                                    <div class="gm-style-iw-a" style="position: absolute;top: ${top}px;">
                                                         <div class="gm-style-iw-t hover-bottom-tip" style="right: 0px; bottom: 61px;">
                                                              <div class="gm-style-iw gm-style-iw-c hover-info" style="width: 212px;max-height: 350px;">
                                                                <div class="w-100" style="max-height: 315px;overflow-x: hidden;overflow-y: auto;">
                                                                    ${text}
                                                                </div>
                                                              </div>
                                                         </div>
                                                    </div>
                                             </div >`;
        }
        return html;
    },
    getAndShowInfo: function (id, type) {
        var html = '';
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: exploit.CONSTS.URL_AJAXGETOBJECTEXPLOIT + '?id=' + id,
            success: function (data) {
                html = exploit.RenderHTML(data, type);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

        return html;
    },
    getAndShowInfoPoly: function (id, position) {
        var htmlData = '';
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: exploit.CONSTS.URL_AJAXGETOBJECTEXPLOIT + '?id=' + id,
            success: function (data) {
                htmlData = exploit.RenderHTML(data, "rectag");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

        if (polylineTemp != null) {
            polylineTemp.setMap(null);
            polylineTemp = null;
        }
    },
    CloseDraw: function () {
        $(exploit.SELECTORS.content_infor_map).removeClass('open');
        $('.draw').removeClass('isForcus');
        exploit.GLOBAL.TypeDraw = -1;

        exploit.RemoveDrawPoint();
        exploit.RemoveLine();
        exploit.RemovePolygon();
    },
    //Tạo polyline tạm
    createPolylineByMouseMoveLoDat: function (path, strokeWidth, strokeOpacity) {
        if (polylineTemp != null) {
            polylineTemp.setMap(null);
        }
        //tạo đối tượng polyline từ PolylineOptions
        polylineTemp = new map4d.Polyline({
            path: path, visible: true, strokeColor: "#00559A", strokeWidth: strokeWidth, strokeOpacity: strokeOpacity,
            closed: false,
            zIndex: 2
        })
        //thêm polyline vào map
        polylineTemp.setMap(map)
    },

    CloseHoverToolBar: function () {
        if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(null);
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo = null;
            ThongTin.clearHighlight();
        }

        // ẩn toolip marker
        if (!map.is3dMode()) {
            $.each(exploit.GLOBAL.listMarker, function (i, item) {
                $.each(item.lstMarker, function (j, marker) {
                    marker.setMap(map);
                    marker.setVisible(true);
                    marker.setInfoWindow('');
                    marker.hideInfoWindow();

                })
            });
        }
    },
    CancelAction: function () {
        exploit.CloseDraw(); // xóa vẽ
        TimKiem.CloseContentTimKiem(); // đóng tìm kiếm
        ReferenceContent.CloseReferenceContent(); // đóng tham chiếu
        $('.toggle-detail-property2').trigger('click'); // đóng content xem thông tin 1 đối tượng

        exploit.CloseHoverToolBar(); // tắt hover
        exploit.GLOBAL.ActionToolbar.XemThongTinHover = false; // đóng chức năng hover

        ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
        ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ

        exploit.ResetIcon(); //reset icon thanh menu

        if ($('.menuMap').find('.disabled').length > 0 && $(`${TimKiem.SELECTORS.check_tree}.check`).length > 0) {
            if (!$(exploit.SELECTORS.content_lst_object).hasClass('open')) // mở danh sách đối tượng
            {
                $(exploit.SELECTORS.content_lst_object).addClass('open')
            }
        }

        $('.draw').prop('disabled', false); // mở lại chức năng trên menu
        $('.draw').removeClass('disabled');
        isAction = false;
    },

    // ẩn hoặc hiện những đối tượng không thuộc quản lý trên map
    HideOrShowBuildingMap: function () {
        map.enable3dMode(true);
        var array = (exploit.GLOBAL.listObjectInGeometry.length > 0) ? exploit.GLOBAL.listObjectInGeometry : exploit.getListObjectInGeometry();
        var arrayAll = array.concat(exploit.GLOBAL.lstArrayQTSC);

        if (!$(exploit.SELECTORS.btn_show_building_out).hasClass('open')) {
            map.setBuildingsEnabled(true);
            $(exploit.SELECTORS.btn_show_building_out).addClass('open');
            var html = `<svg id="Group_1358" data-name="Group 1358" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path id="Path_459" data-name="Path 459" d="M0,0H24V24H0Z" fill="none" />
                                <path id="Path_460" data-name="Path 460" d="M4.52,5.934,1.393,2.808,2.808,1.393l19.8,19.8-1.415,1.414-3.31-3.31A11,11,0,0,1,1.181,12a10.982,10.982,0,0,1,3.34-6.066ZM14.757,16.172l-1.464-1.464a3,3,0,0,1-4-4L7.828,9.243a5,5,0,0,0,6.929,6.929ZM7.974,3.76A11.01,11.01,0,0,1,22.819,12a10.947,10.947,0,0,1-2.012,4.592l-3.86-3.86a5,5,0,0,0-5.68-5.68L7.974,3.761Z" fill="rgba(0,0,0,0.38)" />
                            </svg>`;
            $(exploit.SELECTORS.btn_show_building_out).html(html);

            //map.setBuildingsEnabled(true); // trạng thái hiển thị building của bản đồ
            map.setVisibleBuildings(arrayAll); // hiển thị những building truyền vào
            map.setHiddenBuildings([]); // set lại bulding ẩn = rỗng

            $(exploit.SELECTORS.btn_show_building_out).attr('data-original-title', l("Main:HideNoneManagedObject"));
            timemap.checkBtnTimeChange(true);
        }
        else {
            var dem = $('.check').length;
            if (dem == 0) {
                map.setBuildingsEnabled(false);
            }
            else {
                if (exploit.GLOBAL.lstArrayQTSC.length == 0) {
                    map.setBuildingsEnabled(false);
                }
                else {
                    map.setBuildingsEnabled(true);
                    //map.setBuildingsEnabled(false); // trạng thái hiển thị building của bản đồ
                    map.setVisibleBuildings(exploit.GLOBAL.lstArrayQTSC); // set lại bulding hiển thị = rỗng
                    map.setHiddenBuildings(array); // ẩn những building truyền vào
                }
            }

            $(exploit.SELECTORS.btn_show_building_out).removeClass('open');

            var html = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <defs>
                                </defs>
                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z" />
                                <path class="b" style="fill: rgba(0,0,0,0.56);" d="M1.181,12a11,11,0,0,1,21.638,0A11,11,0,0,1,1.181,12ZM12,17a5,5,0,1,0-5-5A5,5,0,0,0,12,17Zm0-2a3,3,0,1,1,3-3A3,3,0,0,1,12,15Z" />
                            </svg>`;
            $(exploit.SELECTORS.btn_show_building_out).html(html);
            $(exploit.SELECTORS.btn_show_building_out).attr('data-original-title', l("Main:DisplayObjectAreNotManagedByTheOBject"));
            timemap.checkBtnTimeChange(false);
        }

        let objchosen = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
        if (objchosen != null && objchosen != undefined) {
            if (objchosen.idObject != null && objchosen.idObject != "") {
                map.setSelectedBuildings([objchosen.idObject])
            } else {
                objchosen.object.setSelected(true);
            }
        }
    },
    //show hover datalayer and maptile
    ShowInforHover: function (id, lat, lng) {
        if ((exploit.GLOBAL.ActionToolbar.MarkerShowInfo !== null && exploit.GLOBAL.ActionToolbar.MarkerShowInfo.getUserData() !== id) || exploit.GLOBAL.ActionToolbar.MarkerShowInfo === null) {
            if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
                exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(null);
            }
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo = new map4d.Marker({
                position: { lat: lat, lng: lng },
                icon: new map4d.Icon(0.5, 0.5, "/common/point.png")
            });

            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setUserData(id);

            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(map);
            //exploit.getAndShowInfo(id);

            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.hideInfoWindow();
            var html = exploit.getAndShowInfo(id, "rectag");
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setInfoWindow(html);
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.showInfoWindow();
        }
        else {
            if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
                exploit.GLOBAL.ActionToolbar.MarkerShowInfo.showInfoWindow();
            }
        }
    },
    //#region -------Add new maptile geoserver----------
    getUrlTile: function (data) {
        var urlTile = data.result.groundMaptile;

        var Arrayexp = urlTile.trim().split('.');
        exp = Arrayexp[Arrayexp.length - 1];

        if (exp == "" || exp != "png" && exp != "jpg") {
            exp = "png";
        }

        urlTile = urlTile.trim().split('/$')[0];
        urlTile = urlTile.trim().split('/{')[0];
        return urlTile;
    },
    getInforShapeMapTile: function (args, check = false) {
        let listUrl = this.getListUrlMaptile(args, exploit.GLOBAL.listGroundOvelay);
        this.getInforServerMapTile(listUrl, check);
    },
    //get list url
    getListUrlMaptile: function (args, list) {
        let box = map.getBounds();
        let pixel = args.pixel;
        let x = Math.round(pixel.x);
        let y = Math.round(pixel.y);
        let w = Math.round(Number($("canvas").width()));
        let h = Math.round(Number($("canvas").height()));
        let listUrl = [];
        for (var i = list.length; i > 0; i--) {
            var obj = list[i - 1];
            if ((obj.optionDirectory === 2 || obj.optionDirectory === 3) && obj.url !== null && obj.url.length > 0 && obj.url.indexOf("4326") >= 0) {
                var domain = "";
                var layer = "";
                var url = "";
                var bbox = null;
                bbox = box.southwest.lng + "," + box.southwest.lat + "," + box.northeast.lng + "," + box.northeast.lat;
                var endcodeurl = new URL(obj.url);
                domain = endcodeurl.origin + endcodeurl.pathname;
                layer = endcodeurl.searchParams.get("LAYERS");
                url = `${domain}?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&FORMAT=image/png&TRANSPARENT=true&QUERY_LAYERS=${layer}&LAYERS=${layer}&exceptions=application/vnd.ogc.se_inimage&INFO_FORMAT=application/json&FEATURE_COUNT=50&X=${x}&Y=${y}&SRS=EPSG:4326&STYLES=&WIDTH=${w}&HEIGHT=${h}&BBOX=${bbox}`;
                let param = { option: obj.optionDirectory, url: url };
                listUrl.push(param);
            }
            else if (obj.optionDirectory === 3 && this.checkSRSOnUrlVN2000(obj.url).length > 0) {
                var bboxVN20001 = ConvertLocationMap.WGS84toVN2000(box.southwest.lat, box.southwest.lng, this.CONSTS.KTTruc, this.CONSTS.Zone);
                var bboxVN20002 = ConvertLocationMap.WGS84toVN2000(box.northeast.lat, box.northeast.lng, this.CONSTS.KTTruc, this.CONSTS.Zone);
                var bbox = null;
                if (typeof bboxVN20001 !== "undefined" && typeof bboxVN20002 !== "undefined") {
                    bbox = bboxVN20001[1] + "," + bboxVN20001[0] + "," + bboxVN20002[1] + "," + bboxVN20002[0];
                    var domain = "";
                    var layer = "";
                    var url = "";
                    if (obj.url !== null) {
                        var endcodeurl = new URL(obj.url);
                        domain = endcodeurl.origin + endcodeurl.pathname;
                        layer = endcodeurl.searchParams.get("LAYERS");
                        SRS = this.checkSRSOnUrlVN2000(obj.url);
                        url = `${domain}?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&FORMAT=image/png&TRANSPARENT=true&QUERY_LAYERS=${layer}&LAYERS=${layer}&exceptions=application/vnd.ogc.se_inimage&INFO_FORMAT=application/json&FEATURE_COUNT=50&X=${x}&Y=${y}&SRS=EPSG:${SRS}&STYLES=&WIDTH=${w}&HEIGHT=${h}&BBOX=${bbox}`
                        let param = { option: obj.optionDirectory, url: url };
                        listUrl.push(param);
                    }
                }
            }
        }
        return listUrl;
    },
    getInforServerMapTile: function (param, check) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: "/api/LayerToLayer/LayerGeo/getInfor",
            data: JSON.stringify(param),
            success: function (res) {
                if (res.code == "ok") {
                    if (res.message === "2") {
                        if (!check) {
                            exploit.showInforData(res);
                            ThongTin.setHighLightClickList("", "");
                            ThongTin.showHideViewProperty(true);
                        } else exploit.GLOBAL.hoverObjectMaptile = res.result;

                    } else if (res.message == "3") {
                        let resjson = JSON.parse(res.result);
                        ThongTin.GLOBAL.mainObjectSelected = resjson.features[0];
                        if (ReferenceContent.checkVn2000Geojson(resjson.features[0])) {
                            let object = ReferenceContent.ConvertShapeVn2000ToWgs84(resjson.features[0]);
                            ThongTin.GLOBAL.mainObjectSelected = object;
                        }
                        ThongTin.setHighLightClickList("", "");
                        exploit.GetInforMapTile(resjson.features[0].properties);
                        ThongTin.showHideViewProperty(true);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    showInforData: function (data) {
        $(ThongTin.SELECTORS.Item_Menu_Right).first().trigger('click');
        ThongTin.GLOBAL.mainObjectSelected = data.result;
        $('.Title-info').html('');
        $('.Content-info').html('');
        $('.infor-detail').html('');

        //$(documentObject.SELECTORS.listFileUpload).html('');

        var title = ThongTin.GLOBAL.mainObjectSelected.listProperties.find(x => x.codeProperties === "Title" && x.typeSystem === 2);
        if (typeof title !== "undefined") {
            $('.Title-info').html(title.defalutValue);
        }
        var content = ThongTin.GLOBAL.mainObjectSelected.listProperties.find(x => x.codeProperties === "Description" && x.typeSystem === 2);
        if (typeof content !== "undefined") {
            $('.Content-info').html(content.defalutValue);
        }
        var lst = TreeViewLayer.GetPropertiesDirectory(ThongTin.GLOBAL.mainObjectSelected.idDirectory);
        if (lst != null) {
            lst = lst.listProperties.filter(x => (x.codeProperties !== "Title") && (x.codeProperties !== "Description"));
        }
        else {
            lst = ThongTin.GLOBAL.mainObjectSelected.listProperties.filter(x => x.typeSystem != 1 && (x.codeProperties !== "Title") && (x.codeProperties !== "Description"));
        }
        //let demlist = 1;
        let statusfile = false;
        ThongTin.GLOBAL.lstFileMainObject = [];
        $.each(lst, function (i, obj) {

            if (obj.typeSystem != 1) {
                var property = ThongTin.GLOBAL.mainObjectSelected.listProperties.find(x => x.codeProperties == obj.codeProperties);
                if (property != undefined) {
                    /* obj.defalutValue = property.defalutValue;*/
                    if ((obj.typeProperties == "list" || obj.typeProperties == "radiobutton" || obj.typeProperties == "checkbox") && obj.isShow) {
                        var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
                        var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách
                        var valuePropertyView = ""; // value hiển thị
                        try {
                            lstValueDefault = JSON.parse(obj.defalutValue);
                        }
                        catch (e) { }
                        if (lstValueDefault.length > 0) {
                            if (property.defalutValue != "" && property.defalutValue != undefined && property.defalutValue != null) {
                                lstValueObjectMain = property.defalutValue.split(',');
                            }
                            var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng
                            valuePropertyView = lstMapValue.map(x => x.name).join(', ');
                        }
                        else {
                            valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
                        }
                        let text = `<div class="form-group col-md-12 form-common-infor">
                                                <div class="row">
                                                    <label class="col-md-4">${obj.nameProperties}:</label>
                                                    <span class="col-md-8" style = "text-align:right;"><span class="Content-info">${valuePropertyView}</span></span>
                                                </div>
                                            </div>`;
                        $('.infor-detail').append(text);
                        return;
                    }
                    else {
                        obj.defalutValue = property.defalutValue;
                    }

                    if (obj.isShow) {
                        if (obj.typeProperties != "geojson") {
                            var text = '';
                            switch (obj.typeProperties) {
                                case "image":
                                    ThongTin.GLOBAL.ArrayImageMainObject = [];
                                    var value = '';
                                    if (obj.defalutValue !== null && obj.defalutValue !== '') {
                                        var lstImage = JSON.parse(obj.defalutValue);
                                        if (lstImage != null && $.isArray(lstImage)) {
                                            if (lstImage.length > 0) {
                                                ThongTin.GLOBAL.ArrayImageMainObject = lstImage;

                                                value = `<img src="${lstImage[0].url}" 
                                                                    id="img-icon-2d" height="100" style="width: 70%;border: 5px solid  #EDEDED;" onerror="this.onerror = null; this.src = /images/anhMacDinh.png;">
                                                                <a class="btn btn-xs btn-primary btn-view-image" title="${l('Main:SeeAllPicture')}" style="float:right;"><i class="fa fa-search"></i></a>`;
                                            }

                                            text = `<div class="form-group col-md-12 form-common-infor">
                                                        <div class="row">
                                                            <label class="col-md-4">${obj.nameProperties}:</label>
                                                            <span class="col-md-8"><span class="Content-info">${value}</span></span>
                                                        </div>
                                                    </div>`;
                                        }
                                    }
                                    break;
                                case "link":
                                    var value = '';
                                    if (obj.defalutValue !== null && obj.defalutValue !== '') {
                                        var lstLink = JSON.parse(obj.defalutValue);
                                        if (lstLink != null && $.isArray(lstLink)) {
                                            if (lstLink.length > 0) {
                                                value = `<a href="${lstLink[0].url}" class="" style="overflow-wrap: anywhere;" target="_blank" title="${l('Main:OpenLink')}">${lstLink[0].url}</a>`;
                                            }

                                            text = `<div class="form-group col-md-12 form-common-infor">
                                                    <div class="row">
                                                        <label class="col-md-4">${obj.nameProperties}:</label>
                                                        <span class="col-md-8" style = "text-align:right;"><span class="Content-info">${value}</span></span>
                                                    </div>
                                                </div>`;
                                        }
                                    }
                                    break;
                                case "file":
                                    ThongTin.GLOBAL.lstFileMainObject.push({ name: obj.nameProperties, code: obj.codeProperties });
                                    break;
                                case "text":
                                    text += `<div class="form-group col-md-12 form-common-infor">
                                                            <div class="row">
                                                                <label class="col-md-4">${obj.nameProperties}:</label>
                                                                <span class="col-md-12" style="padding:0;"><span class="Content-info">${obj.defalutValue != null ? obj.defalutValue : ""}</span></span>
                                                            </div>
                                                        </div>`;
                                    break;
                                case "stringlarge":
                                    text += `<div class="form-group col-md-12 form-common-infor">
                                                            <div class="row">
                                                                <label class="col-md-4">${obj.nameProperties}:</label>
                                                                <span class="col-md-12" style="padding:0;"><span class="Content-info">${obj.defalutValue != null ? obj.defalutValue : ""}</span></span>
                                                            </div>
                                                        </div>`;
                                    break;
                                default:
                                    text = `<div class="form-group col-md-12 form-common-infor">
                                                            <div class="row">
                                                                <label class="col-md-4">${obj.nameProperties}:</label>
                                                                <span class="col-md-8" style = "text-align:right;"><span class="Content-info">${obj.defalutValue != null ? obj.defalutValue : ""}</span></span>
                                                            </div>
                                                        </div>`;
                                    break;
                            }

                            $('.infor-detail').append(text);
                        }
                    }
                }
                else {
                    let text = `<div class="form-group col-md-12 form-common-infor">
                                                <div class="row">
                                                    <label class="col-md-4">${obj.nameProperties}:</label>
                                                    <span class="col-md-8" style = "text-align:right;"><span class="Content-info"></span></span>
                                                </div>
                                            </div>`;
                    $('.infor-detail').append(text);
                }
            }
        });

        if (statusfile) {
            documentObject.GetFileByObjectMainId(ThongTin.GLOBAL.idMainObject, documentObject.showFileEdit);
        }
    },
    GetInforMapTile: function (data) {
        $(ThongTin.SELECTORS.Item_Menu_Right).first().trigger('click');
        $('.Title-info').html('');
        $('.Content-info').html('');
        $('.infor-detail').html('');
        let statusfile = false;
        if (data !== null) {
            for (const property in data) {
                let val = data[property] !== null ? data[property] : "";
                //let name = property;
                if (property !== "path") {
                    let text = `<div class="form-group col-md-12 form-common-infor">
                                    <div class="row">
                                        <label class="col-md-4">${property}:</label>
                                        <span class="col-md-8" style = "text-align:right;"><span class="Content-info">${val}</span></span>
                                    </div>
                                </div>`;
                    $('.infor-detail').append(text);
                }
            }
        }
    },
    //#endregion

    //#region -------Optimal load data----------
    getCountLayerById: function (id) {
        let totalcount = 0;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: this.CONSTS.URL_GETTOTALOBJECTBYID + "?id=" + id,
            //data: {},
            success: function (data) {
                if (data.code == "ok") {
                    totalcount = data.result;
                    //console.log(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
        return totalcount;
    },
    showOjectLayerByPageAndId: function (id, page = 1) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: true,
            url: this.CONSTS.URL_GETOBJECTPAGINGBYDIRECTORY + "?id=" + id + "&page=" + page,
            success: function (data) {
                if (data.code == "ok") {
                    data.result = JSON.parse(data.result);
                    if (data.result != null && data.result.length > 0) {
                        if (data.result != null && data.result.length > 0) {
                            let directory = {
                                id: id,
                                listMainObject: []
                            };
                            let object = JSON.parse(JSON.stringify(data.result.filter(x => x.geometry.type != "Point" && x.geometry.type != "MultiPoint")));
                            var geojson = exploit.getDataGeojsonByProperties(object, directory.listMainObject, id);
                            exploit.showDataGeojson2(JSON.stringify(geojson), id);
                            var lstmarker = JSON.parse(JSON.stringify(data.result.filter(x => x.geometry.type == "Point" || x.geometry.type == "MultiPoint")));
                            exploit.showListMarkerObject(lstmarker, id, directory.listMainObject);
                            exploit.setListDirectoryMainObject(id, directory);
                            let object3d = data.result.filter(x => x.object3D != null);
                            $.each(object3d, function (i, obj) {

                                if (obj.object3D.type != "para-choosenMap") {
                                    let object3d = exploit.newMap4dBuilding(obj.object3D);
                                    object3d.setMap(map);
                                    object3d.setUserData(obj);
                                    exploit.GLOBAL.listObject3D.push({ id: obj.id, idLayer: id, object: object3d, idObject: '' });
                                } else {
                                    if (obj.object3D.id != "") {
                                        exploit.GLOBAL.lstArrayQTSC.push(obj.object3D.id);
                                    }
                                    exploit.GLOBAL.listObject3D.push({ id: obj.id, idLayer: id, object: null, idObject: obj.object3D.id });
                                }
                            });
                            exploit.showObjectMap();

                            // thêm danh sách đối tượng vào dữ liệu tìm kiếm
                            TimKiem.AddMainObjectLayer(data.result, id);
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    setListDirectoryMainObject: function (id, objDirectory) {
        var obj = exploit.GLOBAL.listDirectoryMainObject.find(x => x.id == id);
        if (obj !== null && typeof obj !== "undefined") {
            obj.listMainObject = obj.listMainObject.concat(objDirectory.listMainObject);
        } else exploit.GLOBAL.listDirectoryMainObject.push(objDirectory);

    },
    setListMarkerMainObject: function (id, objMarker) {
        var obj = exploit.GLOBAL.listMarker.find(x => x.id == id);
        if (obj !== null && typeof obj !== "undefined") {
            obj.lstMarker = obj.lstMarker.concat(objMarker.lstMarker);
        } else exploit.GLOBAL.listMarker.push(objMarker);
    },
    //#endregion

    // clear marker hover info
    ClearHoverInfo: function () {
        if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.hideInfoWindow();
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(null);
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo = null;
        }
    },
    checkSRSOnUrlVN2000: function (url) {
        let check = "";
        $.each(this.CONSTS.ListSRS, function (i, obj) {
            if (url.indexOf(obj.toString()) > -1) {
                check = obj.toString();
                return true;
            }
        });
        return check;
    },
    // ẩn marker hover info
    hideorShowHoverInfo: function (type) {
        var ismap = null;
        if (type) {
            ismap = map;
        }
        if (exploit.GLOBAL.ActionToolbar.MarkerShowInfo != null) {
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.setMap(ismap);
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.hideInfoWindow();
            exploit.GLOBAL.ActionToolbar.MarkerShowInfo.showInfoWindow();
        }
    },
    // hiển thị lại marker 2d
    resetListMarker: function () {
        if (!map.is3dMode()) {
            $.each(exploit.GLOBAL.listMarker, function (i, item) {
                $.each(item.lstMarker, function (j, marker) {
                    marker.setMap(map);
                    marker.setVisible(true);
                    marker.hideInfoWindow();

                })
            });
        }
    },
    // reset highlight doi tuong 2d khi chuyển qua lại chế độ 2d và 3d
    resetHighlight2DModeTo3D: function () {
        var mod = map.is3dMode();
        if (!mod) {
            // highlight đối tượng 2d
            if (ThongTin.GLOBAL.idDirectory != "") {
                // higlight đối tượng 2d dạng point marker
                var layer = exploit.GLOBAL.listMarker.find(x => x.id == ThongTin.GLOBAL.idDirectory);
                if (layer != undefined) {
                    if (layer.lstMarker != undefined && layer.lstMarker.length > 0) {
                        var point = layer.lstMarker.filter(x => x.userData.id == ThongTin.GLOBAL.idMainObject);
                        if (point != undefined) {
                            if (point.length == 1) {

                                ThongTin.highlightObjectPoint(point[0]);
                            }
                            else {
                                ThongTin.hightlightObjectMutiplePoint(point);
                            }

                            ThongTin.GLOBAL.MarkerFocus = point;
                        }
                    }
                }

                // highlight đối tượng 2d dạng geojson (đa giác, line, ...)

                var layerFeature = exploit.GLOBAL.ArrayFeaturesByDirectory.find(x => x.id == ThongTin.GLOBAL.idDirectory);
                if (layerFeature != undefined && (layerFeature.features != null && layerFeature.features != undefined)) {
                    var feature = layerFeature.features.find(x => x._id == ThongTin.GLOBAL.idMainObject);
                    if (feature != undefined) {
                        ThongTin.highlightObject({ feature: feature }); // higlight đối tượng
                    }
                }
            }
        }
    },

    ResetIcon: function () {
        $(exploit.SELECTORS.menu_hori + " .draw").each(function () {
            var type = $(this).attr("data-type");
            switch (type) {
                case "0":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                    </defs>
                                    <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                    <path class="b" style="fill: rgba(0,0,0,0.56);" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-2a8,8,0,1,0-8-8A8,8,0,0,0,12,20ZM7,11.5,16,8l-3.5,9L11,13Z" />
                                </svg>`);
                    break;
                case "1":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                    </defs>
                                    <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                    <path class="b" style="fill: rgba(0,0,0,0.56);" d="M5,8V20H9V8ZM3,7,7,2l4,5V22H3Zm16,9V14H16V12h3V10H17V8h2V6H15V20h4V18H17V16ZM14,4h6a1,1,0,0,1,1,1V21a1,1,0,0,1-1,1H14a1,1,0,0,1-1-1V5A1,1,0,0,1,14,4Z" />
                                </svg>`);
                    break;
                case "2":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                    </defs>
                                    <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                    <path class="b" style="fill: rgba(0,0,0,0.56);" d="M20,16h2v6H16V20H8v2H2V16H4V8H2V2H8V4h8V2h6V8H20Zm-2,0V8H16V6H8V8H6v8H8v2h8V16ZM4,4V6H6V4ZM4,18v2H6V18ZM18,4V6h2V4Zm0,14v2h2V18Z" />
                                </svg>`);
                    break;
                case "3":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                    </defs>
                                    <path class="a" style="fill: none;" d="M0,0H24V24H0Z" />
                                    <path class="b" style="fill: rgba(0,0,0,0.56);" d="M16,13l6.964,4.062-2.973.85,2.125,3.681-1.732,1-2.125-3.68-2.223,2.15ZM14,6h2V8h5a1,1,0,0,1,1,1v4H20V10H10V20h4v2H9a1,1,0,0,1-1-1V16H6V14H8V9A1,1,0,0,1,9,8h5ZM4,14v2H2V14Zm0-4v2H2V10ZM4,6V8H2V6ZM4,2V4H2V2ZM8,2V4H6V2Zm4,0V4H10V2Zm4,0V4H14V2Z" />
                                </svg>`);
                    break;
                case "4":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <defs>
                                    </defs>
                                    <path class="a" d="M0,0H24V24H0Z" style="fill:none;" />
                                    <path class="b" style="fill: rgba(0,0,0,0.56);" d="M11,2a9,9,0,1,1-9,9A9,9,0,0,1,11,2Zm0,16a7,7,0,1,0-7-7A7,7,0,0,0,11,18Zm8.485.071L22.314,20.9,20.9,22.314l-2.828-2.829,1.414-1.414Z" />
                                </svg>`);
                    break;
                case "5":
                    $(this).html(`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                    <path id="Path_6787" data-name="Path 6787" d="M7,7V3A1,1,0,0,1,8,2H21a1,1,0,0,1,1,1V16a1,1,0,0,1-1,1H17v3.993A1.006,1.006,0,0,1,15.993,22H3.007A1.006,1.006,0,0,1,2,20.993L2,8.007A1.006,1.006,0,0,1,3.01,7ZM9,7h6.993A1.006,1.006,0,0,1,17,8.007V15h3V4H9ZM4,9,4,20H15V9Z" transform="translate(-2 -2)" fill="rgba(0,0,0,0.56)" />
                                </svg>`);
                    break;
                default:
            }
        })
    },

    //chọn trạng thái vẽ trên bản đồ
    ChoosenTypeDraw: function (args) {
        let iLatLng = [];
        switch (exploit.GLOBAL.TypeDraw) {
            case 0:
                exploit.DrawPoint(args.location.lat, args.location.lng);
                break;
            case 1:
                if (exploit.GLOBAL.IsResetDraw) {
                    exploit.RemoveLine();
                    exploit.ShowPolylineInfoMap();
                    exploit.GLOBAL.IsResetDraw = false;
                }
                exploit.createPointMutipleLine(args.location.lat, args.location.lng, 1);
                let listMarkerMulti = exploit.GLOBAL.ArrayLine.ListPoint;

                $.each(listMarkerMulti, function (i, obj) {
                    let latLng = { lat: obj.getPosition().lat, lng: obj.getPosition().lng };
                    iLatLng.push(latLng);
                });
                if (iLatLng.length > 1) {
                    let startPoint = [iLatLng[iLatLng.length - 2].lng, iLatLng[iLatLng.length - 2].lat];
                    let endPoint = [iLatLng[iLatLng.length - 1].lng, iLatLng[iLatLng.length - 1].lat];
                    exploit.ShowMeterDrawLine(startPoint, endPoint, false);
                    exploit.ShowPolylineInfoMap();
                }
                exploit.DrawLine();
                break;
            case 2:
                if (exploit.GLOBAL.IsResetDraw) {
                    exploit.RemovePolygon();
                    exploit.ShowPolyGonInfoMap();
                    exploit.GLOBAL.IsResetDraw = false;
                }

                exploit.createPointMutiplePolygon(args.location.lat, args.location.lng);
                if (exploit.GLOBAL.ArrayPolygon.ListPoint.length == 2) {
                    let listarea = exploit.GLOBAL.ArrayPolygon.ListPoint;
                    $.each(listarea, function () {
                        let item = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                        iLatLng.push(item);
                    })

                    exploit.createPolylineLoDat(iLatLng);
                }
                else if (exploit.GLOBAL.ArrayPolygon.ListPoint.length > 2) {
                    exploit.DrawPolygon();
                    if (exploit.GLOBAL.ArrayPolygon.Polyline != null) {
                        exploit.GLOBAL.ArrayPolygon.Polyline.setMap(null);
                        exploit.GLOBAL.ArrayPolygon.Polyline = null;
                    }

                    exploit.ShowPolyGonInfoMap();
                }
                break;
            default:
        }
    },
    // tọa độ bản đồ current
    GetCurrentLocation: function () {
        $.ajax({
            type: "GET",
            url: exploit.CONSTS.GET_LOCATION_CENTER,
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code == "ok") {
                    var cameraPosition = map.getCamera();
                    cameraPosition.target = {
                        lat: data.result.latitude,
                        lng: data.result.longitude
                    };

                    map.moveCamera(cameraPosition, null);
                    exploit.GLOBAL.centerMap.lat = data.result.latitude;
                    exploit.GLOBAL.centerMap.lng = data.result.longitude;
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
};