﻿var polylineEsri, geometryEngineEsri, polygonEsri;
var ReferenceContent = {
    GLOBAL: {},
    CONSTS: {
        KTTruc: kTTruc,
        Zone: zone,
        ListSRS: sRSEPSG,
        isCheckReference: false,
        countNo: 0,
        totalIn: 0,
        URL_AJAXGETINFOR: '/api/LayerToLayer/LayerGeo/getInfor',
        URL_AJAXGETInforMapTile: '/api/LayerToLayer/LayerGeo/searchBboxInfor',
    },

    SELECTORS: {
        content_reference: "#reference-content",
        select_reference_layer: "#select-reference-layer",
        select_referenced_multilayer: "#select-referenced-multilayer",
        btn_reference_layer: "#reference-content #btn-reference-layer",
        btn_reference_cancel: "#reference-content #btn-reference-cancel",
        result_reference_content: ".result-reference-content",
        btn_close_result_reference_content: ".close-result_reference_content",
        infor_result_reference: ".infor-result-reference",
        list_result_referenced: ".list-result-referenced",

        btn_close_content_refercenced: "#btn-close-reference-layer"

    },
    init: function () {
        this.setEsri();
        //add select 2
        $(ReferenceContent.SELECTORS.select_reference_layer).select2({
            placeholder: l("ChossenLayer"),
            allowClear: true,
            width: '100%',
            matcher: matchCustomBasic,
        });
        $(ReferenceContent.SELECTORS.select_referenced_multilayer).select2({
            placeholder: l("ChossenLayer"),
            allowClear: true,
            width: '100%',
            //matcher: matcher
        });
        //add event
        this.setEvent();

    },
    setEvent: function () {
        //event combobox tham chiếu
        $(ReferenceContent.SELECTORS.select_reference_layer).on("change", function () {
            let value = $(this).val();
            if (value !== null && typeof value !== "undefined" && value !== "") {
                $(ReferenceContent.SELECTORS.select_referenced_multilayer).find("option").removeAttr("disabled");
                $(ReferenceContent.SELECTORS.select_referenced_multilayer).find("option[value='" + value + "']").attr("disabled", "disabled");
                ReferenceContent.moveLayerReference(value);
            } else
                $(ReferenceContent.SELECTORS.select_referenced_multilayer).find("option").removeAttr("disabled");
        });
        $(ReferenceContent.SELECTORS.select_referenced_multilayer).on("change", function () {
            let value = $(this).val();
            if (value !== null && typeof value !== "undefined" && value.length > 0) {
                $.each(value, function (i, obj) {
                    $(ReferenceContent.SELECTORS.select_reference_layer).find("option[value='" + obj + "']").attr("disabled", "disabled");
                });
            } else
                $(ReferenceContent.SELECTORS.select_reference_layer).find("option").removeAttr("disabled");
        });
        //event nút tham chiếu
        $(ReferenceContent.SELECTORS.btn_reference_layer).on("click", function () {
            if (ReferenceContent.checkFormSelect()) {
                ReferenceContent.CONSTS.isCheckReference = true;

                $(ThongTin.SELECTORS.inforData).find('.toggle-detail-property2').click(); // dong xem info 1 doi tuong

            }
        });
        $(ReferenceContent.SELECTORS.btn_close_result_reference_content).on('click', function () {
            //ReferenceContent.CloseReferenceContent();
            ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
            ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ
            $(ReferenceContent.SELECTORS.result_reference_content).removeClass('open');
        });

        // đóng content tham chiếu
        $(ReferenceContent.SELECTORS.btn_close_content_refercenced).on("click", function () {
            exploit.CancelAction();
        });
    },
    setEsri: function () {
        require([
            //"esri/Map", "esri/views/MapView",
            "esri/geometry/geometryEngine",
            "esri/geometry/Polyline",
            "esri/geometry/Polygon",
        ], function (
            // Map,
            //MapView,
            geometryEngine,
            Polyline,
            Polygon,
        ) {
            polygonEsri = Polygon;
            polylineEsri = Polyline;
            geometryEngineEsri = geometryEngine;
        });

    },
    CloseReferenceContent: function () {
        $(ReferenceContent.SELECTORS.content_reference).removeClass('open');
        $(ReferenceContent.SELECTORS.result_reference_content).removeClass('open');
        ReferenceContent.CONSTS.isCheckReference = false;
    },
    OpenContentReference: function () {
        ReferenceContent.clearLabelError(); // remove tất cả label error
        if (!$(ReferenceContent.SELECTORS.content_reference).hasClass('open')) {
            ReferenceContent.CONSTS.isCheckReference = false;
            $(ReferenceContent.SELECTORS.content_reference).addClass('open');
            var list = this.getListselectReference();
            if (list.length > 0) {
                var html = this.renderOptionSelect(list);
                $(ReferenceContent.SELECTORS.select_reference_layer).html(html);
                $(ReferenceContent.SELECTORS.select_referenced_multilayer).html(html);
            } else {
                $(ReferenceContent.SELECTORS.select_reference_layer).html("");
                $(ReferenceContent.SELECTORS.select_referenced_multilayer).html("");
            }
            setTimeout(function () {
                $(ReferenceContent.SELECTORS.select_reference_layer).val(null).trigger('change');
                $(ReferenceContent.SELECTORS.select_referenced_multilayer).val(null).trigger('change');
            }, 1000);
        }
    },
    // select danh sách thư mục
    getListselectReference: function () {
        var arrayId = []; // danh sách layer dc checked
        $.each($(`${TimKiem.SELECTORS.check_tree}.check`), function () {
            var idDirectory = $(this).attr('data-id');
            //arrayId.push(idDirectory);
            var layerkey = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == idDirectory && x.type == "file");
            var layerparent = ReferenceContent.getParentLayer(layerkey.parentId, layerkey.id);
            var obj = arrayId.find(x => x.parentId == layerparent.id);
            if (typeof obj != "undefined") {
                obj.listChild.push(layerkey);
            } else {
                var item = {
                    parentId: layerparent.id,
                    parentObj: layerparent,
                    listChild: [layerkey]
                }
                arrayId.push(item);
            }
        });
        return arrayId;
    },
    getParentLayer: function (parentId, currentId) {
        var parent = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == parentId);
        if (typeof parent != "undefined" && parent.level > 1) {
            this.getParentLayer(parent.parentId, parent.id);
        } else if (parent.level == 1) {
            return parent;
        } else
            parent = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == currentId);
        return parent;
    },
    renderOptionSelect: function (list, idSelect = "") {
        html = "";
        $.each(list, function (i, obj) {
            html += `<optgroup label="${obj.parentObj.name}">`;
            $.each(obj.listChild, function (j, obj1) {
                if (idSelect.length > 1)
                    html += `<option value="${obj1.id}" disabled="disabled" ${(j == 0 && i == 0) ? "selected" : ""}>${obj1.name}</option>`;
                else
                    html += `<option value="${obj1.id}" ${(j == 0 && i == 0) ? "selected" : ""}>${obj1.name}</option>`;
            });
            html += `</optgroup>`;
        })
        return html;
    },
    //check click object reference layer /option 1
    checkClickObjectReference: function (args) {
        let idLayer = $(ReferenceContent.SELECTORS.select_reference_layer).val();
        let layerMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == idLayer);
        if (typeof layerMaptile !== "undefined" && (layerMaptile.optionDirectory === 2 || layerMaptile.optionDirectory === 3)) {
            this.checkClickObjectReferenceOptionMaptile(args);
        } else {
            $(this.SELECTORS.infor_result_reference).html("");
            $(this.SELECTORS.list_result_referenced).html("");
            if (this.CONSTS.isCheckReference && this.checkFormSelect()) {

                let idlayerClick = args.feature.getProperties().idDirectory;
                //đối click trúng đối tượng tham chiếu trên map
                if (typeof idLayer != "undefined" && typeof idlayerClick != "undefined" && idLayer === idlayerClick) {
                    let idObject = args.feature.getId();

                    // kiểm tra có phải là đa giác hay k
                    var polygonExist = ReferenceContent.checkIsPolygon(idLayer, idObject);
                    if (polygonExist) {
                        this.eachLayerReferenced(idObject, idLayer);
                    }
                } else { // nếu click trúng hoặc chồng lớp với nhau đi tìm đối tượng theo lớp
                    let point = turf.point([args.location.lng, args.location.lat]);
                    let object = this.SearchLayerInPoly(idLayer, point);
                    if (typeof object !== "undefined") {
                           // kiểm tra có phải là đa giác hay k
                        var polygonExist = ReferenceContent.checkIsPolygon(idLayer, object.id);
                        if (polygonExist) {
                            this.eachLayerReferenced(object.id, idLayer);
                        }
                    } else
                        $(ReferenceContent.SELECTORS.result_reference_content).removeClass('open');
                }
            }
        }

    },
    //check click object reference layer /option 2,3
    checkClickObjectReferenceOptionMaptile: function (args) {
        if (this.CONSTS.isCheckReference && this.checkFormSelect()) {
            let idLayer = $(ReferenceContent.SELECTORS.select_reference_layer).val();
            let layerMaptile = exploit.GLOBAL.listGroundOvelay.filter(x => x.id == idLayer);
            if (typeof layerMaptile !== "undefined") {
                //get link infor
                let url = exploit.getListUrlMaptile(args, layerMaptile);
                //call function get infor
                let objresult = this.getInforServerMapTile(url);
                //call function reference layer eachLayerReferenced (option 1,2,3)
                if (typeof objresult !== "undefined") {
                    objresult = layerMaptile[0].optionDirectory === 3 ? JSON.parse(objresult).features[0] : objresult;
                    objresult = (this.checkVn2000Geojson(objresult)) ? this.ConvertShapeVn2000ToWgs84(objresult) : objresult;
                    this.eachLayerReferenced("", idLayer, objresult);
                }
            }
        }
    },
    // tìm kiếm layer trong hình học
    SearchLayerInPoly: function (idLayer, point) {
        let layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == idLayer);
        var result;
        if (typeof layer !== "undefined") {
            $.each(layer.listMainObject, function (i, item) {
                if (typeof item.geometry !== "undefined" && item.geometry != null) {
                    var inPolygon = ReferenceContent.CheckInGeometryLocation(point, item.geometry.type.toLowerCase(), item);
                    if (inPolygon) {
                        result = item;
                        return false;
                    }
                }
            })
        }
        return result;
    },
    CheckInGeometryLocation: function (point, type, item) {
        let result = false;
        switch (type) {
            case "multipolygon":
                if (item.geometry != null && item.geometry.coordinates != null) {
                    var polyItem = turf.multiPolygon(item.geometry.coordinates);
                    var intersection = turf.booleanPointInPolygon(point, polyItem);
                    if (intersection) {
                        result = true;
                    }
                }
                break;
            case "polygon":
                if (item.geometry != null && item.geometry.coordinates != null) {
                    var polyItem = turf.polygon(item.geometry.coordinates);
                    var intersection = turf.booleanPointInPolygon(point, polyItem);
                    if (intersection) {
                        result = true;
                    }
                }
                break;
        }
        return result;
    },
    // lấy tất cả các đối tượng layer được tham chiếu
    eachLayerReferenced: function (idObject, idLayer, objectClick = null) {
        let object = objectClick === null ? this.getObjectClickMap(idObject, idLayer) : objectClick;
        if (object !== null) {
            let pyl = object.geometry.type.toLocaleLowerCase() == "polygon" ? turf.polygon(object.geometry.coordinates) : turf.multiPolygon(object.geometry.coordinates);
            ThongTin.highlightObjectGeosjon(pyl);
            let listReference = $(ReferenceContent.SELECTORS.select_referenced_multilayer).val();
            let resultReferencedMulti = "";
            this.CONSTS.countNo = 0;
            this.CONSTS.totalIn = 0;
            $.each(listReference, function (i, obj) {
                let objectDirectory = exploit.GLOBAL.listDirectoryMainObject.find(x => x.id == obj)
                if (typeof objectDirectory !== "undefined") {
                    switch (objectDirectory.propertiesDirectory.optionDirectory) {
                        case 1:
                            resultReferencedMulti += ReferenceContent.getObjectOnLayerOption1(obj, pyl);
                            break;
                        case 2:
                            resultReferencedMulti += ReferenceContent.getObjectOnLayerOption2(obj, pyl);
                            break;
                        case 3:
                            resultReferencedMulti += ReferenceContent.getObjectOnLayerOption3(obj, pyl);
                            break;
                    }
                }
            });
            let resultObjectInfor = ReferenceContent.renderObjectInfor(object);
            this.showInforResultReference(resultObjectInfor, resultReferencedMulti);
        }
    },
    // lấy đối tượng trong layer tham chiếu
    getObjectClickMap: function (id, idLayer) {
        if (TimKiem.GLOBAL.ArrayObjectMain.length > 0) {
            let layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == idLayer);
            if (typeof layer !== "undefined") {
                return layer.listMainObject.find(x => x.id == id);
            }
        }
        return null;
    },

    //#region ----option 1,2,3----

    //option 1 
    getObjectOnLayerOption1: function (idLayer, pyl) {
        let layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == idLayer);
        let list = TimKiem.SearchLayerInPoly(layer, pyl);

        // kiểm tra danh sách và chỉ show những đối tượng là đa giác
        if (list != null && list != undefined) {
            list = list.filter(x => x.geometry != null && x.geometry.type.toLocaleLowerCase() == "polygon" || x.geometry.type.toLocaleLowerCase() == "multipolygon");
        }
        return this.renderObjectReferenced(list, layer, pyl);
    },
    //option 2
    getInforServerMapTile: function (param) {
        let result;
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: ReferenceContent.CONSTS.URL_AJAXGETINFOR,
            data: JSON.stringify(param),
            success: function (res) {
                if (res.code == "ok") {
                    result = res.result;
                    let obj = JSON.parse(res.result);
                    if (ReferenceContent.checkVn2000Geojson(obj.features[0])) {
                        obj = ReferenceContent.ConvertShapeVn2000ToWgs84(obj.features[0]);
                    } else {
                        obj = obj.features[0];
                    }
                    ThongTin.highlightObjectGeosjon(obj);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
        return result;
    },
    getObjectOnLayerOption2: function (idLayer, pyl) {
        console.log("option 2");
        let layerMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == idLayer);
        if (typeof layerMaptile !== "undefined") {
            //let a = TimKiem.SearchBBoxMaptile(layerMaptile.url, pyl);
            let url = TimKiem.getUrlInforMapTile(layerMaptile.url, pyl);
            let param = [{ option: layerMaptile.optionDirectory, url: url }];
            let result = this.findObjectMapTile(param);

            if (result != null && result != undefined) {
                result = this.checkListShapeOption23(result.result, pyl);
                let layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == idLayer);
                return this.renderObjectReferenced(result, layer, pyl);
            }
        }
        return "";
    },
    findObjectMapTile: function (param) {
        let result;
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: ReferenceContent.CONSTS.URL_AJAXGETInforMapTile,
            data: JSON.stringify(param),
            success: function (res) {
                if (res.code == "ok") {
                    result = res;
                    //console.log(res);
                    //TimKiem.OpenResultSearchContentMaptile(res.result, res.properties);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
        return result;
    },
    //option 3
    getObjectOnLayerOption3: function (idLayer, pyl) {
        console.log("option 3");
        let layerMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == idLayer);
        if (typeof layerMaptile !== "undefined") {
            let checkVN = this.checkSRSOnUrlVN2000(layerMaptile.url);
            //let a = TimKiem.SearchBBoxMaptile(layerMaptile.url, pyl);
            let url = TimKiem.getUrlInforMapTile(layerMaptile.url, pyl, checkVN);
            let param = [{ option: layerMaptile.optionDirectory, url: url }];
            let result = this.findObjectMapTile(param);
            if (result.code === "ok") {
                result = JSON.parse(result.result);
                result = this.checkListShapeOption23(result.features, pyl, checkVN);
                let layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == idLayer);
                return this.renderObjectReferenced(result, layer, pyl);
            }
        }
        return "";
    },
    checkSRSOnUrlVN2000: function (url) {
        let check = false;
        $.each(ReferenceContent.CONSTS.ListSRS, function (i, obj) {
            if (url.indexOf(obj.toString()) > -1) {
                check = true;
                return true;
            }
        });
        return check;
    },
    ConvertShapeVn2000ToWgs84: function (obj) {
        let type = obj.geometry.type.toLowerCase();
        //let line = [];
        if (type === "polygon") {
            var array = obj.geometry.coordinates;
            var paths = [];
            $.each(array[0], function (i, e) {
                var locationWS84 = ConvertLocationMap.VN2000toWGS84(e[1], e[0], ReferenceContent.CONSTS.KTTruc, ReferenceContent.CONSTS.Zone);
                paths.push([locationWS84[1], locationWS84[0]]);
            });
            let polygon = turf.polygon([paths]);
            return polygon;
        }
        if (type === "multipolygon") {
            var array = obj.geometry.coordinates[0];
            var paths = [];
            let polygon = [];
            $.each(array, function (i, ar) {
                paths = [];
                $.each(ar, function (i, e) {
                    var locationWS84 = ConvertLocationMap.VN2000toWGS84(e[1], e[0], ReferenceContent.CONSTS.KTTruc, ReferenceContent.CONSTS.Zone);
                    paths.push([locationWS84[1], locationWS84[0]]);
                });
                polygon.push(paths);
            });
            return turf.multiPolygon([polygon]);
        }
    },
    //#endregion

    //Show thông tin object tham chiếu và object được tham chiếu
    showInforResultReference: function (htmlinfor, htmlreference) {
        $(ReferenceContent.SELECTORS.result_reference_content).addClass('open');
        $(this.SELECTORS.infor_result_reference).html(htmlinfor);
        $(this.SELECTORS.list_result_referenced).html(htmlreference);
    },
    //html đối tượng được tham chiếu
    renderObjectReferenced: function (list, layer, shape = null) {
        let html = "";
        let listId = list.map(x => x.id);
        let listProperties = this.getPropertiesByArrayMainObject(listId);
        $.each(list, function (i, obj) {
            let area = "";
            let nameLayer = "";
            if (shape !== null) {
                let re = ReferenceContent.getAreaInOutObject(shape, obj);
                ReferenceContent.CONSTS.totalIn = Math.round(Number(ReferenceContent.CONSTS.totalIn + re.in) * 10) / 10;
                area = `<p>Tổng diện tích: ${re.total} (m<sup>2</sup>)</p>
                        <p>Diện tích trong tham chiếu: ${re.in} (m<sup>2</sup>)</p>
                        <p>Diện tích ngoài tham chiếu: ${re.out} (m<sup>2</sup>)</p>`;
                //area += `<p>ST: ${typeof obj.properties.SOTO !== "undefined" ? obj.properties.SOTO : obj.properties.soto} </p>
                //        <p>TT: ${typeof obj.properties.SOTHUTUTHUA !== "undefined" ? obj.properties.SOTHUTUTHUA : obj.properties.sothututhu} </p>
                //        <p>LD: ${typeof obj.properties.LOAIDATBD !== "undefined" ? obj.properties.LOAIDATBD : obj.properties.loaidatbd} </p>
                //        <p>LD: ${typeof obj.properties.DIENTICH !== "undefined" ? obj.properties.DIENTICH : obj.properties.dientich} (m<sup>2</sup>)</p>`;
            }
            let propertiesObject = listProperties.find(x => x.idMainObject == obj.id);
            let htmlProperties = "";
            if (typeof propertiesObject !== "undefined") {
                htmlProperties = ReferenceContent.getPropertiesShowObject(propertiesObject, layer.listProperties);
            }
            var layerEx = exploit.GLOBAL.listDirectoryMainObject.find(x => x.id == layer.id);
            if (typeof layerEx !== "undefined") {
                nameLayer = layerEx.propertiesDirectory.nameTypeDirectory;
            }
            ReferenceContent.CONSTS.countNo += 1;
            let nameObject = typeof obj.nameObject !== "undefined" ? obj.nameObject : obj.id;
            html += `<li class="item-search-referenced" data-id="${obj.id}" data-directory="${obj.idDirectory}">
                            <div class="col-STT">   
                                <span>${ReferenceContent.CONSTS.countNo}</span>
                            </div>
                            <div class="col-Info">
                                <span class="text-info">${nameObject}</span>
                                <p>Lớp dữ liệu : ${nameLayer}</p>
                                ${area}
                                ${htmlProperties}
                            </div>
                         </li>`;
        });
        return html;
    },
    //html thông tin đối tượng
    renderObjectInfor: function (object) {
        let areaObject = this.getAreaObjectPolygonOfMulti(object);
        let areaObjectOut = Math.round(Number(areaObject - this.CONSTS.totalIn) * 10) / 10;
        if (areaObjectOut <= 1) {
            areaObjectOut = 0;
            this.CONSTS.totalIn = areaObject;
        }
        let listProperties = this.getPropertiesByArrayMainObject([object.id]);
        let htmlProperties = "";
        if (typeof listProperties !== "undefined" && listProperties.length > 0) {
            var layer = exploit.GLOBAL.listDirectoryMainObject.find(x => x.id == object.idDirectory);
            htmlProperties = ReferenceContent.getPropertiesShowObject(listProperties[0], layer.propertiesDirectory.listProperties);
        }
        let html = `<p><span>Tên đối tượng: </span><span>${object.nameObject}</span></p>
                    <p><span>Tổng diện tích: </span><span>${areaObject} (m<sup>2</sup>)</span></p>
                    <p><span>Diện tích trong tham chiếu: </span><span>${this.CONSTS.totalIn} (m<sup>2</sup>)</span></p>
                    <p><span>Diện tích ngoài tham chiếu: </span><span>${areaObjectOut} (m<sup>2</sup>)</span></p> ${htmlProperties}`;
        return html;
    },
    //check select 
    checkFormSelect: function () {
        this.clearLabelError();
        let check = true;
        let select_reference_layer = $(ReferenceContent.SELECTORS.select_reference_layer).val();
        if (!validateText(select_reference_layer, "text", 0, 0)) {
            //insertError($(config.SELECTORS.dataName), "other");
            check = false;
            this.showLabelError(ReferenceContent.SELECTORS.select_reference_layer, l("KhaiThac:Error:ErrorReference"));
        }
        let select_referenced_multilayer = $(ReferenceContent.SELECTORS.select_referenced_multilayer).val();
        if (typeof select_referenced_multilayer === "undefined" || select_referenced_multilayer.length <= 0) {
            //insertError($(config.SELECTORS.dataName), "other");
            check = false;
            this.showLabelError(ReferenceContent.SELECTORS.select_referenced_multilayer, l("KhaiThac:Error:ErrorReferenced"));
        }
        return check
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group").append("<lable class='lable-error' style='color:red;'>" + text + "</lable>");
    },
    clearLabelError: function () {
        $('.form-group').find(".lable-error").remove();
        $('.form-group').removeClass("has-error");
    },

    //#region
    // lấy diện tích của geometry polygon
    getAreaObject: function (geometry) {
        let poly = new polygonEsri({
            rings: geometry.coordinates
        });
        var area = geometryEngineEsri.geodesicArea(poly, 'square-meters');
        return Math.abs(Math.round(area * 10) / 10);
    },
    //lấy diện tích của object polygon or multi
    getAreaObjectPolygonOfMulti: function (object) {
        let area = 0;
        if (object.geometry.type.toLocaleLowerCase() === "polygon") {
            area = this.getAreaObject(object.geometry);
        }
        if (object.geometry.type.toLocaleLowerCase() == "multipolygon") {
            for (var i = 0; i < object.geometry.coordinates.length; i++) {
                let polyChild = turf.polygon(object.geometry.coordinates[i]);
                area += this.getAreaObject(polyChild.geometry);
            }
        }
        return Math.round(area * 10) / 10;
    },
    //check vn2000 geojson
    checkVn2000Geojson: function (object) {
        if (object.geometry.type.toLocaleLowerCase() === "polygon") {
            return object.geometry.coordinates[0][0][0] > 180
        }
        if (object.geometry.type.toLocaleLowerCase() === "multipolygon") {
            return object.geometry.coordinates[0][0][0][0] > 180
        }
        return false;
    },
    //get area object 1 
    getAreaInOutObject: function (object1, object2) {
        let re;
        try {
            if (!this.checkVn2000Geojson(object2)) {
                let shape1 = this.convertObjectTurf(object1.geometry);
                let shape2 = this.convertObjectTurf(object2.geometry);
                let shapeIntersect = turf.intersect(shape2, shape1);
                //tổng diện tích
                let areaTotal = this.getAreaObjectPolygonOfMulti(shape2);
                //Dien tich trong quy hoạch
                let areaIn = this.getAreaObjectPolygonOfMulti(shapeIntersect);
                //Dien tich đất ngoài quy hoạch:
                let areaOut = Math.round((areaTotal - areaIn) * 10) / 10;
                re = {
                    total: areaTotal,
                    in: areaIn,
                    out: areaOut
                }
            } else {
                let poly2 = new polygonEsri({
                    rings: TimKiem.GLOBAL.polygonVN200,
                })
                let polyPath = ReferenceContent.convertCoordinate(object2);
                let polygonParam = new polygonEsri({
                    rings: polyPath,
                })
                let geometry = geometryEngineEsri.intersect(polygonParam, poly2);

                /* var area = geometryEngineEsri.geodesicArea(polygonParam, 'square-meters');*/
                var area = polygonParam.rings.length > 1 ? this.calculatorAreaMultiPolygon(polygonParam.rings) : this.AreaPolygonVN2000(polygonParam.rings[0]);
                let areaTotal = Math.abs(Math.round(area * 10) / 10);

                if (geometry !== null && typeof geometry !== "undefined" && geometry.rings !== null) {
                    area = geometry.rings.length > 1 ? this.calculatorAreaMultiPolygon(geometry.rings) : this.AreaPolygonVN2000(geometry.rings[0]);
                    let areaIn = Math.abs(Math.round(area * 10) / 10);
                    let areaOut = Math.round((areaTotal - areaIn) * 10) / 10;
                    re = {
                        total: areaTotal,
                        in: areaIn,
                        out: areaOut
                    }
                }
            }
        }
        catch (e) {
            console.log("error: getAreaInOutObject()");
            re = {
                total: 0,
                in: 0,
                out: 0
            }
        }
        return re;
    },
    AreaPolygonVN2000: function (points) {
        var area = 0.0;
        var first = points[0];
        var e0 = [0, 0];
        var e1 = [0, 0];
        var l = points.length;
        for (var i = 2; i < l; i++) {
            var p = points[i - 1];
            var c = points[i];
            e0[0] = first[0] - c[0];
            e0[1] = first[1] - c[1];
            e1[0] = first[0] - p[0];
            e1[1] = first[1] - p[1];
            area += (e0[0] * e1[1]) - (e0[1] * e1[0]);
        }
        return Math.abs(area / 2);
    },
    //convert object to geometry turf
    convertObjectTurf: function (geometry) {
        if (geometry.type.toLocaleLowerCase() === "polygon") {
            return turf.polygon(geometry.coordinates);
        }
        if (geometry.type.toLocaleLowerCase() === "multipolygon") {
            return turf.multiPolygon(geometry.coordinates);
        }
    },
    //calculator area
    calculatorAreaMultiPolygon: function (rings) {
        let total = 0, check = false;
        for (var i = 0; i < rings.length; i++) {
            if (i == 0) total = this.AreaPolygonVN2000(rings[i]);
            else {
                check = false;
                for (var j = 0; j < i; j++) {
                    let geometry = ReferenceContent.checkGeometryIntersect(rings[i], rings[j]);
                    if (geometry !== null && typeof geometry !== "undefined" && geometry.rings !== null) {
                        check = true;
                    }
                }
                let area = this.AreaPolygonVN2000(rings[i]);
                if (check) total -= area;
                else total += area;
            }
        }
        return total;
    },
    checkListShape: function (list) {
        PlanShap.GLOBAL.listShape = [];
        let poly2 = new polygonEsri({
            rings: PlanShap.GLOBAL.polygonVN200,
        })
        //var poly2= turf.polygon(PlanShap.GLOBAL.polygonVN200);
        $.each(list, function (i, obj) {
            let polyPath = ReferenceContent.convertCoordinate(obj);
            let polygonParam = new polygonEsri({
                rings: polyPath,
            })
            //var poly1 = turf.polygon(polyPath);

            //let geometry = turf.intersect(poly1, poly2);
            let geometry = geometryEngineEsri.intersect(polygonParam, poly2);
            //var area = geometryEngineEsri.geodesicArea(geometry, "hectares");
            if (geometry !== null && typeof geometry !== "undefined" && geometry.rings !== null) {
                var matharea = PlanShap.AreaPolygonVN2000(geometry.rings[0]);
                var polygoShape = {
                    "type": "Feature",
                    "properties": {
                        "DIENTICHQH": matharea.toFixed(1),
                        "LOAIDATBD": obj.properties.LOAIDATBD,
                        "MADVHC": obj.properties.MADVHC,
                        "SOTHUTUTHUA": obj.properties.SOTHUTUTHUA,
                        "SOTO": obj.properties.SOTO,
                        "DIENTICH": obj.properties.DIENTICH,
                        "DIENTICHPL": obj.properties.DIENTICHPL,
                    },
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": geometry.rings
                    }
                }
                PlanShap.GLOBAL.listShape.push(polygoShape);
                // var area = geometryEngineEsri.geodesicArea(geometryEngine.simplify(graphic.geometry), "acres");
                //console.log(obj);
                //console.log(area);
                //console.log(JSON.stringify(geometry));
            }
        });
    },
    checkListShapeOption23: function (list, poly, checkVN = false) {
        let lstResult = [];
        if (!checkVN) {
            $.each(list, function (i, item) {
                if (item.geometry != undefined && item.geometry != null) {
                    var inPolygon = TimKiem.CheckInGeometry(poly, item.geometry.type.toLowerCase(), item);
                    if (inPolygon) {
                        lstResult.push(item);
                    }
                }
            })
        } else {
            //let polyPath =   ReferenceContent.convertCoordinate(poly);
            //let poly2 = new polygonEsri({
            //    rings: TimKiem.GLOBAL.polygonVN200,
            //})
            $.each(list, function (i, item) {
                if (item.geometry != undefined && item.geometry != null) {
                    polyPath = ReferenceContent.convertCoordinate(item);
                    //let polygonParam = new polygonEsri({
                    //    rings: polyPath,
                    //})
                    //let geometry = geometryEngineEsri.intersect(polygonParam, poly2);
                    let geometry = ReferenceContent.checkGeometryIntersect(TimKiem.GLOBAL.polygonVN200, polyPath);
                    if (geometry !== null && typeof geometry !== "undefined" && geometry.rings !== null) {
                        lstResult.push(item);
                    }
                }
            })

        }
        return lstResult
    },
    checkGeometryIntersect: function (shape1, shape2) {
        let poly2 = new polygonEsri({
            rings: shape1,
        })
        //polyPath = ReferenceContent.convertCoordinate(item);
        let polygonParam = new polygonEsri({
            rings: shape2,
        })
        return geometryEngineEsri.intersect(polygonParam, poly2);
    },
    convertCoordinate: function (data) {
        let res = [];
        if (data.geometry.type.toLocaleLowerCase() === "polygon") {
            return data.geometry.coordinates;
        }
        if (data.geometry.type.toLocaleLowerCase() === "multipolygon") {
            let lenght = data.geometry.coordinates[0].length;
            for (var i = 0; i < lenght; i++) {
                let datatemp = data.geometry.coordinates[0][i];
                res.push(datatemp);
            }
            return res;
        }
    },
    // lấy thuộc tính của nhóm đối tượng
    getPropertiesByArrayMainObject: function (array) {
        var re = [];
        var dto = {
            arrayId: array
        };
        $.ajax({
            type: "POST",
            url: TimKiem.CONSTS.URL_GET_PROPERTIES_BY_ARRAY_MAIN_OBJECT,
            data: JSON.stringify(dto),
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.code === "ok") {
                    re = data.result;
                } else console.error(data.message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
        return re;
    },
    // lấy thuộc tính hiển thị tham chiếu
    getPropertiesShowObject: function (propertiesObject, propertiesLayer) {
        let propertiesHTML = "";
        let listPropertiesLayer = propertiesLayer.filter(x => x.typeSystem != 1 && x.typeProperties != "geojson" && x.isShowReference); // lấy thuộc tính của layer
        if (typeof listPropertiesLayer !== "undefined" && listPropertiesLayer.length > 0) {
            $.each(listPropertiesLayer, function (i, itemPropertes) {
                var property = propertiesObject.listProperties.find(x => x.codeProperties == itemPropertes.codeProperties); // map thuộc tính của layer và thuộc tính của đối tượng
                var namePropertyView = itemPropertes.nameProperties; // tên thuộc tính được hiển thị
                var valuePropertyView = ""; // value thuộc tính được hiển thị

                if (property != undefined) {
                    if (itemPropertes.typeProperties !== "link" && itemPropertes.typeProperties !== "file" && itemPropertes.typeProperties !== "image") {
                        if (itemPropertes.typeProperties == "list" || itemPropertes.typeProperties == "radiobutton" || itemPropertes.typeProperties == "checkbox") {
                            var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
                            var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách

                            try {
                                lstValueDefault = JSON.parse(itemPropertes.defalutValue);
                            }
                            catch (e) {

                            }

                            if (lstValueDefault.length > 0) {
                                if (property.defalutValue != "" && property.defalutValue != undefined && property.defalutValue != null) {
                                    lstValueObjectMain = property.defalutValue.split(',');
                                }

                                var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng

                                valuePropertyView = lstMapValue.map(x => x.name).join(', ');
                            }
                        }
                        else {
                            valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
                        }
                        propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;
                    }
                }
                else {
                    propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;
                }
            });
        }
        return propertiesHTML;
    },

    checkIsPolygon: function (idLayer, idObject) {
        var result = false;
        var layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == idLayer);
        if (layer != undefined) {
            if (layer.listMainObject != undefined) {
                var obj = layer.listMainObject.find(x => x.id == idObject);
                if (obj != undefined) {
                    if (obj.geometry != undefined && (obj.geometry.type.toLowerCase() == "polygon" || obj.geometry.type.toLowerCase() == "multipolygon")) {
                        result = true;
                    }
                }
            }
        }

        return result;
    },
    // move đến vị trí zoom của lớp tham chiếu
    moveLayerReference: function (id) {
        let obj = exploit.GLOBAL.listDirectoryMainObject.find(x => x.id === id);
        if (typeof obj !=="undefined" && obj !== null) {
            let lat = obj.propertiesDirectory.location.lat > 0 ? parseFloat(obj.propertiesDirectory.location.lat) : parseFloat(exploit.GLOBAL.defaultConfig.lat);
            let lng = obj.propertiesDirectory.location.lng > 0 ? parseFloat(obj.propertiesDirectory.location.lng) : parseFloat(exploit.GLOBAL.defaultConfig.lng);
            let zoom = obj.propertiesDirectory.zoom > 0 ? obj.propertiesDirectory.zoom : (exploit.GLOBAL.defaultConfig.zoom != null && exploit.GLOBAL.defaultConfig.zoom > 0 ? exploit.GLOBAL.defaultConfig.zoom : 15);
            var cameraPosition = {
                target: { lat: lat, lng: lng },
                tilt: 0,
                bearing: 0,
                zoom: zoom
            };
            map.moveCamera(cameraPosition, null);
        }
    },
    //#endregion
}
