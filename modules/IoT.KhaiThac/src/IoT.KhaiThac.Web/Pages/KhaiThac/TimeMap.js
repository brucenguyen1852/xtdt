﻿
var timemap = {
    SELECTORS: {
        time_select_common: ".time-select-common",
        time_year_select: ".time-year-select",
        time_month_select: ".time-month-select",
        btn_time_save: ".btn-save-time",
        btn_time_close: ".btn-close-time",
        model_change_time_map: ".change-model-time-map",
        time_show: ".calendar .time",
        btn_change_show: ".change-time-map .calendar",
        btn_current_date:".present-years",
    },
    init: function () {
        timemap.setEvent();
    },
    setEvent: function () {
        $(timemap.SELECTORS.time_year_select).children().remove();
        var data = timemap.setYear();
        $(timemap.SELECTORS.time_year_select).append(data);
        timemap.setMonthCurrent();
        timemap.setHtmlShowTime(true, null);
        $(timemap.SELECTORS.btn_time_save).on("click", function () {
            $(timemap.SELECTORS.model_change_time_map).hide();
            timemap.saveTimeMap();
            if (!map.is3dMode()) {
                map.enable3dMode(true);
            }
        });
        $(timemap.SELECTORS.btn_time_close).on("click", function () {
            $(timemap.SELECTORS.model_change_time_map).hide();
        });
        $(timemap.SELECTORS.btn_change_show).on("click", function () {
            if (!$(this).hasClass("hideTime")) {
                $(timemap.SELECTORS.model_change_time_map).show();
            }
        });
        $(timemap.SELECTORS.btn_current_date).on("click", function () {
            timemap.setMonthCurrent();
            timemap.saveTimeMap();
            $(timemap.SELECTORS.model_change_time_map).hide();
        });
    },
    setYear: function () {
        let date = new Date();
        let yearCurrent = date.getFullYear() + 10;
        let html = '';
        for (var i = 2011; i < yearCurrent; i++) {
            if (i == date.getFullYear()) {
                html += '<option selected value="' + i + '">Năm ' + i + '</option>';
            }
            else
                html += '<option value="' + i + '">Năm ' + i + '</option>';
        }
        return html;
    },
    setMonthCurrent: function () {
        let date = new Date();
        let monthCurrent = date.getMonth() + 1;
        $(timemap.SELECTORS.time_month_select).val(monthCurrent);
        let yearCurrent = date.getFullYear();
        $(timemap.SELECTORS.time_year_select).val(yearCurrent);
    },
    saveTimeMap: function () {
        let year = $(timemap.SELECTORS.time_year_select).val();
        let month = Number($(timemap.SELECTORS.time_month_select).val()) - 1;
        let dateCurrent = new Date();
        dateCurrent.setMonth(month);
        dateCurrent.setFullYear(year);
        map.setDate(dateCurrent);
        timemap.setHtmlShowTime(false, dateCurrent);
    },
    setHtmlShowTime: function (check, date) {
        let monthCurrent, year;
        if (check) {
            let dateCurrent = new Date();
            monthCurrent = dateCurrent.getMonth() + 1;
            year = dateCurrent.getFullYear();
        } else {
            monthCurrent = date.getMonth() + 1;
            year = date.getFullYear();
        }
        $(timemap.SELECTORS.time_show).text(monthCurrent + "-" + year);
    },
    checkBtnTimeChange: function (check) {
        if (check) {
            $(timemap.SELECTORS.btn_change_show).removeClass("hideTime");
        } else {
            $(timemap.SELECTORS.btn_change_show).addClass("hideTime");
        }
    },
}

