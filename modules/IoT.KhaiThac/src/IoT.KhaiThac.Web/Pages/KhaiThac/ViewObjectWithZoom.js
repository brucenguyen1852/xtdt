﻿var ViewObjectWithZoom = {
    GLOBAL: {
        OldZoom: 0
    },
    CONSTS: {},
    SELECTORS: {},
    init: function () {
        this.mapEvent();
    },
    setEvent: function () { },
    mapEvent: function () {
        map.addListener("idle", (args) => {
            var zoomCurrent = args.camera.getZoom();
            var mod = map.is3dMode();
            if (!mod) {
                if (ViewObjectWithZoom.GLOBAL.OldZoom != zoomCurrent) {
                    ViewObjectWithZoom.GLOBAL.OldZoom = zoomCurrent;
                    ViewObjectWithZoom.resetAllViewObjectWithZoom(zoomCurrent);
                }
            }
        })
    },
    //check layer được phép show lên có mức zoom hợp lệ
    checkShowObjectWithZoom: function (id) {
        var obj = exploit.GLOBAL.listDirectoryMainObject.find(x => x.id == id);
        if (typeof obj !== "undefined" && obj !== null) {
            if (obj.propertiesDirectory.minZoom > 0 && obj.propertiesDirectory.maxZoom < 23) {
                var camera = map.getCamera().getZoom();
                if (camera >= obj.propertiesDirectory.minZoom && obj.propertiesDirectory.maxZoom >= camera) return true;
                return false;
            } else return true;
        }
        return true;
    },
    //reset lại khi zoom thay đổi
    resetAllViewObjectWithZoom: function (zoomCurrent) {
        //var listHide = exploit.GLOBAL.listDirectoryMainObject.filter(x => (x.propertiesDirectory.minZoom > 0 && x.propertiesDirectory.maxZoom > 0) && (x.propertiesDirectory.minZoom >= zoomCurrent || x.propertiesDirectory.maxZoom <= zoomCurrent));
        var listShow = exploit.GLOBAL.listDirectoryMainObject.filter(x => (x.propertiesDirectory.minZoom <= zoomCurrent && x.propertiesDirectory.maxZoom >= zoomCurrent) || (x.propertiesDirectory.minZoom === 0 && x.propertiesDirectory.maxZoom === 0));
        //hide and show datalayer
        if (typeof listShow !== "undefined" && listShow.length > 0) {
            //clear all
            map.data.clear();
            exploit.GLOBAL.lstFeature = [];
            //show object layer
            var listgeojsonZoom = JSON.parse(JSON.stringify(exploit.GLOBAL.listgeojson));
            $.each(listShow, function (i,obj) {
                ViewObjectWithZoom.hideNotRemoveObjectLayerById(obj.id, listgeojsonZoom);
            });
        } else map.data.clear();
        //hide and marker
        this.hideMarkerNotListShow();
    },
    hideNotRemoveObjectLayerById: function (id, listgeojsonZoom) {
        setTimeout(function () {
            let checklayer = listgeojsonZoom.find(x => x.id == id);
            //show datalayer
            if (typeof checklayer !== "undefined" && checklayer !== null && checklayer !== "") {
                checklayer = listgeojsonZoom.filter(x => x.id == id);
                if (checklayer.length > 0) {
                    checklayer.forEach((geojson) => {
                        exploit.showDataGeojsonReset(geojson.geojson, geojson.id);
                    });
                }
            }
            //show marker
            let layerMarker = exploit.GLOBAL.listMarker.find(x => x.id == id);
            if (layerMarker != null && layerMarker != undefined) {
                layerMarker.lstMarker.forEach((marker) => {
                    if (!marker.isVisible()) marker.setVisible(true);
                });
                //exploit.GLOBAL.listMarker.splice(exploit.GLOBAL.listMarker.findIndex(function (i) {
                //    return i.id === id;
                //}), 1);
            }
            //let layerObject = exploit.GLOBAL.listObject3D.filter(x => x.idLayer == id);
            //if (layerObject.length > 0) {
            //    layerObject.forEach((marker) => {
            //        marker.object != null && marker.object.setMap(null);
            //        exploit.GLOBAL.listObject3D.splice(exploit.GLOBAL.listObject3D.findIndex(function (i) {
            //            return i.id === marker.id;
            //        }), 1);
            //    });
            //}
            //exploit.showObjectMap();

            //let lst = exploit.GLOBAL.listDirectoryMainObject.filter(x => x.id == id);
            //if (lst != null && lst != undefined) {
            //    exploit.GLOBAL.listDirectoryMainObject.splice(exploit.GLOBAL.listDirectoryMainObject.findIndex(function (i) {
            //        return i.id === id;
            //    }), 1);
            //}

        }, 10);
    },
    hideMarkerNotListShow: function (listShow) {
        var listmarker = exploit.GLOBAL.listMarker;
        $.each(listShow, function (i,obj) {
            listmarker = listmarker.filter(x => x.id !== obj.id);
        });
        if (listmarker.length > 0) {
            $.each(listmarker, function (i, obj) {
                obj.lstMarker.forEach((marker) => {
                    if (!marker.isVisible()) return;
                    else marker.setVisible(false);
                });
            });
        }
    },
}