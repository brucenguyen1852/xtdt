﻿var LayerBaseMap = {
    GLOBAL: {
        OverLay: null,
        OrderBaseMap: -1
    },
    CONSTS: {
        URL_AJAX_ARRAY_LAYER_BASE_MAP: "/api/htkt/layerBaseMap/get-list"
    },
    SELECTORS: {
        content_tile_map: ".change-tile-map",
    },
    init: function () {
        this.GetLayerBaseMap();
        this.setEvent();
        //this.setDefaultLayer();
    },
    setEvent: function () {

        $(LayerBaseMap.SELECTORS.content_tile_map).on('click', function () {
            $('#modal-list-layer-base-map').modal('show')
        })

        $(".item-map").click(function () {
            var title = $(this).attr('data-original-title');
            var image = $(this).attr('data-image');

            $('.change-tile-map label').text(title);
            $(LayerBaseMap.SELECTORS.content_tile_map).attr('data-original-title', title)
            //$(LayerBaseMap.SELECTORS.content_tile_map).css('background-image', `url(${image})`)
            $(".item-map").removeClass('item-active');
            $(this).addClass('item-active');
            var order = parseInt($(this).attr('data-order'));

            if (order != LayerBaseMap.GLOBAL.OrderBaseMap) {
                LayerBaseMap.GLOBAL.OrderBaseMap = order;

                var urlTile = $(this).attr("data-url");

                if (urlTile != "") {
                    var exp = "";
                    var host = "";

                    var lst = urlTile.split('${z}/${x}/${y}');

                    var type = "";
                    if (urlTile.includes('{z}/{x}/{y}')) {
                        lst = urlTile.split('{z}/{x}/{y}');
                        type = '{z}/{x}/{y}';
                    }
                    else if (urlTile.includes('${z}/${x}/${y}')) {
                        lst = urlTile.split('${z}/${x}/${y}');
                        type = '${z}/${x}/${y}';
                    }
                    else if (urlTile.includes('x={x}&y={y}&z={z}')) {
                        lst = urlTile.split('x={x}&y={y}&z={z}');
                        type = 'x={x}&y={y}&z={z}';
                    }
                    else if (urlTile.includes('x=${x}&y=${y}&z=${z}')) {
                        lst = urlTile.split('x=${x}&y=${y}&z=${z}');
                        type = 'x=${x}&y=${y}&z=${z}';

                    }

                    if (lst.length > 1) {
                        host = lst[0];
                        exp = lst[1];
                    }
                    else {
                        host = lst[0];
                    }

                    if (lst != 0) {
                        let options = {
                            getUrl: function (x, y, z, is3dMode) {
                                if (type == "{z}/{x}/{y}" || type == '${z}/${x}/${y}') {
                                    return host + `${z}/${x}/${y}` + exp;
                                }
                                else {
                                    return host + `x=${x}&y=${y}&z=${z}` + exp;
                                }
                                //return urlTile + `/${z}/${x}/${y}.${exp.trim()}`
                                //return `https://maptiles.p.rapidapi.com/en/map/v1/{z}/{x}/{y}.png`;
                            },
                            visible: true,
                            zIndex: 1
                        }

                        if (LayerBaseMap.GLOBAL.OverLay != null) {
                            LayerBaseMap.GLOBAL.OverLay.setMap(null);
                        }

                        LayerBaseMap.GLOBAL.OverLay = new map4d.TileOverlay(options)

                        LayerBaseMap.GLOBAL.OverLay.setMap(map);
                    }
                }
                else {
                    if (LayerBaseMap.GLOBAL.OverLay != null) {
                        LayerBaseMap.GLOBAL.OverLay.setMap(null);
                    }
                    else {
                        LayerBaseMap.GLOBAL.OverLay.setMap(null);
                    }
                }

            }

            initTooltip();
        });
    },
    // danh sách biểu đồ nền
    GetLayerBaseMap: function () {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: LayerBaseMap.CONSTS.URL_AJAX_ARRAY_LAYER_BASE_MAP,
            data: {
            },
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    var html = `<div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <div class="item-map item-active data-order="-1" data-toggle="tooltip" data-placement="bottom" title="Mặc định"
                                           style="display: block; background-repeat: no-repeat; background-position: center center; background-size: 100%; background-image: url(${location.origin}/images/logomap4d.png)"
                                            data-url="" data-image="${location.origin}/images/logomap4d.png" >
                                           <label>Mặc định</label>
                                       </div>
                                    </div>
                                `;

                    for (var i = 0; i < data.items.length; i++) {

                        html += `
                            <div class="col-md-3 col-xs-3">
                                <div class="item-map" data-order="${data.items[i].order}" data-toggle="tooltip" data-placement="bottom" title="${data.items[i].nameBasMap}"
                                    style="display: block; background-repeat: no-repeat; background-position: center center; background-size: 100%;  background-image: url(${data.items[i].image});"
                                    data-url="${data.items[i].link}"  data-image="${data.items[i].image}">
                                    <label>${LayerBaseMap.htmlEntities(data.items[i].nameBasMap)}</label>
                                </div>
                            </div>`;
                    }

                    html += `</div>`;

                    $('#body-modal-list-layer-base-map').html(html);
                    initTooltip();
                }
                else {
                    console.log(res);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    htmlEntities: function (str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    },
    setDefaultLayer: function () {
        let options = {
            getUrl: function (x, y, z, is3dMode) {
                return `https://api-extiler.map4d.vn/map/tile/607814ad343b585096aa3d1e/${z}/${x}/${y}.png`;
            },
            visible: true,
            zIndex: 1
        }

        if (LayerBaseMap.GLOBAL.OverLay != null) {
            LayerBaseMap.GLOBAL.OverLay.setMap(null);
        }

        LayerBaseMap.GLOBAL.OverLay = new map4d.TileOverlay(options)

        LayerBaseMap.GLOBAL.OverLay.setMap(map);
    }
}