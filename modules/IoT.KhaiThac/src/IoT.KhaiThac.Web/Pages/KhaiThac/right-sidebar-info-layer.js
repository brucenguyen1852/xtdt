﻿var l = abp.localization.getResource('KhaiThac');
var oldIconHignlight = "";
var ThongTin =
{
    GLOBAL: {
        idMainObject: "",
        idDirectory: "",
        mainObjectSelected: null,
        dataObjectHover: null,
        listHighlight: [],
        listHighlightPoint: [],
        // step info
        ArrayImageMainObject: [],

        //step activity
        ArrayDirectoryActivity: [],
        ArrayActivityDetail: [],
        listObjectImageActivity: [],
        listObjectLinkCameraActivity: [],
        listObjectFileActivity: [],
        listColumnImage: [
            {
                "width": 5,
                "name": l('Main:No.')
            },
            {
                "width": 35,
                "name": l('Main:Name')
            },
            {
                "width": 20,
                "name": l('Main:Image')
            }
            //,
            //{
            //    "width": 30,
            //    "name": "Đường dẫn"
            //}
        ],
        listColumnLinkCamera: [
            {
                "width": 5,
                "name": l('Main:No.')
            },
            {
                "width": 30,
                "name": l('Main:Name')
            },
            {
                "width": 45,
                "name": l('Main:Path')
            },
            //{
            //    "width": 10,
            //    "name": "Thứ tự"
            //}
        ],
        listColumnFile: [
            {
                "width": 5,
                "name": l('Main:No.')
            },
            {
                "width": 25,
                "name": l('Main:Name')
            },
            {
                "width": 10,
                "name": l('Main:Folder')
            },
            {
                "width": 35,
                "name": l('Main:Path')
            },
            //{
            //    "width": 25,
            //    "name": "Hành động"
            //}
        ],

        // step document
        lstDocument: [],
        lstOrder: [],
        lstIcon: { 'word': 'fa fa-file-word-o', 'excel': 'fa fa-file-excel-o"', 'ppt': 'fa fa-file-powerpoint-o', 'pdf': 'fa fa-file-pdf-o', 'file': 'fa fa-file-o' },

        // search obj
        lstSeachObject: [],
        page: 0,
        isReachedScrollEnd: false,
        lstTypeFile: [],
        lstFileMainObject: [],
        cancelRequest: null,
        delayTimerSearch: null,
        MarkerFocus: null
    },
    CONSTS: {
        URL_AJAXGETDATA: '/api/HTKT/ManagementData/get-object',
        URL_DANH_SACH_HOAT_DONG: "/api/htkt/detailActivity/get-list-detail-activity",
        URL_GET_DETAIL_ACTIVITY: "/api/app/detail-activity",
        URL_GET_PROPERTIES_ACTIVITY_DIRECTORY: "/api/khaithac/khaiThacPropertiesDirectoryActivity/get-properties-directory-activity",
        URL_DANH_SACH_LOAI_HOAT_DONG: "/api/htkt/directoryactivity/get-list-new",

        URL_GET_LIST_OBJECT: "/api/HTKT/ManagementData/get-number-object-by-IdDirectory",
    },
    SELECTORS: {
        inforData: "#InforData",
        Item_Menu_Right: ".li-modal-data-right",
        Step_Info: ".step-info",
        Step_Activity: ".step-action",
        Step_Document: ".step-document",
        Btn_View_Image: ".btn-view-image",
        Modal_View_Image: "#myModalViewImageInfor",
        CarouselIndicatorsLiViewImage: "#myModalViewImageInfor .carousel-indicators",
        CarouselIndicatorsViewImage: "#myModalViewImageInfor .carousel-inner",
        // step activity
        table: ".table-activity",
        Modal_Activity_Infor: "#myModalInforActivityDetail",
        Btn_Active_Infor: "#btn-activity-info",
        Modal_Show_Activity_Info: "#myModalActivity",
        Btn_Show_Activity_Infor: ".show-detail-activity-info",
        //- mở rộng
        body_thong_tin: "#body-thong-tin-hoat-dong",
        body_mo_rong: "#body-mo-rong-hoat-dong",
        item_file_place: ".item-FilePlace",
        item_file_place_active: ".item-FilePlace.activefile",
        item_upload_file: ".file-upload",

        imageInfor: ".table-infor-object .image-infor",
        tableTbody: ".table-infor-object tbody",
        tableThead: ".table-infor-object thead",
        table_mo_rong: ".table-infor-object",
        // step document
        listFileUpload: ".list-file-upload",
        liListFile: ".li-list-file",
        iconUpload: ".file-upload-content .upload-file-icon",
        divBorderFile: ".div-border-file",
        fileActive: ".file-active",
        btnDownload: "#btn-download-document",
        btnView: "#btn-view-document",
        div_file: ".div-file",

        // step thong tin
        btnExportfile: ".btn-exportfile-info",

        // search
        btn_open_list: ".toggle-lst-Objec-property2-open",
        btn_close_list: ".toggle-lst-Objec-property2",
        content_lst: ".content-lst",
        content_list_object: ".list-object-content",
        header_list_object: ".lst-Object-property-header",
        input_search_object: "#search-lst-object",
        selected_search: "#select-propeties-search",

        //fileactivity
        downloadActivityFile: ".download-activity-file",
        viewActivityFile: ".view-activity-file"
    },
    init: function () {
        ThongTin.setEvent();
        ThongTin.eventSildeImage();
    },
    setEvent: function () {
        $('.toggle-detail-property2').on('click', function () {
            var mod = map.is3dMode();

            var building = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
            if (building != undefined) {
                if (building.object != undefined) {
                    building.object.setSelected(false);
                }
            }

            if (!$(ThongTin.SELECTORS.inforData).hasClass('detail-property-collapse')) {
                $(ThongTin.SELECTORS.inforData).toggleClass('detail-property-collapse');
                ThongTin.GLOBAL.idMainObject = '';
                ThongTin.GLOBAL.idDirectory = '';
                ThongTin.GLOBAL.ArrayDirectoryActivity = [];
                ThongTin.clearHighlight();
            }
            ThongTin.clearHighlightPoint();
            map.setSelectedBuildings([]);
        });

        $(ThongTin.SELECTORS.Item_Menu_Right).on('click', function () {
            let step = $(this).attr("data-li");
            let stepNow = $(ThongTin.SELECTORS.Item_Menu_Right + '.active').attr('data-li');
            if (step != stepNow) {
                $(ThongTin.SELECTORS.Item_Menu_Right).removeClass("active");
                $(this).addClass("active");
                $('.section-info').css('display', 'none');
                ThongTin.ResetIconInfoData();
                switch (step) {
                    case "step-info":
                        $(ThongTin.SELECTORS.Step_Info).css('display', 'block');
                        $(this).find("a").html(`<svg id="Group_2087" data-name="Group 2087" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                                  <path id="Path_44" data-name="Path 44" d="M0,0H24V24H0Z" fill="none"/>
                                                  <path id="Path_45" data-name="Path 45" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22ZM11,11v6h2V11Zm0-4V9h2V7Z" fill="var(--primary)"/>
                                                </svg>`);
                        break;
                    case "step-action":
                        ThongTin.GetDirectoryActivity(ThongTin.GLOBAL.idDirectory);
                        ThongTin.GetDanhSachHoatDong(ThongTin.GLOBAL.idMainObject, ThongTin.GLOBAL.idDirectory);
                        $(ThongTin.SELECTORS.Step_Activity).css('display', 'block');
                        $(this).find("a").html(`<svg id="Group_2849" data-name="Group 2849" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                                  <path id="Path_6517" data-name="Path 6517" d="M0,0H24V24H0Z" fill="none"/>
                                                  <path id="Path_6518" data-name="Path 6518" d="M16.626,3.132,9.29,10.466,9.3,14.713l4.238-.007,7.331-7.332a9.991,9.991,0,1,1-4.241-4.242ZM20.486,2.1,21.9,3.515l-9.192,9.192-1.412,0,0-1.417L20.485,2.1Z" fill="var(--primary)"/>
                                                </svg>`);
                        break;
                    case "step-document":
                        $(ThongTin.SELECTORS.Step_Document).css('display', 'block');
                        ThongTin.GetFileByObjectMainId(ThongTin.GLOBAL.idMainObject, ThongTin.updateList);
                        $(this).find("a").html(`<svg id="Group_14228" data-name="Group 14228" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                                  <path id="Path_388" data-name="Path 388" d="M0,0H24V24H0Z" fill="none"/>
                                                  <path id="Path_389" data-name="Path 389" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H4V19l2-8H22.5l-2.31,9.243a1,1,0,0,1-.97.757Z" fill="var(--primary)"/>
                                                </svg>`);
                        break;
                    default:
                        break;
                }
            }
        });

        $(ThongTin.SELECTORS.Btn_Active_Infor).on('click', function () {
            if (ThongTin.GLOBAL.ArrayActivityDetail.length > 0) {
                $(ThongTin.SELECTORS.Modal_Activity_Infor).modal('show');
                ThongTin.AddTableDanhSachHoatDong();
            }
        });

        $(ThongTin.SELECTORS.Modal_Activity_Infor).on('click', ThongTin.SELECTORS.Btn_Show_Activity_Infor, function () {
            var id = $(this).attr('data-id');
            var directoryActivityId = $(this).attr('data-directoryActivity');
            ThongTin.GetFileByActivityId(id);

            var lstProperties = ThongTin.GetActivityDirectoryProperties(directoryActivityId);
            if (lstProperties != null) {
                ThongTin.GLOBAL.lstTypeFile = [];
                let type = lstProperties.listProperties.filter(x => x.typeProperties == "file");
                $.each(type, function (i, obj) {
                    ThongTin.GLOBAL.lstTypeFile.push({ key: obj.codeProperties, value: obj.nameProperties });
                });
                $(ThongTin.SELECTORS.Modal_Show_Activity_Info + ' .li-tab').removeClass('active');
                $(ThongTin.SELECTORS.Modal_Show_Activity_Info + ' .li-tab').first().click();
                ThongTin.GLOBAL.listObjectImageActivity = [];
                ThongTin.GetDetailActivityInfo(id, lstProperties.listProperties);
            }
        });

        $(ThongTin.SELECTORS.Step_Activity).on('click', ThongTin.SELECTORS.Btn_Show_Activity_Infor, function () {
            var id = $(this).attr('data-id');
            var directoryActivityId = $(this).attr('data-directoryActivity');
            ThongTin.GetFileByActivityId(id);

            var lstProperties = ThongTin.GetActivityDirectoryProperties(directoryActivityId);
            if (lstProperties != null) {
                ThongTin.GLOBAL.lstTypeFile = [];
                let type = lstProperties.listProperties.filter(x => x.typeProperties == "file");
                $.each(type, function (i, obj) {
                    ThongTin.GLOBAL.lstTypeFile.push({ key: obj.codeProperties, value: obj.nameProperties });
                });
                $(ThongTin.SELECTORS.Modal_Show_Activity_Info + ' .li-tab').removeClass('active');
                $(ThongTin.SELECTORS.Modal_Show_Activity_Info + ' .li-tab').first().click();
                ThongTin.GetDetailActivityInfo(id, lstProperties.listProperties);
            }
        });

        // chuyển tab
        $(ThongTin.SELECTORS.Modal_Show_Activity_Info).on('click', '.li-tab', function () {
            var id = $(this).attr('data-id');
            $(ThongTin.SELECTORS.Modal_Show_Activity_Info + ' .li-tab').removeClass('active');
            $(this).addClass('active');
            $(".div-hoat-dong-body").addClass('hidden');
            $(`#${id}`).removeClass('hidden');
        })

        $(ThongTin.SELECTORS.Modal_Show_Activity_Info).on('click', ThongTin.SELECTORS.item_file_place, function () {
            var id = $(this).attr("data-value");
            $(ThongTin.SELECTORS.item_file_place).removeClass("activefile");
            $(this).addClass("activefile");
            $(ThongTin.SELECTORS.item_upload_file).css("display", "none");
            if (id == "image") {
                $(ThongTin.SELECTORS.item_upload_file).css("display", "block");
                $(".search-newfile").hide();
            } else {
                $(".search-newfile").show();
            }
            ThongTin.updateTable(id);
        });

        // ****step info***
        // export thong tin
        $(ThongTin.SELECTORS.btnExportfile).on("click", function () {
            setTimeout(function () {
                var demo = 'id=' + ThongTin.GLOBAL.idMainObject;
                var a = '/api/HTKT/ManagementData/export-object?' + demo;
                window.location.href = a;
            }, 100);
        });

        // ****step document****
        $(ThongTin.SELECTORS.listFileUpload).on('click', ThongTin.SELECTORS.div_file, function () {
            $(ThongTin.SELECTORS.div_file).removeClass('file-active');
            $(this).addClass('file-active');
        });

        // xem file
        $(ThongTin.SELECTORS.btnView).on('click', function () {
            let url = $(ThongTin.SELECTORS.fileActive).parent().attr('data-url');
            if (url != undefined) {
                let array = url.split('.');
                let typelink = array[array.length - 1].toLowerCase();
                if (typelink.includes('doc') || typelink.includes('ppt') || typelink.includes('xls')) {
                    let url2 = encodeURIComponent(url);
                    window.open("https://view.officeapps.live.com/op/embed.aspx?src=" + url2 + "&embedded=true", '_blank');
                } else {
                    if (typelink.includes('pdf')) {
                        window.open(url, '_blank');
                    } else {
                        swal({
                            title: "Thông báo",
                            text: "Định dạng file này không thể xem. Vui lòng tải xuống!",
                            icon: "warning",
                            button: "Đóng",
                        }).then((value) => {

                        });
                    }
                }
            }
        });
        // tai file
        $(ThongTin.SELECTORS.btnDownload).on('click', function () {
            let url = $(ThongTin.SELECTORS.fileActive).parent().attr('data-url');
            if (url != undefined) {
                let array = url.split('.');
                let typelink = array[array.length - 1].toLowerCase();
                if (typelink.includes('pdf')) {
                    var arrayurl = url.split('/');
                    ThongTin.downloadFilePDF(url, arrayurl[arrayurl.length - 1]);
                } else {
                    window.open(url, '_blank');
                }
            }
        });
        //click map
        let eventClickData = map.data.addListener("click", (args) => {
            if (!ReferenceContent.CONSTS.isCheckReference) {
                if (exploit.GLOBAL.TypeDraw == -1 && TimKiem.GLOBAL.isDrawSearch == -1 && !exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                    ThongTin.GLOBAL.MarkerFocus = null;
                    ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                    ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ

                    // hủy bỏ highlight object3d trước đó
                    var oldObj3d = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
                    if (oldObj3d != undefined) {
                        if (oldObj3d.object != null && oldObj3d.object != undefined) {
                            oldObj3d.object.setSelected(false);
                        }
                    }

                    ThongTin.highlightObject(args); // highlight đối tượng 2d

                    ThongTin.GLOBAL.idMainObject = args.feature.getId();

                    // highlight 3d
                    var obj3d = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
                    if (obj3d != undefined) {
                        if (obj3d.object != null && obj3d.object != undefined) {
                            obj3d.object.setSelected(true);
                        }
                    }

                    ThongTin.getMainObject(ThongTin.GLOBAL.idMainObject);
                    ThongTin.showHideViewProperty(true);
                    ThongTin.GLOBAL.idDirectory = ThongTin.getIdDirectoryByMainObjectId(ThongTin.GLOBAL.idMainObject);
                } else {
                    // do diện tích, lấy tọa độ, đo đường thẳng
                    exploit.ChoosenTypeDraw(args);
                }
            }
            else {
                ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ
                //ThongTin.highlightObject(args);
                ReferenceContent.checkClickObjectReference(args);
            }
        });

        let clickEvent = map.addListener("click", (args) => {
            var userData = args.marker.getUserData();
            //check trạng thái tham chiếu
            if (!ReferenceContent.CONSTS.isCheckReference) {
                // check trạng thái vẽ, trạng thái search, trạng thái hover
                if (exploit.GLOBAL.TypeDraw == -1 && TimKiem.GLOBAL.isDrawSearch == -1 && !exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                    ThongTin.clearHighlightPoint(); // hủy bỏ highlight marker trên bản đồ
                    ThongTin.clearHighlight(); // hủy bỏ highlight geojson trên bản đồ

                    // hủy bỏ highlight object3d trước đó
                    var oldObj3d = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
                    if (oldObj3d != undefined) {
                        if (oldObj3d.object != null && oldObj3d.object != undefined) {
                            oldObj3d.object.setSelected(false);
                        }
                    }

                  
                    if (userData != undefined) {
                        ThongTin.GLOBAL.idMainObject = userData.id;
                        if (ThongTin.GLOBAL.idMainObject != undefined) {
                            ThongTin.getMainObject(ThongTin.GLOBAL.idMainObject);
                            ThongTin.showHideViewProperty(true);
                            ThongTin.GLOBAL.idDirectory = ThongTin.getIdDirectoryByMainObjectId(ThongTin.GLOBAL.idMainObject);

                            var objLayer = exploit.GLOBAL.listMarker.find(x => x.id == ThongTin.GLOBAL.idDirectory);
                            var objPoint = objLayer.lstMarker.filter(x => x.userData.id == ThongTin.GLOBAL.idMainObject);
                            if (objPoint != undefined) {
                                if (objPoint.length == 1) {

                                    ThongTin.highlightObjectPoint(args.marker);
                                }
                                else {
                                    ThongTin.hightlightObjectMutiplePoint(objPoint);
                                }

                                args.marker.hideInfoWindow();

                                ThongTin.GLOBAL.MarkerFocus = args.marker;
                            }

                            // highlight 3d
                            var obj3d = exploit.GLOBAL.listObject3D.find(x => x.id == ThongTin.GLOBAL.idMainObject);
                            if (obj3d != undefined) {
                                if (obj3d.object != null && obj3d.object != undefined) {
                                    obj3d.object.setSelected(true);
                                }
                            }
                        }
                    }
                }
                else {
                    if (exploit.GLOBAL.ActionToolbar.XemThongTinHover) // hover 1 doi tuong
                    {
                        if (userData != undefined) {
                            args.marker.hideInfoWindow();
                            var html = exploit.getAndShowInfo(userData.id, "point");
                            args.marker.setInfoWindow(html);
                            args.marker.showInfoWindow();
                        }
                    }
                    else {
                        // do diện tích, lấy tọa độ, đo đường thẳng
                        exploit.ChoosenTypeDraw(args);
                    }
                }
            }

        }, { marker: true });

        map.addListener("click", (args) => {
            if (ThongTin.GLOBAL.MarkerFocus != null) {
                ThongTin.GLOBAL.MarkerFocus.showInfoWindow();
            }
        })
        // search

        $(ThongTin.SELECTORS.input_search_object).on('keyup', function () {
            //$(mapdata.SELECTORS.inforData).find('.toggle-detail-property2').trigger('click');
            let value = $(this).val().trim();
            if (ThongTin.GLOBAL.delayTimerSearch !== null) clearTimeout(ThongTin.GLOBAL.delayTimerSearch);
            if (ThongTin.GLOBAL.cancelRequest !== null) ThongTin.GLOBAL.cancelRequest.abort();
            ThongTin.GLOBAL.delayTimerSearch = setTimeout(function () {
                ThongTin.resizeMenuLeft();
            }, 500); // Will do the ajax stuff after 1000 ms, or 1 s
        });

        $(ThongTin.SELECTORS.content_lst).scroll(function () {
            var $this = $(this);
            var $results = $(ThongTin.SELECTORS.content_list_object);
            var H1 = $this.scrollTop() + $this.height();
            var H2 = $results.height();
            //console.log("H1: " + H1);
            //console.log("H2: " + H2);
            if (H1 >= H2) {
                ThongTin.GLOBAL.page++;
                ThongTin.AppenedLstObject(TreeViewLayer.GLOBAL.DirectoryForcus);
                //loadResults();
            }
        });

        $(ThongTin.SELECTORS.content_list_object).on('click', '.object-item', function () {
            if (!exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                if (exploit.GLOBAL.TypeDraw != -1) // đang ở trạng thái đo chiều dài, đo diện tích, lấy tọa độ thì remove trạng thái
                {
                    exploit.CloseDraw();
                    $('.draw').prop('disabled', false); // mở lại chức năng trên menu
                    $('.draw').removeClass('disabled');
                }

                var id = $(this).attr('data-id');
                //var idDirectory = $(this).attr('data-directory');
                ThongTin.GLOBAL.idMainObject = id;

                // lấy thông tin object
                ThongTin.getMainObject(ThongTin.GLOBAL.idMainObject);

                ThongTin.showHideViewProperty(true);

                // lấy id lớp dữ liệu
                ThongTin.GLOBAL.idDirectory = ThongTin.getIdDirectoryByMainObjectId(ThongTin.GLOBAL.idMainObject);

                var idLayer = $(this).data("directory");
                ThongTin.setHighLightClickList(id, idLayer);

                if (exploit.GLOBAL.TypeDraw != -1) {
                    $(exploit.SELECTORS.close_geo_info).trigger('click');
                }
            }
        });

        $(ThongTin.SELECTORS.btn_open_list).on('click', function () {
            if (!$('#lst-Object').hasClass('open')) {
                $('#lst-Object').addClass('open');

                $(this).attr('data-original-title', 'Đóng danh sách');
            }
            else {
                $('#lst-Object').removeClass('open');
                $(this).attr('data-original-title', 'Mở danh sách');
            }
        });

        $(ThongTin.SELECTORS.btn_close_list).on('click', function () {
            $('#lst-Object').removeClass('open');
        })

        // tai file hoạt động
        $(ThongTin.SELECTORS.body_mo_rong).on('click', ThongTin.SELECTORS.downloadActivityFile, function () {
            let url = $(this).attr('data-url');
            let name = $(this).attr('data-name');
            if (url != undefined) {
                let array = url.split('.');
                let typelink = array[array.length - 1].toLowerCase();
                if (typelink.includes('pdf')) {
                    var arrayurl = url.split('/');
                    ThongTin.downloadFilePDF(url, arrayurl[arrayurl.length - 1]);
                } else {
                    if (typelink.includes('png') || typelink.includes('jpg') || typelink.includes('jpeg')) {
                        ThongTin.downloadFilePDF(url, name + "." + typelink);
                    } else {
                        window.open(url, '_blank');
                    }

                }
            }
        });

        // xem file
        $(ThongTin.SELECTORS.body_mo_rong).on('click', ThongTin.SELECTORS.viewActivityFile, function () {
            let url = $(this).attr('data-url');
            if (url != undefined) {
                let array = url.split('.');
                let typelink = array[array.length - 1].toLowerCase();
                if (typelink.includes('doc') || typelink.includes('ppt') || typelink.includes('xls')) {
                    let url2 = encodeURIComponent(url);
                    window.open("https://view.officeapps.live.com/op/embed.aspx?src=" + url2 + "&embedded=true", '_blank');
                } else {
                    if (typelink.includes('pdf')) {
                        window.open(url, '_blank');
                    } else {
                        swal({
                            title: l('Main:Notification'),
                            text: l('Main:FileCannotViewedPleaseDowload'),
                            icon: "warning",
                            button: l('Main:Close'),
                        }).then((value) => {

                        });
                    }
                }
            }
        });

        // slide image event
        $(ThongTin.SELECTORS.Modal_View_Image + ' a[data-bs-slide="prev"]').click(function () {
            $('#carouselExampleIndicators').carousel('prev');
        });

        $(ThongTin.SELECTORS.Modal_View_Image + ' a[data-bs-slide="next"]').click(function () {
            $('#carouselExampleIndicators').carousel('next');
        });
    },
    // step infor
    getMainObject: function (id) {
        //id = ThongTin.GLOBAL.idMainObject;
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: ThongTin.CONSTS.URL_AJAXGETDATA + '?id=' + id,
            success: function (data) {
                if (data.code == "ok") {
                    $(ThongTin.SELECTORS.Item_Menu_Right).first().trigger('click');
                    ThongTin.GLOBAL.mainObjectSelected = data.result;
                    $('.Title-info').html('');
                    $('.Content-info').html('');
                    $('.infor-detail').html('');

                    //$(documentObject.SELECTORS.listFileUpload).html('');

                    var title = ThongTin.GLOBAL.mainObjectSelected.listProperties.find(x => x.codeProperties === "Title" && x.typeSystem === 2);
                    if (typeof title !== "undefined") {
                        $('.Title-info').html(title.defalutValue);
                    }
                    var content = ThongTin.GLOBAL.mainObjectSelected.listProperties.find(x => x.codeProperties === "Description" && x.typeSystem === 2);
                    if (typeof content !== "undefined") {
                        $('.Content-info').html(content.defalutValue);
                    }

                    var name = ThongTin.GLOBAL.mainObjectSelected.listProperties.find(x => x.codeProperties === "Name" && x.typeSystem === 2);
                    if (typeof name !== "undefined") {
                        $('.Name-info').html(name.defalutValue);
                    }

                    var lst = TreeViewLayer.GetPropertiesDirectory(ThongTin.GLOBAL.mainObjectSelected.idDirectory);
                    if (lst != null) {
                        lst = lst.listProperties.filter(x => x.typeSystem != 1 && x.codeProperties != "Name" && x.codeProperties !== "Title" && x.codeProperties !== "Description");
                    }
                    else {
                        lst = ThongTin.GLOBAL.mainObjectSelected.listProperties.filter(x => x.typeSystem != 1 && (x.codeProperties !== "Name") && (x.codeProperties !== "Title") && (x.codeProperties !== "Description"));
                    }
                    //let demlist = 1;
                    let statusfile = false;

                    // lưu danh sách thư mục file của đối tượng
                    ThongTin.GLOBAL.lstFileMainObject = [];
                    var arrayFile = lst.filter(x => x.typeSystem != 1 && x.typeProperties == "file");
                    $.each(arrayFile, function (indexFile, objFile) {
                        ThongTin.GLOBAL.lstFileMainObject.push({ name: objFile.nameProperties, code: objFile.codeProperties });
                    });

                    $.each(lst, function (i, obj) {
                        if (obj.typeSystem != 1) {
                            if (obj.typeProperties != "geojson" && obj.typeProperties != "maptile") {
                                var property = ThongTin.GLOBAL.mainObjectSelected.listProperties.find(x => x.codeProperties == obj.codeProperties);
                                if (property != undefined) {
                                    /* obj.defalutValue = property.defalutValue;*/
                                    if ((obj.typeProperties == "list" || obj.typeProperties == "radiobutton" || obj.typeProperties == "checkbox") && obj.isShow) {
                                        var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
                                        var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách
                                        var valuePropertyView = ""; // value hiển thị
                                        try {
                                            lstValueDefault = JSON.parse(obj.defalutValue);
                                        }
                                        catch (e) { }
                                        if (lstValueDefault.length > 0) {
                                            if (property.defalutValue != "" && property.defalutValue != undefined && property.defalutValue != null) {
                                                lstValueObjectMain = property.defalutValue.split(',');
                                            }
                                            var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng
                                            valuePropertyView = lstMapValue.map(x => x.name).join(', ');
                                        }
                                        else {
                                            valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
                                        }
                                        let text = `<div class="form-group col-md-12 form-common-infor">
                                                <div class="row">
                                                    <label class="col-md-4">${obj.nameProperties}:</label>
                                                    <span class="col-md-8 text-left"><span class="Content-info">${valuePropertyView}</span></span>
                                                </div>
                                            </div>`;
                                        $('.infor-detail').append(text);
                                        return;

                                    }
                                    else {
                                        obj.defalutValue = property.defalutValue;
                                    }

                                    if (obj.isShow) {
                                        if (obj.typeProperties != "geojson") {
                                            var text = '';
                                            switch (obj.typeProperties) {
                                                case "image":
                                                    ThongTin.GLOBAL.ArrayImageMainObject = [];
                                                    var value = '';
                                                    if (obj.defalutValue !== null && obj.defalutValue !== '') {
                                                        var lstImage = JSON.parse(obj.defalutValue);
                                                        if (lstImage != null && $.isArray(lstImage)) {
                                                            if (lstImage.length > 0) {
                                                                ThongTin.GLOBAL.ArrayImageMainObject = lstImage;

                                                                value = `<img src="${lstImage[0].url}" 
                                                                    id="img-icon-2d" height="100" style="width: 70%;border: 5px solid  #EDEDED;" onerror="this.onerror = null; this.src = /images/anhMacDinh.png;">
                                                                <a class="btn btn-xs btn-primary btn-view-image" title="${l('Main:SeeAllPicture')}" style="float:right;"><i class="fa fa-search"></i></a>`;
                                                            }

                                                            text = `<div class="form-group col-md-12 form-common-infor">
                                                        <div class="row">
                                                            <label class="col-md-4">${obj.nameProperties}:</label>
                                                            <span class="col-md-8"><span class="Content-info">${value}</span></span>
                                                        </div>
                                                    </div>`;
                                                        }
                                                    }
                                                    break;
                                                case "link":
                                                    var value = '';
                                                    if (obj.defalutValue !== null && obj.defalutValue !== '') {
                                                        var lstLink = JSON.parse(obj.defalutValue);
                                                        if (lstLink != null && $.isArray(lstLink)) {
                                                            if (lstLink.length > 0) {
                                                                value = `<a href="${lstLink[0].url}" class="" style="overflow-wrap: anywhere;" target="_blank" title="${l('Main:OpenLink')}">${lstLink[0].url}</a>`;
                                                            }

                                                            text = `<div class="form-group col-md-12 form-common-infor">
                                                    <div class="row">
                                                        <label class="col-md-4">${obj.nameProperties}:</label>
                                                        <span class="col-md-8 text-left"><span class="Content-info">${value}</span></span>
                                                    </div>
                                                </div>`;
                                                        }
                                                    }
                                                    break;
                                                case "file":
                                                    //ThongTin.GLOBAL.lstFileMainObject.push({ name: obj.nameProperties, code: obj.codeProperties });
                                                    break;
                                                case "text":
                                                    text += `<div class="form-group col-md-12 form-common-infor">
                                                            <div class="row">
                                                                <label class="col-md-4">${obj.nameProperties}:</label>
                                                                <span class="col-md-12" style="padding:0;"><span class="Content-info">${obj.defalutValue != null ? obj.defalutValue : ""}</span></span>
                                                            </div>
                                                        </div>`;
                                                    break;
                                                case "stringlarge":
                                                    text += `<div class="form-group col-md-12 form-common-infor">
                                                            <div class="row">
                                                                <label class="col-md-4">${obj.nameProperties}:</label>
                                                                <span class="col-md-12" style="padding:0;"><span class="Content-info">${obj.defalutValue != null ? obj.defalutValue : ""}</span></span>
                                                            </div>
                                                        </div>`;
                                                    break;

                                                case "bool":
                                                    text = `<div class="form-group col-md-12 form-common-infor">
                                                            <div class="row">
                                                                <label class="col-md-4">${obj.nameProperties}:</label>
                                                                <span class="col-md-8 text-left"><span class="Content-info">${obj.defalutValue != null ? obj.defalutValue == "true" ? "Có" : "Không" : ""}</span></span>
                                                            </div>
                                                        </div>`;
                                                    break;

                                                default:
                                                    text = `<div class="form-group col-md-12 form-common-infor">
                                                            <div class="row">
                                                                <label class="col-md-4">${obj.nameProperties}:</label>
                                                                <span class="col-md-8 text-left"><span class="Content-info">${obj.defalutValue != null ? obj.defalutValue : ""}</span></span>
                                                            </div>
                                                        </div>`;
                                                    break;
                                            }

                                            $('.infor-detail').append(text);
                                        }
                                    }
                                }
                                else {
                                    let text = `<div class="form-group col-md-12 form-common-infor">
                                                <div class="row">
                                                    <label class="col-md-4">${obj.nameProperties}:</label>
                                                    <span class="col-md-8 text-left"><span class="Content-info"></span></span>
                                                </div>
                                            </div>`;
                                    $('.infor-detail').append(text);
                                }
                            }
                            
                        }

                    });

                    if (statusfile) {
                        documentObject.GetFileByObjectMainId(ThongTin.GLOBAL.idMainObject, documentObject.showFileEdit);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    eventSildeImage: function () {
        $(ThongTin.SELECTORS.Step_Info).on("click", ThongTin.SELECTORS.Btn_View_Image, function () {
            if (ThongTin.GLOBAL.ArrayImageMainObject.length > 0) {
                ThongTin.addImageSilde();
                $(ThongTin.SELECTORS.Modal_View_Image).modal("show");
            }
        });

        $('#myModalActivity').on("click", ThongTin.SELECTORS.imageInfor, function () {
            ThongTin.addImageSilde();
            $(ThongTin.SELECTORS.Modal_View_Image).modal("show");
        });
    },
    addImageSilde: function () {
        let html1 = "";
        var lstImage = [];
        var active = $(`${ThongTin.SELECTORS.Item_Menu_Right}.active`).attr('data-li');
        if (active == "step-action") {
            lstImage = ThongTin.GLOBAL.listObjectImageActivity;
        }
        else {
            lstImage = ThongTin.GLOBAL.ArrayImageMainObject;
        }

        $.each(lstImage, function (i, obj) {
            let active = (i == 0) ? "active" : "";
            html1 += '<div class="carousel-item ' + active + '"><img src = "' + obj.url + '" data-holder-rendered="true"></div>';
        });
        $(ThongTin.SELECTORS.CarouselIndicatorsViewImage).children().remove();
        $(ThongTin.SELECTORS.CarouselIndicatorsViewImage).append(html1);
    },
    // step activity
    GetDanhSachHoatDong: function (idMainObject, idDirectory) {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: true,
            url: ThongTin.CONSTS.URL_DANH_SACH_HOAT_DONG,
            data: {
                idMainObject: idMainObject,
                idDirectory: idDirectory
            },
            success: function (data) {
                if (data.code == "ok") {
                    ThongTin.GLOBAL.ArrayActivityDetail = [];
                    if (data.result.items.length == 0) {

                        $(`${ThongTin.SELECTORS.table} tbody`).html(` <tr>
                            <td colspan="4">${l('Main:NoData')}</td>
                            </tr>`);

                    } else {
                        ThongTin.GLOBAL.ArrayActivityDetail = data.result.items;
                        var lstActive = data.result.items;
                        ArrayActivityDetail = data.result.items;
                        var html = ``;
                        for (var i = 0; i < lstActive.length; i++) {
                            html += `<tr>
                                    <td>${i + 1}</td>
                                    <td>${lstActive[i].nameDirectoryActivity}</td>
                                    <td>${moment(lstActive[i].dateActivity).format('DD/MM/YYYY')}</td>
                                    <td><button class="show-detail-activity-info btn btn-xs btn-primary" title="${l('Main:View')}" data-directoryActivity="${lstActive[i].idDirectoryActivity}" data-id="${lstActive[i].id}" type="button"><i class="fa fa-info"></i></button></td>
                                </tr>`;
                        }

                        $(`${ThongTin.SELECTORS.table} tbody`).html(html);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    AddTableDanhSachHoatDong: function () {
        var group_to_values = ThongTin.GLOBAL.ArrayActivityDetail.reduce(function (obj, item) {
            obj[item.nameTypeDirectoryActivity] = obj[item.nameTypeDirectoryActivity] || [];
            obj[item.nameTypeDirectoryActivity].push(item);
            return obj;
        }, {});

        var groups = Object.keys(group_to_values).map(function (key) {
            return { group: key, id: group_to_values[key][0].idDirectoryActivity, data: group_to_values[key] };
        });

        var html = `<input type="file" class="hidden" id="import-file" accept=".xlsx, .xls" />`;
        $('#danh-sach-chi-tiet-hoat-dong').html(html);

        //ListProperties
        for (var i = 0; i < groups.length; i++) {
            html += `<div style="margin: 8px 0px; display:flex;justify-content:space-between;">
                        <h4>${i + 1}. ${groups[i].group}</h4>
                       
                        </div>`;
            var th = `<th style="width: 5%;">${l('Main:No.')}</th>
                    <th style="width: 15%;">${l('Main:OperationName')}</th>
                    <th style="width: 10%;">${l('Main:Date')}</th>`;
            var td = ``;
            var exitsTh = [];
            var exitsCode = []; // chỉ hiển thị những thuộc tính theo properties của layer

            var directoryActivity = ThongTin.GLOBAL.ArrayDirectoryActivity.find(x => x.id == groups[i].id);
            if (directoryActivity != undefined) {
                directoryActivity.listProperties = directoryActivity.listProperties.filter(x => x.typeSystem != 1
                    && x.typeProperties != "image" && x.typeProperties != "file" && x.typeProperties != "link" && x.typeProperties != "geojson" && x.typeProperties != "maptile");

                for (var j = 0; j < directoryActivity.listProperties.length; j++) {
                    if (jQuery.inArray(directoryActivity.listProperties[j].nameProperties, exitsTh) == -1) {
                        if (directoryActivity.listProperties[j].isShow) // kiểm tra hiển thị thuộc tính
                        {
                            th += `<th>${directoryActivity.listProperties[j].nameProperties}</th>`;
                            exitsTh.push(directoryActivity.listProperties[j].nameProperties);
                            exitsCode.push(directoryActivity.listProperties[j].codeProperties);
                        }
                    }
                }

                var lstDetailActivity = groups[i].data;
                for (var j = 0; j < lstDetailActivity.length; j++) {
                    td += `<tr><td class="first-col">${k = j + 1}</td>
                          <td class="item-col">${lstDetailActivity[j].nameDirectoryActivity}</td>
                          <td class="item-col">${moment(lstDetailActivity[j].dateActivity).format('DD/MM/YYYY')}</td>`;

                    for (var k = 0; k < directoryActivity.listProperties.length; k++) {
                        var checkExits = lstDetailActivity[j].listProperties.find(x => x.codeProperties == directoryActivity.listProperties[k].codeProperties);
                        var value = "";
                        if (checkExits != undefined) {
                            if (checkExits.typeProperties == "checkbox" || checkExits.typeProperties == "list" || checkExits.typeProperties == "radiobutton") {

                                try {
                                    var lstItems = JSON.parse(checkExits.defalutValue);

                                    if (lstItems != undefined) {
                                        if (checkExits.typeProperties != "checkbox") {
                                            var valueItem = lstItems.find(x => x.checked == true);

                                            if (valueItem != null) {
                                                value = valueItem.name;
                                            }
                                        }
                                        else {
                                            var string = "";
                                            $.each(lstvalue, function (i, item) {
                                                if (item.checked) {
                                                    string += "," + item.name;
                                                }
                                            });

                                            value = string;
                                        }

                                    }
                                }
                                catch (e) {
                                    var danhSachValue = JSON.parse(directoryActivity.listProperties[k].defalutValue);

                                    if (checkExits.typeProperties != "checkbox") {
                                        var valueItem = danhSachValue.find(x => x.code == checkExits.defalutValue);

                                        if (valueItem != undefined) {
                                            value = valueItem.name;
                                        }
                                    }
                                    else {
                                        var lstCheck = [];
                                        if (checkExits.defalutValue != "" && checkExits.defalutValue != null) {
                                            lstCheck = checkExits.defalutValue.split(",");
                                        }

                                        var arrayCheckboxObject = danhSachValue.filter(x => lstCheck.indexOf(x.code) > -1)
                                        if (arrayCheckboxObject.length > 0) {
                                            value = arrayCheckboxObject.map(x => x.name).toString();
                                        }
                                    }
                                }
                            }
                            else {
                                value = checkExits.defalutValue;
                            }

                            if (exitsCode.includes(directoryActivity.listProperties[k].codeProperties)) // kiểm tra thuộc tính của đối tượng có nằm trong thuộc tính của layer hay ko
                            {
                                if (checkExits.typeProperties == "date") {
                                    td += `<td class="item-col">${value}</td>`;
                                } else {
                                    td += `<td class="item-col">${value}</td>`;
                                }
                            }
                        }
                        else {
                            if (exitsCode.includes(directoryActivity.listProperties[k].codeProperties)) // kiểm tra thuộc tính của đối tượng có nằm trong thuộc tính của layer hay ko
                            {
                                td += `<td class="item-col"></td>`;
                            }
                        }
                    }


                    td += `<td class="second-col">
                            <button class="show-detail-activity-info btn btn-xs btn-primary" title="${l('Main:View')}" data-directoryActivity="${groups[i].id}" data-id="${groups[i].data[j].id}" type="button"><i class="fa fa-info"></i></button>
                       </td></tr>`;
                }

            }

            th += `<th style="width: 80px;">${l('Main:Action')}</th>`;

            var trHead = `<tr>${th}</tr>`;
            var thead = `<thead>${trHead}</thead>`;
            //var trBody = `<tr>${td}</tr>`;
            var tbody = `<tbody>${td}</tbody>`;
            html += `<table class="table">
                            ${thead}
                            ${tbody}
                        </table>`;
        }

        $('.danh-sach-chi-tiet-hoat-dong').html(html);
    },
    GetDirectoryActivity: function (id) {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: true,
            url: ThongTin.CONSTS.URL_DANH_SACH_LOAI_HOAT_DONG,
            data: {
                idDirectory: id
            },
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    ThongTin.GLOBAL.ArrayDirectoryActivity = data;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

    },
    // lấy thuộc tính của loại hoạt động
    GetActivityDirectoryProperties: function (id) {
        var result = null;
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: ThongTin.CONSTS.URL_GET_PROPERTIES_ACTIVITY_DIRECTORY,
            data: {
                id: id,
            },
            success: function (res) {
                if (res.code == "ok") {
                    var data = res.result;
                    if (data != null) {
                        result = data;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

        return result;
    },
    GetDetailActivityInfo: function (id, lstProperties) {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: true,
            url: ThongTin.CONSTS.URL_GET_DETAIL_ACTIVITY + "/" + id,
            success: function (data) {
                if (data != null) {
                    ThongTin.AddElementDetailActivityInfor(data, lstProperties);
                    $(ThongTin.SELECTORS.Modal_Show_Activity_Info).modal('show');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

    },
    AddElementDetailActivityInfor: function (data, lstProperties) {
        $(ThongTin.SELECTORS.body_thong_tin).html("");
        var directoryActivity = data;
        if (directoryActivity != undefined) {
            var result = [];

            $(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} .modal-body .nav-item`).eq(0).children().addClass('active');
            $(ThongTin.SELECTORS.body_thong_tin).removeClass('hidden'); // hiện thị nội dung nhập thông tin
            $(ThongTin.SELECTORS.body_mo_rong).addClass('hidden');
            if (directoryActivity.listProperties == null) {
                directoryActivity.listProperties = [];
            }
            var checkExits = lstProperties.filter(x => x.typeProperties == "image" || x.typeProperties == "link" || x.typeProperties == "file");
            if (checkExits.length == 0) {
                $(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} .modal-body .nav-item`).eq(1).addClass('hidden');
            }
            else {
                $(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} .modal-body .nav-item`).eq(1).removeClass('hidden');
            }

            var textDefault = `<div class="row form-group" style="margin-bottom: 0;">
                                        <label class="control-label col-md-3 col-xs-12">${l('Main:OperationName')}:</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-modal">
                                                    <span>${data.nameDirectoryActivity}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row form-group" style="margin-bottom: 0;">
                                        <label class="control-label col-md-3 col-xs-12">${l('Main:OperationDate')}:</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-modal">
                                                   <span>${moment(data.dateActivity).format('DD/MM/YYYY')}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
            $(ThongTin.SELECTORS.body_thong_tin).append(textDefault);

            //let statusimage = false;
            let statuslink = false;
            //if (activityDetailCurrent != null) {
            //    lstProperties = activityDetailCurrent.listProperties;
            //}

            $.each(lstProperties, function (i, obj) {
                if (obj.typeSystem !== 1) {
                    if (data != null) {
                        var currentProperties = data.listProperties.find(x => x.codeProperties == obj.codeProperties);
                        if (currentProperties != null && currentProperties != undefined) {
                            //obj.codeProperties = currentProperties.codeProperties;
                            //obj.nameProperties = currentProperties.nameProperties;
                            //obj.typeProperties = currentProperties.typeProperties;
                            //obj.isShow = currentProperties.isShow;
                            //obj.isView = currentProperties.isView;
                            //obj.orderProperties = currentProperties.orderProperties;
                            //obj.typeSystem = currentProperties.typeSystem;

                            if (currentProperties.typeProperties == "list" || currentProperties.typeProperties == "checkbox" || currentProperties.typeProperties == "radiobutton") {
                                var valueCurrent = "";
                                try {
                                    var PropertiCurent = JSON.parse(currentProperties.defalutValue); // chuyển json string về type list
                                    if (PropertiCurent != undefined && $.isArray(PropertiCurent)) // kiểm tra null và biến chuyển về có phải là array hay ko? nếu đúng thì kiểm tra value 
                                    {
                                        if (currentProperties.typeProperties == "checkbox") {
                                            var valuePropertiCurentCB = PropertiCurent.filter(x => x.checked == true); // lay value hien tai
                                            if (valuePropertiCurentCB != undefined && valuePropertiCurentCB.length > 0) {
                                                valueCurrent = valuePropertiCurentCB.map(x => x.code);
                                            }
                                        }
                                        else {
                                            var valuePropertiCurent = PropertiCurent.find(x => x.checked == true); // lay value hien tai
                                            if (valuePropertiCurent != undefined) {
                                                valueCurrent = valuePropertiCurent.code;
                                            }
                                        }
                                    }
                                    else {
                                        valueCurrent = currentProperties.defalutValue;
                                    }

                                }
                                catch (e) {
                                    valueCurrent = currentProperties.defalutValue;
                                }

                                if (valueCurrent != "") {
                                    // set value vao danh sach hien thi
                                    var lstItem = JSON.parse(obj.defalutValue);
                                    for (var k = 0; k < lstItem.length; k++) {
                                        if (currentProperties.typeProperties != "checkbox") {
                                            lstItem[k].checked = false;
                                            if (lstItem[k].code == valueCurrent) {
                                                lstItem[k].checked = true;
                                            }
                                        }
                                        else {
                                            var lstValuecheckbox = valueCurrent.split(",");

                                            $.each(lstItem, function (p, item) {
                                                if (lstValuecheckbox.indexOf(item.code) > -1) {
                                                    item.checked = true;
                                                }
                                            });
                                        }
                                    }

                                    obj.defalutValue = JSON.stringify(lstItem);
                                }
                            }
                            else {
                                obj.defalutValue = currentProperties.defalutValue;
                            }
                        }
                    }

                    var element = {
                        NameProperties: obj.nameProperties,
                        CodeProperties: obj.codeProperties,
                        TypeProperties: obj.typeProperties,
                        IsShow: obj.isShow,
                        IsIndexing: obj.isIndexing,
                        IsRequest: obj.isRequest,
                        IsView: obj.isView,
                        IsHistory: obj.isHistory,
                        IsInheritance: obj.isInheritance,
                        IsAsync: obj.isAsync,
                        DefalutValue: obj.defalutValue,
                        ShortDescription: obj.shortDescription,
                        TypeSystem: obj.typeSystem,
                        OrderProperties: obj.orderProperties
                    };

                    result.push(element);

                    if (element.TypeProperties == "image") {
                        if (element.DefalutValue != "") {
                            ThongTin.GLOBAL.listObjectImageActivity = JSON.parse(element.DefalutValue);
                        }
                        else {
                            ThongTin.GLOBAL.listObjectImageActivity = [];
                        }
                        //$('.class-extend').removeClass('hidden');
                        $(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} ${ThongTin.SELECTORS.body_mo_rong} .item-FilePlace`).eq(0).css('display', 'block');

                    }
                    if (element.TypeProperties == "link") {
                        if (element.DefalutValue != "") {
                            ThongTin.GLOBAL.listObjectLinkCameraActivity = JSON.parse(element.DefalutValue);
                        }
                        else {
                            ThongTin.GLOBAL.listObjectLinkCameraActivity = [];
                        }
                        //$('.class-extend').removeClass('hidden');
                        $(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} ${ThongTin.SELECTORS.body_mo_rong} .item-FilePlace`).eq(1).css('display', 'block');

                        //$('.step-extend').css('display', 'block');
                    }

                    if (element.TypeProperties == "file") {
                        //if (element.DefalutValue != "") {
                        //    ThongTin.GLOBAL.listObjectFileActivity = JSON.parse(element.DefalutValue);
                        //}
                        //else {
                        //    ThongTin.GLOBAL.listObjectFileActivity = [];
                        //}
                        //$('.class-extend').removeClass('hidden');
                        $(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} ${ThongTin.SELECTORS.body_mo_rong} .item-FilePlace`).eq(2).css('display', 'block');

                    }
                    if (element.TypeProperties != "geojson" && element.TypeProperties != "image"
                        && element.TypeProperties != "link" && element.TypeProperties != "maptile" && element.TypeProperties != "file") {
                        //var text = ThongTin.selectInput(element);
                        var text = ThongTin.selectInfoActive(element);
                        $(ThongTin.SELECTORS.body_thong_tin).append(text);
                    }

                }
            });

            $(`.item-FilePlace`).each(function () {
                var n = $(this).css("display");
                if (n == "none") {
                    $(this).remove();
                };
            });
            if ($(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} ${ThongTin.SELECTORS.body_mo_rong} .item-FilePlace`).length > 0) {
                $(`${ThongTin.SELECTORS.Modal_Show_Activity_Info} ${ThongTin.SELECTORS.body_mo_rong} .item-FilePlace`).eq(0).trigger('click');
            }
            //ThongTin.eventSildeImage(ThongTin.GLOBAL.listObjectImageActivity);
        }
    },
    selectInput: function (type) {
        var requried = (type.IsRequest) ? "required" : "";
        //if (type.CodeProperties == 'Title') requried = "required";
        var text = `<div class="row form-group" style="margin-bottom: 0;">
                        <label class="control-label col-md-3 col-xs-12">${type.NameProperties}</label>
                        <div class="col-md-9 col-xs-12">
                            <div class="col-sm-12">
                                <div class="form-group form-group-modal">`;
        //<input type="text" id="text-name" class="form-control form-style" placeholder="Tên kiểu" />
        switch (type.TypeProperties) {
            case 'bool':
                if (type.DefalutValue || type.DefalutValue == "true") {
                    text += `<input disabled type="checkbox" data-type="bool" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" class="property-activity-input form-style" checked="true">`;
                } else {
                    text += `<input disabled type="checkbox" data-type="bool" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" class="property-activity-input form-style">`;
                }
                break;
            case 'text':
                text += `<textarea disabled type="text" data-type="text" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" >${type.DefalutValue}</textarea>`;
                break;
            case 'float':
                text += `<input disabled type="number" data-type="float" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'int':
                text += `<input disabled type="number" data-type="int" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'list':
                var lst = JSON.parse(type.DefalutValue);
                text += `<select id="text-modal-${type.CodeProperties}" class="form-control" disabled>`;
                $.each(lst, function (i, obj) {
                    text += `<option ${obj.checked ? "selected" : ""} value="${obj.code}">${obj.name}</option>`;
                });
                text += ` </select>`;
                //text += `<input type="text" data-type="list" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control modal-layer form-style" placeholder="" />`;
                break;
            case 'date':
                text += `<input disabled type="text" data-type="date" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'string':
                text += `<input disabled type="text" data-type="string" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}"class="form-control property-activity-input form-style" placeholder="" />`;
                break;
            case 'stringlarge':
                text += `<textarea disabled type="text" data-type="stringlarge" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" ></textarea>`;
                break;
            case 'geojson':
                text += `<textarea disabled type="text" data-type="geojson" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" ></textarea>`;
                break;
            case 'checkbox':
                var lst2 = JSON.parse(type.DefalutValue);
                $.each(lst2, function (i, obj) {
                    text += `<label class="col-sm-6">
                                <input disabled type="checkbox" name="text-modal-checkbox" data-name="${obj.name} " class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                             </label>`;
                });
                break;
            case 'radiobutton':
                var lst3 = JSON.parse(type.DefalutValue);
                $.each(lst3, function (i, obj) {
                    text += `<label class="col-sm-6">
                                <input disabled type="radio" name="text-modal-radio" data-name="${obj.name} " class="text-modal-${type.CodeProperties}" ${obj.checked ? "checked='checked'" : ""} value="${obj.code}" style="margin: auto 5px;">${obj.name} 
                             </label>`;
                });
                break;
            default:
                return text = '';
            //break;
        }
        text += `                            </div>
                            </div>
                        </div>
                    </div>`;
        return text;
    },
    selectInfoActive: function (type) {
        var requried = (type.IsRequest) ? "required" : "";
        //if (type.CodeProperties == 'Title') requried = "required";
        var text = `<div class="row form-group" style="margin-bottom: 0;">
                        <label class="control-label col-md-3 col-xs-12">${type.NameProperties}:</label>
                        <div class="col-md-9 col-xs-12">
                            <div class="col-sm-12">
                                <div class="form-group form-group-modal">`;
        //<input type="text" id="text-name" class="form-control form-style" placeholder="Tên kiểu" />
        switch (type.TypeProperties) {
            case 'bool':
                if (type.DefalutValue || type.DefalutValue == "true") {
                    text += `<span>${type.DefalutValue}</span>`;
                }
                break;
            case 'list':
                try {
                    var lst = JSON.parse(type.DefalutValue);
                    if (lst != null && lst != undefined && $.isArray(lst)) {
                        text += lst.filter(x => x.checked).map(y => y.name).toString();
                    }
                } catch (e) {
                    text += '';
                }
                break;
            case 'geojson':
                text += `<textarea disabled type="text" data-type="geojson" data-required=${type.IsRequest} id="text-modal-${type.CodeProperties}" value="${type.DefalutValue}" class="form-control property-activity-input form-style" placeholder="" ></textarea>`;
                break;
            case 'checkbox':
                try {
                    var lst2 = JSON.parse(type.DefalutValue);
                    if (lst2 != null && lst2 != undefined && $.isArray(lst2)) {
                        text += lst2.filter(x => x.checked).map(y => y.name).toString();
                    }
                }
                catch (e) {
                    text += '';
                }
                break;
            case 'radiobutton':
                try {
                    var lst3 = JSON.parse(type.DefalutValue);
                    if (lst3 != null && lst3 != undefined && $.isArray(lst3)) {
                        text += lst3.filter(x => x.checked).map(y => y.name).toString();
                    }
                }
                catch (e) {
                    text += '';
                }
                break;
            default:
                text += `<span>${type.DefalutValue}</span>`;
            //break;
        }
        text += `                            </div>
                            </div>
                        </div>
                    </div>`;
        return text;
    },
    updateTable: function (checktext) {
        checktext = typeof checktext !== "undefined" ? checktext : $(ThongTin.SELECTORS.item_file_place_active).attr("data-value");
        var listcolumn = [];
        if (checktext == "image") {
            listcolumn = ThongTin.GLOBAL.listColumnImage;
            if (ThongTin.GLOBAL.listObjectImageActivity !== null && ThongTin.GLOBAL.listObjectImageActivity.length > 0) {
                var test = '';
                for (var i = 0; i < ThongTin.GLOBAL.listObjectImageActivity.length; i++) {
                    test += `<tr>
                                                                <th scope="row">${i + 1}</th>
                                                                <td>${ThongTin.GLOBAL.listObjectImageActivity[i].name}</td>
                                                                <td><img src="${ThongTin.GLOBAL.listObjectImageActivity[i].url}" style="width: 100px;" class="image-infor"></td>
                                                                
                                                            </tr>`;//<td class="url">${ThongTin.GLOBAL.listObjectImageActivity[i].url}</td>
                }
                $(ThongTin.SELECTORS.tableTbody).html('');
                $(ThongTin.SELECTORS.tableTbody).html(test);

                //ThongTin.eventSildeImage(ThongTin.GLOBAL.listObjectImageActivity);

            } else {
                $(ThongTin.SELECTORS.tableTbody).html('');
                $(ThongTin.SELECTORS.tableTbody).append(`<tr>
                                                                            <th scope="row" colspan="3">${l('Main:NoData')}</th>
                                                                        </tr>`);
            }
        }
        if (checktext == "LinkCamera") {
            listcolumn = ThongTin.GLOBAL.listColumnLinkCamera;
            if (ThongTin.GLOBAL.listObjectLinkCameraActivity !== null && ThongTin.GLOBAL.listObjectLinkCameraActivity.length > 0) {
                var testCamera = '';

                for (let i = 0; i < ThongTin.GLOBAL.listObjectLinkCameraActivity.length; i++) {

                    testCamera += `<tr>
                                                                <th scope="row">${i + 1}</th>
                                                                <td>${ThongTin.GLOBAL.listObjectLinkCameraActivity[i].name}</td>
                                                                <td class="url">${ThongTin.GLOBAL.listObjectLinkCameraActivity[i].url}</td>
                                                            </tr>`;
                }

                $(ThongTin.SELECTORS.tableTbody).html('');
                $(ThongTin.SELECTORS.tableTbody).html(testCamera);
            } else {
                $(ThongTin.SELECTORS.tableTbody).html('');
                $(ThongTin.SELECTORS.tableTbody).append(`<tr>
                                                                            <th scope="row" colspan="3">${l('Main:NoData')}</th>
                                                                        </tr>`);
            }
        }

        if (checktext == "FileUpload") {
            listcolumn = ThongTin.GLOBAL.listColumnFile;
            if (ThongTin.GLOBAL.listObjectFileActivity !== null && ThongTin.GLOBAL.listObjectFileActivity.length > 0) {
                let test = '';
                for (let i = 0; i < ThongTin.GLOBAL.listObjectFileActivity.length; i++) {
                    let value = ThongTin.GLOBAL.lstTypeFile.find(x => x.key == ThongTin.GLOBAL.listObjectFileActivity[i].idDirectoryProperties);
                    test += `<tr>
                                                                <th scope="row">${i + 1}</th>
                                                                <td>${ThongTin.GLOBAL.listObjectFileActivity[i].name}</td>
                                                                <td>${(value != null && value != undefined) ? value.value : ThongTin.GLOBAL.listObjectFileActivity[i].idDirectoryProperties}</td >
                                                                <td class="url">${ThongTin.GLOBAL.listObjectFileActivity[i].url}</td>
                                                                
                                                            </tr>`;
                    $(ThongTin.SELECTORS.tableTbody).html('');
                    $(ThongTin.SELECTORS.tableTbody).html(test);
                }
            } else {
                $(ThongTin.SELECTORS.tableTbody).html('');
                $(ThongTin.SELECTORS.tableTbody).append(`<tr>
                                                                            <th scope="row" colspan="4">${l('Main:NoData')}</th>
                                                                        </tr>`);
            }
        }
        var text = `<tr>`;
        for (let i = 0; i < listcolumn.length; i++) {
            text += `<th scope="col" width="${listcolumn[i].width}%">${listcolumn[i].name}</th>`;
        }
        text += `</tr>`;
        $(ThongTin.SELECTORS.tableThead).html('');
        $(ThongTin.SELECTORS.tableThead).append(text);
    },

    //step tai lieu
    GetFileByObjectMainId: function (ObjectMainid, callback) {
        let id = ObjectMainid;

        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + "/api/HTKT/DocumentObject/get-file-by-objectId?Id=" + id,
            data: {
            },
            success: callback
        });
    },
    updateList: function (data) {
        if (data.code === "ok") {
            ThongTin.GLOBAL.lstOrder = [];
            ThongTin.GLOBAL.lstDocument = data.result;
            $(ThongTin.SELECTORS.liListFile).html('');
            //var string = ``;

            var group_to_values = ThongTin.GLOBAL.lstDocument.reduce(function (obj, item) {
                obj[item.nameFolder] = obj[item.nameFolder] || [];
                obj[item.nameFolder].push(item);
                return obj;
            }, {});

            var GroupDocument = Object.keys(group_to_values).map(function (key) {
                return { group: key, data: group_to_values[key] };
            });
            var html = ``;
            $.each(GroupDocument, function (i, obj) {
                var divTailieu = ``;
                for (var j = 0; j < obj.data.length; j++) {
                    let icon = ThongTin.getTypeUrl(obj.data[j].urlDocument);
                    divTailieu += `<div class="div-border-file" data-code-parent="${obj.data[j].nameFolder}" id="${obj.data[j].id}" data-url="${obj.data[j].urlDocument}" data-name="${obj.data[j].nameDocument}">
                                    <div class="div-file" style="text-align: left;">
                                        <i class="${icon} fa-lg"" aria-hidden="true"></i>
                                        <span style="font-weight: 900;">${obj.data[j].nameDocument}</span>
                                    </div>
                                </div>`;
                }
                let value = ThongTin.GLOBAL.lstFileMainObject.find(x => x.code == obj.group);
                html += `<div class="file-upload-content" data-code="${obj.group}">
                                <div class="div-border div-85" data-code="${obj.group}" data-name="${obj.group}">
                                    <div style="text-align: left;">
                                        <h4 style="font-weight: 600;">${i + 1}. ${(value != null && value != undefined) ? value.name : obj.group}</h4 >
                                    </div>
                                </div>
                               
                                <div class="li-list-file div-100">
                                   ${divTailieu}
                                </div>
                            </div>`;
            });

            $(ThongTin.SELECTORS.listFileUpload).html(html);
        }
    },
    getTypeUrl: function (url) {
        let string = ThongTin.GLOBAL.lstIcon.file;
        let array = url.split('.');
        let typelink = array[array.length - 1].toLowerCase();
        if (typelink.includes('doc')) {
            string = ThongTin.GLOBAL.lstIcon.word;
        } else if (typelink.includes('ppt')) {
            string = ThongTin.GLOBAL.lstIcon.ppt;
        }
        else if (typelink.includes('xls')) {
            string = ThongTin.GLOBAL.lstIcon.excel;
        } else if (typelink.includes('pdf')) {
            string = ThongTin.GLOBAL.lstIcon.pdf;
        }
        return string;
    },
    downloadFilePDF: function (url, fileName) {
        $('.spinner').css('display', 'block');
        var req = new XMLHttpRequest();
        req.open("GET", url, true);
        req.responseType = "blob";
        req.onload = function () {
            //Convert the Byte Data to BLOB object.
            var blob = new Blob([req.response], { type: "application/octetstream" });

            //Check the Browser type and download the File.
            var isIE = false || !!document.documentMode;
            if (isIE) {
                window.navigator.msSaveBlob(blob, fileName);
            } else {
                var url = window.URL || window.webkitURL;
                link = url.createObjectURL(blob);
                var a = document.createElement("a");
                a.setAttribute("download", fileName);
                a.setAttribute("href", link);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            }
            $('.spinner').css('display', 'none');
        };
        req.send();
    },
    //highligt Line, MultiLine, Polygon, MultiPolygon
    highlightObject: function (args) {
        ThongTin.clearHighlight();
        ThongTin.GLOBAL.dataObjectHover = args;
        let type = args.feature.getGeometry().getType().toLowerCase();
        let line = [];
        if (type === "multipolygon" || type === "polygon" || type === "linestring") {
            args.feature.getGeometry().forEachLatLng(function (a) {
                if (line.length > 0 && line[0][0] === a.lng && line[0][1] === a.lat) {
                    line.push([a.lng, a.lat]);
                    ThongTin.setHighlight(line);
                    line = [];
                } else {
                    line.push([a.lng, a.lat]);
                }
            });
            if (line.length > 0) ThongTin.setHighlight(line);
        } else {
            $.each(args.feature.getGeometry()._elements, function (i, obj) {
                obj.forEachLatLng(function (a) {
                    line.push([a.lng, a.lat]);
                });
                ThongTin.setHighlight(line);
                line = [];
            });
        }
    },
    highlightObjectMenuRight: function (args) {
        ThongTin.clearHighlight();
        let type = args.getGeometry().getType().toLowerCase();
        let line = [];
        var boundLatLng = [];
        var lat = 0;
        var lng = 0;

        if (type === "multipolygon" || type === "polygon" || type === "linestring") {
            args.getGeometry().forEachLatLng(function (a) {
                if (line.length > 0 && line[0][0] === a.lng && line[0][1] === a.lat) {
                    line.push([a.lng, a.lat]);
                    ThongTin.setHighlight(line);
                    line = [];
                } else {
                    line.push([a.lng, a.lat]);
                }
                boundLatLng.push([a.lng, a.lat]);
                lat = a.lat;
                lng = a.lng;
            });
            if (line.length > 0) ThongTin.setHighlight(line);
        } else if (type == "point") {
            lat = args.getGeometry()._coordinate.lat;
            lng = args.getGeometry()._coordinate.lng;
            boundLatLng.push([lng, lat]);
        } else if (type === "multipoint") {
            var listobj = args.getGeometry().getArray();
            $.each(listobj, function (i, obj) {
                if (i == 0) {
                    lat = obj.lat;
                    lng = obj.lng;
                }
                boundLatLng.push([obj.lng, obj.lat]);
            });
        }
        else {
            $.each(args.getGeometry()._elements, function (i, obj) {
                obj.forEachLatLng(function (a) {
                    line.push([a.lng, a.lat]);
                    lat = a.lat;
                    lng = a.lng;
                    boundLatLng.push([a.lng, a.lat]);
                });
                if (line.length > 0) ThongTin.setHighlight(line);
                line = [];
            });
        }

        //if (boundLatLng.length > 0) {
        //    exploit.fitBounds(boundLatLng);
        //}
    },
    setHighlight: function (line) {
        var strokewitth = 2
        if (ThongTin.GLOBAL.dataObjectHover != null) {
            strokewitth = ThongTin.GLOBAL.dataObjectHover.feature._properties["stroke-width"] != null ? ThongTin.GLOBAL.dataObjectHover.feature._properties["stroke-width"] : 2;
        }
        let polyline = new map4d.Polyline({
            path: line,
            strokeColor: "#ff0000",
            strokeOpacity: 1.0,
            strokeWidth: strokewitth,
            zIndex: 100
        });

        // Thêm polyline vào bản đồ
        if (ThongTin.GLOBAL.dataObjectHover !== null) {
            polyline.setUserData(ThongTin.GLOBAL.dataObjectHover.feature._id);
        }
        polyline.setMap(map);
        ThongTin.GLOBAL.listHighlight.push(polyline);
    },
    clearHighlight: function () {
        if (ThongTin.GLOBAL.listHighlight !== null && ThongTin.GLOBAL.listHighlight.length > 0) {
            $.each(ThongTin.GLOBAL.listHighlight, function () {
                this.setMap(null)
            });
            ThongTin.GLOBAL.listHighlight = [];
        }
    },
    showHideViewProperty: function (isCheck) {
        if (isCheck) {//show
            $(ThongTin.SELECTORS.inforData).removeClass('detail-property-collapse');
            //$(ThongTin.SELECTORS.inforData).css('display', 'block');
        }
        else {//hide
            $(ThongTin.SELECTORS.inforData).addClass('detail-property-collapse');
        }
    },
    getIdDirectoryByMainObjectId: function (id) {
        let value = '';
        let object = exploit.GLOBAL.listDirectoryMainObject.find(x => x.listMainObject.includes(id));
        if (object != undefined) {
            value = object.id;
        }
        return value;
    },
    //highligt point
    highlightObjectPoint: function (args) {
        ThongTin.clearHighlightPoint();
        var icon = args.getIcon();
        oldIconHignlight = icon;
        //args.setIcon(new map4d.Icon(31, 32, "/common/location-1.png"));
        var html = `
                    <div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
                        <div class="gm-style-iw-a" style="position: absolute; top: 60px;">
                            <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
                                <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px;">
                                    <img src= "${icon.url}" style="width: 25px;" />
                                </div>
                            </div>
                        </div>
                    </div>`;

        args.setIconView(html);

        //args.setIcon(new map4d.Icon(38, 38, location.origin + "/images/marker-yellow.png"));
        args.setZIndex(99);
        args.showInfoWindow();

        args.setMap(null);

        //console.log(args.getAnchor());
        //let marker = new map4d.Marker({
        //    position: args.getPosition(),
        //    iconView: html,
        //    anchor: [0.5, 1.0],
        //})
        //marker.setMap(map);
        args.setMap(map);
        ThongTin.GLOBAL.listHighlightPoint.push(args);
    },
    //highligt mutiple point
    hightlightObjectMutiplePoint: function (array) {
        ThongTin.clearHighlightPoint();
        for (var i = 0; i < array.length; i++) {
            var icon = array[i].getIcon();
            oldIconHignlight = icon;
            var html = `
                    <div style="cursor: default; position: absolute; top: 0px; left: 0px; z-index: -190;">
                        <div class="gm-style-iw-a" style="position: absolute; top: 60px;">
                            <div class="gm-style-iw-t" style="right: 0px; bottom: 61px;">
                                <div class="gm-style-iw gm-style-iw-c" style="max-width: 60px; max-height: 756px;">
                                    <img src= "${icon.url}" style="width: 25px;" />
                                </div>
                            </div>
                        </div>
                    </div>`;

            //array[i].setIcon(new map4d.Icon(38, 38, icon.url));
            //array[i].setIcon(new map4d.Icon(31, 32, "/common/location-1.png"));
            array[i].setIconView(html);
            array[i].setZIndex(99);
            array[i].setMap(null);
            array[i].setMap(map);
            ThongTin.GLOBAL.listHighlightPoint.push(array[i]);
            array[i].hideInfoWindow();
        }
    },
    clearHighlightPoint: function () {
        if (ThongTin.GLOBAL.listHighlightPoint !== null && ThongTin.GLOBAL.listHighlightPoint.length > 0) {
            $.each(ThongTin.GLOBAL.listHighlightPoint, function () {
                var icon = this.getIcon();
                //this.setIcon(new map4d.Icon(26, 26, icon.url));
                this.setIconView("");
                this.setZIndex(4);
                this.hideInfoWindow();

                this.setIcon(null);
                this.setIcon(new map4d.Icon(21, 22, oldIconHignlight.url));

                this.setMap(null);
                this.setMap(map);
            });
            ThongTin.GLOBAL.listHighlightPoint = [];
            oldIconHignlight = "";
        }

        ThongTin.GLOBAL.MarkerFocus = null;
    },
    // search obj
    resizeMenuLeft: function () {
        $(ThongTin.SELECTORS.content_list_object).html('');
        ThongTin.GLOBAL.page = 0;
        ThongTin.AppenedLstObject(TreeViewLayer.GLOBAL.DirectoryForcus);
    },
    AppenedLstObject: function (id) {
        var pageSize = Math.ceil($(ThongTin.SELECTORS.content_lst).height() / 44);
        var numberOfRecordToskip = ThongTin.GLOBAL.page * pageSize;

        ThongTin.GetListObject(id, $(ThongTin.SELECTORS.input_search_object).val(), numberOfRecordToskip, pageSize);

        //var data = ThongTin.GLOBAL.lstSeachObject;

        //if (data.length != 0) {

        //    $.each(data, function (i, obj) {
        //        var string = `<li class="object-item" data-id="${obj.id}" data-lat="" data-lng="" data-directory="${obj.idDirectory}">
        //                        <a class="" style="white-space:normal;">
        //                            <span> ${(obj.nameObject != null && obj.nameObject != "") ? obj.nameObject : l('Main:NoData')} </span>
        //                        </a>
        //                    </li>`;
        //        $(ThongTin.SELECTORS.content_list_object).append(string);
        //    });
        //}
    },
    addHtmlSearch: function (data) {
        if (data.length != 0) {
            $.each(data, function (i, obj) {
                var string = `<li class="object-item" data-id="${obj.id}" data-lat="" data-lng="" data-directory="${obj.idDirectory}">
                                <a class="" style="white-space:normal;">
                                    <span> ${(obj.nameObject != null && obj.nameObject != "") ? obj.nameObject : l('Main:NoData')} </span>
                                </a>
                            </li>`;
                $(ThongTin.SELECTORS.content_list_object).append(string);
            });
        }
    },
    GetListObject: function (id, search, skipnumber, countnumber) {
        TreeViewLayer.GLOBAL.lstSeachObject = [];
        search = search.toLowerCase().trim().replace(/\s\s+/g, ' ');
        var code = $(ThongTin.SELECTORS.selected_search).val();
        var type = $(ThongTin.SELECTORS.selected_search + " option:selected").attr("data-type") != undefined ? $(ThongTin.SELECTORS.selected_search + " option:selected").attr("data-type") : "";
        ThongTin.GLOBAL.cancelRequest = $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: ThongTin.CONSTS.URL_GET_LIST_OBJECT + '?id=' + id + "&skipnumber=" + skipnumber + "&countnumber=" + countnumber + "&search=" + search
                + "&code=" + code + "&type=" + type,
            data: {},
            success: function (data) {

                if (data.code == "ok") {
                    if (data.result != null && data.result.length > 0) {
                        ThongTin.GLOBAL.lstSeachObject = data.result;
                        ThongTin.addHtmlSearch(ThongTin.GLOBAL.lstSeachObject);
                    }
                    else {
                        ThongTin.GLOBAL.lstSeachObject = [];
                        if (skipnumber == 0 && search != "") {
                            $(ThongTin.SELECTORS.content_list_object).html("");

                            var html = `<div class="treeview-not-result text-center">

                                                <span style="border: none;text-align: center;font-style: italic;">${l('KhaiThac:SearchNull')} </span>
                
                                          </div>`

                            if ($(ThongTin.SELECTORS.content_list_object).find('.object-item').length == 0) {
                                $(ThongTin.SELECTORS.content_list_object).html(html);
                            }
                        }
                    }
                }
                else {
                    ThongTin.GLOBAL.lstSeachObject = [];
                    if (skipnumber == 0 && search != "") {
                        $(ThongTin.SELECTORS.content_list_object).html("");
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    GetFileByActivityId: function (id) {

        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + "/api/HTKT/DocumentActivity/get-file-by-DetailActivityId?Id=" + id,
            data: {
            },
            success: function (data) {
                ThongTin.GLOBAL.listObjectFileActivity = [];
                if (data.code == "ok" && data.result.length > 0) {
                    $.each(data.result, function (i, obj) {
                        var item = {
                            id: obj.id,
                            name: obj.nameDocument,
                            idDirectoryProperties: obj.nameFolder,
                            url: obj.urlDocument,
                            order: obj.orderDocument
                        };
                        ThongTin.GLOBAL.listObjectFileActivity.push(item);
                    });
                }
            }
        });
    },
    //highligt click list object
    setHighLightClickList: function (idobj, idLayer) {
        var location = null;
        // hightlight marker
        if (exploit.GLOBAL.listObject3D !== null && exploit.GLOBAL.listObject3D.length > 0) {
            var obj3d = exploit.GLOBAL.listObject3D.find(x => x.idLayer == idLayer && x.id == idobj);

            if (obj3d != undefined) {
                if (obj3d.object != null) {
                    obj3d.object.setSelected(true);
                    location = obj3d.object.properties.location;
                } else {
                    map.setSelectedBuildings([obj3d.idObject]);
                }
            }
        }

        // hightlight obj 3d
        if (exploit.GLOBAL.listMarker !== null && exploit.GLOBAL.listMarker.length > 0 && (idLayer !== "" && idLayer !== null)) {
            var objLayer = exploit.GLOBAL.listMarker.find(x => x.id == idLayer);
            if (typeof objLayer !== "undefined" && objLayer.lstMarker.length > 0) {
                var objPoint = objLayer.lstMarker.filter(x => x.userData.id == idobj);
                if (objPoint != undefined) {
                    if (objPoint.length == 1) {
                        location = objPoint[0].properties._position;
                        ThongTin.highlightObjectPoint(objPoint[0]);
                    }
                    else {
                        ThongTin.hightlightObjectMutiplePoint(objPoint);
                    }
                }
            }
        }

        if (!map.is3dMode()) {
            if (exploit.GLOBAL.lstFeature != null && exploit.GLOBAL.lstFeature.length > 0 && (idobj !== "" && idobj !== null)) {
                for (var i = 0; i < exploit.GLOBAL.lstFeature.length; i++) {
                    var arrayFeture = exploit.GLOBAL.lstFeature[i];
                    if (arrayFeture.length > 0) {
                        var geoJson = arrayFeture.find(x => x._id == idobj);
                        if (geoJson != undefined) {

                            var newGeo = {
                                feature: geoJson
                            }
                            //ThongTin.highlightObject(newGeo);
                            ThongTin.highlightObjectMenuRight(geoJson);
                        }
                    }
                }
            } else if (ThongTin.GLOBAL.mainObjectSelected !== null) {
                this.highlightObjectGeosjon(ThongTin.GLOBAL.mainObjectSelected);
            }
        }
    },
    highlightObjectGeosjon: function (data) {
        let type = data.geometry.type.toLowerCase();
        let objLine = null;
        switch (type) {
            case "polygon":
            case "multipolygon":
                var pol = {
                    "geometry": data.geometry,
                    "properties": {},
                    "type": "Feature"
                }
                objLine = turf.polygonToLine(pol);
                if (typeof objLine.features !== "undefined" && objLine.features.length > 0) {
                    $.each(objLine.features, function (i, obj) {
                        if (obj.geometry.type.toLowerCase() === "multilinestring") {
                            $.each(obj.geometry.coordinates, function (i, objcoordinates) {
                                ThongTin.setHighlight(objcoordinates);
                            });
                        } else
                            ThongTin.setHighlight(obj.geometry.coordinates);
                    });
                } else {
                    ThongTin.setHighlight(objLine.geometry.coordinates);
                }
                break;
            case "linestring":
                ThongTin.setHighlight(data.geometry.coordinates);
                break;
            case "multilinestring":
                $.each(data.geometry.coordinates, function (i, obj) {
                    ThongTin.setHighlight(obj);
                });
                break;
            default:
            // code block
        }
    },

    ResetIconInfoData: function () {
        $(ThongTin.SELECTORS.Item_Menu_Right).each(function () {
            var step = $(this).attr("data-li");
            switch (step) {
                case "step-info":
                    $(this).find("a").html(`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><defs></defs><path class="a" d="M0,0H24V24H0Z" /><path class="b" d="M12,22A10,10,0,1,1,22,12,10,10,0,0,1,12,22Zm0-2a8,8,0,1,0-8-8A8,8,0,0,0,12,20ZM11,7h2V9H11Zm0,4h2v6H11Z" /></svg>`);
                    break;
                case "step-document":
                    $(this).find("a").html(`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                            <defs>
                            </defs>
                            <path class="a" style="fill: none;" d="M0,0H24V24H0Z"></path>
                            <path class="b" style="fill: rgba(0,0,0,0.87);" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H19V7H11.586l-2-2H4V17l1.5-6h17l-2.31,9.243a1,1,0,0,1-.97.757Zm16.938-8H7.062l-1.5,6H18.438Z"></path>
                        </svg>`);
                    break;
                case "step-action":
                    $(this).find("a").html(`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                            <defs>
                            </defs>
                            <path class="a" style="fill: none;" d="M0,0H24V24H0Z" />
                            <path class="b" style="fill: rgba(0,0,0,0.87);" d="M16.625,3.133l-1.5,1.5a7.992,7.992,0,1,0,4.242,4.242l1.5-1.5a10,10,0,1,1-4.242-4.242Zm1.739,1.089,1.414,1.414L12,13.414,10.586,12Z" />
                        </svg>`);
                    break;
                default:
            }
        })
    }

};