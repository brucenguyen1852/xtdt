﻿//var l = abp.localization.getResource('KhaiThac');
var isGroupClick = false;
var groupNameChossne = "";
var TimKiem =
{
    GLOBAL: {
        TreeGroup: [],
        isDrawSearch: -1,
        IsResetDrawSearch: false,
        // vẽ polygon
        ArrayPolygon: {
            ListPoint: null,
            Meter: null,
            Polyline: null,
            Polygon: null,
            Acreage: 0,
        },
        // vẽ circle
        DrawCircle: {
            Center: null,
            Radius: 0,
            MarkerCenter: null,
            Circle: null,
            MeterInfo: null
        },
        // vẽ line
        DrawLine: {
            ListPoint: [],
            Distance: 0,
            Polyline: null,
            Buffer: null
        },
        SearchFollowGeometry: -1,
        ArrayObjectMain: [],
        SearchPaging: {
            Size: 30,
            Page: 1,
            ListId: [],
        },
        polygonVN200: []
    },
    CONSTS: {
        KTTruc: kTTruc,
        Zone: zone,
        URL_GET_PROPERTIES_BY_ARRAY_MAIN_OBJECT: "/api/khaithac/khaiThacDetailObject/get-properties-by-array-main-object",
        URL_SEARCH_MAIN_OBJECT_BY_PROPERTIES_VALUE: "/api/khaithac/khaiThacDetailObject/search-properties-by-array-directory"
    },
    SELECTORS: {
        btn_open_content_search: ".toggle-search-content",
        content_search: "#search-content",
        btn_close_tim_kiem: "#close-tim-kiem",
        check_box_search_layer: "#check-search-follow-properties",
        content_chil_layer: "#content-chil-layer",
        ul_chossen_search_retage: ".choosen-search-retage",
        selected_option_folder_parent: "#select-folder-parent",
        selected_option_layer_chil: "#select-search-layer",
        btn_search_polygon: "#btnSearchPolygon",
        btn_search_circle: "#btnSearchCircle",
        btn_search_line_buffer: "#btnSearchLine",
        btn_refresh_search: "#btn-refresh-search",
        btn_search_layer: "#btn-search-layer",
        check_tree: ".checkmark",
        meter_buffer_line: "#meter-buffer-line",
        content_input_buffer: "#content-buffer-input",
        input_search_properties: "#input-search-properties",

        // result search
        item_search_geometry: ".item-search",
        result_content_search: "#result-search-content",
        head_search_result: ".result-search-content-header",
        body_search_result: ".result-search-content-body",
        list_search_result: ".list-result-search",
        item_search_result: ".item-search-result",
        btn_close_search_result: ".close-search-result",
        div_content_list_object: ".div-content-list-object",
        spinner: "#spinner-result-search-body"
    },
    init: function () {
        TimKiem.setEvent();
        TimKiem.Select2Default();
        TimKiem.SelectedChilLayer();
        TimKiem.eventMap();
    },
    setEvent: function () {
        //  button hiển thị mục tìm kiếm
        $(TimKiem.SELECTORS.btn_open_content_search).on('click', function () {
            //if (!$(TimKiem.SELECTORS.content_search).hasClass('open')) {
            //    //exploit.CancelAction();
            //    $(TimKiem.SELECTORS.selected_option_layer_chil).html('');
            //    $(TimKiem.SELECTORS.selected_option_folder_parent).html('');

            //    TimKiem.SelectFolderParent();
            //    $(TimKiem.SELECTORS.content_search).addClass('open');
            //    $(TimKiem.SELECTORS.btn_open_content_search).addClass('isForcus');

            //    $(this).attr('data-original-title', l("CloseSearch"));
            //}
            //else {
            //    TimKiem.CloseContentTimKiem();
            //}
        });

        // button đóng mục tìm kiếm
        $(TimKiem.SELECTORS.btn_close_tim_kiem).on('click', function () {
            exploit.CancelAction();
        });

        // toogle checkbox tìm kiếm theo lớp
        $(TimKiem.SELECTORS.check_box_search_layer).on('change', function () {
            $(TimKiem.SELECTORS.content_chil_layer).slideToggle("slow");
        });

        // check tìm kiếm theo từng vùng
        $(TimKiem.SELECTORS.ul_chossen_search_retage).on('click', 'li', function () {
            $(TimKiem.SELECTORS.ul_chossen_search_retage + " li").removeClass('isForcus');
            $(this).addClass('isForcus');
        });

        $(TimKiem.SELECTORS.selected_option_folder_parent).on('select2:select', function () {
            var id = $(this).val();
            var arrayId = [];
            $.each($(`${TimKiem.SELECTORS.check_tree}.check`), function () {
                var idDirectory = $(this).attr('data-id');

                arrayId.push(idDirectory);
            });

            if (id != undefined && id != "" && id != null) {
                var layer = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == id); // tìm layer
                var layerGroup = TimKiem.GetLayerGroup(layer); // group những layer con của layer

                if (layerGroup != undefined && layerGroup.children.length > 0) {
                    TimKiem.SelectedChilLayer(layerGroup); // hiển thị danh sách layer con
                }
                else {
                    //$(TimKiem.SELECTORS.selected_option_layer_chil).select2('destroy');
                    $(TimKiem.SELECTORS.selected_option_layer_chil).html('');
                }
            }
        });

        // clear chọn lớp thư mục
        $(TimKiem.SELECTORS.selected_option_folder_parent).on('select2:clear', function () {
            $(TimKiem.SELECTORS.selected_option_layer_chil).html('');
        });

        // chọn vẽ đa giác
        $(TimKiem.SELECTORS.item_search_geometry).on('click', function () {
            var type = $(this).attr('data-type');
            switch (type) {
                case "1":   // tìm kiếm theo đa giác
                    TimKiem.GLOBAL.isDrawSearch = 1;
                    TimKiem.GLOBAL.SearchFollowGeometry = 1;

                    $(TimKiem.SELECTORS.content_input_buffer).slideUp("slow"); // ẩn input buffer line

                    $(TimKiem.SELECTORS.input_search_properties).val(""); // clear tìm kiếm theo thuộc tính
                    break;
                case "2":   // tìm kiếm theo hình tròn
                    TimKiem.GLOBAL.isDrawSearch = 2;
                    TimKiem.GLOBAL.SearchFollowGeometry = 2;

                    $(TimKiem.SELECTORS.content_input_buffer).slideUp("slow"); // ẩn input buffer line

                    $(TimKiem.SELECTORS.input_search_properties).val(""); // clear tìm kiếm theo thuộc tính
                    break;
                case "3":   // tìm kiếm theo line
                    TimKiem.GLOBAL.isDrawSearch = 3;
                    TimKiem.GLOBAL.SearchFollowGeometry = 3;
                    $(TimKiem.SELECTORS.content_input_buffer).slideDown("slow"); // ẩn input buffer line
                    $(TimKiem.SELECTORS.input_search_properties).val(""); // clear tìm kiếm theo thuộc tính
                    break;
            }

            TimKiem.RemovePolygon();
            TimKiem.RemoveCircle();
            TimKiem.RemoveLine();
            TimKiem.CloseResultSearchContent();
        });

        $(TimKiem.SELECTORS.meter_buffer_line).on('keyup change', function () {
            if ($(this).val() <= 0) {
                $(this).val(100);
            }
            TimKiem.DrawLine();
        });

        // Làm mới tìm kiếm
        $(TimKiem.SELECTORS.btn_refresh_search).on('click', function () {
            TimKiem.GLOBAL.isDrawSearch = -1; // xét lại trạng thái vẽ
            TimKiem.GLOBAL.IsResetDrawSearch = false;
            TimKiem.GLOBAL.SearchFollowGeometry = -1; // xét lại trạng thái vẽ theo hình học

            $(TimKiem.SELECTORS.content_input_buffer).slideUp("slow"); // ẩn input buffer line
            $(TimKiem.SELECTORS.input_search_properties).val(""); // clear tìm kiếm theo thuộc tính

            TimKiem.RemovePolygon(); // xóa đa giác
            TimKiem.RemoveCircle(); // xóa hình tròn
            TimKiem.RemoveLine(); // xóa line

            $(TimKiem.SELECTORS.ul_chossen_search_retage + " li").removeClass('isForcus'); // bỏ focus tìm kiếm theo hình học

            TimKiem.CloseResultSearchContent(); // đóng content kết quả tìm kiếm
            $('.toggle-detail-property2').trigger('click'); // đóng content xem thông tin 1 đối tượng

            // đóng tìm kiếm theo lớp
            $(TimKiem.SELECTORS.check_box_search_layer).prop('checked', false);
            $(TimKiem.SELECTORS.content_chil_layer).slideUp("slow");
        });

        // tìm kiếm
        $(TimKiem.SELECTORS.btn_search_layer).on('click', function () {
            toastr.remove();
            if (TimKiem.GLOBAL.isDrawSearch != -1 && !TimKiem.GLOBAL.IsResetDrawSearch) {
                swal({
                    title: "Thông báo",
                    text: "Bạn phải kết thúc thao tác vẽ",
                    icon: "warning",
                    button: "Đóng",
                })
            }
            else {
                $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
                $(TimKiem.SELECTORS.list_search_result).html(`<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`);

                TimKiem.GLOBAL.SearchPaging.Page = 1;
                TimKiem.GLOBAL.isDrawSearch = -1; // kết thúc vẽ
                var idParent = $(TimKiem.SELECTORS.selected_option_folder_parent).val(); // id cha
                if (idParent != "" && idParent != null && idParent != undefined) {
                    var idDirectory = idParent;
                    var isParent = false; // trạng thái tìm toàn bộ layer con trong thư mục được chọn

                    var arrayMainResult = []; // danh sách tìm kiếm đối tượng ops1
                    var arrayMainWithMaptileResult = []; // danh sách tìm kiếm đối tượng ops2, ops3

                    if ($(TimKiem.SELECTORS.check_box_search_layer).is(':checked')) // nếu layer con được checked
                    {
                        if ($(TimKiem.SELECTORS.selected_option_layer_chil).val() != "" && $(TimKiem.SELECTORS.selected_option_layer_chil).val() != null) // kiểm tra layer con được chọn
                        {
                            idDirectory = $(TimKiem.SELECTORS.selected_option_layer_chil).val();
                        }
                        else {
                            isParent = true;
                        }
                    }
                    else {
                        isParent = true;
                    }
                    var paths = [];
                    var poly = null;

                    //kiểm tra có tìm kiếm theo vùng mới được vào
                    if (TimKiem.GLOBAL.SearchFollowGeometry === 1 || TimKiem.GLOBAL.SearchFollowGeometry === 2 || TimKiem.GLOBAL.SearchFollowGeometry === 3 || $(TimKiem.SELECTORS.input_search_properties).val() !== "") {
                        $(TimKiem.SELECTORS.result_content_search).addClass('open');
                        TimKiem.CloseSpinner(); // ẩn loader

                        var checkPolyGon = true;
                        switch (TimKiem.GLOBAL.SearchFollowGeometry) // trạng thái tìm theo vùng
                        {
                            case 1:
                                poly = turf.polygon(paths);

                                if (TimKiem.GLOBAL.ArrayPolygon.Polygon != null) {
                                    $.each(TimKiem.GLOBAL.ArrayPolygon.Polygon.paths[0], function (indexPaths, itemPaths) {
                                        paths.push([itemPaths.lng, itemPaths.lat]);
                                    });
                                    poly = turf.polygon([paths]);
                                }
                                else {
                                    checkPolyGon = false;
                                }
                                break;
                            case 2:
                                if (TimKiem.GLOBAL.DrawCircle.Circle != null) {
                                    var point = turf.point([TimKiem.GLOBAL.DrawCircle.Center.lng, TimKiem.GLOBAL.DrawCircle.Center.lat]);
                                    poly = turf.buffer(point, TimKiem.GLOBAL.DrawCircle.Radius + 2, { units: 'meters' });
                                } else {
                                    checkPolyGon = false;
                                }
                                break;
                            case 3:
                                if (TimKiem.GLOBAL.DrawLine.Polyline != null && TimKiem.GLOBAL.DrawLine.Buffer != null) {
                                    $.each(TimKiem.GLOBAL.DrawLine.Buffer.paths[0], function (indexPaths, itemPaths) {
                                        paths.push([itemPaths.lng, itemPaths.lat]);
                                    });
                                    poly = turf.polygon([paths]);
                                }
                                else {
                                    checkPolyGon = false;
                                }
                                break;
                            default:
                        }

                        //==========================================================================================================================================

                        //if (!checkPolyGon) {
                        //    $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
                        //    html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
                        //    $(TimKiem.SELECTORS.list_search_result).html(html);
                        //}
                        //else
                        {
                            var layer = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == idDirectory);
                            if (layer != undefined) {
                                if (isParent) // ====================trạng thái tìm kiếm toàn bộ trong thư mục===============================
                                {
                                    //------------------tìm kiếm bằng thuộc tính--------------------------------------------
                                    if ($(TimKiem.SELECTORS.input_search_properties).val() !== "") {

                                        // lấy danh sách id của toàn bộ layer
                                        var arrayId = []; // danh sách layer option 1
                                        var arrayMaptile = []; // danh sách layer option 2,3

                                        var checkMaptileParent = exploit.GLOBAL.listGroundOvelay.find(x => x.id == layer.id); //kiểm tra layer có maptile option 2,3.
                                        var checkLayerParent = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == layer.id); // kiểm tra layer có nằm trong danh sách checked hay k?

                                        if (typeof checkLayerParent !== "undefined" && typeof checkMaptileParent === "undefined") {
                                            arrayId.push(layer.id);
                                        }
                                        else if (typeof checkMaptileParent !== "undefined") { //search maptile
                                            arrayMaptile.push(layer.id);
                                        }

                                        var layerChils = TimKiem.GetLayerGroup(layer);
                                        var getAllChilsId = TimKiem.GetAllDirectoryByParent(layerChils, arrayId, arrayMaptile);

                                        arrayId = arrayId.concat(getAllChilsId.arrayOp1);

                                        arrayMaptile = arrayId.concat(getAllChilsId.arrayOp2);

                                        arrayId = [...new Set(arrayId)]; // bỏ giá trị trùng lặp
                                        arraarrayMaptileyId = [...new Set(arrayMaptile)]; // bỏ giá trị trùng lặp

                                        // tìm kiếm toàn bộ đối tượng của layer option 1 theo properties
                                        if (arrayId.length > 0) {
                                            var arraySearchResult = TimKiem.SearchLayerByProperties(arrayId, $(TimKiem.SELECTORS.input_search_properties).val());
                                            if (arraySearchResult != null) {
                                                arrayMainResult = arrayMainResult.concat(arraySearchResult);
                                            }
                                        }

                                        // tìm kiếm toàn bộ đối tượng của layer option 2,3 theo properties
                                        if (arrayMaptile.length > 0) {
                                            var arraySearchMaptileResult = TimKiem.SearchLayerMaptileByProperties(arrayMaptile, $(TimKiem.SELECTORS.input_search_properties).val());
                                            if (arraySearchMaptileResult != null) {
                                                arrayMainWithMaptileResult.push(arraySearchMaptileResult);
                                            }
                                        }

                                    }
                                    // ----------------tìm kiếm bằng đa giác---------------------------------------------
                                    else {
                                        if (!checkPolyGon) {
                                            $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
                                            html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
                                            $(TimKiem.SELECTORS.list_search_result).html(html);
                                        }
                                        else {
                                            //------------------- tìm trong lớp cha----------------------------------
                                            var checkMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == layer.id); //kiểm tra layer có maptile option 2,3.
                                            var layerParent = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == layer.id); // kiểm tra layer có nằm trong danh sách checked hay k?
                                            if (typeof layerParent !== "undefined" && typeof checkMaptile === "undefined") {
                                                layerParent.title = layer.name;
                                                layerParent.key = layer.id;

                                                var objectSearchResult = TimKiem.Searching(layerParent, poly); // tìm kiếm đối tượng
                                                arrayMainResult.push(objectSearchResult);
                                            } else if (typeof checkMaptile !== "undefined") { //search maptile
                                                var objSearchMaptileResult = TimKiem.SearchBBoxMaptile(checkMaptile, poly);
                                                if (objSearchMaptileResult != null) {
                                                    arrayMainWithMaptileResult.push(objSearchMaptileResult);
                                                }
                                            }
                                            //----------------------end tìm trong lớp cha ------------------------------


                                            //------------------- tìm trong lớp con----------------------------------
                                            var layerChils = TimKiem.GetLayerGroup(layer);
                                            $.each(layerChils.children, function (index, itemLayer) {
                                                var searchLayer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == itemLayer.key); // kiểm tra layer có nằm trong danh sách checked hay k?
                                                var checkMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == itemLayer.key); //kiểm tra layer có maptile option 2,3.
                                                if (typeof searchLayer !== "undefined" && typeof checkMaptile === "undefined") {
                                                    searchLayer.title = itemLayer.title;
                                                    searchLayer.key = itemLayer.key;

                                                    var objectSearchResult = TimKiem.Searching(searchLayer, poly); // tìm kiếm đối tượng
                                                    arrayMainResult.push(objectSearchResult);
                                                } else if (typeof checkMaptile !== "undefined") { //search maptile
                                                    var objSearchMaptileResult = TimKiem.SearchBBoxMaptile(checkMaptile, poly);
                                                    if (objSearchMaptileResult != null) {
                                                        arrayMainWithMaptileResult.push(objSearchMaptileResult);
                                                    }
                                                }

                                                if (itemLayer.children.length > 0) // nếu có ds con thì check ds con
                                                {
                                                    var objSearchAll = TimKiem.SearchAll(itemLayer, arrayMainResult, arrayMainWithMaptileResult, poly);
                                                    arrayMainResult = objSearchAll.arrayResult;
                                                    arrayMainWithMaptileResult = objSearchAll.arrayMaptileResult;
                                                }
                                            });

                                            //----------------------end tìm trong lớp con ------------------------------
                                        }
                                    }
                                }
                                else // ==========================tìm kiếm theo layer========================================================
                                {
                                    //------------------ tìm kiếm bằng thuộc tính--------------------------------------------
                                    if ($(TimKiem.SELECTORS.input_search_properties).val() !== "") {
                                        // lấy danh sách id của toàn bộ layer
                                        var arrayId = []; // danh sách layer option 1
                                        var arrayMaptile = []; // danh sách layer option 2,3

                                        var checkMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == layer.id); //kiểm tra layer có maptile option 2,3.
                                        var checkLayer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == layer.id); // kiểm tra layer có nằm trong danh sách checked hay k?

                                        if (typeof checkLayer !== "undefined" && typeof checkMaptile === "undefined") {
                                            arrayId.push(layer.id);
                                        }
                                        else if (typeof checkMaptile !== "undefined") { //search maptile
                                            arrayMaptile.push(layer.id);
                                        }

                                        // tìm kiếm toàn bộ đối tượng của layer option 1 theo properties
                                        if (arrayId.length > 0) {
                                            var arraySearchResult = TimKiem.SearchLayerByProperties(arrayId, $(TimKiem.SELECTORS.input_search_properties).val());
                                            if (arraySearchResult != null) {
                                                arrayMainResult = arrayMainResult.concat(arraySearchResult);
                                            }
                                        }

                                        // tìm kiếm toàn bộ đối tượng của layer option 2,3 theo properties
                                        if (arrayMaptile.length > 0) {
                                            var arraySearchMaptileResult = TimKiem.SearchLayerMaptileByProperties(arrayMaptile, $(TimKiem.SELECTORS.input_search_properties).val());
                                            if (arraySearchMaptileResult != null) {
                                                arrayMainWithMaptileResult.push(arraySearchMaptileResult);
                                            }
                                        }

                                    }
                                    // ----------------tìm kiếm bằng đa giác---------------------------------------------
                                    else //----------------tìm kiếm bằng đa giác---------------------------------------------
                                    {
                                        if (!checkPolyGon) {
                                            $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
                                            html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
                                            $(TimKiem.SELECTORS.list_search_result).html(html);
                                        }
                                        else {
                                            var searchLayer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == layer.id); // kiểm tra layer có nằm trong danh sách checked hay k?
                                            var checkMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == layer.id); //kiểm tra layer có maptile option 2,3.
                                            if (typeof searchLayer !== "undefined" && typeof checkMaptile === "undefined") {

                                                searchLayer.title = layer.name;
                                                searchLayer.key = layer.id;

                                                var objectSearchResult = TimKiem.Searching(searchLayer, poly); // tìm kiếm đối tượng

                                                arrayMainResult.push(objectSearchResult);
                                            } else if (typeof checkMaptile !== "undefined") {//search maptile
                                                var objSearchMaptileResult = TimKiem.SearchBBoxMaptile(checkMaptile, poly);
                                                if (objSearchMaptileResult != null) {
                                                    arrayMainWithMaptileResult.push(objSearchMaptileResult);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            console.log(arrayMainWithMaptileResult)
                            console.log(arrayMainResult)

                            var listMainObject = [];
                            $.each(arrayMainResult, function (index, item) {
                                listMainObject = listMainObject.concat(item.listMainObject);
                            });

                            $.each(arrayMainWithMaptileResult, function (index, item) {
                                // set danh sách đối tượng của lớp dữ liệu sd ops2
                                var indexLayerMaptitle = TimKiem.GLOBAL.ArrayObjectMain.findIndex(x => x.id == item.id);

                                if (indexLayerMaptitle != - 1) {
                                    TimKiem.GLOBAL.ArrayObjectMain[indexLayerMaptitle].listMainObject = item.listMainObject;
                                    TimKiem.GLOBAL.ArrayObjectMain[indexLayerMaptitle].name = item.name;
                                }

                                listMainObject = listMainObject.concat(item.listMainObject);
                            });

                            arrayMainResult = arrayMainResult.concat(arrayMainWithMaptileResult);

                            TimKiem.GLOBAL.SearchPaging.ListId = listMainObject.map(x => x.id);

                            if (TimKiem.GLOBAL.SearchPaging.ListId.length > 0) {
                                TimKiem.SearchPaging(TimKiem.GLOBAL.SearchPaging.ListId, arrayMainResult);
                                //TimKiem.GetPropertiesByArrayMainObject(listMainObject.map(x => x.id), arrayMainResult);
                            }

                            if (arrayMainWithMaptileResult.length > 0) {

                            }

                            $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (${TimKiem.GLOBAL.SearchPaging.ListId.length})`);
                            if (TimKiem.GLOBAL.SearchPaging.ListId.length == 0) {
                                html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
                                $(TimKiem.SELECTORS.list_search_result).html(html);
                            }
                        }
                    }
                    else {
                        abp.notify.warn(l("KhaiThac:Warn:SearchWithShape"));
                    }
                }
                else {
                    $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
                    html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
                    $(TimKiem.SELECTORS.list_search_result).html(html);
                }
                $('.toggle-detail-property2').trigger('click'); // đóng content xem thông tin 1 đối tượng
            }
        });

        // click đối tượng trong kết quả tìm kiếm
        $(TimKiem.SELECTORS.list_search_result).on('click', TimKiem.SELECTORS.item_search_result, function () {
            var id = $(this).attr('data-id');
            var directoryId = $(this).attr('data-directory');
            ThongTin.GLOBAL.idMainObject = id;

            ThongTin.getMainObject(ThongTin.GLOBAL.idMainObject);

            ThongTin.showHideViewProperty(true);
            ThongTin.GLOBAL.idDirectory = directoryId;

            //ThongTin.GLOBAL.idDirectory = ThongTin.getIdDirectoryByMainObjectId(ThongTin.GLOBAL.idMainObject);
            var idLayer = $(this).data("directory");
            ThongTin.clearHighlight();
            ThongTin.setHighLightClickList(id, idLayer);

            if (exploit.GLOBAL.TypeDraw != -1) {
                $(exploit.SELECTORS.close_geo_info).trigger('click');
            }
        });

        // đóng content kết quả tìm kiếm
        $(TimKiem.SELECTORS.btn_close_search_result).on('click', function () {
            TimKiem.CloseResultSearchContent();
        });

        //scroll content tìm kiếm
        $(TimKiem.SELECTORS.div_content_list_object).scroll(function () {
            var H1 = $(this).scrollTop() + $(TimKiem.SELECTORS.div_content_list_object).height() + 300;
            var H2 = $(TimKiem.SELECTORS.list_search_result).height();
            if (H1 >= H2) {
                let count = Math.ceil(TimKiem.GLOBAL.SearchPaging.ListId.length / TimKiem.GLOBAL.SearchPaging.Size);
                if (TimKiem.GLOBAL.SearchPaging.Page < count) {
                    TimKiem.GLOBAL.SearchPaging.Page += 1;
                    TimKiem.SearchPaging(TimKiem.GLOBAL.SearchPaging.ListId, []);
                }
            }
        });

        // tìm kiếm theo thuộc tính cơ bản
        $(TimKiem.SELECTORS.input_search_properties).on('focus', function () {
            $(TimKiem.SELECTORS.content_input_buffer).slideUp("slow"); // ẩn input buffer line
            TimKiem.GLOBAL.isDrawSearch = -1; // xét lại trạng thái vẽ
            TimKiem.GLOBAL.IsResetDrawSearch = false;
            TimKiem.GLOBAL.SearchFollowGeometry = -1; // xét lại trạng thái vẽ theo hình học
            TimKiem.RemovePolygon(); // xóa đa giác
            TimKiem.RemoveCircle(); // xóa hình tròn
            TimKiem.RemoveLine(); // xóa line
            $(TimKiem.SELECTORS.ul_chossen_search_retage + " li").removeClass('isForcus'); // bỏ focus tìm kiếm theo hình học
            //TimKiem.CloseResultSearchContent(); // đóng content kết quả tìm kiếm
            $('.toggle-detail-property2').trigger('click'); // đóng content xem thông tin 1 đối tượng

        });
    },
    eventMap: function () {
        // sự kiện click line vẽ kế tiếp
        var clickMap = map.addListener("click", (args) => {
            TimKiem.ChoosenTypeDrawSearch(args);
        });

        // sự kiện click line vẽ kế tiếp
        let eventClickDraw = map.addListener("click", (args) => {
            if (TimKiem.GLOBAL.isDrawSearch != -1) {
                TimKiem.ChoosenTypeDrawSearch(args);
            }
        }, { polyline: true, polygon: true, marker: true, circle: true });

        //click map
        let eventClickData = map.data.addListener("click", (args) => {
            if (TimKiem.GLOBAL.isDrawSearch != -1) {
                TimKiem.ChoosenTypeDrawSearch(args);
            }
        });


        //------------------------------vẽ click 3d---------------------------------------------------------------------------------
        var click3D = map.addListener("click", (args) => {
            if (TimKiem.GLOBAL.isDrawSearch != -1) {
                TimKiem.ChoosenTypeDrawSearch(args);
            }
        }, { building: true })

        //-------------------------------vẽ Click 3d--------------------------------------------------------------------------------------
        // sự kiện click đối tượng 3d
        var click3DMap = map.addListener("click", (args) => {
            if (TimKiem.GLOBAL.isDrawSearch != -1) {
                TimKiem.ChoosenTypeDrawSearch(args);
            }
        }, { mapbuilding: true });

        // kết thúc vẽ
        let eventDoubleClickMapObject = map.addListener("dblClick", (args) => {
            if (TimKiem.GLOBAL.isDrawSearch != -1) {
                TimKiem.GLOBAL.IsResetDrawSearch = true;

                if (polylineTemp != null) {
                    polylineTemp.setMap(null);
                    polylineTemp = null;
                }
            }
        }, { marker: true, polygon: true, polyline: true, circle: true })

        let eventDoubleClickMap = map.addListener("dblClick", (args) => {
            if (TimKiem.GLOBAL.isDrawSearch != -1) {
                TimKiem.GLOBAL.IsResetDrawSearch = true;

                if (polylineTemp != null) {
                    polylineTemp.setMap(null);
                    polylineTemp = null;
                }
            }
        });

        // sự kiện di chuyển chuột trên map
        let eventMouseMove = map.addListener("mouseMove", (args) => {
            // hiển thị line kế tiếp nếu đang vẽ đoạn thẳng hoặc đa giác
            if (TimKiem.GLOBAL.isDrawSearch != -1) {
                if (!TimKiem.GLOBAL.IsResetDrawSearch) {
                    switch (TimKiem.GLOBAL.isDrawSearch) {
                        case 1:
                            if (TimKiem.GLOBAL.ArrayPolygon.ListPoint != null && TimKiem.GLOBAL.ArrayPolygon.ListPoint.length > 0) {
                                let path = [];
                                let listMarker = TimKiem.GLOBAL.ArrayPolygon.ListPoint;
                                let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                                let mousePoint = [args.location.lng, args.location.lat];
                                path.push(endPoint);
                                path.push(mousePoint);
                                exploit.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                            }
                            break;
                        case 2:
                            if (TimKiem.GLOBAL.DrawCircle.Center != null) {
                                let path = [];
                                let endPoint = [TimKiem.GLOBAL.DrawCircle.Center.lng, TimKiem.GLOBAL.DrawCircle.Center.lat];
                                let mousePoint = [args.location.lng, args.location.lat];
                                path.push(endPoint);
                                path.push(mousePoint);
                                exploit.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                                TimKiem.ShowRadius(path);
                            }
                            break;

                        case 3:
                            if (TimKiem.GLOBAL.DrawLine.ListPoint != null && TimKiem.GLOBAL.DrawLine.ListPoint.length > 0) {
                                let path = [];
                                let listMarker = TimKiem.GLOBAL.DrawLine.ListPoint;
                                let endPoint = [listMarker[listMarker.length - 1].getPosition().lng, listMarker[listMarker.length - 1].getPosition().lat];
                                let mousePoint = [args.location.lng, args.location.lat];
                                path.push(endPoint);
                                path.push(mousePoint);
                                TimKiem.createPolylineByMouseMoveLoDat(path, 3.0, 0.7);
                            }
                            break;
                        default: break;
                    }
                }
            }
        });
    },
    Select2Default: function () {
        $(TimKiem.SELECTORS.selected_option_folder_parent).select2({
            placeholder: l("ChoosenFolder"),
            language: "vi",
            allowClear: true,
            width: '100%',
            matcher: matchCustomBasic,
        });

        $(TimKiem.SELECTORS.selected_option_layer_chil).select2({
            placeholder: l("ChossenLayer"),
            language: "vi",
            allowClear: true,
            width: '100%',
            //matcher: matcher
        });
    },
    // select danh sách thư mục
    SelectFolderParent: function () {
        var arrayId = []; // danh sách layer dc checked
        $.each($(`${TimKiem.SELECTORS.check_tree}.check`), function () {
            var idDirectory = $(this).attr('data-id');

            arrayId.push(idDirectory);
        });

        //var option = '<option value=""></option>';
        var option = '';
        if (arrayId.length > 0) {
            $.each(TimKiem.GLOBAL.TreeGroup.children, function (i, item) {

                var check = arrayId.indexOf(item.key); // nếu layer được check thì hiển thị ra
                if (check != -1) {
                    option += `<option value="${item.key}">${item.title}</option>`;
                }
                else {
                    if (item.children.length > 0) // kiểm tra layer con có dc checked hay ko (nếu ds layer con dc checked thì hiển thị layer cha ra)
                    {
                        var check = TimKiem.CheckTreeInGroup(item, arrayId);

                        if (check) {
                            option += `<option value="${item.key}">${item.title}</option>`;
                        }
                    }
                }
            });
        }

        $(TimKiem.SELECTORS.selected_option_folder_parent).html(option);

        // set default selected children
        var idParent = $(TimKiem.SELECTORS.selected_option_folder_parent).val();
        if (idParent != "" && idParent != null) {
            var layer = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == idParent); // tìm layer
            var layerGroup = TimKiem.GetLayerGroup(layer); // group những layer con của layer

            if (layerGroup != undefined && layerGroup.children.length > 0) {
                TimKiem.SelectedChilLayer(layerGroup); // hiển thị danh sách layer con
            }
        }
    },
    // select danh sách con trong thư mục
    SelectedChilLayer: function (layer) {
        var lstoObj2 = [];
        var arrayId = [];
        $.each($(`${TimKiem.SELECTORS.check_tree}.check`), function () {
            var idDirectory = $(this).attr('data-id');

            arrayId.push(idDirectory);
        });
        if (arrayId.length > 0) {
            if (layer != undefined) {
                var level = 1;
                if (layer.children.length > 0) {
                    //var listObj = [];
                    for (var i = 0; i < layer.children.length; i++) {
                        //var obj = {
                        //    id: layer.children[i].key,
                        //    text: layer.children[i].title,
                        //    level: level
                        //};

                        var obj2 = {
                            id: layer.children[i].key,
                            text: layer.children[i].title,
                            level: level
                        };

                        //if (arrayId.indexOf(obj2.id) > -1) {
                        //    lstoObj2.push(obj2);
                        //}

                        if (layer.children[i].children.length > 0) // nếu layer hiện tại có layer con thì kiểm tra
                        {
                            //obj.children = loadOption(layer.children[i], 1);
                            loadOption(layer.children[i], level, arrayId);
                        }
                        else {
                            if (arrayId.indexOf(obj2.id) > -1) {
                                lstoObj2.push(obj2);
                            }
                        }

                        //listObj.push(obj);
                    }

                    $(TimKiem.SELECTORS.selected_option_layer_chil).select2('destroy');
                    $(TimKiem.SELECTORS.selected_option_layer_chil).html('');
                    $(TimKiem.SELECTORS.selected_option_layer_chil).select2({
                        placeholder: l("ChossenLayer"),
                        allowClear: true,
                        width: '100%',
                        data: lstoObj2,
                        templateSelection: formatState,
                        matcher: matchCustom,
                        templateResult: function (data) {
                            //if (!data.element) {
                            //    return data.text;
                            //}

                            //var $element = $(data.element);

                            //var $wrapper = $('<span></span>');
                            //$wrapper.addClass($element[0].className);
                            //$element.attr('data-id', data.id);
                            //var style = $element.attr('style');
                            //$wrapper.attr('style', style);

                            var padding = "";
                            if (data.level != undefined) {
                                padding = `padding-left:${data.level * 10}px;`
                            }
                            //$wrapper.text(data.text);
                            //var checkbox = $('<div class="checkbox"><input id="' + id + '" class="check-' + classElement + '" type="checkbox" ' + (isSelected > -1 ? 'checked' : '') + '><label for="checkbox1">' + state.text + '</label></div>', { id: id });
                            var checkbox = $(`<div class="content-layer-chil" data-id="${data.id}">
                                            <label style="${padding}" class="" data-id="${data.id}">${data.text}</label>
                                        </div>`);
                            return checkbox;

                        },
                    });
                    //return listObj;
                }
            }
        }

        //function loadOption(layer, level) {
        //    if (layer != undefined) {
        //        if (layer.children.length > 0) {
        //            var listObj = [];
        //            for (var i = 0; i < layer.children.length; i++) {
        //                var obj = {
        //                    id: layer.children[i].key,
        //                    text: layer.children[i].title,
        //                    level: level + 1
        //                };

        //                if (arrayId.indexOf(obj2.id) > -1) {
        //                    lstoObj2.push(obj);
        //                }

        //                if (layer.children[i].children.length > 0) {
        //                    obj.children = loadOption(layer.children[i], level + 1);
        //                }

        //                listObj.push(obj);
        //            }

        //            return listObj;

        //        }
        //    }
        //}

        function loadOption(layer, level, arrayId) {
            if (layer != undefined) {
                if (arrayId.indexOf(layer.key) > -1) // nếu layer nằm trong danh sách checked thì hiển thị ra
                {
                    var obj = {
                        id: layer.key,
                        text: layer.title,
                        level: level
                    };

                    lstoObj2.push(obj);
                    level = level + 1;
                }

                if (layer.children.length > 0) // nếu layer có layer con thì kiểm tra layer con
                {
                    for (var i = 0; i < layer.children.length; i++) {
                        var obj = {
                            id: layer.children[i].key,
                            text: layer.children[i].title,
                            level: level
                        };

                        //if (arrayId.indexOf(obj.id) > -1) {
                        //    lstoObj2.push(obj);
                        //}
                        if (layer.children[i].children.length > 0) {
                            loadOption(layer.children[i], level, arrayId);
                        }
                        else {
                            if (arrayId.indexOf(obj.id) > -1) {
                                lstoObj2.push(obj);
                            }
                        }

                    }
                }
            }
        }

        function formatState(state) {

            if (isGroupClick) {
                $(TimKiem.SELECTORS.selected_option_layer_chil + ` option[value='${state.id}']`).eq(1).remove();
                isGroupClick = false;
                return groupNameChossne;
            }
            return state.text;
        };
    },

    // group những layer con của layer truyền vào
    GetLayerGroup: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            icon: null,
            parentId: data.parentId
        };

        var parent = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    if (data.tags != null) {
                        obj.icon = data.tags["icon"];
                    }
                }
            }

            obj.key = parent.id;

            var lstChil = TreeViewLayer.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = TreeViewLayer.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    // kiểm tra checked layer con
    CheckTreeInGroup: function (layer, arrayId) {
        var result = false;
        var check = arrayId.indexOf(layer.key); // nếu layer được check thì trả về true
        if (check != -1) {
            return true;
        }
        else {
            if (layer.children.length > 0) // kiểm tra layer con có được checked hay ko
            {
                $.each(layer.children, function (i, item) {
                    result = TimKiem.CheckTreeInGroup(item, arrayId);
                    if (result) // nếu có thì kết thúc loop và trả về giá trị true
                    {
                        result = true;
                        return false;
                    }
                });
            }
            else {
                return false;
            }
        }

        return result;
    },

    SearchAll: function (layerChils, array, arrayMapTitle, poly) {
        var objResult =
        {
            arrayResult: array,
            arrayMaptileResult: arrayMapTitle
        };

        $.each(layerChils.children, function (index, itemLayer) {

            var searchLayer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == itemLayer.key); // kiểm tra layer có nằm trong danh sách checked hay k?
            var checkMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == itemLayer.key); //kiểm tra layer có maptile option 2,3.
            if (typeof searchLayer !== "undefined" && typeof checkMaptile === "undefined") {
                searchLayer.title = itemLayer.title;
                searchLayer.key = itemLayer.key;

                var objectSearchResult = TimKiem.Searching(searchLayer, poly); // tìm kiếm đối tượng
                objResult.arrayResult.push(objectSearchResult);
            } else if (typeof checkMaptile !== "undefined") { //search maptile
                var objSearchMaptileResult = TimKiem.SearchBBoxMaptile(checkMaptile, poly);
                if (objSearchMaptileResult != null) {
                    objResult.arrayMaptileResult.push(objSearchMaptileResult);
                }
            }

            //var searchLayer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == itemLayer.key); // kiểm tra layer có nằm trong danh sách checked hay k?
            //if (searchLayer != undefined) {
            //    searchLayer.title = itemLayer.title;
            //    searchLayer.key = itemLayer.key;

            //    var objectSearchResult = TimKiem.Searching(searchLayer, poly); // tìm kiếm đối tượng
            //    arrayResult.push(objectSearchResult);
            //}

            if (itemLayer.children.length > 0) {
                objResult = TimKiem.SearchAll(itemLayer, objResult.arrayResult, objResult.arrayMaptileResult, poly);
            }
        });

        return objResult;
    },
    // tìm kiếm đối tượng
    Searching: function (searchLayer, poly) {
        var objectSearchResult =
        {
            id: searchLayer.key,
            name: searchLayer.title,
            optionDirectory: 1,
            listProperties: searchLayer.listProperties,
            listMainObject: []
        };

        if (poly != null) // nếu tìm kiếm trong hình học
        {
            if (poly.geometry.coordinates.length > 0) {
                objectSearchResult.listMainObject = TimKiem.SearchLayerInPoly(searchLayer, poly);
            }
        }
        else {
            objectSearchResult.listMainObject = searchLayer.listMainObject;
            //TimKiem.GetPropertiesByArrayMainObject(objectSearchResult.listMainObject.map(x => x.id), arrayMainResult);
        }

        return objectSearchResult;
    },
    // tìm kiếm layer trong hình học
    SearchLayerInPoly: function (layer, poly) {
        var lstResult = [];
        if (layer != undefined) {
            $.each(layer.listMainObject, function (i, item) {
                if (item.geometry != undefined && item.geometry != null) {
                    var inPolygon = TimKiem.CheckInGeometry(poly, item.geometry.type.toLowerCase(), item);
                    if (inPolygon) {
                        lstResult.push(item);
                    }
                }
            })
        }

        return lstResult;
    },
    // tìm kiếm đối tượng theo thuộc tính (ops1)
    SearchLayerByProperties: function (arrayLayer, value) {
        var arraySearchResult = [];
        var dto = {
            arrayDirectoryId: arrayLayer,
            search: value
        }

        $.ajax({
            type: "POST",
            url: TimKiem.CONSTS.URL_SEARCH_MAIN_OBJECT_BY_PROPERTIES_VALUE,
            data: JSON.stringify(dto),
            async: false,
            beforeSend: function () {
                $(TimKiem.SELECTORS.result_content_search).addClass('open');
                //TimKiem.OpenSpinner(); // hiển thị loader
            },
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                // group đối tượng theo nhóm layer
                var group_to_values = data.result.reduce(function (obj, item) {
                    obj[item.idDirectory] = obj[item.idDirectory] || [];
                    obj[item.idDirectory].push(item);
                    return obj;
                }, {});

                var groups = Object.keys(group_to_values).map(function (key) {
                    return { idDirectory: key, listMainObject: group_to_values[key] };
                });

                // convert object
                arraySearchResult = groups.map(function (obj, item) {
                    var layer = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == obj.idDirectory);
                    var property = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == obj.idDirectory);
                    var objectSearchResult =
                    {
                        id: obj.idDirectory,
                        name: layer.name,
                        listProperties: property.listProperties,
                        listMainObject: property.listMainObject.filter(x => obj.listMainObject.some(y => y.idMainObject == x.id))
                    };
                    return objectSearchResult;
                });

                //TimKiem.GLOBAL.SearchPaging.ListId = data.result.map(x => x.idMainObject);
                //if (TimKiem.GLOBAL.SearchPaging.ListId.length > 0) {
                //    TimKiem.SearchPaging(TimKiem.GLOBAL.SearchPaging.ListId, arraySearchResult);
                //    //TimKiem.GetPropertiesByArrayMainObject(listMainObject.map(x => x.id), arrayMainResult);
                //}

                //$(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (${TimKiem.GLOBAL.SearchPaging.ListId.length})`);
                //if (TimKiem.GLOBAL.SearchPaging.ListId.length == 0) {
                //    html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
                //    $(TimKiem.SELECTORS.list_search_result).html(html);
                //}
            },
            complete: function () {

            },
            error: function () {
                alert("Lỗi ajax");
            }
        });

        return arraySearchResult;
    },
    // tìm kiếm đối tượng theo thuộc tính (ops2, 3)
    SearchLayerMaptileByProperties: function (arrayLayer, value) {
        var lstParamMaptile = [];

        $.each(arrayLayer, function (index, layerid, path = []) {
            var checkMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == layerid); //kiểm tra layer có maptile option 2,3.
            if (checkMaptile != undefined) {
                var mapBouds = map.getBounds();
                var path = [mapBouds.northeast.lng, mapBouds.northeast.lat, mapBouds.southwest.lng, mapBouds.southwest.lat];
                var url = checkMaptile.url;
                var endcodeurl = new URL(url);
                domain = endcodeurl.origin + endcodeurl.pathname;
                layer = endcodeurl.searchParams.get("LAYERS");

                url = `${domain}?service=wfs&version=1.0.0&request=GetFeature&typeNames=${layer}&outputformat=application/json&CQL_FILTER=BBOX(geom, ${path})`;

                let param = { option: checkMaptile.optionDirectory, url: url };
                lstParamMaptile.push(param);
            }
        });

        var objParam = {
            Search: value,
            Params: lstParamMaptile
        };

        var result = null;
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: "/api/LayerToLayer/LayerGeo/searchPropetiesInfor",
            data: JSON.stringify(objParam),
            success: function (res) {
                if (res.code == "ok") {
                    if (res.properties != null && res.properties != 'null') {
                        console.log(res);
                        result = {
                            id: res.properties.idDirectory,
                            name: res.properties.nameTypeDirectory,
                            optionDirectory: res.properties.optionDirectory,
                            listProperties: res.properties.listProperties,
                            listMainObject: res.result
                        };
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

        return result;
    },
    // kiểm tra layer trong hình học
    CheckInGeometry: function (polygon, type, item) {
        var arrayPoint = [];
        var result = false;
        try {
            switch (type) {
                case "point":
                    var pt = turf.point([item.geometry.coordinates[0], item.geometry.coordinates[1]]);
                    var checkInPolygon = turf.booleanPointInPolygon(pt, polygon);
                    if (checkInPolygon) {
                        result = true;
                    }
                    break;
                case "multipolygon":
                    if (item.geometry != null && item.geometry.coordinates != null) {
                        var polyItem = turf.multiPolygon(item.geometry.coordinates);
                        var intersection = turf.intersect(polygon, polyItem);
                        if (intersection != null) {
                            result = true;
                        }
                    }
                    break;
                case "polygon":
                    if (item.geometry != null && item.geometry.coordinates != null) {
                        var polyItem = turf.polygon(item.geometry.coordinates);
                        var intersection = turf.intersect(polygon, polyItem);
                        if (intersection != null) {
                            result = true;
                        }
                    }
                    break;
                case "multipoint":
                    if (item.geometry != null && item.geometry.coordinates != null) {
                        arrayPoint = [];
                        $.each(item.geometry.coordinates, function (indexMutilPoint, itemMutilPoint) {
                            arrayPoint.push([itemMutilPoint[0], itemMutilPoint[1]]);
                        });

                        var points = turf.points(arrayPoint);
                        var contatns = turf.pointsWithinPolygon(points, polygon);
                        if (contatns != null && contatns.features.length > 0) {
                            result = true;
                        }
                    }
                    break;
                case "linestring":
                case "polyline":
                    if (item.geometry != null && item.geometry.coordinates != null) {
                        var lineItem = turf.lineString(item.geometry.coordinates);
                        var intersection = turf.lineIntersect(polygon, lineItem);
                        if (intersection != null && intersection.features.length > 0) {
                            result = true;
                        }
                        else {
                            arrayPoint = [];
                            $.each(item.geometry.coordinates, function (indexMutilPoint, itemMutilPoint) {
                                arrayPoint.push([itemMutilPoint[0], itemMutilPoint[1]]);
                            });

                            var points = turf.points(arrayPoint);
                            var contatns = turf.pointsWithinPolygon(points, polygon);
                            if (contatns != null && contatns.features.length > 0) {
                                result = true;
                            }
                        }
                    }

                    break;
                case "multipolyline":
                case "multilinestring":
                    if (item.geometry != null && item.geometry.coordinates != null) {
                        var lineItem = turf.lineString(item.geometry.coordinates[0]);
                        var intersection = turf.lineIntersect(polygon, lineItem);
                        if (intersection != null && intersection.features.length > 0) {
                            result = true;
                        }
                        else {
                            arrayPoint = [];
                            $.each(item.geometry.coordinates, function (indexMutilPoint, itemMutilPoint) {
                                $.each(itemMutilPoint, function (indexArrayPoints, itemArrayPoints) {
                                    arrayPoint.push([itemArrayPoints[0], itemArrayPoints[1]]);
                                })
                            });

                            var points = turf.points(arrayPoint);
                            var contatns = turf.pointsWithinPolygon(points, polygon);
                            if (contatns != null && contatns.features.length > 0) {
                                result = true;
                            }
                        }
                    }

                    break;
                default:
            }
        }
        catch (e) {
            console.log("eerror: check poylgon");
            result = false;
        }
        return result;
    },

    //-----------------------vẽ hình học tìm kiếm-------------------------------------------------------------
    // đa giác
    // vẽ polygon
    DrawPolygon: function () {
        var iLatLng = [];
        var iLatLngDienTich = [];
        $.each(TimKiem.GLOBAL.ArrayPolygon.ListPoint, function () {
            let latLng = { lng: this.getPosition().lng, lat: this.getPosition().lat };

            iLatLng.push(latLng);

            iLatLngDienTich.push([this.getPosition().lng, this.getPosition().lat]);
        });

        // tính diện tích
        let measure = new map4d.Measure([])
        measure.setPath(
            iLatLngDienTich
        );

        TimKiem.GLOBAL.ArrayPolygon.Meter = measure;

        //drawing polygon
        iLatLng.push(iLatLng[0]);

        if (TimKiem.GLOBAL.ArrayPolygon.Polygon != null) {
            TimKiem.GLOBAL.ArrayPolygon.Polygon.setMap(null);
        }

        let polygonOption = map4d.PolygonOptions = {
            paths: [iLatLng],
            fillOpacity: 0.5,
            fillColor: "#F9F4E4",
            strokeWidth: 2.0,
            strokeColor: "#00559A",
            zIndex: 2
        }

        var polygon = new map4d.Polygon(polygonOption)
        //thêm object vào map
        polygon.setMap(map);

        TimKiem.GLOBAL.ArrayPolygon.Polygon = polygon;

        TimKiem.ShowInfoDT(); // hiển thị diện tích
    },
    // tạo đoạn thẳng polygon
    createPolylineLoDat: function (path) {
        //tạo đối tượng polyline từ PolylineOptions
        var polylineDistance = new map4d.Polyline({
            path: path, visible: true, strokeColor: "#00559A", strokeWidth: 2.0, strokeOpacity: 2.0,
            closed: false,
            zIndex: 2
        })
        //thêm polyline vào map
        polylineDistance.setMap(map);
        if (TimKiem.GLOBAL.ArrayPolygon.Polyline != null) {
            TimKiem.GLOBAL.ArrayPolygon.Polyline.setMap(null);
        }

        TimKiem.GLOBAL.ArrayPolygon.Polyline = polylineDistance
    },
    // tạo các điểm của polygon
    createPointMutiplePolygon: function (lat, lng) {
        var isDraw = true;
        if (TimKiem.GLOBAL.ArrayPolygon.ListPoint != null && TimKiem.GLOBAL.ArrayPolygon.ListPoint.length > 0) {
            var locationLast = TimKiem.GLOBAL.ArrayPolygon.ListPoint[TimKiem.GLOBAL.ArrayPolygon.ListPoint.length - 1].getPosition();
            if (locationLast.lat == lat && locationLast.lng == lng) {
                isDraw = false;
            }
        }

        if (isDraw) {
            let markerDraw = new map4d.Marker({
                position: { lat: lat, lng: lng },
                icon: new map4d.Icon(8, 8, location.origin + "/common/EllipseBlue.svg"),
                anchor: [0.5, 0.5],
                draggable: false,
                zIndex: 2
                //title: name
            })
            markerDraw.setMap(map);

            if (TimKiem.GLOBAL.ArrayPolygon.ListPoint == null) {
                TimKiem.GLOBAL.ArrayPolygon.ListPoint = [];
            }

            TimKiem.GLOBAL.ArrayPolygon.ListPoint.push(markerDraw);
        }
    },
    //tính diện tích poylgon
    ShowPolyGonInfoMap: function () {
        var regt = 0;
        if (TimKiem.GLOBAL.ArrayPolygon.Meter != null) {
            regt = parseFloat(TimKiem.GLOBAL.ArrayPolygon.Meter.area).toFixed(2);
        }
        //var html = `<label>Diện tích: ${regt}(㎡)</label>`;
        //$(TimKiem.SELECTORS.body_infor_map).html(html);
        //$(TimKiem.SELECTORS.content_infor_map).addClass('open');
    },
    // xóa polygon
    RemovePolygon: function () {
        $(exploit.SELECTORS.content_infor_map).removeClass('open');
        // xóa điểm
        if (TimKiem.GLOBAL.ArrayPolygon.ListPoint != null) {
            for (var i = 0; i < TimKiem.GLOBAL.ArrayPolygon.ListPoint.length; i++) {
                TimKiem.GLOBAL.ArrayPolygon.ListPoint[i].setMap(null);
            }
        }

        if (TimKiem.GLOBAL.ArrayPolygon.Polyline != null) {
            TimKiem.GLOBAL.ArrayPolygon.Polyline.setMap(null);
        }


        if (TimKiem.GLOBAL.ArrayPolygon.Polygon != null) {
            TimKiem.GLOBAL.ArrayPolygon.Polygon.setMap(null);
        }

        TimKiem.GLOBAL.ArrayPolygon.Acreage = 0;
        TimKiem.GLOBAL.ArrayPolygon.ListPoint = null;
        TimKiem.GLOBAL.ArrayPolygon.Meter = null;
        TimKiem.GLOBAL.ArrayPolygon.Polyline = null;
        TimKiem.GLOBAL.ArrayPolygon.Polygon = null;

        if (polylineTemp != null) {
            polylineTemp.setMap(null);
            polylineTemp = null;
        }
    },
    // hiển thị thông tin polygon
    ShowInfoDT: function () {
        var regt = 0;
        if (TimKiem.GLOBAL.ArrayPolygon.Meter != null) {
            regt = parseFloat(TimKiem.GLOBAL.ArrayPolygon.Meter.area).toFixed(2);
        }

        if (!$(exploit.SELECTORS.content_infor_map).hasClass('open')) {
            var html = `<label>${l("Acreage")}: ${regt} (㎡)</label>`;
            $(exploit.SELECTORS.body_infor_map).html(html);
            $(exploit.SELECTORS.content_infor_map).addClass('open');
        }
        else {
            $(`${exploit.SELECTORS.body_infor_map} label`).html(`${l("Acreage")}: ${regt} (㎡)`);
        }
    },

    // vòng tròn
    // tạo tâm vòng tròn
    DrawCenterCircle: function (lat, lng) {
        let markerDraw = new map4d.Marker({
            position: { lat: lat, lng: lng },
            icon: new map4d.Icon(8, 8, location.origin + "/common/EllipseBlue.svg"),
            anchor: [0.5, 0.5],
            draggable: false,
            zIndex: 2
            //title: name
        })
        markerDraw.setMap(map);

        TimKiem.GLOBAL.DrawCircle.MarkerCenter = markerDraw;
    },
    // vẽ hình tròn
    DrawCircle: function (location) {
        var from = [TimKiem.GLOBAL.DrawCircle.Center.lng, TimKiem.GLOBAL.DrawCircle.Center.lat];
        var to = [location.lng, location.lat];
        var radius = parseInt(TimKiem.GetDistanceFrom2Point(from, to), 10);
        TimKiem.GLOBAL.DrawCircle.Radius = radius;
        if (radius > 0) {
            if (TimKiem.GLOBAL.DrawCircle.Circle != null) {
                TimKiem.GLOBAL.DrawCircle.Circle.setMap(null);
            }
            let circle = new map4d.Circle({
                center: TimKiem.GLOBAL.DrawCircle.Center,
                fillColor: "#F9F4E4", //"#28a745"
                fillOpacity: 0.5,
                radius: radius,
                strokeWidth: 2.0,
                strokeColor: "#00559A",
                zIndex: 2
            });

            circle.setMap(map);

            TimKiem.GLOBAL.DrawCircle.Circle = circle;
        }
    },
    // đo khoảng cách
    GetDistanceFrom2Point: function (from, to) {
        var from = turf.point(from);
        var to = turf.point(to);
        var options = { units: 'kilometers' };

        var distance = turf.distance(from, to, options);
        distance = distance * 1000;
        return distance;
    },
    // xóa vòng tròn
    RemoveCircle: function () {
        $(exploit.SELECTORS.content_infor_map).removeClass('open');
        if (TimKiem.GLOBAL.DrawCircle.MarkerCenter != null) {
            TimKiem.GLOBAL.DrawCircle.MarkerCenter.setMap(null);
        }

        if (TimKiem.GLOBAL.DrawCircle.Circle != null) {
            TimKiem.GLOBAL.DrawCircle.Circle.setMap(null);
        }

        TimKiem.GLOBAL.DrawCircle.MarkerCenter = null;
        TimKiem.GLOBAL.DrawCircle.Circle = null;
        TimKiem.GLOBAL.DrawCircle.Radius = 0;
        TimKiem.GLOBAL.DrawCircle.Center = null;
    },
    // hiển thị thông tin bán kính của vòng tròn
    ShowRadius: function (paths) {
        //var features = turf.points(paths);
        //var center = turf.center(features);

        //if (TimKiem.GLOBAL.DrawCircle.MeterInfo != null) {
        //    TimKiem.GLOBAL.DrawCircle.MeterInfo.setMap(null);
        //}

        var from = turf.point(paths[0]);
        var to = turf.point(paths[1]);
        var options = { units: 'meters' };

        var distance = turf.distance(from, to, options);

        //TimKiem.GLOBAL.DrawCircle.MeterInfo = new map4d.Marker({
        //    position: center.geometry.coordinates,
        //    anchor: [0.5, 1],
        //    visible: true,
        //    label: new map4d.MarkerLabel({ text: distance.toFixed(2) + " m", color: "000000", fontSize: 12 }),
        //    //icon: new map4d.Icon(1, 1, location.origin + "/common/yellow-point.png"),
        //    icon: new map4d.Icon(1, 1, "")
        //})
        //TimKiem.GLOBAL.DrawCircle.MeterInfo.setMap(map);

        //show info meter

        if (!$(exploit.SELECTORS.content_infor_map).hasClass('open')) {
            var html = `<label>${l("Radius")}: ${distance.toFixed(0)} (m)</label>`;
            $(exploit.SELECTORS.body_infor_map).html(html);
            $(exploit.SELECTORS.content_infor_map).addClass('open');
        }
        else {
            $(`${exploit.SELECTORS.body_infor_map} label`).html(`${l("Radius")}: ${distance.toFixed(0)} (m)`);
        }
    },

    // vẽ line

    //tạo point của đoạn thẳng
    createPointMutipleLine: function (lat, lng) {
        let markerDraw = new map4d.Marker({
            position: { lat: lat, lng: lng },
            icon: new map4d.Icon(8, 8, location.origin + "/common/EllipseBlue.svg"),
            anchor: [0.5, 0.5],
            draggable: false,
            zIndex: 2
            //title: name
        })
        markerDraw.setMap(map);

        if (TimKiem.GLOBAL.DrawLine.ListPoint == null) {
            TimKiem.GLOBAL.DrawLine.ListPoint = [];
        }

        TimKiem.GLOBAL.DrawLine.ListPoint.push(markerDraw);
    },
    // Đo chiều dài của đoạn thẳng
    ShowMeterDrawLine: function (endPoint, mousePoint) {
        let measure = new map4d.Measure([endPoint, mousePoint,]);
        let length = (Math.round(measure.length * 100) / 100).toString();

        TimKiem.GLOBAL.DrawLine.Distance += parseFloat(length)
    },
    // hiển thị meter của đoạn thẳng
    ShowPolylineInfoMap: function () {

        var html = `<div style="display : flex;">
                        <div style="font-weight: bold; margin-right: 6px;">${l("Length")}:</div><div> ${TimKiem.GLOBAL.DrawLine.Distance.toFixed(2)}(m)</div>
                    </div>`;

        $(exploit.SELECTORS.body_infor_map).html(html);
        $(exploit.SELECTORS.content_infor_map).addClass('open');
    },
    // vẽ polyline lên bản dồ
    DrawLine: function () {
        var iLatLng = [];
        $.each(TimKiem.GLOBAL.DrawLine.ListPoint, function (i, obj) {

            var latlng = [obj.getPosition().lng, obj.getPosition().lat];
            iLatLng.push(latlng);
        });

        // vẽ đường thẳng
        if (TimKiem.GLOBAL.DrawLine.Polyline != null) {
            TimKiem.GLOBAL.DrawLine.Polyline.setMap(null);
        }

        var polyline = new map4d.Polyline({
            path: iLatLng, visible: true, strokeColor: "#00559A", strokeWidth: 2.0, strokeOpacity: 2.0,
            closed: false,
            zIndex: 2
        })

        TimKiem.GLOBAL.DrawLine.Polyline = polyline;
        polyline.setMap(map);

        // vẽ chiều rộng của đoạn thẳng
        TimKiem.DrawBuffer();
    },
    //vẽ chiểu rộng của đoạn thẳng lên bản dồ
    DrawBuffer: function () {
        try {
            if (TimKiem.GLOBAL.DrawLine.Polyline != null) {
                var arrayPoint = [];
                // lấy danh sách tọa độ của polyline đã vẽ
                $.each(TimKiem.GLOBAL.DrawLine.Polyline.path, function (index, path) {
                    arrayPoint.push([path.lng, path.lat]);
                });

                // tạo polyline tufn để tạo buffer cho polyline
                var lineString = turf.lineString(arrayPoint);
                var meter = 100;

                if ($(TimKiem.SELECTORS.meter_buffer_line).val() != "") {
                    meter = parseInt($(TimKiem.SELECTORS.meter_buffer_line).val());

                    if (meter < 0) {
                        meter = 100;
                        $(TimKiem.SELECTORS.meter_buffer_line).val(100);
                    }
                }
                var buffered = turf.buffer(lineString, meter, { units: 'meters' });

                if (buffered != undefined) {
                    if (TimKiem.GLOBAL.DrawLine.Buffer != null) {
                        TimKiem.GLOBAL.DrawLine.Buffer.setMap(null);
                    }

                    let polygonBufferOption = map4d.PolygonOptions = {
                        paths: buffered.geometry.coordinates,
                        fillOpacity: 0.5,
                        fillColor: "#F9F4E4",
                        strokeWidth: 2.0,
                        strokeColor: "#00559A",
                        zIndex: 1
                    }

                    var polygonBuffer = new map4d.Polygon(polygonBufferOption)
                    //thêm object vào map
                    polygonBuffer.setMap(map);

                    TimKiem.GLOBAL.DrawLine.Buffer = polygonBuffer;
                }
            }
        }
        catch (err) { console.log('Error: Draw buffer line !!!') }
    },
    // xóa vẽ line
    RemoveLine: function () {
        $(exploit.SELECTORS.content_infor_map).removeClass('open');
        // xóa điểm
        if (TimKiem.GLOBAL.DrawLine.ListPoint != null) {
            for (var i = 0; i < TimKiem.GLOBAL.DrawLine.ListPoint.length; i++) {
                TimKiem.GLOBAL.DrawLine.ListPoint[i].setMap(null);
            }
        }

        // xóa line
        if (TimKiem.GLOBAL.DrawLine.Polyline != null) {
            TimKiem.GLOBAL.DrawLine.Polyline.setMap(null);
        }

        // xóa buffer
        if (TimKiem.GLOBAL.DrawLine.Buffer != null) {
            TimKiem.GLOBAL.DrawLine.Buffer.setMap(null);
        }

        TimKiem.GLOBAL.DrawLine.ListPoint = null;
        TimKiem.GLOBAL.DrawLine.Distance = 0;
        TimKiem.GLOBAL.DrawLine.Polyline = null;
        TimKiem.GLOBAL.DrawLine.Buffer = null;

        if (polylineTemp != null) {
            polylineTemp.setMap(null);
            polylineTemp = null;
        }
    },

    //Tạo polyline tạm
    createPolylineByMouseMoveLoDat: function (path, strokeWidth, strokeOpacity) {
        if (polylineTemp != null) {
            polylineTemp.setMap(null);
        }
        //tạo đối tượng polyline từ PolylineOptions
        polylineTemp = new map4d.Polyline({
            path: path, visible: true, strokeColor: "#00559A", strokeWidth: strokeWidth, strokeOpacity: strokeOpacity,
            closed: false
        })
        //thêm polyline vào map
        polylineTemp.setMap(map)
    },

    //------------------end vẽ hình học tìm kiếm------------------------------------------------------------------------------------------

    // xuất kết quả tìm kiếm ra màn hình
    OpenResultSearchContent: function (data, lstProperties) {
        $(TimKiem.SELECTORS.result_content_search).addClass('open');

        var html = ``;
        var STT = 0;

        $(TimKiem.SELECTORS.list_search_result).html(html);

        $.each(data, function (index, item) {
            $.each(item.listMainObject, function (indexObject, object) {
                var properties = lstProperties.find(x => x.idMainObject == object.id); // lấy thuộc tính của đối tượng
                var propertiesHTML = '';

                if (properties != undefined) // nếu đối tượng có thuộc tính động
                {
                    var lstpropertiesView = item.listProperties.filter(x => x.typeSystem != 1 && x.typeProperties != "geojson" && x.isShowSearchExploit); // lấy thuộc tính của layer
                    $.each(lstpropertiesView, function (indexProperties, itemPropertes) {
                        var property = properties.listProperties.find(x => x.codeProperties == itemPropertes.codeProperties); // map thuộc tính của layer và thuộc tính của đối tượng
                        var namePropertyView = itemPropertes.nameProperties; // tên thuộc tính được hiển thị
                        var valuePropertyView = ""; // value thuộc tính được hiển thị

                        if (property != undefined) {
                            if (itemPropertes.typeProperties !== "link" && itemPropertes.typeProperties !== "file" && itemPropertes.typeProperties !== "image") {
                                if (itemPropertes.typeProperties == "list" || itemPropertes.typeProperties == "radiobutton" || itemPropertes.typeProperties == "checkbox") {
                                    var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
                                    var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách

                                    try {
                                        lstValueDefault = JSON.parse(itemPropertes.defalutValue);
                                    }
                                    catch (e) {

                                    }

                                    if (lstValueDefault.length > 0) {
                                        if (property.defalutValue != "" && property.defalutValue != undefined && property.defalutValue != null) {
                                            lstValueObjectMain = property.defalutValue.split(',');
                                        }

                                        var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng

                                        valuePropertyView = lstMapValue.map(x => x.name).join(', ');
                                    }
                                }
                                else {
                                    valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
                                }

                                propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;

                            }
                        }
                        else {
                            propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;
                        }
                    });
                }

                html += `<li class="item-search-result" id="item-search-result-${object.id}" data-id="${object.id}" data-directory="${object.idDirectory}">
                            <div class="col-STT">   
                                <span>${STT + 1}</span>
                            </div>
                            <div class="col-Info">
                                <span class="text-info">${object.nameObject}</span>
                                <p>Lớp dữ liệu : ${item.name}</p>
                                ${propertiesHTML}
                            </div>
                         </li>`;
                STT++;
            })
        });

        if (STT > 0) {
            $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (${STT})`)
            $(TimKiem.SELECTORS.list_search_result).html(html);
        }
        else {
            $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
            html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
            $(TimKiem.SELECTORS.list_search_result).html(html);
        }

        $(TimKiem.SELECTORS.list_search_result).animate({
            scrollTop: 0
        }, 'slow');

        TimKiem.CloseSpinner(); // ẩn loader
    },
    OpenResultSearchContentNew: function (data, lstProperties) {
        $(TimKiem.SELECTORS.result_content_search).addClass('open');
        var html = ``;
        var STT = (TimKiem.GLOBAL.SearchPaging.Page - 1) * TimKiem.GLOBAL.SearchPaging.Size;

        data = data.length > 0 ? data : TimKiem.GLOBAL.ArrayObjectMain;
        $.each(lstProperties, function (i, obj) {
            $.each(data, function (index, item) {
                var objectMain = item.listMainObject.find(x => x.id == obj.idMainObject);
                var propertiesHTML = '';
                if (objectMain != undefined) // nếu đối tượng có thuộc tính động
                {
                    var lstpropertiesView = item.listProperties.filter(x => x.typeSystem != 1 && x.typeProperties != "geojson" && x.isShowSearchExploit); // lấy thuộc tính của layer
                    $.each(lstpropertiesView, function (indexProperties, itemPropertes) {
                        var property = obj.listProperties.find(x => x.codeProperties == itemPropertes.codeProperties); // map thuộc tính của layer và thuộc tính của đối tượng
                        var namePropertyView = itemPropertes.nameProperties; // tên thuộc tính được hiển thị
                        var valuePropertyView = ""; // value thuộc tính được hiển thị

                        if (property != undefined) {
                            if (itemPropertes.typeProperties !== "link" && itemPropertes.typeProperties !== "file" && itemPropertes.typeProperties !== "image") {
                                if (itemPropertes.typeProperties == "list" || itemPropertes.typeProperties == "radiobutton" || itemPropertes.typeProperties == "checkbox") {
                                    var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
                                    var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách

                                    try {
                                        lstValueDefault = JSON.parse(itemPropertes.defalutValue);
                                    }
                                    catch (e) {

                                    }

                                    if (lstValueDefault.length > 0) {
                                        if (property.defalutValue != "" && property.defalutValue != undefined && property.defalutValue != null) {
                                            lstValueObjectMain = property.defalutValue.split(',');
                                        }

                                        var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng

                                        valuePropertyView = lstMapValue.map(x => x.name).join(', ');
                                    }
                                }
                                else {
                                    valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
                                }

                                propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;

                            }
                        }
                        else {
                            propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;
                        }
                    });

                    let nameLayer = typeof item.title === "undefined" ? item.name : item.title;
                    html += `<li class="item-search-result" id="item-search-result-${objectMain.id}" data-id="${objectMain.id}" data-directory="${objectMain.idDirectory}">
                            <div class="col-STT">   
                                <span>${STT + 1}</span>
                            </div>
                            <div class="col-Info">
                                <span class="text-info">${objectMain.nameObject}</span>
                                <p>Lớp dữ liệu : ${nameLayer}</p>
                                ${propertiesHTML}
                            </div>
                         </li>`;
                    STT++;
                    $(TimKiem.SELECTORS.list_search_result).append(html);
                    html = ``;
                }
            });

        });

        //$.each(data, function (index, item) {
        //    $.each(item.listMainObject, function (indexObject, object) {
        //        var properties = lstProperties.find(x => x.idMainObject == object.id); // lấy thuộc tính của đối tượng
        //        var propertiesHTML = '';

        //        if (properties != undefined) // nếu đối tượng có thuộc tính động
        //        {
        //            var lstpropertiesView = item.listProperties.filter(x => x.typeSystem != 1 && x.typeProperties != "geojson" && x.isShowSearchExploit); // lấy thuộc tính của layer
        //            $.each(lstpropertiesView, function (indexProperties, itemPropertes) {
        //                var property = properties.listProperties.find(x => x.codeProperties == itemPropertes.codeProperties); // map thuộc tính của layer và thuộc tính của đối tượng
        //                var namePropertyView = itemPropertes.nameProperties; // tên thuộc tính được hiển thị
        //                var valuePropertyView = ""; // value thuộc tính được hiển thị

        //                if (property != undefined) {
        //                    if (itemPropertes.typeProperties !== "link" && itemPropertes.typeProperties !== "file" && itemPropertes.typeProperties !== "image") {
        //                        if (itemPropertes.typeProperties == "list" || itemPropertes.typeProperties == "radiobutton" || itemPropertes.typeProperties == "checkbox") {
        //                            var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
        //                            var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách

        //                            try {
        //                                lstValueDefault = JSON.parse(itemPropertes.defalutValue);
        //                            }
        //                            catch (e) {

        //                            }

        //                            if (lstValueDefault.length > 0) {
        //                                if (property.defalutValue != "" && property.defalutValue != undefined && property.defalutValue != null) {
        //                                    lstValueObjectMain = property.defalutValue.split(',');
        //                                }

        //                                var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng

        //                                valuePropertyView = lstMapValue.map(x => x.name).join(', ');
        //                            }
        //                        }
        //                        else {
        //                            valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
        //                        }

        //                        propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;

        //                    }
        //                }
        //                else {
        //                    propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;
        //                }
        //            });
        //        }

        //        html += `<li class="item-search-result" id="item-search-result-${object.id}" data-id="${object.id}" data-directory="${object.idDirectory}">
        //                    <div class="col-STT">   
        //                        <span>${STT + 1}</span>
        //                    </div>
        //                    <div class="col-Info">
        //                        <span class="text-info">${object.nameObject}</span>
        //                        <p>Lớp dữ liệu : ${item.name}</p>
        //                        ${propertiesHTML}
        //                    </div>
        //                 </li>`;
        //        STT++;
        //    })
        //});

        //if (STT > 0) {
        //    $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (${STT})`)
        //    $(TimKiem.SELECTORS.list_search_result).html(html);
        //}
        //else {
        //    $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
        //    html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
        //    $(TimKiem.SELECTORS.list_search_result).html(html);
        //}
        /*$(TimKiem.SELECTORS.list_search_result).append(html);*/

        //$(TimKiem.SELECTORS.list_search_result).animate({
        //    scrollTop: 0
        //}, 'slow');

        TimKiem.CloseSpinner(); // ẩn loader
    },

    CloseResultSearchContent: function () {
        $(TimKiem.SELECTORS.result_content_search).removeClass('open');
        TimKiem.GLOBAL.IsResetDrawSearch = false;
    },
    OpenSpinner: function () {
        $(TimKiem.SELECTORS.body_search_result).hide();
        $(TimKiem.SELECTORS.spinner).show();

    },
    CloseSpinner: function () {
        $(TimKiem.SELECTORS.body_search_result).show();
        $(TimKiem.SELECTORS.spinner).hide();
    },

    // lấy thuộc tính của nhóm đối tượng
    GetPropertiesByArrayMainObject: function (array, arrayMainResult) {
        var dto = {
            arrayId: array
        };

        $.ajax({
            type: "POST",
            url: TimKiem.CONSTS.URL_GET_PROPERTIES_BY_ARRAY_MAIN_OBJECT,
            data: JSON.stringify(dto),
            async: false,
            beforeSend: function () {
                $(TimKiem.SELECTORS.result_content_search).addClass('open');
                //TimKiem.OpenSpinner(); // hiển thị loader
            },
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //setTimeout(function () {
                if (data.code == "ok") {
                    TimKiem.OpenResultSearchContentNew(arrayMainResult, data.result);
                }
                else {
                    $(`${TimKiem.SELECTORS.head_search_result} span`).html(`${l("SearchResult")} (0)`);
                    html = `<p class="search-list-not-found text-center" style="font-style: italic; padding:10px;">${l("SearchResultNull")}</p>`;
                    $(TimKiem.SELECTORS.list_search_result).html(html);
                }
                //}, 10);
            },
            complete: function () {

            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    // dữ liệu tìm kiếm
    // thêm danh sách thuộc tính của layer được checked
    AddPropertiesLayer: function (data, id) {
        // dữ liệu tìm kiếm
        var indexTimkiem = TimKiem.GLOBAL.ArrayObjectMain.findIndex(x => x.id == id);
        if (indexTimkiem == -1) {
            var objTimkiem = {
                id: id,
                listProperties: data,
                listMainObject: []
            };
            TimKiem.GLOBAL.ArrayObjectMain.push(objTimkiem);
        }
        else {
            TimKiem.GLOBAL.ArrayObjectMain[indexTimkiem].listProperties = data;
        }
    },
    // thêm danh sách đối tượng của layer được checked
    AddMainObjectLayer: function (data, id) {
        // dữ liệu tìm kiếm
        var indexTimkiem = TimKiem.GLOBAL.ArrayObjectMain.findIndex(x => x.id == id);
        if (indexTimkiem == -1) {
            var objTimkiem = {
                id: id,
                listProperties: [],
                listMainObject: data
            };
            TimKiem.GLOBAL.ArrayObjectMain.push(objTimkiem);
        }
        else {
            //TimKiem.GLOBAL.ArrayObjectMain[indexTimkiem].listMainObject = data;
            TimKiem.GLOBAL.ArrayObjectMain[indexTimkiem].listMainObject = TimKiem.GLOBAL.ArrayObjectMain[indexTimkiem].listMainObject.concat(data);
        }
    },
    OpenContentTimKiem: function () {
        if (!$(TimKiem.SELECTORS.content_search).hasClass('open')) {
            //exploit.CancelAction();
            $(TimKiem.SELECTORS.selected_option_layer_chil).html('');
            $(TimKiem.SELECTORS.selected_option_folder_parent).html('');

            TimKiem.SelectFolderParent();
            $(TimKiem.SELECTORS.content_search).addClass('open');
            $(TimKiem.SELECTORS.btn_open_content_search).addClass('isForcus');


            $('.div-content').addClass('toolge-search');
            $(this).attr('data-original-title', l("Main:Search"));

            // đóng tìm kiếm theo lớp
            $(TimKiem.SELECTORS.check_box_search_layer).prop('checked', false);
            $(TimKiem.SELECTORS.content_chil_layer).slideUp("slow");

            if ($('.div-content').hasClass('toogle-tree')) {
                $('.toogle-tree-bar').click();
            }
        }
    },
    CloseContentTimKiem: function () {
        $(TimKiem.SELECTORS.content_search).removeClass('open');
        $(TimKiem.SELECTORS.btn_open_content_search).removeClass('isForcus');
        $(TimKiem.SELECTORS.btn_open_content_search).attr('data-original-title', l("Main:Search"));
        $(TimKiem.SELECTORS.ul_chossen_search_retage + " li").removeClass('isForcus');
        $('.div-content').removeClass('toolge-search');

        TimKiem.GLOBAL.isDrawSearch = -1;
        TimKiem.GLOBAL.SearchFollowGeometry = -1;

        $(TimKiem.SELECTORS.content_input_buffer).slideUp("slow"); // ẩn input buffer line
        $(TimKiem.SELECTORS.input_search_properties).val(""); // clear tìm kiếm theo thuộc tính

        TimKiem.RemovePolygon();
        TimKiem.RemoveCircle();
        TimKiem.RemoveLine();
        TimKiem.CloseResultSearchContent();
    },
    //search paging new
    SearchPaging: function (listid, arrayMainResult = []) {
        if (this.GLOBAL.SearchPaging.Page == 1) {
            $(TimKiem.SELECTORS.list_search_result).html("");
        }
        let listidSearch = this.getListObjectPaging(listid, this.GLOBAL.SearchPaging.Page, this.GLOBAL.SearchPaging.Size);
        if (listidSearch.length > 0) {
            TimKiem.GetPropertiesByArrayMainObject(listidSearch, arrayMainResult);
        }
    },
    getListObjectPaging: function (array, index, size) {
        index = Math.abs(parseInt(index));
        index = index > 0 ? index - 1 : index;
        size = parseInt(size);
        size = size < 1 ? 1 : size;

        // filter
        return [...(array.filter((value, n) => {
            return (n >= (index * size)) && (n < ((index + 1) * size))
        }))]
    },
    //#region search paging new maptile
    //search box option 2
    SearchBBoxMaptile: function (object, path = []) {
        var result = [];
        let url = this.getUrlInforMapTile(object.url, path);
        let param = [{ option: object.optionDirectory, url: url }];
        result = this.getInforServerMapTile(param);

        return result;
    },
    getUrlInforMapTile: function (url, path = [], checkVN = false) {
        let bbox = path != null ? this.ConvertPathToShape(path, checkVN) : this.ConvertDrawToShape(TimKiem.GLOBAL.ArrayPolygon.Polygon.getPaths(), "polygon", checkVN);
        var endcodeurl = new URL(url);
        domain = endcodeurl.origin + endcodeurl.pathname;
        layer = endcodeurl.searchParams.get("LAYERS");
        if (bbox.polygon.length > 0 && bbox.polygon.length < 1600) {
            url = `${domain}?service=wfs&version=1.0.0&request=GetFeature&typeNames=${layer}&outputformat=application/json&CQL_FILTER=INTERSECTS(geom,POLYGON${bbox.polygon})`;
        } else
            url = `${domain}?service=wfs&version=1.0.0&request=GetFeature&typeNames=${layer}&outputformat=application/json&CQL_FILTER=BBOX(geom, ${bbox.bbox})`;
        return url;
    },
    getInforServerMapTile: function (param) {
        var result = null;
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: "application/json-patch+json",
            async: false,
            url: "/api/LayerToLayer/LayerGeo/searchBboxInfor",
            data: JSON.stringify(param),
            success: function (res) {
                if (res.code == "ok") {

                    if (res.properties != null && res.properties != 'null') {
                        console.log(res);
                        result = {
                            id: res.properties.idDirectory,
                            name: res.properties.nameTypeDirectory,
                            optionDirectory: res.properties.optionDirectory,
                            listProperties: res.properties.listProperties,
                            listMainObject: res.result
                        };
                    }

                    TimKiem.OpenResultSearchContentMaptile(res.result, res.properties);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });

        return result;
    },
    SendDataTile: function (param) {

        $.ajax({
            type: "GET",
            async: false,
            crossDomain: true,
            url: url,
            success: function (data) {
                console.log(data);
                //PlanShap.CheckListShape(data.features);
                //PlanShap.ShowInforCheckPlan(PlanShap.GLOBAL.listShape);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    },
    ConvertDrawToShape: function (Drawpolygon, type, checkVN = false) {
        //var Drawpolygon = TimKiem.GLOBAL.ArrayPolygon.Polygon.getPaths()[0];
        let maxlat = "", minlat = "", maxlng = "", minlng = "", polygon = "(";
        let re = {};
        TimKiem.GLOBAL.polygonVN200 = [];
        if (type.toLowerCase() === "polygon") {
            $.each(Drawpolygon, function (i, object) {
                let objpl = "(";
                $.each(object, function (j, latlng) {
                    if (maxlat === "") {
                        maxlat = latlng.lat;
                        minlat = latlng.lat;
                        maxlng = latlng.lng;
                        minlng = latlng.lng;
                    } else {
                        if (maxlng < latlng.lng) maxlng = latlng.lng;
                        if (minlng > latlng.lng) minlng = latlng.lng;
                        if (maxlat < latlng.lat) maxlat = latlng.lat;
                        if (minlat > latlng.lat) minlat = latlng.lat;
                    }
                    if (checkVN) {
                        let location = ConvertLocationMap.WGS84toVN2000(latlng.lat, latlng.lng, TimKiem.CONSTS.KTTruc, TimKiem.CONSTS.Zone);
                        objpl = objpl.length > 1 ? (objpl + "," + location[1] + " " + location[0]) : (objpl + location[1] + " " + location[0]);
                        TimKiem.GLOBAL.polygonVN200.push([location[1], location[0]]);
                    } else {
                        objpl = objpl.length > 1 ? (objpl + "," + latlng.lng + " " + latlng.lat) : (objpl + latlng.lng + " " + latlng.lat);
                        //Timkiem.GLOBAL.polygonVN200.push([location[1], location[0]]);
                    }
                });
                objpl += ")";
                polygon = polygon.length > 2 ? (polygon + "," + objpl) : (polygon + objpl);
            });
            polygon += ")";
            re = {
                polygon: polygon,
                bbox: minlng + "," + minlat + "," + maxlng + "," + maxlat
            }
            if (checkVN) {
                let locationMin = ConvertLocationMap.WGS84toVN2000(minlat, minlng, TimKiem.CONSTS.KTTruc, TimKiem.CONSTS.Zone);
                let locationMax = ConvertLocationMap.WGS84toVN2000(maxlat, maxlng, TimKiem.CONSTS.KTTruc, TimKiem.CONSTS.Zone);
                re.bbox = locationMin[1] + "," + locationMin[0] + "," + locationMax[1] + "," + locationMax[0];
            }
        }
        return re;
    },
    ConvertPathToShape: function (geojson, checkVN = false) {
        var Drawpolygon = geojson.geometry.coordinates;
        let maxlat = "", minlat = "", maxlng = "", minlng = "", polygon = "(";
        let re = {};
        TimKiem.GLOBAL.polygonVN200 = [];
        if (geojson.geometry.type.toLowerCase() === "polygon") {
            $.each(Drawpolygon, function (i, object) {
                let objpl = "(";
                $.each(object, function (j, latlng) {
                    if (maxlat === "") {
                        maxlat = latlng[1];
                        minlat = latlng[1];
                        maxlng = latlng[0];
                        minlng = latlng[0];
                    } else {
                        if (maxlng < latlng[0]) maxlng = latlng[0];
                        if (minlng > latlng[0]) minlng = latlng[0];
                        if (maxlat < latlng[1]) maxlat = latlng[1];
                        if (minlat > latlng[1]) minlat = latlng[1];
                    }
                    if (checkVN) {
                        let location = ConvertLocationMap.WGS84toVN2000(latlng[1], latlng[0], TimKiem.CONSTS.KTTruc, TimKiem.CONSTS.Zone);
                        objpl = objpl.length > 1 ? (objpl + "," + location[1] + " " + location[0]) : (objpl + location[1] + " " + location[0]);
                        TimKiem.GLOBAL.polygonVN200.push([location[1], location[0]]);
                    } else {
                        objpl = objpl.length > 1 ? (objpl + "," + latlng[0] + " " + latlng[1]) : (objpl + latlng[0] + " " + latlng[1]);
                        //polygonPath.push([latlng[1], latlng[0]]);
                    }
                });
                objpl += ")";
                polygon = polygon.length > 2 ? (polygon + "," + objpl) : (polygon + objpl);
            });
            polygon += ")";
            re = {
                polygon: polygon,
                bbox: minlng + "," + minlat + "," + maxlng + "," + maxlat,
                //polygonPath: polygonPath
            }
            if (checkVN) {
                let locationMin = ConvertLocationMap.WGS84toVN2000(minlat, minlng, TimKiem.CONSTS.KTTruc, TimKiem.CONSTS.Zone);
                let locationMax = ConvertLocationMap.WGS84toVN2000(maxlat, maxlng, TimKiem.CONSTS.KTTruc, TimKiem.CONSTS.Zone);
                re.bbox = locationMin[1] + "," + locationMin[0] + "," + locationMax[1] + "," + locationMax[0];
            }
        }
        return re;
    },
    OpenResultSearchContentMaptile: function (data, properties) {
        $(TimKiem.SELECTORS.result_content_search).addClass('open');
        $(TimKiem.SELECTORS.list_search_result).html("");
        var html = ``;
        var STT = 0;//(TimKiem.GLOBAL.SearchPaging.Page - 1) * TimKiem.GLOBAL.SearchPaging.Size;


        //data = data.length > 0 ? data : TimKiem.GLOBAL.ArrayObjectMain;
        $.each(data, function (index, item) {
            //var objectMain = item.listMainObject.find(x => x.id == obj.idMainObject);
            var propertiesHTML = '';
            //if (objectMain != undefined) // nếu đối tượng có thuộc tính động
            //{
            var lstpropertiesView = properties.listProperties.filter(x => x.typeSystem != 1 && x.typeProperties != "geojson" && x.isShowSearchExploit); // lấy thuộc tính của layer
            $.each(lstpropertiesView, function (indexProperties, itemPropertes) {
                var property = item.listProperties.find(x => x.codeProperties == itemPropertes.codeProperties); // map thuộc tính của layer và thuộc tính của đối tượng
                var namePropertyView = itemPropertes.nameProperties; // tên thuộc tính được hiển thị
                var valuePropertyView = ""; // value thuộc tính được hiển thị

                if (property != undefined) {
                    if (itemPropertes.typeProperties !== "link" && itemPropertes.typeProperties !== "file" && itemPropertes.typeProperties !== "image") {
                        if (itemPropertes.typeProperties == "list" || itemPropertes.typeProperties == "radiobutton" || itemPropertes.typeProperties == "checkbox") {
                            var lstValueDefault = []; // danh sách value của thuộc tính dạng danh sách
                            var lstValueObjectMain = []; // danh sách value của đối tượng dạng danh sách

                            try {
                                lstValueDefault = JSON.parse(itemPropertes.defalutValue);
                            }
                            catch (e) {

                            }

                            if (lstValueDefault.length > 0) {
                                if (property.defalutValue != "" && property.defalutValue != undefined && property.defalutValue != null) {
                                    lstValueObjectMain = property.defalutValue.split(',');
                                }

                                var lstMapValue = lstValueDefault.filter(x => lstValueObjectMain.indexOf(x.code) > -1); // map 2 thuộc tính danh sách của layer và đối tượng

                                valuePropertyView = lstMapValue.map(x => x.name).join(', ');
                            }
                        }
                        else {
                            valuePropertyView = property.defalutValue == null ? "" : property.defalutValue; // set value cho thuộc tính
                        }

                        propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;

                    }
                }
                else {
                    propertiesHTML += `<p>${namePropertyView} : ${valuePropertyView}</p>`;
                }
            });

            let nameLayer = properties.nameTypeDirectory;
            html += `<li class="item-search-result" id="item-search-result-${item.id}" data-id="${item.id}" data-directory="${properties.idDirectory}">
                            <div class="col-STT">   
                                <span>${STT + 1}</span>
                            </div>
                            <div class="col-Info">
                                <span class="text-info">${item.nameObject}</span>
                                <p>Lớp dữ liệu : ${nameLayer}</p>
                                ${propertiesHTML}
                            </div>
                         </li>`;
            STT++;
            $(TimKiem.SELECTORS.list_search_result).append(html);
            html = ``;
            //}
        });

        TimKiem.CloseSpinner(); // ẩn loader
    },
    //#endregion

    ChoosenTypeDrawSearch: function (args) {
        let iLatLng = [];
        switch (TimKiem.GLOBAL.isDrawSearch) {
            case 1:
                if (TimKiem.GLOBAL.IsResetDrawSearch) {
                    TimKiem.RemovePolygon();
                    TimKiem.GLOBAL.IsResetDrawSearch = false;
                }

                TimKiem.createPointMutiplePolygon(args.location.lat, args.location.lng);
                if (TimKiem.GLOBAL.ArrayPolygon.ListPoint.length == 2) {
                    let listarea = TimKiem.GLOBAL.ArrayPolygon.ListPoint;
                    $.each(listarea, function () {
                        let item = { lng: this.getPosition().lng, lat: this.getPosition().lat };
                        iLatLng.push(item);
                    })

                    TimKiem.createPolylineLoDat(iLatLng);
                }
                else if (TimKiem.GLOBAL.ArrayPolygon.ListPoint.length > 2) {
                    TimKiem.DrawPolygon();
                    if (TimKiem.GLOBAL.ArrayPolygon.Polyline != null) {
                        TimKiem.GLOBAL.ArrayPolygon.Polyline.setMap(null);
                        TimKiem.GLOBAL.ArrayPolygon.Polyline = null;
                    }
                }
                break;
            case 2:
                if (TimKiem.GLOBAL.IsResetDrawSearch) {
                    TimKiem.RemoveCircle();
                    TimKiem.GLOBAL.IsResetDrawSearch = false;
                }

                let item = { lng: args.location.lng, lat: args.location.lat };

                if (TimKiem.GLOBAL.DrawCircle.Center != null) {
                    TimKiem.DrawCircle(item);

                    if (polylineTemp != null) {
                        polylineTemp.setMap(null);
                        polylineTemp = null;
                    }

                    TimKiem.GLOBAL.IsResetDrawSearch = true;

                    if (TimKiem.GLOBAL.DrawCircle.MeterInfo != null) {
                        TimKiem.GLOBAL.DrawCircle.MeterInfo.setMap(null);
                    }
                }
                else {

                    TimKiem.GLOBAL.DrawCircle.Center = item;
                    TimKiem.DrawCenterCircle(item.lat, item.lng);
                }
                break;
            case 3:
                if (TimKiem.GLOBAL.IsResetDrawSearch) {
                    TimKiem.RemoveLine();
                    TimKiem.GLOBAL.IsResetDrawSearch = false;
                }

                TimKiem.createPointMutipleLine(args.location.lat, args.location.lng);

                if (TimKiem.GLOBAL.DrawLine.ListPoint.length >= 2) {

                    let listMarkerMulti = TimKiem.GLOBAL.DrawLine.ListPoint;

                    $.each(listMarkerMulti, function (i, obj) {
                        let latLng = { lat: obj.getPosition().lat, lng: obj.getPosition().lng };
                        iLatLng.push(latLng);
                    });

                    if (iLatLng.length > 1) {
                        let startPoint = [iLatLng[iLatLng.length - 2].lng, iLatLng[iLatLng.length - 2].lat];
                        let endPoint = [iLatLng[iLatLng.length - 1].lng, iLatLng[iLatLng.length - 1].lat];
                        TimKiem.ShowMeterDrawLine(startPoint, endPoint);
                        TimKiem.ShowPolylineInfoMap();
                    }

                    TimKiem.DrawLine();

                }

                break;
            default:
        }
    },

    // lấy toàn bộ id childrent của layer cha truyền vào
    GetAllDirectoryByParent: function (layer, arrayOp1, arrayOp2) {
        var result = {
            arrayOp1: arrayOp1,
            arrayOp2: arrayOp2
        };

        $.each(layer.children, function (index, obj) {
            var checkMaptile = exploit.GLOBAL.listGroundOvelay.find(x => x.id == obj.key); //kiểm tra layer có maptile option 2,3.
            var checkLayer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == obj.key); // kiểm tra layer có nằm trong danh sách checked hay k?

            if (typeof checkLayer !== "undefined" && typeof checkMaptile === "undefined") {
                result.arrayOp1.push(obj.key);
            }
            else if (typeof checkMaptile !== "undefined") { //search maptile
                result.arrayOp2.push(obj.key);
            }

            if (obj.children.length > 0) {
                result = TimKiem.GetAllDirectoryByParent(obj, result.arrayOp1, result.arrayOp2);
            }
        });

        return result;
    }
};

var arraySearch = [];
var searchDefault = "";
function matchCustom(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
        arraySearch = [];
        searchDefault = "";
        return data;
    }
    else {
        searchDefault = params.term;
    }
    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
        return null;
    }

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    if (xoa_dau(data.text.toLowerCase().trim()).indexOf(xoa_dau(params.term.toLowerCase().trim())) > -1) {
        if (searchDefault != params.term) {
            arraySearch = [];
        }

        var modifiedData = $.extend({}, data, true);
        modifiedData.text;

        // You can return modified objects from here
        // This includes matching the `children` how you want in nested data sets
        return modifiedData;
    }
    else {

    }

    // Return `null` if the term should not be displayed
    return null;
}

function matchCustomBasic(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
        return data;
    }

    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
        return null;
    }

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    if (xoa_dau(data.text.toLowerCase().trim()).indexOf(xoa_dau(params.term.toLowerCase().trim())) > -1) {
        var modifiedData = $.extend({}, data, true);
        // You can return modified objects from here
        // This includes matching the `children` how you want in nested data sets
        return modifiedData;
    }

    // Return `null` if the term should not be displayed
    return null;
}