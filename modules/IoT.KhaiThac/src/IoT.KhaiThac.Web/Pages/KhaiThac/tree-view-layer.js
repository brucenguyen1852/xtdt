﻿var TreeViewLayer = {
    GLOBAL: {
        ArrayTree: [],
        ArrayNodeCheck: [],
        DirectoryForcus: "",
        ListPropertiesFocus: [],
        ListIdShow: []
    },
    CONSTS: {
        URL_GETLISTDIRECTORY: "/api/khaithac/khaiThacDirectory/get-list-layer-has-setting",
        URL_GET_PROPERTIES_DIRECTORY: "/api/app/khai-thac-properties-directory"
    },
    SELECTORS: {
        tree: "#checkTree",
        dyntree_node_text: ".dynatree-title",
        search_tree: ".search-tree",
        itemli: ".item-li",
        treeview: ".tree-o-i .treeview ",
        ul_treeview: ".tree-o-i",
        titleItem: ".title-item",
        icon_pull_left_arrow: ".pull-left-container",

        content_left_sidebar: ".map-side-form .scroll-content",
    },
    init: function () {
        TreeViewLayer.setEvent();
        TreeViewLayer.GetListLayer();
        TreeViewLayer.Select2Search();
    },
    setEvent: function () {
        $(TreeViewLayer.SELECTORS.tree).on('click', TreeViewLayer.SELECTORS.dyntree_node_text, function () {
            var folder = $(this).parent().attr('data-folder');
            //var id = $(this).attr('data-id');
            if (folder != "true") {
                $(this).parent().children().eq(1).click();
            }
        })

        $(TreeViewLayer.SELECTORS.ul_treeview).on('click', TreeViewLayer.SELECTORS.icon_pull_left_arrow, function () {
            var id = $(this).attr('data-id');
            if (id != "" && id != undefined) {
                //$('#tree-item-' + id).find('.item-li').click();
                $(`.item-li[data-id="${id}"]`).click();
            }
        });

        $(TreeViewLayer.SELECTORS.ul_treeview).on('click', '.item-li', function () {
            var id = $(this).attr('data-id');
            var isFolder = $(this).attr('data-type');
            var layer = TimKiem.GLOBAL.ArrayObjectMain.find(x => x.id == id);
            if (layer != undefined) {
                $(exploit.SELECTORS.header_list_object_span).html(l('Main:ListObject') + ` (${layer.listMainObject.length})`);
            }

            $(ThongTin.SELECTORS.input_search_object).val("");// reset search đối tượng

            if ($(`#checkmark-${id}`).hasClass('check')) {
                TreeViewLayer.clearHighLightTreeview($(this).parent().find('.checkmark'));
                TreeViewLayer.setHighLightTreeview($(this).parent().find('.checkmark'));
                if (TreeViewLayer.GLOBAL.DirectoryForcus != id) {
                    TreeViewLayer.GLOBAL.DirectoryForcus = id;
                    TreeViewLayer.GLOBAL.ListPropertiesFocus = [];
                    $(ThongTin.SELECTORS.content_list_object).html('');
                    ThongTin.GLOBAL.page = 0;
                    ThongTin.AppenedLstObject(id);

                    $(ThongTin.SELECTORS.inforData).find('.toggle-detail-property2').click(); // dong xem info 1 doi tuong

                    if (exploit.GLOBAL.ActionToolbar.XemThongTinHover) {
                        exploit.CloseHoverToolBar(); // tắt hover
                    }
                }

                if (!$(exploit.SELECTORS.content_lst_object).hasClass('open')) {
                    $(exploit.SELECTORS.content_lst_object).addClass('open')
                    if (exploit.GLOBAL.isMobile) {
                        $(".div-content").addClass("toogle-tree");
                    }
                }

                if (!$(`#tree-ul-${id}`).hasClass('open')) {
                    $(`#tree-ul-${id}`).addClass('open');
                    $(`#tree-ul-${id}`).slideToggle("slow");

                    var element = $(this).parent().find('.icon-menu-tree');
                    if (element.length > 0) {
                        element.addClass('open')
                    }
                }
            }
            else {

                if (!$(`#tree-ul-${id}`).hasClass('open')) {
                    $(`#tree-ul-${id}`).addClass('open');
                    $(`#tree-ul-${id}`).slideToggle("slow");

                    var element = $(this).parent().find('.icon-menu-tree');
                    if (element.length > 0) {
                        element.addClass('open')
                    }
                }
                else {
                    $(`#tree-ul-${id}`).slideToggle("slow");
                    $(`#tree-ul-${id}`).removeClass('open');
                    var element = $(this).parent().find('.icon-menu-tree');
                    if (element.length > 0) {
                        element.removeClass('open')
                    }
                }
            }

            //if (isFolder != "true") {
            //    TreeViewLayer.GetPropertiesDirectory(id);
            //}
        });

        $(TreeViewLayer.SELECTORS.ul_treeview).on('click', '.checkmark', function (e) {
            //e.preventDefault();
            let id = $(this).attr('data-id');

            var $button = $(this);
            if ($button.data('alreadyclicked')) // kiểm tra double click
            {
                $button.data('alreadyclicked', false); // reset

                $(ThongTin.SELECTORS.input_search_object).val("");// reset search đối tượng

                if (!$(this).hasClass('check')) {
                    $(`.icon-menu-tree-${id}`).addClass('open'); // open icon
                    $(`#tree-ul-${id}`).slideDown("slow");
                    $(`#tree-ul-${id}`).addClass('open');

                    TreeViewLayer.GLOBAL.DirectoryForcus = id; // set layer đang được forcus
                    TreeViewLayer.GLOBAL.ListPropertiesFocus = []; // danh sách thuộc tính của layer đang được forcus

                    $(this).addClass('check');

                    TreeViewLayer.CheckLayer(id, true);
                    TreeViewLayer.setHighLightTreeview(this);
                }
                else {
                    $(this).removeClass('check');
                    TreeViewLayer.UnCheckLayer(id);
                }

                if ($button.data('alreadyclickedTimeout')) {
                    clearTimeout($button.data('alreadyclickedTimeout')); // prevent this from happening
                }
                // do what needs to happen on double click. 
                $('#action').val('Was double clicked');
            } else {
                $button.data('alreadyclicked', true);

                var alreadyclickedTimeout = setTimeout(function () {
                    $button.data('alreadyclicked', false); // reset when it happens

                    $(ThongTin.SELECTORS.input_search_object).val("");// reset search đối tượng

                    if (!$('#checkmark-' + id).hasClass('check')) {
                        $(`.icon-menu-tree-${id}`).addClass('open'); // open icon
                        $(`#tree-ul-${id}`).slideDown("slow");
                        $(`#tree-ul-${id}`).addClass('open');

                        TreeViewLayer.GLOBAL.DirectoryForcus = id; // set layer đang được forcus
                        TreeViewLayer.GLOBAL.ListPropertiesFocus = []; // danh sách thuộc tính của layer đang được forcus

                        $('#checkmark-' + id).addClass('check');

                        TreeViewLayer.CheckLayer(id, true);
                        TreeViewLayer.setHighLightTreeview('#checkmark-' + id);
                    }
                    else {
                        $(TreeViewLayer.SELECTORS.titleItem).removeClass("active-treeview");
                        $('#checkmark-' + id).removeClass('check');
                        TreeViewLayer.UnCheckLayer(id);
                    }

                    // do what needs to happen on single click. 
                    // use el instead of $(this) because $(this) is 
                    // no longer the element
                }, 300); // <-- dblclick tolerance here
                $button.data('alreadyclickedTimeout', alreadyclickedTimeout); // store this id to clear if necessary
            }
            return false;

        });

        $(TreeViewLayer.SELECTORS.search_tree).on('keyup', delay(function () {
            $(TreeViewLayer.SELECTORS.ul_treeview + ' .treeview-not-result').remove();

            $(TreeViewLayer.SELECTORS.treeview).hide();
            var value = xoa_dau($(this).val().toLowerCase().trim());
            if (value != "") {
                $(TreeViewLayer.SELECTORS.treeview).each(function () {
                    var id = $(this).attr('data-id');
                    var chilNumber = parseInt($(this).attr('data-chil'));
                    var name = decodeURIComponent($(this).attr('data-search'));
                    var parent = $(this).attr('data-parent');
                    var folder = $(this).attr('data-type');

                    if (xoa_dau(name.trim().replace(/\s\s+/g, ' ').toLowerCase()).includes(xoa_dau(value.trim().replace(/\s\s+/g, ' ').toLowerCase()))) {
                        $(this).show();
                        if (chilNumber > 0) {
                            $('#tree-item-' + id).find('.icon-menu-tree-' + id).addClass('open'); // mở open icon
                            $('#tree-ul-' + id).addClass('open'); // mở nhánh con
                            $('#tree-ul-' + id).slideDown();
                            $('#tree-ul-' + id + " li").show();
                        }

                        if (parent != "null") {
                            $('#tree-item-' + parent).find('.icon-menu-tree-' + parent).addClass('open'); // mở open icon
                            $('#tree-ul-' + parent).addClass('open'); // mở nhánh con

                            search_tree('#tree-item-' + parent, value);
                        }
                    }
                });

                var countNotResult = $(TreeViewLayer.SELECTORS.treeview).filter(function () {
                    return $(this).css('display') == 'none'
                }).length;

                if ($(TreeViewLayer.SELECTORS.treeview).length == countNotResult) {
                    var li = `<div class="treeview-not-result"><span>${l('KhaiThac:SearchNull')}</span></div>`;
                    $(TreeViewLayer.SELECTORS.ul_treeview).append(li);
                }

            } else {
                TreeViewLayer.ReloadTree();
                $(TreeViewLayer.SELECTORS.content_left_sidebar).scrollTop(10);

                $.each(TimKiem.GLOBAL.ArrayObjectMain, function (index, layer) {
                    TreeViewLayer.TriggerForcusToTreeNode(layer.id);
                });

                if (TreeViewLayer.GLOBAL.DirectoryForcus != "") {
                    //$(TreeViewLayer.SELECTORS.content_left_sidebar).scrollTop(10);
                    var activity = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == TreeViewLayer.GLOBAL.DirectoryForcus);
                    if (activity != undefined && activity.level > 0) {
                        setTimeout(function () {
                            $(TreeViewLayer.SELECTORS.content_left_sidebar).animate({
                                scrollTop: $('#tree-item-' + TreeViewLayer.GLOBAL.DirectoryForcus).offset().top - $(TreeViewLayer.SELECTORS.content_left_sidebar).offset().top
                            }, 'slow');
                        }, 500);

                        $('#item-li-' + TreeViewLayer.GLOBAL.DirectoryForcus).click();
                    }
                }
            }

        }, 500));

        function search_tree(element, value) {
            if ($(element).css("display") == "none") {
                $(element).show();

                var id = $(element).attr('data-id');
                var name = $(element).attr('data-search');
                var parent = $(element).attr('data-parent');

                $('#tree-ul-' + id).slideDown();

                if (name != undefined) {
                    //if (name.toLowerCase().includes(value))
                    {
                        if (parent != "null") {
                            // hiển thị ul của cây
                            $('#tree-ul-' + parent).addClass('open'); // mở nhánh con

                            search_tree('#tree-item-' + parent, value);
                        }
                    }
                }
            }
        }
    },
    GetListLayer: function () {
        $.ajax({
            type: "GET",
            url: TreeViewLayer.CONSTS.URL_GETLISTDIRECTORY,
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if (res.code == "ok")
                {
                    var data = res.result;
                    if (data.length > 0) {

                        if (isAdmin == "True") // nếu tài khoản là admin thì show hết
                        {
                            TreeViewLayer.GLOBAL.ArrayTree = data;
                        }
                        else {
                            // lấy danh sách lớp mà tài khoản được phân quyền
                            TreeViewLayer.GetListIdLayerPermission(data);
                            TreeViewLayer.GLOBAL.ArrayTree = data.filter(x => TreeViewLayer.GLOBAL.ListIdShow.includes(x.id) || x.level == "0");
                            //indexdata.GetDirectoryCount();
                        }

                        TimKiem.SelectFolderParent(); // add value cho select folder (content tìm kiếm)
                        var parent = data.find(x => x.level == "0");
                        var treedynamic = TreeViewLayer.FillDynaTree(parent);
                        TimKiem.GLOBAL.TreeGroup = treedynamic;// group tree layer
                        var html = TreeViewLayer.DataTreeGen(treedynamic);

                        $(TreeViewLayer.SELECTORS.ul_treeview).html(html);

                        $(`.item-li`).each(function () {
                            if ($(this).find('.title-item').text() == "") {
                                $(this).remove();
                            }

                            var isFolder = $(this).attr('data-type');
                            var id = $(this).attr('data-id');
                            if (isFolder != "true") {
                                var checkbox = `<label class="container-checkbox container-checkbox-brand" style="width:8%;">
                                              <input type="checkbox">
                                              <span class="checkmark" id="checkmark-${id}" data-id="${id}"></span>
                                            </label>`;


                                if (isAdmin == "True") {
                                    $(this).parent().prepend(checkbox);
                                }
                                else {
                                    var checkPermission = ListIdLayer.find(x => x == id);
                                    if (checkPermission != undefined) {
                                        $(this).parent().prepend(checkbox);
                                    }
                                }
                            }
                        });

                    }
                }

            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    //dynatree
    FillDynaTree: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            icon: null,
            level: data.level,
            parentId: data.parentId
        };

        var parent = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    if (data.tags != null) {
                        obj.icon = data.tags["icon"];
                    }
                }
            }

            obj.key = parent.id;
            var lstChil = TreeViewLayer.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = TreeViewLayer.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    LoadDynaTree: function (data) {
        var treeData = [];
        treeData = data;
        $("#checkTree").dynatree({
            checkbox: true,
            selectMode: 2,
            children: treeData,
            onActivate: function (node) {
            },
            onSelect: function (select, node) {
                var selNodes = node.tree.getSelectedNodes();
                var selKeys = $.map(selNodes, function (node) {
                    return node.data.key;
                });

                TreeViewLayer.GLOBAL.ArrayNodeCheck = selKeys;
            },
            onClick: function (node, event) {
                let id = node.data.key;
                if (node.bSelected) {
                    exploit.HideObjectOfLayer2(id);
                    setTimeout(function () {
                        exploit.HideLocationMapDefault(id);
                        if (ThongTin.GLOBAL.idDirectory == id) {
                            ThongTin.clearHighlight();
                            ThongTin.showHideViewProperty(false);
                        }
                    }, 100);
                } else {
                    let obj = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == id);
                    if (obj != null && typeof (obj) != "undefined") {
                        if (obj.image2D != "" && obj.image2D != null && typeof (obj.image2D) != "undefined") {
                            let array = obj.image2D.split('.');
                            if (array[array.length - 1].toLocaleLowerCase() == "png" || array[array.length - 1].toLocaleLowerCase() == "jpg"
                                || array[array.length - 1].toLocaleLowerCase() == "jpeg") {
                                exploit.GLOBAL.image2dDirectory = obj.image2D;
                            }
                        }
                    }
                    //exploit.showObjectOfLayer(id);
                    setTimeout(function () {
                        exploit.LocationMapDefault(id);
                    }, 100);
                }
            },
            onKeydown: function (node, event) {

            },
            // The following options are only required, if we have more than one tree on one page:
            cookieId: "dynatree-Cb2",
            idPrefix: "dynatree-Cb2-"
        });
    },
    // autoGen
    DataTreeGen: function (data) {
        //$('.title').html(data.title);

        var tree = data.children;

        var html = ``;

        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;

            if (tree[i].icon === "" || tree[i].icon === null || tree[i].icon === "null") {
                if (tree[i].isFolder) {
                    icon = `<svg id="Group_1232" data-name="Group 1232" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                              <path id="Path_388" data-name="Path 388" d="M0,0H24V24H0Z" fill="none"/>
                              <path id="Path_389" data-name="Path 389" d="M3,21a1,1,0,0,1-1-1V4A1,1,0,0,1,3,3h7.414l2,2H20a1,1,0,0,1,1,1V9H4V19l2-8H22.5l-2.31,9.243a1,1,0,0,1-.97.757Z" fill="var(--primary)"/>
                            </svg>`;
                }
                else {
                    icon = `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                                <defs></defs>
                                <path class="a" style="fill:none;" d="M0,0H24V24H0Z"></path>
                                <path class="b " style="fill:var(--primary)" d="M21,9V20.993A1,1,0,0,1,20.007,22H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,4,2H14V8a1,1,0,0,0,1,1Zm0-2H16V2Z"></path>
                            </svg>`;
                }
            }

            if (tree[i].children.length > 0) {
                html += `<li id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-id="${tree[i].key}" data-search="${encodeURIComponent(name.toLowerCase())}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-parent="${tree[i].parentId}">
                            <div class="w-100" style="display:flex;">
                                <span class="pull-left-container" data-id=${tree[i].key}>
                                    <i class="fa fa-angle-right pull-left icon-menu-tree icon-menu-tree-${tree[i].key}"></i>
                                </span>
                                <div href="javascript:;" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}" style="display:flex;">
                                    <span>
                                         ${icon}
                                    </span>
                                     <span class="title-item ${TreeViewLayer.GLOBAL.DirectoryForcus == tree[i].key ? "active-treeview" : ""}" style="margin-left: 5px"> ${name} (${tree[i].children.length})</span>
                                </div>
                            </div>

                            <ul class="tree-ul" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:none;">`;

                html += TreeViewLayer.DataTreeGen(tree[i]);

                html += `
                             </ul>
                        </li>`;
            }
            else {
                html += `<li id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-id="${tree[i].key}" data-search="${encodeURIComponent(name.toLowerCase())}" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-parent="${tree[i].parentId}">
                                <div class="w-100" style="display:flex;">
                                    <div href="javascript:;" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}" style="display:flex;">
                                        <span>
                                            ${icon} 
                                        </span>
                                        <span class="title-item ${TreeViewLayer.GLOBAL.DirectoryForcus == tree[i].key ? "active-treeview" : ""}" style="margin-left: 5px">${name}</span>
                                    </div>    
                                </div>
                           </li>`;
            }
        }

        return html;
    },
    GetPropertiesDirectory: function (id) {
        var result = null;
        $.ajax({
            type: "GET",
            url: TreeViewLayer.CONSTS.URL_GET_PROPERTIES_DIRECTORY + "/" + id + "/async-by-id",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                result = data;
                if (TreeViewLayer.GLOBAL.ListPropertiesFocus.length == 0) {
                    TreeViewLayer.GLOBAL.ListPropertiesFocus = data.listProperties;
                    TreeViewLayer.SelectedSearchProperties();
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });

        return result;
    },
    //set highlight treeview
    setHighLightTreeview: function (el) {
        $(TreeViewLayer.SELECTORS.titleItem).removeClass("active-treeview");
        if ($(el).parent().parent().find(".title-item").length > 0) {
            $(el).parent().parent().find(".title-item").eq(0).addClass("active-treeview");
        }
    },
    clearHighLightTreeview: function (el) {
        $(el).parent().parent().find(".title-item").removeClass("active-treeview");
    },
    Select2Search: function () {
        $(ThongTin.SELECTORS.selected_search).select2({
            placeholder: `Chọn chỉ mục`,
            allowClear: true,
            width: '100%'
        });
    },
    SelectedSearchProperties: function () {
        var lst = TreeViewLayer.GLOBAL.ListPropertiesFocus.filter(x => (x.typeSystem == 3 || x.typeSystem == 2) && (x.typeProperties == "text" || x.typeProperties == "float"
            || x.typeProperties == "int" || x.typeProperties == "string" || x.typeProperties == "stringlarge") && x.isIndexing == true); // chỉ cho hiển thị chỉ mục là : chuỗi và số

        var html = '<option value= ""></option>';
        $(ThongTin.SELECTORS.selected_search).html(html);
        for (var i = 0; i < lst.length; i++) {
            html += `<option value= "${lst[i].codeProperties}" data-type = "${lst[i].typeProperties}">${lst[i].nameProperties}</option>`;
        }

        $(ThongTin.SELECTORS.selected_search).html(html);

    },
    ReloadTree: function () {
        var parent = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.level == "0");
        var treedynamic = TreeViewLayer.FillDynaTree(parent);

        var html = TreeViewLayer.DataTreeGen(treedynamic);

        $(TreeViewLayer.SELECTORS.ul_treeview).html('');
        $(TreeViewLayer.SELECTORS.ul_treeview).html(html);

        $(`.item-li`).each(function () {
            if ($(this).find('.title-item').text() == "") {
                $(this).remove();
            }
            var isFolder = $(this).attr('data-type');
            var id = $(this).attr('data-id');
            var indexChecked = TimKiem.GLOBAL.ArrayObjectMain.findIndex(x => x.id == id);

            if (isFolder != "true") {
                var checkbox = `<label class="container-checkbox container-checkbox-brand">
                                              <input type="checkbox">
                                              <span class="checkmark ${indexChecked > -1 ? "check" : ""}" id="checkmark-${id}" data-id="${id}"></span>
                                            </label>`;

                $(this).parent().prepend(checkbox);
            }
        });
    },
    TriggerForcusToTreeNode: function (id) {
        $(TreeViewLayer.SELECTORS.ul_treeview + ' .treeview').each(function () {
            var idlayer = $(this).attr('data-id');
            var parent = $(this).attr('data-parent');
            var level = parseInt($(this).attr('data-level'));

            if (level == 0) {
                $(this).show();
            }

            if (id == idlayer) {
                $(this).show();
                if (parent != "null") {
                    $(`.icon-menu-tree-${parent}`).addClass('open'); // open icon
                    $(`#tree-ul-${parent}`).slideDown("slow");
                    $(`#tree-ul-${parent}`).addClass('open');

                    //$($(this).find('.icon-menu-tree')).addClass('open');
                    forcus_tree('#tree-item-' + parent, parent);
                }
            }
        });

        function forcus_tree(element, id) {
            $(element).show();
            var idlayer = $(element).attr('data-id');
            var parent = $(element).attr('data-parent');
            var level = parseInt($(element).attr('data-level'));

            if (level == 0) {
                $(this).show();
            }

            if (idlayer != undefined) {
                if (idlayer == id) {
                    if (parent != "null") {
                        // hiển thị ul của cây
                        $(`.icon-menu-tree-${parent}`).addClass('open'); // open icon
                        $(`#tree-ul-${parent}`).slideDown("slow");
                        $(`#tree-ul-${parent}`).addClass('open');


                        forcus_tree('#tree-item-' + parent, parent);
                    }
                }
            }

        }
    },
    // lấy toàn bộ đối tượng và thông tin layer truyền vào
    CheckLayer: function (id, isShow) {
        exploit.GLOBAL.image2dDirectory = '';
        let obj = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == id);
        if (obj != null && typeof (obj) != "undefined") {
            if (obj.image2D != "" && obj.image2D != null && typeof (obj.image2D) != "undefined") {
                let array = obj.image2D.split('.');
                if (array[array.length - 1].toLocaleLowerCase() == "png" || array[array.length - 1].toLocaleLowerCase() == "jpg"
                    || array[array.length - 1].toLocaleLowerCase() == "jpeg") {
                    exploit.GLOBAL.image2dDirectory = obj.image2D; // set icon của layer đang được forcus
                }
            }
        }
        //exploit.showObjectOfLayer(id); // hiển thị danh sách đối tượng của layer đang được forcus
        setTimeout(function () {
            exploit.LocationMapDefault(id, isShow);
        }, 100);

        // get list
        $(ThongTin.SELECTORS.content_list_object).html('');
        ThongTin.GLOBAL.page = 0;

        if (isShow) // kiểm tra để hiên thị danh sách đối tượng layer dc focus
        {
            if (!$(exploit.SELECTORS.content_lst_object).hasClass('open')) {
                $(exploit.SELECTORS.content_lst_object).addClass('open')
                if (exploit.GLOBAL.isMobile) {
                    $(".div-content").addClass("toogle-tree");
                    $(".toogle-tree-bar svg").css("transform", "rotateY(180deg)");
                }
            }

            ThongTin.AppenedLstObject(id); // hiển thị danh sách đối tượng
        }

        TreeViewLayer.GetPropertiesDirectory(id);


        $(ThongTin.SELECTORS.inforData).find('.toggle-detail-property2').click();
    },
    // tắt toàn bộ đối tượng và thông tin layer truyền vào
    UnCheckLayer: function (id) {

        ThongTin.clearHighlightPoint();
        ThongTin.clearHighlight();

        // xoa id qtsc
        var arrayObjQTSC = exploit.GLOBAL.listObject3D.filter(x => x.idLayer == id && x.idObject !== "");
        if (arrayObjQTSC != undefined) {
            var arrayIdQTSC = arrayObjQTSC.map(x => x.idObject);
            var difference = [];

            jQuery.grep(arrayIdQTSC, function (el) {
                if (jQuery.inArray(el, exploit.GLOBAL.lstArrayQTSC) == -1) difference.push(el);
            });

            exploit.GLOBAL.lstArrayQTSC = difference;
        }

        exploit.HideObjectOfLayer2(id);
        setTimeout(function () {
            exploit.HideLocationMapDefault(id);
            if (ThongTin.GLOBAL.idDirectory == id) {
                ThongTin.clearHighlight();
                ThongTin.showHideViewProperty(false);
            }
        }, 100);

        if (TreeViewLayer.GLOBAL.DirectoryForcus == id) {

            TreeViewLayer.GLOBAL.DirectoryForcus = "";

            $(ThongTin.SELECTORS.content_list_object).html('');
            ThongTin.GLOBAL.page = 0;
            // đóng danh sách đối tượng
            $(exploit.SELECTORS.content_lst_object).removeClass('open');
        }

        // dữ liệu tìm kiếm
        var indexTimkiemDirectory = TimKiem.GLOBAL.ArrayObjectMain.findIndex(x => x.id == id);
        if (indexTimkiemDirectory != -1) {
            TimKiem.GLOBAL.ArrayObjectMain.splice(indexTimkiemDirectory, 1);
        }

        if (ThongTin.GLOBAL.idDirectory == id) {
            $(ThongTin.SELECTORS.inforData).find('.toggle-detail-property2').click();
        }

        TreeViewLayer.clearHighLightTreeview(this);
    },

    GetListIdLayerPermission: function (data) {
        var lst = data.filter(x => ListIdLayer.indexOf(x.id) >= 0);
        var lstcheck = [];
        $.each(lst, function (i, obj) {
            let listid = TreeViewLayer.getListIdParent(obj, data);
            lstcheck = lstcheck.concat(listid);
        });

        TreeViewLayer.GLOBAL.ListIdShow = [...new Set(lstcheck)];
    },
    getListIdParent: function (obj, data) {
        let lst = [obj.id];
        if (obj.parentId != "" && obj.parentId != null) {
            let check = data.find(x => x.id == obj.parentId);
            if (check != null && check != undefined) {
                let lstcheck = TreeViewLayer.getListIdParent(check, data);
                lst = lst.concat(lstcheck);
            }
        }
        return lst;
    },
}


function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    //str = str.replace(/\s/g, '');
    return str;
}