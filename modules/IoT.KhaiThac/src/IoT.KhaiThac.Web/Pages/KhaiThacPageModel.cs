﻿using IoT.KhaiThac.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace IoT.KhaiThac.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class KhaiThacPageModel : AbpPageModel
    {
        protected KhaiThacPageModel()
        {
            LocalizationResourceType = typeof(KhaiThacResource);
            ObjectMapperContext = typeof(KhaiThacWebModule);
        }
    }
}