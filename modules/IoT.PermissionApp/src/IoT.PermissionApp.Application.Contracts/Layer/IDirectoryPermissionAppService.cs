﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.PermissionApp.Layer
{
    public interface IDirectoryPermissionAppService : ICrudAppService< //Defines CRUD methods
            DirectoryDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDirectoryDto> //Used to create/update a book

    {
        Task<DirectoryDto> GetDirectoryById(Guid id);
        List<DirectoryDto> GetListChildrenByDirectory(Guid Id);

    }
}
