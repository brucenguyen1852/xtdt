﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.PermissionApp.LayerPermission
{
    public class AddRangeLayerUserPermissionFormModel
    {
        public List<ObjectPermission> UserPermission { get; set; }
        public List<ObjectPermission> RolePermission { get; set; }
        public string IdDirectory { get; set; }
    }

    public class ObjectPermission
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<PermissionDetail> ListPermission { get; set; }
    }
}
