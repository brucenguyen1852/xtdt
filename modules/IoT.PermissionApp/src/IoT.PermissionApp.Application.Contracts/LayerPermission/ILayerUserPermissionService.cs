﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.PermissionApp.LayerPermission
{
    public interface ILayerUserPermissionService : ICrudAppService< //Defines CRUD methods
            LayerUserPermissionDto, //Used to show LayerUserPermission
            Guid, //Primary key of the LayerUserPermission entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateLayerUserPermissionDto> //Used to create/update a LayerUserPermission
    {
        Task<List<LayerUserPermissionDto>> GetListOfUserOrRole(string UserId, string RoleId);
        Task<bool> CreateMany(List<CreateLayerUserPermissionDto> inputs, string idDirectory);
        Task<object> GetPermissionLayer(string idDirectory);
        Task<object> GetPermissionImplement(string idDirectory);
        List<LayerUserPermissionDto> GetListOfUserOrLstRole(string UserId, List<string> RoleId);
        Task<List<PermissionWithLayerInfo>> GetLayerWithPermission();
    }
}
