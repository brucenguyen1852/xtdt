﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.PermissionApp.LayerPermission
{
    public interface IPermissionLayerService : ICrudAppService< //Defines CRUD methods
            PermissionLayerDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreatePermissionLayerDto> //Used to create/update a book
    {
        Task<object> GetListPermission(string filter, string Sorting, int SkipCount, int MaxResultCount);
        Task<object> GetListPermissionAll(string search);
        bool CheckExitsCode(string id, string code);
    }
}
