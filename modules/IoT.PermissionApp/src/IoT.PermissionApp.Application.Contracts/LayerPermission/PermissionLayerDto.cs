﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.PermissionApp.LayerPermission
{
    public class PermissionLayerDto : AuditedEntityDto<Guid>
    {
        public string Name { get; set; } //tên quyền của sử dụng lớp
        public string KeyName { get; set; } //key của quyền này, không được thay đổi
        public bool IsStatus { get; set; } // kiểm tra public hoặc private 
        public Dictionary<string, object> Tags { get; set; }// tab dùng để sau này cần thêm gì thì cứ thêm vào đây
    }
}
