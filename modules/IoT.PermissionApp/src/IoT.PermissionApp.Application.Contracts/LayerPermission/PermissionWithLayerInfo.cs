﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.PermissionApp.LayerPermission
{
    public class PermissionWithLayerInfo : AuditedEntityDto<Guid>
    {
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
        public string ParentId { get; set; }
        public string Search { get; set; }

        public List<ObjectPermission> Users { get; set; }
        public List<ObjectPermission> Roles { get; set; }
    }
}
