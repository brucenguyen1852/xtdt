﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Identity;

namespace IoT.PermissionApp.ManagerIdentity
{
    public interface IManagerIdentityService
    {
        Task<PagedResultDto<IdentityUserDto>> GetListUser(GetIdentityUsersInput input);
        Task<PagedResultDto<IdentityRoleDto>> GetListRole(GetIdentityRoleListInput input);
        Task<ListResultDto<IdentityRoleDto>> GetRolesAsync(Guid id);
        Task<IdentityRoleDto> GetByIdAsync(Guid id);
    }
}
