﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace IoT.PermissionApp
{
    [DependsOn(
        typeof(PermissionAppDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule),
        typeof(AbpIdentityApplicationContractsModule)
        )]
    public class PermissionAppApplicationContractsModule : AbpModule
    {

    }
}
