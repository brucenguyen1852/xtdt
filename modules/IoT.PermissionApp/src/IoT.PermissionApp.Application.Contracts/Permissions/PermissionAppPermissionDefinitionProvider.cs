﻿using IoT.PermissionApp.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace IoT.PermissionApp.Permissions
{
    public class PermissionAppPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(PermissionAppPermissions.GroupName, L("Permission:PermissionApp"));

            //// thêm phân quyền 
            var permissionLayer = myGroup.AddPermission(PermissionAppPermissions.PermissionLayer.Default, L("Permission:PermissionApp.PermissionLayer"));
            //permissionLayer.AddChild(PermissionAppPermissions.PermissionLayer.Create, L("Permission:PermissionApp.PermissionLayer.Create"));
            //permissionLayer.AddChild(PermissionAppPermissions.PermissionLayer.Edit, L("Permission:PermissionApp.PermissionLayer.Edit"));
            //permissionLayer.AddChild(PermissionAppPermissions.PermissionLayer.Delete, L("Permission:PermissionApp.PermissionLayer.Delete"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<PermissionAppResource>(name);
        }
    }
}