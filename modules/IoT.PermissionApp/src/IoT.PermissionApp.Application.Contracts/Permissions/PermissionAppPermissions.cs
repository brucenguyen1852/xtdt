﻿using Volo.Abp.Reflection;

namespace IoT.PermissionApp.Permissions
{
    public class PermissionAppPermissions
    {
        public const string GroupName = "PermissionApp";

        public static class PermissionLayer
        {
            public const string Default = GroupName + ".PermissionLayer";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(PermissionAppPermissions));
        }
    }
}