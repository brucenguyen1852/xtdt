﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.PermissionApp.Layer
{
    public class DirectoryPermissionAppService : CrudAppService<
            Directory, //The Book entity
            DirectoryDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDirectoryDto>, //Used to create/update a book
        IDirectoryPermissionAppService //implement the IBookAppService
    {
        public DirectoryPermissionAppService(IRepository<Directory, Guid> repository) : base(repository)
        {


        }

        public async Task<DirectoryDto> GetDirectoryById(Guid id)
        {
            try
            {
                var directory = await Repository.FindAsync(x => x.Id == id);
                if (directory != null)
                {
                    var jsonDirectory = JsonConvert.SerializeObject(directory);
                    var result = JsonConvert.DeserializeObject<DirectoryDto>(jsonDirectory);

                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DirectoryDto> GetListChildrenByDirectory(Guid Id)
        {
            try
            {
                var directory = Repository.Where(x => x.ParentId == Id.ToString());
                var jsonDirectory = JsonConvert.SerializeObject(directory);
                var result = JsonConvert.DeserializeObject<List<DirectoryDto>>(jsonDirectory);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
