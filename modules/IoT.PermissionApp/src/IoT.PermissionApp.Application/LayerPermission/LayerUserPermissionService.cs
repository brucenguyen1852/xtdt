﻿using IoT.Common;
using IoT.PermissionApp.Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;

namespace IoT.PermissionApp.LayerPermission
{
    public class LayerUserPermissionService : CrudAppService<
           LayerUserPermission, //The LayerUserPermission entity
           LayerUserPermissionDto, //Used to show LayerUserPermission
           Guid, //Primary key of the LayerUserPermission entity
           PagedAndSortedResultRequestDto, //Used for paging/sorting
           CreateLayerUserPermissionDto>, //Used to create/update a LayerUserPermission
       ILayerUserPermissionService //implement the ILayerUserPermissionService
    {
        public IIdentityUserRepository UserRepository { get; }
        protected IIdentityRoleRepository RoleRepository { get; }

        protected IDirectoryPermissionAppService _directoryPermissionAppService;

        private readonly IRepository<Directory, Guid> _directories;
        public LayerUserPermissionService(IRepository<LayerUserPermission, Guid> repository,
                                          IIdentityUserRepository UserRepository,
                                          IIdentityRoleRepository RoleRepository,
                                          IDirectoryPermissionAppService directoryPermissionAppService,
                                          IRepository<Directory, Guid> directories) : base(repository)
        {
            this.UserRepository = UserRepository;
            this.RoleRepository = RoleRepository;
            _directoryPermissionAppService = directoryPermissionAppService;
            _directories = directories;
        }

        public override async Task<LayerUserPermissionDto> CreateAsync(CreateLayerUserPermissionDto input)
        {
            var data = new LayerUserPermission()
            {
                UserId = input.UserId,
                RoleId = input.RoleId,
                IdDirectory = input.IdDirectory,
                ListPermisson = input.ListPermisson
            };

            var obj = await Repository.InsertAsync(data);

            var result = obj.Clone<LayerUserPermissionDto>();
            return result;
        }

        public async Task<List<LayerUserPermissionDto>> GetListOfUserOrRole(string UserId, string RoleId)
        {
            var lstResult = new List<LayerUserPermissionDto>();

            var lst = new List<LayerUserPermission>();
            if (!string.IsNullOrWhiteSpace(UserId) && !string.IsNullOrWhiteSpace(RoleId))
            {
                lst = Repository.Where(x => x.UserId == UserId || x.RoleId == RoleId).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(UserId))
            {
                lst = Repository.Where(x => x.UserId == UserId).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(RoleId))
            {
                lst = Repository.Where(x => x.RoleId == RoleId).ToList();
            }
            lstResult = lst.Clone<List<LayerUserPermissionDto>>();
            return lstResult;
        }

        public override async Task<LayerUserPermissionDto> UpdateAsync(Guid id, CreateLayerUserPermissionDto input)
        {
            try
            {
                var currentData = await Repository.GetAsync(id);

                if (currentData == null)
                {
                    return null;
                }

                currentData.UserId = input.UserId;
                currentData.RoleId = input.RoleId;
                currentData.IdDirectory = input.IdDirectory;
                currentData.ListPermisson = input.ListPermisson;

                var obj = await Repository.UpdateAsync(currentData);

                var result = obj.Clone<LayerUserPermissionDto>();

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> CreateMany(List<CreateLayerUserPermissionDto> inputs, string idDirectory)
        {
            var currentUserId = CurrentUser.Id.ToString();
            var lstRecordOld = Repository.Where(x => x.IdDirectory == idDirectory && x.UserId != currentUserId);

            // xóa user, role cho directory childrent
            await DeletePermissionLayerChildrent(idDirectory, lstRecordOld.Where(x => !string.IsNullOrEmpty(x.UserId)).Select(x => x.UserId).ToList(), lstRecordOld.Where(x => !string.IsNullOrEmpty(x.RoleId)).Select(x => x.RoleId).ToList());

            // xóa record cũ
            await Repository.DeleteManyAsync(lstRecordOld);

            if (inputs != null && inputs.Count() > 0)
            {
                var lstData = inputs.Clone<List<LayerUserPermission>>();
                await Repository.InsertManyAsync(lstData);

                // phân quyền cho các lớp con
                var directory = await _directoryPermissionAppService.GetDirectoryById(Guid.Parse(idDirectory));
                if (directory != null)
                {
                    if (directory.Type != "file")
                    {
                        var lstChil = _directoryPermissionAppService.GetListChildrenByDirectory(Guid.Parse(idDirectory));
                        lstChil = lstChil.FindAll(x => x.Type == "file");

                        var lstImplement = new List<LayerUserPermission>();
                        foreach (var directoryChil in lstChil)
                        {
                            foreach (var item in inputs)
                            {
                                if (!string.IsNullOrEmpty(item.UserId))
                                {
                                    var lstRecordOldUserImplement = Repository.Where(x => x.IdDirectory == directoryChil.Id.ToString() && x.UserId == item.UserId);
                                    // xóa record cũ theo user
                                    await Repository.DeleteManyAsync(lstRecordOldUserImplement);
                                }

                                if (!string.IsNullOrEmpty(item.RoleId))
                                {
                                    var lstRecordOldRoleImplement = Repository.Where(x => x.IdDirectory == directoryChil.Id.ToString() && x.RoleId == item.RoleId);
                                    // xóa record cũ theo user
                                    await Repository.DeleteManyAsync(lstRecordOldRoleImplement);
                                }

                                item.IdDirectory = directoryChil.Id.ToString();

                                var dtoImplement = item.Clone<LayerUserPermission>();
                                lstImplement.Add(dtoImplement);
                            }
                        }

                        if (lstImplement.Count() > 0)
                        {
                            await Repository.InsertManyAsync(lstImplement);
                        }
                    }
                }
            }

            return true;
        }

        public async Task<object> GetPermissionLayer(string idDirectory)
        {
            var currentUserId = CurrentUser.Id.ToString();
            var lstKeyName = new List<PermissionDetail>();

            var roleAdmin = await RoleRepository.GetListAsync(x => x.IsStatic); // role admin
            var lstRoleId = roleAdmin.Select(x => x.Id); // danh sách vai trò admin

            var users = await UserRepository.GetListAsync(x => x.Roles.Any(y => lstRoleId.Contains(y.RoleId))); // lấy những tài khoản có vai trò là admin
            var userAdminId = users.Select(x => x.Id.ToString()); // danh sách id tài khoản

            var lstRecord = Repository.Where(x => x.IdDirectory == idDirectory && !userAdminId.Contains(x.UserId)); // chỉ hiển thị những user k phải là admin
            if (lstRecord.Count() > 0)
            {
                var lstUser = new List<ObjectPermission>();
                var lstRole = new List<ObjectPermission>();

                foreach (var item in lstRecord)
                {
                    if (!string.IsNullOrEmpty(item.UserId))
                    {
                        var guidId = Guid.Parse(item.UserId);
                        var user = await this.UserRepository.GetAsync(guidId);
                        if (user != null)
                        {
                            var userPermission = new ObjectPermission()
                            {
                                Id = item.UserId,
                                Name = user.Name + "(" + user.UserName + ")",
                                ListPermission = item.ListPermisson
                            };

                            lstUser.Add(userPermission);
                        }
                    }

                    if (!string.IsNullOrEmpty(item.RoleId))
                    {
                        var guidId = Guid.Parse(item.RoleId);
                        var role = await this.RoleRepository.GetAsync(guidId);
                        if (role != null)
                        {
                            var rolePermission = new ObjectPermission()
                            {
                                Id = item.RoleId,
                                Name = role.Name,
                                ListPermission = item.ListPermisson
                            };

                            lstRole.Add(rolePermission);
                        }
                    }
                }

                return new { ListUser = lstUser, ListRole = lstRole };
            }

            return new { ListUser = new List<ObjectPermission>(), ListRole = new List<ObjectPermission>() };
        }

        public async Task<object> GetPermissionImplement(string idDirectory)
        {

            var currentUserId = CurrentUser.Id.ToString();
            var lstKeyName = new List<PermissionDetail>();

            var lstRecord = Repository.Where(x => x.IdDirectory == idDirectory); // chỉ hiển thị những user k phải là admin
            if (lstRecord.Count() > 0)
            {
                var lstUser = new List<ObjectPermission>();
                var lstRole = new List<ObjectPermission>();

                foreach (var item in lstRecord)
                {
                    if (!string.IsNullOrEmpty(item.UserId))
                    {
                        var guidId = Guid.Parse(item.UserId);
                        var user = await this.UserRepository.GetAsync(guidId);
                        if (user != null)
                        {
                            var userPermission = new ObjectPermission()
                            {
                                Id = item.UserId,
                                Name = user.Name + "(" + user.UserName + ")",
                                ListPermission = item.ListPermisson
                            };

                            lstUser.Add(userPermission);
                        }
                    }

                    if (!string.IsNullOrEmpty(item.RoleId))
                    {
                        var guidId = Guid.Parse(item.RoleId);
                        var role = await this.RoleRepository.GetAsync(guidId);
                        if (role != null)
                        {
                            var rolePermission = new ObjectPermission()
                            {
                                Id = item.RoleId,
                                Name = role.Name,
                                ListPermission = item.ListPermisson
                            };

                            lstRole.Add(rolePermission);
                        }
                    }
                }

                return new { ListUser = lstUser, ListRole = lstRole };
            }

            return new { ListUser = new List<ObjectPermission>(), ListRole = new List<ObjectPermission>() };

        }

        public List<LayerUserPermissionDto> GetListOfUserOrLstRole(string UserId, List<string> RoleId)
        {
            var lstResult = new List<LayerUserPermissionDto>();

            var lst = new List<LayerUserPermission>();
            if (!string.IsNullOrWhiteSpace(UserId) && RoleId.Count > 0)
            {
                lst = Repository.Where(x => x.UserId == UserId || RoleId.Contains(x.RoleId)).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(UserId))
            {
                lst = Repository.Where(x => x.UserId == UserId).ToList();
            }
            else if (RoleId.Count > 0)
            {
                lst = Repository.Where(x => RoleId.Contains(x.RoleId)).ToList();
            }
            lstResult = lst.Clone<List<LayerUserPermissionDto>>();
            return lstResult;
        }

        private async Task DeletePermissionLayerChildrent(string idDirectory, List<string> users, List<string> roles)
        {
            var directory = await _directoryPermissionAppService.GetDirectoryById(Guid.Parse(idDirectory));
            if (directory != null)
            {
                if (directory.Type != "file")
                {
                    var lstChil = _directoryPermissionAppService.GetListChildrenByDirectory(Guid.Parse(idDirectory));
                    lstChil = lstChil.FindAll(x => x.Type == "file");

                    foreach (var directoryChil in lstChil)
                    {
                        var lstRecordOldUserImplement = Repository.Where(x => x.IdDirectory == directoryChil.Id.ToString() && users.Contains(x.UserId));
                        // xóa record cũ theo user
                        await Repository.DeleteManyAsync(lstRecordOldUserImplement);

                        var lstRecordOldRoleImplement = Repository.Where(x => x.IdDirectory == directoryChil.Id.ToString() && roles.Contains(x.RoleId));
                        // xóa record cũ theo role
                        await Repository.DeleteManyAsync(lstRecordOldRoleImplement);
                    }
                }
            }
        }

        public async Task<List<PermissionWithLayerInfo>> GetLayerWithPermission()
        {
            var lstResult = new List<PermissionWithLayerInfo>();

            // lấy danh sách layer
            var lstLayer = _directories.Where(x => x.Level != -1).ToList();
            lstResult = lstLayer.Clone<List<PermissionWithLayerInfo>>();

            var roleAdmin = await RoleRepository.GetListAsync(x => x.IsStatic); // role admin
            var lstRoleId = roleAdmin.Select(x => x.Id); // danh sách vai trò admin

            var users = await UserRepository.GetListAsync(x => x.Roles.Any(y => lstRoleId.Contains(y.RoleId))); // lấy những tài khoản có vai trò là admin
            var userAdminId = users.Select(x => x.Id.ToString()); // danh sách id tài khoản

            foreach (var item in lstResult)
            {
                item.Users = new List<ObjectPermission>();
                item.Roles = new List<ObjectPermission>();

                // kiểm tra layer trong collection permisson layer
                var permissionLayerUser = this.Repository.Where(x => x.IdDirectory == item.Id.ToString() && !userAdminId.Contains(x.UserId)).ToList();
                foreach (var permissionUser in permissionLayerUser)
                {
                    // lấy toàn bộ user và quyền của những user đó đang dc phân quyền cho layer
                    if (!string.IsNullOrEmpty(permissionUser.UserId))
                    {
                        var guidId = Guid.Parse(permissionUser.UserId);
                        var user = await this.UserRepository.GetAsync(guidId);
                        if (user != null)
                        {
                            var userPermission = new ObjectPermission()
                            {
                                Id = permissionUser.UserId,
                                Name = user.Name + "(" + user.UserName + ")",
                                ListPermission = permissionUser.ListPermisson
                            };

                            item.Users.Add(userPermission);
                        }
                    }

                    // lấy toàn bộ role và quyền của những role đó đang dc phân quyền cho layer
                    if (!string.IsNullOrEmpty(permissionUser.RoleId))
                    {
                        var guidId = Guid.Parse(permissionUser.RoleId);
                        var role = await this.RoleRepository.GetAsync(guidId);
                        if (role != null)
                        {
                            var rolePermission = new ObjectPermission()
                            {
                                Id = permissionUser.RoleId,
                                Name = role.Name,
                                ListPermission = permissionUser.ListPermisson
                            };

                            item.Roles.Add(rolePermission);
                        }
                    }
                }
            }

            return lstResult.OrderByDescending(x => x.Type).ToList();
        }
    }
}
