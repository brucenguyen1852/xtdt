﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.PermissionApp.LayerPermission
{
    public class PermissionLayerService : CrudAppService<
           PermissionLayer, //The Book entity
           PermissionLayerDto, //Used to show books
           Guid, //Primary key of the book entity
           PagedAndSortedResultRequestDto, //Used for paging/sorting
           CreatePermissionLayerDto>, //Used to create/update a book
       IPermissionLayerService //implement the IBookAppService
    {
        public PermissionLayerService(IRepository<PermissionLayer, Guid> repository) : base(repository)
        {
        }

        public override async Task<PermissionLayerDto> CreateAsync(CreatePermissionLayerDto input)
        {
            var data = new PermissionLayer()
            {
                Name = input.Name,
                KeyName = input.KeyName,
                IsStatus = input.IsStatus
            };

            var obj = await Repository.InsertAsync(data);

            var result = obj.Clone<PermissionLayerDto>();
            return result;
        }

        public override async Task<PermissionLayerDto> UpdateAsync(Guid id, CreatePermissionLayerDto input)
        {
            var currentData = await Repository.GetAsync(id);

            if (currentData == null)
            {
                return null;
            }

            currentData.Name = input.Name;
            currentData.KeyName = input.KeyName;
            currentData.IsStatus = input.IsStatus;

            var obj = await Repository.UpdateAsync(currentData);

            var result = obj.Clone<PermissionLayerDto>();

            return result;
        }

        public async Task<object> GetListPermission(string filter, string Sorting, int SkipCount, int MaxResultCount)
        {
            var lstResult = new List<PermissionLayerDto>();
            var lstObject = new List<PermissionLayer>();
            if (MaxResultCount > 0)
            {
                lstObject = Repository.WhereIf(!string.IsNullOrEmpty(filter), x => x.Name.ToLower().Contains(filter.ToLower())).Skip(SkipCount).Take(MaxResultCount).ToList();
            }
            else
            {
                lstObject = Repository.OrderBy(x => x.CreationTime).ToList();
            }

            var total = await Repository.CountAsync();

            lstObject = lstObject.OrderBy(x => x.CreationTime).ToList();

            lstResult = lstObject.Clone<List<PermissionLayerDto>>();

            var obj = new
            {
                totalCount = total,
                items = lstResult
            };

            return obj;
        }

        public async Task<object> GetListPermissionAll(string search)
        {
            var lstResult = new List<PermissionLayerDto>();

            var lstObject = new List<PermissionLayer>();
            lstObject = Repository.OrderBy(x => x.CreationTime).ToList();

            var total = await Repository.CountAsync();

            lstObject = lstObject.OrderBy(x => x.CreationTime).ToList();

            lstResult = lstObject.Clone<List<PermissionLayerDto>>();

            var obj = new
            {
                totalCount = total,
                items = lstResult
            };

            return obj;
        }

        public bool CheckExitsCode(string id, string code)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var guiId = Guid.Parse(id);
                var lst = this.Repository.Where(x => x.Id != guiId && x.KeyName == code);
                if (lst.Count() > 0)
                {
                    return false;
                }

                return true;
            }
            else
            {
                var lst = this.Repository.Where(x => x.KeyName == code);
                if (lst.Count() > 0)
                {
                    return false;
                }

                return true;
            }
        }

    }
}
