﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Identity;
using Volo.Abp.PermissionManagement;

namespace IoT.PermissionApp.ManagerIdentity
{
    public class ManagerIdentityService : PermissionAppAppService, IManagerIdentityService
    {
        protected IIdentityUserRepository UserRepository { get; }
        protected IIdentityRoleRepository RoleRepository { get; }

        protected IPermissionGrantRepository permissionGrantRepository { get; }

        public ManagerIdentityService(IIdentityUserRepository UserRepository,
                                      IIdentityRoleRepository RoleRepository,
                                      IPermissionGrantRepository permissionGrantRepository)
        {
            this.UserRepository = UserRepository;
            this.RoleRepository = RoleRepository;
            this.permissionGrantRepository = permissionGrantRepository;
        }

        public async Task<PagedResultDto<IdentityUserDto>> GetListUser(GetIdentityUsersInput input)
        {
            var currenid = CurrentUser.Id;
            var lstPermission = await permissionGrantRepository.GetListAsync(x => x.ProviderName == "U" && x.Name == "HTKT.Administration.ManagementData");

            var lstUserId = lstPermission.Select(x => Guid.Parse(x.ProviderKey)).ToList();

            //var lstPermissionRole = await permissionGrantRepository.GetListAsync(x => x.ProviderName == "R" && x.Name == "HTKT.Administration.ManagementData");
            //var lstRoleKey = lstPermissionRole.Select(x => x.ProviderKey).ToList();

            //var lstRole = await RoleRepository.GetListAsync(x => lstRoleKey.Contains(x.Name) && !x.IsStatic);

            //var count = await UserRepository.GetCountAsync(input.Filter); //TODO:
            //var users = await UserRepository.GetListAsync(input.Sorting, input.MaxResultCount, input.SkipCount, input.Filter);

            var users = await UserRepository.GetListAsync(x => lstUserId.Contains(x.Id));

            if (!string.IsNullOrEmpty(input.Filter))
            {
                users = users.Where(u => u.UserName.Contains(input.Filter) ||
                    u.NormalizedUserName.Contains(input.Filter.ToUpper()) ||
                    u.Email.Contains(input.Filter) ||
                    (u.Name != null && u.Name.Contains(input.Filter)) ||
                    (u.Surname != null && u.Surname.Contains(input.Filter)) ||
                    (u.PhoneNumber != null && u.PhoneNumber.Contains(input.Filter))).ToList();
            }

            var roleAdmin = await RoleRepository.GetListAsync(x => x.IsStatic);
            var lstRoleId = roleAdmin.Select(x => x.Id);

            users = users.Where(x => (x.Roles.Count() == 0 || x.Roles.Any(y => !lstRoleId.Contains(y.RoleId))) && x.Id != currenid).ToList();

            var count = users.Count();

            users = users.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();
            //var userss = await UserRepository.GetListAsync(x => x.UserName != "admin" && x.Id != currenid);

            var listUser = users.Clone<List<IdentityUserDto>>();

            return new PagedResultDto<IdentityUserDto>(
                count,
                listUser
            );
        }

        public async Task<PagedResultDto<IdentityRoleDto>> GetListRole(GetIdentityRoleListInput input)
        {
            try
            {
                var lstPermission = await permissionGrantRepository.GetListAsync(x => x.ProviderName == "R" && x.Name == "HTKT.Administration.ManagementData");
                var lstRoleKey = lstPermission.Select(x => x.ProviderKey).ToList();

                var lstRole = await RoleRepository.GetListAsync(x => lstRoleKey.Contains(x.Name) && !x.IsStatic);

                if (!string.IsNullOrEmpty(input.Filter))
                {
                    lstRole = lstRole.Where(x => x.Name.Contains(input.Filter) ||
                        x.NormalizedName.Contains(input.Filter.ToUpper())).ToList();
                }

                var totalCount = lstRole.Count();
                lstRole = lstRole.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

                //var list = await RoleRepository.GetListAsync(input.Sorting, input.MaxResultCount, input.SkipCount, filter: input.Filter);
                //var totalCount = await RoleRepository.GetCountAsync(input.Filter);
                //list = list.Where(x => !x.IsStatic).ToList();

                var listRole = lstRole.Clone<List<IdentityRoleDto>>();
                return new PagedResultDto<IdentityRoleDto>(
                    totalCount,
                    listRole
                );
            }
            catch (Exception ex)
            {
                return new PagedResultDto<IdentityRoleDto>(
                   0,
                   new List<IdentityRoleDto>()
               );
            }

        }

        public async Task<ListResultDto<IdentityRoleDto>> GetRolesAsync(Guid id)
        {
            var roles = await UserRepository.GetRolesAsync(id);
            return new ListResultDto<IdentityRoleDto>(
                ObjectMapper.Map<List<IdentityRole>, List<IdentityRoleDto>>(roles)
            );
        }
        public async Task<IdentityRoleDto> GetByIdAsync(Guid id)
        {
            var list = await RoleRepository.GetAsync(id);
            var listRole = list.Clone<IdentityRoleDto>();
            return listRole;
        }

    }
}
