﻿using IoT.PermissionApp.Localization;
using Volo.Abp.Application.Services;

namespace IoT.PermissionApp
{
    public abstract class PermissionAppAppService : ApplicationService
    {
        protected PermissionAppAppService()
        {
            LocalizationResource = typeof(PermissionAppResource);
            ObjectMapperContext = typeof(PermissionAppApplicationModule);
        }
    }
}
