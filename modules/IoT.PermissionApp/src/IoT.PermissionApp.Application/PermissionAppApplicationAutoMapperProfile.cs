﻿using AutoMapper;
using IoT.PermissionApp.LayerPermission;

namespace IoT.PermissionApp
{
    public class PermissionAppApplicationAutoMapperProfile : Profile
    {
        public PermissionAppApplicationAutoMapperProfile()
        {
            CreateMap<PermissionLayer, PermissionLayerDto>();
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}