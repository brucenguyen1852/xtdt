﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace IoT.PermissionApp.Samples
{
    public class SampleAppService : PermissionAppAppService, ISampleAppService
    {
        public Task<SampleDto> GetAsync()
        {
            return Task.FromResult(
                new SampleDto
                {
                    Value = 42
                }
            );
        }

        [Authorize]
        public Task<SampleDto> GetAuthorizedAsync()
        {
            return Task.FromResult(
                new SampleDto
                {
                    Value = 42
                }
            );
        }
    }
}