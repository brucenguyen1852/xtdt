﻿using Volo.Abp.Localization;

namespace IoT.PermissionApp.Localization
{
    [LocalizationResourceName("PermissionApp")]
    public class PermissionAppResource
    {
        
    }
}
