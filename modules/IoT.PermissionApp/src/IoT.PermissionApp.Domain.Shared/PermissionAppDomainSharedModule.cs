﻿using Volo.Abp.Modularity;
using Volo.Abp.Localization;
using IoT.PermissionApp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Validation;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;
using Volo.Abp.Identity;

namespace IoT.PermissionApp
{
    [DependsOn(
        typeof(AbpValidationModule),
        typeof(AbpIdentityProDomainSharedModule)
    )]
    public class PermissionAppDomainSharedModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<PermissionAppDomainSharedModule>();
            });

            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Add<PermissionAppResource>("en")
                    .AddBaseTypes(typeof(AbpValidationResource))
                    .AddVirtualJson("/Localization/PermissionApp");
            });

            Configure<AbpExceptionLocalizationOptions>(options =>
            {
                options.MapCodeNamespace("PermissionApp", typeof(PermissionAppResource));
            });
        }
    }
}
