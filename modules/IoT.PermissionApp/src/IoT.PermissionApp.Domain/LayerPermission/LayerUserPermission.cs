﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.PermissionApp.LayerPermission
{
    public class LayerUserPermission : AuditedAggregateRoot<Guid>
    {
        public string UserId { get; set; } // id user nếu có id này sẽ không có id roleId
        public string RoleId { get; set; } // id quyền lấy ở bản approle
        public string IdDirectory { get; set; } // id thư mục hoặc lớp
        public List<PermissionDetail> ListPermisson { get; set; } // list quyền của lớp
    }
}
