﻿namespace IoT.PermissionApp
{
    public static class PermissionAppDbProperties
    {
        public static string DbTablePrefix { get; set; } = "PermissionApp";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "PermissionApp";
    }
}
