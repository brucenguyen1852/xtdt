﻿using Volo.Abp.Domain;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace IoT.PermissionApp
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(PermissionAppDomainSharedModule),
        typeof(AbpIdentityProDomainModule)
    )]
    public class PermissionAppDomainModule : AbpModule
    {

    }
}
