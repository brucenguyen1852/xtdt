﻿using Volo.Abp.Settings;

namespace IoT.PermissionApp.Settings
{
    public class PermissionAppSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from PermissionAppSettings class.
             */
        }
    }
}