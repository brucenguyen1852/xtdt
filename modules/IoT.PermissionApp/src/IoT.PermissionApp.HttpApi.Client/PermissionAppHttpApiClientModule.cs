﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace IoT.PermissionApp
{
    [DependsOn(
        typeof(PermissionAppApplicationContractsModule),
        typeof(AbpHttpClientModule),
        typeof(AbpIdentityHttpApiClientModule))]
    public class PermissionAppHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "PermissionApp";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(PermissionAppApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
