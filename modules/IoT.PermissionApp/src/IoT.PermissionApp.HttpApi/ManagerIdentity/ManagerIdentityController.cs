﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Identity;

namespace IoT.PermissionApp.ManagerIdentity
{
    [RemoteService]
    [Route("api/permissionApp/manager_identity")]
    public class ManagerIdentityController : PermissionAppController
    {
        private readonly IManagerIdentityService _managerIdentityService;
        public ManagerIdentityController(IManagerIdentityService managerIdentityService)
        {
            _managerIdentityService = managerIdentityService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("get-list-user")]
        public async Task<ActionResult> GetListUser(GetIdentityUsersInput input)
        {
            try
            {
                var obj = await this._managerIdentityService.GetListUser(input);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("get-list-role")]
        public async Task<ActionResult> GetListRole(GetIdentityRoleListInput input)
        {
            try
            {
                var obj = await this._managerIdentityService.GetListRole(input);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
