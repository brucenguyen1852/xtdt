﻿using IoT.PermissionApp.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace IoT.PermissionApp
{
    public abstract class PermissionAppController : AbpController
    {
        protected PermissionAppController()
        {
            LocalizationResource = typeof(PermissionAppResource);
        }
    }
}
