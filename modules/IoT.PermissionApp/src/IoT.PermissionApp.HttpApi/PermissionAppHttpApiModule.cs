﻿using Localization.Resources.AbpUi;
using IoT.PermissionApp.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Identity;

namespace IoT.PermissionApp
{
    [DependsOn(
        typeof(PermissionAppApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule),
        typeof(AbpIdentityHttpApiModule))]
    public class PermissionAppHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(PermissionAppHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<PermissionAppResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
