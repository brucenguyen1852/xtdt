﻿using IoT.Common;
using IoT.PermissionApp.LayerPermission;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.PermissionApp.PermissonApp
{
    [RemoteService]
    [Route("api/permissionApp/layer_user_permission")]
    public class LayerUserPermissionController : PermissionAppController
    {
        private readonly ILayerUserPermissionService _layerUserPermissionService;
        public LayerUserPermissionController(ILayerUserPermissionService layerUserPermissionService)
        {
            _layerUserPermissionService = layerUserPermissionService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult> CreateFolder(CreateLayerUserPermissionDto dto)
        {
            try
            {
                var create = new CreateLayerUserPermissionDto();
                create.UserId = dto.UserId;
                create.RoleId = dto.RoleId;
                create.IdDirectory = dto.IdDirectory;
                create.ListPermisson = dto.ListPermisson;
                var rs = await _layerUserPermissionService.CreateAsync(create);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, rs));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [HttpGet("get-by-User-Or-Role")]
        public async Task<ActionResult> GetListOfUserOrRole(string UserId, string RoleId)
        {
            try
            {
                if (!string.IsNullOrEmpty(UserId) || !string.IsNullOrEmpty(RoleId))
                {
                    var result = await _layerUserPermissionService.GetListOfUserOrRole(UserId, RoleId);
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Lỗi dữ liệu đầu vào", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("create-many")]
        public async Task<ActionResult> AddRangeLayerUserPermisison(AddRangeLayerUserPermissionFormModel model)
        {
            try
            {
                var listModel = new List<CreateLayerUserPermissionDto>();

                foreach (var item in model.UserPermission)
                {
                    var data = new CreateLayerUserPermissionDto()
                    {
                        IdDirectory = model.IdDirectory,
                        UserId = item.Id,
                        ListPermisson = item.ListPermission,
                        RoleId = string.Empty
                    };

                    listModel.Add(data);
                }

                foreach (var item in model.RolePermission)
                {
                    var data = new CreateLayerUserPermissionDto()
                    {
                        IdDirectory = model.IdDirectory,
                        UserId = string.Empty,
                        ListPermisson = item.ListPermission,
                        RoleId = item.Id
                    };

                    listModel.Add(data);
                }

                var exits = await this._layerUserPermissionService.CreateMany(listModel, model.IdDirectory);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, exits));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDirectory"></param>
        /// <returns></returns>
        [HttpGet("get-permission-layer-by-id")]
        public async Task<ActionResult> GetPermissionLayerAsync(string idDirectory)
        {
            try
            {
                var obj = await this._layerUserPermissionService.GetPermissionLayer(idDirectory);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDirectory"></param>
        /// <returns></returns>
        [HttpGet("get-permission-layer-implement-by-id")]
        public async Task<ActionResult> GetPermissionLayerImplement(string idDirectory)
        {
            try
            {
                var obj = await this._layerUserPermissionService.GetPermissionImplement(idDirectory);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, obj));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-layer-with-permisison-user")]
        public async Task<object> GetLayerWithPermission()
        {
            try
            {
                var result = await _layerUserPermissionService.GetLayerWithPermission();
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
    }
}
