﻿using IoT.Common;
using IoT.PermissionApp.LayerPermission;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.PermissionApp.PermissonApp
{
    [RemoteService]
    [Route("api/permissionApp/permission-layer")]
    public class PermissonLayerController : PermissionAppController
    {
        private readonly IPermissionLayerService _permissionLayerService;
        public PermissonLayerController(IPermissionLayerService permissionLayerService)
        {
            _permissionLayerService = permissionLayerService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="Sorting"></param>
        /// <param name="SkipCount"></param>
        /// <param name="MaxResultCount"></param>
        /// <returns></returns>
        [HttpGet("get-list")]
        public async Task<ActionResult> GetList(string filter,string Sorting, int SkipCount, int MaxResultCount)
        {
            try
            {
                var rs = await _permissionLayerService.GetListPermission(filter, Sorting, SkipCount, MaxResultCount);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, rs));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet("get-list-all")]
        public async Task<ActionResult> GetListAll(string search)
        {
            try
            {
                var rs = await _permissionLayerService.GetListPermissionAll(search);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, rs));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("create-permission-layer")]
        public async Task<ActionResult> CreateLayerBaseMap(FormPermissionLayer param)
        {
            try
            {
                var dto = new CreatePermissionLayerDto()
                {
                    Name = param.Name,
                    KeyName = param.KeyName,
                    IsStatus = param.IsStatus
                };

                var createDto = await _permissionLayerService.CreateAsync(dto);
                if (createDto != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, createDto));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Thêm mới thất bại", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost("update-permission-layer")]
        public async Task<ActionResult> UpdateLayerBaseMap(FormPermissionLayer param)
        {
            try
            {
                if (string.IsNullOrEmpty(param.Id))
                {
                    return Ok(new { code = "error", message = "Id không được bỏ trống", result = "null" });
                }

                var dto = new CreatePermissionLayerDto()
                {
                    Name = param.Name,
                    KeyName = param.KeyName,
                    IsStatus = param.IsStatus
                };

                var idGuid = Guid.Parse(param.Id);
                var updateDto = await _permissionLayerService.UpdateAsync(idGuid, dto);

                if (updateDto != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, updateDto));
                }

                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Cập nhật không thành công", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("check-exits-code")]
        public ActionResult CheckExitsCode(string id, string code)
        {
            try
            {
                var checkExits= _permissionLayerService.CheckExitsCode(id, code);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, checkExits));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, false));
            }
        }
    }
}
