﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.PermissionApp.MongoDB
{
    [ConnectionStringName(PermissionAppDbProperties.ConnectionStringName)]
    public interface IPermissionAppMongoDbContext : IAbpMongoDbContext
    {
        /* Define mongo collections here. Example:
         * IMongoCollection<Question> Questions { get; }
         */
    }
}
