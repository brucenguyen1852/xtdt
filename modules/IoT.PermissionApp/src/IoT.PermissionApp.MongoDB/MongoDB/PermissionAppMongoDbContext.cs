﻿using IoT.PermissionApp.Layer;
using IoT.PermissionApp.LayerPermission;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.PermissionApp.MongoDB
{
    [ConnectionStringName(PermissionAppDbProperties.ConnectionStringName)]
    public class PermissionAppMongoDbContext : AbpMongoDbContext, IPermissionAppMongoDbContext
    {
        /* Add mongo collections here. Example:
         * public IMongoCollection<Question> Questions => Collection<Question>();
         */
        public IMongoCollection<PermissionLayer> PermissionLayers => Collection<PermissionLayer>();
        public IMongoCollection<LayerUserPermission> LayerUserPermissions => Collection<LayerUserPermission>();

        public IMongoCollection<Directory> Directorys => Collection<Directory>();
        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigurePermissionApp();
        }
    }
}