﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace IoT.PermissionApp.MongoDB
{
    public static class PermissionAppMongoDbContextExtensions
    {
        public static void ConfigurePermissionApp(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new PermissionAppMongoModelBuilderConfigurationOptions(
                PermissionAppDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}