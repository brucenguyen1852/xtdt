﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Identity.MongoDB;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace IoT.PermissionApp.MongoDB
{
    [DependsOn(
        typeof(PermissionAppDomainModule),
        typeof(AbpMongoDbModule),
        typeof(AbpIdentityProMongoDbModule)
        )]
    public class PermissionAppMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<PermissionAppMongoDbContext>(options =>
            {
                options.AddDefaultRepositories();
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, MongoQuestionRepository>();
                 */
            });
        }
    }
}
