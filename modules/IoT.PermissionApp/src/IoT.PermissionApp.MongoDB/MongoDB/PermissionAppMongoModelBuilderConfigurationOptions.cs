﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace IoT.PermissionApp.MongoDB
{
    public class PermissionAppMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public PermissionAppMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}