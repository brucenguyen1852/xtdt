﻿using IoT.PermissionApp.Localization;
using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;
using IoT.PermissionApp.Permissions;

namespace IoT.PermissionApp.Web.Menus
{
    public class PermissionAppMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            //Add main menu items.
            var l = context.GetLocalizer<PermissionAppResource>();
           
            var menuManagermentlayer = context.Menu.GetMenuItemOrNull(PermissionAppMenus.ManagementLayer);
            if (menuManagermentlayer != null && await context.IsGrantedAsync(PermissionAppPermissions.PermissionLayer.Default))
            {
                menuManagermentlayer.AddItem(new ApplicationMenuItem(PermissionAppMenus.Prefix, l["Permission:PermissionApp"], url: "~/PermissionApp"));
                menuManagermentlayer.AddItem(new ApplicationMenuItem(PermissionAppMenus.Prefix, l["Permission:PermissionWithLayer"], url: "~/PermissionWithLayer"));
            }
            else
            {
                if (await context.IsGrantedAsync(PermissionAppPermissions.PermissionLayer.Default))
                {
                    context.Menu.AddItem(new ApplicationMenuItem(PermissionAppMenus.Prefix, displayName: l["Permission:PermissionApp"], "~/PermissionApp", icon: "fa fa-globe"));
                }
            }
            //return Task.CompletedTask;
        }
    }
}