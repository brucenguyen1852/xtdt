﻿mn_selected = "mn_permission_layer";

var l = abp.localization.getResource('PermissionApp');
var _layerBaseMapAppService = ioT.hTKT.layerBaseMap;

var PermissionLayer =
{
    /*
        * Global Objects
     */
    GLOBAL:
    {
        table: "",
        ImageChoosen: '',
        OldImage: '',
        ArrayPermissionLayer: []
    },

    /*
        * CONSTANTS  Objects
     */
    CONSTANTS:
    {
        URL_CREATE: '/api/permissionApp/permission-layer/create-permission-layer',
        URL_UPDATE: '/api/permissionApp/permission-layer/update-permission-layer',
        URL_DELETE: '/api/app/permission-layer/',
        URL_GET_LIST_PERMISSION_LAYER: '/api/permissionApp/permission-layer/get-list',
        URL_CHECK_EXITS_CODE: '/api/permissionApp/permission-layer/check-exits-code'
    },

    /*
        * SELECTORS OBJECT
     */
    SELECTORS:
    {
        table: '.table',
        content_Input: ".content-input",
        btn_Save: "#btn-save",
        btn_Image: ".open-image",
        image_layer_map: "#img-layer-map",
        btn_edit: ".edit",
        btn_delete: ".delete",
        btn_huy_edit: "#btn-huy-edit",
        btn_open_form_add_new_Permission:".add-permission",
        // form
        Input_Form: ".input-form",
        Input_Id: "#idPermissionLayer",
        Input_Name: "#namePermissionLayer",
        Input_KeyName: "#keyNamePermissionLayer",
        Input_Status: "#isStatusPermissionLayer",
        btn_edit_row: ".edit-row",
        btn_delete_row: ".delete-row",
        btn_close_modal:"#close-modal",
        modal_form_input: ".modal-input",
        filter_input:".search-table"
    },
    /*
        * Init function
     */
    init: function () {
        this.setCollumsTalbe();
        this.setDataTable();
        this.setUpEvent();
    },
    /*
       * Set up event
    */
    setUpEvent: function () {
        $(PermissionLayer.SELECTORS.btn_Save).on('click', function () {
            if (PermissionLayer.checkFormInfor()) {
                if (abp.auth.isGranted('PermissionApp.PermissionLayer.Create') || abp.auth.isGranted('PermissionApp.PermissionLayer.Edit')) // có phân quyền thêm mới và cập nhật
                {

                    //if (checkDeleteImage)
                    {
                        var url = PermissionLayer.CONSTANTS.URL_CREATE;
                        var message = "";

                        if (!abp.auth.isGranted('PermissionApp.PermissionLayer.Create')) // kiểm phân quyền thêm mới
                        {
                            url = '';
                            message = '';
                        }

                        if ($(PermissionLayer.SELECTORS.Input_Id).val() != "") {
                            url = PermissionLayer.CONSTANTS.URL_UPDATE;
                            message = "";

                            if (!abp.auth.isGranted('PermissionApp.PermissionLayer.Edit') && !abp.auth.isGranted('PermissionApp.PermissionLayer.Create')) // kiểm tra phân quyền cập nhật
                            {
                                url = '';
                                message = '';
                            }
                        }

                        var dto = {
                            id: $(PermissionLayer.SELECTORS.Input_Id).val(),
                            name: $(PermissionLayer.SELECTORS.Input_Name).val(),
                            keyName: $(PermissionLayer.SELECTORS.Input_KeyName).val(),
                            isStatus: $(PermissionLayer.SELECTORS.Input_Status).is(':checked'),
                        }

                        if (url != "") {
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                url: url,
                                data: JSON.stringify(dto),
                                success: function (result) {
                                    if (result.code == "ok") {

                                        abp.notify.success(message + l('PermissionLayer:BaseMapSuccessfully'));

                                        // reset form input
                                        PermissionLayer.clearLabelError();

                                        PermissionLayer.ResetFormInput();

                                        if (!$(PermissionLayer.SELECTORS.btn_huy_edit).hasClass('hidden')) {
                                            $(PermissionLayer.SELECTORS.btn_huy_edit).addClass('hidden');
                                        }

                                        $(PermissionLayer.SELECTORS.modal_form_input).modal('hide'); // đóng modal

                                        PermissionLayer.GLOBAL.table.ajax.reload();
                                        //$('input').val('');
                                    }
                                    else {
                                        swal({
                                            title: l('PermissionLayer:Notification'),
                                            text: message + l('PermissionLayer:BaseMapFailed'),
                                            icon: "error",
                                            button: l('PermissionLayer:Close'),
                                        }).then((value) => {
                                        });
                                    }
                                },
                                error: function () {
                                }
                            });
                        }
                    }

                }
            }
        });

        $(PermissionLayer.SELECTORS.Input_Form).on('keyup change', function () {
            //PermissionLayer.clearLabelError();
            if ($(this).val() != "") {
                $(this).parents(".form-group").find(".lable-error").remove();
                $(this).parents(".form-group").removeClass("has-error");
            }
            //else {
            //    if($(this).parents(".form-group").find(".lable-error").length == 0){
            //        PermissionLayer.showLabelError($(this), $(this).attr('data-required'));
            //    }
            //}
        });

        $(PermissionLayer.SELECTORS.btn_huy_edit).on('click', function () {
            swal({
                title: l('PermissionLayer:Notification'),
                text: l('PermissionLayer:AreYouCancel'),
                icon: "warning",
                buttons: [l('PermissionLayer:Close'), l('PermissionLayer:Agree')]
            }).then((value) => {
                if (value) {
                    $(this).addClass('hidden');
                    PermissionLayer.ResetFormInput();
                }
            });
        })

        $(PermissionLayer.SELECTORS.Input_Name).on("keyup", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            ""
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                this.value = this.value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }

            $(PermissionLayer.SELECTORS.Input_KeyName).val('PermissionLayer.' + value);

            $(PermissionLayer.SELECTORS.Input_KeyName).parents(".form-group").find(".lable-error").remove();
            $(PermissionLayer.SELECTORS.Input_KeyName).parents(".form-group").removeClass("has-error");

        });

        $(document).bind("ajaxSend", function () {
            $('button').prop('disabled', true);
        }).bind("ajaxComplete", function () {

            $('button').prop('disabled', false);
        });

        // open thêm mới permisison
        $(PermissionLayer.SELECTORS.btn_open_form_add_new_Permission).on('click', function () {
            $(PermissionLayer.SELECTORS.modal_form_input).modal('show');
        });

        // sửa hàng
        $(PermissionLayer.SELECTORS.table).on('click', PermissionLayer.SELECTORS.btn_edit_row, function () {
            var id = $(this).attr('data-id');
            $(PermissionLayer.SELECTORS.modal_form_input).modal('show');

            $(PermissionLayer.SELECTORS.btn_huy_edit).removeClass('hidden');
            PermissionLayer.clearLabelError();
            $('.error-image').addClass('hidden');

            var dataLayer = PermissionLayer.GLOBAL.ArrayPermissionLayer.find(x => x.id == id);

            PermissionLayer.setDataToForm(dataLayer);
        });

        // xóa hàng
        $(PermissionLayer.SELECTORS.table).on('click', PermissionLayer.SELECTORS.btn_delete_row, function () {
            var id = $(this).attr('data-id');

            PermissionLayer.ResetFormInput();
            var dataLayer = PermissionLayer.GLOBAL.ArrayPermissionLayer.find(x => x.id == id);
            swal({
                title: l("Layer:Notification"),
                text: l("PermissionLayer:AreYouDeletePermissionLayer"),
                icon: "warning",
                buttons: [
                    l("Layer:Cancel"),
                    l("Layer:Delete")
                ],
                dangerMode: true,
            }).then(function (isConfirm) {

                if (isConfirm) {

                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: PermissionLayer.CONSTANTS.URL_DELETE + id,
                        success: function (result) {
                            abp.notify.success(l("Layer:DeleteBaseMapSuccessfully"));
                            PermissionLayer.GLOBAL.table.ajax.reload();

                        },
                        error: function () {
                        }
                    });
                }

            });

        });

        $(PermissionLayer.SELECTORS.modal_form_input).on('hidden.bs.modal', function () {
            PermissionLayer.ResetFormInput();
        });

        $(PermissionLayer.SELECTORS.modal_form_input).on('show.bs.modal', function () {
            PermissionLayer.ResetFormInput();
        });

        $(PermissionLayer.SELECTORS.btn_close_modal).on('click', function () {
            $(PermissionLayer.SELECTORS.modal_form_input).modal('hide');
        });

        // keyup filter
        $(PermissionLayer.SELECTORS.filter_input).on('keyup', delay(function () {
            PermissionLayer.GLOBAL.table.ajax.reload();
        }, 500));

    },
    checkFormInfor: function () {
        PermissionLayer.clearLabelError();
        let check = true;
        //if (!validateText(inputName, "text", 0, 0)) {

        if ($(PermissionLayer.SELECTORS.Input_Name).val() == '') {
            check = false;

            PermissionLayer.showLabelError(PermissionLayer.SELECTORS.Input_Name, $(PermissionLayer.SELECTORS.Input_Name).attr('data-required'));
        }

        if ($(PermissionLayer.SELECTORS.Input_KeyName).val() == '') {
            check = false;

            PermissionLayer.showLabelError(PermissionLayer.SELECTORS.Input_KeyName, $(PermissionLayer.SELECTORS.Input_KeyName).attr('data-required'));
        }
        else {
            var checkExits = PermissionLayer.CheckExitsKeyName($(PermissionLayer.SELECTORS.Input_Id).val(), $(PermissionLayer.SELECTORS.Input_KeyName).val());
            if (!checkExits) {
                check = checkExits;
            }
        }

        if (check) {
            check = PermissionLayer.CheckedHtmlEntities();
        }

        return check;
    },
    readURL(input, img) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(img).attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    },
    showLabelError: function (element, text) {
        $(element).parents(".form-group").addClass('has-error');

        $(element).parents(".form-group").append("<lable class='col-md-12 lable-error' style='color:red; margin-left: -14px;'>" + text + "</lable>");
    },
    clearLabelError: function () {
        $('.form-group').removeClass("has-error");
        $('.form-group').find(".lable-error").remove();
    },
    setCollumsTalbe: function () {
        // action
        abp.ui.extensions.entityActions.get("permissionlayer").addContributor(
            function (actionList) {
                return actionList.addManyTail(
                    [
                        {
                            text: l('Edit'),
                            visible: abp.auth.isGranted('PermissionApp.PermissionLayer.Edit'),
                            action: function (data) {
                                $(PermissionLayer.SELECTORS.btn_huy_edit).removeClass('hidden');
                                PermissionLayer.clearLabelError();
                                $('.error-image').addClass('hidden');

                                var id = data.record.id;

                                var dataLayer = PermissionLayer.GLOBAL.ArrayPermissionLayer.find(x => x.id == id);

                                PermissionLayer.setDataToForm(dataLayer);
                            }
                        },
                        {
                            text: l('Delete'),
                            visible: abp.auth.isGranted('PermissionApp.PermissionLayer.Delete'),
                            confirmMessage: function (data) {
                                //return l('RoleDeletionConfirmationMessage', data.record.name);
                            },
                            action: function (data) {
                                var id = data.record.id;
                                PermissionLayer.ResetFormInput();
                                var dataLayer = PermissionLayer.GLOBAL.ArrayPermissionLayer.find(x => x.id == id);

                                $.ajax({
                                    type: "DELETE",
                                    dataType: 'json',
                                    contentType: "application/json; charset=utf-8",
                                    url: PermissionLayer.CONSTANTS.URL_DELETE + id,
                                    success: function (result) {

                                        swal({
                                            title: l("PermissionLayer:Notification"),
                                            text: l("PermissionLayer:DeleteBaseMapSuccessfully"),
                                            icon: "success",
                                            button: l("PermissionLayer:Close"),
                                        });

                                        PermissionLayer.GLOBAL.table.ajax.reload();

                                    },
                                    error: function () {
                                    }
                                });
                            }
                        }
                    ]
                );
            }
        );

        // collums
        abp.ui.extensions.tableColumns.get("permissionlayer").addContributor(
            function (columnList) {
                columnList.addManyTail(
                    [
                        {
                            title: l('PermissionLayer:No.'),
                            data: "STT",
                            render: function (data, type, row, index) {
                                var pageInfo = PermissionLayer.GLOBAL.table.page.info();
                                var num = pageInfo.start + (index.row + 1);
                                return num;
                            },
                            orderable: false,
                            width: 50,
                        },
                        {
                            title: l('PermissionLayer:PermissionLayerName'),
                            data: "name",
                            width: 200,
                            render: function (data, type, row) {

                                return `<span>${PermissionLayer.htmlEntities(data)}</span>`;
                            },
                            orderable: false,
                        },
                        {
                            title: l('PermissionLayer:PermissionLayerIsStatus'),
                            data: "isStatus",
                            render: function (data, type, row) {

                                return `<span>${data ? "Công khai" : "Mặc định"}</span>`;
                            },
                            orderable: false,
                            width: 200
                        },
                        //{
                        //    title: l('Actions'),
                        //    render: function (data, type, row) {
                        //        var result = '';
                        //        result += `<a class="edit-row" data-id="${row.id}" href="javascript:;" style="margin-right: 5px;"><svg id="Component_97_1" data-name="Component 97 – 1" xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                        //                          <g id="Group_3211" data-name="Group 3211" transform="translate(-1591 -247)">
                        //                            <rect id="Rectangle_3430" data-name="Rectangle 3430" width="36" height="36" rx="4" transform="translate(1591 247)" fill="#00559a"/>
                        //                            <g id="Group_2917" data-name="Group 2917" transform="translate(1600 256)">
                        //                              <path id="Path_6610" data-name="Path 6610" d="M0,0H18V18H0Z" fill="none"/>
                        //                              <path id="Path_6611" data-name="Path 6611" d="M4.551,15.419h1.1l7.224-7.226-1.1-1.1L4.551,14.322Zm12.41,1.552H3V13.679L13.42,3.256a.775.775,0,0,1,1.1,0l2.194,2.195a.776.776,0,0,1,0,1.1L7.842,15.419h9.119ZM12.872,6l1.1,1.1,1.1-1.1-1.1-1.1L12.872,6Z" transform="translate(-0.98 -0.99)" fill="#fff"/>
                        //                            </g>
                        //                          </g>
                        //                    </svg></a>`;


                        //        result += `<a class="delete-row" data-id="${row.id}" href="javascript:;"><svg id="Component_94_1" data-name="Component 94 – 1" xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                        //                            <rect id="Rectangle_3431" data-name="Rectangle 3431" width="36" height="36" rx="4" fill="red"/>
                        //                            <g id="Group_2931" data-name="Group 2931" transform="translate(9 9)">
                        //                            <path id="Path_6552" data-name="Path 6552" d="M0,0H18V18H0Z" fill="none"/>
                        //                            <path id="Path_6553" data-name="Path 6553" d="M14.9,5.584V15.621a.717.717,0,0,1-.717.717H4.151a.717.717,0,0,1-.717-.717V5.584H2V4.151H16.338V5.584Zm-10.037,0V14.9h8.6V5.584ZM5.584,2h7.169V3.434H5.584ZM8.452,7.735H9.886v5.018H8.452Z" transform="translate(-0.169 -0.169)" fill="#fff"/>
                        //                            </g>
                        //                        </svg></a>`;

                        //        return result;
                        //    },
                        //    orderable: false,
                        //    width: 50
                        //}
                    ]
                );
            },
            0 //adds as the first contributor
        );
    },
    setDataTable: function () {
        PermissionLayer.GLOBAL.table = $(PermissionLayer.SELECTORS.table).DataTable(abp.libs.datatables.normalizeConfiguration({
            //order: [[2, "asc"]],
            lengthMenu:
                [
                    [5, 10, 25, 50, 100],
                    ['5', '10', '25', '50', '100']
                ],

            searching: false,
            processing: true,
            scrollX: true,
            serverSide: true,
            paging: true,
            scrollCollapse: true,
            headerCallback: function (thead, data, start, end, display) {
                $(thead).closest('thead').find('th').addClass('custom-head-table');
                $('#DataTables_Table_0_length').html(l("PermissionLayer:TotalRow") + ": " + data.length);
            },
            createdRow: function (row, data, index) {

            },
            //ajax: abp.libs.datatables.createAjax(_testKhaiThacService.getList, ""),
            ajax: {
                url: PermissionLayer.CONSTANTS.URL_GET_LIST_PERMISSION_LAYER,
                type: 'GET',
                data: function (d) {
                    //d.filter = $('input.page-search-filter-text').val();
                    if (d.columns[d.order[0].column].name != "") {
                        d.sorting = d.columns[d.order[0].column].name + " " + d.order[0].dir;
                    }
                    else {
                        d.sorting = "";
                    }
                    d.filter = $(PermissionLayer.SELECTORS.filter_input).val();
                    d.skipCount = d.start;
                    d.maxResultCount = d.length;

                    delete d.columns;
                    delete d.draw;
                    delete d.length;
                    delete d.order;
                    delete d.search;
                },
                //"dataSrc": "items",
                dataFilter: function (resdata) {
                    var res = JSON.parse(resdata);
                    if (res.code == "ok") {
                        var data = res.result;
                        //var json = jQuery.parseJSON(data);
                        var rs = data;
                        data.recordsTotal = rs.totalCount;
                        data.recordsFiltered = rs.totalCount;
                        data.data = rs.items;
                        PermissionLayer.GLOBAL.ArrayPermissionLayer = rs.items;

                        return JSON.stringify(data); // return JSON string
                    }
                    else {
                        console.log(resdata);
                        return null;
                    }
                },
                complete: function (data) {

                }
            },
            columnDefs: abp.ui.extensions.tableColumns.get("permissionlayer").columns.toArray(),
        }));
    },
    setDataToForm: function (data) {
        $(PermissionLayer.SELECTORS.Input_Name).val(data.name); // data name
        $(PermissionLayer.SELECTORS.Input_KeyName).val(data.keyName); // data keyname
        $(PermissionLayer.SELECTORS.Input_Id).val(data.id); // data id
        $(PermissionLayer.SELECTORS.Input_Status).prop('checked', data.isStatus);
    },
    ResetFormInput: function () {
        $(PermissionLayer.SELECTORS.Input_Name).val(''); // data name
        $(PermissionLayer.SELECTORS.Input_KeyName).val(''); // data keyname
        $(PermissionLayer.SELECTORS.Input_Id).val(''); // data id
        $(PermissionLayer.SELECTORS.Input_Status).prop('checked', false);

        $('.lable-error').remove();
    },
    htmlEntities: function (str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    },
    CheckedHtmlEntities: function () {
        var check = true;
        $('input[type=text]').each(function () {
            var value = $(this).val();
            if ($(this).val() != "") {
                let re = /<[a-zA-Z]*>|<\/[a-zA-Z]*>/g;
                if (re.test(value)) {
                    $(this).parent(".form-group").append("<lable class='col-md-12 lable-error' style='color:red; margin-left: -14px;'>+ " + l('PermissionLayer:DataInputInvalid') + " +</lable>");
                    check = false;
                }
            }
        });
        return check;
    },
    CheckExitsKeyName: function (id, keyname) {
        var reesult = true;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: PermissionLayer.CONSTANTS.URL_CHECK_EXITS_CODE,
            data: {
                id: id,
                code: keyname
            },
            async: false,
            success: function (res) {
                if (res.code == "ok") {
                    if (res.result) {
                        reesult = true;
                    }
                    else {
                        reesult = false;
                        PermissionLayer.showLabelError(PermissionLayer.SELECTORS.Input_Name, "Phân quyền đã được sử dụng");
                    }
                }
                else {
                    console.log(res);
                    reesult = false; 
                }
            },
            error: function () {
            }
        });

        return reesult;
    }
};

/*
* Page loaded
*/
$(document).ready(function () {
    PermissionLayer.init();
});

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
};