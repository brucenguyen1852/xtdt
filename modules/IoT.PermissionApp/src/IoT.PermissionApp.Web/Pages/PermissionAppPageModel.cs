﻿using IoT.PermissionApp.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace IoT.PermissionApp.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class PermissionAppPageModel : AbpPageModel
    {
        protected PermissionAppPageModel()
        {
            LocalizationResourceType = typeof(PermissionAppResource);
            ObjectMapperContext = typeof(PermissionAppWebModule);
        }
    }
}