﻿var PermissionWithLayer = {
    /*
        * Global Objects
     */
    GLOBAL:
    {
        ArrayTree: [],

        TypeShow: 1
    },
    /*
        * CONSTANTS  Objects
     */
    CONSTANTS:
    {
        URL_GET_LAYER_WITH_PERMISSION: '/api/permissionApp/layer_user_permission/get-layer-with-permisison-user'
    },
    /*
       * SELECTORS OBJECT
    */
    SELECTORS:
    {
        select_type: '.select-type',
        table:'table',
        table_body: 'table tbody'
    },
    /*
        * Init function
     */
    init: function () {
        this.setUpEvent();
        this.GetLayerWithPermission();
        this.SetSelect2();
    },
    /*
       * Set up event
    */
    setUpEvent: function () {

        // ---- thay đổi hiển thị quyền của người dùng hoặc vai trò --------
        $(PermissionWithLayer.SELECTORS.select_type).on('change', function () {
            PermissionWithLayer.GLOBAL.TypeShow = parseInt($(this).val());
            PermissionWithLayer.GetLayerWithPermission();
        });

        // ---- scroll table ----
        $(PermissionWithLayer.SELECTORS.table).on('scroll', function () {
            $(PermissionWithLayer.SELECTORS.table + " > *").width($(PermissionWithLayer.SELECTORS.table).width() + $(PermissionWithLayer.SELECTORS.table).scrollLeft());
        });

        // zom in zoom out màn hình
        $(window).resize(function () {
            $(PermissionWithLayer.SELECTORS.table + " > *").width($(PermissionWithLayer.SELECTORS.table).width() + $(PermissionWithLayer.SELECTORS.table).scrollLeft());
            if (screen.width == window.innerWidth) {
                console.log("you are on normal page with 100% zoom");
            } else if (screen.width > window.innerWidth) {
                console.log("you have zoomed in the page i.e more than 100%");
            } else {
                console.log("you have zoomed out i.e less than 100%")
            }
        });
    },
    GetLayerWithPermission: function () {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                abp.ui.setBusy();
            },
            url: PermissionWithLayer.CONSTANTS.URL_GET_LAYER_WITH_PERMISSION,
            success: function (result) {
                if (result.code == "ok") {

                    var data = result.result;
                    PermissionWithLayer.GLOBAL.ArrayTree = data;
                    var parent = data.find(x => x.level == "0");

                    var treedynamic = PermissionWithLayer.FillDynaTree(parent);
                    var html = PermissionWithLayer.GenTable(treedynamic);
                    $(PermissionWithLayer.SELECTORS.table_body).html('');
                    $(PermissionWithLayer.SELECTORS.table_body).html(html);
                    //console.log(treedynamic)
                }
                else {
                }
            },
            complete: function () {
                abp.ui.clearBusy();
            },
            error: function () {
            }
        });
    },
    //dynatree
    FillDynaTree: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            icon: null,
            level: data.level,
            parentId: data.parentId,
            listUser: data.users,
            listRole: data.roles
        };

        var parent = PermissionWithLayer.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    obj.icon = data.image2D;
                }
            }

            obj.key = parent.id;
            var lstChil = PermissionWithLayer.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = PermissionWithLayer.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    GenTable: function (data) {
        var tree = data.children;

        var html = '';
        var paddingParent = 0;

        if (data.level > 1)
        {
            for (var i = data.level; i > 1; i--)
            {
                paddingParent += (i + 20);
            }
        }

        $.each(tree, function (index, node)
        {
            var padding = 0;
            if (node.level > 1) {
                padding = paddingParent + 20;
            }

            html += '<tr>';
            html += `<td class="tree-col"><span style = "padding-left: ${padding}px;">${node.title}</span></td>`;

            var valueVIEW = 'admin';
            var valueCREATE = 'admin';
            var valueEDIT = 'admin';
            var valueDELETE = 'admin';
            var valuePERMISSION = 'admin';

            if (PermissionWithLayer.GLOBAL.TypeShow == 1)
            {
                // kiểm tra tất cả quyền của user
                $.each(node.listUser, function (indexUser, user) {
                    $.each(user.listPermission, function (indexPermission, permission) {
                        switch (permission.keyName)
                        {
                            case "PermissionLayer.VIEW":
                                valueVIEW += ", " + user.name;
                                break;
                            case "PermissionLayer.CREATE":
                                valueCREATE += ", " + user.name;
                                break;
                            case "PermissionLayer.EDIT":
                                valueEDIT += ", " + user.name;
                                break;
                            case "PermissionLayer.DELETE":
                                valueDELETE += ", " + user.name;
                                break;
                            case "PermissionLayer.PERMISSION":
                                valuePERMISSION += ", " + user.name;
                                break;
                            default:
                                break;
                        }
                    });
                });
            }
            else {
                // kiểm tra tất cả quyền của role
                $.each(node.listRole, function (indexUser, role) {
                    $.each(role.listPermission, function (indexPermission, permission) {
                        switch (permission.keyName) {
                            case "PermissionLayer.VIEW":
                                valueVIEW += ", " + role.name;
                                break;
                            case "PermissionLayer.CREATE":
                                valueCREATE += ", " + role.name;
                                break;
                            case "PermissionLayer.EDIT":
                                valueEDIT += ", " + role.name;
                                break;
                            case "PermissionLayer.DELETE":
                                valueDELETE += ", " + role.name;
                                break;
                            case "PermissionLayer.PERMISSION":
                                valuePERMISSION += ", " + role.name;
                                break;
                            default:
                                break;
                        }
                    });
                });
            }

            html += `<td class="permission-col">${valueVIEW}</td>`;
            html += `<td class="permission-col">${valueCREATE}</td>`;
            html += `<td class="permission-col">${valueEDIT}</td>`;
            html += `<td class="permission-col">${valueDELETE}</td>`;
            html += `<td class="permission-col">${valuePERMISSION}</td>`;
            html += '</tr>';

            if (node.children.length > 0)
            {
                html += PermissionWithLayer.GenTable(node);
            }
        });

        return html;
        //$('table tbody').html(html);
    },
    SetSelect2: function () {
        $(PermissionWithLayer.SELECTORS.select_type).select2();
    }
}


/*
* Page loaded
*/
$(document).ready(function () {
    PermissionWithLayer.init();
});

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
};