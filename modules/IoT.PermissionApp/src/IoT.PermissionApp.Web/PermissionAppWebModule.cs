﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.DependencyInjection;
using IoT.PermissionApp.Localization;
using IoT.PermissionApp.Web.Menus;
using Volo.Abp.AspNetCore.Mvc.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.UI.Navigation;
using Volo.Abp.VirtualFileSystem;
using IoT.PermissionApp.Permissions;
using Volo.Abp.Identity.Web;

namespace IoT.PermissionApp.Web
{
    [DependsOn(
        typeof(PermissionAppHttpApiModule),
        typeof(AbpAspNetCoreMvcUiThemeSharedModule),
        typeof(AbpAutoMapperModule),
        typeof(AbpIdentityWebModule)
        )]
    public class PermissionAppWebModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.PreConfigure<AbpMvcDataAnnotationsLocalizationOptions>(options =>
            {
                options.AddAssemblyResource(typeof(PermissionAppResource), typeof(PermissionAppWebModule).Assembly);
            });

            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(PermissionAppWebModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpNavigationOptions>(options =>
            {
                options.MenuContributors.Add(new PermissionAppMenuContributor());
            });

            Configure<AbpVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<PermissionAppWebModule>();
            });

            context.Services.AddAutoMapperObjectMapper<PermissionAppWebModule>();
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<PermissionAppWebModule>(validate: true);
            });

            Configure<RazorPagesOptions>(options =>
            {
                options.Conventions.AuthorizePage("/PermissionApp/Index", PermissionAppPermissions.PermissionLayer.Default);
                //Configure authorization.
            });
        }
    }
}
