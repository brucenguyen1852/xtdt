﻿namespace IoT.PermissionApp
{
    /* Inherit from this class for your application layer tests.
     * See SampleAppService_Tests for example.
     */
    public abstract class PermissionAppApplicationTestBase : PermissionAppTestBase<PermissionAppApplicationTestModule>
    {

    }
}