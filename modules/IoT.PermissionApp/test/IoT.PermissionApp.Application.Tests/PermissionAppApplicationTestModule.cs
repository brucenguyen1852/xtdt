﻿using Volo.Abp.Modularity;

namespace IoT.PermissionApp
{
    [DependsOn(
        typeof(PermissionAppApplicationModule),
        typeof(PermissionAppDomainTestModule)
        )]
    public class PermissionAppApplicationTestModule : AbpModule
    {

    }
}
