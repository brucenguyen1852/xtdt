﻿namespace IoT.PermissionApp
{
    /* Inherit from this class for your domain layer tests.
     * See SampleManager_Tests for example.
     */
    public abstract class PermissionAppDomainTestBase : PermissionAppTestBase<PermissionAppDomainTestModule>
    {

    }
}