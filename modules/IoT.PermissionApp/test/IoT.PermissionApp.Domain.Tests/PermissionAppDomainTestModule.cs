using IoT.PermissionApp.MongoDB;
using Volo.Abp.Modularity;

namespace IoT.PermissionApp
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(PermissionAppMongoDbTestModule)
        )]
    public class PermissionAppDomainTestModule : AbpModule
    {
        
    }
}
