﻿using IoT.PermissionApp.Samples;

namespace IoT.PermissionApp.EntityFrameworkCore.Samples
{
    public class SampleRepository_Tests : SampleRepository_Tests<PermissionAppEntityFrameworkCoreTestModule>
    {
        /* Don't write custom repository tests here, instead write to
         * the base class.
         * One exception can be some specific tests related to EF core.
         */
    }
}
