﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace IoT.PermissionApp
{
    [DependsOn(
        typeof(PermissionAppHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class PermissionAppConsoleApiClientModule : AbpModule
    {
        
    }
}
