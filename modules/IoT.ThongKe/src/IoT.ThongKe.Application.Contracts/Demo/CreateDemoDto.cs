﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.ThongKe.Demo
{
    public class CreateDemoDto
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
