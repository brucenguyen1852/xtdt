﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.ThongKe.Demo
{
    public class DemoDto : AuditedEntityDto<Guid>
    {
        public bool IsDeleted { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
