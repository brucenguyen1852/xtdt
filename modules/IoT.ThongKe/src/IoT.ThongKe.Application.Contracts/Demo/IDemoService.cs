﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe.Demo
{
    public interface IDemoService: ICrudAppService< //Defines CRUD methods
            DemoDto, //Used to show 
            Guid, //Primary key of the  entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDemoDto> //Used to create/update a 
    {
        Task<List<DemoDto>> GetListDemoAsync();
    }
}
