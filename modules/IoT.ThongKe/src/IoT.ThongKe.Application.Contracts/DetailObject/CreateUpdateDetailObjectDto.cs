﻿using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.ThongKe.DetailObject
{
    public class CreateUpdateDetailObjectDto
    {
        public string IdMainObject { get; set; }
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
    }
}
