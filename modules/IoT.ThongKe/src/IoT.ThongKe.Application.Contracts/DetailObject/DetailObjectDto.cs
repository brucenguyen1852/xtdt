﻿using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.ThongKe.DetailObject
{
    public class DetailObjectDto : AuditedEntityDto<Guid>
    {
        public string IdMainObject { get; set; }
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
    }
}
