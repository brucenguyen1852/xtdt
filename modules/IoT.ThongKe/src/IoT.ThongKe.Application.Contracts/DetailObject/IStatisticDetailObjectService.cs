﻿using IoT.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe.DetailObject
{
    public interface IStatisticDetailObjectService : ICrudAppService< //Defines CRUD methods
            DetailObjectDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateDetailObjectDto> //Used to create/update a PropertiesDirectory
    {
        object GetAllDetailMainObject(List<string> lstMainId, List<FilterProperties> filters, int skip, int result);

        Task<DetailObjectDto> GetAsyncById(string idMainObject);
    }
}
