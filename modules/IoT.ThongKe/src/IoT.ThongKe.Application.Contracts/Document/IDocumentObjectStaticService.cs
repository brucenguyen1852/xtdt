﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe.Document
{
    public interface IDocumentObjectStaticService : ICrudAppService<
            DocumentObjectDTO,
            Guid,
            PagedAndSortedResultRequestDto>
    {
        Task<List<DocumentObjectDTO>> GetListDocumentByIdMainObject(string id);
    }
}
