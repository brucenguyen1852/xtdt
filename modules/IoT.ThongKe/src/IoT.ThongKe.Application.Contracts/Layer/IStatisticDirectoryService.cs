﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe.Layer
{
    public interface IStatisticDirectoryService : ICrudAppService< //Defines CRUD methods
            StatisticDirectoryDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto>
    {

    }
}
