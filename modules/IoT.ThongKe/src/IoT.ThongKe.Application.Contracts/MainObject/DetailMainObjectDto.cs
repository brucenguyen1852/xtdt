﻿using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.ThongKe.MainObject
{
    public class DetailMainObjectDto 
    {
        public Guid Id { get; set; }
        public string NameDirectory { get; set; }
        public string Name { get; set; }
        public string  PlanToDo { get; set; } 
        public string  HostName { get; set; }
        public string District { get; set; }
        public string WardDistrict { get; set; }
        public string Acreage { get; set; }
        public string Location { get; set; }
        public string ProjectPregress { get; set; }
        public string ProjectCost { get; set; }
    }
}
