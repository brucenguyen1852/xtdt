﻿using IoT.ThongKe.Common;
using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.ThongKe.MainObject
{
    public class GetMainObjectInput : PagedAndSortedResultRequestDto
    { 
        public string FilterText { get; set; }
        public string IdDirectory { get; set; }
        public string NameObject { get; set; }
        public string HostName { get; set; }
        public string Location { get; set; }
        public GetMainObjectInput()
        {

        }
    }
}
