﻿using IoT.ThongKe.DetailObject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe.MainObject
{
    public interface IMainObjectAppService : IApplicationService
    {
        Task<PagedResultDto<MainObjectDto>> GetListAsync(GetMainObjectInput input);
        Task<MainObjectDto> GetAsync(Guid id);
        Task<PagedResultDto<DetailMainObjectDto>> GetAll(GetMainObjectInput input);
    }
}
