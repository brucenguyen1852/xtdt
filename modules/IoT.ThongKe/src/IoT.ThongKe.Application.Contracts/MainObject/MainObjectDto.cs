﻿using IoT.ThongKe.Common;
using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.ThongKe.MainObject
{
    public class MainObjectDto : AuditedEntityDto<Guid>
    {
        public string IdDirectory { get; set; }
        public string NameObject { get; set; } 
    }
}
