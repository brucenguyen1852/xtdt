﻿using IoT.ThongKe.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace IoT.ThongKe.Permissions
{
    public class ThongKePermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(ThongKePermissions.GroupName, L("Permission:ThongKe"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<ThongKeResource>(name);
        }
    }
}