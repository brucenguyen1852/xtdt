﻿using Volo.Abp.Reflection;

namespace IoT.ThongKe.Permissions
{
    public class ThongKePermissions
    {
        public const string GroupName = "ThongKe";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(ThongKePermissions));
        }

        /*public class MainObject
        {
            public const string Default = GroupName + ".MainObject";
            public const string Export = Default + ".Export";
        }*/
    }
}