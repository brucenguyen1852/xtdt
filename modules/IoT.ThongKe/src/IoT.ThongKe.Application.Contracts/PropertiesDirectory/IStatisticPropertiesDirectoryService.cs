﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe.PropertiesDirectory
{
    public interface IStatisticPropertiesDirectoryService: ICrudAppService< //Defines CRUD methods
            PropertiesDirectoryDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdatePropertiesDirectoryDto> //Used to create/update a PropertiesDirectory
    {
        Task<PropertiesDirectoryDto> GetAsyncById(string idDirectory);
    }
}
