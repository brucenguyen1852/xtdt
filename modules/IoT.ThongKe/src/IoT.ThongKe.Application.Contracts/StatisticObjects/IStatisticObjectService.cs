﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe.StatisticObjects
{
    public interface IStatisticObjectService : ICrudAppService< //Defines CRUD methods
            StatictisObjectDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto> //Used for paging/sorting
    {
        Task<object> StatisticQtyObjectByDirectory(string id);
        List<InfoObjectData> GetObjectByIdDriectoryExport(string id, string interval);

        Task<object> GetListAllObjectByIdDriectory(string id, int skipnumber, int countnumber, bool havegeometry);
    }
}
