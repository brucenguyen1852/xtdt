﻿using IoT.ThongKe.Common;
using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.ThongKe.StatisticObjects
{
    public class InfoObjectData
    {
        public string Id { get; set; }
        public string NameObject { get; set; } // Tên đối tượng
        public Geometry Geometry { get; set; }
        public List<PropertiesObject> ListProperties { get; set; } // Bảnh thuộc tính
        public string IdDirectory { get; set; }
        public PropertiesGeojson Properties { get; set; } = new PropertiesGeojson(); // properties geojson 2D
        public Object3D object3D { get; set; }
        public Dictionary<string, object> Tags { get; set; }

        public int TotalCount { get; set; }
    }
}
