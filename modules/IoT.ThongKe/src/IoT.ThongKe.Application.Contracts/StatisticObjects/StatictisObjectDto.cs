﻿using IoT.ThongKe.Common;
using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace IoT.ThongKe.StatisticObjects
{
    public class StatictisObjectDto : AuditedEntityDto<Guid>
    {
        public string IdDirectory { get; set; } //Id của thư mục
        public string NameObject { get; set; } // Tên đối tượng
        public Geometry Geometry { get; set; }
        public PropertiesGeojson Properties { get; set; } = new PropertiesGeojson(); // properties geojson 2D
        public Object3D object3D { get; set; }
    }
}
