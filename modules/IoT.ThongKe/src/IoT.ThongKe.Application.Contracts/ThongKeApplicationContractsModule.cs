﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace IoT.ThongKe
{
    [DependsOn(
        typeof(ThongKeDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class ThongKeApplicationContractsModule : AbpModule
    {

    }
}
