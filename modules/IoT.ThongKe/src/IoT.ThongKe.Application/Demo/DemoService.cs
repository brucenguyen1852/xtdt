﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.ThongKe.Demo
{
    public class DemoService : CrudAppService<
            Demo, //The Book entity
            DemoDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateDemoDto>, //Used to create/update a book
        IDemoService //implement the IBookAppService
    {
        public DemoService(IRepository<Demo, Guid> repository) : base(repository)
        {
        }

        public override async Task<DemoDto> CreateAsync(CreateDemoDto input)
        {
            try
            {
                Demo layerDataType = new Demo();
                Demo layerData = new Demo()
                {
                    Value = input.Value.Trim(),
                    Text = input.Text.Trim(),
                    IsDeleted = false
                };
                layerDataType = await Repository.InsertAsync(layerData);

                var json2 = JsonConvert.SerializeObject(layerDataType);
                var result = JsonConvert.DeserializeObject<DemoDto>(json2);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<DemoDto>> GetListDemoAsync()
        {
            List<DemoDto> layerDataTypeDtoDtos = new List<DemoDto>();
            try
            {
                var objdirectory = await Repository.GetListAsync();
                if (objdirectory != null)
                {
                    var json = JsonConvert.SerializeObject(objdirectory);
                    layerDataTypeDtoDtos = JsonConvert.DeserializeObject<List<DemoDto>>(json);
                }

                return layerDataTypeDtoDtos;
            }
            catch (Exception ex)
            {
                //throw ex;
                return layerDataTypeDtoDtos;
            }
        }
    }
}
