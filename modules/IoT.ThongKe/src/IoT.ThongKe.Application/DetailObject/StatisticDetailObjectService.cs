﻿using IoT.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.ThongKe.DetailObject
{
    public class StatisticDetailObjectService :
        CrudAppService<
            DetailObject, //The DetailObject entity
            DetailObjectDto, //Used to show DetailObject
            Guid, //Primary key of the DetailObject entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateDetailObjectDto>, //Used to create/update a DetailObject
        IStatisticDetailObjectService //implement the MainObjeIMainObjectServicect
    {
        public StatisticDetailObjectService(IRepository<DetailObject, Guid> repository)
              : base(repository)
        {

        }
        public object GetAllDetailMainObject(List<string> lstMainId, List<FilterProperties> filters, int skip, int maxresult)
        {

                var activitydetails = new List<DetailObject>();
                var totalcount = 0;
                if (maxresult == 0)
                {
                    var uiactivitydetails = this.Repository.Where(x => (lstMainId != null && lstMainId.Contains(x.IdMainObject)));
                    //var detail = details.FindAll(x => x.IdMainObject == id).FirstOrDefault();
                    if (filters != null)
                    {
                        foreach (var item in filters)
                        {
                            if (item.Type == "checkbox")
                            {
                                uiactivitydetails = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key && y.TypeProperties == "checkbox"));
                                var lstobj = new List<DetailObject>();
                                foreach (var im in uiactivitydetails)
                                {
                                    var lst = im.ListProperties.Find(x => x.TypeProperties == "checkbox" && !string.IsNullOrWhiteSpace(x.DefalutValue));
                                    if (lst != null)
                                    {
                                        var arr = lst.DefalutValue.ToLower().Split(',').Contains(item.Search.ToLower());
                                        if (arr)
                                        {
                                            lstobj.Add(im);
                                        }
                                    }
                                }

                                uiactivitydetails = lstobj.AsQueryable();
                                //var zz = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key)).ToList();
                            }
                            else if (item.Type == "radiobutton" || item.Type == "list")
                            {
                                uiactivitydetails = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key && y.DefalutValue.ToLower().Contains(item.Search.ToLower())));
                            }
                            else
                            {
                                uiactivitydetails = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key && y.DefalutValue.ToLower() == item.Search.ToLower()));
                            }
                        }
                    }

                    activitydetails = uiactivitydetails.ToList();
                }
                else
                {
                    var uiactivitydetails = Repository.Where(x => (lstMainId != null && lstMainId.Contains(x.IdMainObject)));
                    if (filters != null)
                    {
                        foreach (var item in filters)
                        {
                            if (item.Type == "checkbox")
                            {
                                uiactivitydetails = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key && y.TypeProperties == "checkbox"));
                                var lstobj = new List<DetailObject>();
                                foreach(var im in uiactivitydetails)
                                {
                                    var lst = im.ListProperties.Find(x => x.TypeProperties == "checkbox" && !string.IsNullOrWhiteSpace(x.DefalutValue));
                                    if(lst != null)
                                    {
                                        var arr = lst.DefalutValue.ToLower().Split(',').Contains(item.Search.ToLower());
                                        if (arr)
                                        {
                                            lstobj.Add(im);
                                        }
                                    }
                                }

                                uiactivitydetails = lstobj.AsQueryable();
                                //var zz = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key)).ToList();
                            }
                            else if (item.Type == "radiobutton" || item.Type == "list")
                            {
                                uiactivitydetails = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key && y.DefalutValue.ToLower().Contains(item.Search.ToLower())));
                            }
                            else
                            {
                                uiactivitydetails = uiactivitydetails.Where(x => x.ListProperties.Any(y => y.CodeProperties == item.Key && y.DefalutValue.ToLower().Contains(item.Search.ToLower())));
                            }
                        }
                    }

                    totalcount = uiactivitydetails.ToList().Count();
                    activitydetails = uiactivitydetails.Skip(skip).Take(maxresult).ToList();
                    //totalcount = Repository.Count(x => (lstMainId != null && lstMainId.Contains(x.IdMainObject)));
                }

                var result = activitydetails.Clone<List<DetailObjectDto>>();

                return new { items = result, totalCount = totalcount };
        }

        public async Task<DetailObjectDto> GetAsyncById(string idMainObject)
        {
            try
            {
                if (!string.IsNullOrEmpty(idMainObject))
                {
                    var objdirectory = await Repository.FindAsync(x => x.IdMainObject == idMainObject);

                    var json = JsonConvert.SerializeObject(objdirectory);
                    DetailObjectDto result = JsonConvert.DeserializeObject<DetailObjectDto>(json);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
        }
    }
}
