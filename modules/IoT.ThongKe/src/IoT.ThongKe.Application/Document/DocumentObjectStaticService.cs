﻿using IoT.Common;
using IoT.ThongKe.DocumentObjectStatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.ThongKe.Document
{
    public class DocumentObjectStaticService : CrudAppService<
            DocumentObject, //The DetailObject entity
            DocumentObjectDTO, //Used to show DetailObject
            Guid, //Primary key of the DetailObject entity
            PagedAndSortedResultRequestDto>,
            IDocumentObjectStaticService
    {
        public DocumentObjectStaticService(IRepository<DocumentObject, Guid> repository)
            : base(repository)
        {

        }
        public async Task<List<DocumentObjectDTO>> GetListDocumentByIdMainObject(string id)
        {
                var list = Repository.Where(x => x.IdMainObject == id).ToList();
                var result = list.Clone<List<DocumentObjectDTO>>();
                return result;
        }
    }
}
