﻿using IoT.Common;
using IoT.ThongKe.DetailObject;
using IoT.ThongKe.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using IoT.ThongKe.PropertiesDirectory;
using IoT.HTKT.Layer;

namespace IoT.ThongKe.MainObject
{
    public class MainObjectAppService : ApplicationService, IMainObjectAppService
    {
        private readonly IMainObjectRepository _mainobjectRepository;
        private readonly IDetailObjectRepository _detailobjectRepository;
        private readonly IDirectoryService _directory;
        public MainObjectAppService(IMainObjectRepository mainobjectRepository, IDetailObjectRepository detailobjectRepository, IDirectoryService directory)
        {
            _mainobjectRepository = mainobjectRepository;
            _detailobjectRepository = detailobjectRepository;
            _directory = directory;
        }

        public virtual async Task<PagedResultDto<DetailMainObjectDto>> GetAll(GetMainObjectInput input)
        {
            try
            {
                var totalCount = await _mainobjectRepository.GetCountAsync(input.FilterText, input.IdDirectory, input.NameObject);
                var items = await _mainobjectRepository.GetListAsync(input.FilterText, input.IdDirectory, input.NameObject, input.Sorting, input.MaxResultCount, input.SkipCount);
                var detail = await _detailobjectRepository.GetListAsync();
                var result = new List<DetailMainObjectDto>();

                if (items.Count != 0)
                {
                    foreach (var item in items)
                    {
                        var item_detail = detail.Find(x => x.IdMainObject == item.Id.ToString());
                        var data_maindetail = new DetailMainObjectDto();
                        if (item_detail != null)
                        {
                            foreach (var data in item_detail.ListProperties)
                            {
                                switch (data.CodeProperties)
                                {
                                    case "PlanToDo":
                                        data_maindetail.PlanToDo = data.DefalutValue;
                                        break;
                                    case "Acreage":
                                        data_maindetail.Acreage = data.DefalutValue;
                                        break;
                                    case "ProjectPregress":
                                        data_maindetail.ProjectPregress = data.DefalutValue;
                                        break;
                                    case "HostName":
                                        data_maindetail.HostName = data.DefalutValue;
                                        break;
                                    case "Location":
                                        data_maindetail.Location = data.DefalutValue;
                                        break;
                                    case "ProjectCost":
                                        data_maindetail.ProjectCost = data.DefalutValue;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            data_maindetail.District = item.District; 
                            data_maindetail.WardDistrict = item.WardDistrict;
                            data_maindetail.Name = item.NameObject;
                            data_maindetail.Id = item.Id;
                            //data_maindetail.NameDirectory = 
                        }
                        result.Add(data_maindetail);
                    }
                }


                //Filter

                if (!string.IsNullOrWhiteSpace(input.FilterText))
                {
                    var i = input.FilterText.ToUpper();
                    result = result.Where(x => x.Name.ToUpper().Contains(i) || x.HostName.ToUpper().Contains(i) || x.Location.ToUpper().Contains(i)).ToList();
                }
                else
                {
<<<<<<< Updated upstream
                    return new PagedResultDto<DetailMainObjectDto>()
                    {
                        Items = result,
                        TotalCount = totalCount
=======
                    return new PagedResultDto<DetailMainObjectDto>
                    {
                        TotalCount = result.Count,
                        Items = result
>>>>>>> Stashed changes
                    };
                }    

                return new PagedResultDto<DetailMainObjectDto>
                {
                    TotalCount = result.Count,
                    Items = result
                };
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public virtual async Task<MainObjectDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<Object.MainObject, MainObjectDto>(await _mainobjectRepository.GetAsync(id));
        }

        public virtual async Task<PagedResultDto<MainObjectDto>> GetListAsync(GetMainObjectInput input)
        {
            var totalCount = await _mainobjectRepository.GetCountAsync(input.FilterText, input.IdDirectory, input.NameObject);
            var items = await _mainobjectRepository.GetListAsync(input.FilterText, input.IdDirectory, input.NameObject, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<MainObjectDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Object.MainObject>, List<MainObjectDto>>(items)
            };
        }
    }
}
