﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.ThongKe.PropertiesDirectory
{
    public class StatisticPropertiesDirectoryService :
        CrudAppService<
            IoT.ThongKe.Layer.PropertiesDirectory, //The PropertiesDirectory entity
            PropertiesDirectoryDto, //Used to show PropertiesDirectory
            Guid, //Primary key of the PropertiesDirectory entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdatePropertiesDirectoryDto>, //Used to create/update a PropertiesDirectory
        IStatisticPropertiesDirectoryService //implement the IPropertiesDirectoryAppService
    {
        public StatisticPropertiesDirectoryService(IRepository<IoT.ThongKe.Layer.PropertiesDirectory, Guid> repository) : base(repository)
        {
        }

        public async Task<PropertiesDirectoryDto> GetAsyncById(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var objdirectory = await Repository.FindAsync(x => x.IdDirectory == id);
                    if (objdirectory == null)
                    {
                        objdirectory = await Repository.FindAsync(x => x.Id == new Guid(id));
                    }
                    var json = JsonConvert.SerializeObject(objdirectory);
                    PropertiesDirectoryDto result = JsonConvert.DeserializeObject<PropertiesDirectoryDto>(json);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
        }
    }
}
