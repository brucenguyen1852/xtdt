﻿ using IoT.Common;
using IoT.ThongKe.DetailObject;
using IoT.ThongKe.Layer;
using IoT.ThongKe.Object;
using IoT.ThongKe.PropertiesDirectory;
using IoT.ThongKe.StatisticObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;


namespace IoT.ThongKe.StatisticsObject
{
    public class StatisticObjectService : CrudAppService<
            Object.MainObject, //The Book entity
            StatictisObjectDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto>,
            IStatisticObjectService
    {
        private readonly IStatisticDirectoryService _statisticDirectoryService;
        private readonly IStatisticPropertiesDirectoryService _statisticPropertiesDirectoryService;
        private readonly IStatisticDetailObjectService _statisticDetailObjectService;
        public StatisticObjectService(IRepository<Object.MainObject, Guid> repository, IStatisticDirectoryService statisticDirectoryService,
           IStatisticPropertiesDirectoryService statisticPropertiesDirectoryService, IStatisticDetailObjectService statisticDetailObjectService) : base(repository)
        {
            _statisticDirectoryService = statisticDirectoryService;
            _statisticPropertiesDirectoryService = statisticPropertiesDirectoryService;
            _statisticDetailObjectService = statisticDetailObjectService;
        }

        public async Task<object> GetListAllObjectByIdDriectory(string id, int skipnumber, int countnumber, bool havegeometry)
        {
            var lstResult = new List<InfoObjectData>();
            
                List<Object.MainObject> lstObject = Repository.Where(x => x.IdDirectory == id).Skip(skipnumber).Take(countnumber).ToList();
                //lstObject = lstObject.Skip(dataTableViewModel.start).Take(dataTableViewModel.length).ToList();

                lstResult = lstObject.Clone<List<InfoObjectData>>();

                foreach (var item in lstResult)
                {
                    if (!havegeometry)
                    {
                        item.Geometry = null;
                    }
                    item.object3D = null;
                    item.Tags = null;
                    var detail = await _statisticDetailObjectService.GetAsyncById(item.Id.ToString());
                    if (detail != null)
                    {
                        item.ListProperties = detail.ListProperties;
                    }
                }

                return lstResult;
        }

        public List<InfoObjectData> GetObjectByIdDriectoryExport(string id, string interval)
        {
            var lstResult = new List<InfoObjectData>();
            List<Object.MainObject> objectz = new List<Object.MainObject>();
            //int totalcount = 0;
            
                if (interval == "Chọn thời gian")
                {
                    objectz = Repository.Where(x => x.IdDirectory == id).ToList();
                }
                else
                {
                    var intervalTime = interval ?? DateTime.Now.AddMonths(-1).AddDays(2).ToString("dd/MM/yyyy") + "-" +
                    DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");

                    var intervalTimes = intervalTime.Split('-');
                    var startDate = DateTime.ParseExact(intervalTimes[0], "dd/MM/yyyy", CultureInfo.CurrentCulture);
                    var endDate = DateTime.ParseExact(intervalTimes[1], "dd/MM/yyyy", CultureInfo.CurrentCulture);

                    endDate = endDate.AddDays(1);

                    objectz = Repository.Where(x => x.IdDirectory == id && x.CreationTime != null
                                                               && (x.CreationTime >= startDate && x.CreationTime <= endDate)).ToList();

                    //if (result == 0)
                    //{
                    //    objectz = Repository.Where(x => x.IdDirectory == id && x.CreationTime != null
                    //                                            && (x.CreationTime >= startDate && x.CreationTime <= endDate)).ToList();
                    //}
                    //else
                    //{
                    //    objectz = Repository.Where(x => x.IdDirectory == id && x.CreationTime != null
                    //                                                && (x.CreationTime >= startDate && x.CreationTime <= endDate)).Skip(skip).Take(result).ToList();
                    //    totalcount = Repository.Count(x => x.IdDirectory == id && x.CreationTime != null
                    //                                                && (x.CreationTime >= startDate && x.CreationTime <= endDate));
                    //}
                }
                lstResult = objectz.Clone<List<InfoObjectData>>();
                return lstResult;
        }

        public async Task<object> StatisticQtyObjectByDirectory(string id)
        {
            var count = await Repository.CountAsync(x => x.IdDirectory == id);
            //var listObject = await Repository.GetListAsync();
            //var listObjectByDirectory = listObject.FindAll(x => x.IdDirectory == id);
            var objDirectory = await _statisticDirectoryService.GetAsync(Guid.Parse(id));
            var properties = await _statisticPropertiesDirectoryService.GetAsyncById(objDirectory.Id.ToString());
            return new
            {
                total = count,
                objDirectory = objDirectory,
                properties = properties.ListProperties
            };
        }
    }
}
