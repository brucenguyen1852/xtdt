﻿using IoT.ThongKe.Layer;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace IoT.ThongKe.StatitsticDirectory
{
    public class StatisticDirectoryService : CrudAppService<
            Directory, //The Book entity
            StatisticDirectoryDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto>,
            IStatisticDirectoryService
    {
        public StatisticDirectoryService(IRepository<Directory, Guid> repository) : base(repository)
        {

        }
    }
}
