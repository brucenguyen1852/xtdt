﻿using IoT.ThongKe.Localization;
using Volo.Abp.Application.Services;

namespace IoT.ThongKe
{
    public abstract class ThongKeAppService : ApplicationService
    {
        protected ThongKeAppService()
        {
            LocalizationResource = typeof(ThongKeResource);
            ObjectMapperContext = typeof(ThongKeApplicationModule);
        }
    }
}
