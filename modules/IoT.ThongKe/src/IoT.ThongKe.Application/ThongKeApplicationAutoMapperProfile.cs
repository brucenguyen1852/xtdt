﻿using AutoMapper;
using IoT.ThongKe.Layer;
using IoT.ThongKe.MainObject;
using IoT.ThongKe.StatisticObjects;

namespace IoT.ThongKe
{
    public class ThongKeApplicationAutoMapperProfile : Profile
    {
        public ThongKeApplicationAutoMapperProfile()
        {
            CreateMap<Object.MainObject, StatictisObjectDto>();
            CreateMap<Directory, StatisticDirectoryDto>();
            CreateMap<Object.MainObject, MainObjectDto>();
            //CreateMap<Object.MainObject, DetailMainObjectDto>();
        }
    }
}