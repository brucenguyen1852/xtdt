﻿using IoT.ThongKe.MainObject;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace IoT.ThongKe
{
    [DependsOn(
        typeof(ThongKeDomainModule),
        typeof(ThongKeApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class ThongKeApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAutoMapperObjectMapper<ThongKeApplicationModule>();
            context.Services.AddSingleton<MainObjectAppService>();
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<ThongKeApplicationModule>(validate: true);
            });
        }
    }
}
