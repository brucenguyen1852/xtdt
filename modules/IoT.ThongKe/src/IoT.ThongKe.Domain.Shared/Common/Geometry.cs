﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.ThongKe.Common
{
    public class Geometry
    {
        /// <summary>
        /// Type (Polygon or MultiLineString)
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Coordinates
        /// </summary>
        [JsonProperty("coordinates")]
        public IList<object> Coordinates { get; set; }
    }
}
