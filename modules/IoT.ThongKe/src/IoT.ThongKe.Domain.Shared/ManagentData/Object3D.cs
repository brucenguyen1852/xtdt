﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.ThongKe.ManagentData
{
    public class Object3D
    {
        public string Id { get; set; }
        public string ObjectUrl { get; set; }
        public string Texture { get; set; }
        public Location Location { get; set; }
        public string Name { get; set; }
        public double Scale { get; set; }
        public double Bearing { get; set; }
        public double Elevation { get; set; }
        public int Height { set; get; }
        public List<Location> Coordinates { set; get; }
        public bool Status { set; get; }
        public string Type { get; set; } = string.Empty;
    }

    public class Location
    {
        public double Lng { get; set; }
        public double Lat { set; get; }
    }
}
