﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace IoT.ThongKe.Demo
{
    public class Demo : AuditedAggregateRoot<Guid>
    {
        public bool IsDeleted { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
