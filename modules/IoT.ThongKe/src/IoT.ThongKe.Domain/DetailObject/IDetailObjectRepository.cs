﻿using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace IoT.ThongKe.DetailObject
{
    public interface IDetailObjectRepository : IRepository<DetailObject,Guid>
    {
        Task<List<DetailObject>> GetListAsync(
            string IdMainObject = null,
            List<PropertiesObject> listProperties = null,
            CancellationToken cancellationToken = default);
    }
}
