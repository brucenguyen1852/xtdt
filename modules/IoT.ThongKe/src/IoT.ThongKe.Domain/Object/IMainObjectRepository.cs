﻿using IoT.ThongKe.Common;
using IoT.ThongKe.ManagentData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace IoT.ThongKe.Object
{
    public interface IMainObjectRepository : IRepository<MainObject, Guid>
    {
        Task<List<MainObject>> GetListAsync(
            string filterText = null,
            string idDirectory = null,
            string nameObject = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default);
        Task<long> GetCountAsync(
            string filterText = null,
            string idDirectory = null,
            string nameObject = null,
            CancellationToken cancellationToken = default
            );
        //Task<List<MainObject>> GetAll(
        //    string filterText = null,
        //    string idDirectory = null,
        //    string nameObject = null,
        //    string sorting = null,
        //    int maxResultCount = int.MaxValue,
        //    int skipCount = 0,
        //    CancellationToken cancellationToken = default);
    }
}
