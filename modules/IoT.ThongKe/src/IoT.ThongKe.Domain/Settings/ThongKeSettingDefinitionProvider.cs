﻿using Volo.Abp.Settings;

namespace IoT.ThongKe.Settings
{
    public class ThongKeSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from ThongKeSettings class.
             */
        }
    }
}