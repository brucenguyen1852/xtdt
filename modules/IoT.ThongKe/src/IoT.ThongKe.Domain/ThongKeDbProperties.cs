﻿namespace IoT.ThongKe
{
    public static class ThongKeDbProperties
    {
        public static string DbTablePrefix { get; set; } = "ThongKe";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "ThongKe";
    }
}
