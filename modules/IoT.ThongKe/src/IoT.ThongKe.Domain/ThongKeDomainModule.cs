﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace IoT.ThongKe
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(ThongKeDomainSharedModule)
    )]
    public class ThongKeDomainModule : AbpModule
    {

    }
}
