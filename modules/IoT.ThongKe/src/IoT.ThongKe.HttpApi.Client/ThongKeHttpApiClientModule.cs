﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace IoT.ThongKe
{
    [DependsOn(
        typeof(ThongKeApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class ThongKeHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "ThongKe";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(ThongKeApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
