﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.ThongKe.Demo
{
    [RemoteService]
    [Microsoft.AspNetCore.Mvc.Route("api/HTKT/Demo2")]
    public class DemoController : ThongKeController
    {
        private readonly IDemoService _layerDataTypeService;

        public DemoController(IDemoService layerDataTypeService)
        {
            _layerDataTypeService = layerDataTypeService;
        }

        [HttpPost("create")]
        public async Task<ActionResult> Create(CreateDemoDto param)
        {
            try
            {
                var obj = await _layerDataTypeService.CreateAsync(param);
                return Ok(new { code = "ok", message = "", result = obj });
            }
            catch (Exception ex)
            {
                return Ok(new { code = "error", message = ex.Message, result = "null" });
            }
        }

        [HttpGet("get-list")]
        public async Task<ActionResult> GetList()
        {
            try
            {
                var obj = await _layerDataTypeService.GetListDemoAsync();
                return Ok(new { code = "ok", message = "", result = obj });
            }
            catch (Exception ex)
            {
                return Ok(new { code = "error", message = ex.Message, result = "null" });
            }
        }
    }
}
