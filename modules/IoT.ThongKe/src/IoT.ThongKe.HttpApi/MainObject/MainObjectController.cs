﻿using IoT.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace IoT.ThongKe.MainObject
{
    [RemoteService(Name = "ThongKe")]
    [Route("api/thongke/main-object")]
    public class MainObjectController : ThongKeController
    {
        private readonly IMainObjectAppService _mainObjectAppService;
        public MainObjectController(IMainObjectAppService mainObjectAppService)
        {
            _mainObjectAppService = mainObjectAppService;
        }

        [HttpGet("get-list-async")]
        public virtual Task<PagedResultDto<MainObjectDto>> GetListAsync(GetMainObjectInput input)
        {
            return _mainObjectAppService.GetListAsync(input);
        }

        [HttpGet("get-all")]
        public virtual Task<PagedResultDto<DetailMainObjectDto>> GetAll(GetMainObjectInput input)
        {
            return _mainObjectAppService.GetAll(input);
        }


        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> GetAsync(Guid id)
        {
            try
            {
                var res = await this._mainObjectAppService.GetAsync(id);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, res));
            }
            catch (Exception e)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, e.Message, string.Empty));
            }
        }
    }
}
