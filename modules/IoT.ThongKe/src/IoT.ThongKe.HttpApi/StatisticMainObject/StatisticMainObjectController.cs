﻿using ClosedXML.Excel;
using IoT.Common;
using IoT.ThongKe.DetailObject;
using IoT.ThongKe.Document;
using IoT.ThongKe.PropertiesDirectory;
using IoT.ThongKe.StatisticObjects;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace IoT.ThongKe.StatisticMainObject
{
    [RemoteService]
    [Route("api/Statistic/MainObject")]
    public class StatisticMainObjectController : ThongKeController
    {
        private readonly IStatisticObjectService _statisticObjectService;
        private readonly IStatisticPropertiesDirectoryService _propertiesDirectoryAppService;
        private readonly IStatisticDetailObjectService _detailObjectService;
        private readonly IDocumentObjectStaticService _documentObjectStatisticService;
        public StatisticMainObjectController(IStatisticObjectService statisticObjectService,
                                            IStatisticPropertiesDirectoryService propertiesDirectoryAppService,
                                            IStatisticDetailObjectService detailObjectService,
                                            IDocumentObjectStaticService documentObjectStatisticService)
        {
            _statisticObjectService = statisticObjectService;
            _propertiesDirectoryAppService = propertiesDirectoryAppService;
            _detailObjectService = detailObjectService;
            _documentObjectStatisticService = documentObjectStatisticService;
        }

        [HttpGet("ObjectByIdDirectory")]
        public async Task<object> ObjectByIdDirectory(string id)
        {
            try
            {
                var result = await _statisticObjectService.StatisticQtyObjectByDirectory(id);
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
            }
            catch(Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        #region export
        [HttpPost("export-directory")]
        public async Task<ActionResult> Export(ExportDirectoryModel model)
        {
            var Messageee = "";
            try
            {
                Messageee += "start# ";
                DataTable dt = await GetData(model);
                string fileName = /*dt.TableName + ".xlsx"; */ "Danh_sach_doi_tuong.xlsx";

                // create avatars directory if not exist
                var serverAvatarsPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/BaoCao/BangBieu");

                if (!System.IO.Directory.Exists(serverAvatarsPath))
                {
                    System.IO.Directory.CreateDirectory(serverAvatarsPath);
                }

                string oldPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/BaoCao/BangBieu/" + fileName);
                if (System.IO.File.Exists(oldPath))
                {
                    System.IO.File.Delete(oldPath);
                }

                Messageee += "WorkBOOk# ";
                using (XLWorkbook wb = new XLWorkbook())
                {
                    Messageee += "Add DataTable in worksheet";
                    //Add DataTable in worksheet  
                    var wsDep = wb.Worksheets.Add(dt);

                    for (int i = 1; i <= dt.Columns.Count; i++)
                    {
                        wsDep.Cell(1, i).Style.Fill.SetBackgroundColor(XLColor.GreenPigment);
                        //wsDep.Cell(2, i).Style.Fill.SetBackgroundColor(XLColor.GreenPigment);
                    }

                    var rowEnd = dt.Rows.Count + 2;
                    var rage = wsDep.Range(wsDep.Cell(rowEnd, 2), wsDep.Cell(rowEnd, dt.Columns.Count));
                    rage.Merge(true);
                    rage.Value = dt.Rows.Count;

                    rage.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    rage.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    rage.Style.Font.Bold = true;

                    wsDep.Cell(rowEnd, 1).Value = "Tổng";
                    wsDep.Cell(rowEnd, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    wsDep.Cell(rowEnd, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    wsDep.Cell(rowEnd, 1).Style.Font.Bold = true;


                    Messageee += "/AdjustToContents# ";
                    wsDep.Columns().AdjustToContents();

                    Messageee += "/SAVES# ";
                    wb.SaveAs(oldPath);

                    Messageee += "/ASVEt# ";
                }
                //return Ok(new { code = "ok", message = "", result = true, fileName = dt.TableName + ".xlsx"});
                
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, dt.TableName + ".xlsx"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, ""));
            }
        }
        private async Task<DataTable> GetData(ExportDirectoryModel model)
        {
            try
            {
                //Creating DataTable  
                DataTable dt = new DataTable();

                var directoryInfo = await this._propertiesDirectoryAppService.GetAsyncById(model.IdDirectory);

                //Setiing Table Name  
                dt.TableName =  directoryInfo.NameTypeDirectory.Replace(" ", "_");/*  "Danh_sach_doi_tuong";*/

                //Add Columns  
                dt.Columns.Add("STT", typeof(string));

                var lstProperties = directoryInfo.ListProperties.Where(x => x.TypeSystem != 1
                                                        && model.ListCode.Contains(x.CodeProperties)).OrderBy(x => x.OrderProperties).ToList();

                var lstMainObject = _statisticObjectService.GetObjectByIdDriectoryExport(model.IdDirectory, model.Interval);

                //var lstMainObject = items.Clone<Items>();

                var xlistData = lstMainObject.Select(x => x.Id);

                var objectItem = _detailObjectService.GetAllDetailMainObject(xlistData.ToList(), model.Filters, 0, 0);

                var lstMainDeitalObject = new List<DetailObjectDto>();

                var items = objectItem.Clone<Items>();
                if (items != null)
                {
                    lstMainDeitalObject = items.items.Clone<List<DetailObjectDto>>();
                }

                //lstMainDeitalObject = lstMainDeitalObject.Where(x => xlistData.Contains(x.IdMainObject)).ToList();
                var lstThead = new List<string>();
                var dem = 0;
                foreach (var item in lstProperties)
                {
                    //if (item.TypeProperties != "file")
                    //{
                        var name = item.NameProperties;
                        if (lstThead.Contains(name))
                        {
                            dem++;
                            name = item.NameProperties + "_" + dem;
                        }
                        else
                        {
                            dem = 0;
                        }

                        dt.Columns.Add(name, typeof(string));
                        lstThead.Add(name);
                    //}
                }

                //dt.Rows.Add(row);

                var stt = 1;
                //Add Rows in DataTable .
                foreach (var item in lstMainDeitalObject)
                {
                    var lstPropertiesValue = item.ListProperties;

                    if (lstPropertiesValue != null)
                    {
                        lstPropertiesValue = lstPropertiesValue.Where(x => x.TypeSystem != 1 /*&& x.TypeProperties != "file"*/
                                                            && model.ListCode.Contains(x.CodeProperties)).OrderBy(x => x.OrderProperties).ToList();
                        DataRow rowItem = dt.NewRow();
                        rowItem["STT"] = stt;

                        var lstBody = new List<string>();
                        var demBody = 0;

                        foreach (var item2 in lstPropertiesValue)
                        {
                            var obj = lstProperties.Find(x => x.CodeProperties == item2.CodeProperties); //item2.NameProperties;
                            var key = obj.NameProperties;
                            if (lstBody.Contains(key))
                            {
                                demBody++;
                                key = item2.NameProperties + "_" + demBody;
                            }
                            else
                            {
                                demBody = 0;
                            }

                            lstBody.Add(key);


                            //if (item2.TypeProperties == "checkbox" || item2.TypeProperties == "list" || item2.TypeProperties == "radiobutton")
                            //{
                            //    rowItem[key] = string.Empty;

                            //    if (!string.IsNullOrEmpty(item2.DefalutValue))
                            //    {
                            //        if (CheckConvert(item2.DefalutValue))
                            //        {
                            //            var lstItems = JsonConvert.DeserializeObject<List<ItemValueProperties>>(item2.DefalutValue);
                            //            //var aa = JsonConvert.SerializeObject(lstItems);
                            //            if (lstItems != null)
                            //            {
                            //                var valueItem = lstItems.Find(x => x.Checked == true);

                            //                if (valueItem != null)
                            //                {
                            //                    rowItem[key] = valueItem.Name;
                            //                }
                            //            }
                            //        }else
                            //            rowItem[key] = item2.DefalutValue;
                            //    }
                            //}
                            //else
                            //{
                            try
                            {
                                    switch (item2.TypeProperties)
                                    {
                                        case "text":
                                            string sText = HTML.HtmlToPlainText(item2.DefalutValue);
                                            rowItem[key] = sText.ReplaceFirst("\n", "", StringComparison.Ordinal);
                                            break;
                                        case "image":
                                            if (string.IsNullOrEmpty(item2.DefalutValue))
                                            {
                                                rowItem[key] = string.Empty;
                                            }
                                            else
                                            {
                                                var array = JsonConvert.DeserializeObject<List<ImageLinkUrl>>(item2.DefalutValue);
                                                rowItem[key] = string.Join(Environment.NewLine, array.Select(x => x.url));
                                            }
                                            break;
                                        case "link":
                                            if (string.IsNullOrEmpty(item2.DefalutValue))
                                            {
                                                rowItem[key] = string.Empty;
                                            }
                                            else
                                            {
                                                var arrayLink = JsonConvert.DeserializeObject<List<ImageLinkUrl>>(item2.DefalutValue);
                                                rowItem[key] = string.Join(Environment.NewLine, arrayLink.Select(x => x.url));
                                            }
                                            break;
                                        case "file":
                                            var listDocument = await _documentObjectStatisticService.GetListDocumentByIdMainObject(item.IdMainObject);
                                            if (listDocument.Count() > 0)
                                            {
                                                rowItem[key] = string.Join(Environment.NewLine, listDocument.Select(x => x.UrlDocument));
                                            }
                                            else
                                                rowItem[key] = string.Empty;
                                            break;
                                        case "checkbox":
                                        case "list":
                                        case "radiobutton":
                                            rowItem[key] = string.Empty;

                                            if (!string.IsNullOrEmpty(item2.DefalutValue))
                                            {
                                                if (CheckConvert(item2.DefalutValue))
                                                {
                                                    var lstItems = JsonConvert.DeserializeObject<List<ItemValueProperties>>(item2.DefalutValue);
                                                    if (lstItems != null)
                                                    {
                                                        var valueItem = lstItems.Where(x => x.Checked == true).ToList();
                                                        string value = "";
                                                        foreach (var valitem in valueItem)
                                                        {
                                                            if (valitem != null)
                                                            {
                                                                value = (value.Length <= 1) ? valitem.Name : string.Format("{0},{1}", value, valitem.Name);
                                                            }
                                                        }
                                                        rowItem[key] = value;
                                                    }
                                                }
                                                else
                                                    rowItem[key] = item2.DefalutValue;
                                            }
                                            break;
                                        case "geojson":
                                        var objMain = lstMainObject.Find(x => x.Id == item.IdMainObject);
                                        if (objMain != null)
                                        {
                                            var str = JsonConvert.SerializeObject(objMain.Geometry);//.Replace("\"", "'");
                                            if (str.Length <= 32766)
                                            {
                                                rowItem[key] = str;
                                            }
                                            else
                                                rowItem[key] = string.Empty;
                                        }
                                        break;
                                        default:
                                            rowItem[key] = item2.DefalutValue;
                                            break;
                                    }
                                }
                                catch
                                {
                                    rowItem[key] = string.Empty;
                                }
                            //}
                        }

                        dt.Rows.Add(rowItem);
                        stt++;
                    }

                    //dt.Rows.Add(item.Name, item.Code, item.StreetName, item.CityName, item.DistrictName,
                    //            item.WardName, item.Address, item.ButtressSingle, item.ButtressDouble, item.StopLine, item.RouteCount,
                    //            item.RouteCode, item.HasBusStopHome, item.Description, item.Status, item.BusStopType, item.ProjectName, item.Latitude, item.Longitude);

                }

                dt.AcceptChanges();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost("review-export")]
        public async Task<ActionResult> ReviewExport(ReviewExportPangingModel model)
        {
            try
            {
                var directoryInfo = await this._propertiesDirectoryAppService.GetAsyncById(model.IdDirectory);

                var lstProperties = directoryInfo.ListProperties.Where(x => x.TypeSystem != 1 /*&& x.TypeProperties != "file"*/
                                                        && model.ListCode.Contains(x.CodeProperties)).OrderBy(x => x.OrderProperties).ToList();

                var lstMainObject = _statisticObjectService.GetObjectByIdDriectoryExport(model.IdDirectory, model.Interval);

                //var lstMainObjectClont = lstMainObject.Clone<Items>();

                var xlistData = lstMainObject.Select(x => x.Id);

                var objectItem = _detailObjectService.GetAllDetailMainObject(xlistData.ToList(), model.Filters == null ? new List<FilterProperties>() : model.Filters, model.SkipCount, model.MaxResultCount);

                var lstMainDeitalObject = new List<DetailObjectDto>();

                var items = objectItem.Clone<Items>();
                if (items != null)
                {
                    lstMainDeitalObject = items.items.Clone<List<DetailObjectDto>>();
                }

                //var xlistData = lstMainObjectClont.items.Clone<List<InfoObjectData>>().Select(x => x.Id);

                //var lstMainDeitalObject = _detailObjectService.GetAllDetailMainObject(xlistData.ToList(), new List<FilterProperties>(), model.SkipCount, model.MaxResultCount);


                //lstMainDeitalObject = lstMainDeitalObject.Skip(model.SkipCount).Take(model.MaxResultCount).ToList();

                //return Ok(new { items = lstMainDeitalObject, listProperties = lstProperties, totalCount = items.totalCount });
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, new { items = lstMainDeitalObject, listProperties = lstProperties, totalCount = items.totalCount }));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, ""));
            }

        }
        #endregion
        #region --- private----
        private bool CheckConvert(string value)
        {
            try
            {
                JsonConvert.DeserializeObject<List<ItemValueProperties>>(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        #endregion

        [HttpGet("get-objectProperties-by-IdDirectory")]
        public async Task<ActionResult> GetObjectPropertiesByIdDirectory(string id, int skipnumber, int countnumber)
        {
            try
            {
                var result = await _statisticObjectService.GetListAllObjectByIdDriectory(id, skipnumber, countnumber, false);
                if (result != null)
                {
                    return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, result));
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorData, "Không tìm thấy dữ liệu", "null"));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        #region export
        [HttpGet("exportdata")]
        public async Task<object> ExportData(string idDirectory)
        {
            var Messageee = "";
            try
            {
                Messageee += "start# ";
                DataTable dt = await GetDataCharStatistic(idDirectory);
                string fileName = "Danh_sach_doi_tuong_" + idDirectory + ".xlsx";

                // create avatars directory if not exist
                var serverAvatarsPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/BaoCao/ChartStatistic");

                if (!System.IO.Directory.Exists(serverAvatarsPath))
                {
                    System.IO.Directory.CreateDirectory(serverAvatarsPath);
                }

                string oldPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), $"wwwroot/BaoCao/ChartStatistic/" + fileName);
                if (System.IO.File.Exists(oldPath))
                {
                    System.IO.File.Delete(oldPath);
                }

                Messageee += "WorkBOOk# ";
                using (XLWorkbook wb = new XLWorkbook())
                {
                    Messageee += "Add DataTable in worksheet";
                    //Add DataTable in worksheet  
                    var wsDep = wb.Worksheets.Add(dt);

                    for (int i = 1; i <= dt.Columns.Count; i++)
                    {
                        wsDep.Cell(1, i).Style.Fill.SetBackgroundColor(XLColor.GreenPigment);
                        //wsDep.Cell(2, i).Style.Fill.SetBackgroundColor(XLColor.GreenPigment);
                    }

                    Messageee += "/AdjustToContents# ";
                    wsDep.Columns().AdjustToContents();

                    Messageee += "/SAVES# ";
                    wb.SaveAs(oldPath);

                    Messageee += "/ASVEt# ";
                }
                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, true));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }

        private async Task<DataTable> GetDataCharStatistic(string idDirectory)
        {
            //Creating DataTable  
            DataTable dt = new DataTable();
            List<InfoObjectData> xlistdata = new List<InfoObjectData>();
            var lst = await this._statisticObjectService.GetListAllObjectByIdDriectory(idDirectory, 0, Int32.MaxValue, true);
            xlistdata = lst.Clone<List<InfoObjectData>>();
            var header = await _propertiesDirectoryAppService.GetAsyncById(idDirectory);
            //Setiing Table Name  
            dt.TableName = "Danh_sach_doi_tuong";

            //Add Columns  
            dt.Columns.Add("STT", typeof(string));
            //dt.Columns.Add("Tên đối tượng của dữ liệu", typeof(string));

            if (xlistdata.Count == 0)
            {
                return null;
            }

            DataRow row = dt.NewRow();
            row["STT"] = "";
            //row["Tên hoạt động của dữ liệu"] = "NameDirectoryActivity";
            //row["Ngày tháng"] = "DateActivity";

            var lstProperties = header.ListProperties.Where(x => x.TypeSystem != 1).OrderBy(x => x.OrderProperties).ToList();
            //propertiesDirectoryActivity.ListProperties.Where(x => x.TypeSystem != 1 && x.TypeProperties != "file").OrderBy(x => x.OrderProperties).ToList();

            foreach (var item in lstProperties)
            {
                if (item.TypeProperties != "maptile")
                {
                    dt.Columns.Add(item.NameProperties, typeof(string));
                    row[item.NameProperties] = item.CodeProperties;
                }
            }

            //dt.Rows.Add(row);

            var stt = 1;
            //Add Rows in DataTable 
            foreach (var item in xlistdata)
            {
                DataRow rowItem = dt.NewRow();
                rowItem["STT"] = stt.ToString();
                //rowItem["Tên đối tượng của dữ liệu"] = string.IsNullOrEmpty(item.NameObject) ? string.Empty : item.NameObject;
                //rowItem["Ngày tháng"] = item.DateActivity?.AddDays(1).ToString("dd/MM/yyyy");

                var valueProperties = item.ListProperties.Where(x => x.TypeSystem != 1).OrderBy(x => x.OrderProperties);

                foreach (var item3 in lstProperties)
                {
                    var item2 = valueProperties.Where(x => x.CodeProperties == item3.CodeProperties).FirstOrDefault();
                    if (item2 != null)
                    {
                        if (item2.TypeProperties != "maptile")
                        {
                            if (item2.TypeProperties == "checkbox" || item2.TypeProperties == "list" || item2.TypeProperties == "radiobutton")
                            {
                                rowItem[item3.NameProperties] = string.Empty;
                                try
                                {
                                    var lstItems = JsonConvert.DeserializeObject<List<ItemValueProperties>>(item2.DefalutValue);
                                    var aa = JsonConvert.SerializeObject(lstItems);
                                    if (lstItems != null)
                                    {
                                        var valueItem = lstItems.FindAll(x => x.Checked == true);

                                        if (valueItem != null && valueItem.Count > 0)
                                        {
                                            if (item2.TypeProperties != "checkbox")
                                            {
                                                rowItem[item3.NameProperties] = valueItem.FirstOrDefault().Name;
                                            }
                                            else
                                            {
                                                rowItem[item3.NameProperties] = string.Join(",", valueItem.Select(x => x.Name));
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                    var propertyLayer = lstProperties.Find(x => x.CodeProperties == item2.CodeProperties);
                                    if (propertyLayer != null)
                                    {
                                        var danhSachValue = JsonConvert.DeserializeObject<List<ItemValueProperties>>(propertyLayer.DefalutValue);
                                        if (item2.TypeProperties != "checkbox")
                                        {
                                            var valueItem = danhSachValue.Find(x => x.Code == item2.DefalutValue);

                                            if (valueItem != null)
                                            {
                                                rowItem[item3.NameProperties] = valueItem.Name;
                                            }
                                        }
                                        else
                                        {
                                            var lstCheck = new List<string>();
                                            if (!string.IsNullOrEmpty(item2.DefalutValue))
                                            {
                                                lstCheck = item2.DefalutValue.Split(",").ToList();
                                            }

                                            var arrayCheckboxObject = danhSachValue.FindAll(x => lstCheck.Contains(x.Code));
                                            if (arrayCheckboxObject.Count() > 0)
                                            {
                                                rowItem[item3.NameProperties] = string.Join(",", arrayCheckboxObject.Select(x => x.Name));
                                            }

                                        }
                                    }

                                    //rowItem[item2.NameProperties] = item2.DefalutValue;
                                }
                            }
                            else
                            {
                                try
                                {
                                    var valueProperty = string.Empty;
                                    switch (item2.TypeProperties)
                                    {
                                        case "text":
                                            string sText = HTML.HtmlToPlainText(item2.DefalutValue);

                                            //string ts = string.Join(Environment.NewLine, System.Xml.Linq.XDocument.Parse(item2.DefalutValue).Root.Elements().Select(el => el.Value));
                                            valueProperty = sText.ReplaceFirst("\n", "", StringComparison.Ordinal);
                                            valueProperty = (valueProperty.IndexOf("\n") == valueProperty.Length - 1) ? valueProperty.Substring(0, valueProperty.Length - 1) : valueProperty;
                                            break;
                                        case "image":
                                            if (!string.IsNullOrEmpty(item2.DefalutValue))
                                            {
                                                var array = JsonConvert.DeserializeObject<List<ImageLinkUrl>>(item2.DefalutValue);
                                                valueProperty = string.Join(Environment.NewLine, array.Select(x => x.url));
                                            }
                                            break;
                                        case "link":
                                            if (!string.IsNullOrEmpty(item2.DefalutValue))
                                            {
                                                var arrayLink = JsonConvert.DeserializeObject<List<ImageLinkUrl>>(item2.DefalutValue);
                                                valueProperty = string.Join(Environment.NewLine, arrayLink.Select(x => x.url));
                                            }
                                            break;
                                        case "geojson":
                                            valueProperty = JsonConvert.SerializeObject(item.Geometry);
                                            if (valueProperty.Length > 32766)
                                            {
                                                valueProperty = string.Empty;
                                            }

                                            break;
                                        case "file":
                                            var listDocument = await _documentObjectStatisticService.GetListDocumentByIdMainObject(item.Id);
                                            if (listDocument.Count() > 0)
                                            {
                                                valueProperty = string.Join(Environment.NewLine, listDocument.Select(x => x.UrlDocument));
                                            }
                                            else
                                                valueProperty = string.Empty;
                                            break;
                                        default:
                                            valueProperty = item2.DefalutValue;
                                            break;
                                    }

                                    if (row.ItemArray.Contains(item2.CodeProperties))
                                    {
                                        rowItem[item3.NameProperties] = valueProperty;
                                    }
                                }
                                catch
                                {
                                    rowItem[item3.NameProperties] = string.Empty;
                                }
                            }
                        }
                    }
                    else
                    {
                        rowItem[item3.NameProperties] = "";
                    }
                }

                dt.Rows.Add(rowItem);

                //dt.Rows.Add(item.Name, item.Code, item.StreetName, item.CityName, item.DistrictName,
                //            item.WardName, item.Address, item.ButtressSingle, item.ButtressDouble, item.StopLine, item.RouteCount,
                //            item.RouteCode, item.HasBusStopHome, item.Description, item.Status, item.BusStopType, item.ProjectName, item.Latitude, item.Longitude);
                stt++;
            }

            dt.AcceptChanges();
            return dt;
        }

        [HttpGet("get-file-by-objectId")]
        public async Task<ActionResult> GetFileByObjectId(string id)
        {
            try
            {
                var listDocument = await _documentObjectStatisticService.GetListDocumentByIdMainObject(id);

                return Ok(CommonResponse.CreateResponse(ResponseCodes.Ok, string.Empty, listDocument));
            }
            catch (Exception ex)
            {
                return Ok(CommonResponse.CreateResponse(ResponseCodes.ErrorException, ex.Message, "null"));
            }
        }
        #endregion
    }

    public class ExportDirectoryModel
    {
        public string IdDirectory { get; set; }
        public List<string> ListCode { get; set; }

        public string Interval { get; set; }

        public List<FilterProperties> Filters { get; set; }
    }

    public class ReviewExportPangingModel
    {
        public string IdDirectory { get; set; }
        public List<string> ListCode { get; set; }

        public string Interval { get; set; }
        public string Sorting { get; set; }
        public int SkipCount { get; set; }
        public int MaxResultCount { get; set; }

        public List<FilterProperties> Filters { get; set; }
    }

    public class Items
    {
        public int totalCount { get; set; }
        public object items { get; set; }
    }

    public class ImageLinkUrl
    {
        public string id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }

    public class ItemValueProperties
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("checked")]
        public bool Checked { get; set; }

        [JsonProperty("action")]
        public string Aciton { get; set; }
    }
}
