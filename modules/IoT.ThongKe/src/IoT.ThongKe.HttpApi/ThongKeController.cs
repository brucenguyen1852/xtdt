﻿using IoT.ThongKe.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace IoT.ThongKe
{
    public abstract class ThongKeController : AbpController
    {
        protected ThongKeController()
        {
            LocalizationResource = typeof(ThongKeResource);
        }
    }
}
