﻿using Localization.Resources.AbpUi;
using IoT.ThongKe.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace IoT.ThongKe
{
    [DependsOn(
        typeof(ThongKeApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class ThongKeHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(ThongKeHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<ThongKeResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
