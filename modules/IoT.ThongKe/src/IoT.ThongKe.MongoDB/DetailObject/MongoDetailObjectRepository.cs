﻿using IoT.ThongKe.ManagentData;
using IoT.ThongKe.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace IoT.ThongKe.DetailObject
{
    public class MongoDetailObjectRepository : MongoDbRepository<ThongKeMongoDbContext, DetailObject, Guid>, IDetailObjectRepository
    {
        public MongoDetailObjectRepository(IMongoDbContextProvider<ThongKeMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public async Task<List<DetailObject>> GetListAsync(string IdMainObject = null, List<PropertiesObject> listProperties = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetMongoQueryableAsync()));
            var list = query.ToList();
            return list;
        }

        protected virtual IQueryable<DetailObject> ApplyFilter(
            IQueryable<DetailObject> query
            )
        {
            return query;
        }

    }
}
