﻿using IoT.ThongKe.Common;
using IoT.ThongKe.ManagentData;
using IoT.ThongKe.MongoDB;
using IoT.ThongKe.Object;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace IoT.ThongKe.MainObject
{
    public class MongoMainObjectRepository : MongoDbRepository<ThongKeMongoDbContext, Object.MainObject, Guid>, IMainObjectRepository
    {
        public MongoMainObjectRepository(IMongoDbContextProvider<ThongKeMongoDbContext> dbContextProvider)
            :base(dbContextProvider)
        {

        }

        public async Task<List<Object.MainObject>> GetAll(string filterText = null,
            string idDirectory = null,
            string nameObject = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        )
        {
            var query = ApplyFilter((await GetMongoQueryableAsync(cancellationToken)), filterText, idDirectory, nameObject);
            var result = query.ToList();
            if (!string.IsNullOrWhiteSpace(nameObject))
            {
                result = result.Where(e => RemoveUnicode(e.NameObject.ToLower()).Contains(RemoveUnicode(nameObject.ToLower()))).ToList();
            }
            return result.Skip(skipCount).Take(maxResultCount).ToList();
        }

        public async Task<long> GetCountAsync(string fillerText = null, 
            string idDirectory = null, 
            string nameObject = null, 
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetMongoQueryableAsync(cancellationToken)), fillerText, idDirectory, nameObject);
            return await query.As<IMongoQueryable<Object.MainObject>>().LongCountAsync(GetCancellationToken(cancellationToken));
        }
        public async Task<List<Object.MainObject>> GetListAsync(string fillerText = null, 
            string idDirectory = null, 
            string nameObject = null, 
            string sorting = null, 
            int maxResultCount = int.MaxValue, 
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetMongoQueryableAsync(cancellationToken)), fillerText, idDirectory, nameObject);
            var list = query.ToList();
            if (!string.IsNullOrWhiteSpace(nameObject))
            {
                list = list.Where(e => RemoveUnicode(e.NameObject.ToLower()).Contains(RemoveUnicode(nameObject.ToLower()))).ToList();
            }
            return list.Skip(skipCount).Take(maxResultCount).ToList();
        }

        protected virtual IQueryable<Object.MainObject> ApplyFilter(
            IQueryable<Object.MainObject> query,
            string fillerText = null, 
            string idDirectory = null, 
            string nameObject = null,
            Geometry geometry = null, 
            bool isCheckImplementProperties = false, 
            PropertiesGeojson propertiesGeojson = null,
            Object3D object3D = null,
            string qrCodeObject = null
            )
        {
            return query
                //.WhereIf(!string.IsNullOrWhiteSpace(name), e => e.Name.Trim().ToLower().Contains(name.Trim().ToLower()))
                .WhereIf(!string.IsNullOrWhiteSpace(nameObject), e => e.NameObject.Contains(nameObject));
        }


        private string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                                    "đ",
                                    "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
                                    "í","ì","ỉ","ĩ","ị",
                                    "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
                                    "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
                                    "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
                                    "d",
                                    "e","e","e","e","e","e","e","e","e","e","e",
                                    "i","i","i","i","i",
                                    "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
                                    "u","u","u","u","u","u","u","u","u","u","u",
                                    "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }
    }
}
