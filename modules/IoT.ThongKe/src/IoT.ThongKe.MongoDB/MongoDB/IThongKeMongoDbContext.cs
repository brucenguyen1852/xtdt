﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.ThongKe.MongoDB
{
    [ConnectionStringName(ThongKeDbProperties.ConnectionStringName)]
    public interface IThongKeMongoDbContext : IAbpMongoDbContext
    {
        /* Define mongo collections here. Example:
         * IMongoCollection<Question> Questions { get; }
         */
    }
}
