﻿using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace IoT.ThongKe.MongoDB
{
    [ConnectionStringName(ThongKeDbProperties.ConnectionStringName)]
    public class ThongKeMongoDbContext : AbpMongoDbContext, IThongKeMongoDbContext
    {
        /* Add mongo collections here. Example:
         * public IMongoCollection<Question> Questions => Collection<Question>();
         */
        public IMongoCollection<IoT.ThongKe.Demo.Demo> Demos => Collection<IoT.ThongKe.Demo.Demo>();
        public IMongoCollection<IoT.ThongKe.Object.MainObject> MainObjects => Collection<IoT.ThongKe.Object.MainObject>();
        public IMongoCollection<IoT.ThongKe.Layer.Directory> Directorys => Collection<IoT.ThongKe.Layer.Directory>();
        public IMongoCollection<IoT.ThongKe.Layer.PropertiesDirectory> PropertiesDirectorys => Collection<IoT.ThongKe.Layer.PropertiesDirectory>();
        public IMongoCollection<IoT.ThongKe.DetailObject.DetailObject> DetailObjects => Collection<IoT.ThongKe.DetailObject.DetailObject>();
        public IMongoCollection<IoT.ThongKe.DocumentObjectStatic.DocumentObject> DocumentObjects => Collection<IoT.ThongKe.DocumentObjectStatic.DocumentObject>();

        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigureThongKe();
        }
    }
}