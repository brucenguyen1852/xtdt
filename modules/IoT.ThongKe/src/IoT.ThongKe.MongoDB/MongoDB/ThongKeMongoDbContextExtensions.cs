﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace IoT.ThongKe.MongoDB
{
    public static class ThongKeMongoDbContextExtensions
    {
        public static void ConfigureThongKe(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new ThongKeMongoModelBuilderConfigurationOptions(
                ThongKeDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}