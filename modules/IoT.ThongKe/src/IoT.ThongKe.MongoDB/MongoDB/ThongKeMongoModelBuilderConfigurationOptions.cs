﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace IoT.ThongKe.MongoDB
{
    public class ThongKeMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public ThongKeMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}