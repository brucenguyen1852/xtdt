﻿using IoT.HTKT.Permissions;
using IoT.ThongKe.Localization;
using IoT.ThongKe.Permissions;
using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace IoT.ThongKe.Web.Menus
{
    public class ThongKeMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            //Add main menu items.
            //context.Menu.AddItem(new ApplicationMenuItem(ThongKeMenus.Prefix, displayName: "ThongKe", "~/ThongKe", icon: "fa fa-globe"));
            var l = context.GetLocalizer<ThongKeResource>();
            var identityMenuItem = new ApplicationMenuItem(ThongKeMenus.Prefix, l["Menu:ThongKe"], icon: "fa fa-bar-chart");
            identityMenuItem.AddItem(new ApplicationMenuItem(ThongKeMenus.ChartStatistic, l["Menu:ChartStatistic"], url: "~/ThongKe/ChartStatistic"));
            identityMenuItem.AddItem(new ApplicationMenuItem(ThongKeMenus.TableStatistic, l["Menu:TableStatistic"], url: "~/ThongKe/BangBieu"));
<<<<<<< Updated upstream
            identityMenuItem.AddItem(new ApplicationMenuItem(ThongKeMenus.TableStatistic, l["Menu:DetailMainObject"], url: "~/ThongKe/DetailMainObject"));

=======
            identityMenuItem.AddItem(new ApplicationMenuItem(ThongKeMenus.ThongKeDuLieu, l["Menu:ThongKeDuLieu"], url: "~/ThongKe/ThongKeDuLieu"));
>>>>>>> Stashed changes
            identityMenuItem.Order = 3;
            if (await context.IsGrantedAsync(HTKTPermissions.Report.Default))
            {
                context.Menu.AddItem(identityMenuItem);
            }
            //return Task.CompletedTask;
        }
    }
}