﻿var listFolder, json, item;
var l = abp.localization.getResource('ThongKe');
var TreeViewLayer = {
    GLOBAL: {
        IdDirectory: "",
        ArrayTree: [],
        objectClicked: null
    },
    CONSTS: {
        URL_GETLISTDIRECTORY: "/api/htkt/directory/get-list-new-layer",
        URL_GET_INFO_DIRECTORY: "/api/HTKT/PropertiesDirectory/get-directoryById"
    },
    SELECTORS: {
        tree: "#checkTree",
        listGroupItem: ".list-group-item",
        table_propertis: '.table-properties',
        headingOne: ".panel-heading a",

        // Gentree
        tree_i_o: ".tree-o-i",
        item_tree_i_o: ".item-li",
        searchTree: ".search-tree",
    },
    init: function () {
        TreeViewLayer.setEvent();
        TreeViewLayer.GetListLayer();
    },
    setEvent: function () {
        $(TreeViewLayer.SELECTORS.headingOne).on("click", function () {
            var name = $(this).attr('data-name');
            if ($(this).hasClass("open")) {
                $(this).removeClass("open");
                $(this).html(`<img src="${location.origin}/images/Icon-Minus.svg" /> ${name}`);
                $(this).parent().parent().find(".panel-collapse").addClass("in").addClass('show');
            } else {
                $(this).addClass("open");
                $(this).html(`<img src="${location.origin}/images/Icon-Plus.svg" /> ${name}`);
                $(this).parent().parent().find(".panel-collapse").removeClass("in").removeClass("show");
            }
        });

        $(TreeViewLayer.SELECTORS.tree_i_o).on('click', TreeViewLayer.SELECTORS.item_tree_i_o, function () {
            var id = $(this).attr('data-id');
            var folder = $(this).attr('data-type');

            $(TreeViewLayer.SELECTORS.item_tree_i_o).removeClass('active');
            $(this).addClass('active');

            if (!$(this).next().hasClass('open')) {
                $(this).next().addClass('open');
                $(this).next().slideToggle("slow");

                var element = $(this).find('.icon-menu-tree');
                if (element.length > 0) {
                    element.addClass('open')
                }
            }
            else {
                $(this).next().slideToggle("slow");
                $(this).next().removeClass('open');
                var element = $(this).find('.icon-menu-tree');
                if (element.length > 0) {
                    element.removeClass('open')
                }
            }

            if (folder != "true") {
                if (TreeViewLayer.GLOBAL.IdDirectory != id) {
                    Export.Reset();
                }
            }

            TreeViewLayer.GLOBAL.IdDirectory = '';
            $('#' + id).css('background-color', '#dee2e6');
            model = listFolder.find(x => x.id == id);
            if (model != null) {
                thisId = model.id;
                parentId = model.id;
                level = model.level + 1;
            }

            if (folder != "true") {
                $(Export.SELECTORS.btn_export).prop('disabled', false);
                $(Export.SELECTORS.btn_review).prop('disabled', false);
                $(Export.SELECTORS.btn_check_all).prop('disabled', false);
                TreeViewLayer.GLOBAL.IdDirectory = id;
                $.ajax({
                    type: "GET",
                    url: TreeViewLayer.CONSTS.URL_GET_INFO_DIRECTORY,
                    data: {
                        id: id
                    },
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.code == "ok") {
                            //console.log(data);
                            TreeViewLayer.GLOBAL.objectClicked = data.result;
                            var result = [];
                            var tr = ``;
                            var index = 0

                            $.each(data.result.listProperties, function (i, obj) {
                                if (obj.typeSystem != 1) {
                                    var element = {
                                        NameProperties: obj.nameProperties,
                                        CodeProperties: obj.codeProperties,
                                        TypeProperties: obj.typeProperties,
                                        IsShow: obj.isShow,
                                        IsIndexing: obj.isIndexing,
                                        IsRequest: obj.isRequest,
                                        IsView: obj.isView,
                                        IsHistory: obj.isHistory,
                                        IsInheritance: obj.isInheritance,
                                        IsAsync: obj.isAsync,
                                        DefalutValue: obj.defalutValue,
                                        ShortDescription: obj.shortDescription,
                                        TypeSystem: obj.typeSystem,
                                        OrderProperties: obj.orderProperties
                                    };

                                    //if (obj.typeProperties != "geojson") {
                                    result.push(element);
                                    tr += `
                                    <tr>
                                        <td style="text-align:center; width: 50px;">${index + 1}</td>
                                        <td style="text-align:center; width: 70px;" class="custom-checkbox"><input class="check-properties custom-control-input" data-type="${obj.typeProperties}" data-name="${obj.nameProperties}" data-code="${obj.codeProperties}" type="checkbox" id="${obj.codeProperties}-${index}" /><label class="custom-control-label" for="${obj.codeProperties}-${index}"></label></td>
                                        <td>${obj.nameProperties}</td>
                                        <td>${obj.codeProperties}</td>
                                        <td><input type="${obj.typeProperties == "int" || obj.typeProperties == "float" ? "number" : "text"}" 
                                            data-code="${obj.codeProperties}" data-type="${obj.typeProperties}" class="input-filter form-control" id="filter-${obj.codeProperties}" disabled /></td>
                                    </tr>
                                `;

                                    index++;
                                    $(Export.SELECTORS.btn_check_all).attr('data-all', index);
                                    //}
                                }
                            });

                            $(`${TreeViewLayer.SELECTORS.table_propertis} tbody`).html(tr);
                        }
                        else {
                            console.log(data)
                        }
                    },
                    error: function () {
                        alert("Lỗi ajax");
                    }
                });
            }
            else {
                $(`${TreeViewLayer.SELECTORS.table_propertis} tbody`).html('');
            }
        });

        $(TreeViewLayer.SELECTORS.searchTree).on('keyup', delay(function () {
            $(TreeViewLayer.SELECTORS.tree_i_o + ' .treeview').hide();
            var value = $(this).val().toLowerCase().trim();
            if (value != "") {
                $(TreeViewLayer.SELECTORS.tree_i_o + ' .treeview').each(function () {
                    var name = $(this).attr('data-search');
                    var parent = $(this).attr('data-parent');
                    var level = parseInt($(this).attr('data-level'));

                    if (level == 0) {
                        $(this).show();
                    }
                    else {
                        if (name.toLowerCase().includes(value)) {
                            $(this).show();
                            if (parent != "null") {
                                $('#tree-item-' + parent).show();
                                $('#tree-ul-' + parent).slideDown();
                            }
                        }
                    }
                })
            }
            else {
                $(TreeViewLayer.SELECTORS.tree_i_o + ' .treeview').show();
            }
        }, 500));

        $('.table-properties').on('click', '.checkboxmark', function () {
            if (!$(this).hasClass('check')) {
                $(this).addClass('check');
                $(this).parent().find("input").trigger('click');
            } else {
                $(this).removeClass('check');
                $(this).parent().find("input").trigger('click');
            }
        });

        $('#checkbox-all').on('click', '.checkboxmark-all', function () {
            if (!$(this).hasClass('check')) {
                $(this).addClass('check');
                $(this).parent().find("input").trigger('click');
            } else {
                $(this).removeClass('check');
                $(this).parent().find("input").trigger('click');
            }
        });
    },
    GetListLayer: function () {
        $.ajax({
            type: "GET",
            url: TreeViewLayer.CONSTS.URL_GETLISTDIRECTORY,
            data: {
            },
            async: true,
            beforeSend: function () {
                $('.spinner').css('display', 'block');
            },
            contentType: "application/json; charset=utf-8",
            complete: function () {
                $('.spinner').css('display', 'none');
            },
            success: function (res) {
                if (res.code == "ok")
                {
                    var data = res.result;
                    listFolder = data;
                    TreeViewLayer.GLOBAL.ArrayTree = data;
                    var parent = data.find(x => x.level == "0");
                    var treedynamic = TreeViewLayer.FillDynaTree(parent);
                    var arrayTree = [];
                    arrayTree.push(treedynamic);

                    var html = ``;

                    for (var i = 0; i < arrayTree.length; i++) {
                        var name = arrayTree[i].title;
                        var icon = `<img crossorigin="anonymous" src="${arrayTree[i].icon}" alt="" />`;
                        if (arrayTree[i].icon == "" || arrayTree[i].icon == null) {
                            icon = `<i class="fa ${arrayTree[i].isFolder ? "fa-folder-open" : "fa-file"} text-brand"></i>`;
                        }

                        if (arrayTree[i].children.length > 0) {
                            html += `<li data-level="${arrayTree[i].level}" id="tree-item-${arrayTree[i].key}" class="treeview" style="height: auto;" data-search="${name}" data-parent="${arrayTree[i].parentId}">
                                                                                    <a href="javascript:;" class="item-li" data-type="${arrayTree[i].isFolder}" data-chil="${arrayTree[i].children.length}" data-id="${arrayTree[i].key}">
                                                                                        <span class="pull-left-container">
                                                                                            <i class="fa fa-angle-right pull-left icon-menu-tree open"></i>
                                                                                        </span>
                                                                                        <span class="title-item" style= "padding-left:3rem;">${icon} ${name} (${arrayTree[i].children.length})</span>

                                                                                        <ul class="tree-ul open" id="tree-ul-${arrayTree[i].key}" data-id="${arrayTree[i].key}" style="display:block;">`;

                            html += TreeViewLayer.DataTreeGen(arrayTree[i]);

                            html += `
                                                                                        </ul>
                                                                                    </a>
                                                                                </li>`;
                        }
                        else {
                            html += `<li data-level="${arrayTree[i].level}" id="tree-item-${arrayTree[i].key}" class="treeview" style="height: auto;" data-search="${name}"  data-parent="${arrayTree[i].parentId}">
                                                                    <a href="javascript:;" class="item-li" data-type="${arrayTree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${arrayTree[i].key}">
                                                                    <span class="title-item" style= "padding-left:3rem;"}">${icon} ${name}</span></a>
                                                                </li>`;
                        }
                    }

                    //var html = TreeViewLayer.DataTreeGen(treedynamic);

                    $(`.tree-o-i`).html(html);

                    $(`.item-li`).each(function () {
                        if ($(this).find('.title-item').text() == "") {
                            $(this).remove();
                        }
                    });
                }
                else {
                    console.log(res);
                }
            },
            error: function () {
                alert(l("BangBieu:AjaxError"));
            }
        });
    },
    FillDynaTree: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            level: data.level,
            icon: null,
            parentId: data.parentId
        };

        var parent = TreeViewLayer.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    obj.icon = data.image2D;
                }
            }

            obj.key = parent.id;

            var lstChil = TreeViewLayer.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = TreeViewLayer.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    DataTreeGen: function (data) {
        var tree = data.children;

        var html = ``;

        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;
            if (tree[i].icon == "" || tree[i].icon == null) {
                icon = `<i class="fa ${tree[i].isFolder ? "fa-folder-open" : "fa-file"} text-brand"></i>`;
            }

            if (tree[i].children.length > 0) {
                html += `<li data-level="${tree[i].level}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto; " data-search="${name}" data-parent="${tree[i].parentId}">
                                                                                            <a href="javascript:;" id="item-li-${tree[i].key}" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                                                                                <span class="pull-left-container">
                                                                                                    <i class="fa fa-angle-right pull-left icon-menu-tree ${tree[i].level > 1 ? "open" : ""}" id="arrow-item-${tree[i].key}"></i>
                                                                                                </span>
                                                                                                <span class="title-item" style= "padding-left:3rem;">${icon} ${name} (${tree[i].children.length})</span>

                                                                                                <ul class="tree-ul ${tree[i].level > 1 ? "open" : ""}" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:${tree[i].level == 1 ? "none" : "block"};">`;

                html += TreeViewLayer.DataTreeGen(tree[i]);

                html += `
                                                                                                </ul>
                                                                                            </a>
                                                                                        </li>`;
            }
            else {
                html += `<li data-level="${tree[i].level}" id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${name}"  data-parent="${tree[i].parentId}">
                                                                                                <a href="javascript:;" id="item-li-${tree[i].key}" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                                                                                <span class="title-item" style= "padding-left:3rem;"}">${icon} ${name}</span></a>
                                                                                           </li>`;
            }
        }

        return html;
    },
}


function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}