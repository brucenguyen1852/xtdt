﻿var l = abp.localization.getResource('ThongKe');
var tableConfig = {
    GLOBAL: {
        table: [],
        select: [],
        dataName: '',
        dataCode: '',
        dataVersion: 0,
        idDirectory: '',
        listTypeList: [],
        previousSelect: '',
        rowSelected: null
    },
    CONSTS: {
        URL_AJAXSAVE: '/api/HTKT/PropertiesDirectoryActivity/create',
        URL_AJAXGETDEFAULTDATA: '/api/HTKT/PropertiesDirectoryActivity/get-directoryById',
        URL_AJAXGETDEFAULTDATAFIRST: '/api/HTKT/PropertiesDefault/get-list'
    },
    SELECTORS: {
        tbodySystem: ".table-properties-system tbody",
        tbody: ".table-properties tbody",
        tabletrtd: ".table-properties tr td",
        tabletr: ".table-properties tr",
        textinsert: "input.textinsert",
        eidiinput: ".editinput",
        editselect: ".editselect",
        addRow: ".AddRow",
        headingOne: ".panel-heading a",
        // headingTwo: "#headingTwo a",
        btnSave: '.SaveProperties',
        dataName: '#DataName',
        dataCode: '#DataCode',
        version: '#Version',
        select_lop_du_lieu: ".danh-sach-lop-du-lieu",
        collapseOne: "#collapseOne",
        iconHeadingOne: "#headingOne a i",
        collapseTwo: "#collapseTwo",
        iconHeadingTwo: "#headingTwo a i",
        modalList: ".exampleModal-list"
    },
    init: function () {
        tableConfig.addTbodyTr();
        //tableConfig.setDatatable();
        tableConfig.setUpEvent();
        tableConfig.setUpEventModalList();
        var url = new URL(window.location.href);
        var checkid = url.searchParams.get("id");
        if (checkid !== null) TreeView.ReloadTree(checkid);

        //this.SelectedLayer();
    },
    //add data vào table
    addTbodyTr: function () {
        $(tableConfig.SELECTORS.tbody).children().remove();
        $(tableConfig.SELECTORS.tbodySystem).children().remove();
        let hproperties = "";
        let hsystem = "";
        $.each(tableConfig.GLOBAL.table, function (i, obj) {
            if (obj.TypeSystem == 3 || obj.TypeSystem == 2) {
                let html = `<tr data-count="${i}">
                                <th style="width:2%;text-align: center;">${(i + 1)}</th>
                                <td style="width:10%" data-code="NameProperties"><div class="editinput">${obj.NameProperties}</div></td>
                                <td style="width:10%" data-code="CodeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : ""}">${obj.CodeProperties}</div></td>
                                <td style="width:10%" data-code="TypeProperties"><div class="${obj.TypeSystem !== 2 ? "editinput" : ""}">${(tableConfig.GLOBAL.select.find(x => x.values == obj.TypeProperties).text)}</div></td>
                                <td style="width:2%" data-code="IsShow"><input type="checkbox" class="editcheckbox" ${(obj.IsShow ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsIndexing"><input type="checkbox" class="editcheckbox" ${(obj.IsIndexing ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsRequest"><input type="checkbox" class="editcheckbox" ${(obj.IsRequest ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsView"><input type="checkbox" class="editcheckbox" ${(obj.IsView ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsHistory"><input  type="checkbox" class="editcheckbox" ${(obj.IsHistory ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsInheritance"><input  type="checkbox" class="editcheckbox" ${(obj.IsInheritance ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsAsync"><input type="checkbox" class="editcheckbox" ${(obj.IsAsync ? "checked" : "")}></td>
                                <td style="width:10%" class="${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "unselectable" : ""}" data-code="DefalutValue"><div class="editinput">${(obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") ? "" : obj.DefalutValue}</div></td>
                                <td style="width:10%" data-code="ShortDescription"><div class="editinput">${obj.ShortDescription}</div></td>
                                ${obj.TypeSystem == 2 ? '<td style="width: 5%"></td>' : tableConfig.showButtonRow(obj, i, tableConfig.GLOBAL.table.length)}
                            </tr>`;
                hproperties = hproperties + html;
            }
            if (obj.TypeSystem == 1) {
                let html = `<tr class="notedit" data-count="${i}">
                                <th style="width:2%;text-align: center;">${(i + 1)}</th>
                                <td style="width:10%" data-code="NameProperties"><div>${obj.NameProperties}</div></td>
                                <td style="width:10%" data-code="CodeProperties"><div>${obj.CodeProperties}</div></td>
                                <td style="width:10%" data-code="TypeProperties"><div>${(tableConfig.GLOBAL.select.find(x => x.values == obj.TypeProperties).text)}</div></td>
                                <td style="width:2%" data-code="IsShow"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsShow ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsIndexing"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsIndexing ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsRequest"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsRequest ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsView"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsView ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsHistory"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsHistory ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsInheritance"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsInheritance ? "checked" : "")}></td>
                                <td style="width:2%" data-code="IsAsync"><input disabled type="checkbox" class="editcheckbox" ${(obj.IsAsync ? "checked" : "")}></td>
                                <td style="width:10%" data-code="DefalutValue"><div>${obj.DefalutValue}</div></td>
                                <td style="width:10%" data-code="ShortDescription"><div >${obj.ShortDescription}</div></td>
                                <td style="width: 5%"></td>
                            </tr>`;
                hsystem = hsystem + html;
            }
        });
        $(tableConfig.SELECTORS.tbody).append(hproperties);
        $(tableConfig.SELECTORS.tbodySystem).append(hsystem);
    },
    setUpEvent: function () {
        tableConfig.setEventAffter();
        $(tableConfig.SELECTORS.addRow).on("click", function () {
            let count = (tableConfig.GLOBAL.table.length + 1);
            var data = {
                NameProperties: "",
                CodeProperties: "",
                TypeProperties: "string",
                IsShow: true,
                IsIndexing: true,
                IsRequest: true,
                IsView: true,
                IsHistory: true,
                IsInheritance: true,
                IsAsync: true,
                DefalutValue: "",
                ShortDescription: "",
                TypeSystem: 3,
                OrderProperties: count,
            }
            tableConfig.GLOBAL.table.push(data);
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        });
        $(tableConfig.SELECTORS.headingOne).on("click", function () {
            if ($(this).find("i").hasClass("fa-plus")) {
                $(this).find("i").removeClass("fa-plus");
                $(this).find("i").addClass("fa-minus");
                $(this).parent().parent().find(".panel-collapse").addClass("in");
            } else {
                $(this).find("i").removeClass("fa-minus");
                $(this).find("i").addClass("fa-plus");
                $(this).parent().parent().find(".panel-collapse").removeClass("in").removeClass("show");
            }
        });

        $(tableConfig.SELECTORS.btnSave).on('click', function (e) {
            if (tableConfig.checkFormValidate()) {
                if (tableConfig.GLOBAL.idDirectory != "") {
                    var object = {
                        IdDirectoryActivity: tableConfig.GLOBAL.idDirectory,
                        NameTypeDirectory: $(tableConfig.SELECTORS.dataName).val(),
                        CodeTypeDirectory: $(tableConfig.SELECTORS.dataCode).val(),
                        VerssionTypeDirectory: parseFloat($(tableConfig.SELECTORS.version).val()),
                        IdImplement: "",
                        ListProperties: tableConfig.GLOBAL.table
                    };

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        contentType: "application/json-patch+json",
                        async: false,
                        url: tableConfig.CONSTS.URL_AJAXSAVE,
                        data: JSON.stringify(object),
                        success: function (data) {
                            if (data.code == "ok") {
                                $.ajax({
                                    type: "GET",
                                    contentType: "application/json-patch+json",
                                    async: false,
                                    url: "/api/htkt/directoryactivity/update-name",
                                    data: {
                                        id: object.IdDirectoryActivity,
                                        name: object.NameTypeDirectory,
                                        idDirectory: $(tableConfig.SELECTORS.select_lop_du_lieu).val()
                                    },
                                    success: function (res) {
                                        if (res.code == "ok" && res.result) {
                                            swal({
                                                title: l('Chart:Notification'),
                                                text: l('Chart:UpdateDataSuccessfully'),
                                                icon: "success",
                                                button: l('Chart:Close'),
                                            }).then((value) => {
                                                location.href = "/HTKT/Activity?id=" + tableConfig.GLOBAL.idDirectory;
                                                return;
                                                //TreeView.ReloadTree(tableConfig.GLOBAL.idDirectory);
                                            });
                                        }
                                        else {
                                            console.log(res);
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.error(jqXHR + ":" + errorThrown);
                                    }
                                });

                            } else {
                                swal({
                                    title: l('Chart:Notification'),
                                    text: l('Chart:UpdateDataFail'),
                                    icon: "error",
                                    button: l('Chart:Close'),
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR + ":" + errorThrown);
                            ViewMap.showLoading(false);
                        }
                    });
                }
            }
        });

        $(document).on('click', '.sidebar-toggle', function () {
            setTimeout(function () {
                TreeView.initSplitter();
            }, 300);
        });

        $('#div2').on('keyup', 'input[type=text]', function () {

            if ($(this).val() != "") {
                tableConfig.clearLabelError($(this).parent());
            }
            else {
                insertError($(this), "other");
                check = false;
                TreeView.showLabelError(this, l('Chart:DataCannotBeEmptyAndValid'));
            }
        });

        $('#div2').on('change', 'select', function () {

            if ($(this).val() != "") {
                tableConfig.clearLabelError($(this).parent());
            }
            else {
                //insertError($(this), "other");
                TreeView.showLabelError(this, "Dữ liệu không được để trống và phải hợp lệ");
            }
        })
    },
    clearText: function () {
        $(tableConfig.SELECTORS.eidiinput).show();
        $(tableConfig.SELECTORS.textinsert).remove();
        $(tableConfig.SELECTORS.editselect).remove();
    },
    //get or set data list table
    getOrSetData: function (row, cell, check, val) {
        if (check) {//set
            tableConfig.GLOBAL.table[row][cell] = val;
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        } else {//get
            return tableConfig.GLOBAL.table[row][cell];
        }
    },
    //sort list with order
    sortListOrder: function () {
        tableConfig.GLOBAL.table.sort(function (a, b) {
            return parseInt(a.OrderProperties) - parseInt(b.OrderProperties);
        });
    },
    //event reset sau khi add data table
    setEventAffter: function () {
        $(tableConfig.SELECTORS.tabletrtd).on("click", 'div.editinput', function () {
            if (!$(this).parent().parent().hasClass("notedit") && !$(this).parent().hasClass("unselectable")) {
                tableConfig.clearText();
                $(this).hide();
                let code = $(this).parent().attr("data-code");
                let row = Number($(this).parents("tr").attr("data-count"));
                let val = tableConfig.getOrSetData(row, code, false, "");
                if (code == "TypeProperties") {
                    let html = "<select class='form-control editselect'>";
                    $.each(tableConfig.GLOBAL.select, function (i, obj) {
                        html += "<option value=" + obj.values + " " + (obj.values == val ? "selected" : "") + ">" + obj.text + "</option>";
                    });
                    html += "</select>";
                    $(this).parent().append(html);
                } else if (code == "CodeProperties") {
                    $(this).parent().append('<input type="text" class="form-control textinsert inputCodeProperties" value="' + val + '">');
                }
                else {
                    $(this).parent().append('<input type="text" class="form-control textinsert" value="' + val + '">');
                }
            }
        });
        $(tableConfig.SELECTORS.tabletrtd).on("focusout", ".textinsert", function () {
            var data = $(this).val();
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));//$(this).parent().parent().index();
            tableConfig.getOrSetData(row, code, true, data);
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".editcheckbox", function () {
            let data = $(this).is(":checked");
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));
            tableConfig.getOrSetData(row, code, true, data);
        });
        $(tableConfig.SELECTORS.tabletrtd).on("change", ".editselect", function () {
            let data = $(this).find(":selected").val();
            let code = $(this).parent().attr("data-code");
            let row = Number($(this).parents("tr").attr("data-count"));
            tableConfig.getOrSetData(row, code, true, data);
            let td = $('tr[data-count="' + row + '"]')[0].querySelectorAll("td");
            let tdaction = td[td.length - 1];
            if ($(this).val() == "list" || $(this).val() == "checkbox" || $(this).val() == "radiobutton") {
                tableConfig.GLOBAL.listTypeList = [];
                tableConfig.GLOBAL.rowSelected = tableConfig.GLOBAL.table[row];
                $(tableConfig.SELECTORS.modalList).modal('show');
                $(td[td.length - 3]).addClass("unselectable");
                //$(tdaction).append(`<button type="button" class="btn btn-info showList" style="padding:3px 5px;font-size:13px; margin-left: 2px;"> <i class="fa fa fa-list" aria-hidden="true"></i></button>`);
            } else {
                if (tableConfig.GLOBAL.previousSelect == "list" || tableConfig.GLOBAL.previousSelect == "checkbox" || tableConfig.GLOBAL.previousSelect == "radiobutton") {
                    tableConfig.getOrSetData(row, "DefalutValue", true, "");
                    $(tdaction).children(".showList").remove();
                    $(td[td.length - 3]).removeClass("unselectable");
                }
            }
        });
        $(tableConfig.SELECTORS.tabletrtd).on("focus", ".editselect", function () {
            tableConfig.GLOBAL.previousSelect = '';
            tableConfig.GLOBAL.previousSelect = this.value;
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".deleteRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            tableConfig.GLOBAL.table.splice(row, 1);
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".downRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            var objfind = tableConfig.GLOBAL.table[row];
            var objfindNext = tableConfig.GLOBAL.table[row + 1];
            if (objfind.OrderProperties < tableConfig.GLOBAL.table.length && typeof objfindNext !== "undefined"
                && objfindNext !== null && objfindNext.TypeSystem == 3) {
                var orderCurrent = objfind.OrderProperties;
                tableConfig.GLOBAL.table[row].OrderProperties = objfindNext.OrderProperties;
                tableConfig.GLOBAL.table[row + 1].OrderProperties = orderCurrent;
            }
            tableConfig.sortListOrder();
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        });
        $(tableConfig.SELECTORS.tabletrtd).on("click", ".upRow", function () {
            let row = Number($(this).parents("tr").attr("data-count"));
            var objfind = tableConfig.GLOBAL.table[row];
            var objfindBack = tableConfig.GLOBAL.table[row - 1];
            if (objfind.OrderProperties > 0 && objfind.OrderProperties <= tableConfig.GLOBAL.table.length
                && typeof objfindBack !== "undefined" && objfindBack !== null && objfindBack.TypeSystem == 3) {
                var orderCurrent = objfind.OrderProperties;
                tableConfig.GLOBAL.table[row].OrderProperties = objfindBack.OrderProperties;
                tableConfig.GLOBAL.table[row - 1].OrderProperties = orderCurrent;
            }
            tableConfig.sortListOrder();
            tableConfig.addTbodyTr();
            tableConfig.setEventAffter();
        });

        $(tableConfig.SELECTORS.tabletrtd).on("keyup", ".inputCodeProperties", function () {
            // Our regex
            // a-z => allow all lowercase alphabets
            // A-Z => allow all uppercase alphabets
            // 0-9 => allow all numbers
            // @ => allow @ symbol
            var regex = /^[a-zA-Z0-9_]+$/;
            // This is will test the value against the regex
            // Will return True if regex satisfied
            if (regex.test(this.value) !== true)
                //alert if not true
                //alert("Invalid Input");

                // You can replace the invalid characters by:
                this.value = this.value.replace(/[^a-zA-Z0-9_]+/, '');
        });
    },
    //hiển thị các nút ở hoạt động
    showButtonRow: function (obj, count, length) {
        let html = "";
        if (obj.TypeSystem == 2 || obj.TypeSystem == 1) {
            html += '<td style="width:86px"></td>';
        } else {
            html += '<td style="width:110px;display: flex;flex-direction: row;">';
            html += '<button type = "button" class="btn btn-danger deleteRow" style = "padding:3px 5px;font-size:13px;" > <i class="fa fa-trash" aria-hidden="true"></i></button >';
            html += '<button type = "button" class="btn btn-info downRow" style = "padding:3px 5px;font-size:13px; margin-left: 2px;" > <i class="fa fa-arrow-down" aria-hidden="true"></i></button >';
            html += '<button type = "button" class="btn btn-info upRow" style = "padding:3px 5px;font-size:13px; margin-left: 2px;" > <i class="fa fa-arrow-up" aria-hidden="true"></i></button >';
            if (obj.TypeProperties == "list" || obj.TypeProperties == "checkbox" || obj.TypeProperties == "radiobutton") {
                html += `<button type="button" class="btn btn-info showList" style="padding:3px 5px;font-size:13px; margin-left: 2px;"> <i class="fa fa fa-list" aria-hidden="true"></i></button>`;
            }
            html += '</td > ';
        }
        return html;
    },
    getDefault: function () {
        //$.ajax({
        //    type: "GET",
        //    contentType: "application/json-patch+json",
        //    async: false,
        //    url: tableConfig.CONSTS.URL_AJAXGETDEFAULTDATA + '?id=Chưa có',
        //    data: {
        //    },
        //    success: function (data) {
        //        if (data.code == "ok" && data.result.listProperties != undefined) {
        //            var result = [];
        //            $.each(data.result.listProperties, function (i, obj) {
        //                var element = {
        //                    NameProperties: obj.nameProperties,
        //                    CodeProperties: obj.codeProperties,
        //                    TypeProperties: obj.typeProperties,
        //                    IsShow: obj.isShow,
        //                    IsIndexing: obj.isIndexing,
        //                    IsRequest: obj.isRequest,
        //                    IsView: obj.isView,
        //                    IsHistory: obj.isHistory,
        //                    IsInheritance: obj.isInheritance,
        //                    IsAsync: obj.isAsync,
        //                    DefalutValue: obj.defalutValue,
        //                    ShortDescription: obj.shortDescription,
        //                    TypeSystem: obj.typeSystem,
        //                    OrderProperties: obj.orderProperties
        //                };
        //                result.push(element);
        //            });
        //            tableConfig.GLOBAL.table = result;
        //        } else {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: tableConfig.CONSTS.URL_AJAXGETDEFAULTDATAFIRST,
            data: {
            },
            success: function (data) {
                if (data.code == "ok" && data.result != null) {
                    var result = [];
                    $.each(data.result, function (i, obj) {
                        var element = {
                            NameProperties: obj.nameProperties,
                            CodeProperties: obj.codeProperties,
                            TypeProperties: obj.typeProperties,
                            IsShow: obj.isShow,
                            IsIndexing: obj.isIndexing,
                            IsRequest: obj.isRequest,
                            IsView: obj.isView,
                            IsHistory: obj.isHistory,
                            IsInheritance: obj.isInheritance,
                            IsAsync: obj.isAsync,
                            DefalutValue: obj.defalutValue,
                            ShortDescription: obj.shortDescription,
                            TypeSystem: obj.typeSystem,
                            OrderProperties: obj.orderProperties
                        };
                        result.push(element);
                    });
                    tableConfig.GLOBAL.table = result;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
                ViewMap.showLoading(false);
            }
        });
        //        }
        //    },
        //    error: function (jqXHR, textStatus, errorThrown) {
        //        let messageErorr = AppCommon.getMessageErrorReuqest(jqXHR, errorThrown);
        //        console.log(messageErorr);
        //        ViewMap.showLoading(false);
        //    }
        //});
    },
    resetTable: function () {
        tableConfig.GLOBAL.idDirectory = '';
        tableConfig.GLOBAL.table = [];
        tableConfig.addTbodyTr();
        $('#DataName').val("");
        $('#h5NameData').html("");
        $('#DataCode').val("");
        $('#Version').val(0);
        $(tableConfig.SELECTORS.select_lop_du_lieu).val("").trigger("change.select2");
    },
    setUpEventModalList: function () {
        $('#codeList').on("keyup", function () {
            // Our regex
            // a-z => allow all lowercase alphabets
            // A-Z => allow all uppercase alphabets
            // 0-9 => allow all numbers
            // @ => allow @ symbol
            var regex = /^[a-zA-Z0-9_]+$/;
            // This is will test the value against the regex
            // Will return True if regex satisfied
            if (regex.test(this.value) !== true)
                //alert if not true
                //alert("Invalid Input");

                // You can replace the invalid characters by:
                this.value = this.value.replace(/[^a-zA-Z0-9_]+/, '');
        });

        $('#nameList').on("keyup", function () {
            var value = this.value.toUpperCase();
            var regex = /^[a-zA-Z0-9_]+$/;
            ""
            if (regex.test(this.value) !== true) {
                value = this.value.replace(/ /g, ""); // xóa khoảng trắng
                this.value = this.value.replace(/[~`!@#$%_\^&*()+=\-\[\]\\';,/{}_|\\":<>_\?]/g, ''); // xóa ký tự đặc biệt
                value = removeVietnameseTones(value).toUpperCase(); // xóa dấu
            }

            $('#codeList').val(value);

        });

        $('#btnSaveList').on('click', function () {
            if ($('#codeList').val().trim() !== "" && $('#nameList').val().trim() !== "") {
                let check = tableConfig.GLOBAL.listTypeList.find(x => x.code === $('#codeList').val().trim());
                if (check != null & check != undefined) {
                    swal({
                        title: "Thông báo",
                        text: "Mã thuộc tính này đã tồn tại!",
                        icon: "error",
                        button: "Đóng",
                    });
                } else {
                    if ($('#idList').val().trim() === "") {
                        let id = tableConfig.uuidv4();
                        let object = {
                            id: id,
                            name: $('#nameList').val().trim(),
                            code: $('#codeList').val().trim(),
                            checked: false,
                            action: tableConfig.getActionList(id)
                        };

                        tableConfig.GLOBAL.listTypeList.push(object);
                    } else {
                        let object = tableConfig.GLOBAL.listTypeList.find(x => x.id === $('#idList').val().trim());
                        object.name = $('#nameList').val().trim();
                        object.code = $('#codeList').val().trim();
                    }
                    $('#codeList').val('');
                    $('#nameList').val('');
                    $('#idList').val('');
                    tableConfig.updateTable();
                }
            }
        });

        $(document).on('click', '.btn-deleteTable-list', function () {
            swal({
                title: "Thông báo",
                text: "Bạn có chắc chắn muốn xóa đối tượng này không? ",
                icon: "warning",
                buttons: [
                    'Đóng',
                    'Đồng ý'
                ],
            }).then((value) => {
                if (value) {
                    let id = $(this).attr('delete-for');
                    tableConfig.GLOBAL.listTypeList = JSON.parse(JSON.stringify(tableConfig.GLOBAL.listTypeList.filter(x => x.id !== id)));
                    tableConfig.updateTable();
                }
            });
        });

        $(document).on('click', '.btn-editTable-list', function () {
            let id = $(this).attr('edit-for');
            let object = tableConfig.GLOBAL.listTypeList.find(x => x.id === id);
            $('#codeList').val(object.code);
            $('#nameList').val(object.name);
            $('#idList').val(object.id);
        });

        $(document).on('click', '.showList', function () {
            $('#idList').val('');
            $('#nameList').val('');
            $('#codeList').val('');
            let row = Number($(this).parents("tr").attr("data-count"));
            tableConfig.GLOBAL.rowSelected = tableConfig.GLOBAL.table[row];
            $(tableConfig.SELECTORS.modalList).modal('show');
        });

        $(tableConfig.SELECTORS.modalList).on('show.bs.modal', function () {
            if (tableConfig.GLOBAL.rowSelected != null && tableConfig.GLOBAL.rowSelected.DefalutValue != "" && tableConfig.GLOBAL.rowSelected.DefalutValue != null) {
                tableConfig.GLOBAL.listTypeList = JSON.parse(tableConfig.GLOBAL.rowSelected.DefalutValue);
            }
            tableConfig.updateTable();
        });

        $(tableConfig.SELECTORS.modalList).on('hidden.bs.modal', function () {
            tableConfig.GLOBAL.rowSelected.DefalutValue = JSON.stringify(tableConfig.GLOBAL.listTypeList);
            tableConfig.GLOBAL.listTypeList = [];
        });
    },
    updateTable: function () {
        $('.div-list table tbody').html('');
        var test = '';
        if (tableConfig.GLOBAL.listTypeList.length > 0) {
            var count = 0;
            for (var i = 0; i < tableConfig.GLOBAL.listTypeList.length; i++) {
                //if (tableConfig.GLOBAL.listTypeList[i].status) {
                count++;
                test += `<tr>
                                                                            <th scope="row">${count}</th>
                                                                            <td>${tableConfig.GLOBAL.listTypeList[i].name}</td>
                                                                            <td>${tableConfig.GLOBAL.listTypeList[i].code}</td>
                                                                            <td>${tableConfig.GLOBAL.listTypeList[i].action}</td>
                                                                        </tr>`;
                //}
            }
        }
        if (test != "") {
            $('.div-list table tbody').append(test);
        } else {
            $('.div-list table tbody').append(`<tr>
                                                                                    <th scope="row" colspan="4">Không có dữ liệu</th>
                                                                                </tr>`);
        }

        //text += `</tr>`;
        //$('.div-list table thead').html('');
        //$('.div-list table thead').append(text);
    },
    //new Guid Id
    uuidv4: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    getActionList: function (id) {
        return `<a href="#" title="Xóa" class="delete btn btn-danger btn-xs btn-deleteTable-list" delete-for="${id}"><i class="fa fa-trash-o"></i></a>
                        <a href="#" class= "btn btn-primary btn-xs btn-editTable-list" edit-for="${id}" title = "Sửa" > <i class="fa fa-edit"></i></a>`;
    },

    // danh sach lop du lieu
    SelectedLayer: function () {
        $.ajax({
            url: TreeView.CONSTS.URL_DANH_SACH_LAYER,
            method: "GET",
            async: false,
            success: function (result) {
                $(tableConfig.SELECTORS.select_lop_du_lieu).html('');

                var $option = "<option value = ''>-- Chọn lớp dữ liệu --</option>";
                $.each(result, function (index, item) {
                    if (item.type == "file") {
                        $option += '<option value="' + item.id + '">' + item.name + '</option>';
                    }
                });

                $(tableConfig.SELECTORS.select_lop_du_lieu).html($option)
            }
        });


        $(tableConfig.SELECTORS.select_lop_du_lieu).select2({
            //language: Host + "/libs/select2/js/i18n/vi.js",
            placeholder: `Chọn lớp dữ liệu`,
            allowClear: true,
            width: '100%'
        });
    },
    checkFormValidate: function () {
        TreeView.clearLabelError();

        let check = true;
        let inputName = $(tableConfig.SELECTORS.dataName).val();
        if (!validateText(inputName, "text", 0, 0)) {
            insertError($(tableConfig.SELECTORS.dataName), "other");
            check = false;
            TreeView.showLabelError(tableConfig.SELECTORS.dataName, "Dữ liệu không được để trống và phải hợp lệ");
        }

        let code = $(tableConfig.SELECTORS.dataCode).val();
        if (!validateText(code, "text", 0, 0)) {
            insertError($(tableConfig.SELECTORS.dataCode), "other");
            check = false;
            TreeView.showLabelError(tableConfig.SELECTORS.dataCode, "Dữ liệu không được để trống và phải hợp lệ");
        }

        if ($(tableConfig.SELECTORS.select_lop_du_lieu).val() == "") {
            TreeView.showLabelError(tableConfig.SELECTORS.select_lop_du_lieu, "Vui lòng bạn chọn lớp dữ liệu");
            check = false;
        }

        return check;
    },
    clearLabelError: function (element) {
        $(element).find(".lable-error").remove();
        $(element).removeClass("has-error");
    }
    ////get data table
    //getDatatable
}

function validateName(str) {
    var pattern = new RegExp('^(?!\s+$)([a-zA-Z0-9\sáàạảãâấầậẩẫăắằặẳẵÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴéèẹẻẽêếềệểễÉÈẸẺẼÊẾỀỆỂỄóòọỏõôốồộổỗơớờợởỡÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠúùụủũưứừựửữÚÙỤỦŨƯỨỪỰỬỮíìịỉĩÍÌỊỈĨđĐýỳỵỷỹÝỲỴỶỸ,.\-_]{0,1000})+$');
    return !!pattern.test(str);
}