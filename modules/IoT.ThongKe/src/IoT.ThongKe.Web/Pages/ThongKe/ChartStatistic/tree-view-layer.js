﻿var l = abp.localization.getResource('ThongKe');
var TreeViewLayer2 = {
    GLOBAL: {
        ArrayTree: [],
        ArrayNodeCheck: [],
        Labels: [],
        Ids: [],
        Dataset: [{
            label: "Biểu đồ thống kê số lượng đối tượng",
            data: [],
            backgroundColor: [],
            borderColor: [],
            borderWidth: 1
        }],
        listObject: [],
        listPropertiesLayer: [],
        nameUrlFileTable: '',
        page: 0,
        length: 20,
        inCallback: false,
        isReachedScrollEnd: false,
        idClicked: ''
    },
    CONSTS: {
        URL_GETLISTDIRECTORY: "/api/app/directory/directory-not-properties",
        URL_AJAXGETOBJECT: '/api/Statistic/MainObject/get-objectProperties-by-IdDirectory',
        URL_EXPORT_DOI_TUONG: '/api/Statistic/MainObject/exportdata'
    },
    SELECTORS: {
        tree: "#checkTree",
        dyntree_node_text: ".dynatree-title",
        detailLayerInfo: ".detail-layer-info",
        Modal_Object_Infor: "#myModalInforObject",
        btnExportDetailObjectInfo: ".export-detail-object-info",
        divContent: "#divContent",
        tableModal: "#tableModal",
        loading: "#loading-map",
    },
    init: function () {
        TreeViewLayer2.setEvent();
        TreeViewLayer2.GetListLayer();
    },
    setEvent: function () {

        $(TreeViewLayer2.SELECTORS.tree).on('click', TreeViewLayer2.SELECTORS.dyntree_node_text, function () {
            var folder = $(this).parent().attr('data-folder');
            //var id = $(this).attr('data-id');
            if (folder != "true") {
                $(this).parent().children().eq(1).click();
            }
        });

        $(".tree-o-i").on('click', '.item-li', function () {
            var id = $(this).attr('data-id');

            if ($(`#checkmark-${id}`).hasClass('check')) {
                if (TreeViewLayer2.GLOBAL.DirectoryForcus != id) {
                    if (!$('#lst-Object').hasClass('open')) {
                        $('#lst-Object').addClass('open')
                    }

                    //TreeViewLayer2.GLOBAL.DirectoryForcus = id;
                    //$(ThongTin.SELECTORS.content_list_object).html('');
                    //ThongTin.GLOBAL.page = 0;
                    //ThongTin.AppenedLstObject(id);
                }
            }

            if (!$(this).next().hasClass('open')) {
                $(this).next().addClass('open');
                $(this).next().slideToggle("slow");

                var element = $(this).find('.icon-menu-tree');
                if (element.length > 0) {
                    element.addClass('open')
                }
            }
            else {
                $(this).next().slideToggle("slow");
                $(this).next().removeClass('open');
                var element = $(this).find('.icon-menu-tree');
                if (element.length > 0) {
                    element.removeClass('open')
                }
            }

        });

        $(".tree-o-i").on('click', '.checkmark', function () {
            let id = $(this).attr('data-id');

            if (!$(this).hasClass('check')) {
                let max_select = 20;
                $(this).addClass('check');
                if (TreeViewLayer2.GLOBAL.ArrayNodeCheck.length >= max_select) {
                    //node.bSelected = !node.bSelected;
                    //console.log(this);
                    $(this).removeClass('check');
                    //$(this).css("background-color", "#fff");

                    swal({
                        title: "Thông báo",
                        text: "Chỉ được chọn tối đa " + max_select + " lớp",
                        icon: "warning",
                        type: "warning"
                    });
                } else {
                    TreeViewLayer2.GLOBAL.ArrayNodeCheck.push(id);
                    $.ajax({
                        type: "GET",
                        url: "/api/Statistic/MainObject/ObjectByIdDirectory",
                        data: {
                            id: id
                        },
                        async: true,
                        success: function (res) {
                            if (res.code == "ok") {
                                var data = res.result;
                                if (data != undefined) {
                                //lineChart.data.labels.push(item.data);
                                //lineChart.data.datasets.data.push(item.total);
                                var randomcolor = getRandomColor();
                                var toColor = hexToRgb(randomcolor);
                                let dataset = {
                                    label: '',
                                    data: data.total,
                                    backgroundColor: `rgba(${toColor.r}, ${toColor.g}, ${toColor.b}, 0.7)`,
                                    borderColor: `rgba(${toColor.r}, ${toColor.g}, ${toColor.b}, 1)`,
                                    borderWidth: 1
                                };

                                TreeViewLayer2.GLOBAL.Labels.push(data.objDirectory.name);
                                TreeViewLayer2.GLOBAL.Ids.push(data.objDirectory.id);
                                TreeViewLayer2.GLOBAL.Dataset[0].data.push(dataset.data);
                                TreeViewLayer2.GLOBAL.Dataset[0].backgroundColor.push(dataset.backgroundColor);
                                TreeViewLayer2.GLOBAL.Dataset[0].borderColor.push(dataset.borderColor);

                                var tr = `<tr>
                                                <td>${data.objDirectory.name}</td>
                                                <td class="text-center">${dataset.data}</td>
                                                <td class="text-center">
                                                    <button class="detail-layer-info btn btn-default" data-id="${data.objDirectory.id}" type="button"><i class="fa fa-eye" aria-hidden="true"></i> Xem</button>
                                                </td>
                                            </tr>`;

                                $('.data-chart-table').append(tr);

                                lineChart.data.labels = TreeViewLayer2.GLOBAL.Labels;
                                lineChart.data.datasets = TreeViewLayer2.GLOBAL.Dataset;
                                lineChart.update();

                                let obj = {
                                    id: id,
                                    name: data.objDirectory.name,
                                    lstProperties: data.properties
                                };
                                TreeViewLayer2.GLOBAL.listPropertiesLayer.push(obj);
                            }
                            }
                        },
                        error: function () {
                            alert("Lỗi ajax");
                        }
                    });
                }
            }
            else {
                $(this).removeClass('check');
                let checkExistLabel = id;
                var index = TreeViewLayer2.GLOBAL.Ids.indexOf(checkExistLabel);
                if (index > -1) {
                    TreeViewLayer2.GLOBAL.ArrayNodeCheck.splice(index, 1);
                    TreeViewLayer2.GLOBAL.Labels.splice(index, 1);
                    TreeViewLayer2.GLOBAL.Ids.splice(index, 1);
                    TreeViewLayer2.GLOBAL.Dataset[0].data.splice(index, 1);
                    TreeViewLayer2.GLOBAL.Dataset[0].backgroundColor.splice(index, 1);
                    TreeViewLayer2.GLOBAL.Dataset[0].borderColor.splice(index, 1);
                    lineChart.update();

                    $(".data-chart-table tr").eq(index).remove();
                    let obj = TreeViewLayer2.GLOBAL.listPropertiesLayer.findIndex(x => x.id == id);
                    if (obj > -1) {
                        TreeViewLayer2.GLOBAL.listPropertiesLayer.splice(obj, 1);
                    }
                }
            }

        });

        $('.data-chart-table').on('click', TreeViewLayer2.SELECTORS.detailLayerInfo, function () {
            let id = $(this).attr('data-id');
            TreeViewLayer2.GLOBAL.page = 0;
            TreeViewLayer2.GLOBAL.isReachedScrollEnd = false;
            TreeViewLayer2.getListObject(id, TreeViewLayer2.GLOBAL.page, TreeViewLayer2.GLOBAL.length);
            TreeViewLayer2.GLOBAL.idClicked = id;
            $(TreeViewLayer2.SELECTORS.divContent).scroll(TreeViewLayer2.scrollHandler);
        });

        $(TreeViewLayer2.SELECTORS.Modal_Object_Infor).on('hidden.bs.modal', function () {
            TreeViewLayer2.GLOBAL.listObject = [];
            TreeViewLayer2.GLOBAL.nameUrlFileTable = '';
            TreeViewLayer2.GLOBAL.idClicked = '';
        });

        $(TreeViewLayer2.SELECTORS.Modal_Object_Infor).on('click', TreeViewLayer2.SELECTORS.btnExportDetailObjectInfo, function () {
            var id = $(this).attr('data-id');
            TreeViewLayer2.showLoading(true);
            $.ajax({
                type: "GET",
                dataType: 'json',
                contentType: "application/json-patch+json",
                async: true,
                url: TreeViewLayer2.CONSTS.URL_EXPORT_DOI_TUONG,
                data: {
                    idDirectory: id
                },
                success: function (data) {
                    if (data.code == "ok") {

                        var local = window.location.origin;
                        var obj = TreeViewLayer2.GLOBAL.listPropertiesLayer.find(x => x.id == id);
                        var url = `${local}/BaoCao/ChartStatistic/Danh_sach_doi_tuong_${id}.xlsx`;
                        TreeViewLayer2.Download(url, "Danh_sach_doi_tuong_" + obj.name + ".xlsx");
                        TreeViewLayer2.showLoading(false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR + ":" + errorThrown);
                }
            });
        });

    },
    Download: function (url, filename) {
        fetch(url)
            .then(resp => resp.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.href = url;
                // the filename you want
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                window.URL.revokeObjectURL(url);
            })
            .catch(() => alert('oh no!'));
    },
    getListObject: function (id, skipnumber, countnumber) {
        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + TreeViewLayer2.CONSTS.URL_AJAXGETOBJECT + '?id=' + id + "&skipnumber=" + skipnumber + "&countnumber=" + countnumber,
            data: {
            },
            success: function (data) {
                if (data.code == "ok") {
                    if (data.result != null && data.result.length > 0) {
                        TreeViewLayer2.GLOBAL.listObject = TreeViewLayer2.GLOBAL.listObject.concat(data.result);
                        $(TreeViewLayer2.SELECTORS.Modal_Object_Infor).modal('show');
                        TreeViewLayer2.AddTableDanhSachObject(id, TreeViewLayer2.GLOBAL.page, TreeViewLayer2.GLOBAL.length);
                    } else {
                        TreeViewLayer2.GLOBAL.isReachedScrollEnd = true;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR + ":" + errorThrown);
            }
        });
    },
    AddTableDanhSachObject: function (id, numberSkip, pageSize) {
        let obj = TreeViewLayer2.GLOBAL.listPropertiesLayer.find(x => x.id == id);
        let html = '';
        if (obj != null && obj != undefined) {
            let lst = obj.lstProperties.filter(x => x.typeSystem != 1);
            html += `<div style="margin: 8px 0px;display:flex;justify-content: flex-end;">
                    <button class="export-detail-object-info btn btn-success" data-id="${id}" type="button"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Xuất file</button>
                    </div>`;
            var th = `<th style="width: 5%;">STT</th>
                    <th style="width: 15%;display:none;">Tên đối tượng</th>`;
            var td = ``;
            let indextable = numberSkip * pageSize;
            var lstresult = TreeViewLayer2.GLOBAL.listObject.slice(numberSkip * pageSize, (numberSkip * pageSize) + pageSize);
            $.each(lstresult, function (index, item) {
                td += `<tr>`;
                td += `<td>${indextable + 1}</td>`;
                td += `<td style="display:none;">${item.nameObject != null ? item.nameObject : "Chưa xác định"}</td>`;
                $.each(lst, function (i, obj) {
                    if (obj.typeProperties != "geojson" && obj.typeProperties != "maptile") {
                        if (index === 0) {
                            th += `<th>${obj.nameProperties}</th>`;
                        }
                        let checkproperties = item.listProperties.find(x => x.codeProperties == obj.codeProperties);
                        if (checkproperties != undefined && checkproperties != null) {
                            var value = '';
                            if (checkproperties.typeProperties == "link" || checkproperties.typeProperties == "image") {
                                try {
                                    let lst = JSON.parse(checkproperties.defalutValue);
                                    if ($.isArray(lst) && lst.length > 0) {
                                        value += `<ul>`;
                                        for (var im = 0; im < lst.length; im++) {
                                            value += `<li>${lst[im].url}</li>`;
                                        }
                                        value += `</ul>`;
                                    }
                                }
                                catch {
                                    value = "";
                                }
                            } else if (checkproperties.typeProperties == "file") {
                                TreeViewLayer2.GetFileByObjectMainId(item.id, TreeViewLayer2.showDataFileInTable);
                                value = TreeViewLayer2.GLOBAL.nameUrlFileTable;
                            }
                            else {
                                if (checkproperties.typeProperties == "checkbox" || checkproperties.typeProperties == "list" || checkproperties.typeProperties == "radiobutton") // nếu là thuộc tính danh sách thì lấy những value đã dc check
                                {
                                    try {
                                        var lstItems = JSON.parse(checkproperties.defalutValue);

                                        if (lstItems != undefined && $.isArray(lstItems)) {
                                            if (checkproperties.typeProperties != "checkbox") {
                                                var valueItem = lstItems.find(x => x.checked == true);

                                                if (valueItem != null) {
                                                    value = valueItem.name;
                                                }
                                            }
                                            else {
                                                var string = "";
                                                $.each(lstvalue, function (i, item) {
                                                    if (item.checked) {
                                                        string += "," + item.name;
                                                    }
                                                });

                                                value = string;
                                            }
                                        }
                                        else {
                                            var danhSachValue = JSON.parse(obj.defalutValue);

                                            if (checkproperties.typeProperties != "checkbox") {
                                                var valueItem3 = danhSachValue.find(x => x.code == checkproperties.defalutValue);

                                                if (valueItem3 != undefined) {
                                                    value = valueItem3.name;
                                                }
                                            }
                                            else {
                                                var lstCheck = [];
                                                if (checkproperties.defalutValue != "" && checkproperties.defalutValue != null) {
                                                    lstCheck = checkproperties.defalutValue.split(",");
                                                }

                                                var arrayCheckboxObject = danhSachValue.filter(x => lstCheck.indexOf(x.code) > -1)
                                                if (arrayCheckboxObject.length > 0) {
                                                    value = arrayCheckboxObject.map(x => x.name).toString();
                                                }
                                            }

                                        }
                                    }
                                    catch {
                                        var danhSachValue2 = JSON.parse(obj.defalutValue);

                                        if (checkproperties.typeProperties != "checkbox") {
                                            var valueItem2 = danhSachValue2.find(x => x.code == checkproperties.defalutValue);

                                            if (valueItem2 != undefined) {
                                                value = valueItem2.name;
                                            }
                                        }
                                        else {
                                            var lstCheck2 = [];
                                            if (checkproperties.defalutValue != "" && checkproperties.defalutValue != null) {
                                                lstCheck2 = checkproperties.defalutValue.split(",");
                                            }

                                            var arrayCheckboxObject2 = danhSachValue2.filter(x => lstCheck2.indexOf(x.code) > -1)
                                            if (arrayCheckboxObject2.length > 0) {
                                                value = arrayCheckboxObject2.map(x => x.name).toString();
                                            }
                                        }

                                    }
                                }
                                else {
                                    if (checkproperties.typeProperties == "bool") {
                                        if (checkproperties.defalutValue == "true" || checkproperties.defalutValue == true || checkproperties.defalutValue == "on") {
                                            value = "Có";
                                        } else {
                                            value = "Không";
                                        }
                                    }
                                    else {
                                        value = checkproperties.defalutValue;
                                    }
                                }
                            }
                            td += `<td>${value != null ? value : "Chưa xác định"}</td>`;
                        } else {
                            td += `<td></td>`;
                        }
                    }
                });
                td += `</tr>`;
                indextable++;
            });
            if (numberSkip == 0) {
                var table = `<table class="table" id="tableModal">
                            <thead><tr>${th}</tr></thead>
                            <tbody>${td}</tbody>
                        </table>`;
                html += `<div id="divContent" style="overflow-x: scroll;max-width: 100%;overflow-y: auto;max-height: 680px;">` + table + `</div>`;
            } else {
                $('#tableModal tbody').append(td);
                return;
            }
        }

        $('.danh-sach-chi-tiet-hoat-dong').html('');
        $('.danh-sach-chi-tiet-hoat-dong').html(html);
    },
    GetFileByObjectMainId: function (ObjectMainid, callback) {
        let id = ObjectMainid;

        $.ajax({
            type: "GET",
            contentType: "application/json-patch+json",
            async: false,
            url: document.location.origin + "/api/Statistic/MainObject/get-file-by-objectId?Id=" + id,
            data: {
            },
            success: callback
        });
    },
    showDataFileInTable: function (data) {
        if (data.code === "ok") {
            if (data.result.length > 0) {
                let name = `<ul>`;
                $.each(data.result, function (i, obj) {
                    name += `<li>${obj.urlDocument}</li>`;
                });
                name += `</ul>`;
                TreeViewLayer2.GLOBAL.nameUrlFileTable = name;
            }
        }

    },
    scrollHandler: function () {

        var h1 = $(TreeViewLayer2.SELECTORS.divContent).scrollTop()
        var h2 = $(TreeViewLayer2.SELECTORS.tableModal).height() - $(TreeViewLayer2.SELECTORS.divContent).height();
        //console.log("H1: " + h1);
        //console.log("H2: " + h2);
        if (h1 >= h2 && !TreeViewLayer2.GLOBAL.isReachedScrollEnd) {
            TreeViewLayer2.loadTable();
        }
    },
    loadTable: function () {
        if (TreeViewLayer2.GLOBAL.page > -1 && !TreeViewLayer2.GLOBAL.inCallback) {
            TreeViewLayer2.GLOBAL.inCallback = true;
            TreeViewLayer2.GLOBAL.page++;
            //$(menuLeft.SELECTORS.spinner).show();
            setTimeout(function () {
                let status = TreeViewLayer2.appendTable();
                if (!status) {
                    TreeViewLayer2.GLOBAL.page = -1;
                }
                TreeViewLayer2.GLOBAL.inCallback = false;
                //$(menuLeft.SELECTORS.spinner).hide();
            }, 500);

        }
    },
    appendTable: function () {
        let status = false;
        var pageSize = TreeViewLayer2.GLOBAL.length;
        var numberOfRecordToskip = TreeViewLayer2.GLOBAL.page * pageSize;
        TreeViewLayer2.getListObject(TreeViewLayer2.GLOBAL.idClicked, numberOfRecordToskip, pageSize);
        return true;
    },
    GetListLayer: function () {
        $.ajax({
            type: "GET",
            url: TreeViewLayer2.CONSTS.URL_GETLISTDIRECTORY,
            data: {
            },
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.length > 0) {
                    TreeViewLayer2.GLOBAL.ArrayTree = data;

                    var parent = data.find(x => x.level == "0");
                    var treedynamic = TreeViewLayer2.FillDynaTree(parent);

                    var html = TreeViewLayer2.DataTreeGen(treedynamic);


                    $(`.tree-o-i`).html(html);

                    $(`.item-li`).each(function () {
                        if ($(this).find('.title-item').text() == "") {
                            $(this).remove();
                        }

                        var isFolder = $(this).attr('data-type');
                        var id = $(this).attr('data-id');

                        if (isFolder != "true") {
                            var checkbox = `<label class="container-checkbox">
                                              <input type="checkbox">
                                              <span class="checkmark" id="checkmark-${id}" data-id="${id}"></span>
                                            </label>`;

                            $(this).parent().prepend(checkbox);
                        }
                    });

                    //var arrayTree = [];
                    //arrayTree.push(treedynamic);
                    //TreeViewLayer2.LoadDynaTree(arrayTree);
                }
            },
            error: function () {
                alert("Lỗi ajax");
            }
        });
    },
    showLoading: function (isCheck) {
        if (isCheck) {
            $(TreeViewLayer2.SELECTORS.loading).show();
        }
        else {
            $(TreeViewLayer2.SELECTORS.loading).hide();
        }
    },
    //dynatree
    FillDynaTree: function (data) {
        var obj =
        {
            title: data.name,
            tooltip: data.name,
            select: false,
            isFolder: false,
            key: data.id,
            children: [],
            expand: true,
            icon: null
        };

        var parent = TreeViewLayer2.GLOBAL.ArrayTree.find(x => x.id == data.id);

        if (parent != null) {
            if (data.type != "file") {
                obj.isFolder = true;
            }
            else {
                if (data.image2D != null && data.image2D != "") {
                    obj.icon = data.image2D;
                }
            }

            obj.key = parent.id;

            var lstChil = TreeViewLayer2.GLOBAL.ArrayTree.filter(x => x.parentId == parent.id);

            if (lstChil != null && lstChil.length > 0) {
                //obj.select = true;

                var dataChil = [];
                for (var i = 0; i < lstChil.length; i++) {
                    var objchil = TreeViewLayer2.FillDynaTree(lstChil[i]);
                    dataChil.push(objchil);
                }

                obj.children = dataChil;
            }
            return obj;
        }

    },
    LoadDynaTree: function (data) {
        var treeData = [];

        treeData = data;
        $("#checkTree").dynatree({
            checkbox: true,
            selectMode: 2,
            children: treeData,
            onActivate: function (node) {
            },
            onSelect: function (select, node) {
                var selNodes = node.tree.getSelectedNodes();
                var selKeys = $.map(selNodes, function (node) {
                    return node.data.key;
                });

                TreeViewLayer2.GLOBAL.ArrayNodeCheck = selKeys;
                let id = node.data.key;
                if (!select) {
                    let checkExistLabel = node.data.title;
                    var index = TreeViewLayer2.GLOBAL.Labels.indexOf(checkExistLabel);
                    if (index > -1) {
                        TreeViewLayer2.GLOBAL.Labels.splice(index, 1);
                        TreeViewLayer2.GLOBAL.Dataset[0].data.splice(index, 1);
                        TreeViewLayer2.GLOBAL.Dataset[0].backgroundColor.splice(index, 1);
                        TreeViewLayer2.GLOBAL.Dataset[0].borderColor.splice(index, 1);
                        lineChart.update();

                        $(".data-chart-table tr").eq(index).remove();
                    }
                } else {
                    let max_select = 2;
                    if (TreeViewLayer2.GLOBAL.ArrayNodeCheck.length > max_select) {
                        node.bSelected = !node.bSelected;
                        swal({
                            title: "Thông báo",
                            text: "Chỉ được chọn tối đa " + max_select + " lớp",
                            icon: "warning",
                            type: "warning"
                        });
                    } else {
                        $.ajax({
                            type: "GET",
                            url: "/api/Statistic/MainObject/ObjectByIdDirectory",
                            data: {
                                id: id
                            },
                            async: true,
                            success: function (res) {
                                if (res.code == "ok") {
                                    var data = res.result;
                                    if (data != undefined) {
                                    //lineChart.data.labels.push(item.data);
                                    //lineChart.data.datasets.data.push(item.total);
                                    var randomcolor = getRandomColor();
                                    var toColor = hexToRgb(randomcolor);
                                    let dataset = {
                                        label: '',
                                        data: data.total,
                                        backgroundColor: `rgba(${toColor.r}, ${toColor.g}, ${toColor.b}, 0.7)`,
                                        borderColor: `rgba(${toColor.r}, ${toColor.g}, ${toColor.b}, 1)`,
                                        borderWidth: 1
                                    };

                                    TreeViewLayer2.GLOBAL.Labels.push(data.objDirectory.name);
                                    TreeViewLayer2.GLOBAL.Dataset[0].data.push(dataset.data);
                                    TreeViewLayer2.GLOBAL.Dataset[0].backgroundColor.push(dataset.backgroundColor);
                                    TreeViewLayer2.GLOBAL.Dataset[0].borderColor.push(dataset.borderColor);

                                    var tr = `<tr>
                                                <td>${data.objDirectory.name}</td>
                                                <td class="text-center">${dataset.data}</td>
                                            </tr>`;

                                    $('.data-chart-table').append(tr);

                                    lineChart.data.labels = TreeViewLayer2.GLOBAL.Labels;
                                    lineChart.data.datasets = TreeViewLayer2.GLOBAL.Dataset;
                                    lineChart.update();
                                }
                                }
                            },
                            error: function () {
                                alert("Lỗi ajax");
                            }
                        });
                    }
                }

            },
            onClick: function (node, event) {


            },
            onKeydown: function (node, event) {

            },
            // The following options are only required, if we have more than one tree on one page:
            cookieId: "dynatree-Cb2",
            idPrefix: "dynatree-Cb2-"
        });
    },
    DataTreeGen: function (data) {
        //$('.title').html(data.title);

        var tree = data.children;

        var html = ``;

        for (var i = 0; i < tree.length; i++) {
            var name = tree[i].title;
            var icon = `<img crossorigin="anonymous" src="${tree[i].icon}" alt="" />`;
            if (tree[i].icon == "" || tree[i].icon == null) {
                icon = `<i class="fa ${tree[i].isFolder ? "fa-folder-open" : "fa-file"}"></i>`;
            }

            if (tree[i].children.length > 0) {
                html += `<li id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${name}" data-parent="${tree[i].parentId}">
                            <a href="javascript:;" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                <span class="pull-left-container">
                                    <i class="fa fa-angle-right pull-left icon-menu-tree"></i>
                                </span>
                                <span class="title-item" style= "padding-left:${tree[i].isFolder ? "3rem;" : "3rem;"}">${icon} ${name} (${tree[i].children.length})</span>

                                <ul class="tree-ul" id="tree-ul-${tree[i].key}" data-id="${tree[i].key}" style="display:none;">`;

                html += TreeViewLayer2.DataTreeGen(tree[i]);

                html += `
                                </ul>
                            </a>
                        </li>`;
            }
            else {
                html += `<li id="tree-item-${tree[i].key}" class="treeview" style="height: auto;" data-search="${name}"  data-parent="${tree[i].parentId}">
                                <a href="javascript:;" class="item-li" data-type="${tree[i].isFolder}" data-chil="${tree[i].children.length}" data-id="${tree[i].key}">
                                <span class="title-item" style= "padding-left:${tree[i].isFolder ? "3rem;" : "3rem;"}">${icon} ${name}</span></a>    
                           </li>`;
            }
        }

        return html;
    }
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

