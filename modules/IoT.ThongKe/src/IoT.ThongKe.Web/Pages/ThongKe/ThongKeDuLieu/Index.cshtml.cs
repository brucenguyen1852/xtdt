﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace IoT.ThongKe.Web.Pages.ThongKe.ThongKeDuLieu
{
    public class IndexModel : AbpPageModel
    {
        public string MaDuAnFilter { get; set; }
        public string TenDuAnFilter { get; set; }
        public string Directory { get; set; }
        public string District { get; set; }
        public string SubDistrict { get; set; }
        public string Acreage { get; set; }
        public string PlanToDo { get; set; }
        public string Progress { get; set; }
        public List<SelectListItem> ProgressList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem {Value = "", Text= "Chọn tiến độ dự án"},
            new SelectListItem {Value = "DangTamDung", Text= "Đang hoạt động"},
            new SelectListItem {Value = "DangHoatDong", Text= "Đang triển khai"},
            new SelectListItem {Value = "DangTamDung", Text= "Đang tạm dừng"}
        };

        public List<SelectListItem> PlanToDoList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem {Value = "", Text= "Chọn danh mục dự án "},
            new SelectListItem {Value = "DauTuTrongNuoc", Text= "Đầu tư trong nước"},
            new SelectListItem {Value = "TrongDiemDauTu", Text= "Trọng điểm thu hút đầu từ"},
            new SelectListItem {Value = "DauTuNuocNgoai", Text= "Đầu tư nước ngoài"}
        };

        public List<SelectListItem> ListDistrict { get; set; }
        public List<SelectListItem> ListSubDistrict { get; set; }
        public List<SelectListItem> ListDirectory { set; get; }
        public List<SelectListItem> ListAcreage { set; get; }

        public async Task OnGetAsync()
        {
            ListAcreage = new List<SelectListItem>();
            ListDirectory = new List<SelectListItem>();
            ListDistrict = new List<SelectListItem>();
            ListSubDistrict = new List<SelectListItem>();
            await Task.CompletedTask;
        }
    }
}
