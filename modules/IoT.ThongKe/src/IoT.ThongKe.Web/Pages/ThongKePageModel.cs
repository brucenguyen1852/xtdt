﻿using IoT.ThongKe.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace IoT.ThongKe.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class ThongKePageModel : AbpPageModel
    {
        protected ThongKePageModel()
        {
            LocalizationResourceType = typeof(ThongKeResource);
            ObjectMapperContext = typeof(ThongKeWebModule);
        }
    }
}