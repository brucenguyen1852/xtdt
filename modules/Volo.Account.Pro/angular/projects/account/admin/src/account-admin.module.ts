import { CoreModule } from '@abp/ng.core';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [CoreModule],
})
export class AccountAdminModule {}
