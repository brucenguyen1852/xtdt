import { CoreModule } from '@abp/ng.core';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AccountSettingsModule } from '@volo/abp.ng.account/admin';
import { ACCOUNT_SETTING_TAB_PROVIDERS } from './providers/setting-tab.provider';

/**
 * @deprecated Use AccountAdminConfigModule instead. To be deleted in v5.0
 * The module can be imported from '@volo/abp.ng.account/admin/config'
 */
@NgModule({
  imports: [CoreModule, AccountSettingsModule],
  exports: [AccountSettingsModule],
  declarations: [],
})
export class AccountConfigModule {
  static forRoot(): ModuleWithProviders<AccountConfigModule> {
    return {
      ngModule: AccountConfigModule,
      providers: [ACCOUNT_SETTING_TAB_PROVIDERS],
    };
  }
}
