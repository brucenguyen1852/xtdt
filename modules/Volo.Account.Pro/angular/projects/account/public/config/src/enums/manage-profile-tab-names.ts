export const enum eAccountManageProfileTabNames {
  ProfilePicture = 'AbpAccount::ProfileTab:Picture',
  ChangePassword = 'AbpAccount::ProfileTab:Password',
  PersonalInfo = 'AbpAccount::ProfileTab:PersonalInfo',
  TwoFactor = 'AbpAccount::ProfileTab:TwoFactor',
}
