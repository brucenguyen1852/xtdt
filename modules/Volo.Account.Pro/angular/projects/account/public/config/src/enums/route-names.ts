export const enum eAccountRouteNames {
  Account = 'AbpAccount::Menu:Account',
  Login = 'AbpAccount::Login',
  Register = 'AbpAccount::Register',
  EmailConfirmation = 'AbpAccount::EmailConfirmation',
  LinkLogged = 'AbpAccount::LinkLogged',
  ForgotPassword = 'AbpAccount::ForgotPassword',
  ResetPassword = 'AbpAccount::ResetPassword',
  ManageProfile = 'AbpAccount::ManageYourProfile',
  MySecurityLogs = 'AbpAccount::MySecurityLogs',
}
