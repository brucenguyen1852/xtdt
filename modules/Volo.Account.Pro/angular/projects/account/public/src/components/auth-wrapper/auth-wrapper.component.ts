import {
  ConfigStateService,
  EnvironmentService,
  getRoutePath,
  LanguageInfo,
  MultiTenancyService,
  RouterEvents,
  RoutesService,
  SessionStateService,
  SubscriptionService,
} from '@abp/ng.core';
import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import snq from 'snq';
import { eAccountComponents } from '../../enums/components';

@Component({
  selector: 'abp-auth-wrapper',
  templateUrl: './auth-wrapper.component.html',
  providers: [SubscriptionService],
})
export class AuthWrapperComponent implements OnInit {
  protected multiTenancy: MultiTenancyService;
  protected router: Router;
  protected route: ActivatedRoute;
  protected routes: RoutesService;
  protected configState: ConfigStateService;
  protected sessionState: SessionStateService;
  protected environment: EnvironmentService;
  protected subscription: SubscriptionService;
  protected routerEvents: RouterEvents;

  private _tenantBoxVisible = true;
  tenantBoxKey = eAccountComponents.TenantBox;
  logoKey = eAccountComponents.Logo;

  get isMultiTenancyEnabled$() {
    return this.configState.getDeep$('multiTenancy.isEnabled');
  }

  get languages$() {
    return this.configState.getDeep$('localization.languages');
  }

  get enableLocalLogin$(): Observable<boolean> {
    return this.configState
      .getSetting$('Abp.Account.EnableLocalLogin')
      .pipe(map(value => value?.toLowerCase() !== 'false'));
  }

  get isTenantBoxVisible() {
    return this._tenantBoxVisible && this.multiTenancy.isTenantBoxVisible;
  }

  set isTenantBoxVisible(value: boolean) {
    this.multiTenancy.isTenantBoxVisible = value;
  }

  get defaultLanguage$(): Observable<{ displayName: string; flagIcon: string }> {
    return this.languages$.pipe(
      map(languages => {
        const lang: Partial<LanguageInfo> = snq(
          () => languages.find(l => l.cultureName === this.selectedLangCulture),
          {} as any,
        );
        return {
          displayName: lang.displayName || '',
          flagIcon: lang.flagIcon,
        };
      }),
    );
  }

  get dropdownLanguages$(): Observable<LanguageInfo[]> {
    return this.languages$.pipe(
      map(
        languages =>
          snq(() => languages.filter(lang => lang.cultureName !== this.selectedLangCulture)),
        [],
      ),
    );
  }

  get selectedLangCulture(): string {
    return this.sessionState.getLanguage();
  }

  get appInfo() {
    return this.environment.getEnvironment().application;
  }

  get pageLabel(): string {
    const path = getRoutePath(this.router);
    const route = this.routes.search({ path });
    return route?.name || '';
  }

  private setTenantBoxVisibility = () => {
    this._tenantBoxVisible = this.route.snapshot.firstChild.data.tenantBoxVisible ?? true;
  };

  constructor(protected injector: Injector) {
    this.multiTenancy = injector.get(MultiTenancyService);
    this.router = injector.get(Router);
    this.route = injector.get(ActivatedRoute);
    this.routes = injector.get(RoutesService);
    this.configState = injector.get(ConfigStateService);
    this.sessionState = injector.get(SessionStateService);
    this.environment = injector.get(EnvironmentService);
    this.subscription = injector.get(SubscriptionService);
    this.routerEvents = injector.get(RouterEvents);
  }

  ngOnInit() {
    this.setTenantBoxVisibility();
    this.listenToNavigationEnd();
  }

  private listenToNavigationEnd() {
    const stream = this.routerEvents.getNavigationEvents('End');
    this.subscription.addOne(stream, this.setTenantBoxVisibility);
  }

  onChangeLang(cultureName: string) {
    this.sessionState.setLanguage(cultureName);
  }
}
