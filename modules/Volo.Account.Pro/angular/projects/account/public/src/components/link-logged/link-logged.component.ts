import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IdentityLinkUserService } from '@volo/abp.commercial.ng.ui/config';
import { getRedirectUrl } from '../../utils/auth-utils';

@Component({
  selector: 'abp-link-logged',
  template: `
    <div>
      <button type="button" class="btn btn-primary btn-block mb-3" (click)="navigateToMainPage()">
        &larr;
        <span class="ml-1">{{ 'AbpAccount::StayWithCurrentAccount' | abpLocalization }}</span>
      </button>
      <button
        (click)="navigateToMainPageForLinkLogin()"
        class="btn btn-secondary btn-block"
        type="button"
      >
        <span class="mr-1">{{
          'AbpAccount::ReturnToPreviousAccount' | abpLocalization: tenantAndUserName
        }}</span>
        &rarr;
      </button>
    </div>
  `,
})
export class LinkLoggedComponent implements OnInit {
  tenantAndUserName = '';

  get linkUser() {
    const {
      linkUserId,
      linkTenantId,
      linkUserName,
      linkTenantName,
    } = this.route.snapshot.queryParams;

    return { linkUserId, linkTenantId, linkUserName, linkTenantName };
  }

  constructor(
    private injector: Injector,
    private router: Router,
    private route: ActivatedRoute,
    private identityLinkUserService: IdentityLinkUserService,
  ) {}

  ngOnInit() {
    this.init();
  }

  protected init() {
    const { linkUserId, linkTenantId, linkUserName, linkTenantName } = this.linkUser;

    if (!linkUserId) {
      this.navigateToLogin();
      return;
    }

    this.identityLinkUserService
      .isLinked({ tenantId: linkTenantId, userId: linkUserId })
      .subscribe(res => {
        if (!res) {
          this.navigateToLogin();
        }
      });

    this.tenantAndUserName = linkTenantId ? `${linkTenantName}\\${linkUserName}` : linkUserName;
  }

  protected navigateToLogin() {
    this.router.navigateByUrl('/account/login');
  }

  navigateToMainPage(queryParams?: Params) {
    this.router.navigate([getRedirectUrl(this.injector) || '/'], { queryParams });
  }

  navigateToMainPageForLinkLogin() {
    const { linkUserId, linkTenantId } = this.linkUser;
    this.navigateToMainPage({ handler: 'linkLogin', linkUserId, linkTenantId });
  }
}
