export const enum eAccountRouteNames {
  Account = 'AbpAccount::Menu:Account',
  Login = 'AbpAccount::Login',
  Register = 'AbpAccount::Register',
  ForgotPassword = 'AbpAccount::ForgotPassword',
  ResetPassword = 'AbpAccount::ResetPassword',
  ManageProfile = 'AbpAccount::ManageYourProfile',
}
