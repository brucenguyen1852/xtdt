import type { ExtensibleObject } from '@abp/ng.core';
import { eProfilePictureType } from '@volo/abp.commercial.ng.ui/config';

export interface ChangePasswordInput {
  currentPassword: string;
  newPassword: string;
}

export interface ProfileDto extends ExtensibleObject {
  userName: string;
  email: string;
  emailConfirmed: boolean;
  name: string;
  surname: string;
  phoneNumber: string;
  phoneNumberConfirmed: boolean;
  isExternal: boolean;
  hasPassword: boolean;
}
export interface UpdateProfileDto extends ExtensibleObject {
  userName: string;
  email: string;
  name: string;
  surname: string;
  phoneNumber: string;
}

export interface ConfirmEmailInput {
  userId: string;
  token: string;
  tenantId?: string;
}

export interface ConfirmPhoneNumberInput {
  token: string;
  userId: string;
}

export interface ProfilePictureInput {
  type: eProfilePictureType;
  imageContent: number[];
}

export interface ProfilePictureSourceDto {
  type: eProfilePictureType;
  source?: string;
  fileContent: number[];
}

export interface RegisterDto {
  userName: string;
  emailAddress: string;
  password: string;
  appName: string;
  returnUrl?: string;
  returnUrlHash?: string;
  captchaResponse?: string;
}

export interface ResetPasswordDto {
  userId?: string;
  resetToken: string;
  password: string;
}

export interface SendEmailConfirmationTokenDto {
  appName: string;
  email: string;
  userId: string;
  returnUrl?: string;
  returnUrlHash?: string;
}

export interface SendPhoneNumberConfirmationTokenDto {
  phoneNumber: string;
  userId: string;
}

export interface SendPasswordResetCodeDto {
  email: string;
  appName: string;
  returnUrl?: string;
  returnUrlHash?: string;
}

export interface IFormFile {
  contentType: string;
  contentDisposition: string;
  headers: any;
  length: number;
  name: string;
  fileName: string;
}
