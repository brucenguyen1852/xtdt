import { Profile } from '@abp/ng.core';

export type ProfileResponse = Profile.Response;
