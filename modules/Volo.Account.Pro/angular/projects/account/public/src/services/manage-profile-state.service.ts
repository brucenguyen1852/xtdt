import { InternalStore, Profile } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface ManageProfileState {
  profile: Profile.Response;
  hideEmailVerificationBtn: boolean;
}

@Injectable({ providedIn: 'root' })
export class ManageProfileStateService {
  private readonly store = new InternalStore({} as ManageProfileState);

  get createStateStream() {
    return this.store.sliceState;
  }

  getProfile$(): Observable<Profile.Response> {
    return this.store.sliceState(state => state.profile);
  }

  getProfile(): Profile.Response {
    return this.store.state.profile;
  }

  setProfile(profile: Profile.Response) {
    this.store.patch({ profile });
  }

  setHideEmailVerificationBtn(hideEmailVerificationBtn: boolean) {
    this.store.patch({ hideEmailVerificationBtn });
  }
}
