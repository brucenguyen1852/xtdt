﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Volo.Abp.Account
{
    public class SendEmailSecurityCodeDto
    {
        [Required]
        public Guid UserId { get; set; }
    }
}
