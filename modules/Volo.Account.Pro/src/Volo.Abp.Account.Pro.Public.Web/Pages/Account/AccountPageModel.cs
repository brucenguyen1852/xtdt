﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Owl.reCAPTCHA;
using Volo.Abp.Account.Localization;
using Volo.Abp.AspNetCore.ExceptionHandling;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.ExceptionHandling;
using Volo.Abp.Identity;
using IdentityUser = Volo.Abp.Identity.IdentityUser;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
    public abstract class AccountPageModel : AbpPageModel
    {
        public IAccountAppService AccountAppService { get; set; }
        public SignInManager<IdentityUser> SignInManager { get; set; }
        public IdentityUserManager UserManager { get; set; }
        public IdentitySecurityLogManager IdentitySecurityLogManager { get; set; }
        public IIdentityLinkUserAppService IdentityLinkUserAppService { get; set; }
        public IOptions<IdentityOptions> IdentityOptions { get; set; }
        public IOptionsSnapshot<reCAPTCHAOptions> ReCaptchaOptions { get; set; }
        public IExceptionToErrorInfoConverter ExceptionToErrorInfoConverter { get; set; }

        protected AccountPageModel()
        {
            ObjectMapperContext = typeof(AbpAccountPublicWebModule);
            LocalizationResourceType = typeof(AccountResource);
        }

        protected virtual void CheckCurrentTenant(Guid? tenantId)
        {
            if (CurrentTenant.Id != tenantId)
            {
                throw new ApplicationException(
                    $"Current tenant is different than given tenant. CurrentTenant.Id: {CurrentTenant.Id}, given tenantId: {tenantId}"
                );
            }
        }

        protected virtual string GetLocalizeExceptionMessage(Exception exception)
        {
            if (exception is ILocalizeErrorMessage || exception is IHasErrorCode)
            {
                return ExceptionToErrorInfoConverter.Convert(exception, false).Message;
            }

            return exception.Message;
        }
    }
}
