﻿(function($) {

    var l = abp.localization.getResource("AbpAccount");

    var myLinkUsers = new abp.ModalManager(abp.appPath + "Account/LinkUsers/LinkUsersModal");
    myLinkUsers.onOpen(function () {
        $("#linkUserLoginForm").data("dataTable").columns.adjust();
    });

    $(function() {
        $('a.dropdown-item').on("click", function() {
            if($(this).attr('id') === "MenuItem_Account.MyLinkUsers"){
                myLinkUsers.open();
            }
        });
    });

    abp.ui.extensions.entityActions
        .get("account.linkUsers")
        .addContributor(function (actionList) {
            return actionList.addManyTail([
                {
                    text: l("LoginAsThisUser"),
                    action: function (data) {
                        $("#linkUserLoginForm input[name='LinkUserId']").val(
                            data.record.targetUserId
                        );
                        if (data.record.targetTenantId) {
                            $("#linkUserLoginForm input[name='LinkTenantId']").val(
                                data.record.targetTenantId
                            );
                        }
                        $("#linkUserLoginForm").submit();
                    },
                },
                {
                    text: l("Delete"),
                    confirmMessage: function (data) {
                        return l(
                            'DeleteLinkUserConfirmationMessage',
                            data.record.targetTenantName ?
                                data.record.targetTenantName + "\\" + data.record.targetUserName :
                                data.record.targetUserName
                        );
                    },
                    visible: function (data) {
                        return data.directlyLinked
                    },
                    action: function (data) {
                        volo.abp.identity.identityLinkUser
                            .unlink({
                                UserId: data.record.targetUserId,
                                TenantId: data.record.targetTenantId,
                            })
                            .then(function () {
                                _dataTable.ajax.reload(null, false);
                            });
                    },
                },
            ]);
        });

    abp.ui.extensions.tableColumns.get("account.linkUsers").addContributor(
        function (columnList) {
            columnList.addManyTail([
                {
                    title: l("Actions"),
                    rowAction: {
                        items: abp.ui.extensions.entityActions
                            .get("account.linkUsers")
                            .actions.toArray(),
                    },
                },
                {
                    title: l("TenantAndUserName"),
                    render: function (data, type, row) {

                        row.targetUserName = $.fn.dataTable.render.text().display(row.targetUserName);

                        if (row.targetTenantName) {
                            return $.fn.dataTable.render.text().display(row.targetTenantName) + "\\" + row.targetUserName;
                        } else {
                            return row.targetUserName;
                        }
                    },
                    orderable: true
                },
                {
                    title: l("DirectlyLinked"),
                    data: "directlyLinked",
                    dataFormat: "boolean",
                    orderable: false
                }
            ]);
        },
        0 //adds as the first contributor
    );

})(jQuery);
