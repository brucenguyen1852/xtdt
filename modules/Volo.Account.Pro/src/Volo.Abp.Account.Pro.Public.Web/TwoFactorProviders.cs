﻿using Microsoft.AspNetCore.Identity;

namespace Volo.Abp.Account.Public.Web
{
    public static class TwoFactorProviders
    {
        public static readonly string Email = TokenOptions.DefaultEmailProvider;
        public static readonly string Phone = TokenOptions.DefaultPhoneProvider;

        public static readonly string GoogleAuthenticator = "GoogleAuthenticator"; //TODO: Not implemented yet
    }
}
