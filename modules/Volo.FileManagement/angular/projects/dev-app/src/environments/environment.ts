const baseUrl = 'http://localhost:4200';

export const environment = {
  production: false,
  application: {
    name: 'File Management',
  },
  oAuthConfig: {
    // Use test-app backend istead of chat backend projects
    issuer: 'https://localhost:44326',
    redirectUri: baseUrl,
    clientId: 'AbpCommercialTest_App',
    responseType: 'code',
    scope: 'offline_access AbpCommercialTest',
  },
  apis: {
    default: {
      url: 'https://localhost:44398',
      rootNamespace: 'AbpCommercialTest',
    },
    FileManagement: {
      url: 'https://localhost:44398',
      rootNamespace: 'Volo',
    },
  },
};
