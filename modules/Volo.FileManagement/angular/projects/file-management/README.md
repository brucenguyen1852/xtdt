# FileManagement

This is a startup project based on the ABP framework. For more information, visit <a href="https://abp.io/" target="_blank">abp.io</a>

## Code scaffolding

Run `ng generate component component-name --project file-management` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project file-management`.
> Note: Don't forget to add `--project file-management` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build file-management` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build file-management`, go to the dist folder `cd dist/file-management` and run `npm publish`.

## Running unit tests

Run `ng test file-management` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
