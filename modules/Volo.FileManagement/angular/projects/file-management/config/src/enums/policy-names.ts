export const enum eFileManagementPolicyNames {
  DirectoryDescriptor = 'FileManagement.DirectoryDescriptor',
  DirectoryDescriptorCreate = 'FileManagement.DirectoryDescriptor.Create',
  DirectoryDescriptorDelete = 'FileManagement.DirectoryDescriptor.Delete',
  DirectoryDescriptorUpdate = 'FileManagement.DirectoryDescriptor.Update',
  FileDescriptor = 'FileManagement.FileDescriptor',
  FileDescriptorCreate = 'FileManagement.FileDescriptor.Create',
  FileDescriptorDelete = 'FileManagement.FileDescriptor.Delete',
  FileDescriptorUpdate = 'FileManagement.FileDescriptor.Update',
}
