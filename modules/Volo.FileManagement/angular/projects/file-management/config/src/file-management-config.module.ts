import { ModuleWithProviders, NgModule } from '@angular/core';
import { FILE_MANAGEMENT_ROUTE_PROVIDERS } from './providers/route.provider';
import { FILE_MANAGEMENT_FEATURES_PROVIDERS } from '@volo/abp.ng.file-management/common';

@NgModule()
export class FileManagementConfigModule {
  static forRoot(): ModuleWithProviders<FileManagementConfigModule> {
    return {
      ngModule: FileManagementConfigModule,
      providers: [
        FILE_MANAGEMENT_ROUTE_PROVIDERS,
        FILE_MANAGEMENT_FEATURES_PROVIDERS,
      ],
    };
  }
}
