import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { NzTreeNode } from 'ng-zorro-antd/tree';
import { TreeNode, SubscriptionService } from '@abp/ng.core';
import {
  DirectoryTreeService,
  ROOT_ID,
} from '../../services/directory-tree.service';
import { NavigatorService } from '../../services/navigator.service';
import { MoveService } from '../../services/move.service';
import { DirectoryContentDto } from '../../proxy/file-management/directories/models';
import { DeleteService } from '../../services/delete.service';
import { eFileManagementPolicyNames } from '@volo/abp.ng.file-management/config';

@Component({
  selector: 'abp-file-management-directory-tree',
  templateUrl: './file-management-directory-tree.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [SubscriptionService],
})
export class FileManagementDirectoryTreeComponent implements OnInit {
  directories: TreeNode<DirectoryContentDto>[];

  createPolicy = eFileManagementPolicyNames.DirectoryDescriptorCreate;
  updatePolicy = eFileManagementPolicyNames.DirectoryDescriptorUpdate;
  deletePolicy = eFileManagementPolicyNames.DirectoryDescriptorDelete;
  contextMenuPolicy = `${this.createPolicy} || ${this.updatePolicy} || ${this.deletePolicy}`;

  createModalVisible = false;
  createModalParentId = '';

  renameModalVisible = false;
  contentToRename: DirectoryContentDto;

  rootId = ROOT_ID;

  updateDirectories = (directories) => {
    this.directories = directories;
    this.cdr.markForCheck();
  };

  constructor(
    public service: DirectoryTreeService,
    private navigator: NavigatorService,
    private move: MoveService,
    private cdr: ChangeDetectorRef,
    private deleteService: DeleteService,
    private subscription: SubscriptionService
  ) {}

  ngOnInit(): void {
    this.subscription.addOne(
      this.service.directoryTree$,
      this.updateDirectories
    );
  }

  onDrop({ dragNode, node }: { dragNode: NzTreeNode; node: NzTreeNode }) {
    this.move
      .moveTo(this.nzNodeToFolderInfo(dragNode), this.nzNodeToFolderInfo(node))
      .subscribe();
  }

  onNodeClick(node) {
    if (node.isRoot) {
      this.onRootClick();
    } else {
      this.navigator.goToFolder(node);
    }
  }

  onRootClick() {
    this.navigator.goToRoot();
  }

  onCreate(node: DirectoryContentDto) {
    this.createModalVisible = true;
    this.createModalParentId = node.id;
  }

  onRename(node: DirectoryContentDto) {
    this.renameModalVisible = true;
    this.contentToRename = { ...node, isDirectory: true };
  }

  onDelete(folder: DirectoryContentDto) {
    this.deleteService.deleteFolder(folder).subscribe();
  }

  private nzNodeToFolderInfo(node: NzTreeNode) {
    return { id: node.origin.id, name: node.origin.title };
  }
}
