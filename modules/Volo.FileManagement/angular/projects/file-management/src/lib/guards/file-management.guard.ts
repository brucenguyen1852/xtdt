import { Inject, Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { FILE_MANAGEMENT_FEATURES } from '@volo/abp.ng.file-management/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModuleVisibility } from '@volo/abp.commercial.ng.ui/config';

@Injectable()
export class FileManagementGuard implements CanActivate {
  constructor(
    @Inject(FILE_MANAGEMENT_FEATURES)
    private auditLoggingFeatures: Observable<ModuleVisibility>
  ) {}

  canActivate() {
    return this.auditLoggingFeatures.pipe(map((features) => features.enable));
  }
}
