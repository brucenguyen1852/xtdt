import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { DirectoryDescriptorService } from '../../proxy/file-management/directories/directory-descriptor.service';
import { UpdateStreamService } from '../../services/update-stream.service';
import { mapRootIdToEmpty } from '../../services/directory-tree.service';

@Injectable()
export class CreateFolderModalService {
  constructor(
    private service: DirectoryDescriptorService,
    private updateStream: UpdateStreamService
  ) {}

  create(name: string, parent?: string) {
    const parentId = mapRootIdToEmpty(
      parent || this.updateStream.currentDirectory
    );
    return this.service
      .create({ name, parentId: mapRootIdToEmpty(parentId) })
      .pipe(
        tap((_) => this.updateStream.patchStore({ createdDirectory: parentId }))
      );
  }
}
