import { DirectoryContentDto } from '../proxy/file-management/directories/models';

export type FolderInfo = Pick<DirectoryContentDto, 'name' | 'id'>;
export type FileInfo = FolderInfo;
