
export interface RemoteStreamContent {
  contentType?: string;
  contentLength?: number;
}
