import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { downloadBlob, EnvironmentService, RestService } from '@abp/ng.core';
import { FileInfo } from '../models/common-types';
import { FileDescriptorService } from '../proxy/file-management/files/file-descriptor.service';

@Injectable()
export class DownloadService {
  apiName = 'FileManagement';

  get apiUrl() {
    return this.environment.getApiUrl(this.apiName);
  }

  constructor(
    private restService: RestService,
    private fileDescriptorService: FileDescriptorService,
    private environment: EnvironmentService
  ) {}

  downloadFile(file: FileInfo) {
    return this.fileDescriptorService.getDownloadToken(file.id).pipe(
      tap((res) => {
        window.open(
          `${this.apiUrl}/api/file-management/file-descriptor/download/${file.id}?token=${res.token}`,
          '_self'
        );
      })
    );
  }

  /**
   *
   * @deprecated to be deleted in v5.0
   */
  download = (id: string) =>
    this.restService.request<any, any>(
      {
        method: 'GET',
        url: `/api/file-management/file-descriptor/download/${id}`,
        responseType: 'blob',
      } as any,
      { apiName: this.apiName }
    );
}
