import { Injectable } from '@angular/core';
import { filter, tap, switchMap } from 'rxjs/operators';
import {
  ConfirmationService,
  Confirmation,
  ToasterService,
} from '@abp/ng.theme.shared';
import { DirectoryDescriptorService } from '../proxy/file-management/directories/directory-descriptor.service';
import { FolderInfo } from '../models/common-types';
import { mapRootIdToEmpty } from './directory-tree.service';
import { UpdateStreamService } from './update-stream.service';

@Injectable()
export class MoveService {
  constructor(
    private service: DirectoryDescriptorService,
    private confirmation: ConfirmationService,
    private updateStream: UpdateStreamService,
    private toaster: ToasterService
  ) {}

  moveTo(source: FolderInfo, target: FolderInfo) {
    const id = source.id;
    const newParentId = mapRootIdToEmpty(target.id);
    return this.confirmation
      .warn(
        'FileManagement::DirectoryMoveConfirmMessage',
        'FileManagement::AreYouSure',
        { messageLocalizationParams: [source.name, target.name] }
      )
      .pipe(
        filter((status) => status === Confirmation.Status.confirm),
        switchMap((_) => this.service.move({ id, newParentId })),
        tap((_) => {
          this.updateStream.refreshContent();
          this.toaster.success('FileManagement::SuccessfullyMoved');
        })
      );
  }
}
