import { Injectable, OnDestroy } from '@angular/core';
import { InternalStore } from '@abp/ng.core';
import { Subscription } from 'rxjs';

import { FolderInfo } from '../models/common-types';
import {
  DirectoryTreeService,
  mapRootIdToEmpty,
  ROOT_NODE,
} from './directory-tree.service';
import { UpdateStreamService } from './update-stream.service';

@Injectable()
export class NavigatorService implements OnDestroy {
  private navigatedFolders: FolderInfo[] = [ROOT_NODE];
  private store = new InternalStore<FolderInfo[]>(this.navigatedFolders);
  navigatedFolderPath$ = this.store.sliceState(
    (state) => state,
    (s1, s2) => s1 === s2
  );

  subscription = new Subscription();

  constructor(
    private directory: DirectoryTreeService,
    private updateStream: UpdateStreamService
  ) {
    this.setupListeners();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  goToFolder(folder: FolderInfo) {
    this.updateCurrentFolderPath(folder);
  }

  goToRoot() {
    this.goToFolder(null);
  }

  goUpFolder() {
    if (this.navigatedFolders.length === 0) {
      // already at the root, do nothing.
      return;
    } else if (this.navigatedFolders.length === 1) {
      this.goToRoot();
    } else {
      const folder = this.navigatedFolders[this.navigatedFolders.length - 2];
      this.goToFolder(folder);
    }
  }

  getCurrentFolder() {
    return (
      this.navigatedFolders.length &&
      this.navigatedFolders[this.navigatedFolders.length - 1]
    );
  }

  getCurrentFolderId() {
    return mapRootIdToEmpty(this.getCurrentFolder()?.id);
  }

  private updateCurrentFolderPath(folder: FolderInfo) {
    this.next([...this.directory.findFullPathOf(folder)]);
  }

  private next(newValue: FolderInfo[]) {
    this.navigatedFolders = newValue;
    this.store.patch(this.navigatedFolders);

    this.updateStream.patchStore({
      currentDirectory: this.getCurrentFolderId(),
    });
  }

  private updateFolderName = (updatedFolder: { id: string; name: string }) => {
    const newNavigatedFolders = [...this.navigatedFolders];
    newNavigatedFolders
      .filter((folder) => folder.id === updatedFolder.id)
      .forEach((folder) => (folder.name = updatedFolder.name));
    this.next(newNavigatedFolders);
  };

  private removeDeletedFolder = (id: string) => {
    const index = this.navigatedFolders.findIndex((folder) => folder.id === id);
    if (index > -1) {
      this.next(this.navigatedFolders.slice(0, index));
    }
  };

  private setupListeners() {
    this.subscription.add(
      this.updateStream.directoryRename$.subscribe(this.updateFolderName)
    );
    this.subscription.add(
      this.updateStream.directoryDelete$.subscribe(this.removeDeletedFolder)
    );
  }
}
