import { Injectable } from '@angular/core';
import { skip, filter } from 'rxjs/operators';
import { InternalStore } from '@abp/ng.core';
import { merge } from 'rxjs';

export interface UpdateStreamState {
  content: number;
  createdDirectory: string;
  renamedDirectory: { id: string; name: string };
  deletedDirectory: string;
  currentDirectory: string;
}

@Injectable()
export class UpdateStreamService {
  private store = new InternalStore<Partial<UpdateStreamState>>({});
  readonly currentDirectory$ = this.sliceState(
    (state) => state.currentDirectory
  );
  get currentDirectory() {
    return this.store.state.currentDirectory;
  }

  readonly directoryRename$ = this.sliceState(
    (state) => state.renamedDirectory
  );
  readonly directoryDelete$ = this.sliceState(
    (state) => state.deletedDirectory
  );
  readonly directoryCreate$ = this.sliceState(
    (state) => state.createdDirectory
  );

  readonly contentRefresh$ = merge(
    this.sliceState((state) => state.content),
    this.currentDirectory$,
    this.directoryCreate$.pipe(filter((id) => id === this.currentDirectory))
  );

  patchStore = (state: Partial<UpdateStreamState>) => this.store.patch(state);

  refreshContent = () => this.store.patch({ content: new Date().getTime() });

  private sliceState<Slice>(fn: (state: UpdateStreamState) => Slice) {
    // skip initial empty values
    return this.store.sliceState(fn).pipe(skip(1));
  }
}
