using Volo.FileManagement.MongoDB;
using Volo.Abp.Modularity;

namespace Volo.FileManagement
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(FileManagementMongoDbTestModule)
        )]
    public class FileManagementDomainTestModule : AbpModule
    {
        
    }
}
