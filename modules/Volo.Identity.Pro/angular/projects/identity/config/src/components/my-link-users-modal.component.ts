import { AuthService, ConfigStateService, EnvironmentService, ListService } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, Injector, OnInit } from '@angular/core';
import { IdentityLinkUserService, LinkUserDto } from '@volo/abp.commercial.ng.ui/config';
import { LinkLoginHandler } from '../handlers/link-login.handler';

@Component({
  selector: 'abp-my-link-users-modal',
  templateUrl: 'my-link-users-modal.component.html',
  providers: [ListService],
})
export class MyLinkUsersModalComponent implements OnInit {
  private _isModalOpen = true;
  get isModalOpen() {
    return this._isModalOpen;
  }
  set isModalOpen(value: boolean) {
    if (value && !this._isModalOpen) this.getAllList();
    this._isModalOpen = value;
  }

  data: LinkUserDto[] = [];

  get currentUser() {
    return this.configState.getAll().currentUser;
  }

  get currentTenant() {
    return this.configState.getAll().currentTenant;
  }

  readonly list: ListService;
  protected linkUserService: IdentityLinkUserService;
  protected confirmation: ConfirmationService;
  protected configState: ConfigStateService;
  protected authService: AuthService;
  protected environment: EnvironmentService;
  protected linkLoginHandler: LinkLoginHandler;

  constructor(private injector: Injector) {
    this.list = injector.get(ListService);
    this.linkUserService = injector.get(IdentityLinkUserService);
    this.confirmation = injector.get(ConfirmationService);
    this.configState = injector.get(ConfigStateService);
    this.authService = injector.get(AuthService);
    this.environment = injector.get(EnvironmentService);
    this.linkLoginHandler = injector.get(LinkLoginHandler);
  }

  private navigateToLogin() {
    this.linkUserService.generateLinkToken().subscribe(token => {
      if (this.authService.isInternalAuth) {
        const { id: linkUserId, userName: linkUserName } = this.currentUser;
        const { id: linkTenantId, name: linkTenantName } = this.currentTenant;

        this.authService.logout().subscribe(() => {
          this.authService.navigateToLogin({
            linkToken: token,
            linkUserId,
            linkUserName,
            ...(linkTenantId && {
              linkTenantId,
              linkTenantName,
            }),
          });
          this.isModalOpen = false;
        });

        return;
      }

      const { issuer, redirectUri } = this.environment.getEnvironment().oAuthConfig;

      const returnUrl = `${redirectUri}?handler=linkLogin&linkUserId=${this.currentUser.id}${
        this.currentTenant.id ? '&linkTenantId=' + this.currentTenant.id : ''
      }`;

      const fullUrl = `${issuer}/Account/Login?LinkUserId=${this.currentUser.id}${
        this.currentTenant.id ? '&LinkTenantId=' + this.currentTenant.id : ''
      }&LinkToken=${encodeURIComponent(token)}&ReturnUrl=${encodeURIComponent(returnUrl)}`;

      window.open(fullUrl, '_self');
    });
  }

  ngOnInit() {
    this.getAllList();
  }

  getAllList() {
    this.linkUserService.getAllList().subscribe(res => (this.data = res.items));
  }

  createNewLinkUser() {
    this.confirmation.warn('AbpAccount::NewLinkUserWarning', 'AbpUi::AreYouSure').subscribe(res => {
      if (res === Confirmation.Status.confirm) {
        this.navigateToLogin();
      }
    });
  }

  delete(record: LinkUserDto) {
    this.confirmation
      .warn('AbpAccount::DeleteLinkUserConfirmationMessage', 'AbpUi::AreYouSure', {
        messageLocalizationParams: [record.targetUserName],
      })
      .subscribe(res => {
        if (res === Confirmation.Status.confirm) {
          this.linkUserService
            .unlink({ userId: record.targetUserId, tenantId: record.targetTenantId })
            .subscribe(() => this.getAllList());
        }
      });
  }

  loginAsThisUser(record: LinkUserDto) {
    this.linkLoginHandler
      .linkLogin({
        linkUserId: record.targetUserId,
        linkTenantId: record.targetTenantId,
      })
      .subscribe(() => (this.isModalOpen = false));
  }
}
