export const enum eIdentityPolicyNames {
  IdentityManagement = 'AbpIdentity.Roles || AbpIdentity.Users || AbpIdentity.ClaimTypes || AbpIdentity.OrganizationUnits',
  Roles = 'AbpIdentity.Roles',
  Users = 'AbpIdentity.Users',
  ClaimTypes = 'AbpIdentity.ClaimTypes',
  OrganizationUnits = 'AbpIdentity.OrganizationUnits',
  SecurityLogs = 'AbpIdentity.SecurityLogs',
}
