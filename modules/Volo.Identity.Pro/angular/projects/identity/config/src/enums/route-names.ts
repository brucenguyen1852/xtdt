export const enum eIdentityRouteNames {
  IdentityManagement = 'AbpIdentity::Menu:IdentityManagement',
  Roles = 'AbpIdentity::Roles',
  Users = 'AbpIdentity::Users',
  ClaimTypes = 'AbpIdentity::ClaimTypes',
  OrganizationUnits = 'AbpIdentity::OrganizationUnits',
  SecurityLogs = 'AbpIdentity::SecurityLogs',
}
