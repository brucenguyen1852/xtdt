import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IdentityLinkUserService, LinkLoginInput } from '@volo/abp.commercial.ng.ui/config';
import { from, of } from 'rxjs';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';
import { ToasterService } from '@abp/ng.theme.shared';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class LinkLoginHandler {
  private handleLinkLoginError = (err: HttpErrorResponse) => {
    this.toaster.error(err.error?.error);
    return of(null);
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private identityLinkUserService: IdentityLinkUserService,
    private toaster: ToasterService,
  ) {
    this.listenToQueryParams();
  }

  private listenToQueryParams() {
    this.route.queryParams
      .pipe(
        filter(params => params.handler === 'linkLogin' && params.linkUserId),
        switchMap((params: LinkLoginInput) => this.linkLogin(params)),
      )
      .subscribe();
  }

  linkLogin(input: LinkLoginInput) {
    return this.identityLinkUserService.linkLogin(input).pipe(
      catchError(this.handleLinkLoginError),
      tap(res => {
        localStorage.setItem('access_token', res.access_token);
        if (res.refresh_token) localStorage.setItem('refresh_token', res.refresh_token);
      }),
      switchMap(() =>
        from(
          this.router.navigate(['.'], {
            relativeTo: this.route,
            queryParams: { handler: null, linkUserId: null, linkTenantId: null },
            queryParamsHandling: 'merge',
          }),
        ),
      ),
      tap(() => location.reload()),
    );
  }
}
