import { CoreModule, noop } from '@abp/ng.core';
import { ThemeSharedModule } from '@abp/ng.theme.shared';
import { APP_INITIALIZER, Injector, ModuleWithProviders, NgModule } from '@angular/core';
import { IdentitySettingsComponent } from './components/identity-settings.component';
import { MyLinkUsersModalComponent } from './components/my-link-users-modal.component';
import { IDENTITY_ROUTE_PROVIDERS } from './providers/route.provider';
import { IDENTITY_SETTING_TAB_PROVIDERS } from './providers/setting-tab.provider';
import { OPEN_MY_LINK_USERS_MODAL } from '@volo/abp.commercial.ng.ui/config';
import { openMyLinkUsersFactory } from './utils/factories';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { LinkLoginHandler } from './handlers/link-login.handler';

@NgModule({
  imports: [CoreModule, ThemeSharedModule, NgbDropdownModule],
  declarations: [IdentitySettingsComponent, MyLinkUsersModalComponent],
  exports: [IdentitySettingsComponent, MyLinkUsersModalComponent],
})
export class IdentityConfigModule {
  static forRoot(): ModuleWithProviders<IdentityConfigModule> {
    return {
      ngModule: IdentityConfigModule,
      providers: [
        IDENTITY_ROUTE_PROVIDERS,
        IDENTITY_SETTING_TAB_PROVIDERS,
        { provide: OPEN_MY_LINK_USERS_MODAL, useFactory: openMyLinkUsersFactory, deps: [Injector] },
        { provide: APP_INITIALIZER, multi: true, useFactory: noop, deps: [LinkLoginHandler] },
      ],
    };
  }
}
