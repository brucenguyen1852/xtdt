import { ComponentRef, Injector } from '@angular/core';
import { ContentProjectionService, PROJECTION_STRATEGY } from '@abp/ng.core';
import { MyLinkUsersModalComponent } from '../components/my-link-users-modal.component';

export function openMyLinkUsersFactory(injector: Injector) {
  const contentProjectionService = injector.get(ContentProjectionService);
  let componentRef: ComponentRef<MyLinkUsersModalComponent>;

  return () => {
    if (componentRef) {
      componentRef.instance.isModalOpen = true;
      return;
    }

    componentRef = contentProjectionService.projectContent(
      PROJECTION_STRATEGY.AppendComponentToBody(MyLinkUsersModalComponent),
    );
  };
}
