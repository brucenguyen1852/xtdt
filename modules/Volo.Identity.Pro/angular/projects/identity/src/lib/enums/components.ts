export const enum eIdentityComponents {
  Claims = 'Identity.ClaimsComponent',
  Roles = 'Identity.RolesComponent',
  Users = 'Identity.UsersComponent',
  OrganizationUnits = 'Identity.OrganizationUnitsComponent',
  OrganizationMembers = 'Identity.OrganizationMembersComponent',
  OrganizationRoles = 'Identity.OrganizationRolesComponent',
  SecurityLogs = 'Identity.SecurityLogs',
}
