﻿namespace Volo.Abp.Identity.Features
{
    public class IdentityProFeature
    {
        public const string GroupName = "Identity";

        public const string TwoFactor = GroupName + ".TwoFactor";
    }
}
