﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace Volo.Abp.Identity.MongoDB
{
    [DependsOn(
        typeof(AbpIdentityProDomainModule),
        typeof(AbpIdentityMongoDbModule),
        typeof(AbpMongoDbModule)
        )]
    public class AbpIdentityProMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<IdentityProMongoDbContext>(options =>
            {
                options.ReplaceDbContext<IAbpIdentityMongoDbContext>();
            });
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            LicenseChecker.Check<AbpIdentityProDomainModule>(context);
        }
    }
}
