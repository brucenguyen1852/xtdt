﻿using System;
using Volo.Abp.MongoDB;

namespace Volo.Abp.Identity.MongoDB
{
    public static class IdentityProMongoDbContextExtensions
    {
        public static void ConfigureIdentityPro(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new IdentityMongoModelBuilderConfigurationOptions(
                AbpIdentityDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);

            builder.ConfigureIdentity(configurationOptions =>
            {
                configurationOptions.CollectionPrefix = options.CollectionPrefix;
            });

        }
    }
}
