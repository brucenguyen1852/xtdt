﻿using JetBrains.Annotations;

namespace Volo.Abp.Identity.MongoDB
{
    public class IdentityProMongoModelBuilderConfigurationOptions : IdentityMongoModelBuilderConfigurationOptions
    {
        public IdentityProMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}
