﻿(function ($) {

    var l = abp.localization.getResource('AbpIdentity');

    var _identityClaimTypeAppService = volo.abp.identity.identityClaimType;
    var _editModal = new abp.ModalManager(abp.appPath + 'Identity/ClaimTypes/EditModal');
    var _createModal = new abp.ModalManager(abp.appPath + 'Identity/ClaimTypes/CreateModal');

    var _dataTable = null;

    abp.ui.extensions.entityActions.get("identity.claimType").addContributor(
        function (actionList) {
            return actionList.addManyTail(
                [
                    {
                        text: l('Edit'),
                        visible: function (data) {
                            return abp.auth.isGranted('AbpIdentity.ClaimTypes.Update') && !data.isStatic;
                        },
                        action: function (data) {
                            _editModal.open({
                                id: data.record.id
                            });
                        }
                    },
                    {
                        text: l('Delete'),
                        visible: abp.auth.isGranted('AbpIdentity.ClaimTypes.Delete'),
                        //confirmMessage: function (data) {
                        //    return l('ClaimTypeDeletionConfirmationMessage', data.record.name);
                        //},
                        action: function (data) {
                            swal({
                                title: l("Swal:Notification"),
                                text: l("ClaimTypeDeletionConfirmationMessage", data.record.name),
                                icon: "warning",
                                buttons: [
                                    l("Swal:Cancel"),
                                    l("Swal:Agree")
                                ],
                            }).then((value) => {
                                if (value) {
                                    _identityClaimTypeAppService
                                        .delete(data.record.id)
                                        .then(function () {
                                            _dataTable.ajax.reload(null, false);
                                        });
                                }
                            });
                        }
                    }
                ]
            );
        }
    );

    abp.ui.extensions.tableColumns.get("identity.claimType").addContributor(
        function (columnList) {
            columnList.addManyTail(
                [
                    {
                        title: l("Name"),
                        data: "name",
                        className: "name-th",
                    },
                    {
                        title: l("ValueType"),
                        data: "valueTypeAsString",
                        className: "type-th",
                        orderable: false
                    },
                    {
                        title: l("Description"),
                        data: "description",
                        className: "description-th",
                    },
                    {
                        title: l("Regex"),
                        data: "regex",
                        className: "regex-th",
                    },
                    {
                        title: l("Required"),
                        data: "required",
                        className: "required-th",
                        render: function (data) {
                            if (data) {
                                return '<i class="fa fa-check"></i>';
                            }
                            return '<i class="fa fa-times"></i>';
                        }
                    },
                    {
                        title: l("IsStatic"),
                        data: "isStatic",
                        className: "isStatic-th",
                        render: function (data) {
                            if (data) {
                                return '<i class="fa fa-check"></i>';
                            }
                            return '<i class="fa fa-times"></i>';
                        }
                    },
                    {
                        title: l("Roles:Action"),
                        targets: 0,
                        data: null,
                        defaultContent: '',
                        className: "delete-th",
                        rowAction:  {
                            targets: 0,
                            autoWidth: false,
                            orderable: false,
                            //className: 'text-center width-10',
                            defaultContent: '',
                            element: $("<button/>")
                                .addClass("btn btn-danger")
                                .attr("title", l('Delete'))
                                .append($("<i/>").addClass("fa fa-trash")).click(function () {
                                    var data = $(this).data();
                                    swal({
                                        title: l("Swal:Notification"),
                                        text: l("ClaimTypeDeletionConfirmationMessage", data.name),
                                        icon: "warning",
                                        buttons: [
                                            l("Swal:Cancel"),
                                            l("Swal:Agree")
                                        ],
                                    }).then((value) => {
                                        if (value) {
                                            _identityClaimTypeAppService
                                                .delete(data.id)
                                                .then(function () {
                                                    _dataTable.ajax.reload(null, false);
                                                });
                                        }
                                    });
                                }),
                            visible: function () {
                                return abp.auth.isGranted('AbpIdentity.ClaimTypes.Delete');
                            }
                        },
                        //rowAction: {
                        //    items: abp.ui.extensions.entityActions.get("identity.claimType").actions.toArray()
                        //}
                    }
                ]
            );
        },
        0 //adds as the first contributor
    );

    $(function () {

        var getFilter = function () {
            return {
                filter: $('#IdentityClaimTypesWrapper input.page-search-filter-text').val()
            };
        };

        _dataTable = $('#IdentityClaimTypesWrapper table').DataTable(abp.libs.datatables.normalizeConfiguration({
            order: [[0, "asc"]],
            processing: true,
            serverSide: true,
            searching: false,
            scrollX: true,
            paging: true,
            ajax: abp.libs.datatables.createAjax(_identityClaimTypeAppService.getList, getFilter),
            columnDefs: abp.ui.extensions.tableColumns.get("identity.claimType").columns.toArray()
        }));

        _createModal.onResult(function () {
            _dataTable.ajax.reload(null, false);
        });

        _editModal.onResult(function () {
            _dataTable.ajax.reload(null, false);
        });

        $('button[name=CreateClaimType]').click(function (e) {
            e.preventDefault();
            _createModal.open();
        });

        $('#IdentityClaimTypesWrapper form.page-search-form').submit(function (e) {
            e.preventDefault();
            _dataTable.ajax.reload();
        });
    });

})(jQuery);
