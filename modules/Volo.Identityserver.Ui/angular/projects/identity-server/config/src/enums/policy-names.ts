export const enum eIdentityServerPolicyNames {
  IdentityServer = 'IdentityServer.Client || IdentityServer.IdentityResource || IdentityServer.ApiResource',
  Clients = 'IdentityServer.Client',
  IdentityResources = 'IdentityServer.IdentityResource',
  ApiResources = 'IdentityServer.ApiResource',
  ApiScopes = 'IdentityServer.ApiScope',
}
