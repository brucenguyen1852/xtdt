export const enum eIdentityServerRouteNames {
  IdentityServer = 'AbpIdentityServer::Menu:IdentityServer',
  Clients = 'AbpIdentityServer::Menu:Clients',
  IdentityResources = 'AbpIdentityServer::Menu:IdentityResources',
  ApiResources = 'AbpIdentityServer::Menu:ApiResources',
  ApiScopes = 'AbpIdentityServer::Menu:ApiScopes',
}
