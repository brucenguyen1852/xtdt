export const enum eIdentityServerComponents {
  IdentityResources = 'IdentityServer.IdentityResourcesComponent',
  Clients = 'IdentityServer.ClientsComponent',
  ApiResources = 'IdentityServer.ApiResourcesComponent',
  ApiScopes = 'IdentityServer.ApiScopesComponent',
}
