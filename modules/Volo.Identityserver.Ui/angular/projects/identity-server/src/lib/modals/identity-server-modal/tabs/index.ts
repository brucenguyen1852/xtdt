export * from './identity-server-modal-claims-tab/identity-server-modal-claims-tab.component';
export * from './identity-server-modal-free-text-values-tab/identity-server-modal-free-text-values-tab.component';
export * from './identity-server-modal-info-tab/identity-server-modal-info-tab.component';
export * from './identity-server-modal-properties-tab/identity-server-modal-properties-tab.component';
export * from './identity-server-modal-secrets-tab/identity-server-modal-secrets-tab.component';
export * from './identity-server-modal-tab-group.component';
export * from './identity-server-modal-tab.component';
