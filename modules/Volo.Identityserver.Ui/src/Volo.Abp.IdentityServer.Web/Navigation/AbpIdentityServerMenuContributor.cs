﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System.Threading.Tasks;
using Volo.Abp.IdentityServer.Localization;
using Volo.Abp.UI.Navigation;

namespace Volo.Abp.IdentityServer.Web.Navigation
{
    public class AbpIdentityServerMenuContributor : IMenuContributor
    {
        public virtual async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        protected virtual Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            var l = context.GetLocalizer<AbpIdentityServerResource>();

            var identityServerMenuItem = new ApplicationMenuItem(
                AbpIdentityServerMenuNames.GroupName,
                l["Menu:IdentityServer"],
                icon: "fa fa-server"
            );

            context.Menu.GetAdministration().AddItem(identityServerMenuItem);

            identityServerMenuItem.AddItem(new ApplicationMenuItem(AbpIdentityServerMenuNames.Clients, l["Menu:Clients"], "~/IdentityServer/Clients", requiredPermissionName: AbpIdentityServerPermissions.Client.Default));
            identityServerMenuItem.AddItem(new ApplicationMenuItem(AbpIdentityServerMenuNames.IdentityResources, l["Menu:IdentityResources"], "~/IdentityServer/IdentityResources", requiredPermissionName: AbpIdentityServerPermissions.IdentityResource.Default));
            identityServerMenuItem.AddItem(new ApplicationMenuItem(AbpIdentityServerMenuNames.ApiResources, l["Menu:ApiResources"], "~/IdentityServer/ApiResources", requiredPermissionName: AbpIdentityServerPermissions.ApiResource.Default));
            identityServerMenuItem.AddItem(new ApplicationMenuItem(AbpIdentityServerMenuNames.ApiScopes, l["Menu:ApiScopes"], "~/IdentityServer/ApiScopes", requiredPermissionName: AbpIdentityServerPermissions.ApiScope.Default));

            return Task.CompletedTask;
        }
    }
}
