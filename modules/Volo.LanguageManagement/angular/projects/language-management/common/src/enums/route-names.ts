export const enum eLanguageManagementRouteNames {
  LanguageManagement = 'LanguageManagement::LanguageManagement',
  Languages = 'LanguageManagement::Languages',
  LanguageTexts = 'LanguageManagement::LanguageTexts',
}
