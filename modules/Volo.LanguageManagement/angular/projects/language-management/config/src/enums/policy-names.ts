export const enum eLanguageManagementPolicyNames {
  LanguageManagement = 'LanguageManagement.Languages || LanguageManagement.LanguageTexts',
  Languages = 'LanguageManagement.Languages',
  LanguageTexts = 'LanguageManagement.LanguageTexts',
}
