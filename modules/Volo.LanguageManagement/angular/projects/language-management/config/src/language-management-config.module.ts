import { ModuleWithProviders, NgModule } from '@angular/core';
import { LANGUAGE_MANAGEMENT_FEATURES_PROVIDERS } from '@volo/abp.ng.language-management/common';
import { LANGUAGE_MANAGEMENT_ROUTE_PROVIDERS } from './providers/route.provider';

@NgModule()
export class LanguageManagementConfigModule {
  static forRoot(): ModuleWithProviders<LanguageManagementConfigModule> {
    return {
      ngModule: LanguageManagementConfigModule,
      providers: [LANGUAGE_MANAGEMENT_ROUTE_PROVIDERS, LANGUAGE_MANAGEMENT_FEATURES_PROVIDERS],
    };
  }
}
