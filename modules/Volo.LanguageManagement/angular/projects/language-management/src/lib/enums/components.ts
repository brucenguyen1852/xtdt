export const enum eLanguageManagementComponents {
  Languages = 'LanguageManagement.LanguagesComponent',
  LanguageTexts = 'LanguageManagement.LanguageTextsComponent',
}
