import { Inject, Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { LANGUAGE_MANAGEMENT_FEATURES } from '@volo/abp.ng.language-management/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModuleVisibility } from '@volo/abp.commercial.ng.ui/config';

@Injectable()
export class LanguageManagementGuard implements CanActivate {
  constructor(
    @Inject(LANGUAGE_MANAGEMENT_FEATURES)
    private languageManagementFeatures: Observable<ModuleVisibility>,
  ) {}

  canActivate() {
    return this.languageManagementFeatures.pipe(map(features => features.enable));
  }
}
