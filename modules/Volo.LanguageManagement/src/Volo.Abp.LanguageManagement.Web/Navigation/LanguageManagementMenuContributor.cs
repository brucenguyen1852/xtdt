﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Volo.Abp.Features;
using Volo.Abp.LanguageManagement.Localization;
using Volo.Abp.UI.Navigation;

namespace Volo.Abp.LanguageManagement.Navigation
{
    public class LanguageManagementMenuContributor : IMenuContributor
    {
        public virtual async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        protected virtual async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            var featureChecker = context.ServiceProvider.GetRequiredService<IFeatureChecker>();

            if (!await featureChecker.IsEnabledAsync(LanguageManagementFeatures.Enable))
            {
                return;
            }

            var l = context.GetLocalizer<LanguageManagementResource>();

            var languagesMenu = new ApplicationMenuItem(
                LanguageManagementMenuNames.GroupName,
                l["Menu:Languages"],
                icon: "fa fa-globe"
            );

            context.Menu.GetAdministration().AddItem(languagesMenu);

            languagesMenu.AddItem(new ApplicationMenuItem(LanguageManagementMenuNames.Languages, l["Languages"], "~/LanguageManagement", requiredPermissionName: LanguageManagementPermissions.Languages.Default));
            languagesMenu.AddItem(new ApplicationMenuItem(LanguageManagementMenuNames.Texts, l["LanguageTexts"], "~/LanguageManagement/Texts", requiredPermissionName: LanguageManagementPermissions.LanguageTexts.Default));
        }
    }
}
