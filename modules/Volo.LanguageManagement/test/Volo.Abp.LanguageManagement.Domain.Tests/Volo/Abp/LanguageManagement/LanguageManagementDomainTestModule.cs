using Volo.Abp.LanguageManagement.MongoDB;
using Volo.Abp.Modularity;

namespace Volo.Abp.LanguageManagement
{
    [DependsOn(
        typeof(LanguageManagementMongoDbTestModule)
        )]
    public class LanguageManagementDomainTestModule : AbpModule
    {

    }
}
