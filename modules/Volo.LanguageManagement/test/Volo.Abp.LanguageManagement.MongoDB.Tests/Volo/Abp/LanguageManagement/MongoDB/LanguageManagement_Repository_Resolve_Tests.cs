﻿using Xunit;

namespace Volo.Abp.LanguageManagement.MongoDB
{
    [Collection(MongoTestCollection.Name)]
    public class LanguageManagement_Repository_Resolve_Tests : LanguageManagement_Repository_Resolve_Tests<LanguageManagementMongoDbTestModule>
    {

    }
}
