import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { eThemeLeptonComponents } from '../../enums';
import { SubscriptionService } from '@abp/ng.core';
import { Store } from '@ngxs/store';
import { LayoutStateService } from '../../services/layout-state.service';

@Component({
  selector: 'abp-header',
  templateUrl: './header.component.html',
  providers: [SubscriptionService],
  encapsulation: ViewEncapsulation.None,
})
export class HeaderComponent implements AfterViewInit {
  isNavbarExpanded$ = this.layoutStateService.get$('isNavbarExpanded');
  isMenuExpanded$ = this.layoutStateService.get$('isMenuExpanded');
  smallScreen$ = this.layoutStateService.get$('smallScreen');
  isMenuPlacementTop$ = this.layoutStateService.get$('isMenuPlacementTop');
  logoComponentKey = eThemeLeptonComponents.Logo;
  navbarComponentKey = eThemeLeptonComponents.Navbar;
  navbarMobileComponentKey = eThemeLeptonComponents.NavbarMobile;
  sidebarComponentKey = eThemeLeptonComponents.Sidebar;

  @Output()
  mouseMoveContainer = new EventEmitter<Array<ElementRef<HTMLElement>>>();

  @ViewChild('navbarBrand', { read: ElementRef })
  navbarBrandRef: ElementRef<any>;
  constructor(
    private subscription: SubscriptionService,
    private renderer: Renderer2,
    private store: Store,
    private layoutStateService: LayoutStateService,
  ) {}

  ngAfterViewInit() {
    this.mouseMoveContainer.next([this.navbarBrandRef]);
  }

  navbarIconClick(isNavbarExpanded: boolean) {
    this.layoutStateService.patch({ isNavbarExpanded });
  }

  menuIconClick(isMenuExpanded: boolean) {
    this.layoutStateService.patch({ isMenuExpanded });
  }
}
