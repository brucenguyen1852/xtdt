﻿using Volo.Abp.Ui.Branding;

namespace Volo.Abp.LeptonTheme.Blazor.Host
{
    public class LeptonThemeDemoHostBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "MyProjectName";
    }
}
