﻿namespace Volo.Abp.AspNetCore.Components.Server.LeptonTheme.Bundling
{
    public class BlazorLeptonThemeBundles
    {
        public static class Styles
        {
            public static string Global = "Blazor.LeptonTheme.Global";
        }

        public static class Scripts
        {
            public static string Global = "Blazor.LeptonTheme.Global";
        }
    }
}