﻿using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Components.Server.LeptonTheme.Components.ApplicationLayout.MainHeader;
using Volo.Abp.AspNetCore.Components.Web.Theming.Toolbars;

namespace Volo.Abp.AspNetCore.Components.Server.LeptonTheme
{
    public class LeptonThemeToolbarContributor : IToolbarContributor
    {
        public Task ConfigureToolbarAsync(IToolbarConfigurationContext context)
        {
            if (context.Toolbar.Name == StandardToolbars.Main)
            {
                context.Toolbar.Items.Add(new ToolbarItem(typeof(MainHeaderToolbarLanguageSwitch)));
                context.Toolbar.Items.Add(new ToolbarItem(typeof(MainHeaderToolbarUserMenu)));
            }

            return Task.CompletedTask;
        }
    }
}
