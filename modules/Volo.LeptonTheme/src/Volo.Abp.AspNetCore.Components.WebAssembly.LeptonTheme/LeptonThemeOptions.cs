﻿using System;

namespace Volo.Abp.AspNetCore.Components.WebAssembly.LeptonTheme
{
    public class LeptonThemeOptions
    {
        public Type FooterComponent { get; set; }
    }
}
