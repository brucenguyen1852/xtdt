using Volo.Abp.Account;
using Volo.Abp.AuditLogging;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer;
using Volo.Abp.LanguageManagement;
using Volo.Abp.LeptonTheme.Management;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TextTemplateManagement;
using Volo.Saas.Host;
using Volo.FileManagement;
using IoT.HTKT;
using IoT.KhaiThac;
using IoT.ThongKe;
using IoT.PermissionApp;
using IoT.Geoserver;

namespace IoT
{
    [DependsOn(
        typeof(IoTDomainSharedModule),
        typeof(AbpFeatureManagementApplicationContractsModule),
        typeof(AbpIdentityApplicationContractsModule),
        typeof(AbpPermissionManagementApplicationContractsModule),
        typeof(SaasHostApplicationContractsModule),
        typeof(AbpAuditLoggingApplicationContractsModule),
        typeof(AbpIdentityServerApplicationContractsModule),
        typeof(AbpAccountPublicApplicationContractsModule),
        typeof(AbpAccountAdminApplicationContractsModule),
        typeof(LanguageManagementApplicationContractsModule),
        typeof(LeptonThemeManagementApplicationContractsModule),
        typeof(TextTemplateManagementApplicationContractsModule)
    )]
    [DependsOn(typeof(AbpAccountSharedApplicationContractsModule))]
    [DependsOn(typeof(FileManagementApplicationContractsModule))]
    [DependsOn(typeof(HTKTApplicationContractsModule))]
    [DependsOn(typeof(KhaiThacApplicationContractsModule))]
    [DependsOn(typeof(ThongKeApplicationContractsModule))]
    [DependsOn(typeof(PermissionAppApplicationContractsModule))]
    [DependsOn(typeof(GeoserverApplicationContractsModule))]
    public class IoTApplicationContractsModule : AbpModule
    {

    }
}
