using AutoMapper;
using IoT.Users;
using Volo.Abp.AutoMapper;

namespace IoT
{
    public class IoTApplicationAutoMapperProfile : Profile
    {
        public IoTApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */

            CreateMap<AppUser, AppUserDto>().Ignore(x => x.ExtraProperties);
        }
    }
}