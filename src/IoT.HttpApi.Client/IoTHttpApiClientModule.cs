using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Account;
using Volo.Abp.AuditLogging;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer;
using Volo.Abp.LanguageManagement;
using Volo.Abp.LeptonTheme.Management;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TextTemplateManagement;
using Volo.Saas.Host;
using Volo.FileManagement;
using IoT.HTKT;
using IoT.KhaiThac;
using IoT.ThongKe;
using IoT.PermissionApp;
using IoT.Geoserver;

namespace IoT
{
    [DependsOn(
        typeof(IoTApplicationContractsModule),
        typeof(AbpIdentityHttpApiClientModule),
        typeof(AbpPermissionManagementHttpApiClientModule),
        typeof(AbpFeatureManagementHttpApiClientModule),
        typeof(SaasHostHttpApiClientModule),
        typeof(AbpAuditLoggingHttpApiClientModule),
        typeof(AbpIdentityServerHttpApiClientModule),
        typeof(AbpAccountAdminHttpApiClientModule),
        typeof(AbpAccountPublicHttpApiClientModule),
        typeof(LanguageManagementHttpApiClientModule),
        typeof(LeptonThemeManagementHttpApiClientModule),
        typeof(TextTemplateManagementHttpApiClientModule)
    )]
    [DependsOn(typeof(FileManagementHttpApiClientModule))]
    [DependsOn(typeof(HTKTHttpApiClientModule))]
    [DependsOn(typeof(KhaiThacHttpApiClientModule))]
    [DependsOn(typeof(ThongKeHttpApiClientModule))]
    [DependsOn(typeof(PermissionAppHttpApiClientModule))]
    [DependsOn(typeof(GeoserverHttpApiClientModule))]
    public class IoTHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "Default";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(IoTApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
