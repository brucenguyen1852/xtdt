using Localization.Resources.AbpUi;
using IoT.Localization;
using Volo.Abp.Account;
using Volo.Abp.AuditLogging;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer;
using Volo.Abp.LanguageManagement;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.HttpApi;
using Volo.Saas.Host;
using Volo.Abp.LeptonTheme;
using Volo.Abp.Localization;
using Volo.Abp.TextTemplateManagement;
using Volo.FileManagement;
using IoT.HTKT;
using IoT.KhaiThac;
using IoT.ThongKe;
using IoT.PermissionApp;
using IoT.Geoserver;

namespace IoT
{
    [DependsOn(
        typeof(IoTApplicationContractsModule),
        typeof(AbpIdentityHttpApiModule),
        typeof(AbpPermissionManagementHttpApiModule),
        typeof(AbpFeatureManagementHttpApiModule),
        typeof(AbpAuditLoggingHttpApiModule),
        typeof(AbpIdentityServerHttpApiModule),
        typeof(AbpAccountAdminHttpApiModule),
        typeof(AbpAccountPublicHttpApiModule),
        typeof(LanguageManagementHttpApiModule),
        typeof(SaasHostHttpApiModule),
        typeof(LeptonThemeManagementHttpApiModule),
        typeof(TextTemplateManagementHttpApiModule)
        )]
    [DependsOn(typeof(FileManagementHttpApiModule))]
    [DependsOn(typeof(HTKTHttpApiModule))]
    [DependsOn(typeof(KhaiThacHttpApiModule))]
    [DependsOn(typeof(ThongKeHttpApiModule))]
    [DependsOn(typeof(PermissionAppHttpApiModule))]
    [DependsOn(typeof(GeoserverHttpApiModule))]
    public class IoTHttpApiModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            ConfigureLocalization();
        }

        private void ConfigureLocalization()
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<IoTResource>()
                    .AddBaseTypes(
                        typeof(AbpUiResource)
                    );
            });
        }
    }
}
