using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AuditLogging.MongoDB;
using Volo.Abp.BackgroundJobs.MongoDB;
using Volo.Abp.FeatureManagement.MongoDB;
using Volo.Abp.Identity.MongoDB;
using Volo.Abp.IdentityServer.MongoDB;
using Volo.Abp.LanguageManagement.MongoDB;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.MongoDB;
using Volo.Abp.SettingManagement.MongoDB;
using Volo.Abp.TextTemplateManagement.MongoDB;
using Volo.Saas.MongoDB;
using Volo.Abp.BlobStoring.Database.MongoDB;
using Volo.Abp.Uow;
using Volo.FileManagement.MongoDB;
using IoT.HTKT.MongoDB;
using IoT.KhaiThac.MongoDB;
using IoT.ThongKe.MongoDB;
using IoT.PermissionApp.MongoDB;
using IoT.Geoserver.MongoDB;

namespace IoT.MongoDB
{
    [DependsOn(
        typeof(IoTDomainModule),
        typeof(AbpPermissionManagementMongoDbModule),
        typeof(AbpSettingManagementMongoDbModule),
        typeof(AbpIdentityMongoDbModule),
        typeof(AbpIdentityServerMongoDbModule),
        typeof(AbpBackgroundJobsMongoDbModule),
        typeof(AbpAuditLoggingMongoDbModule),
        typeof(AbpFeatureManagementMongoDbModule),
        typeof(LanguageManagementMongoDbModule),
        typeof(SaasMongoDbModule),
        typeof(TextTemplateManagementMongoDbModule),
        typeof(BlobStoringDatabaseMongoDbModule)
    )]
    //[DependsOn(typeof(AbpIdentityProMongoDbModule))]
    [DependsOn(typeof(FileManagementMongoDbModule))]
    [DependsOn(typeof(HTKTMongoDbModule))]
    [DependsOn(typeof(KhaiThacMongoDbModule))]
    [DependsOn(typeof(ThongKeMongoDbModule))]
    [DependsOn(typeof(PermissionAppMongoDbModule))]
    [DependsOn(typeof(GeoserverMongoDbModule))]
    public class IoTMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<IoTMongoDbContext>(options =>
            {
                options.AddDefaultRepositories();
            });

            Configure<AbpUnitOfWorkDefaultOptions>(options =>
            {
                options.TransactionBehavior = UnitOfWorkTransactionBehavior.Disabled;
            });
        }
    }
}
