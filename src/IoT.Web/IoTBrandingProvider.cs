﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace IoT.Web
{
    [Dependency(ReplaceServices = true)]
    public class IoTBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "IoT";
    }
}
