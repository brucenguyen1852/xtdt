﻿namespace IoT.Web.Menus
{
    public class IoTMenus
    {
        private const string Prefix = "IoT";

        public const string Home = Prefix + ".Home";

        public const string HostDashboard = Prefix + ".HostDashboard";

        public const string TenantDashboard = Prefix + ".TenantDashboard";
    }
}
