﻿using IoT.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace IoT.Web.Pages
{
    public abstract class IoTPageModel : AbpPageModel
    {
        protected IoTPageModel()
        {
            LocalizationResourceType = typeof(IoTResource);
        }
    }
}